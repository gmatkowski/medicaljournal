<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCouriersToDistricts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('districts', function (Blueprint $table) {
            $table->boolean('jayon')->default(0)->after('city_id');
            $table->boolean('acommerce')->default(0)->after('city_id');
            $table->boolean('ninja')->default(0)->after('city_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('districts', function (Blueprint $table) {
            $table->dropColumn(['jayon']);
            $table->dropColumn(['acommerce']);
            $table->dropColumn(['ninja']);
        });
    }
}
