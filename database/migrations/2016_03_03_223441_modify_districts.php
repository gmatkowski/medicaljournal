<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyDistricts extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('districts', function (Blueprint $table)
        {
            $table->dropForeign('districts_province_id_foreign');
            $table->dropColumn(['province_id']);
            $table->integer('city_id')->nullable()->unsigned()->after('name');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('districts', function (Blueprint $table)
        {
            $table->dropForeign('districts_city_id_foreign');
            $table->dropColumn(['city_id']);
            $table->integer('province_id')->unsigned()->after('name');
            $table->foreign('province_id')->references('id')->on('provinces')->onDelete('cascade');
        });
    }
}
