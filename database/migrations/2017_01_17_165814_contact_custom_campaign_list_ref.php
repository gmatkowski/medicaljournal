<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ContactCustomCampaignListRef extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function (Blueprint $table)
        {
            $table->string('callcenter_custom_campaign_ref')->nullable()->after('callcenter_custom_campaign');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table)
        {
            $table->dropColumn(['callcenter_custom_campaign_ref']);
        });
    }
}
