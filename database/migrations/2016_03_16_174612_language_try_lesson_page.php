<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LanguageTryLessonPage extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('language_translations', function (Blueprint $table)
        {
            $table->text('description_try')->after('description_short');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('language_translations', function (Blueprint $table)
        {
            $table->dropColumn(['description_short']);
        });
    }
}
