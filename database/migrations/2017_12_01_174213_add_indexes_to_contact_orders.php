<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToContactOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contacts', function(Blueprint $table)
        {
            $table->index(['created_at', 'email', 'phone', 'product']);
            $table->index(['cc_sell', 'cc_reject', 'cc_hold', 'postback', 'postback_hold']);
        });

        Schema::table('orders', function(Blueprint $table)
        {
            $table->index(['created_at', 'email', 'phone', 'status', 'method', 'shipped']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contacts', function (Blueprint $table)
        {
            $table->dropIndex(['created_at', 'email', 'phone', 'product', 'cc_sell', 'cc_reject', 'cc_hold', 'postback', 'postback_hold']);
        });

        Schema::table('orders', function (Blueprint $table)
        {
            $table->dropIndex(['created_at', 'email', 'phone', 'status', 'method', 'shipped']);
        });
    }
}
