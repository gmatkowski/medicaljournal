<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Orderaddresscountry extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_adresses', function (Blueprint $table)
        {
            $table->dropColumn(['country']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_adresses', function (Blueprint $table)
        {
            $table->string('country')->after('city');
        });
    }
}
