<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Ips extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip2location', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('ip_from');
            $table->integer('ip_to');
            $table->char('country_code', 2);
            $table->string('country_name', 64);
            $table->index('ip_from');
            $table->index('ip_to');
            $table->index(['ip_from', 'ip_to']);
            $table->index('country_code');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ip2location');
    }
}
