<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderAddressAddress extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::rename('order_adresses', 'order_addresses');
        Schema::table('order_addresses', function (Blueprint $table)
        {
            $table->string('address')->after('city')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::rename('order_addresses', 'order_adresses');
        Schema::table('order_addresses', function (Blueprint $table)
        {
            $table->dropColumn(['address']);
        });
    }
}
