<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class HeadersText2Text3 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('header_translations', function (Blueprint $table) {
            $table->string('text2')->after('text');
            $table->string('text3')->after('text2');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('header_translations', function (Blueprint $table) {
            $table->dropColumn(['text2']);
            $table->dropColumn(['text3']);
        });
    }
}
