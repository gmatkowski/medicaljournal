<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddressAndDistrictToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('old_address')->nullable()->after('phone');
            $table->string('old_district')->nullable()->after('old_address');
            $table->boolean('old')->default(0)->after('old_district');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['old_address']);
            $table->dropColumn(['old_district']);
            $table->dropColumn(['old']);
        });
    }
}
