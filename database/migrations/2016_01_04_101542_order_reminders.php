<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderReminders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_reminders', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id')->unsigned()->nullable();
            $table->tinyInteger('status')->default(0);
            $table->tinyInteger('repeated')->default(0);
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order_reminders');
    }
}
