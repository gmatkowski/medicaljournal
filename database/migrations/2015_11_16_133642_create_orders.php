<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrders extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->double('price')->nullable();
            $table->enum('method', ['transfer', 'cc', 'paypal'])->default('transfer');
            $table->string('token', 8)->nullable();
            $table->smallInteger('downloads')->default(0);
            $table->smallInteger('status')->nullable()->default(0);
            $table->string('lang', 2)->default('pl');
            $table->string('tr_id')->nullable();
            $table->dateTime('payed_at')->nullable();
            $table->dateTime('sended_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}
