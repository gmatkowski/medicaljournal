<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('headers', function (Blueprint $table)
        {
            $table->increments('id');
            $table->tinyInteger('active')->default(1);
            $table->timestamps();
        });

        Schema::create('header_translations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('header_id')->unsigned();
            $table->string('text');
            $table->string('locale')->index();
            $table->unique(['header_id', 'locale']);
            $table->foreign('header_id')->references('id')->on('headers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(['headers', 'headers_translations']);
    }
}
