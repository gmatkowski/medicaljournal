<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStats extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stats', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('icon');
            $table->timestamps();
        });

        Schema::create('stat_translations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('stat_id')->unsigned();
            $table->string('name');
            $table->string('value');
            $table->string('locale')->index();
            $table->unique(['stat_id', 'locale']);
            $table->foreign('stat_id')->references('id')->on('stats')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(['stats', 'stat_translations']);
    }
}
