<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsMigration extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('code');
            $table->string('icon');
            $table->tinyInteger('level');
            $table->mediumInteger('sequence')->default(1);
            $table->timestamps();
        });

        Schema::create('lesson_translations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('lesson_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->string('locale')->index();
            $table->unique(['lesson_id', 'locale']);
            $table->foreign('lesson_id')->references('id')->on('lessons')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(['lessons', 'lesson_translations']);
    }
}
