<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExternalClientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('external_clients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('token');
            $table->timestamps();
        });

        Schema::table('orders', function (Blueprint $table) {
            $table->integer('ext_client_id')->nullable()->unsigned()->after('contact_id');
            $table->foreign('ext_client_id')->references('id')->on('external_clients')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropForeign('ext_client_id');
            $table->dropColumn(['ext_client_id']);
        });

        Schema::drop('external_clients');
    }
}
