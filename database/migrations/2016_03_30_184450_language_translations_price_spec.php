<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LanguageTranslationsPriceSpec extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('language_translations', function (Blueprint $table) {
            $table->double('price_spec')->after('price_old')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('language_translations', function (Blueprint $table) {
            $table->dropColumn(['price_spec']);
        });
    }
}
