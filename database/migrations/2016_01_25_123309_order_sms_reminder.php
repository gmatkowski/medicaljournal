<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderSmsReminder extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_reminders', function (Blueprint $table)
        {
            $table->string('type')->default('mail')->after('order_id');
            $table->index(['order_id', 'type']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_reminders', function (Blueprint $table)
        {
            $table->dropColumn(['type']);
        });
    }
}
