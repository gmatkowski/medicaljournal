<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetttingsTreshold extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('settings', function (Blueprint $table)
        {
            $table->renameColumn('available_treshold', 'available_treshold_from');
            $table->smallInteger('available_treshold_to')->after('available_treshold')->default(100);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('settings', function (Blueprint $table)
        {
            $table->renameColumn('available_treshold_from', 'available_treshold');
            $table->dropColumn(['available_treshold_to']);
        });
    }
}
