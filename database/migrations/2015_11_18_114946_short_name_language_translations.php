<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ShortNameLanguageTranslations extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('language_translations', function (Blueprint $table)
        {
            $table->string('name_short')->nullable()->after('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('language_translations', function (Blueprint $table)
        {
            $table->dropColumn(['name_short']);
        });
    }
}
