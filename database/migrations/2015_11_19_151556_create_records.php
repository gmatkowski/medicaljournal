<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecords extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('records', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('language_id')->unsigned()->nullable();
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('record_translations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('record_id')->unsigned();
            $table->string('name');
            $table->string('record');
            $table->string('locale')->index();
            $table->unique(['record_id', 'locale']);
            $table->foreign('record_id')->references('id')->on('records')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(['records', 'record_translations']);
    }
}
