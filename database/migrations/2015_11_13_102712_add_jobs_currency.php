<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJobsCurrency extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('job_translations', function (Blueprint $table) {
            $table->string('currency')->default('pln')->after('price_old');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('job_translations', function (Blueprint $table) {
            $table->dropColumn(['currency']);
        });
    }
}
