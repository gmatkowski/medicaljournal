<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOpinionsMigration extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opinions', function (Blueprint $table)
        {
            $table->increments('id');
            $table->string('code');
            $table->timestamps();
        });

        Schema::create('opinion_translations', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('opinion_id')->unsigned();
            $table->string('name');
            $table->text('description');
            $table->string('locale')->index();
            $table->unique(['opinion_id', 'locale']);
            $table->foreign('opinion_id')->references('id')->on('opinions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop(['opinions', 'opinion_translations']);
    }
}
