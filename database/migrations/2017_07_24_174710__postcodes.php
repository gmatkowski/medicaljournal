<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Postcodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postcodes', function (Blueprint $table)
        {
            $table->increments('id');
            $table->integer('name');
            $table->boolean('jayon')->default(0);
            $table->boolean('ninja')->default(0);
            $table->boolean('rpx')->default(0);
            $table->boolean('asp')->default(0);
            $table->boolean('sap')->default(0);
            $table->timestamps();
        });

        Schema::table('subdistricts', function (Blueprint $table) {
            $table->integer('post_code_id')->nullable()->unsigned();
            $table->foreign('post_code_id')->references('id')->on('postcodes')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('subdistricts', function (Blueprint $table) {
            $table->dropColumn(['post_code_id']);
        });
        Schema::drop('postcodes');
    }
}
