<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderAgeSexNotesOtherLang extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('age')->nullable()->after('phone');
            $table->string('sex')->nullable()->after('age');
            $table->string('notes')->nullable()->after('sex');
            $table->string('other_lang')->nullable()->after('notes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['age']);
            $table->dropColumn(['sex']);
            $table->dropColumn(['notes']);
            $table->dropColumn(['other_lang']);
        });
    }
}
