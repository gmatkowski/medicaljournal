<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LanguageFilePartNumber extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('language_files', function (Blueprint $table)
        {
            $table->smallInteger('part')->default(1)->after('file');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('language_files', function (Blueprint $table)
        {
            $table->dropColumn('part');
        });
    }
}
