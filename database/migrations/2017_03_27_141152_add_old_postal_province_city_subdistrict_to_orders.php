<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOldPostalProvinceCitySubdistrictToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->string('old_postalcode')->nullable()->after('phone');
            $table->string('old_province')->nullable()->after('old_address');
            $table->string('old_city')->nullable()->after('old_address');
            $table->string('old_subdistrict')->nullable()->after('old_district');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn(['old_postalcode']);
            $table->dropColumn(['old_province']);
            $table->dropColumn(['old_city']);
            $table->dropColumn(['old_subdistrict']);
        });
    }
}
