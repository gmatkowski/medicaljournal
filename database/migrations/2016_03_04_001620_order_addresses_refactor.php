<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrderAddressesRefactor extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_addresses', function (Blueprint $table)
        {
            $table->dropForeign('order_adresses_subdistrict_id_foreign');
            $table->dropColumn(['subdistrict_id']);
            $table->string('subdistrict')->nullable()->after('address');
            $table->integer('city_id')->nullable()->unsigned()->after('address');
            $table->foreign('city_id')->references('id')->on('cities')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_addresses', function (Blueprint $table)
        {
            $table->dropForeign('order_addresses_city_id_foreign');
            $table->dropColumn(['city_id', 'subdistrict']);
            $table->integer('district_id')->nullable()->unsigned()->after('address');
            $table->foreign('district_id')->references('id')->on('districts')->onDelete('cascade');
        });
    }
}
