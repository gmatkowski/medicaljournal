<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
            $table->increments('id');
			$table->string('icon');
			$table->string('author');
			$table->integer('views')->default(0);
            $table->timestamps();
		});

		Schema::create('post_translations', function (Blueprint $table)
		{
			$table->increments('id');
			$table->integer('post_id')->unsigned();
			$table->string('title');
			$table->text('article');
			$table->string('locale')->index();
			$table->unique(['post_id', 'locale']);
			$table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('posts');
	}

}
