<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;


class UpdateAddressSeeder extends Seeder {


    protected $country;


    protected $countries;


    protected $provinces;


    protected $cities;


    protected $districts;



    public function __construct(\App\Repositories\CountryRepository $countryRepository, \App\Repositories\ProvinceRepository $provinceRepository, \App\Repositories\DistrictRepository $districtRepository, \App\Repositories\CityRepository $cityRepository)
    {
        $this->countries = $countryRepository;
        $this->provinces = $provinceRepository;
        $this->districts = $districtRepository;
        $this->cities = $cityRepository;
    }


    public function run()
    {
        try
        {
            /*DB::table('countries')->delete();
            DB::unprepared('ALTER TABLE countries AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE provinces AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE cities AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE districts AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE subdistricts AUTO_INCREMENT = 1');*/

            $this->country = $this->countries->findByField('name', 'Indonesia')->first();

            DB::transaction(function ()
            {
                $file_path = storage_path('app/update.xls');

                Excel::load($file_path, function ($reader)
                {
                    $results = $reader->all();
                    $results->each(function ($sheet)
                    {
                        $object = $sheet->all();
                        $province = $this->insertOrGetProvince($this->country, $object['province']);
                        if ($province)
                        {
                            $city = $this->insertOrGetCity($province, $object['city']);
                            if ($city)
                            {

                                $this->insertOrGetDistrict($city, $object['district']);
                            }
                        }


                    });

                });
            });

            return true;

        } catch (\Exception $e)
        {
            echo $e->getMessage();

            return false;
        }
    }


    protected function insertOrGetCity(\App\Province $province, $name)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $this->cities->findByField('name', $name);

        if ($result->count() > 0)
        {
            $object = $result->first();

            return $object;
        }
        else
        {
            $city = $this->cities->create([
                'name'        => ucfirst(strtolower($name)),
                'province_id' => $province->id
            ]);

            return $city;
        }
    }


    protected function insertOrGetDistrict(\App\City $city, $name)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $this->districts->findByField('name', $name);

        if ($result->count() > 0)
        {
            return $result->first();
        }
        else
        {
            $district = $this->districts->create([
                'name'    => ucfirst(strtolower($name)),
                'city_id' => $city->id
            ]);

            return $district;
        }
    }


    protected function insertOrGetProvince(\App\Country $country, $name)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $this->provinces->findByField('name', $name);

        if ($result->count() > 0)
        {
            return $result->first();
        }
        else
        {
            $province = $this->provinces->create([
                'name'       => ucfirst(strtolower($name)),
                'country_id' => $country->id
            ]);

            return $province;
        }
    }
}
