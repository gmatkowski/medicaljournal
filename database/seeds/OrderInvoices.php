<?php

use Illuminate\Database\Seeder;

class OrderInvoices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('order_invoices')->insert([
            'invoice_no' => 120,
            'credit_no' => 147
        ]);
    }
}
