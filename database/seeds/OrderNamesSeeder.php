<?php

use Illuminate\Database\Seeder;

class OrderNamesSeeder extends Seeder {

    protected $orders;

    public function __construct(\App\Repositories\OrderRepository $orderRepository)
    {
        $this->orders = $orderRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->orders->all() as $order)
        {
            $order->first_name = $order->first_name;
            $order->last_name = $order->last_name;
            $order->save();
        }
    }
}
