<?php

use Illuminate\Database\Seeder;

/**
 * Class RolesSeeder
 */
class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * retrieve current user - role associations
         *
         * */
        $results = DB::table('users')
            ->join('role_user', 'users.id', '=', 'role_user.user_id')
            ->join('roles', 'role_user.role_id', '=', 'roles.id')
            ->select('users.id', 'roles.name')
            ->get();
        $usersWithRoles = [];
        foreach ($results as $result) {
            $usersWithRoles[$result->id] = $result->name;
        }

        /*
         * drop tables and reset indexes
         *
         * */
        DB::table('roles')->delete();
        DB::table('permissions')->delete();
        DB::table('permission_role')->delete();
        DB::table('role_user')->delete();
        DB::unprepared('ALTER TABLE roles AUTO_INCREMENT = 1');
        DB::unprepared('ALTER TABLE permissions AUTO_INCREMENT = 1');
        DB::unprepared('ALTER TABLE permission_role AUTO_INCREMENT = 1');
        DB::unprepared('ALTER TABLE role_user AUTO_INCREMENT = 1');

        /*
         * create roles
         *
         * */
        $consultant = new \App\Role();
        $consultant->name = 'consultant';
        $consultant->display_name = 'Consultant';
        $consultant->save();

        $admin = new \App\Role();
        $admin->name = 'admin';
        $admin->display_name = 'Administrator';
        $admin->save();

        $css = new \App\Role();
        $css->name = 'css';
        $css->display_name = 'Customer Service Specialist';
        $css->save();

        $ccadmin = new \App\Role();
        $ccadmin->name = 'ccadmin';
        $ccadmin->display_name = 'Call Center Administrator';
        $ccadmin->save();

        $headcc = new \App\Role();
        $headcc->name = 'headcc';
        $headcc->display_name = 'Head Call Center';
        $headcc->save();

        $editor = new \App\Role();
        $editor->name = 'editor';
        $editor->display_name = 'Editor';
        $editor->save();

        /*
         * create permissions
         *
         * */
        $signIn = new \App\Permission();
        $signIn->name = 'sing-in';
        $signIn->display_name = 'Permission to sing in';
        $signIn->save();

        $delCont = new \App\Permission();
        $delCont->name = 'delete-contacts';
        $delCont->display_name = 'Permission to delete contacts';
        $delCont->save();

        $lisCont = new \App\Permission();
        $lisCont->name = 'list-contacts';
        $lisCont->display_name = 'Permission to list contacts';
        $lisCont->save();

        $ExpCon = new \App\Permission();
        $ExpCon->name = 'export-contacts';
        $ExpCon->display_name = 'Permission to export contacts';
        $ExpCon->save();

        $ChgOrSt = new \App\Permission();
        $ChgOrSt->name = 'change-order-status';
        $ChgOrSt->display_name = 'Permission to change order status';
        $ChgOrSt->save();

        $ExpOrd = new \App\Permission();
        $ExpOrd->name = 'export-orders';
        $ExpOrd->display_name = 'Permission to export orders';
        $ExpOrd->save();

        $CreOrd = new \App\Permission();
        $CreOrd->name = 'create-order';
        $CreOrd->display_name = 'Permission to create an order';
        $CreOrd->save();

        $EdiOrd = new \App\Permission();
        $EdiOrd->name = 'edit-order';
        $EdiOrd->display_name = 'Permission to edit an order';
        $EdiOrd->save();

        $lAllOrd = new \App\Permission();
        $lAllOrd->name = 'list-all-orders';
        $lAllOrd->display_name = 'Permission to list all orders';
        $lAllOrd->save();

        $lCodOrd = new \App\Permission();
        $lCodOrd->name = 'list-cod-orders';
        $lCodOrd->display_name = 'Permission to list COD orders';
        $lCodOrd->save();

        $lCodBOr = new \App\Permission();
        $lCodBOr->name = 'list-cod-by-css-orders';
        $lCodBOr->display_name = 'Permission to list COD orders created by css or consultant';
        $lCodBOr->save();

        $lOwnOrd = new \App\Permission();
        $lOwnOrd->name = 'list-own-orders';
        $lOwnOrd->display_name = 'Permission to list own orders';
        $lOwnOrd->save();

        $DelOrd = new \App\Permission();
        $DelOrd->name = 'delete-order';
        $DelOrd->display_name = 'Permission to delete orders';
        $DelOrd->save();

        $SendOr = new \App\Permission();
        $SendOr->name = 'send-order';
        $SendOr->display_name = 'Permission to send orders';
        $SendOr->save();

        $MaSOr = new \App\Permission();
        $MaSOr->name = 'mark-order-as-sent';
        $MaSOr->display_name = 'Permission to mark orders as sent';
        $MaSOr->save();

        $FilOr = new \App\Permission();
        $FilOr->name = 'filter-orders';
        $FilOr->display_name = 'Permission to filter order list';
        $FilOr->save();

        $CrePos = new \App\Permission();
        $CrePos->name = 'create-posts';
        $CrePos->display_name = 'Permission to create blog posts';
        $CrePos->save();

        $EdiPos = new \App\Permission();
        $EdiPos->name = 'edit-posts';
        $EdiPos->display_name = 'Permission to edit blog posts';
        $EdiPos->save();

        $DelPos = new \App\Permission();
        $DelPos->name = 'delete-posts';
        $DelPos->display_name = 'Permission to delete blog posts';
        $DelPos->save();

        $ManCon = new \App\Permission();
        $ManCon->name = 'manage-content';
        $ManCon->display_name = 'Permission to add, modify, delete page content';
        $ManCon->save();

        $ManUsr = new \App\Permission();
        $ManUsr->name = 'manage-users';
        $ManUsr->display_name = 'Permission to add, modify, delete page users';
        $ManUsr->save();

        /*
         * attach permissions to roles
         *
         * */
        $consultant->attachPermission($signIn);
        $consultant->attachPermission($CreOrd);
        $consultant->attachPermission($lOwnOrd);
        $consultant->attachPermission($lCodOrd);

        $css->attachPermission($signIn);
        $css->attachPermission($CreOrd);
        $css->attachPermission($lisCont);
        $css->attachPermission($MaSOr);
        $css->attachPermission($lAllOrd);
        $css->attachPermission($FilOr);
        $css->attachPermission($EdiOrd);
        $css->attachPermission($lCodOrd);
        $css->attachPermission($DelOrd);
        $css->attachPermission($SendOr);
        $css->attachPermission($ExpOrd);

        $headcc->attachPermission($signIn);
        $headcc->attachPermission($lAllOrd);
        $headcc->attachPermission($SendOr);
        $headcc->attachPermission($CreOrd);
        $headcc->attachPermission($lisCont);
        $headcc->attachPermission($MaSOr);
        $headcc->attachPermission($lCodOrd);
        $headcc->attachPermission($DelOrd);
        $headcc->attachPermission($EdiOrd);
        $headcc->attachPermission($ManUsr);

        $ccadmin->attachPermission($signIn);
        $ccadmin->attachPermission($lAllOrd);
        $ccadmin->attachPermission($SendOr);
        $ccadmin->attachPermission($CreOrd);
        $ccadmin->attachPermission($lisCont);
        $ccadmin->attachPermission($MaSOr);
        $ccadmin->attachPermission($ExpOrd);
        $ccadmin->attachPermission($lCodOrd);
        $ccadmin->attachPermission($DelOrd);
        $ccadmin->attachPermission($EdiOrd);
        $ccadmin->attachPermission($ManUsr);

        $editor->attachPermission($signIn);
        $editor->attachPermission($CrePos);
        $editor->attachPermission($EdiPos);
        $editor->attachPermission($DelPos);
        $editor->attachPermission($ManCon);

        /*
         * reassign roles to users
         *
         * */
        foreach (\App\User::all() as $user) {
            $user->attachRole(\App\Role::where('name',$usersWithRoles[$user->id])->first());
            $user->save();
        }
    }
}
