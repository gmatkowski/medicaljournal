<?php

use Illuminate\Database\Seeder;

class HeaderSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Header::create([
            'text' => 'Mulai Berbicara <strong>Bahasa Apapun Dengan Cepat <span style="color: rgb(237, 65, 65); ">dalam 30 Hari</span></strong>'
        ]);
    }
}
