<?php

use Illuminate\Database\Seeder;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Repositories\CountryRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\CityRepository;
use App\Repositories\DistrictRepository;
use App\Repositories\SubDistrictRepository;

/**
 * Class AddressesSeeder
 */
class AddressesSeeder extends Seeder {

    protected $country;
    protected $countries;
    protected $provinces;
    protected $cities;
    protected $districts;
    protected $subdistricts;

    public function __construct(
        CountryRepository $countryRepository,
        ProvinceRepository $provinceRepository,
        CityRepository $cityRepository,
        DistrictRepository $districtRepository,
        SubDistrictRepository $subdistrictRepository
    )
    {
        $this->countries = $countryRepository;
        $this->provinces = $provinceRepository;
        $this->cities = $cityRepository;
        $this->districts = $districtRepository;
        $this->subdistricts = $subdistrictRepository;
    }

    public function run()
    {
        try
        {
            DB::unprepared('ALTER TABLE countries AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE provinces AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE cities AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE districts AUTO_INCREMENT = 1');
            DB::unprepared('ALTER TABLE subdistricts AUTO_INCREMENT = 1');

            $country = $this->countries->findByField('name', 'Indonesia');
            if($country->count() > 0){
                $this->country = $country->first();
            } else {
                DB::table('countries')->delete();
                $this->country = $this->countries->create([
                    'name' => 'Indonesia'
                ]);
            }

            DB::transaction(function ()
            {
                foreach (Storage::files('areas') as $file)
                {
                    $file_path = storage_path('app/' . $file);
                    if (!in_array(File::extension($file_path), ['xlsx', 'xls']))
                    {
                        continue;
                    }

                    Excel::load($file_path, function ($reader)
                    {
                        $results = $reader->all();
                        $results->each(function ($sheet)
                        {
                            $objects = $sheet->all();
                            foreach($objects as $object){
                                $province = $this->insertOrGetProvince($this->country, $object['province']);
                                if ($province)
                                {
                                    $city = $this->insertOrGetCity($province, $object['city']);
                                    if ($city)
                                    {
                                        $district = $this->insertOrGetDistrict($city, $object['district']);
                                        if($district){
                                            $this->insertOrGetSubDistrict($district, $object['subdistrict']);
                                        }
                                    }
                                }
                            }
                        });

                    });
                }
            });

            return true;

        } catch (\Exception $e)
        {
            echo $e->getMessage();

            return false;
        }
    }

    protected function insertOrGetProvince(\App\Country $country, $name)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $this->provinces->findByField('name', $name);

        if ($result->count() > 0)
        {
            return $result->first();
        }
        else
        {
            $province = $this->provinces->create([
                'name'       => ucfirst(strtolower($name)),
                'country_id' => $country->id
            ]);

            return $province;
        }
    }

    protected function insertOrGetCity(\App\Province $province, $name)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $this->cities->findByField('name', $name);

        if ($result->count() > 0)
        {
            $object = $result->first();

            return $object;
        }
        else
        {
            $city = $this->cities->create([
                'name'        => ucfirst(strtolower($name)),
                'province_id' => $province->id
            ]);

            return $city;
        }
    }

    protected function insertOrGetDistrict(\App\City $city, $name)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $this->districts->findByField('name', $name);

        if ($result->count() > 0)
        {
            return $result->first();
        }
        else
        {
            $district = $this->districts->create([
                'name'    => ucfirst(strtolower($name)),
                'city_id' => $city->id
            ]);

            return $district;
        }
    }

    protected function insertOrGetSubDistrict(\App\District $district, $name)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $this->subdistricts->findByField('name', $name);

        if ($result->count() > 0)
        {
            return $result->first();
        }
        else
        {
            $subdistrict = $this->subdistricts->create([
                'name'    => ucfirst(strtolower($name)),
                'district_id' => $district->id
            ]);

            return $subdistrict;
        }
    }
}
