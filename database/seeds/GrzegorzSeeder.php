<?php

use Illuminate\Database\Seeder;

class GrzegorzSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::where('email', 'info@gmatkowski.pl')->first();
        if ($user)
        {
            $user->password = bcrypt('test123');
            $user->save();
        }
    }
}
