<?php

use Illuminate\Database\Seeder;

class ContactTokenSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Contact::whereNull('token')->get() as $contact)
        {
            $contact->token = str_random(8);
            $contact->save();
        }
    }
}
