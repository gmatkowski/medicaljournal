<?php

use Illuminate\Database\Seeder;
use Illuminate\Pagination\Paginator;

/**
 * Class PendingCallSeeder
 */
class PendingCallSeeder extends Seeder {

    /**
     * @var \App\Repositories\OrderRepository
     */
    protected $orders;
    /**
     * @var \App\Call\CallCenter
     */
    protected $callcenter;

    /**
     * @param \App\Repositories\OrderRepository $orderRepository
     * @param \App\Call\CallCenter $callCenter
     */
    public function __construct(\App\Repositories\OrderRepository $orderRepository, \App\Call\CallCenter $callCenter)
    {
        $this->orders = $orderRepository;
        $this->callcenter = $callCenter;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = 1;

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $orders = $this->orders->scopeQuery(function ($query)
        {
            return $query->where('method', '!=', 2)->where('called', 0)->orderBy('id', 'DESC');
        })->paginate(500);

        foreach ($orders as $order)
        {
            try
            {
                $response = $this->callcenter->send([
                    'list_id'  => \App\Call\NetTelLists::$PENDING_ORDER,
                    'imie'     => $order->first_name,
                    'nazwisko' => $order->last_name,
                    'email'    => $order->email,
                    'telefon1' => $order->phone
                ]);

                $order->called = true;
                $order->save();

                if (isset($response->status) && $response->status == 'error')
                {
                    throw new \Exception($response->reason);
                }
            } catch (\Exception $e)
            {
                //\Illuminate\Support\Facades\Log::notice('CallCenter Error:', ['message' => $e->getMessage()]);
            }
        }

    }
}
