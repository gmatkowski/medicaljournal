<?php

use Illuminate\Database\Seeder;

class NewRoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $viewer = new \App\Role();
        $viewer->name = 'viewer';
        $viewer->display_name = 'Viewer';
        $viewer->save();

        $permissionRepository = app('App\Repositories\PermissionRepository');

        $signIn = $permissionRepository->findWhere([['name', '=', 'sing-in']])->first();
        $ExpOrd = $permissionRepository->findWhere([['name', '=', 'export-orders']])->first();
        $lAllOrd = $permissionRepository->findWhere([['name', '=', 'list-all-orders']])->first();
        $lCodOrd = $permissionRepository->findWhere([['name', '=', 'list-cod-orders']])->first();
        $lCodBOr = $permissionRepository->findWhere([['name', '=', 'list-cod-by-css-orders']])->first();
        $lOwnOrd = $permissionRepository->findWhere([['name', '=', 'list-own-orders']])->first();
        $FilOr = $permissionRepository->findWhere([['name', '=', 'filter-orders']])->first();

        $viewer->attachPermission($signIn);
        $viewer->attachPermission($ExpOrd);
        $viewer->attachPermission($lAllOrd);
        $viewer->attachPermission($lCodOrd);
        $viewer->attachPermission($lCodBOr);
        $viewer->attachPermission($lOwnOrd);
        $viewer->attachPermission($FilOr);
    }
}
