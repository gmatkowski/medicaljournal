<?php

use Illuminate\Database\Seeder;

class ContactCallSeeder extends Seeder {

    protected $callCenter;
    protected $contacts;

    public function __construct(\App\Call\CallCenter $callCenter, \App\Repositories\ContactRepository $contactRepository)
    {
        $this->callCenter = $callCenter;
        $this->contacts = $contactRepository;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contacts = $this->contacts->scopeQuery(function ($query)
        {
            return $query->where('called', 0)->orderBy('id', 'DESC');
        })->paginate(500);

        foreach ($contacts as $contact)
        {
            try
            {
                $response = $this->callCenter->send([
                    'list_id'  => $contact->getCallCenterList(),
                    'imie'     => $contact->first_name,
                    'nazwisko' => $contact->last_name,
                    'email'    => $contact->email,
                    'telefon1' => $contact->phone
                ]);

                $contact->called = 1;
                $contact->save();

                if (isset($response->status) && $response->status == 'error')
                {
                    throw new \Exception($response->reason);
                }

            } catch (\Exception $e)
            {
                //\Illuminate\Support\Facades\Log::notice('CallCenter Error:', ['message' => $e->getMessage()]);
            }
        }
    }
}
