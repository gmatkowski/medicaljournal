<?php

use Illuminate\Database\Seeder;

class SettingsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Setting::create([
            'available_free'        => 5,
            'available_gone'        => 26,
            'available_treshold'    => 30,
            'available_treshold_to' => 120
        ]);
    }
}
