<?php

use Illuminate\Database\Seeder;

class OrderMethodRefactoringSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (\App\Order::all() as $order)
        {
            $order->method = $this->getNewMethod($order);
            $order->save();
        }
    }

    private function getNewMethod($order)
    {
        switch ($order->method2)
        {
            case 'cod':
                return 2;
                break;
            case 'transfer':
                return 1;
                break;
            case 'cc':
                return 3;
                break;
        }
    }
}
