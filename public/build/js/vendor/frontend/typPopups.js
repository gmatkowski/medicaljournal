var Popups = {
    listeners: function () {
        $('[data-action="show-popup-form"]').on('click', function (e) {
            e.preventDefault();

            $('[data-container="popup-form" ]').slideToggle();
        })
    },
    init: function () {
        this.listeners();
    }
}

$(function () {
    Popups.init();
})

window.Popups = Popups;