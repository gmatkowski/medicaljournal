(function($) {
    "use strict";
    jQuery(document).on('ready', function(){
        // back to top
        $('#back-to-top').fadeOut(duration);
        var offset = 220;
        var duration = 1000;
        $(window).on('scroll', function() {
            if ($(this).scrollTop() > offset) {
                $('.back-to-top').fadeIn(duration);
            } else {
                $('.back-to-top').fadeOut(duration);
            }
        });

        $('#back-to-top').on('click', function(event) {
            event.preventDefault();
            $('html, body').animate({scrollTop: 0}, duration);
            return false;
        })
    });

})(jQuery);