<?php namespace App\Sms;

/**
 * Interface SmsProvider
 * @package App\Sms
 */
interface SmsProvider {

    /**
     * @param $recipient
     * @param $message
     * @return mixed
     */
    static public function send($recipient, $message);
}