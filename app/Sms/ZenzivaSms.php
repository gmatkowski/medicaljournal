<?php namespace App\Sms;

use App\Order;
use App\Contact;
use App\Helpers\StrHelper;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

/**
 * Class Zenziva
 * @package App\Sms
 */
class ZenzivaSms implements SmsProvider {

    static private $recipient;
    static private $type;

    static private function workingDays($product)
    {
        $now = Carbon::now();

        $cc = app('App\Call\Focus');
        $campaignId = $cc->getCampaignId($product);

        switch ($campaignId) {
            case 71:
                // 10:30 - 19:50
                $startingTime = Carbon::now()->startOfDay()->addHours(10)->addMinutes(30);
                $endingTime = Carbon::now()->startOfDay()->addHours(19)->addMinutes(50);
                break;
            case 72:
                // 8:00 - 17:50
                $startingTime = Carbon::now()->startOfDay()->addHours(8);
                $endingTime = Carbon::now()->startOfDay()->addHours(17)->addMinutes(50);
                break;
            default:
                $startingTime = Carbon::now()->endOfDay();
                $endingTime = Carbon::now()->startOfDay();
        }

        return $now->isWeekday() && $now->gt($startingTime) && $now->lt($endingTime);
    }

    static public function smsAlpha(Contact $contact)
    {
        if(self::workingDays($contact->product))
        {
            $message = self::getHelloSms($contact->phone);
            if($message)
            {
                self::$type = 'Alpha';
                self::prepareToSend($contact->phone, $message);
            }
        }
    }

    static public function smsConfirm(Order $order)
    {
        $message = self::getPendingSms($order->price);
        if($message)
        {
            self::$type = 'Confirm';
            self::prepareToSend($order->phone, $message);
        }
    }

    static public function smsDelivery(Order $order)
    {
        $message = self::getDeliverySms($order);
        if($message)
        {
            self::$type = 'Delivery';
            self::prepareToSend($order->phone, $message);
        }
    }

    private static function prepareToSend($phone, $message)
    {
        $phone = StrHelper::validatePhone($phone);
        if($phone){
            $recipient='62'.ltrim($phone, '0');
            self::send($recipient, $message);
        }
    }

    static public function send($recipient, $message)
    {
        if(app()->environment() == 'production')
        {
            $ch = curl_init();

            curl_setopt_array($ch, array(
                    CURLOPT_URL            => 'https://reguler.zenziva.net/apps/smsapi.php',
                    CURLOPT_POSTFIELDS     => 'userkey=rhan9c&passkey=Digital321&nohp='.$recipient.'&pesan='.urlencode($message),
                    CURLOPT_HEADER         => 0,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_SSL_VERIFYHOST => 2,
                    CURLOPT_SSL_VERIFYPEER => 0,
                    CURLOPT_TIMEOUT        => 30,
                    CURLOPT_POST           => 1,
                )
            );

            $status = (string)simplexml_load_string(curl_exec($ch))->message->status;

            self::$recipient = $recipient;
            self::checkStatus($status);

            curl_close($ch);
        }
    }

    static private function checkStatus($status)
    {
        switch ($status) {
            case 0:
                Log::info('Sending '.self::$type.' SMS to: '.self::$recipient.' :: success');
                break;
            case 5:
                Log::info('Sending '.self::$type.' SMS to: '.self::$recipient.' :: failed, Invalid UserKey / PassKey');
                break;
            case 99:
                Log::info('Sending '.self::$type.' SMS to: '.self::$recipient.' :: failed, NO Credits');
                break;
        }
    }

    private static function getHelloSms($phone)
    {
        return 'Hallo, Aplikasi anda nomor '.$phone.' sudah kami terima. Konsultan kami akan segera menghubungi anda. Trimakasih.';
    }

    private static function getPendingSms($totalPrice)
    {
        return 'Pesanan anda sudah terkonfirmasi, total harga pesanan anda adalah Rp '.$totalPrice.' Trimakasih';
    }

    private static function getDeliverySms(Order $order)
    {
        switch ($order->address->subDistrict->postcode->shipping_provider_name) {
            case 'JAYON':
                $deliveryNumber = 'transaction id: '.$order->trx_jayon.', delivery id: '.$order->did_jayon;
                break;
            case 'NINJA':
                $deliveryNumber = 'transaction id: '.$order->id_ninja.', tracking id: '.$order->id_tracking_ninja;
                break;
            case 'RPX':
                $deliveryNumber = 'package number: '.$order->package_number;
                break;
        }
        if(isset($deliveryNumber))
        {
            return 'Pesanan anda telah terkirim. Nomor pesanannya adalah '.$deliveryNumber.'. Paket bisa diambil tanpa menunggu pemberitahuan.';
        }
        return false;
    }
}