<?php namespace App\Sms;

use Illuminate\Support\Facades\Log;

/**
 * Class DumpSms
 * @package App\Sms
 */
Class DumpSms implements SmsProvider {

    /**
     * @param $recipient
     * @param $message
     * @return bool
     */
    public static function send($recipient, $message)
    {
        return false;
        Log::info('Sending SMS to: '.$recipient . ' :: ' . $message);

        return true;
    }

}