<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @package App
 */
class Stock extends Model {

    /**
     * @var string
     */
    public $table = 'stocks';

    /**
     * @var array
     */
    public $fillable = ['open', 'in', 'out', 'close', 'language_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id');
    }

}
