<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslation
 * @package App
 */
class JobTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'job_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['name', 'price_new', 'price_old', 'currency'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo('App\Job');
    }

}
