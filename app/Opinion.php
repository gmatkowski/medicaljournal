<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Opinion extends Model implements Transformable {

    use Translatable, TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'opinions';

    /**
     * @var array
     */
    protected $translatedAttributes = ['name', 'description', 'icon'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'main', 'icon'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated'];

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    /**
     * @return string
     */
    public function getIconPathAttribute()
    {
        return asset('opinions/icons/' . $this->icon);
    }

}
