<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Province
 * @package App
 */
class Province extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'provinces';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'country_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany('App\City');
    }

    public function delete()
    {
        return;
    }
}
