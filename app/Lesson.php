<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

/**
 * Class Lesson
 * @package App
 */
class Lesson extends Model implements Transformable {

    use Translatable, TransformableTrait;

    /**
     * @var array
     */
    protected $levels = [];

    /**
     * @var string
     */
    protected $table = 'lessons';

    /**
     * @var array
     */
    protected $translatedAttributes = ['name', 'description'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'icon', 'level'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated', 'icon_path', 'level_name', 'level_class_name'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->levels = trans('lessons.levels');

        parent::__construct($attributes);
    }

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    /**
     * @return string
     */
    public function getIconPathAttribute()
    {
        return asset('lessons/icons/' . $this->icon);
    }

    /**
     * @return null
     */
    public function getLevelNameAttribute()
    {
        $phrase = trans('lessons.levels');

        return isset($phrase[$this->level]) ? $phrase[$this->level] : null;
    }

    /**
     * @return string
     */
    public function getLevelClassNameAttribute()
    {
        switch ($this->level)
        {
            case 1:
                return 'basic';
                break;
            case 2:
                return 'intermediate';
                break;
            case 3:
                return 'advanced';
                break;
        }
    }
}
