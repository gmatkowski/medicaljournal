<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslation
 * @package App
 */
class StatTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'stat_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['name', 'value'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function stat()
    {
        return $this->belongsTo('App\Stat');
    }

}
