<?php

namespace App;

use Exception;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

/**
 * Class Order
 * @package App
 */
class Order extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var array
     */
    public $statuses = [
        0 => 'Pending',
        1 => 'Paid',
        2 => 'Sent',
        3 => 'Downloaded',
        4 => 'Rejected',
        5 => 'To confirm',
        6 => 'Cancelled',
        7 => 'Delivered',
        8 => 'Returned to Sender'
    ];

    /**
     * @var array
     */
    public $versions = [
        1 => 'Elektroniczna',
        2 => 'Przesyłka fizyczna'
    ];

    /**
     * @var array
     */
    public $methods = [
        1 => 'Transfer',
        2 => 'COD',
        3 => 'Credit Card',
        4 => 'BCA',
        5 => 'Mandiri ClickPay',
        6 => 'Mandiri ATM',
        7 => 'Bank Transfer'
    ];

    /*
        public function __construct(array $attributes = [])
        {
            parent::__construct($attributes);
        }
    */
    /**
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'old_postalcode', 'old_address', 'old_province', 'old_city', 'old_district', 'old_subdistrict', 'old', 'age', 'sex', 'notes', 'comment', 'other_lang', 'to_confirm', 'price', 'method', 'calls', 'token', 'lang', 'nocod', 'version', 'token_id', 'done', 'first_name', 'last_name', 'trx_jayon', 'did_jayon', 'icc_comment', 'ext_client_id', 'id_ninja', 'id_tracking_ninja', 'unique_number', 'confirmation', 'size', 'package_number'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'is_done'    => 'boolean',
        'shipped'    => 'boolean',
        'to_confirm' => 'boolean',
        'called'     => 'boolean',
        'duplicated' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $appends = ['languages_names', 'status_name', 'first_name', 'last_name', 'version_name', 'method_name', 'is_done', 'ref_or_id', 'summary_price', 'created_at_formated', 'sended_at_formated', 'payed_at_formated'];


    /**
     * @return mixed
     */
    public function getRefOrIdAttribute()
    {
        return 'CM'.$this->id;
//        return !is_null($this->ref) ? $this->ref : $this->id;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function languages()
    {
        return $this->hasMany('App\OrderLanguage')->with(['language']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reminders()
    {
        return $this->hasMany('App\OrderReminder');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany('App\OrderFile');
    }

    /**
     * @return mixed
     */
    public function address()
    {
        return $this->hasOne('App\OrderAddress')->with(['district']);
    }

    public function invoice()
    {
        return $this->hasOne('App\OrderInvoice', 'order_id');
    }

    public function externalClient()
    {
        return $this->belongsTo('App\ExternalClient', 'ext_client_id');
    }

    /**
     * @return mixed
     */
    public function getLanguagesNamesAttribute()
    {
        $results = collect();
        foreach ($this->languages as $lang)
        {
            $results->push($lang->language->name);
        }

        return implode(', ', $results->toArray());
    }

    /**
     * @return mixed
     */
    public function getStatusNameAttribute()
    {
        return isset($this->statuses[$this->status]) ? $this->statuses[$this->status] : null;
    }

    /**
     * @return mixed
     */
    public function getVersionNameAttribute()
    {
        return isset($this->versions[$this->version]) ? $this->versions[$this->version] : null;
    }

    /**
     * @return mixed
     */
    public function getMethodNameAttribute()
    {
        return isset($this->methods[$this->method]) ? $this->methods[$this->method] : null;
    }

    /**
     * @return array
     */
    public function getStatuses()
    {
        return $this->statuses;
    }

    /**
     * @return array
     */
    public function getMethods()
    {
        return $this->methods;
    }

    /**
     * @return mixed
     */
    public function getFirstNameAttribute()
    {
        if (!empty($this->getOriginal('first_name')))
        {
            return $this->getOriginal('first_name');
        }

        $name = explode(' ', $this->name);

        return isset($name[0]) ? $name[0] : '';
    }

    /**
     * @return mixed
     */
    public function getLastNameAttribute()
    {
        if (!empty($this->getOriginal('last_name')))
        {
            return $this->getOriginal('last_name');
        }

        $name = explode(' ', $this->name);
        array_shift($name);

        return implode(' ', $name);
    }

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    /**
     * @return static
     */
    public function getSendedAtFormatedAttribute()
    {
        return Carbon::parse($this->sended_at);
    }

    /**
     * @return static
     */
    public function getPayedAtFormatedAttribute()
    {
        return Carbon::parse($this->payed_at);
    }

    /**
     * @return mixed
     */
    public function getIsDoneAttribute()
    {
        return $this->done;
    }

    /**
     * @return bool
     */
    public function isCOD()
    {
        return $this->method == 2;
    }

    public function getSummaryPriceAttribute()
    {
        $sum = 0;
        foreach ($this->languages as $language)
        {
            $sum += $language->qty * $language->price;
        }

        return round(str_replace(',', '.', $sum));
    }

    public function hasProduct($languageId)
    {
        foreach ($this->languages as $language)
        {
            if($language->language_id == $languageId){
                return true;
            }
        }
        return false;
    }

    public function getLanguagePrice($languageId)
    {
        foreach ($this->languages as $language)
        {
            if($language->language_id == $languageId){
                return $language->price;
            }
        }
    }

    public function getQty($languageId)
    {
        foreach ($this->languages as $language)
        {
            if($language->language_id == $languageId){
                $languagePrice = $this->getLanguagePrice($languageId);
                switch ($languageId) {
                    case 1:
                        $qty = $languagePrice=='245000'? '2in1' : $language->qty;
                        break;
                    default:
                        $qty = $languagePrice=='225000'? '2in1' : $language->qty;
                        break;
                }
                return $qty;
            }
        }
        return 1;
    }

    public function getInvoicePrefix()
    {
        $prefix = collect();
        foreach ($this->languages as $language)
        {
            switch ($language->language_id) {
                case 1:
                    $prefix->push('GRCNA');
                    break;
                case 2:
                    $prefix->push('BLMSK');
                    break;
                case 3:
                    $prefix->push('DRHLX');
                    break;
                default:
                    $prefix->push('GRCNA');
                    break;
            }
        }
        return implode('/', $prefix->toArray());
    }

    public function isPriceDefault()
    {
        $products = count($this->languages);
        if($products==1){
            $language = $this->languages->first();
            if($language->qty==1){
                if($language->price!=$this->price){
                    return true;
                }
            }
        }
        return false;
    }

    public function getPostalCode()
    {
        if(isset($this->address) && isset($this->address->subDistrict) && isset($this->address->subDistrict->postcode)){
            return $this->address->subDistrict->postcode->name;
        }
    }

    public function getCourier()
    {
        if($this->method===7)
        {
            return 'RPX';
//            return $this->whichCourierForNonCod();
        }
        if(isset($this->address) && isset($this->address->subDistrict) && isset($this->address->subDistrict->postcode)){
            return $this->address->subDistrict->postcode->shipping_provider_name;
        }
        if(isset($this->address) && isset($this->address->district)){
            return $this->address->district->shipping_provider_name;
        }
        return 'Missing address';
    }

//    public function whichCourierForNonCod()
//    {
//        if(isset($this->address) && isset($this->address->subDistrict) && isset($this->address->subDistrict->postcode))
//        {
//            switch ($this->address->subDistrict->postcode->name) {
//                case 85111:
//                case 85112:
//                case 85116:
//                case 85118:
//                case 85119:
//                case 85141:
//                case 85142:
//                case 85143:
//                case 85144:
//                case 85146:
//                case 85147:
//                case 85148:
//                case 85221:
//                case 85223:
//                case 85225:
//                case 85226:
//                case 85227:
//                case 85228:
//                case 85229:
//                case 85231:
//                case 85232:
//                case 85233:
//                case 85234:
//                case 85235:
//                case 85236:
//                case 85237:
//                case 85238:
//                case 85239:
//                case 85351:
//                case 85353:
//                case 85361:
//                case 85362:
//                case 85363:
//                case 85364:
//                case 85365:
//                case 85367:
//                case 85368:
//                case 85369:
//                case 85391:
//                case 85392:
//                case 85393:
//                case 85511:
//                case 85514:
//                case 85515:
//                case 85516:
//                case 85517:
//                case 85519:
//                case 85551:
//                case 85552:
//                case 85561:
//                case 85562:
//                case 85563:
//                case 85571:
//                case 85572:
//                case 85573:
//                case 85574:
//                case 85575:
//                case 85611:
//                case 85612:
//                case 85613:
//                case 85614:
//                case 85615:
//                case 85616:
//                case 85617:
//                case 85651:
//                case 85661:
//                case 85671:
//                case 85681:
//                case 85682:
//                case 85711:
//                case 85712:
//                case 85713:
//                case 85714:
//                case 85715:
//                case 85716:
//                case 85717:
//                case 85718:
//                case 85752:
//                case 85761:
//                case 85762:
//                case 85763:
//                case 85764:
//                case 85765:
//                case 85766:
//                case 85771:
//                case 85772:
//                case 85773:
//                case 85811:
//                case 85812:
//                case 85813:
//                case 85814:
//                case 85815:
//                case 85816:
//                case 85817:
//                case 85819:
//                case 85851:
//                case 85861:
//                case 85871:
//                case 85872:
//                case 85881:
//                case 85912:
//                case 85913:
//                case 85914:
//                case 85916:
//                case 85917:
//                case 85918:
//                case 85972:
//                case 85973:
//                case 85974:
//                case 85981:
//                case 85982:
//                case 85983:
//                case 86111:
//                case 86112:
//                case 86113:
//                case 86114:
//                case 86115:
//                case 86116:
//                case 86118:
//                case 86152:
//                case 86153:
//                case 86161:
//                case 86171:
//                case 86181:
//                case 86183:
//                case 86211:
//                case 86212:
//                case 86213:
//                case 86214:
//                case 86215:
//                case 86216:
//                case 86217:
//                case 86219:
//                case 86252:
//                case 86253:
//                case 86261:
//                case 86262:
//                case 86271:
//                case 86272:
//                case 86312:
//                case 86313:
//                case 86314:
//                case 86315:
//                case 86316:
//                case 86317:
//                case 86318:
//                case 86319:
//                case 86352:
//                case 86353:
//                case 86361:
//                case 86362:
//                case 86371:
//                case 86372:
//                case 86374:
//                case 86375:
//                case 86376:
//                case 86377:
//                case 86378:
//                case 86379:
//                case 86381:
//                case 86382:
//                case 86411:
//                case 86412:
//                case 86413:
//                case 86414:
//                case 86415:
//                case 86416:
//                case 86417:
//                case 86418:
//                case 86419:
//                case 86452:
//                case 86461:
//                case 86462:
//                case 86463:
//                case 86464:
//                case 86472:
//                case 86511:
//                case 86512:
//                case 86513:
//                case 86514:
//                case 86515:
//                case 86516:
//                case 86517:
//                case 86518:
//                case 86519:
//                case 86561:
//                case 86571:
//                case 86572:
//                case 86581:
//                case 86582:
//                case 86583:
//                case 86584:
//                case 86591:
//                case 86592:
//                case 86611:
//                case 86612:
//                case 86613:
//                case 86615:
//                case 86616:
//                case 86681:
//                case 86682:
//                case 86683:
//                case 86684:
//                case 86685:
//                case 86691:
//                case 86692:
//                case 86752:
//                case 86753:
//                case 86754:
//                case 86756:
//                case 86757:
//                case 87111:
//                case 87112:
//                case 87113:
//                case 87114:
//                case 87115:
//                case 87116:
//                case 87152:
//                case 87153:
//                case 87161:
//                case 87171:
//                case 87172:
//                case 87181:
//                case 87182:
//                case 87211:
//                case 87212:
//                case 87213:
//                case 87214:
//                case 87215:
//                case 87216:
//                case 87217:
//                case 87252:
//                case 87253:
//                case 87254:
//                case 87255:
//                case 87257:
//                case 87258:
//                case 87261:
//                case 87262:
//                case 87263:
//                case 87271:
//                case 87272:
//                case 87282:
//                case 87283:
//                case 87284:
//                    $courier = 'NINJA';
//                    break;
//                default:
//                    $courier = 'RPX';
//                    break;
//            }
//            return $courier;
//        }
//
//        return 'Missing address';
//    }

    public function getCovered()
    {
        if(isset($this->address) && isset($this->address->subDistrict) && isset($this->address->subDistrict->postcode)){
            return $this->address->subDistrict->postcode->code_covered;
        }
        if(isset($this->address) && isset($this->address->district)){
            return $this->address->district->district_covered;
        }
    }

    /**
     * Delete the model from the database.
     *
     * @return bool|null
     *
     * @throws \Exception
     */
    public function delete()
    {
        if ($this->invoice()->first() || $this->shipped) {
            return;
        }

        parent::delete();
    }
}
