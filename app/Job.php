<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Job extends Model implements Transformable {

    use Translatable, TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'jobs';

    /**
     * @var array
     */
    protected $translatedAttributes = ['name', 'price_new', 'price_old', 'currency'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'price_new', 'price_old', 'currency'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated', 'percent'];

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    public function getPercentAttribute()
    {
        return round($this->price_new / $this->price_old * 100, 0) - 100;
    }

    public function getBasePercent($jobs, $key)
    {
        $percent = 100 - $key * 15;

        if ($percent > 100)
        {
            $percent = 90;
        }

        return $percent;
    }

    public function getOldPercent($jobs, $key)
    {
        $base_percent = $this->getBasePercent($jobs, $key);

        $calc = $this->price_old / $this->price_new;

        $percent = $base_percent * $calc;

        return round($percent, 0);

    }

}
