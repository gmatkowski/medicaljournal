<?php
/**
 * User: Grzegorz
 * Date: 2016-10-31
 * Time: 10:14
 */

namespace App\Shipping;

//use Cache;
use App\Order;
//use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

/**
 * Class RPXProvider
 * @package App\Shipping
 */
class RPXProvider implements ShippingProvider {

    /**
     * @var string
     */
    public static $wsdl = 'http://api.rpxholding.com/wsdl/rpxwsdl.php?wsdl';
    /**
     * @var string
     */
    public static $format = 'JSON';

    /**
     * @var \SoapClient
     */
    protected $client;

    /**
     * @var
     */
    protected $username;

    /**
     * @var
     */
    protected $password;

    /**
     * @var
     */
    protected $shipper_id;

    /**
     * RPXProvider constructor.
     */
    public function __construct(Config $c)
    {
        $this->client = new \SoapClient(self::$wsdl);
        $this->username = $c::get('rpx.username');
        $this->password = $c::get('rpx.password');
        $this->shipper_id = $c::get('rpx.shipper_id');
    }

    /**
     * @param $data
     * @return mixed
     */
    private function collectData(Order $order)
    {
        $shipper_name = $order->getLanguagesNamesAttribute();
        $desc_of_goods = $order->size ? 'products: '.$order->getLanguagesNamesAttribute().' '.$order->size : 'products: '.$order->getLanguagesNamesAttribute();

        if(strpos($order->getLanguagesNamesAttribute(), 'Move Powerbank') !== false)
        {
            $shipper_name='';
            $desc_of_goods='';
        }

        $data = array_merge([
            'user'     => $this->username,
            'password' => $this->password,
        ], [
            'awb'                   => '',
            'package_id'            => str_replace("-", $order->ref_or_id, ""),
            'order_type'            => 'MP',
            'order_number'          => str_replace("-", $order->ref_or_id, ""),
            'service_type_id'       => 'ERP',
            'shipper_account'       => $this->shipper_id,
            'shipper_name'          => $shipper_name,
            'shipper_company'       => '',
            'shipper_address1'      => '',
            'shipper_address2'      => '',
//            'shipper_company'       => 'Digital Commerce Indonesia',
//            'shipper_address1'      => 'Graha Lima Lima',
//            'shipper_address2'      => 'Jl. Tanah abang II No.57',
            'shipper_kelurahan'     => 'Petojo Selatan, Gambir',
            'shipper_kecamatan'     => 'Jakarta Pusat',
            'shipper_city'          => 'Jakarta',
            'shipper_state'         => 'DKI JAKARTA INDONESIA',
            'shipper_zip'           => '10160',
            'shipper_phone'         => '62 888 01000488',
            'identity_no'           => $order->isCOD() ? 'COD' : 'NON_COD',
            'shipper_mobile_no'     => '62 888 01000488',
            'shipper_email'         => 'Garcinia.order@gmail.com',
            'consignee_account'     => '',
            'consignee_name'        => $order->name,
            'consignee_company'     => $order->name,
            'consignee_address1'    => $order->address->address,
            'consignee_address2'    => '',
            'consignee_kelurahan'   => $order->address->district->name,
            'consignee_kecamatan'   => $order->address->subdistrict,
            'consignee_city'        => $order->address->district->city->name,
            'consignee_state'       => $order->address->district->city->province->name,
            'consignee_zip'         => $order->getPostalCode(),
            'consignee_phone'       => $order->phone,
            'consignee_mobile_no'   => $order->phone,
            'consignee_email'       => $order->email,
            'desc_of_goods'         => $desc_of_goods,
            'tot_package'           => 1,
            'actual_weight'         => 1,
            'tot_weight'            => 1,
            'tot_declare_value'     => $order->isCOD() ? $order->price : '',
            'tot_dimensi'           => '',
            'flag_mp_spec_handling' => '',
            'insurance'             => 'N',
            'surcharge'             => 'N',
            'high_value'            => 'N',
            'value_docs'            => 'N',
            'electronic'            => 'N',
            'flag_dangerous_goods'  => 'N',
            'flag_birdnest'         => 'N',
            'format'                => self::$format,
        ]);

        return $data;
    }

//    public function sendPickupRequest(Order $order)
//    {
//        return $this->__respond($this->client->__soapCall('sendPickupRequest', $this->collectData([
//            'order_type'             => 'MP',
//            'pickup_agent_id'        => 'DIGITALCOMMERCE',
//            'pickup_ready_time'      =>  Carbon::now()->format('Y-m-d h:n'),
//            'pickup_request_by'      => 'Anastasya Welis',
//            'pickup_account_number'  =>  $this->shipper_id,
//            'pickup_company_name'    => 'DIGITALCOMMERCE',
//            'pickup_company_address' => 'GRAHA LIMA LIMA , LANTAI DASAR, JL TANAH ABANG II NO 57, JAKARTA PUSAT, 10160',
//            'pickup_city'            => 'JAK',
//            'pickup_postal_code'     => '10160',
//            'service_type'           => 'ERP',
//            'desc_of_goods'          => 'DIGITALCOMMERCE PACKAGES',
//            'tot_declare_value'      => $order->isCOD() ? $order->price : '',
//            'office_closed_time'     => '18:00',
//            'pickup_shipper_name'    => 'DIGITALCOMMERCE',
//            'pickup_company_email'   => 'contact@medical-jurnal.com',
//            'pickup_cellphone'       => '62 888 01000488',
//            'pickup_phone'           => '62 888 01000488',
//            'destin_postal_code'     => $this->getPostalCode($order->getPostalCode(), $order->address->district->city->name),
//            'destin_city'            => $this->getCityName($order->address->district->city->name),
//            'destin_province'        => $this->getProvince($order->address->district->city->province->name),
//            'total_weight'           => '1',
//            'total_package'          => '1',
//            'format'                 => self::$format
//        ])));
//    }

    /**
     * @param Order $order
     * @return mixed
     */
    public function sale(Order $order)
    {
        Log::info('Sending order (' . $order->id . ') using RPX');

        $data = $this->collectData($order);

        $response = $this->client->__soapCall('sendShipmentData', $data);

        Log::notice('RPX response: ', [
            'response' => $response
        ]);

        return $this->xmlToJson($response);
    }

    /**
     * @param Order $order
     */
    public function ship(Order $order)
    {

    }

    public function xmlToJson($jsonResponse)
    {
        $xmlResponse = @simplexml_load_string($jsonResponse);
        if ($xmlResponse) {
            $jsonParsedResponse = json_encode($xmlResponse);
            return $this->__respond($jsonParsedResponse);
        } else {
            return $this->__respond($jsonResponse);
        }
    }

//    /**
//     * @return mixed
//     */
//    public function getPostalCode($postalCode, $cityName)
//    {
//        return Cache::remember('RPXpostals', 60, function() use($postalCode, $cityName) {
//            return $this->__respondPostal($postalCode, $this->client->__soapCall('getPostalCode', $this->collectData([
//                'city_id' => $this->getCityName($cityName),
//                'format' => self::$format
//            ])));
//        });
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getCityName($cityName)
//    {
//        return Cache::remember('RPXcities', 60, function() use($cityName) {
//            return $this->__respondCity($cityName, $this->client->__soapCall('getCity', $this->collectData([
//                'format' => self::$format
//            ])));
//        });
//    }
//
//    /**
//     * @return mixed
//     */
//    public function getProvince($provinceName)
//    {
//        return Cache::remember('RPXprovinces', 60, function() use($provinceName) {
//            return $this->__respondProvince(strtoupper($provinceName), $this->client->__soapCall('getProvince', $this->collectData([
//                'format' => self::$format
//            ])));
//        });
//    }

    /**
     * @param Order $order
     */
    public function status(Order $order)
    {

    }

//    public function __respondPostal($postalCode, $response)
//    {
//        $response = json_decode($response);
//        if (isset($response->RPX->DATA[0]->POSTAL_CODE))
//        {
//            $data = $response->RPX->DATA;
//            foreach($data as $postal){
//                if($postalCode==$postal->POSTAL_CODE){
//                    return $postal->POSTAL_CODE;
//                }
//            }
//            throw new ShippingException('Order Postal Code is not handled by RPX');
//        } else
//        {
//            throw new ShippingException('No data results. Something goes wrong with RPX response.');
//        }
//    }
//
//    public function __respondCity($cityName, $response)
//    {
//        $response = json_decode($response);
//        if (isset($response->RPX->DATA[0]->CITY_ID))
//        {
//            $data = $response->RPX->DATA;
//            foreach($data as $city){
//                if(strtolower($cityName)==strtolower($city->CITY_NAME)){
//                    return $city->CITY_ID;
//                }
//            }
//            throw new ShippingException('Order City name is not handled by RPX');
//        } else
//        {
//            throw new ShippingException('No data results. Something goes wrong with RPX response.');
//        }
//    }
//
//    public function __respondProvince($provinceName, $response)
//    {
//        $response = json_decode($response);
//        if (isset($response->RPX->DATA[0]->PROVINCE))
//        {
//            $data = $response->RPX->DATA;
//            foreach($data as $province){
//                if(strpos($provinceName, $province->PROVINCE) !== false){
//                    return $province->PROVINCE;
//                }
//            }
//            throw new ShippingException('Order Province name is not handled by RPX');
//        } else
//        {
//            throw new ShippingException('No data results. Something goes wrong with RPX response');
//        }
//    }

    /**
     * @param $response
     * @return mixed
     * @throws ShippingException
     */
    public function __respond($response)
    {
        $response = json_decode($response);

        if (isset($response->RPX->DATA[0]->RESULT))
        {
            if ($response->RPX->DATA[0]->RESULT!='Success')
            {
                throw new ShippingException($response->RPX->DATA[0]->RESULT);
            }
        }
        else
        {
            throw new ShippingException('No data results. Something goes wrong with RPX response.');
        }

        return [
            'status'         => 200,
            'package_number' => isset($response->RPX->DATA[0]->AWB_RETURN) ? $response->RPX->DATA[0]->AWB_RETURN : null
        ];
    }
}