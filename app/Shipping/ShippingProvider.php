<?php namespace App\Shipping;

use App\Order;

interface ShippingProvider {

    public function sale(Order $order);
    public function ship(Order $order);
    public function status(Order $order);

}