<?php
namespace App\Shipping;

use App\Order;
use App\Repositories\StockRepository;
use Carbon\Carbon;
use App\Sms\ZenzivaSms;
use Illuminate\Support\Facades\Log;

class ProviderSwitcher implements ShippingProvider
{
    private $stocks;
    private $orderMailer;

    public function __construct(StockRepository $stockRepository)
    {
        $this->stocks = $stockRepository;
        $this->orderMailer = app('App\Mail\OrderMailer');
    }

    public function sale(Order $order)
    {
        if($order->getLanguagesNamesAttribute()=='Perfect Shape' && ($order->size=='' || $order->size==null))
        {
            return [
                'status' => 500,
                'message' => 'Please fill a Size first'
            ];
        }

        if($order->isCOD()){

            switch ($order->address->subDistrict->postcode->shipping_provider_name) {
                case 'JAYON':
                    Log::info('Using Jayon provider');
                    $shipper = app(JayonExpressProvider::class);
                    break;
                case 'NINJA':
                    Log::info('Using Ninja provider');
                    $shipper = app(NinjaProvider::class);
                    break;
                case 'RPX':
                    Log::info('Using RPX provider');
                    $shipper = app(RPXProvider::class);
                    break;
            }

        } else {

            Log::info('Using RPX provider');
            $shipper = app(RPXProvider::class);

//            switch ($order->whichCourierForNonCod()) {
//                case 'NINJA':
//                    Log::info('Using Ninja provider');
//                    $shipper = app(NinjaProvider::class);
//                    break;
//                case 'RPX':
//                    Log::info('Using RPX provider');
//                    $shipper = app(RPXProvider::class);
//                    break;
//            }
        }

         $response = $shipper->sale($order);

        if($response['status']==200 || $response['status']==202){
            $this->stockOut($order);
            ZenzivaSms::smsDelivery($order);
            if($order->method == 2)
            {
                $this->orderMailer->codOrderSent($order);
            }
            if($order->method != 2)
            {
                $this->orderMailer->noncodOrderSent($order);
            }
        }

         return $response;
    }

    protected function stockOut(Order $order)
    {
        if($order->isCOD()){
            $orderLanguages = $order->languages;
            foreach($orderLanguages as $orderLanguage){
                $languageTranslations = $orderLanguage->language->languageTranslation;
                foreach($languageTranslations as $languageTranslation){
                    if($languageTranslation->amount!=0){
                        $languageTranslation->amount = $languageTranslation->amount - 1;
                        $languageTranslation->save();
                    }
                }

                $stock = $this->stocks->searchPaginateStocksWithDate(1, $orderLanguage->language->symbol, Carbon::now()->format('d/m/Y').' - '.Carbon::now()->format('d/m/Y'))->first();
                if($stock)
                {
                    $stock->out += 1;
                    $stock->save();
                }
            }
        }
    }

    public function ship(Order $order)
    {

    }

    public function status(Order $order)
    {

    }
}