<?php namespace App\Shipping;

use App\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\Config;
use anlutro\cURL\cURL;
use Illuminate\Support\Facades\Log;

/**
 * Class AcommerceProvider
 * @package App\Shipping
 */
class AcommerceProvider implements ShippingProvider
{
    /**
     *
     */
    const ORDER_ID_PREFIX = 'Order';
    /**
     *
     */
    const SHIPPING_TYPE = 'NEXT_DAY';

    /**
     * @var
     */
    protected $auth_url;
    /**
     * @var
     */
    protected $shipping_url;
    /**
     * @var
     */
    protected $sales_url;

    /**
     * @var cURL
     */
    protected $curl;

    /**
     * @var
     */
    protected $username;
    /**
     * @var
     */
    protected $apiKey;
    /**
     * @var
     */
    protected $channel;
    /**
     * @var
     */
    protected $partner_id;

    /**
     * @var null
     */
    protected $token = null;

    /**
     * @throws \Exception
     */
    public function __construct()
    {
        foreach (Config::get('acommerce') as $key => $value) {
            $this->{$key} = $value;
        }
        $this->curl = new cURL();
        $this->auth();
        //Log::notice('acommerce constructor called');
    }

    /**
     * @param $response
     * @return \Illuminate\Support\Collection
     */
    private function parse($response)
    {
        return collect(json_decode($response));
    }

    /**
     * @throws \Exception
     */
    private function auth()
    {
        $credentials = [
            'auth' => [
                'apiKeyCredentials' => [
                    'username' => $this->username,
                    'apiKey'   => $this->apiKey
                ]
            ]
        ];

        Log::info('Acommerce auth with credentials', $credentials);

        $response = $this->call(
            $this->auth_url,
            $credentials,
            'POST'
        );

        if ($response->statusCode == 401) {
            throw new \Exception($response->statusText);
        }

        $response = $this->parse($response);
        if (!$response->has('token')) {
            throw new \Exception('No authentification');
        }

        $this->token = $response->get('token');

        return $this;
    }


    /**
     * @param Order $order
     * @return array
     * @throws \Exception
     */
    private function collectSaleData(Order $order)
    {
        if (!$order->address)
        {
            throw new \Exception('Address object not defined');
        }

        $customer = [
            'addressee'   => $order->address->full_name,
            'address1'    => $order->address->address,
            'postalCode'  => $order->getPostalCode(),
            'subDistrict' => $order->address->subdistrict,
            'district'    => $order->address->district->name,
            'city'        => $order->address->district->city->name,
            'province'    => $order->address->district->city->province->name,
            'country'     => $order->address->district->city->province->country->name,
            'phone'       => $order->phone,
            'email'       => $order->email
        ];

        $items = collect();
        foreach ($order->languages as $lang)
        {
            $items->push([
                'partnerId' => Config::get('acommerce.partner_id'),
                'itemId'    => $lang->language->shipment_item_id,
                'qty'       => $order->getLanguageQtyAttribute(),
                'subTotal'  => intval($lang->price)
            ]);
        }

        Log::info('shpping data', $items->toArray());

        return [
            'customerInfo'      => $customer,
            'orderShipmentInfo' => $customer,
            'paymentType'       => 'COD',
            'shippingType'      => self::SHIPPING_TYPE,
            'orderItems'        => $items->toArray()
        ];
    }

    private function collectShipData(Order $order)
    {
        if (!$order->address) {
            throw new \Exception('Address object not defined');
        }

        $shipSender = [
            'addressee'   => 'Digital Commerce Indonesia',
            'address1'    => 'Graha Lima Lima',
            'address2'    => 'Jl. Tanah abang II No.57',
            'postalCode'  => '10160',
            'subDistrict' => 'Petojo Selatan, Gambir',
            'district'    => 'Jakarta Pusat',
            'city'        => 'Jakarta',
            'province'    => 'DKI Jakarta',
            'country'     => 'Indonesia',
            'phone'       => '88801015917',
            'email'       => 'Garcinia.order@gmail.com',
            'www'         => 'promos-seru.com'
        ];

        $shipShipment = [
            'addressee'   => $order->address->full_name,
            'address1'    => $order->address->address,
            'postalCode'  => $order->getPostalCode(),
            'subDistrict' => $order->address->subdistrict,
            'district'    => $order->address->district->name,
            'city'        => $order->address->district->city->name,
            'province'    => $order->address->district->city->province->name,
            'country'     => $order->address->district->city->province->country->name,
            'phone'       => $order->phone,
            'email'       => $order->email,
        ];

        $total = $order->languages->pluck('price', 'qty')->map(function ($price, $qty){
            $qty = $qty ? $qty : 1;
            return $price * $qty;
        })->sum();

        return [
            'shipCreatedTime'  => Carbon::now()->toIso8601String(),
            'shipSender'       => $shipSender,
            'shipShipment'     => $shipShipment,
            'shipPickup'       => $shipSender,
            'shipInsurance'    => false,
            'shipShippingType' => self::SHIPPING_TYPE,
            'shipPaymentType'  => 'COD',
            'shipPackages'     => [new \StdClass()],
            'shipGrossTotal'   => $total,

        ];
    }

    /**
     * @param Order $order
     * @return \Illuminate\Support\Collection
     */
    public function sale(Order $order)
    {
        if (!Config::get('app.debug')) {
            $validateEmail = $this->validateEmail($order->email);
            if($validateEmail->has('error')){
                return $this->validateResponse($validateEmail);
            } else {
                Log::info('Sending order (' . $order->id . ') using Acommerce');

                $response = $this->parse($this->call($this->sales_url . '/' . $this->channel . '/order/' . $order->ref_or_id, $this->collectSaleData($order), 'PUT'));

                Log::notice('acommerce response (SALE): ' . $response);

                return $this->validateResponse($response);
            }
        }
    }

    /**
     * @param Order $order
     * @return \Illuminate\Support\Collection
     */
    public function ship(Order $order)
    {
        $validateEmail = $this->validateEmail($order->email);
        if($validateEmail->has('error')){
            return $this->validateResponse($validateEmail);
        } else {
            Log::info('Sending order (' . $order->id . ') using Acommerce');

            $response = $this->parse($this->call($this->shipping_url . '/' . $this->partner_id . '/order/' . $order->ref_or_id, $this->collectShipData($order), 'PUT'));

            Log::notice('acommerce response (SHIP): ' . $response);

            return $this->validateResponse($response);
        }
    }


    /**
     * @param Order $order
     */
    public function status(Order $order)
    {

    }

    /**
     * @param $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->token = $token;

        return $this;
    }

    /**
     * @param $url
     * @param $data
     * @param string $method
     * @return \anlutro\cURL\Response
     */
    private function call($url, $data, $method = 'get')
    {
        Log::notice('request body: ' . json_encode($data));

        $request = $this->curl->newJsonRequest($method, $url, $data);
        if ($this->token) {
            $request
                ->setOption(CURLOPT_CAINFO, storage_path('app/cert/cacert.pem'))
                ->setHeader('X-Subject-Token', $this->token->token_id);
        }
        return $request->send();
    }

    protected function validateEmail($email)
    {
        if($email=='' && !filter_var($email, FILTER_VALIDATE_EMAIL)){
            $invalidEmail = new \stdClass();
            $invalidEmail->message = 'Invalid e-mail.';
            return collect([
                'error' => $invalidEmail
            ]);
        }
        return collect();
    }

    protected function validateResponse($orderResponse)
    {
        if($orderResponse->has('error')){
            $message = $orderResponse->get('error')->message;
            if($orderResponse->get('error')->message=='Order modification is not yet support.'){
                $message = 'Your order has already been shipped. Please mark it as sent.';
            }
        }
        return [
            'status' => $orderResponse->has('error') ? 500 : 200,
            'message' => $orderResponse->has('error') ? $message : null
        ];
    }
}