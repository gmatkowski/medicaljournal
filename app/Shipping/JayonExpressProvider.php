<?php

namespace App\Shipping;

use App\Order;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Config;
use anlutro\cURL\cURL;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

/**
 * Class JayonExpressProvider
 * @package App\Shipping
 */
class JayonExpressProvider implements ShippingProvider
{
    /**
     * @var string
     */
    public $endpoint = 'order';

    /**
     * @var string
     */
    public $method = 'POST';

    /**
     * @var string
     */
    public $returnFormat = 'json';

    public $baseURL;
    public $curl;

    public $apiKey;
    public $merchantId;

//    private $orderRepo;

    /**
     * JayonExpressProvider constructor.
     * @param Config $c
     */
    public function __construct(Config $c, OrderRepository $orderRepository)
    {
        $this->baseURL = $c::get('jayon.baseURL');
        $this->curl = new cURL();

//        $this->orderRepo = $orderRepository;
    }

    /**
     * @param Order $order
     * @return array
     */
    private function collectData(Order $order)
    {
        $trx_detail = collect();
        foreach ($order->languages as $lang) {
            $trx_detail->push([
                'unit_description' => $order->size ? $lang->language->name.' '.$order->size : $lang->language->name,
                'unit_price'       => $lang->price,
                'unit_quantity'    => $lang->qty,
                'unit_total'       => ceil($lang->qty*$lang->price),
                'unit_discount'    => 0,
            ]);
        }

        $trx = [
            'api_key'           => $this->apiKey,
            'buyer_name'        => $order->address->full_name,
            'recipient_name'    => $order->address->full_name,
            'shipping_address'  => $order->address->address,
            'buyerdeliveryzone' => $order->address->district->city->province->name,
            'buyerdeliverycity' => $order->address->district->city->name,
            'buyerdeliverytime' => Carbon::now()->addDay(2)->toDateString(),
            'auto_confirm'      => false,
            'email'             => $order->email,
            'zip'               => $order->getPostalCode(),
            'phone'             => $order->phone,
            'chargeable_amount' => $order->price,
            'currency'          => 'IDR',
            'merchant_id'       => $this->merchantId,
            'buyer_id'          => $order->ref_or_id,
            'trx_detail'        => $trx_detail->toArray(),
            'delivery_type'     => $order->isCOD() ? 'COD' : 'Delivery Only',
            'show_merchant'     => 1,
            'show_shop'         => 1,
            'cod_bearer'        => 'merchant',
            'delivery_bearer'   => 'merchant',
            'transaction_id'    => $this->merchantId . '_' . str_replace(array(' ', '.'), '', microtime()),
            'cod_method'        => 'cash',
//            'ccod_method'       => 'full',
//            'width'             => $_POST('width'),
//            'height'            => $_POST('height'),
//            'length'            => $_POST('length'),
            'weight'            => '1'
//            'status'            => $_POST('status'),
//            'delivery_cost'     => '0 by dafault',
//            'cod_cost'          => '0 if delivery type "delivery only"',
//            'mobile1'           => '',
//            'mobile2'           => '',
//            'total_price'       => $_POST('total_price'),
//            'total_discount'    => $_POST('total_discount'),
//            'total_tax'         => $_POST('total_tax'),
//            'buyerdeliveryslot' => 'int 1 or 2',
//            'directions'        => 'descriptive explanation on how to get to delivery point, optional',
        ];

        return $trx;
    }

    /**
     * @param Order $order
     * @return \anlutro\cURL\Response
     */
    public function sale(Order $order)
    {
        $this->prepareApiKeysForProduct($order);

        Log::info('Sending order (' . $order->id . ') with product (' .$order->getLanguagesNamesAttribute(). ') using JayonExpress');

        $data = $this->collectData($order);
        $fullURL = $this->baseURL . $this->endpoint . '/key/' . $this->apiKey . '/trx/' . $data['transaction_id'];

        $result = $this->curl->jsonPost($fullURL, $data);

        $this->saveOrderStatusParams($order, $data['transaction_id'], json_decode($result->body)->delivery_id);

        Log::notice('JayonExpress response: ',[
            'response' => $result
        ]);

        return $this->validateResponse($result);
    }

    /**
     * @param Order $order
     */
    public function ship(Order $order)
    {

    }

    /**
     * @param Order $order
     */
    public function status(Order $order)
    {
        $this->prepareApiKeysForProduct($order);

        $url = $this->baseURL.'orderstatus/format​/json/​key​/'.$this->apiKey.'/​trx​/'.$order->trx_jayon.'/​did​/'.$order->did_jayon.'/​enc​/plain';
        $url = preg_replace( '/[\x{200B}-\x{200D}\x{FEFF}]/u', '', $url );
        $response = $this->curl->get($url);
        if($response->statusCode==200 || $response->statusCode==202){
            $status = json_decode($response->body)->did->{$order->did_jayon};
            $orderStatus = $this->saveStatus($order, $status);
            return [
                'status' => $orderStatus==7 ? 'D' : '',
                'statusText' => $status
            ];
        }

        return [
            'status' => null,
            'statusText' => null
        ];
    }

    public function saveStatus(Order $order, $status)
    {
        switch ($status) {
            case 'revoked':
                $order->status = 4;
                break;
            case 'canceled':
                $order->status = 6;
                break;
            case 'delivered':
                $order->status = 7;
                break;
            case 'returned':
                $order->status = 8;
                break;
        }
        $order->save();
        return $order->status;
    }

    protected function prepareApiKeysForProduct(Order $order)
    {
        $c=app(Config::class);
        switch ($order->languages[0]->language->name) {
            case 'Garcinia-cambogia':
            case 'Garcinia Promo':
                $this->apiKey = $c::get('jayon.garciiApiKey');
                $this->merchantId = $c::get('jayon.garciiMerchantId');
                break;
            case 'Black-mask':
                $this->apiKey = $c::get('jayon.blackApiKey');
                $this->merchantId = $c::get('jayon.blackMerchantId');
                break;
            case 'Dr. Hallux':
                $this->apiKey = $c::get('jayon.halluproApiKey');
                $this->merchantId = $c::get('jayon.halluproMerchantId');
                break;
            case 'Perfect Shape':
                $this->apiKey = $c::get('jayon.shapeApiKey');
                $this->merchantId = $c::get('jayon.shapeMerchantId');
                break;
            case 'Detoclean':
            case 'Detoclean Promo':
            case 'Detoxic':
                $this->apiKey = $c::get('jayon.detocleanApiKey');
                $this->merchantId = $c::get('jayon.detocleanMerchantId');
                break;
            case 'Green Coffee Fat Burner':
                $this->apiKey = $c::get('jayon.coffeeApiKey');
                $this->merchantId = $c::get('jayon.coffeenMerchantId');
                break;
            case 'Joint Cure':
            case 'Joint Cure Promo':
            case 'Flexa Plus':
                $this->apiKey = $c::get('jayon.jointApiKey');
                $this->merchantId = $c::get('jayon.jointMerchantId');
                break;
            case 'PostureFixerPRO':
                $this->apiKey = $c::get('jayon.postureApiKey');
                $this->merchantId = $c::get('jayon.postureMerchantId');
                break;
            case 'Fly Bra':
                $this->apiKey = $c::get('jayon.brapiKey');
                $this->merchantId = $c::get('jayon.braMerchantId');
                break;
            case 'Move Powerbank':
                $this->apiKey = $c::get('jayon.powerbankapiKey');
                $this->merchantId = $c::get('jayon.powerbankMerchantId');
                break;
            case 'Ultraslim':
            case 'Diet Booster':
                $this->apiKey = $c::get('jayon.ultraslimapiKey');
                $this->merchantId = $c::get('jayon.ultraslimMerchantId');
                break;
            case 'Bustiere (Brest Creame)':
                $this->apiKey = $c::get('jayon.bustiereapiKey');
                $this->merchantId = $c::get('jayon.bustiereMerchantId');
                break;
        }
    }

    protected function validateResponse($orderResponse)
    {
        return [
            'status' => $orderResponse->statusCode,
            'message' => json_decode($orderResponse->body)->status
        ];
    }

    private function saveOrderStatusParams(Order $order, $trx_id, $did)
    {
        $order->trx_jayon = $trx_id;
        $order->did_jayon = $did;
        $order->save();
    }
}