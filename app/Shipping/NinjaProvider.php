<?php

namespace App\Shipping;

use App\Order;
use anlutro\cURL\cURL;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class NinjaProvider implements ShippingProvider {

    private $method = 'POST';
    private $accessEndpoint;
    private $endpoint;
    private $trackEndpoint;
    private $curl;

    private $clientId;
    private $clientSecret;

    public function __construct(Config $c)
    {
//        $this->accessEndpoint = $c::get('app.debug') == false ? 'https://api.ninjavan.co/id/oauth/access_token?grant_type=client_credentials' : 'https://api-sandbox.ninjavan.co/sg/oauth/access_token?grant_type=client_credentials';
//        $this->endpoint = $c::get('app.debug') == false ? 'https://api.ninjavan.co/id/3.0/orders' : 'https://api-sandbox.ninjavan.co/sg/3.0/orders';
        $this->accessEndpoint = 'https://api.ninjavan.co/id/oauth/access_token?grant_type=client_credentials';
        $this->endpoint = 'https://api.ninjavan.co/id/3.0/orders';
        $this->trackEndpoint = 'https://api.ninjavan.co/id/2.0/track';
        $this->curl = new cURL();
    }

    private function collectData(Order $order)
    {
        $details = $order->size ? 'products: '.$order->getLanguagesNamesAttribute().' '.$order->size : 'products: '.$order->getLanguagesNamesAttribute();

        if(strpos($order->getLanguagesNamesAttribute(), 'Move Powerbank') !== false)
        {
            $details='';
        }

        $data = [
            "from_postcode" => "10160",
            "from_address1" => "GRAHA LIMA LIMA , LANTAI DASAR, JL TANAH ABANG II NO 57",
            "from_city" => "Jakarta",
            "from_country" => "Indonesia",
            "from_email" => "anastasyawelis@gmail.com",
            "from_name" => "Garcinia-cambogia",
            "from_contact" => "62 888 01000488",
            "to_postcode" => $order->getPostalCode(),
            "to_address1" => $order->address->address ? $order->address->address : '',
            "to_city" => $order->address->district->city->name ? $order->address->district->city->name : '',
            "to_country" => $order->address->district->city->province->country->name ? $order->address->district->city->province->country->name : '',
            "to_email" => $order->email ? $order->email : '',
            "to_name" => $order->name ? $order->name : '',
            "to_contact" => $order->phone ? $order->phone : '',
            "pickup_instruction" => $details,
            "delivery_instruction" => $details,
            "delivery_date" => $order->created_at->modify('+1 day')->format('Y-m-d'),
            "pickup_date" => $order->created_at->format('Y-m-d'),
            "pickup_timewindow_id" => -1,
            "delivery_timewindow_id" => -1,
            "max_delivery_days" => 2,
            "type" => "NORMAL",
            "parcel_size" => 0,
            "parcel_volume" => 1500,
            "parcel_weight" => 1
        ];
        if($order->isCOD())
        {
            $data['cod_goods'] = $order->price;
        }
        return $data;
    }

    /**
     * @param Order $order
     */
    public function sale(Order $order)
    {
        $this->prepareApiKeysForProduct($order);

        Log::info('Sending order (' . $order->id . ') with product (' .$order->getLanguagesNamesAttribute(). ') using Ninja');

        $accessRequest = $this->curl->newRequest($this->method, $this->accessEndpoint, [
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret
        ])
            ->setHeader('Content-Type', 'application/x-www-form-urlencoded');
        $accessResponse = $accessRequest->send();

        $accessToken = json_decode($accessResponse->body)->access_token;

        $data = $this->collectData($order);
        $orderRequest = $this->curl->newJsonRequest('POST', $this->endpoint, [$data] )
           ->setHeader('Authorization', 'Bearer '.$accessToken);
        $orderResponse = $orderRequest->send();

        Log::notice('Ninja response: ',[
            'response' => $orderResponse
        ]);

        return $this->validateResponse($orderResponse, $order);
    }

    /**
     * @param Order $order
     */
    public function ship(Order $order)
    {

    }

    /**
     * @param Order $order
     */
    public function status(Order $order)
    {
        if(isset($order->id_tracking_ninja))
        {
            $this->prepareApiKeysForProduct($order);

            $response = $this->curl->jsonPost($this->trackEndpoint, ['trackingIds' => [$order->id_tracking_ninja]]);
            if($response->statusCode==200 || $response->statusCode==202){
                $status = json_decode($response->body)[0]->status;
                $orderStatus = $this->saveStatus($order, $status);
                return [
                    'status' => $orderStatus==7 ? 'D' : '',
                    'statusText' => $status
                ];
            }
        }

        return [
            'status' => null,
            'statusText' => null
        ];
    }

    public function saveStatus(Order $order, $status)
    {
        switch ($status) {
            case 'Pickup fail':
                $order->status = 4;
                break;
            case 'Cancelled':
                $order->status = 6;
                break;
            case 'Successful Delivery':
            case 'Completed':
                $order->status = 7;
                break;
            case 'Returned to Sender':
                $order->status = 8;
                break;
        }
        $order->save();
        return $order->status;
    }

    protected function prepareApiKeysForProduct(Order $order)
    {
        $c=app(Config::class);
        switch ($order->languages[0]->language->name) {
            case 'Garcinia-cambogia':
            case 'Garcinia Promo':
                $this->clientId = $c::get('ninja.garciiClientId');
                $this->clientSecret = $c::get('ninja.garciiClientSecret');
                break;
            case 'Black-mask':
                $this->clientId = $c::get('ninja.blackClientId');
                $this->clientSecret = $c::get('ninja.blackClientSecret');
                break;
            case 'Dr. Hallux':
                $this->clientId = $c::get('ninja.haluxClientId');
                $this->clientSecret = $c::get('ninja.haluxClientSecret');
                break;
            case 'Perfect Shape':
                $this->clientId = $c::get('ninja.shapeClientId');
                $this->clientSecret = $c::get('ninja.shapeClientSecret');
                break;
            case 'Detoclean':
            case 'Detoclean Promo':
            case 'Detoxic':
                $this->clientId = $c::get('ninja.detocleanClientId');
                $this->clientSecret = $c::get('ninja.detocleanClientSecret');
                break;
            case 'Green Coffee Fat Burner':
                $this->clientId = $c::get('ninja.coffeeClientId');
                $this->clientSecret = $c::get('ninja.coffeeClientSecret');
                break;
            case 'Joint Cure':
            case 'Joint Cure Promo':
            case 'Flexa Plus':
                $this->clientId = $c::get('ninja.jointClientId');
                $this->clientSecret = $c::get('ninja.jointClientSecret');
                break;
            case 'PostureFixerPRO':
                $this->clientId = $c::get('ninja.postureClientId');
                $this->clientSecret = $c::get('ninja.postureClientSecret');
                break;
            case 'Fly Bra':
                $this->clientId = $c::get('ninja.braClientId');
                $this->clientSecret = $c::get('ninja.braClientSecret');
                break;
            case 'Move Powerbank':
                $this->clientId = $c::get('ninja.powerbankClientId');
                $this->clientSecret = $c::get('ninja.powerbankClientSecret');
                break;
            case 'Ultraslim':
            case 'Diet Booster':
                $this->clientId = $c::get('ninja.ultraslimClientId');
                $this->clientSecret = $c::get('ninja.ultraslimClientSecret');
                break;
            case 'Bustiere (Brest Creame)':
                $this->clientId = $c::get('ninja.bustiereClientId');
                $this->clientSecret = $c::get('ninja.bustiereClientSecret');
                break;
        }
    }

    protected function validateResponse($orderResponse, Order $order)
    {
        switch ($orderResponse->statusCode) {
            case 200:
            case 202:
                $this->saveOrderStatusParams($order, json_decode($orderResponse->body)[0]->id, json_decode($orderResponse->body)[0]->tracking_id);
                $message = json_decode($orderResponse->body)[0]->message;
                break;
            case 400:
            case 401:
            case 500:
                $message = json_decode($orderResponse->body)->message;
                break;
            default:
                $message = $orderResponse->body;
        }

        return [
            'status' => $orderResponse->statusCode,
            'message' => $message
        ];
    }

    private function saveOrderStatusParams(Order $order, $id, $tracking_id)
    {
        $order->id_ninja = $id;
        $order->id_tracking_ninja = $tracking_id;
        $order->save();
    }
}
