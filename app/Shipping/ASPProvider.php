<?php namespace App\Shipping;

use App\Order;
use Carbon\Carbon;
use anlutro\cURL\cURL;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class ASPProvider implements ShippingProvider {

    private $curl;
    private $username = 'APi_ASPexpress';
    private $password = 'Onl-83721_odi';
    private $shipment_url = 'http://182.23.64.40/sibs/asp/api/raw_data/awb';
//    private $city_url = 'http://182.23.64.40/sibs/asp/api/city/list';
//    private $service_url = 'http://182.23.64.40/sibs/asp/api/service/list';
//    private $delivery_url = 'http://182.23.64.40/sibs/asp/api/price/list_all';

    public function __construct(Config $c)
    {
        $this->curl = new cURL();
        $this->username = $c::get('asp.username');
        $this->password = $c::get('asp.password');
    }

    public function ship(Order $order)
    {
    }

    private function collectData(Order $order)
    {
        $data = [
            'allocation_code'			    => '2103', // key
            'shipment_date_string'		    => Carbon::now()->format('dmY'),
            'shipment_time_string'	        => Carbon::now()->format('h:n'),
            'unique_shipment_no'            => '',
            'unique_reference_no'		    => $order->id,
            'company_id'		            => 'CGK20022', // key
            'company_name'			        => 'PT. DIGITAL ACCOMERS INDONESIA', // key
            'city_origin_name'			    => 'Jakarta',
            'city_destination_name'		    => $order->address->district->city->name,
            'service_name'			        => 'REGULER',
            'quantity'				        => 1,
            'weight'			            => 1,
            'insurance_flag'			    => 0,
            'cod_shipment_cost_flag'		=> $order->isCOD() ? 1 : 0,
            'cod_goods_cost_flag'		    => $order->isCOD() ? 1 : 0,
            'goods_is_document_flag'		=> 0,
            'goods_need_packing_flag'		=> 0,
            'goods_is_boxed_flag'		    => 1,
            'goods_value_are'			    => substr($order->price, 0, 9),
            'goods_is_high_values_flag'	    => 0,
            'goods_is_electronic_flag'	    => 0,
            'goods_is_dangerous_flag'		=> 0,
            'goods_is_live_animal_flag'	    => 0,
            'goods_is_live_plant_flag'	    => 0,
            'goods_is_food_flag'			=> 0,
            'goods_textile_flag'			=> 0,
            'shipment_req_pickup_flag'	    => 0,
            'goods_description'             => 'products: '.$order->getLanguagesNamesAttribute(),
            'shipper_name'			        => $order->getLanguagesNamesAttribute(),
//            'shipper_name'			        => 'Digital Commerce Indonesia',
            'shipper_province_name'		    => 'JAKARTA',
            'shipper_district_name'		    => 'PETOJO SELATAN , GAMBIR',
            'shipper_subdistrict_name'	    => 'Jakarta Pusat',
            'shipper_address'			    => 'Jakarta',
//            'shipper_address'			    => 'Graha Lima Lima, Jl. Tanah abang II No.57',
            'shipper_handphone_number'	    => '62 888 01000488',
            'recipient_name'			    => $order->name,
            'recipient_province_name'		=> $order->address->district->city->province->name,
            'recipient_district_name'		=> $order->address->district->name,
            'recipient_subdistrict_name'	=> $order->address->subdistrict,
            'recipient_address'			    => $order->address->address,
            'recipient_handphone_number'	=> $order->phone,
        ];

        return $data;
    }

    public function sale(Order $order)
    {
        Log::info('Sending order (' . $order->id . ') using ASP');

        $data = $this->collectData($order);

        $request = $this->curl->newRequest('post', $this->shipment_url, $data)
            ->auth($this->username, $this->password);
        $result = $request->send();

        Log::notice('ASP response: ', [
            'response' => json_decode($result->body)->status->msg
        ]);

        return $this->validateResponse($result);
    }

//    public function getCityList()
//    {
//        $request = $this->curl->newRequest('get', $this->city_url)
//            ->auth($this->username, $this->password);
//        $result = $request->send();
//
//        $test='test';
//    }

//    public function getServiceList()
//    {
//        $request = $this->curl->newRequest('get', $this->service_url)
//            ->auth($this->username, $this->password);
//        $result = $request->send();
//
//        $test='test';
//    }

//    public function postDeliveryCharge()
//    {
//        $request = $this->curl->newRequest('post', $this->delivery_url, [
//            'from'      => 'JK00',
//            'thru'      => 'JB03',
//            'weight'    => 1
//        ])
//            ->auth($this->username, $this->password);
//        $result = $request->send();
//
//        $test='test';
//    }

    public function status(Order $order)
    {

    }

    /**
     * @param $orderResponse
     * @return array
     */
    protected function validateResponse($orderResponse)
    {
        if(strpos(json_decode($orderResponse->body)->status->msg, 'Failed')!== false){
            $status=500;
        } else {
            $status=200;
        }

        return [
            'status' => $status,
            'message' => json_decode($orderResponse->body)->status->msg
        ];
    }
}
