<?php namespace App\Shipping;

use App\Order;
use Carbon\Carbon;
use anlutro\cURL\cURL;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class SAPProvider implements ShippingProvider {

    private $curl;
    private $username;
    private $password;
    private $shipment_url = 'http://182.23.64.40/sibs/sap/api/raw_data/awb';
//    private $city_url = 'http://182.23.64.40/sibs/sap/api/city/list';
//    private $service_url = 'http://182.23.64.40/sibs/sap/api/service/list';
//    private $delivery_url = 'http://182.23.64.40/sibs/sap/api/price/list_all';

    public function __construct(Config $c)
    {
        $this->curl = new cURL();
    }

    public function ship(Order $order)
    {
    }

    private function collectData(Order $order)
    {
        $data = [
            'allocation_code'			    => '1103', // key
            'shipment_date_string'		    => Carbon::now()->format('dmY'),
            'shipment_time_string'	        => Carbon::now()->format('h:n'),
            'unique_reference_no'		    => substr(explode('@', $order->email)[0], 0, 3).rand(0, 9223372036854775807),
            'company_id'		            => 'CGKN02265', // key
            'company_name'			        => 'Digital Commerce Indonesia', // key
            'city_origin_name'			    => 'Jakarta',
            'city_destination_name'		    => 'BARANANGSIANG',
            'service_name'			        => 'REGULER',
            'quantity'				        => 1,
            'weight'			            => 1,
            'insurance_flag'			    => 1,
            'cod_shipment_cost_flag'		=> $order->isCOD() ? 1 : 0,
            'cod_goods_cost_flag'		    => $order->isCOD() ? 1 : 0,
            'goods_is_document_flag'		=> 0,
            'goods_need_packing_flag'		=> 1,
            'goods_is_boxed_flag'		    => 1,
            'goods_value_are'			    => substr($order->price, 0, 9),
            'goods_is_high_values_flag'	    => 0,
            'goods_is_electronic_flag'	    => 0,
            'goods_is_dangerous_flag'		=> 0,
            'goods_is_live_animal_flag'	    => 0,
            'goods_is_live_plant_flag'	    => 0,
            'goods_is_food_flag'			=> 0,
            'goods_textile_flag'			=> 0,
            'shipment_req_pickup_flag'	    => 0,
            'goods_description'             => 'products: '.$order->getLanguagesNamesAttribute(),
//            'shipper_name'			        => 'Digital Commerce Indonesia',
            'shipper_name'			        => $order->getLanguagesNamesAttribute(),
            'shipper_province_name'		    => 'JAKARTA',
            'shipper_district_name'		    => 'PETOJO SELATAN , GAMBIR',
            'shipper_subdistrict_name'	    => 'Jakarta Pusat',
            'shipper_address'			    => 'Jakarta',
//            'shipper_address'			    => 'Graha Lima Lima, Jl. Tanah abang II No.57',
            'shipper_handphone_number'	    => '62 888 01000488',
            'recipient_name'			    => $order->name,
            'recipient_province_name'		=> $order->address->district->city->province->name,
            'recipient_district_name'		=> $order->address->district->name,
            'recipient_subdistrict_name'	=> $order->address->subdistrict,
            'recipient_address'			    => $order->address->address,
            'recipient_handphone_number'	=> $order->phone,
        ];

        return $data;
    }

    public function sale(Order $order)
    {
        $this->prepareApiKeysForProduct($order);

        Log::info('Sending order (' . $order->id . ') using SAP');

        $data = $this->collectData($order);

        $request = $this->curl->newRequest('post', $this->shipment_url, $data)
            ->auth($this->username, $this->password);
        $result = $request->send();

        Log::notice('SAP response: ', [
            'response' => json_decode($result->body)->status->msg
        ]);

        return $this->validateResponse($result);
    }

//    public function getCityList()
//    {
//        $request = $this->curl->newRequest('get', $this->city_url)
//            ->auth($this->username, $this->password);
//        $result = $request->send();
//
//        $test='test';
//    }

//    public function getServiceList()
//    {
//        $request = $this->curl->newRequest('get', $this->service_url)
//            ->auth($this->username, $this->password);
//        $result = $request->send();
//
//        $test='test';
//    }

//    public function postDeliveryCharge()
//    {
//        $request = $this->curl->newRequest('post', $this->delivery_url, [
//            'from'      => 'JK00',
//            'thru'      => 'JB03',
//            'weight'    => 1
//        ])
//            ->auth($this->username, $this->password);
//        $result = $request->send();
//
//        $test='test';
//    }

    public function status(Order $order)
    {

    }

    protected function prepareApiKeysForProduct(Order $order)
    {
        $c=app(Config::class);
        switch ($order->languages[0]->language->name) {
            case 'Garcinia-cambogia':
                $this->username = $c::get('sap.garciiUsername');
                $this->password = $c::get('sap.garciiPassword');
                break;
            case 'Black-mask':
                $this->username = $c::get('sap.blackUsername');
                $this->password = $c::get('sap.blackPassword');
                break;
            case 'Dr. Hallux':
                $this->username = $c::get('sap.garciiUsername');
                $this->password = $c::get('sap.garciiPassword');
                break;
            case 'Green Coffee Fat Burner':
                $this->username = $c::get('sap.garciiUsername');
                $this->password = $c::get('sap.garciiPassword');
                break;
            default:
                $this->username = $c::get('sap.garciiUsername');
                $this->password = $c::get('sap.garciiPassword');
                break;
        }
    }

    /**
     * @param $orderResponse
     * @return array
     */
    protected function validateResponse($orderResponse)
    {
        if(strpos(json_decode($orderResponse->body)->status->msg, 'Failed')!== false){
            $status=500;
        } else {
            $status=200;
        }

        return [
            'status' => $status,
            'message' => json_decode($orderResponse->body)->status->msg
        ];
    }
}
