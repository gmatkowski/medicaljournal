<?php namespace App\Formatters;


use Laravelrus\LocalizedCarbon\DiffFormatters\DiffFormatterInterface;

/**
 * Class PlDiffFormatter
 * @package App\Formatters
 */
class IdDiffFormatter implements DiffFormatterInterface {

    /**
     * @param $isNow
     * @param $isFuture
     * @param $delta
     * @param $unit
     * @return string
     */
    public function format($isNow, $isFuture, $delta, $unit)
    {

        $unitStr = \Lang::choice("units." . $unit, $delta, array(), 'id');
        $txt = $delta . ' ' . $unitStr;
        //$txt .= $delta == 1 ? '' : 's';

        if ($isNow)
        {
            $txt .= ($isFuture) ? ' dari sekarang' : ' yang lalu';

            return $txt;
        }

        $txt .= ($isFuture) ? ' setelah' : ' sebelum';

        return $txt;
    }
}
