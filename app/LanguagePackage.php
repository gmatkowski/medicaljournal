<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class LanguagePackage
 * @package App
 */
class LanguagePackage extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'language_packages';
    /**
     * @var array
     */
    protected $fillable = ['in_pack', 'percent', 'price'];

}
