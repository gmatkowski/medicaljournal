<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

/**
 * Class Language
 * @package App
 */
class Language extends Model implements Transformable {

    use Translatable, TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'languages';

    /**
     * @var array
     */
    protected $translatedAttributes = ['name', 'name_short', 'description_short', 'price', 'price_old', 'price_spec', 'currency', 'description_try', 'amount', 'min_amount'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'name_short', 'description_short', 'description_try', 'image', 'image_10days', 'icon', 'price', 'price_old', 'price_spec', 'currency', 'symbol', 'sample', 'is_active', 'shipment_item_id', 'amount', 'min_amount'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated', 'image_path','image_10_path', 'icon_path', 'sample_path', 'has_sample'];

    /**
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files()
    {
        return $this->hasMany('App\LanguageFile');
    }

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    /**
     * @return string
     */
    public function getImagePathAttribute()
    {
        return asset('languages/images/' . $this->image);
    }

    public function getImage10PathAttribute()
    {
        return asset('languages/images/' . $this->image_10days);
    }

    /**
     * @return string
     */
    public function getIconPathAttribute()
    {
        return asset('languages/icons/' . $this->icon);
    }


    /**
     * @return string
     */
    public function getSamplePathAttribute()
    {
        return asset('languages/sample/' . $this->sample);
    }

    /**
     * @param $value
     */
    public function setSampleAttribute($value)
    {
        $this->attributes['sample'] = empty($value) ? null : $value;
    }

    public function getHasSampleAttribute()
    {
        return !is_null($this->sample);
    }

    public function languageTranslation()
    {
        return $this->hasMany('App\LanguageTranslation');
    }

    public function stocks()
    {
        return $this->hasMany('App\Stock');
    }
}
