<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use App;
use DiffFormatter;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Forms\DataForm;
use Illuminate\Support\Facades\Auth;

class AppServiceProvider extends ServiceProvider {

    use FormBuilderTrait;

    /**
     * Bootstrap any application services.
     *git pu
     * @return void
     */
    public function boot()
    {
        $formatter = new App\Formatters\IdDiffFormatter;
        DiffFormatter::extend('id', $formatter);

        app('view')->composer('layout.admin', function ($view)
        {
            $action = app('request')->route()->getAction();

            $controller = class_basename($action['controller']);

            list($controller, $action) = explode('@', $controller);

            $view->with(compact('controller', 'action'));
        });

        /*app('view')->composer('layout.app', function ($view)
        {
            $tryForm = $this->form(DataForm::class, [
                'method' => 'POST',
                'url'    => route('page.try.send')
            ]);

            $inactiveForm = $this->form(DataForm::class, [
                'method' => 'POST',
                'url'    => route('page.inactive.send')
            ])->add('language', 'hidden');

            $view->with([
                'tryForm'      => $tryForm,
                'inactiveForm' => $inactiveForm,
            ]);
        });*/

        app('view')->composer('part.instagram', function ($view)
        {
            $instagramRepository = app('App\Repositories\InstagramRepository');

            $view->with([
                'instagramImages' => $instagramRepository->getFeed()
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('App\Repositories\UserRepository', 'App\Repositories\UserRepositoryEloquent');
        App::bind('App\Repositories\PageRepository', 'App\Repositories\PageRepositoryEloquent');
        App::bind('App\Repositories\JobRepository', 'App\Repositories\JobRepositoryEloquent');
        App::bind('App\Repositories\LessonRepository', 'App\Repositories\LessonRepositoryEloquent');
        App::bind('App\Repositories\LanguageRepository', 'App\Repositories\LanguageRepositoryEloquent');
        App::bind('App\Repositories\OpinionRepository', 'App\Repositories\OpinionRepositoryEloquent');
        App::bind('App\Repositories\OrderRepository', 'App\Repositories\OrderRepositoryEloquent');
        App::bind('App\Repositories\OrderFileRepository', 'App\Repositories\OrderFileRepositoryEloquent');
        App::bind('App\Repositories\RecordRepository', 'App\Repositories\RecordRepositoryEloquent');
        App::bind('App\Repositories\StatRepository', 'App\Repositories\StatRepositoryEloquent');
        App::bind('App\Repositories\ContactRepository', 'App\Repositories\ContactRepositoryEloquent');
        App::bind('App\Repositories\LanguageFileRepository', 'App\Repositories\LanguageFileRepositoryEloquent');
        App::bind('App\Repositories\LanguagePackageRepository', 'App\Repositories\LanguagePackageRepositoryEloquent');
        App::bind('App\Repositories\LanguageRenewLinkRepository', 'App\Repositories\LanguageRenewLinkRepositoryEloquent');
        App::bind('App\Repositories\OrderInvoiceRepository', 'App\Repositories\OrderInvoiceRepositoryEloquent');

        App::bind('App\Repositories\CountryRepository', 'App\Repositories\CountryRepositoryEloquent');
        App::bind('App\Repositories\ProvinceRepository', 'App\Repositories\ProvinceRepositoryEloquent');
        App::bind('App\Repositories\CityRepository', 'App\Repositories\CityRepositoryEloquent');
        App::bind('App\Repositories\DistrictRepository', 'App\Repositories\DistrictRepositoryEloquent');
        App::bind('App\Repositories\SubDistrictRepository', 'App\Repositories\SubDistrictRepositoryEloquent');
        App::bind('App\Repositories\OrderAddressRepository', 'App\Repositories\OrderAddressRepositoryEloquent');
        App::bind('App\Repositories\FaqRepository', 'App\Repositories\FaqRepositoryEloquent');
        App::bind('App\Repositories\SettingRepository', 'App\Repositories\SettingRepositoryEloquent');
        App::bind('App\Repositories\RoleRepository', 'App\Repositories\RoleRepositoryEloquent');
        App::bind('App\Repositories\PermissionRepository', 'App\Repositories\PermissionRepositoryEloquent');
        App::bind('App\Repositories\PostRepository', 'App\Repositories\PostRepositoryEloquent');
        App::bind('App\Repositories\PostCodeRepository', 'App\Repositories\PostCodeRepositoryEloquent');
        App::bind('App\Repositories\HeaderRepository', 'App\Repositories\HeaderRepositoryEloquent');
        App::bind('App\Repositories\ExternalClientRepository', 'App\Repositories\ExternalClientRepositoryEloquent');
        App::bind('App\Repositories\OrderLanguageRepository', 'App\Repositories\OrderLanguageRepositoryEloquent');
        App::bind('App\Repositories\StockRepository', 'App\Repositories\StockRepositoryEloquent');
        App::bind('App\Repositories\BankTransferUniqueRepository', 'App\Repositories\BankTransferUniqueRepositoryEloquent');

        /* Payment */
        App::bind('App\Payment\CodProvider', 'App\Payment\VeritransCodProvider');
        App::bind('App\Payment\PaymentProvider', 'App\Payment\VeritransTransferProvider');

        /* Shipping */
        App::bind('App\Shipping\ShippingProvider', 'App\Shipping\ProviderSwitcher');

        /* Sms */
        //App::bind('App\Sms\SmsProvider', Config::get('app.debug') == true ? 'App\Sms\DumpSms' : 'App\Sms\CmSms');
        App::bind('App\Sms\SmsProvider', 'App\Sms\DumpSms');

        App::bind('App\Call\CallCenter', 'App\Call\Focus');

        if ($this->app->environment() !== 'production')
        {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
