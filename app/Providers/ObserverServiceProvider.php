<?php namespace App\Providers;

use App\Contact;
use App\Faq;
use App\Job;
use App\Language;
use App\LanguagePackage;
use App\Lesson;
use App\Observers\ContactObserver;
use App\Observers\FaqObserver;
use App\Observers\JobObserver;
use App\Observers\LanguageObserver;
use App\Observers\LessonObserver;
use App\Observers\OpinionObserver;
use App\Observers\OrderObserver;
use App\Observers\PageObserver;
use App\Observers\RecordObserver;
use App\Observers\SettingObserver;
use App\Observers\StatObserver;
use App\Observers\HeaderObserver;
use App\Opinion;
use App\Order;
use App\Page;
use App\Record;
use App\Setting;
use App\Stat;
use App\Header;
use Illuminate\Support\ServiceProvider;


class ObserverServiceProvider extends ServiceProvider {

    /**
     * Bootstrap any necessary services.
     *
     * @return void
     */
    public function boot()
    {
        Page::observe(new PageObserver());
        Language::observe(new LanguageObserver());
        Job::observe(new JobObserver());
        Opinion::observe(new OpinionObserver());
        Lesson::observe(new LessonObserver());
        LanguagePackage::observe(new LanguagePackage());
        Contact::observe(new ContactObserver());
        Order::observe(new OrderObserver());
        Record::observe(new RecordObserver());
        Stat::observe(new StatObserver());
        Faq::observe(new FaqObserver());
        Setting::observe(new SettingObserver());
        Header::observe(new HeaderObserver());
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
    }

}