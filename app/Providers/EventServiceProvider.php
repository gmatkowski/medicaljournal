<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\OrderWasCreated' => [
            'App\Listeners\EmailOrderConfirmation',
        ],
        'App\Events\OrderWasPayed' => [
            'App\Listeners\EmailOrderWithProduct',
        ],
        'App\Events\ContactWasCreated' => [
            'App\Listeners\SendContactToAffilator',
            'App\Listeners\SendContactToCallCenter'
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
