<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LanguageRenewLinkRepository
 * @package namespace App\Repositories;
 */
interface LanguageRenewLinkRepository extends RepositoryInterface
{
    //
}
