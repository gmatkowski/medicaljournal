<?php

namespace App\Repositories;

use App\BankTransferUnique;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class BankTransferUniqueRepositoryEloquent
 * @package namespace App\Repositories;
 */
class BankTransferUniqueRepositoryEloquent extends BaseRepository implements BankTransferUniqueRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return BankTransferUnique::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
