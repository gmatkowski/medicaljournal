<?php

namespace App\Repositories;

use App\Repositories\UserRepository;
use App\User;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class UserRepositoryEloquent
 * @package namespace AppRepositories;
 */
class UserRepositoryEloquent extends BaseRepository implements UserRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return User::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function newest($per_page = 10)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'DESC');
        })->paginate($per_page);
    }

    public function stockAlerts()
    {
        return $this->scopeQuery(function ($query)
        {
            return $query
                ->where('show_stock', 1)
                ->orderBy('id', 'DESC');
        })->all();
    }
}
