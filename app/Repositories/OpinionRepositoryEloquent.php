<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OpinionRepository;
use App\Opinion;
use Cache;

/**
 * Class OpinionRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OpinionRepositoryEloquent extends BaseRepository implements OpinionRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Opinion::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }


    /**
     * @param int $status
     * @return mixed
     */
    public function opinions($status = 0)
    {
        return Cache::tags(['opinions::list', 'opinions::status::' . $status])->rememberForever('opinions::' . $status, function () use ($status)
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->orderBy('id', 'DESC');
            })->findByField('main', $status);
        });
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'DESC');
        })->paginate($per_page);
    }
}
