<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SubDistrictRepository
 * @package namespace App\Repositories;
 */
interface SubDistrictRepository extends RepositoryInterface
{
    //
}
