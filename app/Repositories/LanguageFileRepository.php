<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LanguageFileRepository
 * @package namespace App\Repositories;
 */
interface LanguageFileRepository extends RepositoryInterface
{
    //
}
