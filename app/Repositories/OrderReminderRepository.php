<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderReminderRepository
 * @package namespace App\Repositories;
 */
interface OrderReminderRepository extends RepositoryInterface
{
    //
}
