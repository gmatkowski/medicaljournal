<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\SubDistrictRepository;
use App\SubDistrict;

/**
 * Class SubDistrictRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SubDistrictRepositoryEloquent extends BaseRepository implements SubDistrictRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return SubDistrict::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
