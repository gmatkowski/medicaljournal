<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\StatRepository;
use App\Stat;
use Cache;

/**
 * Class StatRepositoryEloquent
 * @package namespace App\Repositories;
 */
class StatRepositoryEloquent extends BaseRepository implements StatRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Stat::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }

    /**
     * @return mixed
     */
    public function stats()
    {
        return Cache::tags(['stats::list'])->rememberForever('stat', function ()
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->orderBy('id','ASC');
            })->all();
        });
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'ASC');
        })->paginate($per_page);
    }
}
