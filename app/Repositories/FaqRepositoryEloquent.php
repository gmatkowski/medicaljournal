<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\FaqRepository;
use App\Faq;
use Cache;

/**
 * Class LessonRepositoryEloquent
 * @package namespace App\Repositories;
 */
class FaqRepositoryEloquent extends BaseRepository implements FaqRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Faq::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }

    /**
     * @param int $type
     * @return mixed
     */
    public function faqs($type)
    {
        return Cache::tags(['faqs::list'])->rememberForever('faq::'.$type, function () use($type){
            return $this->scopeQuery(function ($query)
            {
                return $query->orderBy('id', 'ASC');
            })->findByField('type', $type);
        });
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'ASC');
        })->paginate($per_page);
    }
}
