<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface StatRepository
 * @package namespace App\Repositories;
 */
interface StatRepository extends RepositoryInterface
{
    //
}
