<?php namespace App\Repositories\Criteria;

use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Class UserCriteria
 * @package App\Repositories\Criteria
 */
class UserCriteria implements CriteriaInterface {

    /**
     * @var User
     */
    protected $user;


    /**
     * UserCriteria constructor.
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (is_array($this->user))
        {
            $model = $model->whereIn('user_id', $this->user);
        }
        else{
            $model = $model->where('user_id', $this->user->id);
        }

        return $model;
    }
}