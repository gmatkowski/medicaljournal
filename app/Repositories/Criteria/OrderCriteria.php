<?php namespace App\Repositories\Criteria;

use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Class UserCriteria
 * @package App\Repositories\Criteria
 */
class OrderCriteria implements CriteriaInterface {

    /**
     * @var User
     */
    protected $filter;


    /**
     * UserCriteria constructor.
     * @param $filter
     */
    public function __construct($filter)
    {
        $this->filter = $filter;
    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ($this->filter == 'cc')
        {
            $model = $model->whereNotNull('user_id');
        }
        elseif ($this->filter == 'customer'){
            $model = $model->whereNull('user_id');
        }

        return $model;
    }
}