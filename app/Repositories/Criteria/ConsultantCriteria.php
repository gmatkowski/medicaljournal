<?php namespace App\Repositories\Criteria;

use App\User;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;

/**
 * Class ConsultantCriteria
 * @package App\Repositories\Criteria
 */
class ConsultantCriteria implements CriteriaInterface {

    /**
     * @var User
     */
    protected $filter;


    /**
     * UserCriteria constructor.
     * @param $filter
     */
    public function __construct()
    {

    }

    /**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $model = $model->whereHas('role', function ($query) {
                $query->where('name', '=', 'consultant');
        });

        return $model;
    }
}