<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderInvoiceRepository
 * @package namespace App\Repositories;
 */
interface OrderInvoiceRepository extends RepositoryInterface
{
    //
}
