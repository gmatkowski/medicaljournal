<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use App\Repositories\PageRepository;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

use App\Page;
use Cache;

/**
 * Class PageRepositoryEloquent
 * @package namespace AppRepositories;
 */
class PageRepositoryEloquent extends BaseRepository implements PageRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Page::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'DESC');
        })->paginate($per_page);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return Cache::tags(['pages::list', 'page::' . $id])->rememberForever('page::id::' . $id, function () use ($id)
        {
            return $this->find($id);
        });
    }

    /**
     * @return mixed
     */
    public function getWelcomeSet()
    {
        return Cache::tags(['pages::list'])->rememberForever('welcome::set', function ()
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->whereIn('id', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 21, 25])->orderBy('id', 'ASC');
            })->all();
        });
    }

    /**
     * @return mixed
     */
    public function getFooterSet()
    {
        return Cache::tags(['pages::list'])->rememberForever('footer::set', function ()
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->whereIn('id', [12, 13, 20])->orderBy('id', 'ASC');
            })->all();
        });
    }

    public function getLanguageseSet()
    {
        return Cache::tags(['pages::list'])->rememberForever('languages::set', function ()
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->whereIn('id', [17, 18, 23])->orderBy('id', 'ASC');
            })->all();
        });
    }
}
