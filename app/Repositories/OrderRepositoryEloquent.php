<?php

namespace App\Repositories;

use DB;
use App\Order;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class OrderRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrderRepositoryEloquent extends BaseRepository implements OrderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Order::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function beforecodWithAddress()
    {
        return $this->with(['address'])->scopeQuery(function ($query) {
            return $query
                ->has('address')
                ->where('shipped', 0)
                ->whereNotNull('user_id')
//                ->whereNotNull('sex')
//                ->whereNotNull('age')
                ->where('user_id', '<>', '')
                ->orderBy('created_at', 'ASC');

        })->whereHas('address', function ($query) {
            return $query
                ->whereNotNull('city')
                ->whereNotNull('district_id');
        })->all();
    }

    public function aftercodWithAddress()
    {
        return $this->with(['address'])->scopeQuery(function ($query) {
            return $query
                ->has('address')
                ->where('shipped', 1)
                ->whereNotNull('user_id')
//                ->whereNotNull('sex')
//                ->whereNotNull('age')
                ->where('user_id', '<>', '')
                ->orderBy('created_at', 'ASC');

        })->whereHas('address', function ($query) {
            return $query
                ->whereNotNull('city')
                ->whereNotNull('district_id');
        })->all();
    }

    public function forSms()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereNotNull('trx_jayon')
                ->whereNotNull('did_jayon')
                ->where('created_at', '>', Carbon::now()->subDay(3)->format('Y-m-d H:i:s'));
        })->all();
    }

    public function getConsultantsCounterFromMarchUntilNow($productId = 1)
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereNotNull('user_id')
                ->where('created_at', '>', Carbon::createFromDate(2017, 3, 1));
        })
            ->whereHas('languages', function ($query) use ($productId) {
                return $query->where('language_id', '=', $productId);
            })
            ->all()->count();
    }

    public function getCustomersCounterFromMarchUntilNow($productId = 1)
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->where('created_at', '>', Carbon::createFromDate(2017, 3, 1));
        })
            ->whereHas('languages', function ($query) use ($productId) {
                return $query->where('language_id', '=', $productId);
            })
            ->all()->count();
    }

    /**
     * @param $ids
     * @return mixed
     */
    public function byIds($ids)
    {
        return $this->scopeQuery(function ($query) use ($ids) {
            return $query->whereIn('id', $ids);
        })->all();
    }

    /**
     * @param $dt1
     * @param $dt2
     * @return mixed
     */
    public function getByDateRange($dt1, $dt2, $productId)
    {
        return $this->with(['languages'])->scopeQuery(function ($query) use ($dt1, $dt2) {
            return $query
                ->where('created_at', '>', $dt1)
                ->where('created_at', '<', $dt2);
        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query
                ->where('language_id', '=', $productId);
        })
        ->all();
    }

    public function getGarciniaFromFebruary()
    {
        return $this->with(['languages', 'contact'])->scopeQuery(function ($query) {
            return $query
                ->whereNull('ref')
                ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', '13/02/2017')->startOfDay());
        })
            ->whereHas('languages', function ($query) {
                return $query->where('language_id', '=', 1);
            })
            ->whereHas('contact', function ($query) {
                return $query
                    ->whereNotNull('cc_id')
//                    ->where('invalid_phone', '=', 0)

                    ->where('cc_sell', '=', 0);
            })
            ->paginate(20);
    }

    public function getBlackMaskFromFirstFebruary()
    {
        return $this->with(['languages', 'contact'])->scopeQuery(function ($query) {
            return $query
                ->whereNull('ref')
                ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', '31/01/2017')->startOfDay());
        })
        ->whereHas('languages', function ($query) {
            return $query->where('language_id', '=', 2);
        })
        ->whereHas('contact', function ($query) {
            return $query
                ->whereNotNull('cc_id')
//                ->where('invalid_phone', '=', 0)

                ->where('cc_sell', '=', 0);
        })
        ->paginate(20);
    }

    /**
     * @param int $method
     * @param int $status
     * @param null $dt1
     * @param null $dt2
     * @return mixed
     */
    public function byMethod($method = 1, $status = -1, $dt1 = null, $dt2 = null, $productId)
    {
        return $this->with(['address', 'languages'])->scopeQuery(function ($query) use ($method, $status, $dt1, $dt2) {

            if (!is_null($dt1)) {
                $query->where('created_at', '>', $dt1);
            }

            if (!is_null($dt2)) {
                $query->where('created_at', '<', $dt2);

            }

            if ($method == 2 && $status == 'shipped')
            {
                $query->where('shipped', 1);

            } elseif ($status == 'not-shipped') {
                $query->where('shipped', 0)->where('to_confirm', 0);
            } elseif ($method == 2 && $status == 'to-confirm') {
                $query->where('shipped', 0)->where('to_confirm', 1)->whereNull('duplicated');
            } else {
                if (!is_array($status) && $status >= 0) {
                    $query->where('status', $status);
                } elseif (is_array($status)) {
                    $query->whereIn('status', $status);
                }
            }

            if($method == 7){
                return $query->where('method', 7)->orderBy('id', 'DESC');
            }

            return $query->where('method', 2)->orderBy('id', 'DESC');
        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->all();
    }

    /**
     * @param $token
     * @return mixed
     */
    public function byToken($token)
    {
        return $this->findByField('token', $token)->first();
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->with(['files'])->scopeQuery(function ($query) {
            return $query->orderBy('id', 'DESC');
        })->paginate($per_page);
    }

    public function listAll($per_page, $productId)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
            return $query
                ->where('archived', 0)
                ->orderBy('id', 'DESC');
        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->newest($per_page);
    }

    /**
     * @param $per_page
     * @return mixed
     */
    public function toNotSent($per_page, $productId)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
            return $query
//                ->has('address')
                ->orderBy('id', 'DESC')
                ->where('method', 2)
                ->where('shipped', 0)
                ->where('to_confirm', 0)
                ->where('archived', 0)
                ->whereNull('duplicated')
                ->where('user_id', '<>', '');

        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->paginate($per_page);
    }

    public function searchNotSent($productId, $searchQuery)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) use ($searchQuery) {
            return $query
                ->where('method', 2)
                ->where('shipped', 0)
                ->where('to_confirm', 0)
                ->where('archived', 0)
                ->whereNull('duplicated')
                ->where('user_id', '<>', '')
                ->where(function($query) use ($searchQuery) {
                    $query->where('name', '=', $searchQuery)
                        ->orwhere('id', '=', $searchQuery)
                        ->orwhere('email', '=', $searchQuery)
                        ->orwhere('first_name', '=', $searchQuery)
                        ->orwhere('phone', '=', $searchQuery)
                        ->orwhere('last_name', '=', $searchQuery);
                });
        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all();
    }

//    /**
//     * @param $per_page
//     * @return mixed
//     */
//    public function duplicated($per_page, $productId)
//    {
//        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
//            return $query
////                ->has('address')
//                ->orderBy('id', 'DESC')
//                ->where('method', 2)
//                ->where('shipped', 0)
//                ->where('duplicated', 1)
//                ->where('archived', 0)
//                ->groupBy('phone','email');
//
//        })
//        ->whereHas('languages', function ($query) use ($productId) {
//            return $query->where('language_id', '=', $productId);
//        })
//        ->paginate($per_page);
//    }

    /**
     * @param $per_page
     * @return mixed
     */
    public function toConfirm($per_page, $productId)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
            return $query
//                ->has('address')
                ->orderBy('id', 'DESC')
                ->where('method', 2)
                ->where('shipped', 0)
                ->where('to_confirm', 1)
                ->where('archived', 0)
                ->whereNull('duplicated');
//                ->whereNull('user_id');

        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->paginate($per_page);
    }

    /**
     * @param $productId
     * @param $searchQuery
     * @return mixed
     */
    public function searchToConfirm($productId, $searchQuery)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) use ($searchQuery) {
            return $query
                ->where('method', 2)
                ->where('shipped', 0)
                ->where('to_confirm', 1)
                ->where('archived', 0)
                ->whereNull('duplicated')
                ->where(function($query) use ($searchQuery) {
                    $query->where('name', '=', $searchQuery)
                        ->orwhere('id', '=', $searchQuery)
                        ->orwhere('email', '=', $searchQuery)
                        ->orwhere('first_name', '=', $searchQuery)
                        ->orwhere('phone', '=', $searchQuery)
                        ->orwhere('last_name', '=', $searchQuery);
                });
        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all();
    }

    /**
     * @param $per_page
     * @return mixed
     */
    public function archived($per_page, $productId)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
            return $query
//                ->has('address')
                ->orderBy('id', 'DESC')
                ->where('method', 2)
                ->where('archived', 1);
        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->paginate($per_page);
    }

    /**
     * @param $per_page
     * @return mixed
     */
    public function aftercod($per_page, $productId)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
            return $query
//                ->has('address')
                ->orderBy('id', 'DESC')->where('method', 2)->where('shipped', 1);
        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->paginate($per_page);
    }

    /**
     * @param $productId
     * @param $searchQuery
     * @return mixed
     */
    public function searchAfterCod($productId, $searchQuery)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) use ($searchQuery) {
            return $query
                ->where('method', 2)
                ->where('shipped', 1)
                ->where(function($query) use ($searchQuery) {
                    $query->where('name', '=', $searchQuery)
                        ->orwhere('id', '=', $searchQuery)
                        ->orwhere('email', '=', $searchQuery)
                        ->orwhere('first_name', '=', $searchQuery)
                        ->orwhere('phone', '=', $searchQuery)
                        ->orwhere('last_name', '=', $searchQuery);
                });
        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all();
    }

    /**
     * @param $per_page
     * @return mixed
     */
    public function bankTransfer($per_page, $productId)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
            return $query
//                ->has('address')
                ->orderBy('id', 'DESC')->where('method', 7)->where('shipped', 0);

        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->paginate($per_page);
    }

    /**
     * @param $productId
     * @param $searchQuery
     * @return mixed
     */
    public function searchBankTransfer($productId, $searchQuery)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) use ($searchQuery) {
            return $query
                ->where('method', 7)
                ->where('shipped', 0)
                ->where(function($query) use ($searchQuery) {
                    $query->where('name', '=', $searchQuery)
                        ->orwhere('id', '=', $searchQuery)
                        ->orwhere('email', '=', $searchQuery)
                        ->orwhere('first_name', '=', $searchQuery)
                        ->orwhere('phone', '=', $searchQuery)
                        ->orwhere('last_name', '=', $searchQuery);
                });
        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all();
    }

    /**
     * @param $per_page
     * @return mixed
     */
    public function bankTransferAftercod($per_page, $productId)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) {
            return $query
//                ->has('address')
                ->orderBy('id', 'DESC')->where('method', 7)->where('shipped', 1);
        })
            ->whereHas('languages', function ($query) use ($productId) {
                return $query->where('language_id', '=', $productId);
            })
            ->paginate($per_page);
    }

    /**
     * @param $productId
     * @param $searchQuery
     * @return mixed
     */
    public function searchBankTransferAfterCod($productId, $searchQuery)
    {
        return $this->with(['files', 'languages'])->scopeQuery(function ($query) use ($searchQuery) {
            return $query
                ->where('method', 7)
                ->where('shipped', 1)
                ->where(function($query) use ($searchQuery) {
                    $query->where('name', '=', $searchQuery)
                        ->orwhere('id', '=', $searchQuery)
                        ->orwhere('email', '=', $searchQuery)
                        ->orwhere('first_name', '=', $searchQuery)
                        ->orwhere('phone', '=', $searchQuery)
                        ->orwhere('last_name', '=', $searchQuery);
                });
        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all();
    }

    /**
     * @param int $status
     * @param int $treshold
     * @param int $max
     * @return \Illuminate\Support\Collection
     */
    public function forReminder($status = 0, $treshold = 24, $max = 3)
    {
        $orders = $this->with(['reminders' => function ($query) use ($status) {
            return $query->where('status', $status)->where('type', 'mail')->orderBy('created_at', 'DESC');
        }])->scopeQuery(function ($query) use ($status, $treshold, $max) {
            return $query
                ->where('created_at', '<', Carbon::now()->subHours($treshold))
                ->where('status', $status)
                ->where('version', 1)
                ->where(function ($query) use ($status, $max) {
                    return $query
                        ->whereDoesntHave('reminders')
                        ->orWhereHas('reminders', function ($query) use ($status, $max) {
                            return $query->where('status', $status)->where('type', 'mail')->where('repeated', '<', $max);
                        });
                })
                ->groupBy('email')
                ->orderBy('id', 'DESC');
        })->all();

        $result = collect();

        foreach ($orders as $order) {
            $reminder = $order->reminders->last();

            if (is_null($reminder) || ($reminder->repeated < $max && $reminder->created_at < Carbon::now()->subHours($treshold))) {
                $result->push($order);
            }
        }

        return $result;
    }

    /**
     * @param int $limit
     * @return mixed
     */
    public function forCronStatus($limit = 100)
    {
        return $this->scopeQuery(function ($query) use ($limit) {
            return $query
                ->where(function ($query) {
                    $query->where('status', 0)->orWhere('status', 4);
                })
                ->where('called', 0)
                ->where('method', '!=', 2)
                ->where('created_at', '<', Carbon::now()->subHours(48))
                ->orderBy('created_at', 'ASC')
                ->take($limit);
        })->all();
    }

    /**
     * @return mixed
     */
    public function forCronAlmostFailure()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereDoesntHave('reminders', function ($query) {
                    return $query->where('type', 'sms');
                })
                ->where('status', 0)
                ->where('method', '!=', 2)
                ->where('created_at', '<', Carbon::now()->subHours(17));
        })->all();
    }

    /**
     * @param $searchQuery
     * @return mixed
     */
    public function search($searchQuery, $productId)
    {
        return $this->with(['languages'])->scopeQuery(function ($query) use ($searchQuery) {
            return $query
                ->where('name', '=', $searchQuery)
                ->orwhere('id', '=', $searchQuery)
                ->orwhere('email', '=', $searchQuery)
                ->orwhere('first_name', '=', $searchQuery)
                ->orwhere('phone', '=', $searchQuery)
                ->orwhere('last_name', '=', $searchQuery);
        })
        ->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })
        ->all();
    }

    public function jayonSent()
    {
        return $this->scopeQuery(function ($query){

            return $query
                ->where('status', '=', 2)
                ->whereNotNull('trx_jayon')
                ->whereNotNull('did_jayon')
                ->whereHas('address', function ($query) {
                    $query->whereHas('subDistrict', function ($query) {
                        $query->whereHas('postcode', function ($query) {
                            $query->where('jayon', '=', 1);
                        });
                    });
                })
                ->where('created_at','>', Carbon::today()->subMonth())
                ->orderBy('created_at', 'DESC');

        })->all();
    }

    public function ninjaSent()
    {
        return $this->scopeQuery(function ($query){

            return $query
                ->where('status', '=', 2)
                ->whereNotNull('id_ninja')
                ->whereNotNull('id_tracking_ninja')
                ->whereHas('address', function ($query) {
                    $query->whereHas('subDistrict', function ($query) {
                        $query->whereHas('postcode', function ($query) {
                            $query->where('ninja', '=', 1);
                        });
                    });
                })
                ->where('created_at','>', Carbon::today()->subMonth())
                ->orderBy('created_at', 'DESC');

        })->all();
    }

    /**
     * @param $limit
     * @param $request
     * @return mixed
     */
    public function getFiltered($request)
    {
        return $this->scopeQuery(function ($query) use ($request) {

            if ($request->has('province'))
                $query->whereHas('address', function ($query) use ($request) {
                    $query->whereHas('district', function ($query) use ($request) {
                        $query->whereHas('city', function ($query) use ($request) {
                            $query->whereHas('province', function ($query) use ($request) {
                                $query->where('name', '=', $request->input('province'));
                            });
                        });
                    });
                });

            if ($request->has('city'))
                $query->whereHas('address', function ($query) use ($request) {
                    $query->whereHas('district', function ($query) use ($request) {
                        $query->whereHas('city', function ($query) use ($request) {
                            $query->where('name', '=', $request->input('city'));
                        });
                    });
                });

            if ($request->has('district'))
                $query->whereHas('address', function ($query) use ($request) {
                    $query->whereHas('district', function ($query) use ($request) {
                        $query->where('name', '=', $request->input('district'));
                    });
                });

            if ($request->has('subdistrict'))
                $query->whereHas('address', function ($query) use ($request) {
                    $query->where('subdistrict', '=', $request->input('subdistrict'));
                });

            if ($request->has('age_from') && $request->has('age_to')) {
                $query
                    ->where('age', '>=', $request->input('age_from'))
                    ->where('age', '<=', $request->input('age_to'));

            } elseif ($request->has('age_from')) {
                $query->where('age', '>=', $request->input('age_from'));


            } elseif ($request->has('age_to')) {
                $query->where('age', '<=', $request->input('age_to'));
            }

            if ($request->has('sex'))
                $query->where('sex', '=', $request->input('sex'));

            if ($request->has('other_lang'))
                $query->where('other_lang', '=', $request->input('other_lang'));

            return $query;
        });
    }

    /**
     * @param $order
     * @return mixed
     */
    public function getDuplicate($order)
    {
        return $this->scopeQuery(function ($query) use ($order) {
            return $query
                ->where('method', 2)
                ->where(function ($query) use ($order) {
                    $query
                        ->where('name', $order->name)
                        ->orWhere('email', $order->email)
                        ->orWhere('phone', $order->phone);
                });
        })->all();
    }

    /**
     * @param $roleName
     * @return mixed
     */
    public function getByRole ($roleName){
        return $this->scopeQuery(function ($query) use ($roleName) {

            return $query->whereHas('user', function ($query) use ($roleName) {
                return $query->whereHas('role', function ($query) use ($roleName) {
                    return $query->where('name', '=', $roleName);
                });
            })->where('created_at', '>', Carbon::today());

        })->all();
    }

    public function getConsultantsCounterFromToday()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereNotNull('user_id')
                ->where('created_at', '>', Carbon::today());
        })->all()->count();
    }

    public function getOrdersWithDistrictId()
    {
        return $this->scopeQuery(function ($query) {
            return $query->whereHas('address', function ($query)
            {
                return $query->whereNotNull('district_id');
            });

        })->all();
    }

    public function getOrdersWithAddress()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereNotNull('ref')
                ->whereNotNull('user_id');
        })->all();
    }

    public function todays($productId)
    {
        return $this->with(['languages'])->scopeQuery(function ($query) {

            return $query
                ->whereNotNull('ref')
                ->where('created_at', '>=', Carbon::today()->toDateString());

        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all()->count();
    }

    public function yesterdays($productId)
    {
        return $this->with(['languages'])->scopeQuery(function ($query) {

            return $query
                ->whereNotNull('ref')
                ->where('created_at', '>=', Carbon::yesterday()->toDateString())
                ->where('created_at', '<=', Carbon::today()->toDateString());

        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all()->count();
    }

    public function specificDay($productId, $summaryRange)
    {
        return $this->with(['languages'])->scopeQuery(function ($query) use ($summaryRange)  {
            $parts = explode('/', $summaryRange);
            $dateString = $parts[2].'-'.$parts[1].'-'.$parts[0];
            $nextDay = Carbon::parse($dateString)->addDay(1);

            return $query
                ->whereNotNull('ref')
                ->where('created_at', '>=', $dateString)
                ->where('created_at', '<=', $nextDay);
            
        })->whereHas('languages', function ($query) use ($productId) {
            return $query->where('language_id', '=', $productId);
        })->all()->count();
    }

    public function personalStats($range)
    {
        return $this->with(['address'])->scopeQuery(function ($query) use ($range) {
            return $query
//                ->has('address')
                ->where('shipped', 1)
                ->whereNotNull('user_id')
                ->whereNotNull('sex')
                ->whereNotNull('age')
                ->where('user_id', '<>', '')
                ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', $range[0])->startOfDay())
                ->where('created_at', '<', Carbon::createFromFormat('d/m/Y', $range[1])->startOfDay())
                ->orderBy('created_at', 'DESC');

        })
//            ->whereHas('address', function ($query) {
//            return $query
//                ->whereNotNull('city')
//                ->whereNotNull('district_id');
//        })
            ->all();
    }
}
