<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ExternalClientRepository
 * @package namespace App\Repositories;
 */
interface ExternalClientRepository extends RepositoryInterface
{
    //
}
