<?php

namespace App\Repositories;

use App\Stock;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class StockRepositoryEloquent
 * @package namespace App\Repositories;
 */
class StockRepositoryEloquent extends BaseRepository implements StockRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Stock::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function searchAllProductStocks($productSymbol)
    {
        return $this->scopeQuery(function ($query) {

            return $query
                ->orderBy('created_at', 'ASC');

        })
            ->whereHas('language', function ($query) use ($productSymbol) {
                return $query->where('symbol', '=', $productSymbol);
            })
            ->all();
    }

    public function searchPaginateStocksWithDate($per_page = 10, $productSymbol, $range)
    {
        $data = explode(' - ', $range);

        return $this->scopeQuery(function ($query) use ($data) {

            return $query
                ->where('created_at', '>=', Carbon::createFromFormat('d/m/Y', $data[0])->startOfDay())
                ->where('created_at', '<=', Carbon::createFromFormat('d/m/Y', $data[1])->endOfDay())
                ->orderBy('created_at', 'ASC');

        })
        ->whereHas('language', function ($query) use ($productSymbol) {
            return $query->where('symbol', '=', $productSymbol);
        })
        ->paginate($per_page);
    }

    public function searchNextStocks($productSymbol, $date)
    {
        return $this->scopeQuery(function ($query) use ($date) {

            return $query
                ->where('created_at', '>', Carbon::createFromFormat('Y-m-d H:i:s', $date)->startOfDay())
                ->orderBy('created_at', 'ASC');

        })
            ->whereHas('language', function ($query) use ($productSymbol) {
                return $query->where('symbol', '=', $productSymbol);
            })
            ->all();
    }

    public function searchStocksBehind($productSymbol, $date)
    {
        return $this->scopeQuery(function ($query) use ($date) {

            return $query
                ->where('created_at', '<', Carbon::createFromFormat('Y-m-d H:i:s', $date)->endOfDay())
                ->orderBy('created_at', 'DESC');

        })
            ->whereHas('language', function ($query) use ($productSymbol) {
                return $query->where('symbol', '=', $productSymbol);
            })
            ->all();
    }
}
