<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PageRepository
 * @package namespace AppRepositories;
 */
interface PageRepository extends RepositoryInterface
{
    //
}
