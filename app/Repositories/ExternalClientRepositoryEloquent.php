<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use App\ExternalClient;

/**
 * Class CityRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ExternalClientRepositoryEloquent extends BaseRepository implements ExternalClientRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return ExternalClient::class;
    }
}
