<?php namespace App\Repositories;

use MetzWeb\Instagram\Instagram;
use Config;
use Cache;

/**
 * Class InstagramRepository
 * @package App\Repositories
 */
class InstagramRepository {

    /**
     * @var Instagram
     */
    protected $instagram;
    /**
     * @var
     */
    protected $user;

    /**
     *
     */
    public function __construct()
    {
        $this->instagram = new Instagram(array(
            'apiKey'      => Config::get('instagram.connections.main.client_id'),
            'apiSecret'   => Config::get('instagram.connections.main.client_secret'),
            'apiCallback' => Config::get('instagram.connections.main.callback_url'),
        ));
        $this->instagram->setAccessToken(Config::get('instagram.connections.main.access_token'));
    }

    public function getMe()
    {
        return $this->instagram->getUser();
    }


    public function getFeed($id = 'self', $limit = 10)
    {
        return Cache::tags(['instagram::feed', 'instagram::feed::' . $id])->remember('feed::' . $id . '::' . $limit, 60, function () use ($id, $limit)
        {
            return collect($this->instagram->getUserMedia($id, $limit)->data);
        });
    }

}