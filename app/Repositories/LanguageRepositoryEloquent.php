<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LanguageRepository;
use App\Language;
use Cache;

/**
 * Class LanguageRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LanguageRepositoryEloquent extends BaseRepository implements LanguageRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Language::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }

    /**
     * @return mixed
     */
    public function slider()
    {
        return Cache::tags(['languages::list'])->rememberForever('slider', function ()
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->orderBy('id', 'ASC');
            })->all();
        });
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'ASC');
        })->paginate($per_page);
    }

    /**
     * @param $ids
     * @return mixed
     */
    public function byIds($ids)
    {
        return Cache::tags(['languages::list'])->rememberForever('byids::' . implode('::', $ids), function () use ($ids)
        {
            return $this->with(['translations'])->scopeQuery(function ($query) use ($ids)
            {
                return $query->whereIn('id', $ids);
            })->all();
        });
    }

    /**
     * @return mixed
     */
    public function active()
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->where('is_active', 1);
        })->all();
    }

    /**
     * @return mixed
     */
    public function withSample()
    {
        return Cache::tags(['languages::list'])->rememberForever('withsamples', function ()
        {
            return $this->with(['translations'])->scopeQuery(function ($query)
            {
                return $query->whereNotNull('sample')->orderBy('created_at', 'ASC');
            })->all();
        });
    }
}
