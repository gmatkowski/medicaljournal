<?php

namespace App\Repositories;

use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use App\Contact;

/**
 * Class ContactRepositoryEloquent
 * @package namespace App\Repositories;
 */
class ContactRepositoryEloquent extends BaseRepository implements ContactRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Contact::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10, $product)
    {
        return $this->scopeQuery(function ($query) use ($product) {
            return $query->where('product', '=', $product)->orderBy('created_at', 'DESC');
        })->paginate($per_page);
    }

    /**
     * @param int $treshold
     * @param int $maxReminds
     * @param int $limit
     * @return mixed
     */
    public function forCronTry($treshold = 1, $maxReminds = 3, $limit = 100)
    {
        return $this->scopeQuery(function ($query) use ($treshold, $maxReminds, $limit) {
            return $query
                ->doesntHave('order')
                ->where('type', 3)
                ->where(function ($query) use ($maxReminds, $treshold) {
                    $query
                        ->where(function ($query) use ($maxReminds, $treshold) {
                            $query
                                ->where('reminded', '<', $maxReminds)
                                ->where('updated_at', '<', Carbon::now()->subHours($treshold));
                        })
                        ->orWhere(function ($query) use ($maxReminds, $treshold) {
                            $query
                                ->where('reminded', '<', $maxReminds + 1)
                                ->where('updated_at', '<', Carbon::now()->subHours($treshold * 3));
                        });
                })
                ->orderBy('reminded', 'ASC')
                ->orderBy('id', 'ASC')
                ->groupBy('email')
                ->take($limit);
        })->all();
    }

    /**
     * @param $type
     * @param int $treshold
     * @param int $maxReminds
     * @param int $limit
     * @return mixed
     */
    public function forCron($type, $treshold = 1, $maxReminds = 1, $limit = 100)
    {
        if (!is_array($type)) {
            $type = [$type];
        }

        return $this->scopeQuery(function ($query) use ($type, $treshold, $maxReminds, $limit) {
            return $query
                ->doesntHave('order')
                ->whereIn('type', $type)
                ->where('reminded', '<', $maxReminds)
                ->where('updated_at', '<', Carbon::now()->subHours($treshold))
                ->orderBy('reminded', 'ASC')
                ->orderBy('id', 'ASC')
                ->groupBy('email')
                ->take($limit);
        })->all();
    }

    /**
     * @param array $attributes
     * @param array $values
     * @return mixed
     */
    public function createOrUpdate(array $attributes, array $values = [])
    {
        $model = $this->model->updateOrCreate($attributes, $values);
        $this->resetModel();

        if ($model->created_at == Carbon::now()) {
            event(new RepositoryEntityCreated($this, $model));
        } else {
            event(new RepositoryEntityUpdated($this, $model));
        }

        return $this->parserResult($model);
    }

    /**
     * @param $id
     * @param array $columns
     * @return mixed
     */
    public function find($id, $columns = array('*'))
    {
        $this->applyCriteria();
        $this->applyScope();
        $model = $this->model->find($id, $columns);
        $this->resetModel();

        return $this->parserResult($model);
    }

    /**
     * @param $searchQuery
     * @return mixed
     */
    public function search($searchQuery, $product)
    {
        return $this->scopeQuery(function ($query) use ($searchQuery, $product) {
            return $query
                ->where('product', '=', $product)
                ->where('name', '=', $searchQuery)
                ->orwhere('email', '=', $searchQuery)
                ->orwhere('phone', '=', $searchQuery);
        })->all();
    }

    public function searchExternals($id)
    {
        return $this->scopeQuery(function ($query) use ($id) {
            return $query
                ->whereNotNull('ext_client_id')
                ->where('ext_client_id', '=', $id)
//                ->whereNotNull('cc_id')
                ->orderBy('created_at');
        })->all();
    }

    public function currentMonth()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereNotNull('cc_id')
                ->where('created_at', '>', Carbon::now()->startOfMonth());
        })->all();
    }

    public function currentYear()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->where('created_at', '>', Carbon::now()->subYear());
//                ->where('created_at', '>', Carbon::now()->startOfYear());
        })->all();
    }

    public function searchFunCPAExternalsCurrentMonth()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereNotNull('ext_client_id')
                ->where('ext_client_id', '=', 4)
                ->whereNotNull('cc_id')
                ->where('cc_sell', '=', 1)
                ->where('created_at', '>', Carbon::now()->startOfMonth())
                ->where('created_at', '<', Carbon::tomorrow())
                ->orderBy('created_at');
        })->all();
    }

    public function searchPaginateExternals($per_page = 10, $product)
    {
        return $this->scopeQuery(function ($query) use ($product) {
            return $query
                ->whereNotNull('ext_client_id')
//                ->whereNotNull('cc_id')
                ->where('product', '=', $product)
                ->orderBy('created_at');
        })->paginate($per_page);
    }

    public function searchExternalsWithDateAndStatus($product, $range, $status, $client)
    {
        $data = explode(' - ', $range);

        return $this->scopeQuery(function ($query) use ($product, $data, $status, $client) {

            switch ($status) {
                case 'all' :
                    return $query
                        ->where('ext_client_id', '=', $client)
                        ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', $data[0])->startOfDay())
                        ->where('created_at', '<', Carbon::createFromFormat('d/m/Y', $data[1])->startOfDay())
                        ->where('product', '=', $product);
                    break;
                case 'sold' :
                    return $query
                        ->where('ext_client_id', '=', $client)
                        ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', $data[0])->startOfDay())
                        ->where('created_at', '<', Carbon::createFromFormat('d/m/Y', $data[1])->startOfDay())
                        ->where('product', '=', $product)
                        ->where('cc_sell', '=', 1);
                    break;
                case 'hold' :
                    return $query
                        ->where('ext_client_id', '=', $client)
                        ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', $data[0])->startOfDay())
                        ->where('created_at', '<', Carbon::createFromFormat('d/m/Y', $data[1])->startOfDay())
                        ->where('product', '=', $product)
                        ->where('cc_sell', '=', 0)
                        ->where('cc_reject', '=', 0)
                        ->where('cc_hold', '=', 1);
                    break;
                case 'reject' :
                    return $query
                        ->where('ext_client_id', '=', $client)
                        ->where('created_at', '>', Carbon::createFromFormat('d/m/Y', $data[0])->startOfDay())
                        ->where('created_at', '<', Carbon::createFromFormat('d/m/Y', $data[1])->startOfDay())
                        ->where('product', '=', $product)
                        ->where('cc_sell', '=', 0)
                        ->where('cc_hold', '=', 0)
                        ->where('cc_reject', '=', 1);
                    break;
            }

        });
    }

    public function searchAllExternals($product)
    {
        return $this->scopeQuery(function ($query) use ($product) {
            return $query
                ->whereNotNull('ext_client_id')
//                ->whereNotNull('cc_id')
                ->where('product', '=', $product);
        })->all();
    }

    public function searchSold()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->where('cc_sell', '=', 1);
        })->all();
    }

    public function searchNotSoldExternals($product)
    {
        return $this->scopeQuery(function ($query) use ($product) {
            return $query
                ->whereNotNull('ext_client_id')
//                ->whereNotNull('cc_id')
                ->where('cc_sell', '=', 0)
                ->where('product', '=', $product);
        })->all();
    }

    public function searchSoldExternals($product)
    {
        return $this->scopeQuery(function ($query) use ($product) {
            return $query
                ->whereNotNull('ext_client_id')
//                ->whereNotNull('cc_id')
                ->where('cc_sell', '=', 1)
                ->where('product', '=', $product);
        })->all();
    }

    public function searchOnHoldExternals($product, $tooOld = null)
    {
        if($tooOld){
            return $this->scopeQuery(function ($query) use ($product) {
                return $query
                    ->whereNotNull('ext_client_id')
//                    ->whereNotNull('cc_id')
                    ->where('cc_hold', '=', 1)
                    ->where('created_at', '<', Carbon::today()->subDay(7))
                    ->where('product', '=', $product);
            })->all();
        } else {
            return $this->scopeQuery(function ($query) use ($product) {
                return $query
                    ->whereNotNull('ext_client_id')
//                    ->whereNotNull('cc_id')
                    ->where('cc_hold', '=', 1)
                    ->where('product', '=', $product);
            })->all();
        }
    }

    public function searchRejectExternals($product)
    {
        return $this->scopeQuery(function ($query) use ($product) {
            return $query
                ->whereNotNull('ext_client_id')
//                ->whereNotNull('cc_id')
                ->where('cc_reject', '=', 1)
                ->where('product', '=', $product);
        })->all();
    }

    public function searchDuplicateExternals($product)
    {
        return \App\Contact::whereIn('id', function ($query) use ($product) {
            $query
                ->select('id')
                ->from('contacts')
                ->whereNotNull('ext_client_id')
//                ->whereNotNull('cc_id')
                ->groupBy('phone')
                ->havingRaw('count(*) > 1')
                ->where('product', '=', $product);
        })->get();
    }

    /**
     * @param $dt1
     * @param $dt2
     * @return mixed
     */
    public function getByDateRange($dt1, $dt2, $product, $externals = false)
    {
        return $this->scopeQuery(function ($query) use ($dt1, $dt2, $product, $externals) {

            if($externals){
                return $query
                    ->where('created_at', '>', $dt1)
                    ->where('created_at', '<', $dt2)
                    ->whereNotNull('ext_client_id')

//                    ->whereNotNull('cc_id')

//                   ->where('invalid_phone', '=', 0)

                    ->where('product', '=', $product);
            } else {
                return $query
                    ->where('created_at', '>', $dt1)
                    ->where('created_at', '<', $dt2)
                    ->where('product', '=', $product);
            }

        })->all();
    }

    public function getAllInDateRange($from, $to)
    {
        return $this->scopeQuery(function ($query) use ($from, $to) {
            return $query
                ->where('created_at', '>', $from)
                ->where('created_at', '<=', $to);
        })->all();
    }

    public function getAllGarciniaInDateRange($from, $to)
    {
        return $this->scopeQuery(function ($query) use ($from, $to) {
            return $query
                ->where('product', 'cambogia')
                ->where('call_center', 1)
                ->whereNotNull('ext_client_id')
                ->where('created_at', '>', $from)
                ->where('created_at', '<=', $to);
        })->all();
    }

    public function getAllByDateFrom($dt1)
    {
        return $this->scopeQuery(function ($query) use ($dt1) {

            return $query
                ->where('created_at', '>', $dt1);

        })->all();
    }
    
    public function getInvalidPhones()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->where('invalid_phone', '=', 1);
        })->paginate(20);
    }

    public function searchByPhone($phone)
    {
        $contacts = $this->scopeQuery(function ($query) use ($phone) {
            return $query
                ->where('phone', '=', $phone);
        })->all();

        $contact = $contacts->last();

        if(isset($contact)){
            $created = Carbon::parse($contact->created_at);
            $daysAgo = $created->diffInDays();

            if($daysAgo<2){
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function createOrUpdateByPhone(array $compare, array $newData)
    {
        $contacts = $this->scopeQuery(function ($query) use ($compare) {
            return $query
                ->where('phone', '=', $compare['phone']);
        })->all();

        $contact = $contacts->last();

        if(isset($contact)){

            if($contact->phone=='1234567890')
            {
                $model = $this->update($newData, $contact->id);
                return $model;
            }

            $created = Carbon::parse($contact->created_at);
            $daysAgo = $created->diffInDays();

            $model = null;
            
            if($daysAgo<2){
                dd('phone '.$contact->phone.' already exist.');
            } else {
                $model = $this->create($newData);
            }
        } else {
            $model = $this->create($newData);
        }

        return $model;
    }

    public function focusCallCenter($num, $popup)
    {
        return $this->with(['order'])->scopeQuery(function ($query) use ($num, $popup) {
            return $query
                ->where('call_center', 0)
//                ->where('popup', $popup)
                ->where('invalid_phone', 0)
                ->orderBy('created_at', 'ASC')
                ->take($num);
        })->all();
    }

    public function todays($product)
    {
        return $this->scopeQuery(function ($query) use ($product) {
            return $query
                ->where('created_at', '>=', Carbon::today())
                ->where('product', '=', $product);
        })->all()->count();
    }

    public function yesterdays($product)
    {
        return $this->scopeQuery(function ($query) use ($product)  {
            return $query
                ->where('created_at', '>=', Carbon::yesterday())
                ->where('created_at', '<=', Carbon::today())
                ->where('product', '=', $product);
        })->all()->count();
    }

    public function specificDate($product, $summaryRange, $external = false, $externalSold = false)
    {
        return $this->scopeQuery(function ($query) use ($product, $summaryRange, $external, $externalSold)  {

            $parts = explode(' - ', $summaryRange);
            $dt1 = Carbon::createFromFormat('d/m/Y h:i:s', $parts[0].' 00:00:00');
            $dt2 = Carbon::createFromFormat('d/m/Y h:i:s', $parts[1].' 00:00:00');

            if($external && $externalSold){
                return $query
                    ->where('created_at', '>', $dt1)
                    ->where('created_at', '<', $dt2)
                    ->where('product', '=', $product)
                    ->whereNotNull('ext_client_id')
//                    ->whereNotNull('cc_id')
                    ->where('cc_sell', '=', 1);
            }
            if($external){
                return $query
                    ->where('created_at', '>', $dt1)
                    ->where('created_at', '<', $dt2)
                    ->where('product', '=', $product)
                    ->whereNotNull('ext_client_id');
//                    ->whereNotNull('cc_id');
            }
            return $query
                ->where('created_at', '>', $dt1)
                ->where('created_at', '<', $dt2)
                ->where('product', '=', $product);
        })->all()->count();
    }

    public function allLeadsSentFromToday()
    {
        return $this->scopeQuery(function ($query)  {
            return $query
                ->where('call_center', '=', 1)
                ->where('created_at', '>', Carbon::yesterday())
                ->where('created_at', '<', Carbon::tomorrow());
        })->all()->count();
    }

    public function allLeadsSoldFromToday()
    {
        return $this->scopeQuery(function ($query)  {
            return $query
                ->where('cc_sell', '=', 1)
                ->where('created_at', '>', Carbon::yesterday())
                ->where('created_at', '<', Carbon::tomorrow());
        })->all()->count();
    }

    public function allTodays()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->where('created_at', '>=', Carbon::today()->addHours(14)->addMinutes(5));
        })->all();
    }
}
