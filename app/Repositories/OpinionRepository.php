<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OpinionRepository
 * @package namespace App\Repositories;
 */
interface OpinionRepository extends RepositoryInterface
{
    //
}
