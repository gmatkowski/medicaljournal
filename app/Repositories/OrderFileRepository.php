<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderFileRepository
 * @package namespace App\Repositories;
 */
interface OrderFileRepository extends RepositoryInterface
{
    //
}
