<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderAddressRepository
 * @package namespace App\Repositories;
 */
interface OrderAddressRepository extends RepositoryInterface
{
    //
}
