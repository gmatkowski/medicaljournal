<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface BankTransferUniqueRepository
 * @package namespace App\Repositories;
 */
interface BankTransferUniqueRepository extends RepositoryInterface
{
    //
}
