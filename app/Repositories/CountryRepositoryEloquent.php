<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CountryRepository;
use App\Country;
use Cache;
use Prettus\Repository\Events\RepositoryEntityCreated;
use Prettus\Repository\Events\RepositoryEntityUpdated;
use Carbon\Carbon;

/**
 * Class CountryRepositoryEloquent
 * @package namespace App\Repositories;
 */
class CountryRepositoryEloquent extends BaseRepository implements CountryRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Country::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function all($columns = array('*'))
    {
        return Cache::tags(['countries'])->rememberForever('countries', function () use ($columns)
        {
            return parent::all($columns);
        });
    }

    public function find($id, $columns = array('*'))
    {
        return Cache::tags(['countries', 'country::' . $id])->rememberForever('country::' . $id, function () use ($id, $columns)
        {
            return parent::find($id, $columns);
        });
    }
}
