<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrderReminderRepository;
use App\OrderReminder;

/**
 * Class OrderReminderRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrderReminderRepositoryEloquent extends BaseRepository implements OrderReminderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderReminder::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
