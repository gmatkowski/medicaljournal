<?php

namespace App\Repositories;

use App\OrderLanguage;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class LanguageRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrderLanguageRepositoryEloquent extends BaseRepository implements OrderLanguageRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderLanguage::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
