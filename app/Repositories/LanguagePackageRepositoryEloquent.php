<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LanguagePackageRepository;
use App\LanguagePackage;
use Cache;

/**
 * Class LanguagePackageRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LanguagePackageRepositoryEloquent extends BaseRepository implements LanguagePackageRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LanguagePackage::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function packages()
    {
        return Cache::tags(['languages::packages::list'])->rememberForever('packages', function ()
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->orderBy('id', 'ASC');
            })->all();
        });
    }
}
