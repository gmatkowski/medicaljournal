<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LanguageRenewLinkRepository;
use App\LanguageRenewLink;

/**
 * Class LanguageRenewLinkRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LanguageRenewLinkRepositoryEloquent extends BaseRepository implements LanguageRenewLinkRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LanguageRenewLink::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @param LanguageRenewLink $languageRenewLink
     * @return LanguageRenewLink
     */
    public function setInactive(LanguageRenewLink $languageRenewLink)
    {
        $languageRenewLink->active = 0;
        $languageRenewLink->save();

        return $languageRenewLink;
    }

    /**
     * @param int $page_page
     * @return mixed
     */
    public function listing($page_page = 50)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->where('active', 1)->orderBy('created_at', 'DESC')->orderBy('id', 'DESC');
        })->paginate($page_page);
    }
}
