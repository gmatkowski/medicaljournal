<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LanguageFileRepository;
use App\LanguageFile;

/**
 * Class LanguageFileRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LanguageFileRepositoryEloquent extends BaseRepository implements LanguageFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return LanguageFile::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
