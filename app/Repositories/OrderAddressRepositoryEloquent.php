<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\OrderAddress;

/**
 * Class OrderAddressRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrderAddressRepositoryEloquent extends BaseRepository implements OrderAddressRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderAddress::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function noDistrictId()
    {
        return $this->scopeQuery(function ($query) {
            return $query
                ->whereNotNull('postal_code')
                ->whereNotNull('address')
                ->whereNotNull('province')
                ->whereNotNull('city')
                ->whereNull('district_id')
                ->whereNotNull('subdistrict');
        })
        ->whereHas('order', function ($query) {
            return $query
                ->whereNotNull('old_district');
        })
        ->all();
    }
}
