<?php

namespace App\Repositories;

use App\PostCode;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class PostCodeRepositoryEloquent
 * @package namespace App\Repositories;
 */
class PostCodeRepositoryEloquent extends BaseRepository implements PostCodeRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return PostCode::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
