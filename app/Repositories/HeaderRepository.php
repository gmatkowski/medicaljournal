<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface HeaderRepository
 * @package namespace App\Repositories;
 */
interface HeaderRepository extends RepositoryInterface
{
    //
}
