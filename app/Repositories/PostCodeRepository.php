<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface PostCodeRepository
 * @package namespace App\Repositories;
 */
interface PostCodeRepository extends RepositoryInterface
{
    //
}
