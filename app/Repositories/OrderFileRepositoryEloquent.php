<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\OrderFileRepository;
use App\OrderFile;

/**
 * Class OrderFileRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrderFileRepositoryEloquent extends BaseRepository implements OrderFileRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderFile::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
