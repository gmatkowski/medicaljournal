<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface LanguagePackageRepository
 * @package namespace App\Repositories;
 */
interface LanguagePackageRepository extends RepositoryInterface
{
    //
}
