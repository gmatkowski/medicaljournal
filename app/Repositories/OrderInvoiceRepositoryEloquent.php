<?php

namespace App\Repositories;

use App\OrderInvoice;
use Carbon\Carbon;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;

/**
 * Class OrderInvoiceRepositoryEloquent
 * @package namespace App\Repositories;
 */
class OrderInvoiceRepositoryEloquent extends BaseRepository implements OrderInvoiceRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return OrderInvoice::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    public function getTo($dt1)
    {
        return $this->scopeQuery(function ($query) use ($dt1) {
            return $query
                ->where('created_at', '>', Carbon::now()->firstOfYear()->startOfDay())
                ->where('created_at', '<', $dt1);
        })->all();
    }

    public function salesReport($dt1, $dt2)
    {
        return $this->with(['order'])->scopeQuery(function ($query) use ($dt1, $dt2) {

            return $query
                ->where('created_at', '>', $dt1)
                ->where('created_at', '<', $dt2);
        })->all();
    }
}
