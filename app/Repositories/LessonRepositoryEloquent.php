<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\LessonRepository;
use App\Lesson;
use Cache;

/**
 * Class LessonRepositoryEloquent
 * @package namespace App\Repositories;
 */
class LessonRepositoryEloquent extends BaseRepository implements LessonRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Lesson::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }

    /**
     * @return mixed
     */
    public function lessons()
    {
        return Cache::tags(['lessons::list'])->rememberForever('lesson', function ()
        {
            return $this->scopeQuery(function ($query)
            {
                return $query->orderBy('level', 'ASC')->orderBy('id','ASC');
            })->all();
        });
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'ASC');
        })->paginate($per_page);
    }
}
