<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\RecordRepository;
use App\Record;
use Cache;

/**
 * Class RecordRepositoryEloquent
 * @package namespace App\Repositories;
 */
class RecordRepositoryEloquent extends BaseRepository implements RecordRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Record::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }

    /**
     * @return mixed
     */
    public function records()
    {
        return Cache::tags(['records::list'])->rememberForever('records', function ()
        {
            return $this->with(['language'])->scopeQuery(function ($query)
            {
                return $query->orderBy('id', 'DESC');
            })->all();
        });
    }

    /**
     * @param int $per_page
     * @return mixed
     */
    public function newest($per_page = 10)
    {
        return $this->with(['language'])->scopeQuery(function ($query)
        {
            return $query->orderBy('id', 'DESC');
        })->paginate($per_page);
    }
}
