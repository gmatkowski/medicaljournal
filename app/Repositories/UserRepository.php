<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepository
 * @package namespace AppRepositories;
 */
interface UserRepository extends RepositoryInterface
{
    //
}
