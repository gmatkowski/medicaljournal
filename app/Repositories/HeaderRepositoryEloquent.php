<?php

namespace App\Repositories;

use App\Repositories\Criteria\TranslationsCriteria;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\HeaderRepository;
use App\Header;
use Cache;

/**
 * Class HeaderRepositoryEloquent
 * @package namespace App\Repositories;
 */
class HeaderRepositoryEloquent extends BaseRepository implements HeaderRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Header::class;
    }

    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
        $this->pushCriteria(new TranslationsCriteria());
    }

    /**
     * @return mixed
     */
    public function getActive()
    {
        return Cache::tags(['headers::list'])->rememberForever('header::', function () {
            return $this->scopeQuery(function ($query) {
                return $query->where('active', '=', '1');
            })->all();
        });
    }
}
