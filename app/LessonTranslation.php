<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslation
 * @package App
 */
class LessonTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'lesson_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['name', 'description'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Lesson');
    }

}
