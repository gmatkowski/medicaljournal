<?php
namespace App\Mail;

use App\Order;
use App\Contact;
use App\Repositories\LanguageRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Mail\Mailer;

class OrderMailer
{
    private $mail;
    private $subject;
    private $languageRepo;

    public function __construct(Mailer $mail, LanguageRepository $languageRepository) {
        $this->mail = $mail;
        $this->languageRepo = $languageRepository;
    }

    public function contactMade(Contact $contact)
    {
        if($contact->email)
        {
            switch ($contact->product) {
                case 'cambogia':
                    $languageId = 1;
                    break;
                case 'blackmask':
                    $languageId = 2;
                    break;
                case 'hallupro':
                    $languageId = 3;
                    break;
                case 'waist':
                    $languageId = 4;
                    break;
                case 'detoclean':
                    $languageId = 5;
                    break;
                case 'coffee':
                    $languageId = 6;
                    break;
                case 'joint':
                    $languageId = 7;
                    break;
                case 'posture':
                    $languageId = 8;
                    break;
                case 'bra':
                    $languageId = 9;
                    break;
            }
            if($languageId)
            {
                $language = $this->languageRepo->find($languageId);

                if($language)
                {
                    $this->subject = 'Order Confirmation';

                    $this->send(
                        $contact, __FUNCTION__, [
                            'productName' => $language->name
                        ]
                    );
                }
            }
        }
    }

    public function codOrderMade(Order $order)
    {
        if($order->email) {
            $this->send($order, __FUNCTION__, $this->getMadeData($order));
        }
    }

    public function codOrderSent(Order $order)
    {
        if($order->email) {
            $this->send($order, __FUNCTION__, $this->getSentData($order));
        }
    }

    public function codOrderDelivered(Order $order)
    {
        if($order->email) {
            $this->send($order, __FUNCTION__, $this->getDeliveredData($order));
        }
    }

    public function noncodOrderMade(Order $order)
    {
        if($order->email) {
            $data = $this->getMadeData($order);
            $data['accounts'] = [
                [
                    'method' => 'BCA',
                    'account_number' => '6540 300 737'
                ],
                [
                    'method' => 'Mandiri',
                    'account_number' => '121 000 687 4865'
                ]
            ];
        }

        $this->send($order, __FUNCTION__, $data);
    }

    public function noncodOrderSent(Order $order)
    {
        if($order->email) {
            $this->send($order, __FUNCTION__, $this->getSentData($order));
        }
    }

    public function noncodOrderDelivered(Order $order)
    {
        if($order->email) {
            $this->send($order, __FUNCTION__, $this->getDeliveredData($order));
        }
    }

    private function getMadeData(Order $order)
    {
        $this->subject = 'Konfirmasi Pemesanan ('.$order->languages_names.') ('.$order->id.')';

        return [
            'customer_first_name' => $order->first_name,
            'customer_last_name' => $order->last_name,
            'order_id' => $order->ref,
            'day_and_date' => $order->created_at->copy()->format('d M Y'),
            'estimated_time_delivery' => $this->getEstimatedTime($order),
            'courier' => $order->address->subDistrict->postCode->shipping_provider_name,
            'customer_address' => $order->address->address,
            'customer_phone_number' => $order->phone,
            'customer_email' => $order->email,
            'orders' => $order->languages,
            'inbound_number' => '08111221588',
            'our_email' => 'contact@medical-jurnal.com',
            'whatsapp_number' => '0813 8333 9294',
            'subtotal' => $order->price,
            'kodeunik' => $order->unique_number,
            'total' => $order->price+$order->unique_number
        ];
    }

    private function getSentData(Order $order)
    {
        $this->subject = 'Pesanan ('.$order->languages_names.') dengan ('.$order->id.') Telah DIkirim!';

        return [
            'customer_first_name' => $order->first_name,
            'customer_last_name' => $order->last_name,
            'order_id' => $order->ref,
            'estimated_time_delivery' => $this->getEstimatedTime($order),
            'courier' => $order->address->subDistrict->postCode->shipping_provider_name,
            'customer_address' => $order->address->address,
            'customer_phone_number' => $order->phone,
            'customer_email' => $order->email,
            'orders' => $order->languages,
            'inbound_number' => '08111221588',
            'our_email' => 'contact@medical-jurnal.com',
            'whatsapp_number' => '0813 8333 9294',
            'subtotal' => $order->price,
            'kodeunik' => $order->unique_number,
            'total' => $order->price+$order->unique_number
        ];
    }

    private function getDeliveredData(Order $order)
    {
        $this->subject = 'Pesanan ('.$order->languages_names.') dengan ('.$order->id.') Telah Diterima';

        return [
            'customer_first_name' => $order->first_name,
            'customer_last_name' => $order->last_name,
            'our_website' => 'http://promos-seru.com',
            'order_id' => $order->ref,
            'orders' => $order->languages,
            'inbound_number' => '08111221588',
            'our_email' => 'contact@medical-jurnal.com',
            'whatsapp_number' => '0813 8333 9294',
            'subtotal' => $order->price,
            'kodeunik' => $order->unique_number,
            'total' => $order->price+$order->unique_number
        ];
    }

    private function getEstimatedTime(Order $order)
    {
        if($order->address->subDistrict->leadtime) {
            $exploded = explode('-', $order->address->subDistrict->leadtime);
            $from = $order->created_at->copy()->addDays($exploded[0]);
            $to = $order->created_at->copy()->addDays($exploded[1]);
        }
        else
        {
            $from = $order->created_at->copy()->addDays(6);
            $to = $order->created_at->copy()->addDays(14);
        }

        $weeekendsDays = $from->diffInDaysFiltered(function(Carbon $date) {
            return $date->isWeekend();
        }, $to);

        $to->addDays($weeekendsDays);

        if($from->isSaturday())
        {
            $from->addDays(2);
        }

        if($from->isSunday())
        {
            $from->addDay();
        }

        if($to->isSaturday())
        {
            $to->addDays(2);
        }

        if($to->isSunday())
        {
            $to->addDay();
        }

        return $from->format('d M Y').'-'.$to->format('d M Y');
    }

    private function send($object, $template, $data)
    {
        if(app()->environment() == 'production' && $object->email)
        {
            $this->mail->send('email.used.'.$template, $data, function ($m) use ($object) {
                $m->to($object->email)->subject($this->subject);
            });
        }
    }
}