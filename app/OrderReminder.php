<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class OrderReminder
 * @package App
 */
class OrderReminder extends Model implements Transformable {

    /**
     * @var string
     */
    protected $table = 'order_reminders';

    use TransformableTrait;

    /**
     * @var array
     */
    protected $fillable = [
        'status', 'repeated', 'type'
    ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
