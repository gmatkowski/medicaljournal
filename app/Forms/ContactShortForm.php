<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ContactShortForm extends Form {

    public function buildForm()
    {
        $this
            ->add('first_name', 'text', [
                'rules' => 'required|min:3',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.first.name.short')
                ]
            ])
            ->add('last_name', 'text', [
                'rules' => 'required|min:3',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.last.name.short')
                ]
            ])
            ->add('email', 'email', [
                'rules' => 'required|email',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.email')
                ]
            ])
            ->add('phone', 'text', [
                'rules' => 'required|min:9',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.phone.short'),
                    'pattern'     => '[0-9]*',
                ]
            ])
            ->add('type','hidden',['default_value' => 2])
            ->add('submit', 'submit', ['label' => trans('contact.submit.short')]);
    }
}