<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class GoToPageForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('product', 'hidden')
            ->add('page', 'text', [
                'rules' => 'required|max:7',
                'attr'  => [
                    'class' => '',
                    'size' => 6
                ]
            ])
            ->add('submit', 'submit', ['label' => 'Go to Page', 'attr' => ['class' => 'btn btn-xs btn-primary']]);
    }
}