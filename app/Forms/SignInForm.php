<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class SignInForm extends Form {

    public function buildForm()
    {
        $this
            ->add('email', 'email', ['rules' => 'required|email', 'label' => false, 'attr' => ['class' => 'form-control', 'required' => 'true', 'placeholder' => trans('translations.email')]])
            ->add('password', 'password', ['rules' => 'required|min:3', 'label' => false, 'attr' => ['class' => 'form-control', 'required' => 'true', 'placeholder' => trans('translations.password')]])
            ->add('submit', 'submit', ['label' => trans('translations.signin'), 'attr' => ['required' => 'true', 'class' => 'btn btn-default submit']]);
    }
}