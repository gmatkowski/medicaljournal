<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class DataForm extends Form {

    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:3',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('try.name')
                ]
            ])
            ->add('email', 'email', [
                'rules' => 'required|email',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('try.email')
                ]
            ])
            ->add('phone', 'text', [
                'rules' => 'required|min:9',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('try.phone'),
                    'pattern'     => '[0-9]*',
                ]
            ])
            ->add('age', 'text', [
                'rules' => 'required|min:1',
                'attr' => [
                    'required' => 'required',
                    'placeholder' => trans('try.age')
                ]
            ])
            ->add('type','hidden',['default_value' => 3])
            ->add('submit', 'submit', ['label' => trans('try.submit')]);
    }
}