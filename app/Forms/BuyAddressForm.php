<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class BuyAddressForm extends Form {

    public function buildForm()
    {
        $countries = app('App\Repositories\CountryRepository');
        $countriesList = $countries->all();

        $items = $countriesList->first()->provinces->sortBy('name')->lists('name', 'id')->toArray();
        $items[0] = trans('languages.buy.province');

        $required = $this->getData('isPost') ? 'required|' : '';

        $this
            ->add('nocod', 'hidden', ['default_value' => 0])

            ->add('country_id', 'select', [
                'label' => trans('languages.buy.country.label'),
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'rules'    => $required . 'min:1',
                'choices'  => $countriesList->lists('name', 'id')->toArray(),
                'selected' => $countriesList->first()->id,
                'attr'     => [
                    'disabled' => true,
                    'class' => 'selectable form-control',
                    'data-name' => 'country',
                ]
            ])
            ->add('province_id', 'select', [
                'label' => trans('languages.buy.province.label'),
                'rules'       => $required . 'min:1',
                'choices'     => $items,
                'attr'        => [
                    'class' => 'selectable form-control',
                    'data-name' => 'province',
                ],
                'empty_value' => trans('languages.buy.province')
            ])
            ->add('city_id', 'select', [
                'label' => trans('languages.buy.city.label'),
                'rules'       => $required . 'min:1',
                'choices'     => [],
                'attr'        => [
                    'disabled' => true,
                    'class' => 'selectable form-control',
                    'data-name' => 'city',
                ],
                'empty_value' => trans('languages.buy.city')
            ])
            ->add('district_id', 'select', [
                'label' => trans('languages.buy.district.label'),
                'rules'       => $required . 'min:1',
                'choices'     => [],
                'attr'        => [
                    'disabled' => true,
                    'class' => 'selectable last form-control',
                    'data-name' => 'district',
                ],
                'empty_value' => trans('languages.buy.district')
            ])
            ->add('subdistrict', 'text', [
                'label' => trans('languages.buy.subdistrict.label'),
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr'  => [
                    'placeholder' => trans('languages.buy.subdistrict')
                ]
            ])

            ->add('address', 'text', [
                'label' => trans('languages.buy.address.label'),
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr'  => [
                    'placeholder' => trans('languages.buy.address'),

                ]
            ])
            ->add('postal_code', 'text', [
                'label' => trans('languages.buy.postal.code.label'),
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr'  => [
                    'placeholder' => '10150',
                ]
            ])
            ->add('comment', 'textarea', [
                'label' => trans('languages.buy.comment.label'),
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'attr'  => [
                    'rows' => 3,
                ]
            ]);
    }
}
