<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class ContactForm extends Form {

    public function buildForm()
    {
        $this
            ->add('name', 'text', [
                'rules' => 'required|min:3',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.name')
                ]
            ])
            ->add('email', 'email', [
                'rules' => 'required|email',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.email')
                ]
            ])
            ->add('phone', 'text', [
                'rules' => 'required|min:9',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.phone'),
                    'pattern'     => '[0-9]*',
                ]
            ])
            ->add('message', 'textarea', [
                'rules' => 'required|min:10',
                'attr'  => [
                    'required'    => 'true',
                    'placeholder' => trans('contact.message')
                ]
            ])
            ->add('type','hidden',['default_value' => 1])
            ->add('submit', 'submit', ['label' => trans('contact.submit')]);
    }
}