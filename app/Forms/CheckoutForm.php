<?php namespace App\Forms;

use App\Helpers\StrHelper;
use Kris\LaravelFormBuilder\Form;

class CheckoutForm extends Form
{
    public function buildForm()
    {
        $countryRepository = app('App\Repositories\CountryRepository');
        $countriesList = $countryRepository->all();
        $cityRepository = app('App\Repositories\CityRepository');
        $citiesList = $cityRepository->all();

        $c = app('Illuminate\Support\Facades\Config');

        $this
            ->add('email', 'email', [
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('contact.email')
                ]
            ])
            ->add('first_name', 'text', [
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('contact.first.name')
                ]
            ])
            ->add('surname', 'text', [
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('contact.last.name')
                ]
            ])
            ->add('address', 'text', [
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('contact.address')
                ]
            ])
            ->add('district', 'text', [
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('languages.buy.district.label')
                ]
            ])
            ->add('subdistrict', 'text', [
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('languages.buy.subdistrict')
                ]
            ])
            ->add('postcode', 'text', [
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('contact.postcode')
                ]
            ])
            ->add('city', 'select', [
                'rules' => 'required',
                'choices' => $citiesList->lists('name', 'id')->toArray(),
                'attr'  => [
                    'placeholder' => trans('contact.city')
                ]
            ])
            ->add('country', 'select', [
                'rules' => 'required',
                'choices'  => $countriesList->lists('name', 'id')->toArray(),
                'selected' => $countriesList->first()->id,
                'attr'  => [
                    'placeholder' => trans('contact.country')
                ]
            ])

//            ->add('terms', 'checkbox', [
//                'rules' => 'required'
//            ])
            ->add('price', 'hidden', ['attr' => ['class' => 'priceTotal'], 'default_value' => StrHelper::dotsInPrice($c::get('veritrans.price'))])
            ->add('qty', 'hidden', ['attr' => ['class' => 'qty'], 'default_value' => 3]);
    }
}