<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class MandiriForm extends Form
{

    public function buildForm()
    {
        $this
            ->add('mandiri_card_number', 'text', [
                'rules' => 'required',
                'attr' => [
                    'required' => 'required',
                    'placeholder' => trans('checkout.mandiri.card.number'),
                    'id' => 'mandiri_card_number',
                ]
            ])
            ->add('mandiri_input1' , 'text', [
                'label' => 'Input 1',
                'attr' => [
                    'readonly' => 'readonly',
                    'id' => 'mandiri_input1',
                ]
            ])
            ->add('mandiri_input2' , 'text', [
                'label' => 'Input 2',
                'attr' => [
                    'readonly' => 'readonly',
                    'id' => 'mandiri_input2',
                ]
            ])
            ->add('mandiri_input3' , 'text', [
                'label' => 'Input 3',
                'attr' => [
                    'readonly' => 'readonly',
                    'id' => 'mandiri_input3',
                ]
            ])
            ->add('mandiri_token', 'text', [
                'rules' => 'required|size:6',
                'attr' => [
                    'required' => 'required',
                    'placeholder' => trans('checkout.mandiri.token')
                ]
            ]);
    }
}
