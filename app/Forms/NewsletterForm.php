<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class NewsletterForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('email', 'text', [
                'label' => trans('languages.buy.email.label'),
                'rules' => 'required|email',
                'attr'  => [
                    'placeholder' => 'radene@example.com',
                    'pattern'     => '[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}',
                    'data-validation' => 'required',
                    'data-validation-error-msg' => 'Wajib diisi'
                ]
            ])
            ->add('terms', 'checkbox', [
                'rules' => 'required',
                'checked' => 'checked'
            ])
            ->add('submit', 'submit', ['label' => trans('languages.buy.submit'), 'attr' => ['id' => 'submit-button']]);
    }
}