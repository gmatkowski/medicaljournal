<?php namespace App\Forms\Admin;

use App\Lesson;
use Kris\LaravelFormBuilder\Form;

class StatForm extends Form {

    public function buildForm()
    {
        $this
            ->add('icon', 'file', ['label' => trans('admin.stats.icon')])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}