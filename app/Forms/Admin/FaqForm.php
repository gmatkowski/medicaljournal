<?php namespace App\Forms\Admin;

use App\Lesson;
use Kris\LaravelFormBuilder\Form;

class FaqForm extends Form {

    public function buildForm()
    {
        $this
            ->add('icon', 'file', ['label' => trans('admin.common.icon')])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']])
            ->add('type', 'select', [
                'label' => trans('admin.common.type'),
                'choices' => ['1' => '1 (/bahasa)', '2' => '2 (/keluar)'],
                'required' => 'true'
            ]);
    }
}