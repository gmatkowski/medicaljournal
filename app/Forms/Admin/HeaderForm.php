<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

/**
 * Class HeaderForm
 * @package App\Forms\Admin
 */
class HeaderForm extends Form
{
    /**
     *
     */
    public function buildForm()
    {
        $this
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}
