<?php

namespace App\Forms\Admin\Consultant;

use Kris\LaravelFormBuilder\Form;

class OrderForm extends Form {

    public function buildForm()
    {
        $countryRepository = app('App\Repositories\CountryRepository');
        $countriesList = $countryRepository->all();
        $items = $countriesList->first()->provinces->sortBy('name')->lists('name', 'id')->toArray();

        $this
            ->add('first_name', 'text', ['rules' => 'required|min:2'])
            ->add('last_name', 'text', ['rules' => 'required|min:2'])
            ->add('email', 'text', ['rules' => 'email'])
            ->add('phone', 'text', ['rules' => 'required'])
            ->add('age', 'text')
            ->add('sex', 'select', [
                'choices' => [
                    'female' => 'female',
                    'male' => 'male',
                    'other' => 'other',
                ],
                'empty_value' => '= select ='
            ])
            ->add('notes', 'textarea', ['attr' => ['rows' => '3']])
            ->add('other_lang', 'text', ['label' => 'Other languages'])
            ->add('postal_code', 'text', [
                'rules' => 'required|min:3',
                'attr'  => [
                    'required' => 'required',
                    'label'    => 'Postal code'
                ]
            ])
            ->add('address', 'text', [
                'rules' => 'required|min:3',
                'attr'  => [
                    'required' => 'required',
                    'label'    => 'Address'
                ]
            ])
            ->add('country_id', 'select', [
                'rules'    => 'required|min:1',
                'choices'  => $countriesList->lists('name', 'id')->toArray(),
                'selected' => $countriesList->first()->id,
                'label'       => 'Country',
                'attr'     => [
                    'placeholder' => '-- Choose --',
                ]
            ])
            ->add('province_id', 'select', [
                'rules'   => 'required|min:1',
                'choices' => $items,
                'label'       => 'Province',
                'attr'    => [
                    'required'    => 'required',
                    'placeholder' => '-- Choose --',
                ],

            ])
            ->add('city_id', 'select', [
                'rules'   => 'required|min:1',
                'choices' => [],
                'label'       => 'City',
                'attr'    => [
                    'disabled'    => true,
                    'required'    => 'required',
                    'placeholder' => '-- Choose --',
                ]
            ])
            ->add('district_id', 'select', [
                'rules'   => 'required|min:1',
                'choices' => [],
                'label'       => 'District',
                'attr'    => [
                    'disabled'    => true,
                    'required'    => 'required',
                    'placeholder' => '-- Choose --',
                ]
            ])
            ->add('subdistrict', 'text', ['rules' => 'required|min:2'])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => 'Save order', 'attr' => ['required' => 'true']]);
    }
}
