<?php namespace App\Forms\Admin;

use App\Lesson;
use Kris\LaravelFormBuilder\Form;

class LessonForm extends Form {

    public function buildForm()
    {
        $this
            ->add('level', 'select', ['label' => trans('admin.lessons.level'), 'choices' => trans('lessons.levels'), 'attr' => ['required' => 'true']])
            ->add('icon', 'file', ['label' => trans('admin.common.icon')])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}