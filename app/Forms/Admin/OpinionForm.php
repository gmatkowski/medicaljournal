<?php namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class OpinionForm extends Form {

    public function buildForm()
    {
        $this
            ->add('main', 'select', ['label' => trans('admin.opinions.homepage'), 'choices' => [0 => trans('admin.common.no'), 1 => trans('admin.common.yes')], 'attr' => ['required' => 'true']])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}