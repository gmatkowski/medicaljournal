<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class SearchForm extends Form
{
    public function buildForm()
    {
        $this->formOptions['class'] = 'form-horizontal form-label-left';
        $this
            ->add('product', 'hidden')
            ->add('query', 'text', [
                'attr'  => ['class' => 'form-control has-feedback-left'],
                'rules' => 'required|min:3',
                'label' => false
            ])
            ->add('submit', 'submit', ['label' => 'Search', 'attr' => ['class' => 'btn btn-success pull-left']]);
    }
}
