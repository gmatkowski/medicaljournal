<?php namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class PageForm extends Form {

    public function buildForm()
    {
        $this
            ->add('id', 'hidden')
            ->add('icon', 'file', ['label' => trans('admin.common.image')])
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}