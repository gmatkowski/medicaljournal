<?php namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class JobForm extends Form {

    public function buildForm()
    {
        $this
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}