<?php namespace App\Forms\Admin;

use Illuminate\Support\Facades\App;
use Kris\LaravelFormBuilder\Form;

class RecordForm extends Form {

    public function buildForm()
    {
        $languages = App::make('App\Repositories\LanguageRepository');

        $options = $languages->all()->lists('name_short', 'id')->all();

        $this
            ->add('language_id', 'select', ['label' => trans('admin.common.language'), 'choices' => $options, 'attr' => ['required' => 'true']])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}