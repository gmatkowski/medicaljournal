<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class StockForm extends Form {

    public function buildForm()
    {
        $this
            ->add('in', 'text')
            ->add('out', 'text')

            ->add('productSymbol', 'hidden')
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => 'Save']);
    }
}
