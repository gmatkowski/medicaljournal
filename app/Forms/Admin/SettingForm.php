<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class SettingForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('available_free', 'text', ['rules' => 'required', 'label' => trans('admin.settings.left')])
            ->add('available_gone', 'text', ['rules' => 'required', 'label' => trans('admin.settings.sold')])
            ->add('available_treshold_from', 'text', ['rules' => 'required', 'label' => trans('admin.settings.interval')])
            ->add('available_treshold_to', 'text', ['rules' => 'required', 'label' => trans('admin.settings.threshold')])
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}
