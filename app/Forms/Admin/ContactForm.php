<?php namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class ContactForm extends Form {

    public function buildForm()
    {
        $languages = app('App\Repositories\LanguageRepository');

        $this
            ->add('name', 'text', [
                'rules' => 'required|min:3',
                'attr'  => [
                    'required' => 'true'
                ]
            ])
            ->add('email', 'email', [
                'rules' => 'required|email',
                'attr'  => [
                    'required' => 'true'
                ]
            ])
            ->add('phone', 'text', [
                'rules' => 'required|min:9',
                'attr'  => [
                    'required' => 'true'
                ]
            ])
            ->add('message', 'textarea')
            ->add('language_id', 'select', $languages->newest()->lists('name', 'id'))
            ->add('submit', 'submit', ['label' => trans('contact.submit')]);
    }
}