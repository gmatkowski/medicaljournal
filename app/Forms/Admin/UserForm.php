<?php namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;
use App\Repositories\RoleRepository;

class UserForm extends Form {

    public function buildForm()
    {
        $roles = app(RoleRepository::class);
        $roleId = isset($this->getModel()['role']) ? $this->getModel()['role']->getAttributeValue('id') : null;
        $this
            ->add('role', 'select', ['rules' => 'required', 'label' => trans('translations.role'), 'choices' => $roles->all()->lists('display_name','id')->toArray(), 'selected' => $roleId])
            ->add('name', 'text', ['rules' => 'required|min:3', 'label' => trans('translations.name'), 'attr' => ['required' => 'true']])
            ->add('email', 'email', ['rules' => 'required|email|unique:users', 'label' => trans('translations.email'), 'attr' => ['required' => 'true']])
            ->add('signature', 'text', ['label' => trans('admin.users.signature')])
            ->add('password', 'password', ['rules' => 'required|min:5|confirmed', 'label' => trans('translations.password')])
            ->add('password_confirmation', 'password', ['rules' => 'required|min:5', 'label' => trans('translations.password_confirmation')])
            ->add('show_stock', 'checkbox', ['label' => trans('translations.showStock')])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('translations.send'), 'attr' => ['required' => 'true']]);
    }
}