<?php namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class LanguageFileForm extends Form {

    public function buildForm()
    {
        $this
            ->add('file', 'file', ['label' => trans('admin.products.file'), 'rules' => 'required'])
            ->add('id', 'hidden')
            ->add('language_id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}