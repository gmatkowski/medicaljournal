<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class PostForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('icon', 'file', ['label' => trans('admin.blog.image')])
            ->add('id', 'hidden')
            ->add('author', 'text', ['label' => trans('admin.blog.author')])
            ->add('submit', 'submit', ['label' => trans('admin.blog.save'), 'attr' => ['required' => 'true']]);
    }
}
