<?php

namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class OrderForm extends Form {

    public function buildForm()
    {
        $postCodeRepository = app('App\Repositories\PostCodeRepository');
        $postCodesList = $postCodeRepository->all();

        $items = $postCodesList ? $postCodesList->lists('name', 'id')->toArray() : [];

        $countryRepository = app('App\Repositories\CountryRepository');
        $countriesList = $countryRepository->all();

        $this
            ->add('first_name', 'text', ['rules' => 'required|min:2|max:60'])
            ->add('last_name', 'text', ['rules' => 'required|min:2|max:60'])
            ->add('email', 'text', ['rules' => 'email'])
            ->add('phone', 'text', [
                'rules' => 'required|max:30'
            ])
            ->add('age', 'text')
            ->add('sex', 'select', [
                'choices' => [
                    'female' => 'female',
                    'male' => 'male',
                    'other' => 'other',
                ],
                'empty_value' => '= select ='
            ])
            ->add('size', 'text', [
                'attr'  => [
                    'style' => 'width:225px'
                ]
            ])
            ->add('notes', 'textarea', ['attr' => ['rows' => '3']])
            ->add('other_lang', 'text', ['label' => 'Other languages'])
            ->add('address', 'text', [
                'rules' => 'required|min:3|max:200',
                'attr'  => [
                    'required' => 'required',
                    'label'    => 'Address'
                ]
            ])
            ->add('postal_code', 'select', [
                'rules' => 'required',
                'choices'  => $items,
                'label'    => 'Postal Code',
                'attr'  => [
                    'required' => 'required',
                    'placeholder' => '-- Choose --',
                ]
            ])
            ->add('subdistrict', 'select', [
                'rules' => 'required',
                'choices' => [],
                'label'       => 'SubDistrict',
                'attr'    => [
                    'required'    => 'required',
                    'placeholder' => '-- Choose --',
                ]
            ])

            ->add('hiddenDistrict', 'hidden', ['attr' => ['id' => 'hidden0']])
            ->add('district_id', 'select', [
                'rules'   => 'required',
                'choices' => [],
                'label'       => 'District',
                'attr'    => [
                    'required'    => 'required',
                    'placeholder' => '-- Choose --',
                ]
            ])
            ->add('hiddenCity', 'hidden', ['attr' => ['id' => 'hidden1']])
            ->add('city_id', 'select', [
                'rules'   => 'required',
                'choices' => [],
                'label'       => 'City',
                'attr'    => [
                    'required'    => 'required',
                    'placeholder' => '-- Choose --',
                ]
            ])
            ->add('hiddenProvince', 'hidden', ['attr' => ['id' => 'hidden2']])
            ->add('province_id', 'select', [
                'rules'   => 'required',
                'choices' => [],
                'label'       => 'Province',
                'attr'    => [
                    'required'    => 'required',
                    'placeholder' => '-- Choose --',
                ],
            ])

            ->add('country_id', 'text', [
                'label'    => 'Country',
                'value'    => $countriesList->first()->name,
                'attr'     => [
                    'disabled' => true,
                ]
            ])
            ->add('unique_number', 'text')
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => 'Save order', 'attr' => ['required' => 'true']]);
    }
}
