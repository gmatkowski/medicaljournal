<?php namespace App\Forms\Admin;

use Kris\LaravelFormBuilder\Form;

class LanguageForm extends Form {

    public function buildForm()
    {
        $this
            ->add('is_active', 'select', ['label' => trans('admin.common.active'), 'choices' => [0 => trans('admin.common.no'), 1 => trans('admin.common.yes')], 'attr' => ['required' => 'true']])
            ->add('amount', 'text', ['rules' => 'required', 'label' => trans('admin.common.stock')])
            ->add('min_amount', 'text', ['rules' => 'required', 'label' => trans('admin.common.minStock')])
            ->add('symbol', 'text', ['rules' => 'required', 'label' => trans('admin.products.symbol')])
            ->add('sample', 'file', ['label' => trans('admin.products.trial')])
            ->add('image', 'file', ['label' => trans('admin.common.image')])
            ->add('image_10days', 'file', ['label' => trans('admin.products.image_10days')])
            ->add('icon', 'file', ['label' => trans('admin.products.icon')])
            ->add('shipment_item_id', 'text', ['label' => trans('admin.products.prodid')])
            ->add('id', 'hidden')
            ->add('submit', 'submit', ['label' => trans('admin.common.save'), 'attr' => ['required' => 'true']]);
    }
}