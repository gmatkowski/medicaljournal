<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;

class BuyForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('first_name', 'text', [
                'label' => trans('languages.buy.first.name.label'),
                'label_attr' => ['class' => 'col-md-3 control-label'],
                'rules' => 'required',
                'attr'  => [
                    'placeholder' => trans('languages.buy.first.name'),
                    'data-validation-error-msg' => 'Wajib diisi'
                ]
            ])
//            ->add('last_name', 'text', [
//                'label' => trans('languages.buy.last.name.label'),
//                'label_attr' => ['class' => 'col-md-3 control-label'],
//                'attr'  => [
//                    'placeholder' => trans('languages.buy.last.name'),
//                ]
//            ])
            ->add('email', 'text', [
                'label' => trans('languages.buy.email.label'),
                'rules' => 'required|email',
                'attr'  => [
                    'placeholder' => 'radene@example.com',
                    'pattern'     => '[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}',
                    'data-validation' => 'required',
                    'data-validation-error-msg' => 'Wajib diisi'
                ]
            ])
            ->add('phone', 'text', [
                'label' => trans('languages.buy.phone.label'),
                'rules' => 'required|min:7',
                'attr'  => [
                    'placeholder' => '0217364234'
                ]
            ])
            ->add('productId', 'hidden', ['attr' => ['id' => 'productId']])
            ->add('params', 'hidden')
            ->add('submit', 'submit', ['label' => trans('languages.buy.submit'), 'attr' => ['id' => 'submit-button']]);
    }
}