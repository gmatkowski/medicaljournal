<?php namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Config;
use LaravelLocalization;

class PaymentForm extends Form {

    public function buildForm()
    {
        $md5sum = [];

        if ($this->getModel())
        {
            $md5sum = ['default_value' => md5(Config::get('transferuj.id') . $this->getModel()->kwota . $this->getModel()->crc . Config::get('transferuj.security_code'))];
        }

        $this
            ->add('imie', 'hidden')
            ->add('nazwisko', 'hidden')
            ->add('telefon', 'hidden')
            ->add('email', 'hidden')
            ->add('id', 'hidden', ['default_value' => Config::get('transferuj.id')])
            ->add('kwota', 'hidden')
            ->add('crc', 'hidden')
            ->add('opis', 'hidden')
            ->add('kanal', 'hidden')
            ->add('md5sum', 'hidden', $md5sum)
            ->add('wyn_url', 'hidden', ['default_value' => route('transfer.order.status')])
            ->add('pow_url', 'hidden', ['default_value' => route('page.index', ['result' => 'ok'])])
            ->add('pow_url_blad', 'hidden', ['default_value' => route('page.index', ['result' => 'error'])])
            ->add('akceptuje_regulamin', 'hidden', ['default_value' => 1])
            ->add('jezyk', 'hidden', ['default_value' => LaravelLocalization::getCurrentLocale()])
            ->add('submit', 'submit');;

    }
}