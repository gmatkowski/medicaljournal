<?php

namespace App\Forms;

use Carbon\Carbon;
use Kris\LaravelFormBuilder\Form;

class CreditCardForm extends Form {

    public function buildForm()
    {
        $current_year = Carbon::now()->format('Y');
        $max_year = Carbon::now()->addYears(10)->format('Y');
        $months = range(1, 12);
        $months = array_map(function($item) {
            return str_pad($item, 2, '0', STR_PAD_LEFT);
        }, $months);

        $this
            ->add('cc_number', 'text', ['attr' => ['size' => 20, 'required' => 'required', 'id' => 'card-number', 'placeholder' => trans('checkout.cc.number')]])
            ->add('cc_owner', 'text', ['attr' => ['required' => 'required', 'id' => 'card-owner', 'placeholder' => trans('checkout.cc.owner.name')]])
            ->add('cc_expiry_month', 'select', [
                'choices' => array_combine($months, $months),
                'attr'    => [
                    'required'    => 'required',
                    'id'          => 'card-expiry-month',
                    'placeholder' => trans('checkout.cc.month'),
                ]
            ])
            ->add('cc_expiry_year', 'select', [
                'choices' => array_combine(range($current_year, $max_year), range($current_year, $max_year)),
                'attr'    => [
                    'required'    => 'required',
                    'id'          => 'card-expiry-year',
                    'placeholder' => trans('checkout.cc.year'),
                ]
            ])
            ->add('cc_cvv', 'text', ['attr' => ['size' => 3, 'required' => 'required', 'id' => 'card-cvv', 'placeholder' => trans('checkout.cc.ccv')]])
            ->add('token_id', 'hidden', ['attr' => ['id' => 'token-id']]);
    }
}
