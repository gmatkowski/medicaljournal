<?php

namespace App\Forms;

use Kris\LaravelFormBuilder\Form;
use Illuminate\Support\Facades\Config;

class BankTransferForm extends Form {

    public function buildForm()
    {

        if (Config::get('veritrans.vt_web'))
        {
            $this
                ->add('banks', 'select', [
                    'rules'       => 'required',
                    'choices'     => [
                        1 => 'Mandiri Bill Payment',
                        2 => 'Mandiri/ATM Bersama',
                        3 => 'BCA/Prima',
                        4 => 'Permata/Alto'
                    ],
                    'empty_value' => trans('checkout.empty.value')
                ]);
        }
        else
        {
            $this->add('banks', 'hidden');
        }

    }
}
