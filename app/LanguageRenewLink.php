<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class LanguageRenewLink
 * @package App
 */
class LanguageRenewLink extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'language_renew_links';

    /**
     * @var array
     */
    protected $fillable = ['token', 'active'];

    /**
     * @var array
     */
    protected $appends = ['link'];

    /**
     * @return string
     */
    public function getLinkAttribute()
    {
        return route('promotion.reset', ['token' => $this->token]);
    }

}
