<?php

namespace App;

use App\Call\NetTelLists;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Contact
 * @package App
 */
class Contact extends Model implements Transformable
{

    use TransformableTrait;

    /**
     * @var array
     */
    protected $types = [
        1 => 'Contact',
        2 => 'Got interested?',
        3 => 'Trial lesson',
    ];

    /**
     * @var string
     */
    protected $table = 'contacts';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'product', 'popup', 'age', 'message', 'language_id', 'type', 'reminded', 'token', 'other_lang', 'call_center', 'click_id', 'pub_id', 'ext_client_id', 'cc_sell', 'cc_price', 'cc_id', 'cc_reject', 'cc_hold', 'params', 'postback', 'postback_hold', 'postback_fake', 'cc_unique_id', 'classifier_name', 'base_url', 'ext_price'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'type_name', 'language_name', 'first_name', 'last_name', 'external_name'
    ];

    protected $fieldSearchable = [
        'name',
        'email',
        'phone'
    ];

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    /**
     * @return mixed
     */
    public function getTypeNameAttribute()
    {
        return isset($this->types[$this->type]) ? $this->types[$this->type] : $this->types[1];
    }

    /**
     * @return string
     */
    public function getLanguageNameAttribute()
    {
        return $this->language_id > 0 ? $this->language->name : 'none';
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function order()
    {
        return $this->hasOne('App\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language')->with(['translations']);
    }

    public function externalClient()
    {
        return $this->belongsTo('App\ExternalClient', 'ext_client_id');
    }

    /**
     * @return mixed
     */
    public function getFirstNameAttribute()
    {
        $name = explode(' ', $this->name);

        return isset($name[0]) ? $name[0] : '';
    }

    /**
     * @return mixed
     */
    public function getLastNameAttribute()
    {
        $name = explode(' ', $this->name);
        array_shift($name);

        return implode(' ', $name);
    }

    public function getExternalNameAttribute()
    {
        return isset($this->externalClient) ? $this->externalClient->name : null;
    }

    public function getCallCenterList()
    {
        if (Config::get('app.debug')) {
            return NetTelLists::$TESTING;
        }

        switch ($this->type) {
            case 1:
                return NetTelLists::$CALL_ME;
                break;
            case 2:
                return NetTelLists::$CALL_ME;
                break;
            case 3:
                return NetTelLists::$TRY_LESSON;
                break;
            default:
                return 0;
        }
    }

    public function getStatus()
    {
        if($this->postback_fake)
        {
            return 'SOLD';
        }

        if($this->cc_sell)
        {
            return 'SOLD';
        }

        if($this->cc_reject)
        {
            return 'CLOSED';
        }

        if($this->cc_hold)
        {
            return 'ON HOLD';
        }

        return 'processing';
    }

    public function isTooLate()
    {
        $created = $this->getCreatedAtFormatedAttribute();
        $daysAgo = $created->diffInDays();
        if($this->cc_hold==true && $this->cc_reject==false && $this->cc_sell==false && $daysAgo>7){
            return true;
        }
        return false;
    }

    public function delete()
    {
        if ($this->call_center) {
            return;
        }

        parent::delete();
    }

    public function getCallComment()
    {
        if (
            !$this->postback_fake
            && $this->classifier_name!='WAITING PAYMENT'
            && $this->classifier_name!='WAITING PAYMENT TRANSFER'
            && $this->classifier_name!='WAITING PAYMENT BANK TRANSFER'
            && $this->classifier_name!='WAITING PAYMENT BANKTRANSFER'
            && $this->classifier_name!=null
        ){
            return $this->classifier_name;
        }
        return '';
    }
}
