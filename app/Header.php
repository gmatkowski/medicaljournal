<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;


/**
 * Class Header
 * @package App
 */
class Header extends Model implements Transformable
{

    use Translatable, TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'headers';

    /**
     * @var array
     */
    protected $translatedAttributes = ['text', 'text2', 'text3'];

    /**
     * @var array
     */
    protected $fillable = ['text', 'text2', 'text3'];

}
