<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class SubDistrict
 * @package App
 */
class PostCode extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'postcodes';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'jayon', 'ninja', 'rpx', 'asp', 'sap'
    ];

    protected $appends = [
        'shipping_provider_name', 'code_covered'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subDistricts()
    {
        return $this->hasMany('App\SubDistrict');
    }

    public function getShippingProviderNameAttribute()
    {
        if ($this->attributes['jayon'] == 1)
        {
            return 'JAYON';
        }

        if ($this->attributes['ninja'] == 1)
        {
            return 'NINJA';
        }

        if ($this->attributes['rpx'] == 1)
        {
            return 'RPX';
        }

//        if ($this->attributes['asp'] == 1)
//        {
//            return 'ASP';
//        }
//
//        if ($this->attributes['sap'] == 1)
//        {
//            return 'SAP';
//        }

        return 'Area not supported anymore';
    }

    public function getCodeCoveredAttribute()
    {
        if ($this->attributes['jayon'] == 0 && $this->attributes['ninja'] == 0 && $this->attributes['rpx'] == 0 && $this->attributes['asp'] == 0 && $this->attributes['sap'] == 0){
            return false;
        }
        return true;
    }

    public function delete()
    {
        return;
    }
}
