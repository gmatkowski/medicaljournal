<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class District
 * @package App
 */
class District extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'districts';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'city_id', 'ninja', 'acommerce', 'jayon', 'rpx', 'sap', 'asp'
    ];

    protected $appends = [
        'shipping_provider_name', 'district_covered'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City', 'city_id')->with(['province']);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subDistricts()
    {
        return $this->hasMany('App\SubDistrict');
    }

    public function getShippingProviderNameAttribute()
    {
        if ($this->attributes['jayon'] == 1)
        {
            return 'JAYON';
        }

        if ($this->attributes['ninja'] == 1)
        {
            return 'NINJA';
        }

        if ($this->attributes['rpx'] == 1)
        {
            return 'RPX';
        }

//        if ($this->attributes['asp'] == 1)
//        {
//            return 'ASP';
//        }
//
//        if ($this->attributes['sap'] == 1)
//        {
//            return 'SAP';
//        }

        return 'Area not supported anymore';
    }

    public function getDistrictCoveredAttribute()
    {
        if ($this->attributes['jayon'] == 0 && $this->attributes['ninja'] == 0 && $this->attributes['rpx'] == 0 && $this->attributes['asp'] == 0 && $this->attributes['sap'] == 0){
            return false;
        }
        return true;
    }

    public function delete()
    {
        return;
    }
}
