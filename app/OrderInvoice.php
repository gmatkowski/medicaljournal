<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderInvoice
 * @package App
 */
class OrderInvoice extends Model {

    /**
     * @var string
     */
    protected $table = 'order_invoices';

    /**
     * @var array
     */
    protected $fillable = [
        'invoice_no', 'credit_no', 'order_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }
}
