<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderLanguage
 * @package App
 */
class OrderLanguage extends Model {

    /**
     * @var string
     */
    protected $table = 'order_languages';

    /**
     * @var array
     */
    protected $fillable = [
        'price', 'qty', 'order_id', 'language_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id');
    }
}
