<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Setting
 * @package App
 */
class Setting extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'settings';

    /**
     * @var array
     */
    protected $fillable = [
        'available_free', 'available_gone', 'available_treshold_from', 'available_treshold_to'
    ];

}
