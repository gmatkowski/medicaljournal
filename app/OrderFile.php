<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class OrderFile
 * @package App
 */
class OrderFile extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'order_files';

    /**
     * @var array
     */
    protected $fillable = [
        'hash'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file()
    {
        return $this->belongsTo('App\LanguageFile');
    }

}
