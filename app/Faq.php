<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

/**
 * Class Faq
 * @package App
 */
class Faq extends Model implements Transformable {

    use Translatable, TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'faqs';

    /**
     * @var array
     */
    protected $translatedAttributes = ['name', 'description'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'description', 'icon', 'type'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated', 'icon_path'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    /**
     * @return string
     */
    public function getIconPathAttribute()
    {
        return asset('faqs/icons/' . $this->icon);
    }
}
