<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslation
 * @package App
 */
class PageTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'page_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['title', 'content'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function page()
    {
        return $this->belongsTo('App\Page');
    }

}
