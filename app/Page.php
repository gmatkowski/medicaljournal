<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Page
 * @package App
 */
class Page extends Model {

    use Translatable, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'pages';

    /**
     * @var array
     */
    protected $translatedAttributes = ['title', 'content'];

    /**
     * @var array
     */
    protected $fillable = [ 'title', 'content', 'icon'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated', 'icon_path'];

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    public function getIconPathAttribute()
    {
        return asset('pages/icons/' . $this->icon);
    }
}
