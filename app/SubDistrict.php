<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class SubDistrict
 * @package App
 */
class SubDistrict extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'subdistricts';

    /**
     * @var array
     */
    protected $casts = [
        'cod'  => 'boolean',
        'ccod' => 'boolean',
    ];

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'leadtime', 'cod', 'ccod', 'district_id', 'post_code_id'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function postcode()
    {
        return $this->belongsTo('App\PostCode', 'post_code_id');
    }

    public function orderAddresses()
    {
        return $this->hasMany('App\OrderAddress');
    }

    public function delete()
    {
        return;
    }
}
