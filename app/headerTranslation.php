<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeaderTranslation extends Model
{
    /**
     * @var string
     */
    public $table = 'header_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['text', 'text2', 'text3'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function header()
    {
        return $this->belongsTo('App\Header');
    }
}
