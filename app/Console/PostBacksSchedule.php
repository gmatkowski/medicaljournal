<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;

class PostBacksSchedule
{
    /**
     * @param Schedule $schedule
     */
    public function register(Schedule $schedule)
    {
        $schedule
            ->command('cron:send-post-backs sold')
            ->everyThirtyMinutes()
            ->appendOutputTo(storage_path('logs/post-backs.log'));

        $schedule
            ->command('cron:send-post-backs rejected')
            ->everyTenMinutes()
            ->appendOutputTo(storage_path('logs/post-backs.log'));

        $schedule
            ->command('cron:send-post-backs hold')
            ->everyFiveMinutes()
            ->appendOutputTo(storage_path('logs/post-backs.log'));
    }
}