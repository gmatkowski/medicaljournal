<?php

namespace App\Console;

use App\Console\Commands\AddAdressToOrders;
use App\Console\Commands\AddCouriers;
use App\Console\Commands\AddSubdistrictsToOrderAddress;
use App\Console\Commands\AffbayReanimate;
use App\Console\Commands\AlertStock;
use App\Console\Commands\AlertUs;
use App\Console\Commands\BankTransfer;
use App\Console\Commands\ChangeCouriers;
use App\Console\Commands\ChangePostCouriers;
use App\Console\Commands\CheckStock;
use App\Console\Commands\CompareContactsAndSend;
use App\Console\Commands\ContactStatusCheck;
use App\Console\Commands\CutASPSAP;
use App\Console\Commands\DeliveryStatus;
use App\Console\Commands\FCCUpdateRecords;
use App\Console\Commands\FoxyAdsResend;
use App\Console\Commands\InvoicesPdf;
use App\Console\Commands\InvoicesXls;
use App\Console\Commands\Mailing;
use App\Console\Commands\MarkAsSentToCss;
use App\Console\Commands\MissingArea;
use App\Console\Commands\OVHResend;
use App\Console\Commands\PostBacks;
use App\Console\Commands\ResendContactsToAffbay;
use App\Console\Commands\ResetInvoice;
use App\Console\Commands\RestoreGreenCoffe;
use App\Console\Commands\SendContacts;
use App\Console\Commands\TestStock;
use App\Console\Commands\UpdateAreas;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Console\Commands\FailurePayment;
use App\Console\Commands\LeaveData;
use App\Console\Commands\PendingPayment;
use App\Console\Commands\TryLesson;
use App\Console\Commands\OrderStatusCheck;
use App\Console\Commands\OrderSms;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Contracts\Foundation\Application;

class Kernel extends ConsoleKernel {

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        FailurePayment::class,
        PendingPayment::class,
        LeaveData::class,
        TryLesson::class,
        OrderStatusCheck::class,
        OrderSms::class,
        SendContacts::class,
        CompareContactsAndSend::class,
        AddCouriers::class,
        UpdateAreas::class,
        MarkAsSentToCss::class,
        ContactStatusCheck::class,
        BankTransfer::class,
        AddAdressToOrders::class,
        InvoicesPdf::class,
        InvoicesXls::class,
        Mailing::class,
        ChangeCouriers::class,
        AddSubdistrictsToOrderAddress::class,
        ResetInvoice::class,
        TestStock::class,
        FCCUpdateRecords::class,
        CheckStock::class,
        AlertStock::class,
        MissingArea::class,
        DeliveryStatus::class,
        ChangePostCouriers::class,
        CutASPSAP::class,
        RestoreGreenCoffe::class,
        PostBacks::class,
        OVHResend::class,
        FoxyAdsResend::class,
        AffbayReanimate::class,
        AlertUs::class,
        ResendContactsToAffbay::class
    ];

    /**
     * @var PullingRecordsSchedule
     */
    private $recordsSchedule;

    /**
     * @var PostBacks
     */
    private $postBacksSchedule;

    public function __construct(Application $app, Dispatcher $events, PullingRecordsSchedule $recordsSchedule, PostBacksSchedule $postBacksSchedule)
    {
        parent::__construct($app, $events);

        $this->recordsSchedule = $recordsSchedule;
        $this->postBacksSchedule = $postBacksSchedule;
    }

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        //$schedule->command('cron:failure-payment')->everyTenMinutes()->appendOutputTo(storage_path('app/failure-payment.log'));
        //$schedule->command('cron:leave-data')->everyTenMinutes()->appendOutputTo(storage_path('app/leave-data.log'));
        //$schedule->command('cron:pending-payment')->everyTenMinutes()->appendOutputTo(storage_path('app/pending-payment.log'));
        //$schedule->command('cron:order-sms')->everyTenMinutes()->appendOutputTo(storage_path('app/order-sms.log'));
        /*
        $schedule
            ->command('cron:try-lesson')
            ->everyTenMinutes()
            ->appendOutputTo(storage_path('app/try-lesson.log'));
        */
        /*
        $schedule
            ->command('cron:order-status-check')
            ->everyThirtyMinutes()
            ->appendOutputTo(storage_path('logs/order-status-check.log'));
        */

        $schedule
            ->command('cron:send-contact')
            ->everyMinute()
            ->appendOutputTo(storage_path('logs/send-contact.log'));

        // this custom schedule class will delegate complex process of importing records from callcenter
//        $this->recordsSchedule->register($schedule);
//
//        $this->postBacksSchedule->register($schedule);

//        $schedule
//            ->command('cron:sms-status-check')
//            ->cron('0 0 */2 * *')
//            ->appendOutputTo(storage_path('logs/sms-status-check.log'));

//        $schedule
//            ->command('cron:invoices-pdf')
//            ->twiceDaily()
//            ->appendOutputTo(storage_path('logs/invoices-pdf.log'));

//        $schedule
//            ->command('cron:invoices-xls')
//            ->hourly()
//            ->appendOutputTo(storage_path('logs/invoices-xls.log'));

//        $schedule
//            ->command('cron:check-stock')
//            ->dailyAt('00:00')
//            ->appendOutputTo(storage_path('logs/check-stock.log'));
//
//        $schedule
//            ->command('cron:alert-stock')
//            ->dailyAt('23:00')
//            ->appendOutputTo(storage_path('logs/alert-stock.log'));
//
//        $schedule
//            ->command('cron:delivery-status')
//            ->hourly()
//            ->appendOutputTo(storage_path('logs/delivery-status.log'));

//        $schedule
//            ->command('cron:alert-us')
//            ->everyThirtyMinutes()
//            ->appendOutputTo(storage_path('logs/alert-us.log'));
    }
}
