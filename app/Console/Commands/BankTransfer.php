<?php

namespace App\Console\Commands;

use App\Repositories\OrderRepository;
use Illuminate\Console\Command;

class BankTransfer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:bank-transfer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Changing order payment method to bank transfer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OrderRepository $orderRepo)
    {
        $orders = $orderRepo->findWhere([['bank_transfer', '=', 1]]);
        foreach($orders as $order){
            $order->method=7;
            $order->save();
        }
    }
}