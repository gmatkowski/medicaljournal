<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\OrderRepository;
use App\Repositories\DistrictRepository;

class AddAdressToOrders extends Command {

    protected $signature = 'cron:add-address';

    protected $description = 'Add full address and district name to orders';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(OrderRepository $orderRepo, DistrictRepository $districtRepo)
    {
        $orders = $orderRepo->getOrdersWithDistrictId();
        foreach($orders as $order){
            $district = $districtRepo->findWhere([['id', '=', $order->address()->first()->district_id]])->first();
            if($district){
                $postalcode = $order->address()->first()->postal_code;
                $address = $order->address()->first()->address;
                $subdistrict = $order->address()->first()->subdistrict;

                $order->old_postalcode = $postalcode ? $postalcode : null;
                $order->old_address = $address ? $address : null;
                $order->old_province = $district->city->province->name;
                $order->old_city = $district->city->name;
                $order->old_district = $district->name;
                $order->old_courier = $district->shipping_provider_name;
                $order->old_subdistrict = $subdistrict ? $subdistrict : null;
                $order->old = 1;
                $order->save();
            }
        }

    }
}
