<?php

namespace App\Console\Commands;

use App\Helpers\StrHelper;
use App\Language;
use App\Repositories\LanguageRepository;
use App\Repositories\StockRepository;
use App\Repositories\UserRepository;
use App\Stock;
use Carbon\Carbon;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Console\Command;

class CheckStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:check-stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking stock once a midnight and sending emails';

    private $stocks;

    private $users;

    private $languages;

    protected $mail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(StockRepository $stockRepository, LanguageRepository $languageRepository, UserRepository $userRepository, Mailer $mail)
    {
        parent::__construct();
        $this->stocks = $stockRepository;
        $this->languages = $languageRepository;
        $this->users = $userRepository;
        $this->mail = $mail;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $languages = $this->languages->all();
        foreach($languages as $language)
        {
            $stock = $this->stocks->searchPaginateStocksWithDate(1, $language->symbol, Carbon::yesterday()->format('d/m/Y').' - '.Carbon::yesterday()->format('d/m/Y'))->first();
            if($stock && $stock->close===null){

                if($stock->open<0){
                    $stock->open=0;
                    $stock->save();
                }

                $close = $stock->open + ($stock->in - $stock->out);

                $stock->close = StrHelper::mustBeMoreThanZero($close);
                $stock->save();

                $language->amount = StrHelper::mustBeMoreThanZero($close);
                $language->save();

                $this->sendMails($stock);

                $this->createNewStock($language, $stock->close);
            } else {
                $stocks = $this->stocks->searchAllProductStocks($language->symbol);
                if(count($stocks)==0)
                {
                    $this->createNewStock($language);
                }
            }
        }
    }

    private function createNewStock(Language $language, $open = 0)
    {
        $stock = $this->stocks->create([
            'open' => $open,
            'in' => 0,
            'out' => 0,
            'close' => null
        ]);
        $language->stocks()->save($stock);
    }

    private function sendMails(Stock $yesterdayStock)
    {
        $users = $this->users->stockAlerts();
        foreach($users as $user)
        {
            $this->mail->send('email.stock', ['content' => Carbon::now().' stock of '.$yesterdayStock->language->name.' : OPEN('.$yesterdayStock->open.'), IN('.$yesterdayStock->in.'), OUT('.$yesterdayStock->out.'), CLOSE('.$yesterdayStock->close.')'], function ($m) use ($user)
            {
                $m->to($user->email)->subject('Stock Report');
            });
        }
    }
}
