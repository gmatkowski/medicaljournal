<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Repositories\ContactRepository;
use Illuminate\Console\Command;

class FunCPAResend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:funcpa-resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'FunCPA postBacks Approves beetwen 08 NOV 04:00 - 10 NOV 14:30 (Jakarta time) resend';

    private $contacts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contacts = $contactRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from = Carbon::createFromDate(2017, 11, 8)->startOfDay()->addHours(4);
        $to = Carbon::createFromDate(2017, 11, 10)->startOfDay()->addHours(14)->addMinutes(30);

        $solds = $this->contacts->findWhere([['ext_client_id', '=', 4], ['postback', '=', 1], ['cc_sell', '=', 1], ['created_at', '>', $from], ['created_at', '<', $to]])->all();
        foreach($solds as $contact)
        {
            $postBack = file_get_contents('https://funcpa.ru/api/asia/?id='.$contact->click_id.'&status=sale');
            if(isset($postBack))
            {
                Log::info('Resend FunCPA Sold Post Back sent', ['contact_id' => $contact->id, 'ext_client_id' => $contact->ext_client_id, 'click_id' => $contact->click_id]);
            }
        }
    }
}
