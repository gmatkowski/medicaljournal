<?php

namespace App\Console\Commands;

use App\Call\CallCenter;
use App\Repositories\OrderRepository;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class OrderStatusCheck
 * @package App\Console\Commands
 */
class OrderStatusCheck extends Command {

    /**
     * @var string
     */
    protected $signature = 'cron:order-status-check';

    /**
     * @var string
     */
    protected $description = 'Check orders status';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function handle(OrderRepository $orderRepository, CallCenter $callCenter)
    {
        $orders = $orderRepository->forCronStatus(100);

        if ($orders->count() > 0)
        {
            $data = [
                'action'   => 'fcc-add-records',
                'campaign' => 'failed',
                'contacts' => $orders,
            ];
            $response = $callCenter->send($data);
            $respBody = json_decode($response);

            $contactsToUpdate = [];
            foreach ($orders as $contact){
                $contactsToUpdate[] = $contact->id;
            }

            if ($respBody->success){
                DB::table('orders')
                    ->whereIn('id', $contactsToUpdate)
                    ->update(['called' => 1]);
            } else {
                Log::notice('Focus call center error: '.$respBody->message);
            }
        }
    }
}
