<?php

namespace App\Console\Commands;

use App\Repositories\OrderRepository;
use App\Repositories\SubDistrictRepository;
use Illuminate\Console\Command;
use App\Repositories\DistrictRepository;

class AddSubdistrictsToOrderAddress extends Command
{
    protected $signature = 'cron:subdistricts-address';

    protected $description = 'Change districts couriers';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(
        DistrictRepository $districtRepo,
        SubDistrictRepository $subDistrictRepo,
        OrderRepository $orderRepo
    )
    {
        $orders = $orderRepo->getOrdersWithAddress();
        foreach($orders as $order){
            $districtName = strtolower($order->old_district);
            $subdistrictName = strtolower($order->old_subdistrict);
            $district = $districtRepo->findWhere([['name', '=', $districtName]])->first();
            $subDistrict = $subDistrictRepo->findWhere([['name', '=', $subdistrictName]])->first();
            $order->address()->create([
                'full_name'   => $order->last_name ? $order->first__name.' '.$order->last_name : $order->first__name,
                'first_name'  => $order->first__name,
                'last_name'   => $order->last_name ? $order->last_name : $order->first__name,
                'postal_code' => $order->old_postalcode,
                'address'     => $order->old_address,
                'province' => $order->old_province,
                'city' => $order->old_city,
                'district_id' => $district ? $district->id : null,
                'subdistrict' => $order->old_subdistrict,
                'sub_district_id' => $subDistrict ? $subDistrict->id : null
            ]);
        }

    }
}
