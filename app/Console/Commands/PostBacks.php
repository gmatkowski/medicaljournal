<?php

namespace App\Console\Commands;

use App\Contact;
use Carbon\Carbon;
use anlutro\cURL\cURL;
use Illuminate\Support\Facades\Log;
use App\Repositories\ContactRepository;
use App\Repositories\LanguageRepository;
use Illuminate\Console\Command;

class PostBacks extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:send-post-backs {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending post backs to partners';

    private $contactRepo;
    private $languageRepository;

    private $curl;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContactRepository $contactRepository, LanguageRepository $languageRepository)
    {
        $this->contactRepo = $contactRepository;
        $this->languageRepository = $languageRepository;
        $this->curl = new cURL();
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        switch($this->argument('type')) {
            case 'fake':
//                $this->fakeSolds();
                break;
            case 'sold':
                $contacts = $this->contactRepo->findWhere([['postback', '=', 0], ['postback_fake', '=', 0], ['cc_sell', '=', 1], ['ext_client_id', '<>', ''], ['created_at', '>', Carbon::now()->subWeeks(2)]])->all();
                break;
            case 'rejected':
                $contacts = $this->contactRepo->findWhere([['postback', '=', 0], ['postback_fake', '=', 0], ['cc_reject', '=', 1], ['cc_sell', '=', 0], ['ext_client_id', '<>', ''], ['created_at', '>', Carbon::now()->subWeeks(2)]])->all();
                break;
            case 'hold':
                $contacts = $this->contactRepo->findWhere([['postback_hold', '=', 0], ['postback_fake', '=', 0], ['cc_hold', '=', 1], ['cc_sell', '=', 0], ['ext_client_id', '<>', ''], ['created_at', '>', Carbon::now()->subWeeks(2)]])->all();
                break;
            default:
                $contacts = collect();
        }

        foreach($contacts as $contact)
        {
//            if($this->checkPostBack($contact, 'postback')===null)
//            {
            try {

                $this->{$this->argument('type').'SoInformExternalClients'}($contact);

            } catch (\Exception $e) {

                $this->invalidPostBack($e->getMessage());
            }
//            }
        }
    }

    private function fakeSolds()
    {
//        'IDR' => 0.0000736614,
//        'THB' => 0.030102

        $grouped = collect($this->contactRepo->findWhere(
                [
                    ['postback_fake', '=', 0],
                    ['ext_client_id', '<>', ''],
                    ['created_at', '>', Carbon::now()->subDay()]
                ]
            )->all()
        )
            ->groupBy('ext_client_id');

        foreach($grouped as $key => $externalClient)
        {
            if($externalClient->groupBy('cc_sell')->count()==2)
            {
                $reaLeadsCounter = $externalClient->count(); //

                $payoutDollars = 28;

                $sold = $externalClient->groupBy('cc_sell')[1];
                $approvedSoldCounter = $sold->count(); //

                $realApprovedPercent = (100*$approvedSoldCounter)/$reaLeadsCounter;

                if($realApprovedPercent<50)
                {
                    $earnoutDollars = 0; //
                    $productsSolds = $sold->groupBy('product');
                    foreach($productsSolds as $productName => $items)
                    {
//                        switch ($productName) {
//                            case 'cambogia':
//                                $productId = 1;
//                                break;
//                            case 'blackmask':
//                                $productId = 2;
//                                break;
//                            case 'hallupro':
//                                $productId = 3;
//                                break;
//                            case 'waist':
//                                $productId = 4;
//                                break;
//                            case 'detoclean':
//                                $productId = 5;
//                                break;
//                            case 'coffee':
//                                $productId = 6;
//                                break;
//                            case 'joint':
//                                $productId = 7;
//                                break;
//                            case 'posture':
//                                $productId = 8;
//                                break;
//                        }
//                        $product = $this->languageRepository->find($productId);
                        foreach($items as $item)
                        {
//                        $earnoutDollars+=floor($product->price*0.0000736614); // $ exchange
                            $earnoutDollars+=110;
                        }
                    }

                    $kpi = 3;

                    if($earnoutDollars>0)
                    {
                        $maxPayoutDollars = $earnoutDollars/$kpi; //

                        $shouldHasCounter = floor($maxPayoutDollars/$payoutDollars);
                        $shouldHasComparedToRealCounter = $shouldHasCounter>$reaLeadsCounter
                            ? $reaLeadsCounter
                            : $shouldHasCounter; //

                        $fakesCounter = floor($shouldHasComparedToRealCounter - $approvedSoldCounter);
                        $fakesComparedToApproved = $fakesCounter < 0
                            ? 0
                            : $fakesCounter; //

                        foreach($externalClient as $key => $item)
                        {
                            if($item->cc_reject==1)
                            {
                                $externalClient->pull($key);
                            }
                        }

                        $notsold = $externalClient
                            ->groupBy('cc_sell')[0];

                        $fakes = collect($notsold)->take($fakesComparedToApproved);

                        $fakeApprovedPercent = ($fakesComparedToApproved + $approvedSoldCounter)/$reaLeadsCounter;
                        if($fakeApprovedPercent<=0.65)
                        {
                            $fakesLastCounter = collect();
                            foreach($fakes as $fakeContact)
                            {
                                try
                                {
                                    if($fakeContact->cc_reject==0 && $fakeContact->cc_sell==0)
                                    {
                                        $fakeSent = $this->soldSoInformExternalClients($fakeContact);
                                        if($fakeSent)
                                        {
                                            $fakeSaved = $this->savePostBack($fakeContact, 'postback_fake', 'Fake');
                                            if($fakeSaved)
                                            {
                                                $request = $this->curl
                                                    ->newRequest('put', 'https://affbay.asia/api/v1/lead/mock-conversion', [
                                                        'id' => $fakeContact->id,
                                                        'domain' => 'promos-seru'
                                                    ])
                                                    ->setHeader('authorization', "Q0awDyUvAvHBTD2szyC3RCPOuxNaQ6CKgqsiwP6y4gdCvNRFp4");
                                                $response = $request->send();
                                                $array = $response->toArray();

                                                if (sizeof($warnings = array_get($array, 'data.warnings')))

                                                    Log::warning("Warnings during mock conversion found", $warnings);

                                                $message = ($response->statusCode == 201)
                                                    ? "Contact successfully mock"
                                                    : "Something went wrong during mock conversion";

                                                Log::info($message, $array);

                                                $fakesLastCounter->push([]);
                                            }
                                        }
                                    }

                                } catch (\Exception $e) {

                                    $this->invalidPostBack($e->getMessage());
                                }
                            }

                            Log::info('Fakes counters:', [
                                'real' => $reaLeadsCounter,
                                'sold' => $approvedSoldCounter,
                                'fakes' => $fakesLastCounter->count()
                            ]);
                        }
                    }
                }
            }
        }
    }

//    private function checkPostBack(Contact $contact, $postBackName)
//    {
//        $contacts = $this->contactRepo->findByField('click_id', $contact->click_id)->all();
//        $contacts = collect($contacts);
//        return $contacts->first(function ($key, $value) use ($postBackName) {
//            return $value->{$postBackName} == 1;
//        });
//    }

    private function soldSoInformExternalClients(Contact $contact)
    {
        switch ($contact['ext_client_id']) {
            case 1:
                $url = $this->curl->buildUrl('http://metacpa.ru/lead/status', ['id' => $contact->click_id, 'status' => 'sell', 'key' => 'dzj7lY6Hq05gmQmY2xP3', 'pub_id' => $contact->pub_id]);
                break;
            case 4:
                $postBack = file_get_contents('https://funcpa.ru/api/asia/?id='.$contact->click_id.'&status=sale');
                break;
            case 6:
                $postBack = file_get_contents('https://api.affninja.com/affbay?status=approved&external_id='.$contact->id.'&lead_hash='.$contact->click_id.'&api_key=z06G75Ng7NEqnx9l');
                break;
            case 7:
                $postBack = file_get_contents('http://cc.salesup-crm.com/API.aspx?action=UpdateStatus&api_key=52jOnvApJEOoHC3L5CSXeA&outer_id='.$contact->id.'&status=Approved');
                break;
//            case 9:
//                $url = $this->curl->buildUrl('http://18.195.192.24:8222/postback', ['sub_id' => $contact->click_id, 'status' => 'approved']);
//                break;
            case 10:
                $postBack = file_get_contents('https://foxyads.ru/integration/affbay-asia/postback/status?uniq_id='.$contact->click_id.'&aim=approved&status=hold');
                break;
            case 11:
                $postBack = file_get_contents('http://www.go2asian.com/43d7f61/postback?subid='.$contact->click_id.'&status=Approved');
                break;
            case 13:
                $postBack = file_get_contents('http://cc.salesup-crm.com/API.aspx?action=UpdateStatus&api_key=xyXZPKjTlkqN_CC8zZQLsA&outer_id='.$contact->id.'&status=Approved');
                break;
            case 14:
                $postBack = file_get_contents('http://cc.salesup-crm.com/APILvAffBay.aspx?lord_id='.$contact->click_id.'&status=Approved');
                break;
            case 18:
                $postBack = file_get_contents('https://leadrock.com/postback/65c9/Lead/approved?click_id='.$contact->click_id);
                break;
            case 20:
                $postBack = file_get_contents('http://128.199.192.85/f4da603/postback&subid='.$contact->click_id.'&status=approved');
                break;
            case 21:
                $postBack = file_get_contents('https://advertiser.drcash.me/v1/postback?access_token=bbc684b39b26be4e599104a3495cc84191317155&status=sale&tracking_uuid='.$contact->click_id.'&transaction_id='.$contact->click_id.'&payout=22');
                break;
            case 27:
                $postBack = file_get_contents('https://api.monsterleads.pro/method/lead.edit?api_key=aa67cc232538119f13422feb3d8ffe42&lead_key='.$contact->click_id.'&status=2&format=json');
                break;
        }
        if(isset($url)) {
            $postBack = $this->curl->get($url);
        }
        if(isset($postBack))
        {
            return $this->savePostBack($contact, 'postback', 'Sold');
        }
    }

    private function rejectedSoInformExternalClients(Contact $contact)
    {
        switch ($contact->ext_client_id) {
            case 1:
                $url = $this->curl->buildUrl('http://metacpa.ru/lead/status', ['id' => $contact->click_id, 'status' => $this->getMetacpaRejected($contact), 'key' => 'dzj7lY6Hq05gmQmY2xP3', 'pub_id' => $contact->pub_id]);
                break;
            case 4:
                $postBack = file_get_contents('https://funcpa.ru/api/asia/?id='.$contact->click_id.'&status=reject&comment='.urlencode($contact->classifier_name));
                break;
            case 6:
                $postBack = file_get_contents('https://api.affninja.com/affbay?status=cancelled&external_id='.$contact->id.'&lead_hash='.$contact->click_id.'&api_key=z06G75Ng7NEqnx9l');
                break;
            case 7:
                $postBack = file_get_contents('http://cc.salesup-crm.com/API.aspx?action=UpdateStatus&api_key=52jOnvApJEOoHC3L5CSXeA&outer_id='.$contact->id.'&status=Rejected&call_comment='.urlencode($contact->classifier_name));
                break;
//            case 9:
//                $url = $this->curl->buildUrl('http://18.195.192.24:8222/postback', ['sub_id' => $contact->click_id, 'status' => 'rejected']);
//                break;
            case 10:
                $postBack = file_get_contents('https://foxyads.ru/integration/affbay-asia/postback/status?uniq_id='.$contact->click_id.'&aim=approved&status=rejected');
                break;
            case 11:
                $postBack = file_get_contents('http://www.go2asian.com/43d7f61/postback?subid='.$contact->click_id.'&status=Rejected');
                break;
            case 13:
                $postBack = file_get_contents('http://cc.salesup-crm.com/API.aspx?action=UpdateStatus&api_key=xyXZPKjTlkqN_CC8zZQLsA&outer_id='.$contact->id.'&status=Rejected&call_comment='.urlencode($contact->classifier_name));
                break;
            case 14:
                $postBack = file_get_contents('http://cc.salesup-crm.com/APILvAffBay.aspx?lord_id='.$contact->click_id.'&status=Rejected&call_comment='.urlencode($contact->classifier_name));
                break;
            case 18:
                $postBack = file_get_contents('https://leadrock.com/postback/65c9/Lead/rejected?click_id='.$contact->click_id);
                break;
            case 20:
                $postBack = file_get_contents('http://128.199.192.85/f4da603/postback&subid='.$contact->click_id.'&status=rejected');
                break;
            case 21:
                $postBack = file_get_contents('https://advertiser.drcash.me/v1/postback?access_token=bbc684b39b26be4e599104a3495cc84191317155&status=rejected&tracking_uuid='.$contact->click_id.'&transaction_id='.$contact->click_id.'&payout=22');
                break;
            case 27:
                $postBack = file_get_contents('https://api.monsterleads.pro/method/lead.edit?api_key=aa67cc232538119f13422feb3d8ffe42&lead_key='.$contact->click_id.'&status=3&format=json&comments='.urlencode($contact->classifier_name));
                break;
        }
        if(isset($url)) {
            $postBack = $this->curl->get($url);
        }
        if(isset($postBack))
        {
            $this->savePostBack($contact,'postback','Rejected');
        }
    }

    private function holdSoInformExternalClients(Contact $contact)
    {
        switch ($contact->ext_client_id) {
            case 1:
                $url = $this->curl->buildUrl('http://metacpa.ru/lead/status', ['id' => $contact->click_id, 'status' => $this->getMetacpaHold($contact), 'key' => 'dzj7lY6Hq05gmQmY2xP3', 'pub_id' => $contact->pub_id]);
                break;
            case 2:
                $url = $this->curl->buildUrl('http://zm8q8.voluumtrk2.com/postback', ['cid' => $contact->click_id, 'payout' => 0.0 ,'txid' => 'expect']);
                break;
            case 7:
                $postBack = file_get_contents('http://cc.salesup-crm.com/API.aspx?action=UpdateStatus&api_key=52jOnvApJEOoHC3L5CSXeA&outer_id='.$contact->id.'&status=Hold&call_comment='.urlencode($contact->getCallComment()));
                break;
//            case 9:
//                $url = $this->curl->buildUrl('http://18.195.192.24:8222/postback', ['sub_id' => $contact->click_id, 'status' => 'hold']);
//                break;
            case 10:
                $postBack = file_get_contents('https://foxyads.ru/integration/affbay-asia/postback/status?uniq_id='.$contact->click_id.'&aim=approved&status=wait');
                break;
            case 11:
                $postBack = file_get_contents('http://www.go2asian.com/43d7f61/postback?subid='.$contact->click_id.'&status=Hold');
                break;
            case 13:
                $postBack = file_get_contents('http://cc.salesup-crm.com/API.aspx?action=UpdateStatus&api_key=xyXZPKjTlkqN_CC8zZQLsA&outer_id='.$contact->id.'&status=Hold&call_comment='.urlencode($contact->getCallComment()));
                break;
            case 14:
                $postBack = file_get_contents('http://cc.salesup-crm.com/APILvAffBay.aspx?lord_id='.$contact->click_id.'&status=Hold&call_comment='.urlencode($contact->getCallComment()));
                break;
            case 20:
                $postBack = file_get_contents('http://128.199.192.85/f4da603/postback&subid='.$contact->click_id.'&status=awaiting');
                break;
            case 21:
                $postBack = file_get_contents('https://advertiser.drcash.me/v1/postback?access_token=bbc684b39b26be4e599104a3495cc84191317155&status=new&tracking_uuid='.$contact->click_id.'&transaction_id='.$contact->click_id.'&payout=22');
                break;
            case 27:
                $postBack = file_get_contents('https://api.monsterleads.pro/method/lead.edit?api_key=aa67cc232538119f13422feb3d8ffe42&lead_key='.$contact->click_id.'&status=1&format=json&comments='.urlencode($contact->classifier_name));
                break;
        }
        if(isset($url)) {
            $postBack = $this->curl->get($url);
        }
        if(isset($postBack))
        {
            $this->savePostBack($contact, 'postback_hold', 'Hold');
        }
    }

    private function savePostBack(Contact $contact, $postBackName, $type)
    {
        $contact->{$postBackName} = 1;
        $contact->save();
        Log::info($type.' Post Back sent', ['contact_id' => $contact->id, 'ext_client_id' => $contact->ext_client_id, 'click_id' => $contact->click_id]);

        return true;
    }

    private function invalidPostBack($message)
    {
        Log::info('Invalid Post Back Partner URL', ['message' => $message]);
    }

    private function getMetacpaRejected(Contact $contact)
    {
        switch ($contact->getCallComment()) {
            case 'OTHER LANGUAGE':
            case 'PREGNANT':
            case 'PREGNAT':
                $status = 'contradiction';
                break;
            case 'DOUBLE':
                $status = 'double';
                break;
            case 'ALREADY ORDER':
            case 'ALREADY ORDER COD':
            case 'ALREADY ORDER DIGITAL':
            case 'ORDER BY CSS':
            case 'ORDER BY WEB':
                $status = 'old_customer';
                break;
            case 'OUT OF AREA':
                $status = 'other_geo';
                break;
            case 'CANCEL ORDER':
            case 'DO NOT BELIEVE':
            case 'DON\'T BELIEVE':
            case 'DON\'T BELIVE IN METHOD':
            case 'DON\'T BELIEVE IN METHOD':
            case 'DONT\' BELIEVE IN METHOD':
            case 'INBOUND SALE':
            case 'NO MONEY':
            case 'REJECTED':
            case 'CHANGE KLASYFIKATOR':
                $status = 'refuse';
                break;
            case 'CHANGE NUMBER':
            case 'WRONG NUMBER':
            case 'WRONG NUMBER/PERSON':
            default:
                $status = 'wrong_number';
        }

        return $status;
    }

    private function getMetacpaHold(Contact $contact)
    {
        switch ($contact->getCallComment()) {
            case 'BUSY':
            case 'CALL BACK':
            case 'CALLBACK':
            case 'NO SIGNAL':
            case 'NOT ANSWERED':
            case 'VOICE MAIL':
            case 'SILENCE IN THE PHONE':
            case 'SILENT IN THE PHONE':
            case 'SILENT ON THE PHONE':
            case 'NO ANSWER':
            case '':
                $status = 'no_answer';
                break;
            default:
                $status = 'failed_call';
        }

        return $status;
    }
}
