<?php

namespace App\Console\Commands;

use App\Repositories\CountryRepository;
use App\Repositories\PostCodeRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\SubDistrictRepository;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\CityRepository;
use App\Repositories\DistrictRepository;

class ChangeCouriers extends Command
{
    protected $signature = 'cron:change-couriers';

    protected $description = 'Change districts couriers';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(
        CountryRepository $countryRepo,
        ProvinceRepository $provinceRepo,
        CityRepository $cityRepo,
        DistrictRepository $districtRepo,
        SubDistrictRepository $subDistrictRepo,
        PostCodeRepository $postCodeRepo
    )
    {
        $counter = collect();
        $country = $countryRepo->findByField('name', 'Indonesia')->first();
        Excel::load(storage_path('app/leadtime.csv'), function ($reader) use ($counter, $country, $provinceRepo, $cityRepo, $districtRepo, $subDistrictRepo, $postCodeRepo) {
            $results = $reader->all();
            $results->each(function ($sheet) use ($counter, $country, $provinceRepo, $cityRepo, $districtRepo, $subDistrictRepo, $postCodeRepo) {
                $object = $sheet->all();

                $provinceName = strtolower($object['province']);
                $cityName = strtolower($object['city']);
                $districtName = strtolower($object['district']);
                $subdistrictName = strtolower($object['subdistrict']);
                $postCodeNumber = strtolower($object['postcode']);
                $leadtime = strtolower($object['leadtime']);

//                $postCode = $this->insertOrGetPostCode($postCodeNumber, $postCodeRepo);
//                if($postCode){
//                    if($courierName)
//                    {
//                        $postCode->{$courierName} = 0;
//                    }
//                    $postCode->ninja = 1;
//                    $postCode->save();
//                }

                $province = $this->insertOrGetProvince($country, $provinceName, $provinceRepo);
                if ($province) {
                    $city = $this->insertOrGetCity($province, $cityName, $cityRepo);
                    if ($city) {
                        $district = $this->insertOrGetDistrict($city, $districtName, $districtRepo);
                        if($district){
                            $subDistrict = $this->insertOrGetSubDistrict($district, $subdistrictName, $subDistrictRepo);
                            if($subDistrict){
                                $postCode = $this->insertOrGetPostCode($postCodeNumber, $postCodeRepo);
                                if($postCode){
                                    $subDistrict->leadtime = $leadtime;
                                    $subDistrict->save();

                                    $counter->push($leadtime);

//                                    if($courierName!='n/a'){
//                                        $postCode->{$courierName} = 1;
//                                    }
//                                    $postCode->save();
//                                    $subDistrict->post_code_id = $postCode->id;
//                                    $subDistrict->save();
                                }
                            }
                        }
                    }
                }
            });
        });
        echo $counter->count();
    }

    protected function insertOrGetProvince(\App\Country $country, $name, ProvinceRepository $provinceRepo)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $provinceRepo->findWhere([['name', '=', $name]]);

        if ($result->count() > 0)
        {
            return $result->first();
        }
//        else
//        {
//            $province = $provinceRepo->create([
//                'name'       => $name,
//                'country_id' => $country->id
//            ]);
//
//            return $province;
//        }
    }

    protected function insertOrGetCity(\App\Province $province, $name, CityRepository $cityRepo)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $cityRepo->findWhere([['name', '=', $name], ['province_id', '=', $province->id]]);

        if ($result->count() > 0)
        {
            return $result->first();
        }
//        else
//        {
//            $city = $cityRepo->create([
//                'name'        => $name,
//                'province_id' => $province->id
//            ]);
//
//            return $city;
//        }
    }

    protected function insertOrGetDistrict(\App\City $city, $name, DistrictRepository $districtRepo)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $districtRepo->findWhere([['name', '=', $name], ['city_id', '=', $city->id]]);

        if ($result->count() > 0)
        {
            return $result->first();
        }
//        else
//        {
//            $district = $districtRepo->create([
//                'name'    => $name,
//                'city_id' => $city->id
//            ]);
//
//            return $district;
//        }
    }

    protected function insertOrGetSubDistrict(\App\District $district, $name, SubDistrictRepository $subDistrictRepo)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $subDistrictRepo->findWhere([['name', '=', $name], ['district_id', '=', $district->id]]);

        if ($result->count() > 0)
        {
            return $result->first();
        }
//        else
//        {
//            $subdistrict = $subDistrictRepo->create([
//                'name'    => $name,
//                'district_id' => $district->id
//            ]);
//
//            return $subdistrict;
//        }
    }

    protected function insertOrGetPostCode($number, PostCodeRepository $postCodeRepo)
    {
        if (empty($number))
        {
            return false;
        }

        $result = $postCodeRepo->findWhere([['name', '=', $number]]);

        if ($result->count() > 0)
        {
            return $result->first();
        }
//        else
//        {
//            $postcode = $postCodeRepo->create([
//                'name' => $number
//            ]);
//
//            return $postcode;
//        }
    }
}
