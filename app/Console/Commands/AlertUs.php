<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Mail\Mailer;
use App\Repositories\ContactRepository;

class AlertUs extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:alert-us';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alert e-mails when something goes wrong on master system functions';

    private $mail;
    private $contacts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Mailer $mail, ContactRepository $contactRepository)
    {
        $this->mail = $mail;
        $this->contacts = $contactRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $lastCreatedContact = $this->contacts->orderBy('created_at', 'DESC')->first()->created_at->diffInMinutes();
        if($lastCreatedContact>60)
        {
            $data = 'It is more than 60 minutes on '.url().' when nothing came on Contacts. We have probably problem with Contacts saving. Please log in to panel and confirm it manually.';
            $this->send($data);
        }

        $lastSentContact = $this->contacts->focusCallCenter(100, 0)->first();
        if($lastSentContact && $lastSentContact->created_at->diffInMinutes()>15)
        {
            $data = 'Sending contacts to Call Center does not work on '.url().', Oldest Contacts created more than 15 minutes ago not sent. Please log in to panel and confirm it manually.';
            $this->send($data);
        }
    }

    private function send($data)
    {
        if(app()->environment() == 'production')
        {
            $this->mail->send('email.alert', [
                'data' => $data
            ], function ($m) {
                $m->to('sylwester@efirst.asia')

                    ->cc('robert@efirst.asia')
                    ->cc('a.sharkov@efirst.asia')
                    ->cc('przemyslaw.sierant@efirst.asia')
                        
                    ->subject('Efirst Error Warning !!!');
            });
        }
    }
}
