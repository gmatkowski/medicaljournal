<?php

namespace App\Console\Commands;

use App\Call\CallCenter;
use App\Contact;
use App\Repositories\ContactRepository;
use Illuminate\Console\Command;

class ContactStatusCheck extends Command
{
    use ExportTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:contact-status-check {hoursAgoFrom} {hoursAgoTo?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check orders status';

    private $contactRepo;

    private $callCenter;

    protected $campaigns;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContactRepository $contactRepository, CallCenter $callCenter)
    {
        $this->contactRepo = $contactRepository;
        $this->callCenter = $callCenter;
        $this->campaigns = ['medical', 'newProducts', 'promo', 'bustiere'];
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach($this->campaigns as $campaign)
        {
            $this->callCenter->setActiveCampaing($campaign);

            $classifiersCollection = $this->getClassifiersList($this->callCenter);

            $from = $this->formatTime((int)$this->argument('hoursAgoFrom'));
            $to = $this->formatTime((int)$this->argument('hoursAgoTo'));

            $exportRecordsBodyRecords = $this->exportContacts($this->callCenter, $from, $to);
            foreach($exportRecordsBodyRecords as $record){
                $this->classifiersListFromExportRecord($record, $classifiersCollection);
            }
        }
    }

    private function classifiersListFromExportRecord($record, $classifiersCollection)
    {
        if(isset($record->values->CREATED) && isset($record->values->PHONE) && isset($record->values->FROM_WEB) && $record->values->FROM_WEB=='promos-seru')
        {
            $created_at = $record->values->CREATED;
            $phone = $record->values->PHONE;

            $classifiers_id = $record->classifiers_id;
            if(isset($classifiers_id)){
                $classifier = $this->getClassifier($classifiersCollection, $classifiers_id);
                if($classifier){
                    $statusOk = $this->checkStatus(strtoupper($classifier->name));
                } else {
                    $statusOk = $this->checkAutomaticStatus($classifiers_id);
                }
                if($statusOk){
                    $contact = $this->contactRepo->findWhere([['created_at', '=', $created_at], ['phone', '=', $phone], ['cc_sell', '=', 0]])->first();
                    if($contact){
                        if($classifier){
                            $this->save($contact, $statusOk, $record, strtoupper($classifier->name));
                        } else {
                            $this->save($contact, $statusOk, $record, null);
                        }
                    }
                }
            }
        }
    }

    private function getClassifier($classifiersCollection, $classifiers_id)
    {
        return $classifiersCollection->first(function ($key, $value) use ($classifiers_id) {
            return $value->id == $classifiers_id;
        });
    }

    private function checkStatus($classifierName)
    {
        if($this->isSold($classifierName)){
            return 'SOLD';
        }
        if($this->isOnHold($classifierName)){
            return 'ON HOLD';
        }
        if($this->isRejected($classifierName)){
            return 'REJECTED';
        }
        return null;
    }

    private function isSold($classifierName)
    {
        if(
               $classifierName=='SELL'
            || $classifierName=='UPSELL'
            || $classifierName=='CROSSELL'
            || $classifierName=='SELL & UPSELL'
            || $classifierName=='SELL & CROSSELL'
            || $classifierName=='UPSELL & CROSSELL'
            || $classifierName=='SELL & CROSS SELL'
            || $classifierName=='UPSELL & CROSS SELL'
            || $classifierName=='SOLD'
            || $classifierName=='INBOUND SALE'
            || $classifierName=='INBOUND SELL'

//           strpos($classifierName, 'SELL') !== false
        ){
            return true;
        }
        return false;
    }

    private function isOnHold($classifierName)
    {
        if(
               $classifierName=='CALL BACK'
            || $classifierName=='CALLBACK'
            || $classifierName=='VOICE MAIL'
            || $classifierName=='NOT ANSWERED'
            || $classifierName=='BUSY'
            || $classifierName=='NO SIGNAL'
            || $classifierName=='NO ANSWER'
            || $classifierName=='WAITING PAYMENT BANK TRANSFER'
            || $classifierName=='AWAITING'
            || $classifierName=='HOLD'
            || $classifierName=='SILENT IN THE PHONE'
            || $classifierName=='SILENCE IN THE PHONE'
            || $classifierName=='WAITING PAYMENT BANKTRANSFER'
            || $classifierName=='SILENT ON THE PHONE'
            || $classifierName=='WAITING PAYMENT TRANSFER'
            || $classifierName=='WAITING PAYMENT'
        ){
            return true;
        }
        return false;
    }

    private function isRejected($classifierName)
    {
        if(
               $classifierName=='ALREADY ORDER'
            || $classifierName=='ALREADY ORDER COD'
            || $classifierName=='PREGNANT'
            || $classifierName=='OUT OF AREA'
            || $classifierName=='DON\'T BELIEVE'
            || $classifierName=='DO NOT BELIEVE'
            || $classifierName=='OTHER LANGUANGE'
            || $classifierName=='PREGNAT'
            || $classifierName=='CANCEL ORDER'
            || $classifierName=='DON\'T BELIVE IN METHOD'
            || $classifierName=='REJECTED'
            || $classifierName=='CHANGE NUMBER'
            || $classifierName=='ORDER BY CSS'
            || $classifierName=='NO MONEY'
            || $classifierName=='CHANGE KLASYFIKATOR'
            || $classifierName=='OTHER LANGUAGE'
            || $classifierName=='ORDER BY WEB'
            || $classifierName=='DONT\' BELIVE IN METHOD'
            || $classifierName=='ALREADY ORDER DIGITAL'
            || $classifierName=='DOUBLE'
            || $classifierName=='WRONG NUMBER'
            || $classifierName=='WRONG NUMBER/PERSON'
            || $classifierName=='DON\'T BELIEVE IN METHOD'
            || $classifierName=='DONT\' BELIEVE IN METHOD'
        ){
            return true;
        }
        return false;
    }

    /**
     * @param $classifiers_id
     * @return string
     */
    private function checkAutomaticStatus($classifiers_id)
    {
        switch ($classifiers_id) {
            case 819:
            case 827:
            case 835:
                $statusOk = 'SOLD';
                break;
            case 1:
            case 2:
            case 3:
            case 4:
            case 6:
            case 8:
            case 9:
            case 10:
            case 820:
            case 828:
            case 834:
            case 836:
                $statusOk = 'ON HOLD';
                break;
            case 5:
            case 7:
            case 11:
            case 12:
            case 18:
            case 821:
            case 823:
            case 824:
            case 825:
            case 826:
            case 829:
            case 830:
            case 831:
            case 832:
            case 833:
            case 852:
                $statusOk = 'REJECTED';
                break;
            default:
                $statusOk = 'ON HOLD';
        }

        return $statusOk;
    }

    private function save(Contact $contact, $status, $record, $classifierName)
    {
        if($contact->called==0)
        {
            $contact->called=1;
//        $contact->cc_id=$record->id;
        }

        switch ($status) {
            case 'SOLD':
                if($contact->cc_reject==1 && $contact->cc_sell==0)
                {
                    $contact->postback = 0;
                }
                if($contact->cc_sell==0)
                {
                    $contact->cc_sell=1;
                    $this->changeClassifier($contact, $classifierName);
                }
                break;
            case 'REJECTED':
                if($contact->cc_sell==0)
                {
                    $contact->cc_reject=1;
                    $this->changeClassifier($contact, $classifierName);
                }
                break;
            case 'ON HOLD':
                if($contact->cc_sell==0 && $contact->classifier_name && $contact->classifier_name!=$classifierName)
                {
                    $contact->postback_hold = 0;
                }
                if($contact->cc_sell==0 && $contact->cc_reject==1 && $contact->classifier_name && $contact->classifier_name!=$classifierName)
                {
                    $contact->postback_hold = 0;
                }
                if($contact->cc_sell==0)
                {
                    $contact->cc_hold=1;
                    $this->changeClassifier($contact, $classifierName);
                }
                break;
        }

        if(isset($record->values->PRICE)){
            $contact->cc_price=$record->values->PRICE;
//           $contact->params=$record->values->PARAMS;
        }

        $contact->save();
    }

    private function changeClassifier(Contact $contact, $classifierName)
    {
        if($classifierName)
        {
            if($contact->classifier_name!=$classifierName)
            {
                $contact->classifier_name = $classifierName;
            }
        }
    }
}
