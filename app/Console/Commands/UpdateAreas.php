<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\ProvinceRepository;
use App\Repositories\CityRepository;
use App\Repositories\DistrictRepository;

class UpdateAreas extends Command {

    protected $signature = 'cron:update-areas';

    protected $description = 'Update double areas';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(ProvinceRepository $provinceRepo, CityRepository $cityRepo, DistrictRepository $districtRepo)
    {
//        $Ntb = $provinceRepo->findWhere([['id', '=', 13]])->first();
//        $Ntb->name = 'Nusa tenggara barat';
//        $Ntb->save();
//
//        $Ntt = $provinceRepo->findWhere([['id', '=', 17]])->first();
//        $Ntt->name = 'Nusa tenggara timur';
//        $Ntt->save();
//
//        $cityRepo->delete(140);
//
//        $jawaTengach = $provinceRepo->findWhere([['name', '=', 'Jawa Tengah']])->first();
//        $city = $cityRepo->create([
//            'name'        => 'Cilacap',
//            'province_id' => $jawaTengach->id
//        ]);
//
//        $districtRepo->create([
//            'name'      => 'Adipala',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Cipari',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Dayeuhluhur',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Gandrungmangu',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Jeruklegi',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Karangpucung',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Kawunganten',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Kedungrejo',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Kesugihan',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Kroya',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Majenang',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Maos',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Nusawungu',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Sampang',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Sidareja',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Wanar',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Patimuan',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Bantarsari',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Kampung laut',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Cilacap',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Cilacap selatan',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Cilacap tengah',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//        $districtRepo->create([
//            'name'      => 'Cilacap utara',
//            'city_id'   => $city->id,
//            'acommerce' => 1
//        ]);
//
//        $Sukabumi = $cityRepo->findWhere([['name', '=', 'Sukabumi']])->first();
//        $Sukabumi->province_id = 4;
//        $Sukabumi->save();
//        $provinceRepo->delete(23);
//
//        $city = $cityRepo->findWhere([['name', '=', 'Jakarta timur']])->first();
//        $districtRepo->create([
//            'name'      => 'Cipayung',
//            'city_id'   => $city->id,
//            'jayon'     => 1
//        ]);
    }
}
