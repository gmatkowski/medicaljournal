<?php

namespace App\Console\Commands;

use App\Repositories\OrderRepository;
use Illuminate\Console\Command;

class RestoreGreenCoffe extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:restore-green-coffee';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(OrderRepository $orderRepository)
    {
        $order = $orderRepository->find(81831);
        $order->languages()->updateOrCreate([
            'language_id' => 6
        ], [
            'qty'         => 3,
            'price'       => 490000
        ]);
    }
}
