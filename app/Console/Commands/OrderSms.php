<?php

namespace App\Console\Commands;

use App\Repositories\OrderRepository;
use App\Sms\SmsProvider;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;

/**
 * Class OrderSms
 * @package App\Console\Commands
 */
class OrderSms extends Command {

    /**
     * @var string
     */
    protected $signature = 'cron:order-sms';

    /**
     * @var string
     */
    protected $description = 'Send SMS to almose failure orders';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param OrderRepository $orderRepository
     * @param SmsProvider $smsProvider
     */
    public function handle(OrderRepository $orderRepository, SmsProvider $smsProvider)
    {
        $orders = $orderRepository->forCronAlmostFailure()->take(10);
        try
        {
            foreach ($orders as $order)
            {
                $recipient = Config::get('sms.prefix') . (int)$order->phone;
                $message = trans('sms.almost.failed.order');

                $smsProvider::send($recipient, $message);
                $order->reminders()->create([
                    'status'   => $order->status,
                    'repeated' => 1,
                    'type'     => 'sms'
                ]);

            }
        } catch (\Exception $e)
        {

        }

        echo Carbon::now()->format('Y-m-d H:i');
        echo "\n";
        print_r($orders->pluck('email', 'id')->toArray());
    }
}
