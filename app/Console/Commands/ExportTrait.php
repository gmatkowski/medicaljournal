<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Call\CallCenter;

trait ExportTrait {

    public function formatTime($hoursAgo)
    {
        return Carbon::now()->setTimezone('Asia/Jakarta')->subHours($hoursAgo);
    }

    public function getClassifiersList(CallCenter $callCenter)
    {
        $classifiersList = $callCenter->send([
            'action' => 'fcc-classifiers-list',
            'campaign' => $callCenter->getActiveCampaing()
        ]);
        $classifiersBody = json_decode($classifiersList->body);
        $classifiersCollection = collect($classifiersBody);

        return $classifiersCollection;
    }

    public function exportContacts(CallCenter $callCenter, $from, $to)
    {
        $exportRecords = $callCenter->send([
            'action' => 'fcc-export-records',

            'campaign' => $callCenter->getActiveCampaing(),
            'from' => $from,
            'to' => $to
        ]);
        $exportRecordsBody = json_decode($exportRecords->body);
        return $exportRecordsBody->records;
    }
}