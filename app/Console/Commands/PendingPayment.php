<?php

namespace App\Console\Commands;

use App\Call\CallCenter;
use App\Call\NetTelLists;
use App\Jobs\SendOrderReminder;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Log;

/**
 * Class PendingPayment
 * @package App\Console\Commands
 */
class PendingPayment extends Command {

    use DispatchesJobs;

    /**
     * @var string
     */
    protected $signature = 'cron:pending-payment';

    /**
     * @var string
     */
    protected $description = 'Send reminder for pending orders';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }


    public function handle(OrderRepository $orderRepository, CallCenter $callCenter)
    {
        $repeated = collect();
        $orders = $orderRepository->forReminder(0, 4, 1)->take(10);
        foreach ($orders as $order)
        {
            $rep = $repeated->search($order->email);

            $this->dispatch(
                new SendOrderReminder($order, $rep !== false)
            );

            if ($rep === false)
            {
                $repeated->push($order->email);

                if ($order->called == 0)
                {
                    try
                    {
                        $response = $callCenter->send([
                            'list_id'  => NetTelLists::$PENDING_ORDER,
                            'imie'     => $order->first_name,
                            'nazwisko' => $order->last_name,
                            'email'    => $order->email,
                            'telefon1' => $order->phone
                        ]);

                        $order->called = true;
                        $order->save();

                        if (isset($response->status) && $response->status == 'error')
                        {
                            throw new \Exception($response->reason);
                        }

                    } catch (\Exception $e)
                    {
                        //Log::notice('CallCenter Error (AUTO):', ['message' => $e->getMessage()]);
                    }
                }
            }
        }

        echo Carbon::now()->format('Y-m-d H:i');
        echo "\n";
        print_r($orders->pluck('email', 'id')->toArray());
    }
}
