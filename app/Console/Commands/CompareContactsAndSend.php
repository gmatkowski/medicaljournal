<?php

namespace App\Console\Commands;

use App\Repositories\ContactRepository;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * Class CompareContactsAndSend
 * @package App\Console\Commands
 */
class CompareContactsAndSend extends Command {

    use SendContactsTrait;

    /**
     * @var string
     */
    protected $signature = 'contacts:cs';


    /**
     * @var string
     */
    protected $description = 'Command description';

    /**
     * @var ContactRepository
     */
    protected $contacts;

    /**
     * CompareContactsAndSend constructor.
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository)
    {
        parent::__construct();

        $this->contacts = $contactRepository;
    }

    /**
     *
     */
    public function handle()
    {
        $contacts = $this->contacts->scopeQuery(function($query){
            return $query->whereRaw('email not in (select email from contacts_compare)');
        })->all();

        $this->send($contacts);
    }
}
