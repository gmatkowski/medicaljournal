<?php

namespace App\Console\Commands;

use App\Contact;
use Carbon\Carbon;
use App\Helpers\StrHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Repositories\ContactRepository as CR;

/**
 * Class PendingPayment
 * @package App\Console\Commands
 */
class SendContacts extends Command {

    use SendContactsTrait;

    /**
     * @var string
     */
    protected $signature = 'cron:send-contact';

    /**
     * @var string
     */
    protected $description = 'Push contacts co call center app';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CR $contactRepo
     */
    public function handle(CR $contactRepo)
    {
        $callCenter = app('App\Call\CallCenter');

        Log::notice('Focus cron started at: ' . Carbon::now()->format('Y-m-d H:i:s'));

        $contacts = $contactRepo->focusCallCenter(1000, 0);

        //filtering start
        $clearContacts = collect();
        $lostContacts = collect();
        foreach($contacts as $contact){
            if($contact->product!='detoclean' && $contact->product!='ultraslim' && $contact->product!='joint')
            {
                $phone = StrHelper::validatePhone($contact->phone);
                if($phone){
                    $contact->phone = $phone;
                    $clearContacts->push($contact);
                } else {
                    $lostContacts->push($contact);
                }
            }
        }
        $clearContactsCounter = $clearContacts->count();
        $lostContactsCounter = $lostContacts->count();
        if ($clearContactsCounter > 0)
        {
            Log::notice('Focus number of clearContacts: '.$clearContactsCounter.', contactsLost: '.$lostContactsCounter);
            $callCenter->setActiveCampaing('medical');
            $this->sendContacts($callCenter, $clearContacts);
        }
        if($lostContactsCounter > 0){
            foreach($lostContacts as $contact){
                $contact->invalid_phone=1;
                $contact->save();
            }
        }
        //filtering end
    }
}
