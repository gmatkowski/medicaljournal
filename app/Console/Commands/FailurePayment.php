<?php

namespace App\Console\Commands;

use App\Jobs\SendOrderReminder;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class FailurePayment
 * @package App\Console\Commands
 */
class FailurePayment extends Command {

    use DispatchesJobs;

    /**
     * @var string
     */
    protected $signature = 'cron:failure-payment';


    /**
     * @var string
     */
    protected $description = 'Send reminder for failure orders';

    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param OrderRepository $orderRepository
     */
    public function handle(OrderRepository $orderRepository)
    {
        $repeated = collect();
        $orders = $orderRepository->forReminder(4, 24, 1)->take(10);
        foreach ($orders as $order)
        {
            $this->dispatch(
                new SendOrderReminder($order, $repeated->search($order->email) !== false)
            );

            if ($repeated->search($order->email) === false)
            {
                $repeated->push($order->email);
            }

        }

        echo Carbon::now()->format('Y-m-d H:i');
        echo "\n";
        print_r($orders->pluck('email','id')->toArray());
    }
}
