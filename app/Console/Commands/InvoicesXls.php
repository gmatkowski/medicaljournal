<?php

namespace App\Console\Commands;

use Illuminate\Contracts\Filesystem\Factory as FileSystem;
use Illuminate\Console\Command;
use Excel;
use Carbon\Carbon;
use App\Jobs\CreateZipFile;
use App\Helpers\InvoiceHelper;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\File;
use App\Repositories\OrderRepository;
use App\Repositories\LanguageRepository;

class InvoicesXls extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:invoices-xls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Invoices on XLS';

    private $dateFrom = '31/08/2016';
    private $dateTo = '01/01/2017';

    /**
     * @var OrderRepository
     */
    protected $orders;

    /**
     * @var LanguageRepository
     */
    protected $languages;

    /**
     * @var InvoiceHelper
     */
    protected $invoice;

    /**
     * @var FileSystem
     */
    protected $fileSystem;

    private $products = [
        'garcinia-cambogia', 'black-mask', 'hallu-pro'
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        OrderRepository $orderRepository,
        LanguageRepository $languageRepository,
        InvoiceHelper $invoiceHelper,
        FileSystem $fileSystem
    )
    {
        $this->orders = $orderRepository;
        $this->languages = $languageRepository;
        $this->invoice = $invoiceHelper;
        $this->fileSystem = $fileSystem;
        parent::__construct();
    }

    public function handle()
    {
        if(!$this->fileSystem->disk('public')->exists('Invoices-31.08.2016-01.01.2017-promoseru.zip')) {

            $files = collect();

            foreach ($this->products as $productSymbol) {
                $dt1 = Carbon::createFromFormat('d/m/Y', $this->dateFrom)->startOfDay();
                $dt2 = Carbon::createFromFormat('d/m/Y', $this->dateTo)->startOfDay();
                $orders = $this->orders->getByDateRange($dt1, $dt2, $this->getProductId($productSymbol));
                $rows = collect();
                foreach ($orders as $key => $order) {
                    if(!is_null($order->user_id)){
                        $data = $this->invoice->create($order->id, 'get');
                        $rows->push([
                            'No.' => ++$key,
                            'Order Date' => $data['order_date'],
                            'Order number' => 'CM' . $data['order_number'],
                            'Invoice number' => $data['invoice_number'],
                            'Invoice date' => Carbon::now()->format('d/m/Y'),
                            'Ship To' => $data['name_to'] . ', ' . $data['surname_to'],
                            'Amount' => $data['price_before_vat'],
                            'Vat' => $data['vat'],
                            'Total' => $data['total_price']
                        ]);
                    }
                }
                $info = Excel::create('Invoices-' . $productSymbol . '-31.08.2016-01.01.2017', function ($excel) use ($rows) {
                    $excel->sheet('Invoices', function ($sheet) use ($rows) {
                        $sheet->fromArray($rows->toArray());
                    });
                })->store('xls', false, true);

                $files->push($info['full']);
                unset($info);
            }

            $filename = 'Invoices-31.08.2016-01.01.2017-promoseru.zip';
            $file_path = public_path('exports/' . $filename);

            $response = $this->dispatch(
                new CreateZipFile($files->toArray(), $file_path)
            );

            if ($response) {
                foreach ($files as $file) {
                    File::delete($file);
                }

                return 'Done.';
            }

        }
    }

    protected function getProductId($productSymbol)
    {
        if($productSymbol){
            return $this->languages->findWhere([['symbol', '=', $productSymbol]])->first()->id;
        }
        return 1;
    }
}
