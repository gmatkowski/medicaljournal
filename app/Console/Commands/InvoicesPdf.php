<?php

namespace App\Console\Commands;

use Illuminate\Contracts\Filesystem\Factory as FileSystem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Repositories\LanguageRepository;
use Barryvdh\Snappy\Facades\SnappyPdf;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\File;
use App\Helpers\InvoiceHelper;
use App\Jobs\CreateZipFile;
use Illuminate\Console\Command;
use Carbon\Carbon;

class InvoicesPdf extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:invoices-pdf';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Invoices on PDF';

    /**
     * @var OrderRepository
     */
    protected $orders;

    /**
     * @var LanguageRepository
     */
    protected $languages;

    /**
     * @var InvoiceHelper
     */
    protected $invoice;

    /**
     * @var FileSystem
     */
    protected $fileSystem;

    private $types = [
        'get' => 'exports/invoices-all-31.08.2016-01.01.2017-',
        'cancel' => 'exports/invoices-credit-note-31.08.2016-01.01.2017-'
    ];

    private $products = [
        'garcinia-cambogia', 'black-mask', 'hallu-pro'
    ];

    private $exportPostFix = '.zip';

    private $disk = 'public';

    private $exportsDir = 'exports';

    private $dateFrom = '31/08/2016';
    private $dateTo = '01/01/2017';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        OrderRepository $orderRepository,
        LanguageRepository $languageRepository,
        InvoiceHelper $invoiceHelper,
        FileSystem $fileSystem
    )
    {
        $this->orders = $orderRepository;
        $this->languages = $languageRepository;
        $this->invoice = $invoiceHelper;
        $this->fileSystem = $fileSystem;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach($this->types as $type => $destinationPath){
            foreach($this->products as $productSymbol){
                if(!$this->fileSystem->disk($this->disk)->exists($destinationPath.$productSymbol.$this->exportPostFix)) {
                    $dt1 = Carbon::createFromFormat('d/m/Y', $this->dateFrom)->startOfDay();
                    $dt2 = Carbon::createFromFormat('d/m/Y', $this->dateTo)->startOfDay();
                    $orders = $this->orders->getByDateRange($dt1, $dt2, $this->getProductId($productSymbol));

                    $files = collect();
                    foreach($orders as $order){
                        $data = $this->invoice->create($order->id, $type);
                        $pdf = SnappyPdf::loadView('pdf.invoice', $data);
                        $filepath = storage_path($this->exportsDir.'/invoice-'.$order->id.'.pdf');
                        $pdf->save($filepath);
                        $files->push($filepath);
                    }

                    $zip_path = public_path($destinationPath.$productSymbol.$this->exportPostFix);
                    if(!$this->fileSystem->disk($this->disk)->exists($this->exportsDir)) {
                        $this->fileSystem->disk($this->disk)->makeDirectory($this->exportsDir);
                    }

                    $response = $this->dispatch(
                        new CreateZipFile($files->toArray(), $zip_path)
                    );

                    if($response){
                        foreach ($files as $file) {
                            File::delete($file);
                        }
                    }
                }
            }
        }

        return;
    }

    protected function getProductId($productSymbol)
    {
        if($productSymbol){
            return $this->languages->findWhere([['symbol', '=', $productSymbol]])->first()->id;
        }
        return 1;
    }
}
