<?php

namespace App\Console\Commands;

use App\OrderInvoice;
use App\Repositories\OrderInvoiceRepository;
use Illuminate\Console\Command;

class ResetInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:reset-invoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset invoice numbers';

    private $invoices;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(OrderInvoiceRepository $orderInvoiceRepository)
    {
        parent::__construct();
        $this->invoices = $orderInvoiceRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $invoices = $this->invoices->all();
//        foreach($invoices as $invoice)
//        {
//            if($invoice->order_id){
//                $invoice->delete();
//            }
//        }
//
//        $invoice = $this->invoices->findWhere([['order_id', '=', null]])->first();
//        $invoice->invoice_no = 0;
//        $invoice->credit_no = 0;
//        $invoice->save();

        $invoice = new OrderInvoice();
        $invoice->invoice_no = 2163;
        $invoice->credit_no = 0;
        $invoice->save();

        $invoice = $this->invoices->findByField('invoice_no', 2164)->first();
        $invoice->delete();
    }
}
