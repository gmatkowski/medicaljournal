<?php

namespace App\Console\Commands;

use App\Jobs\SendContactReminder;
use App\Repositories\ContactRepository;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class TryLesson
 * @package App\Console\Commands
 */
class TryLesson extends Command {

    use DispatchesJobs;

    /**
     * @var string
     */
    protected $signature = 'cron:try-lesson';


    /**
     * @var string
     */
    protected $description = 'Send reminder try lesson people';


    /**
     *
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * @param ContactRepository $contactRepository
     */
    public function handle(ContactRepository $contactRepository)
    {
        $repeated = collect();
        $contacts = $contactRepository->forCronTry(24, 2);

        foreach ($contacts as $contact)
        {
            $this->dispatch(
                new SendContactReminder($contact, $repeated->search($contact->email) !== false)
            );

            if ($repeated->search($contact->email) === false)
            {
                $repeated->push($contact->email);
            }
        }

        echo Carbon::now()->format('Y-m-d H:i');
        echo "\n";
        print_r($contacts->pluck('email', 'id')->toArray());
    }
}
