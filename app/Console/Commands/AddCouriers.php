<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\CountryRepository;
use App\Repositories\ProvinceRepository;
use App\Repositories\CityRepository;
use App\Repositories\DistrictRepository;
use App\Repositories\OrderRepository;

class AddCouriers extends Command {

    protected $signature = 'cron:add-couriers';

    protected $description = 'Add couriers to districts';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(CountryRepository $countryRepo, ProvinceRepository $provinceRepo, CityRepository $cityRepo, DistrictRepository $districtRepo, OrderRepository $ordersRepo)
    {
//        $provinces = $provinceRepo->all();
//        foreach($provinces as $province){
//            $province->delete($province->id);
//        }
//
//        $couriers = [
//            'jayon' => 'jayon/jayonDistricts.csv',
//            'ninja' => 'ninja/ninjaDistricts.csv',
//            'rpx' => 'rpx/rpxDistricts.csv',
//            'asp' => 'asp/aspDistricts.csv',
//            'sap' => 'sap/sapDistricts.csv'
//        ];
//
//        $country = $countryRepo->findByField('name', 'Indonesia')->first();
//
//        foreach($couriers as $courier => $path) {
//
//            $file_path = storage_path('app/'.$path);
//
//            Excel::load($file_path, function ($reader) use ($courier, $country, $provinceRepo, $cityRepo, $districtRepo) {
//                $results = $reader->all();
//                $results->each(function ($sheet) use ($courier, $country, $provinceRepo, $cityRepo, $districtRepo) {
//                    $object = $sheet->all();
//
//                    $provinceName = strtolower($object['province']);
//                    $cityName = strtolower($object['city']);
//                    $districtName = strtolower($object['district']);
//
//                    $province = $this->insertOrGetProvince($country, $provinceName, $provinceRepo);
//                    if ($province) {
//                        $city = $this->insertOrGetCity($province, $cityName, $cityRepo);
//                        if ($city) {
//                            $district = $this->insertDistrict($city, $districtName, $districtRepo);
//                            $district->{$courier} = 1;
//                            $district->save();
//                        }
//                    }
//                });
//            });
//        }
//
//        $orders = $ordersRepo->all();
////        DB::statement('SET FOREIGN_KEY_CHECKS=0');
//        foreach($orders as $order){
//            if($order->ref!=null && $order->user_id!=null){
//               $districtName = strtolower($order->old_district);
//               $district = $districtRepo->findWhere([['name', '=', $districtName]])->first();
//               $order->address()->create([
//                    'full_name'   => $order->last_name ? $order->first__name.' '.$order->last_name : $order->first__name,
//                    'first_name'  => $order->first__name,
//                    'last_name'   => $order->last_name ? $order->last_name : $order->first__name,
//                    'postal_code' => $order->old_postalcode,
//                    'address'     => $order->old_address,
//                    'province' => $order->old_province,
//                    'city' => $order->old_city,
//                    'district_id' => $district ? $district->id : null,
//                    'subdistrict' => $order->old_subdistrict,
//               ]);
//            }
//        }
////        DB::statement('SET FOREIGN_KEY_CHECKS=1');

    }

    protected function insertOrGetProvince(\App\Country $country, $name, ProvinceRepository $provinceRepo)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $provinceRepo->findWhere([['name', '=', $name]]);

        if ($result->count() > 0)
        {
            return $result->first();
        }
        else
        {
            $province = $provinceRepo->create([
                'name'       => $name,
                'country_id' => $country->id
            ]);

            return $province;
        }
    }

    protected function insertOrGetCity(\App\Province $province, $name, CityRepository $cityRepo)
    {
        if (empty($name))
        {
            return false;
        }

        $result = $cityRepo->findWhere([['name', '=', $name]]);

        if ($result->count() > 0)
        {
            return $result->first();
        }
        else
        {
            $city = $cityRepo->create([
                'name'        => $name,
                'province_id' => $province->id
            ]);

            return $city;
        }
    }

    protected function insertDistrict(\App\City $city, $name, DistrictRepository $districtRepo)
    {
        if (empty($name))
        {
            return false;
        }

        $district = $districtRepo->create([
            'name'    => $name,
            'city_id' => $city->id
        ]);

        return $district;
    }
}
