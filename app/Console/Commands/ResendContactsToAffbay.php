<?php

namespace App\Console\Commands;

use App\Events\ContactWasCreated;
use App\Repositories\ContactRepository;
//use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Event;

class ResendContactsToAffbay extends Command {

    /**
     * @var string
     */
    protected $signature = 'resend:contacts:affbay {from} {to}';

    /**
     * @var string
     */
    protected $description = 'Resends contacts fo Affbay.';

    private $contacts;

    public function __construct(
        ContactRepository $contactRepository
    ) {
        $this->contacts = $contactRepository;
        parent::__construct();
    }

    public function handle()
    {
//        $from = $this->argument('from');
//        $to = $this->argument('to');
//
//        if ($from < $to) {
//            $this->error("from is less than to");
//        }
//
//        $fromDate = Carbon::now()->subHours($from);
//        $toDate = Carbon::now()->subHours($to);
//
//        $fromDate = Carbon::createFromFormat('Y-m-d H:i:s', '2018-06-08 02:30:00');
//        $toDate = Carbon::createFromFormat('Y-m-d H:i:s', '2018-06-08 17:00:00');

        $phones = [
//            '085828884660',
            '085227478070',
        ];

        $counter = collect();
//        $contacts = $this->contacts->getAllGarciniaInDateRange($fromDate, $toDate);
        foreach ($phones as $phone) {
            $contact = $this->contacts->findWhere([['phone', '=', $phone]])->first();
            if($contact)
            {
                Event::fire(new ContactWasCreated($contact));
                $counter->push($contact);
            }
        }
        echo $counter->count();
    }
}