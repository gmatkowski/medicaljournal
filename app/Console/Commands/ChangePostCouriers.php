<?php

namespace App\Console\Commands;

use App\Repositories\PostCodeRepository;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Console\Command;

class ChangePostCouriers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:change-post-couriers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Change Post Codes couriers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PostCodeRepository $postCodeRepo)
    {
        Excel::load(storage_path('app/areas.csv'), function ($reader) use ($postCodeRepo) {
            $results = $reader->all();
            $results->each(function ($sheet) use ($postCodeRepo) {
                $object = $sheet->all();

                $postCodeNumber = strtolower($object['postcode']);
                $oldCourierName = strtolower($object['courier']);
                $newCourierName = strtolower($object['newcourier']);

                $postCode = $postCodeRepo->findByField('name', $postCodeNumber)->first();

                if($postCode)
                {
                    $postCode->{$oldCourierName} = 0;
                    $postCode->{$newCourierName} = 1;

                    $postCode->save();
                }

            });
        });
    }
}
