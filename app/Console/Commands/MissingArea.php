<?php

namespace App\Console\Commands;

use App\Repositories\PostCodeRepository;
use App\Repositories\SubDistrictRepository;
use Illuminate\Console\Command;

class MissingArea extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:missing-area';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating nissing area';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(SubDistrictRepository $subDistrictRepo, PostCodeRepository $postCodeRepo)
    {
        $kedungLumbu = $subDistrictRepo->findWhere([['name', '=', 'kedung lumbu']])->first();
        $postCode = $postCodeRepo->findWhere([['name', '=', 57113]])->first();
        \DB::statement('SET FOREIGN_KEY_CHECKS=0');
        $kedungLumbu->post_code_id = $postCode->id;
        $kedungLumbu->save();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }

}
