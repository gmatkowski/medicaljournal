<?php

namespace App\Console\Commands;

use App\Contact;
use anlutro\cURL\cURL;
use App\Call\CallCenter;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Log;

trait SendContactsTrait {

    private function getExtId($id)
    {
        try
        {
            $data = ['service' => env('CCID_SERVICE'), 'id' => $id];
            if (Config::get('app.debug'))
            {
                $data['dev_key'] = env('DEV_KEY');
            }

            $curl = new cURL();
            $response = $curl->post(env('CCID_URL'), $data);

            if ($response->statusCode != 200)
            {
                throw new \Exception($response->statusText);
            }

            //$resArr = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $response->body), true);
            $response = json_decode($response->body);

            return isset($response->number) ? $response->number : null;
        } catch (\Exception $e)
        {
            Log::error('CCID Api error', [
                'message' => $e->getMessage()
            ]);

            return null;
        }
    }

    public function sendContacts(CallCenter $callCenter, Collection $contacts)
    {
        if ($contacts->count() > 0 && $callCenter->getCampaignId($callCenter->getActiveCampaing()))
        {
            foreach ($contacts as $contact)
            {
                $ext_id = $this->getExtId($contact->id);
                if (!$ext_id)
                {
                    Log::error('Cannot get EXT_ID from service');
                    continue;
                }

                Log::notice('Focus contact type: '.$callCenter->getActiveCampaing());

                $data = [
                    'action'   => 'fcc-add-records',
                    'campaign' => $this->mapper($contact),
                    'contacts' => collect()->push($contact),
                    'ext_id'   => $ext_id
                ];

                $response = $callCenter->send($data);
                $respBody = json_decode($response);

                if (!$respBody->success)
                {
                    Log::notice('Focus call center error: ' . $respBody->message);
                }
                else
                {
                    $contact->cc_id = $respBody->records_id[0]->fcc_id;
                    if($ext_id==$respBody->records_id[0]->ext_id)
                    {
                        $contact->cc_unique_id = $ext_id;
                    }
                    else
                    {
                        $contact->cc_unique_id = $respBody->records_id[0]->ext_id;
                    }
                    $contact->call_center = 1;
                    $contact->save();

                    Log::notice('Focus success: ' . $contact->id);
                }
                Log::info('Data', collect($respBody)->toArray());
            }
        }
    }

    private function mapper(Contact $contact)
    {
        switch ($contact->product)
        {
            case 'vervalen':
            case 'claireinstantwhitening':
            case 'manuskin':
                $campaing = 'newProducts';
                break;
            case 'nutrilashpromo':
            case 'leferyacrpromo':
            case 'forsopromo':
            case 'ultrahairpromo':
                $campaing = 'promo';
                break;
            case 'bustiere':
                $campaing = 'bustiere';
                break;
            default:
                $campaing = 'medical';
        }

        return $campaing;
    }

    public function updateContacts(CallCenter $callCenter, Collection $contacts)
    {
        if ($contacts->count() > 0)
        {
            foreach ($contacts as $contact)
            {
                Log::notice('Focus contact type: '.$callCenter->getActiveCampaing());

                $data = [
                    'action'   => 'fcc-update-records',
                    'campaign' => $contact->product,
                    'contacts' => collect()->push($contact)
                ];

                $response = $callCenter->send($data);
//                $respBody = json_decode($response);

//                if (!$respBody->success)
//                {
//                    Log::notice('Focus call center error: ' . $respBody->message);
//                    Log::info('Data', collect($respBody)->toArray());
//                }
//                else
//                {
//                    Log::notice('Focus update success: ' . $contact->id);
//                }
            }
        }
    }

}