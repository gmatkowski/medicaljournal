<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Shipping\JayonExpressProvider;
use App\Shipping\NinjaProvider;
use App\Repositories\OrderRepository;
use App\Repositories\LanguageRepository;

class DeliveryStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:delivery-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating Order Delivery Status';

    /**
     * @var OrderRepository
     */
    protected $orders;

    /**
     * @var JayonExpressProvider
     */
    protected $jayonShipper;

    /**
     * @var NinjaProvider
     */
    protected $ninjaShipper;

    /**
     * @var LanguageRepository
     */
    protected $languages;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        OrderRepository $orderRepository,
        JayonExpressProvider $jayonProvider,
        NinjaProvider $ninjaProvider,
    LanguageRepository $languageRepository
    )
    {
        $this->orders = $orderRepository;
        $this->jayonShipper = $jayonProvider;
        $this->ninjaShipper = $ninjaProvider;
        $this->languages = $languageRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->jayonStatuses()->ninjaStatuses();
    }

    public function jayonStatuses()
    {
        $orders = $this->orders->jayonSent();
        foreach($orders as $order){
            $this->jayonShipper->status($order);
        }
        return $this;
    }

    public function ninjaStatuses()
    {
        $orders = $this->orders->ninjaSent();
        foreach($orders as $order){
            $this->ninjaShipper->status($order);
        }
    }
}
