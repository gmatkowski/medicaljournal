<?php

namespace App\Console\Commands;

use App\Language;
use App\Repositories\LanguageRepository;
use App\Repositories\UserRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Console\Command;

class AlertStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:alert-stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Alerts on emails when stock is under minimum value';

    private $languages;

    private $users;

    protected $mail;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(LanguageRepository $languageRepository, UserRepository $userRepository, Mailer $mail)
    {
        parent::__construct();
        $this->languages = $languageRepository;
        $this->users = $userRepository;
        $this->mail = $mail;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $languages = $this->languages->all();
        foreach($languages as $language)
        {
            if($language->amount<$language->min_amount)
            {
                $this->sendMails($language);
            }
        }
    }

    private function sendMails(Language $language)
    {
        $users = $this->users->stockAlerts();
        foreach($users as $user)
        {
            $this->mail->send('email.stock', ['content' => Carbon::now().' stock of '.$language->name.' is below minimum('.$language->min_amount.') : STOCK('.$language->amount.')'], function ($m) use ($user)
            {
                $m->to($user->email)->subject('Alert Report');
            });
        }
    }
}
