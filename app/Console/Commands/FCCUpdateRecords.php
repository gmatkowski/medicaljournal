<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use App\Helpers\StrHelper;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use App\Repositories\ContactRepository as CR;

class FCCUpdateRecords extends Command
{
    use SendContactsTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:fcc-update-records';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updating Call Center Records';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(CR $contactRepo)
    {
        $callCenter = app('App\Call\CallCenter');

        Log::notice('Focus CONTACTS UPDATE cron started at: ' . Carbon::now()->format('Y-m-d H:i:s'));

        $contacts = $contactRepo->currentMonth();

        $clearContacts = collect();
        foreach($contacts as $contact){
            $phone = StrHelper::validateDoubleZeroes($contact->phone);
            if($phone){
                $clearContacts->push($contact);
                $phone = substr($phone, 1);
                $contact->phone = $phone;
                $contact->save();
            }
        }
        $clearContactsCounter = $clearContacts->count();
        if ($clearContactsCounter > 0) {
            $this->updateContacts($callCenter, $clearContacts);
        }

        echo $clearContactsCounter."\n";
    }
}
