<?php

namespace App\Console\Commands;

use App\Repositories\ContactRepository;
use Illuminate\Console\Command;
use App\Events\ContactWasCreated;
use Event;

class OVHResend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:ovh-resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resend all missing contacts too Affilator';

    private $contacts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContactRepository $contactRepository)
    {
        parent::__construct();
        $this->contacts = $contactRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contacts = $this->contacts->allTodays();
        foreach($contacts as $contact)
        {
            Event::fire(new ContactWasCreated($contact));
        }
    }
}
