<?php

namespace App\Console\Commands;

use Illuminate\Contracts\Filesystem\Factory as FileSystem;
use Illuminate\Foundation\Bus\DispatchesJobs;
use App\Repositories\ContactRepository;
use Illuminate\Console\Command;
use App\Jobs\CreateZipFile;
use Carbon\Carbon;
use Excel;

class Mailing extends Command
{
    use DispatchesJobs;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:mailing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all emails from 2017';

    private $dateFrom = '01/01/2017';

    private $siteName = 'Medical';

    private $products = ['cambogia', 'blackmask', 'hallupro'];

    /**
     * @var ContactRepository
     */
    protected $contacts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(
        ContactRepository $contactRepository,
        FileSystem $fileSystem
    )
    {
        $this->contacts = $contactRepository;
        $this->fileSystem = $fileSystem;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $dt1 = Carbon::createFromFormat('d/m/Y', $this->dateFrom)->startOfDay();
        $dt2 = Carbon::now();

        $files = collect();
        foreach($this->products as $product){
            $contacts = $this->contacts->getByDateRange($dt1, $dt2, $product);
            $rows = collect();
            foreach ($contacts as $contact) {
                if(!empty($contact->email)){
                    $rows->push([
                        'First Name' => $contact->first_name,
                        'Email' => $contact->email,
                    ]);
                }
            }
            $info = Excel::create($product.'Contacts2017', function ($excel) use ($rows, $product) {
                $excel->sheet($product.'Contacts2017', function ($sheet) use ($rows) {
                    $sheet->fromArray($rows->toArray());
                });
            })->store('xls', false, true);

            $files->push($info['full']);
            unset($info);
        }

        if(!$this->fileSystem->disk('public')->exists('exports')) $this->fileSystem->disk('public')->makeDirectory('exports');

        $this->dispatch(
            new CreateZipFile($files->toArray(), public_path('exports/'.$this->siteName.'Contacts2017.zip'))
        );
    }
}
