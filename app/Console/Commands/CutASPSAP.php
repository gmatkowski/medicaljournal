<?php

namespace App\Console\Commands;

use App\Repositories\PostCodeRepository;
use Illuminate\Console\Command;

class CutASPSAP extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:cutaspsap';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cutting asp and sap areas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(PostCodeRepository $postCodeRepository)
    {
        $asps = $postCodeRepository->findWhere([['asp', '=', 1]])->all();
        foreach($asps as $asp)
        {
            $asp->asp = 0;
            $asp->save();
        }
        $saps = $postCodeRepository->findWhere([['sap', '=', 1]])->all();
        foreach($saps as $sap)
        {
            $sap->sap = 0;
            $sap->save();
        }
    }
}
