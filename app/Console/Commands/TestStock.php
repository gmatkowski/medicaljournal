<?php

namespace App\Console\Commands;

use App\Repositories\LanguageRepository;
use App\Repositories\StockRepository;
use Illuminate\Console\Command;

class TestStock extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:test-stock';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Add tests records to Stocks';

    private $stocks;

    private $languages;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(StockRepository $stockRepository, LanguageRepository $languageRepository)
    {
        parent::__construct();
        $this->stocks = $stockRepository;
        $this->languages = $languageRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $garcinia = $this->languages->find(1);

        $stock = $this->stocks->create([
            'open' => 3089,
            'in' => 26,
            'out' => 125,
            'close' => 2990
        ]);
        $garcinia->stocks()->save($stock);

        $stock = $this->stocks->create([
            'open' => 2990,
            'in' => 68,
            'out' => 36,
            'close' => 3022
        ]);
        $garcinia->stocks()->save($stock);

        $stock = $this->stocks->create([
            'open' => 3022,
            'in' => 18,
            'out' => 40,
            'close' => 3000
        ]);
        $garcinia->stocks()->save($stock);

        $stock = $this->stocks->create([
            'open' => 3000,
            'in' => 20,
            'out' => 40
        ]);
        $garcinia->stocks()->save($stock);
    }
}
