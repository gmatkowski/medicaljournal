<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Repositories\ContactRepository;
use Illuminate\Console\Command;

class FoxyAdsResend extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:foxyads-resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'FoxyAds postBacks Approves and Holds beetwen 03 NOV 04:00 - NOW (Jakarta time) resend';

    private $contacts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contacts = $contactRepository;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from = Carbon::createFromDate(2017, 11, 3)->startOfDay()->addHours(4);

        $solds = $this->contacts->findWhere([['ext_client_id', '=', 10], ['postback', '=', 1], ['cc_sell', '=', 1], ['created_at', '>', $from]])->all();
        foreach($solds as $contact)
        {
            $postBack = file_get_contents('https://foxyads.ru/integration/affbay-asia/postback/status?uniq_id='.$contact->click_id.'&aim=approved&status=hold');
            if(isset($postBack))
            {
                Log::info('Resend FoxyAds Sold Post Back sent', ['contact_id' => $contact->id, 'ext_client_id' => $contact->ext_client_id, 'click_id' => $contact->click_id]);
            }
        }

        $holds = $this->contacts->findWhere([['ext_client_id', '=', 10], ['postback_hold', '=', 1], ['cc_sell', '=', 0], ['cc_reject', '=', 0], ['cc_hold', '=', 1], ['created_at', '>', $from]])->all();
        foreach($holds as $contact)
        {
            $postBack = file_get_contents('https://foxyads.ru/integration/affbay-asia/postback/status?uniq_id='.$contact->click_id.'&aim=approved&status=wait');
            if(isset($postBack))
            {
                Log::info('Resend FoxyAds Hold Post Back sent', ['contact_id' => $contact->id, 'ext_client_id' => $contact->ext_client_id, 'click_id' => $contact->click_id]);
            }
        }
    }
}
