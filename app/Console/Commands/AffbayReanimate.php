<?php

namespace App\Console\Commands;

use Event;
use Carbon\Carbon;
use Illuminate\Console\Command;
use App\Events\ContactWasCreated;
use App\Repositories\ContactRepository;

class AffbayReanimate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:affbay-reanimate {from} {to}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Re-sends contacts to Affbay';

    private $contacts;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(ContactRepository $contactRepository)
    {
        parent::__construct();
        $this->contacts = $contactRepository;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $from = $this->argument('from');
        $to = $this->argument('to');

        if($from < $to)
        {
            $this->error("from is less than to");
        }

        $fromDate = Carbon::now()->subHours($from);
        $toDate = Carbon::now()->subHours($to);

        $counter = collect();
        $contacts = $this->contacts->getAllInDateRange($fromDate, $toDate);
        foreach($contacts as $contact)
        {
            Event::fire(new ContactWasCreated($contact));
            $counter->push($contact);
        }
        echo $counter->count();
    }
}
