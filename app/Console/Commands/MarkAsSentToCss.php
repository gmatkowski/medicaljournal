<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Repositories\RoleRepository as RoleRepo;
use App\Repositories\PermissionRepository as PermissionRepo;

class MarkAsSentToCss extends Command {

    protected $signature = 'cron:mark-as-sent-to-css';

    protected $description = 'Adding mark as sent permission to css role';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(PermissionRepo $permissionRepo, RoleRepo $roleRepo)
    {
        $MaSOr = $permissionRepo->findWhere([['name', '=', 'mark-order-as-sent']])->first();
        $css = $roleRepo->findWhere([['name', '=', 'css']])->first();
        $css->attachPermission($MaSOr);
        $css->save();
    }
}
