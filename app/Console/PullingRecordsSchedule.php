<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;

class PullingRecordsSchedule
{
    /**
     * This schedules mission is to maintain minimum data transfer from call center and still get overlapping
     * to make sure every record that gets updated in call center is eventually being updated to its final state.
     * @param Schedule $schedule
     */
    public function register(Schedule $schedule)
    {
        // every thirty minutes, import data from last hour
        $schedule
            ->command('cron:contact-status-check 1 0')
            ->everyThirtyMinutes()
            ->appendOutputTo(storage_path('logs/contact-status-check.log'));
        // every hour, import data from last 12 hours
        $schedule
            ->command('cron:contact-status-check 12 0')
            ->hourly()
            ->appendOutputTo(storage_path('logs/contact-status-check.log'));
        // import from last two days
        $schedule
            ->command('cron:contact-status-check 48 0')
            ->dailyAt("20:00")
            ->appendOutputTo(storage_path('logs/contact-status-check.log'));
        // import from -4 to -1 days
        $schedule
            ->command('cron:contact-status-check 96 24')
            ->dailyAt("22:00")
            ->appendOutputTo(storage_path('logs/contact-status-check.log'));
        // import from -8 to -3 days
        $schedule
            ->command('cron:contact-status-check 192 72')
            ->dailyAt("00:00")
            ->appendOutputTo(storage_path('logs/contact-status-check.log'));
        // import from -14 to -7 days
        $schedule
            ->command('cron:contact-status-check 336 168')
            ->dailyAt("02:00")
            ->appendOutputTo(storage_path('logs/contact-status-check.log'));
        // import from -20 to -14 days
        $schedule
            ->command('cron:contact-status-check 480 168')
            ->dailyAt("04:00")
            ->appendOutputTo(storage_path('logs/contact-status-check.log'));
    }
}