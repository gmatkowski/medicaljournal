<?php

namespace App\Listeners;

use App\Call\Focus;
use App\Helpers\StrHelper;
use App\Events\ContactWasCreated;
use Illuminate\Support\Facades\Log;
use App\Console\Commands\SendContactsTrait;

class SendContactToCallCenter
{
    use SendContactsTrait;

    /**
     * @var Focus
     */
    private $callCenter;

    public function __construct()
    {
        $this->callCenter = app('App\Call\CallCenter');
    }

    public function handle(ContactWasCreated $event)
    {
        if(app()->environment() == 'production')
        {
            $contact = $event->contact();

            $clearContacts = collect();
            $lostContacts = collect();

            if($contact->product!='detoclean' && $contact->product!='ultraslim' && $contact->product!='joint' && !$contact->call_center)
            {
                $phone = StrHelper::validatePhone($contact->phone);
                if($phone){
                    $contact->phone = $phone;
                    $clearContacts->push($contact);
                } else {
                    $lostContacts->push($contact);
                }
            }

            $clearContactsCounter = $clearContacts->count();
            $lostContactsCounter = $lostContacts->count();
            if ($clearContactsCounter > 0)
            {
                Log::notice('Focus number of clearContacts: '.$clearContactsCounter.', contactsLost: '.$lostContactsCounter);
                $this->callCenter->setActiveCampaing('medical');
                $this->sendContacts($this->callCenter, $clearContacts);
            }
            if($lostContactsCounter > 0){
                foreach($lostContacts as $contact){
                    $contact->invalid_phone=1;
                    $contact->save();
                }
            }
        }
    }
}
