<?php

namespace App\Listeners;

use App\Events\OrderWasPayed;
use App\Jobs\CreateOrderFiles;
use App\Jobs\SendEmail;
use Carbon\Carbon;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

/**
 * Class EmailOrderWithProduct
 * @package App\Listeners
 */
class EmailOrderWithProduct {

    use DispatchesJobs;

    /**
     * @param OrderWasPayed $event
     */
    public function handle(OrderWasPayed $event)
    {
        $this->dispatch(
            new CreateOrderFiles($event->order)
        );

        $this->dispatch(
            new SendEmail($event->order->email, trans('order.email.payed.title', [], 'messages', $event->order->lang), 'email.order_payed', ['order' => $event->order])
        );

        $hashes = $event->order->files->pluck('hash')->toArray();
        Log::notice('Links sent; Order '.$event->order->id . ' | Hashes '.implode(' , ',$hashes));

        $event->order->status = 2;
        $event->order->sended_at = Carbon::parse();
        $event->order->save();
    }
}
