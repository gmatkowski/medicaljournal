<?php

namespace App\Listeners;

use App\Events\OrderWasCreated;
use App\Jobs\SendEmail;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class EmailOrderConfirmation
 * @package App\Listeners
 */
class EmailOrderConfirmation {

    use DispatchesJobs;

    /**
     * Handle the event.
     *
     * @param  OrderWasCreated $event
     * @return void
     */
    public function handle(OrderWasCreated $event)
    {
        if ($event->order->isCOD())
        {
            $this->dispatch(
                new SendEmail($event->order->email, trans('order.email.created.cod.title'), 'email.order_created_cod', ['order' => $event->order])
            );
        }
    }
}
