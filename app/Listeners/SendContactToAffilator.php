<?php

namespace App\Listeners;

use anlutro\cURL\Response;
use App\Contact;
use App\Events\ContactWasCreated;
use anlutro\cURL\cURL;
use App\Helpers\StrHelper;
use Illuminate\Support\Facades\Log;

class SendContactToAffilator
{
    const ENDPOINT = "AFFILATOR_CONTACT_STORE_ENDPOINT";
    const AUTH_HEADER = 'Q0awDyUvAvHBTD2szyC3RCPOuxNaQ6CKgqsiwP6y4gdCvNRFp4';

    /**
     * @var cURL
     */
    private $curl;

    public function __construct(cURL $curl)
    {
        $this->curl = $curl;
    }

    public function handle(ContactWasCreated $event)
    {
        if(app()->environment() == 'production')
        {
            $contact = $event->contact();
            $response = $this->sendRequest($contact);
            $this->log($response);
        }
    }

    /**
     * Send request to Affilator.
     * Possible response statuses:
     * 201 for successfully created resource
     * 403 for authorization error
     * 422 for data errors
     * 500 for any other errors
     * @param $contact
     * @return Response
     */
    protected function sendRequest(Contact $contact): Response
    {
        $url = getenv(self::ENDPOINT);

        $data = $this->mapToArray($contact);

        $request = $this->curl
            ->newRequest('post', $url, compact('data'))
            ->setHeader('authorization', self::AUTH_HEADER);

        return $request->send();
    }

    protected function mapToArray(Contact $contact): array
    {
        $cc = app('App\Call\Focus');

        return [
            'source'               => $cc->getSource($contact),
            'external_client_name' => $contact->external_name,
            'click_id'             => $contact->click_id,
            'pub_id'               => $contact->pub_id,
            'params'               => $contact->params,
            'name'                 => $contact->name,
            'email'                => $contact->email,
            'phone'                => $contact->phone,
            'valid_phone'          => StrHelper::validatePhone($contact->phone) ? true : false,
            'product'              => $contact->product,
            'age'                  => $contact->age,
            'db_name'              => 'medical-jurnal',
            'id'                   => $contact->id,
            'from_web'             => 'promos-seru',
            'created_at'           => $contact->created_at->format("Y-m-d H:i:s"),

            'product_uuid'         => $this->mapProductToSku($contact),
            'user_token'           => $contact->externalClient
                ? $contact->externalClient->token
                : ''
        ];
    }

    protected function mapProductToSku(Contact $contact)
    {
        switch ($contact->product) {
            case 'cambogia':
                $sku = '965d28dd-dc2a-4a6a-97f4-8602b8bc9f77';
                break;
            case 'blackmask':
                $sku = 'dfd06ea2-f459-4754-8c06-34720b483322';
                break;
            case 'hallupro':
                $sku = '387b6aa2-1120-47d9-b3a0-591df640ee1b';
                break;
            case 'waist':
                $sku = 'fd606c5e-acd0-4054-889e-27a3bfb893c1';
                break;
            case 'detoclean':
                $sku = '7e4d6fd9-1bb9-4e8d-b6da-e782d7a7fbe5';
                break;
            case 'coffee':
                $sku = '2d11025f-778c-4419-98b1-e5a51ac71f29';
                break;
            case 'joint':
                $sku = '6590e74d-4575-4ec3-b5b7-f3a9cc21207e';
                break;
            case 'posture':
                $sku = 'bbc8e290-8293-427c-88c6-97a460546199';
                break;
            case 'bra':
                $sku = '77a63631-59db-4602-9142-c1db9e373526';
                break;
            case 'powerbank':
                $sku = 'e4f8aa4a-2090-4ff3-862f-0e83e6a58025';
                break;
            case 'ultraslim':
                $sku = '3f74be50-f76e-4548-b27e-7b49cca3f870';
                break;

            case 'bustiere':
                $sku = '90147575-1308-4f14-b601-c8030d29d269';
                break;
            case 'vervalen':
                $sku = 'a543801a-3a18-4486-95bc-1ca704ce1be3';
                break;
            case 'claireinstantwhitening':
                $sku = '2c138441-4b8c-4993-ae79-0261e639c101';
                break;
            case 'manuskin':
                $sku = '55e7d87b-2697-47a1-8d19-3aa601044b99';
                break;

            case 'flexaplus':
                $sku = '49473744-051c-44fa-a356-25481e520c53';
                break;
            case 'detoxic':
                $sku = 'a47aef28-7850-453c-baa3-079f37001e34';
                break;
            case 'dietbooster':
                $sku = '91c61152-df54-4880-950e-421305a0321b';
                break;

            case 'imove':
                $sku = '60d72f29-41f2-4aaa-bcc0-ce6129298d7d';
                break;
            case 'nutrilashpromo':
                $sku = '0cc178b8-d4f5-498e-91f8-2eb43400a0db';
                break;
            case 'leferyacrpromo':
                $sku = 'a9074e61-2dfa-4d75-b770-a3fd7a7cbad6';
                break;
            case 'forsopromo':
                $sku = '6facfdb3-4402-4565-b7cd-3e346c7cd611';
                break;
            case 'ultrahairpromo':
                $sku = 'f0cb269b-4305-4370-8b57-935cb078126c';
                break;
            case 'musclegain':
                $sku = '1c42efd6-793a-4d2d-b12c-305317818d01';
                break;
            default:
                $sku = null;
        }
        return $sku;
    }

    /**
     * Log response
     * @param Response $response
     */
    protected function log(Response $response)
    {
        $array = $response->toArray();

        if (sizeof($warnings = array_get($array, 'data.warnings')))

            Log::warning("Warnings during sending contact found", $warnings);

        $message = ($response->statusCode == 201)
            ? "Contact successfully delivered"
            : "Something went wrong during sending contact";

        Log::info($message, $array);
    }
}
