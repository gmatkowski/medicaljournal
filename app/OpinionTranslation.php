<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslation
 * @package App
 */
class OpinionTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'opinion_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['name', 'description', 'icon'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function opinion()
    {
        return $this->belongsTo('App\Opinion');
    }

}
