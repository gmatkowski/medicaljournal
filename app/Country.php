<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Country
 * @package App
 */
class Country extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'countries';

    /**
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    public function provinces()
    {
        return $this->hasMany('App\Province');
    }

    public function delete()
    {
        return;
    }
}
