<?php namespace App\Call;

/**
 * Class NetTelLists
 * @package App\Call
 */
class NetTelLists {

    /**
     * @var int
     */
    public static $TRY_LESSON = 5740;
    /**
     * @var int
     */
    public static $CALL_ME = 5780;
    /**
     * @var int
     */
    public static $PENDING_ORDER = 5790;

    /**
     * @var int
     */
    public static $TESTING = 5725;
}