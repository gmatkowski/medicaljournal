<?php namespace App\Call;

interface CallCenter {

    public function send($data = []);
    public function getActiveCampaing();

}