<?php

namespace App\Call;

use App\Console\Commands\ExportTrait;
use App\Contact;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use anlutro\cURL\cURL;
use Illuminate\Support\Facades\Log;
use App\Repositories\ContactRepository;

/**
 * Class Focus
 * @package App\Call
 */
class Focus implements CallCenter {

    use ExportTrait;

    /**
     * @var string
     */
    private $baseUrl = 'https://ptdim.fcc-online.pl/external-api/';

//    private $affilatorUrl = 'http://affbay.asia/api/v1/leads/store';

    /**
     * @var string
     */
    private $key = '97023b86e77eb1855d15ed7dc501bf30';

    /**
     * @var array
     */
    private static $lists = [
        'medical' => 72,
        'newProducts' => 71,
//        'upsell_2017' => 53
        'promo' => 83,
        'bustiere' => 84
    ];

    /**
     * @var string
     */
    private $login = 'dim.admin';

    /**
     * @var string
     */
    private $domain = '@ptdim';

    /**
     * @var cURL
     */
    private $curl;

    /**
     * @var string
     */
    private $active_campaing;

//    private $contacts;

    private $orders;

    private $contacts;

    /**
     * Focus constructor.
     */
    public function __construct(OrderRepository $orderRepository, ContactRepository $contactRepository)
    {
        $this->curl = new cURL();
        $this->orders = $orderRepository;

        $this->contacts = $contactRepository;
    }

    /**
     * @param $active_campaing
     */
    public function setActiveCampaing($active_campaing)
    {
        $this->active_campaing = $active_campaing;
    }

    /**
     * @return string
     */
    public function getActiveCampaing()
    {
        return $this->active_campaing;
    }

    /**
     * @return \anlutro\cURL\Response|bool
     */
    public function getCampaignList()
    {
        return $this->send([
            'action' => 'fcc-campaigns-list'
        ]);
    }

    public function getLeads()
    {
        $this->setActiveCampaing('medical');

        $classifiersCollection = $this->getClassifiersList($this);

        $exportRecordsBodyRecords = $this->exportContacts($this, $this->formatTime(100), $this->formatTime(76));
        foreach($exportRecordsBodyRecords as $record){

//            $name = $record->values->NAME;
            $phone = $record->numbers[0];

//            $params = isset($record->values->PARAMS) ? $record->values->PARAMS : null;
//            if($params){
//                $decodedParams = base64_decode($params);
//
//                if (strpos($decodedParams, 'w1MJGU4VPK2302O61ACQBB90') !== false) {
//                    $test = 'test';
//                }
//            }

            if($phone=='082214142212'){
                $created_at = $record->values->CREATED;

                $test = 'test';

                $classifiers_id = $record->classifiers_id;
                $classifier = $classifiersCollection->first(function ($key, $value) use ($classifiers_id) {
                    return $value->id == $classifiers_id;
                });
            }

        }
    }

    /**
     * @param array $data
     * @return \anlutro\cURL\Response|bool
     */
    public function send($data = [])
    {
        $ccParams = $this->getParams($data);
        $params = array_merge($this->basicParams(), $ccParams);
        $url = $this->baseUrl . $data['action'];

        $request = $this->curl->newRequest('post', $url, ['json' => json_encode($params)])
            ->setHeader('Expect: ', '');
        $response = $request->send();

//        $respBody = json_decode($response);
//
//        if($data['action']=='fcc-add-records' && $respBody->success){
//            $this->shouldIsendToAffilator($ccParams);
//        }

        return $response;
    }

//    private function shouldIsendToAffilator($ccParams)
//    {
//        foreach($ccParams['records'] as $record)
//        {
//            $unique_id = isset($record['values']['UNIQUE_ID']) ? $record['values']['UNIQUE_ID'] : null;
//            $source = isset($record['values']['SOURCE']) ? $record['values']['SOURCE'] : null;
//            $ext_client_name = isset($record['values']['EXT_CLIENT_NAME']) ? $record['values']['EXT_CLIENT_NAME'] : null;
//            $phone = isset($record['values']['PHONE']) ? $record['values']['PHONE'] : null;
//            $params = isset($record['values']['PARAMS']) ? $record['values']['PARAMS'] : null;
//            $product = isset($record['values']['PRODUCT']) ? $record['values']['PRODUCT'] : null;
//
//            if($phone && (($params && $params!='no_params' && $this->validForAffilator($params)) || $source=='api')){
//
//                $this->sendToAffilator($unique_id, $source, $ext_client_name, $phone, $params, $product);
//
//            }
//        }
//    }

    private function validForAffilator($params)
    {
        $decodedParams = base64_decode($params);

        if ((strpos($decodedParams, 'tr_id') !== false || strpos($decodedParams, 'click_id') !== false) && strpos($decodedParams, 'campaign_id') !== false) {

           return true;
        }
        return false;
    }

//    private function sendToAffilator($unique_id, $source, $ext_client_name, $phone, $params, $product)
//    {
//        $request = $this->curl->newJsonRequest('post', $this->affilatorUrl, [
//            'values.UNIQUE_ID' => $unique_id,
//            'values.SOURCE' => $source,
//            'values.EXT_CLIENT_NAME' => $ext_client_name,
//            'values.PHONE' => $phone,
//            'values.PARAMS' => $params,
//            'values.PRODUCT' => $product
//        ])
//            ->setHeader('Authorization', 'Tn7WS4rzA8WNVye9y2TgzJ25RT92W1aTpSucbMZfZ0yxeQ6cAD');
//        $request->send();
//    }

    /**
     * @return array
     */
    private function basicParams()
    {
        $change = str_random(50);
        $params = [
            'login'  => $this->login . $this->domain,
            'change' => $change,
            'hash'   => md5($this->login . $change . $this->key),
            'method' => 'md5',
        ];

        return $params;
    }

    /**
     * @param $data
     * @return array
     */
    protected function getParams($data)
    {
        switch ($data['action'])
        {
            case 'fcc-add-records' :
                return $this->addRecords($data);
                break;
            case 'fcc-update-records' :
                return $this->updateRecords($data);
                break;
            case 'fcc-campaigns-list':
                return [];
                break;
            case 'fcc-export-records':
                return $this->exportRecords($data);
                break;
            case 'fcc-classifiers-list':
                return $this->classifiersList($data);
                break;
        }
    }

    /**
     * @param $data
     * @return array
     */
    public function addRecords($data)
    {
        $params = [
            'campaigns_id' => $this->getCampaignId($data['campaign']),
        ];
        foreach ($data['contacts'] as $contact)
        {
            $params['records'][] = [
                'values'  => array_merge($this->getValues($contact), ['UNIQUE_ID' => (string)$data['ext_id']]),
                'numbers' => [$contact->phone],
//                'emails'  => [(string)$contact->email],
                /*'skills'         => [],
                'priority'       => '',
                'private'        => '',
                'recall'         => '',
                'classifiers_id' => '',*/
                'ext_id'  => $data['ext_id'],
                'segment' => $contact->product
            ];
        }

        return $params;
    }

    /**
     * @param $data
     * @return array
     */
    public function updateRecords($data)
    {
        $params = [
            'campaigns_id' => $this->getCampaignId($data['campaign']),
        ];
        foreach ($data['contacts'] as $contact)
        {
            $params['updates'][] = [
                'records_id' => $contact->cc_id,
                'values'  => $this->getValues($contact),
                'numbers' => [$contact->phone],
//                'emails'  => [(string)$contact->email],
                /*'skills'         => [],
                'priority'       => '',
                'private'        => '',
                'recall'         => '',
                'classifiers_id' => '',*/
            ];
        }

        return $params;
    }

    protected function getValues(Contact $contact)
    {
        return [
            'NAME'  => $contact->name,
            'EMAIL' => $contact->email ? (string)$contact->email : '',
            'AGE'   => $contact->age ? (string)$contact->age : '',
            'PHONE' => $contact->phone,
            'CREATED' => Carbon::parse($contact->created_at)->format('Y-m-d H:i:s'),
            'SEGMENT' => $contact->product.'_'.Carbon::parse($contact->created_at)->weekOfYear.'_W_'.Carbon::now()->year.'_'.$this->getPartnerFromParams($contact),
            'PARTNER' => $this->getPartnerFromParams($contact),
            'PRODUCT' => $contact->product,
            'PRODUK' => $contact->product,
            'EXT_CLIENT_ID' => $contact->ext_client_id ? (string)$contact->ext_client_id : 'no_ext_client',
            'EXT_CLIENT_NAME' => $contact->external_name ? $contact->external_name : 'no_ext_client_name',
            'PARAMS' => $this->getValuesParams($contact),
            'SOURCE' => $this->getSource($contact),
            'COUNTRY' => 'ID',
            'CURRENCY' => 'IDR',
            'PRICE' => $contact->order && $contact->order->price ? (string)$contact->order->price : '',
            'CONTACT_ID' => (string)$contact->id,
            'FROM_WEB' => 'promos-seru',
        ];
    }

    public function exportRecords($data){

        $params = [
            'campaigns_id' => $this->getCampaignId($data['campaign']),
            'from' => $data['from']->format('Y-m-d H:i:s'),
            'to' => $data['to']->format('Y-m-d H:i:s')
        ];

        return $params;
    }

    public function classifiersList($data){

        $params = [
            'campaigns_id' => $this->getCampaignId($data['campaign'])
        ];

        return $params;
    }

    public function getCampaignId($campaign)
    {
        return isset(self::$lists[$campaign]) ? self::$lists[$campaign] : '';
    }

    public function getSource(Contact $contact)
    {
        if($contact->ext_client_id && $contact->external_name) {
           return 'api' ;
        }
        if($contact->params && $this->validForAffilator($contact->params)){
            return 'affilator';
        }
        return 'web';
    }

    public function getPartnerFromParams(Contact $contact)
    {
        if($contact->params){
            $params = base64_decode($contact->params);
            $click_id = 'no_partner';
            if (strpos($params, 'click_id') !== false) {
                $click_id = explode('click_id=', $params)[1];
            }
            if (strpos($params, 'tr_id') !== false) {
                $click_id = explode('tr_id=', $params)[1];
            }
            if (strpos($click_id, '&') !== false) {
                $click_id = explode('&', $click_id)[0];
            }
            return $click_id;

        }elseif($contact->click_id){
            return $contact->click_id;
        }
        return 'no_partner';
    }

    public function getCampaignIdFromParams(Contact $contact)
    {
        if($contact->params){
            $params = base64_decode($contact->params);
            $campaignId = 'no_campaign_id';
            if (strpos($params, 'campaign_id') !== false) {
                $campaignId = explode('campaign_id=', $params)[1];
            }
            if (strpos($campaignId, '&') !== false) {
                $campaignId = explode('&', $campaignId)[0];
            }
            return $campaignId;

        }
        return 'no_campaign_id';
    }

    public function getV1(Contact $contact)
    {
        if($contact->params){
            $params = base64_decode($contact->params);
            $v1 = 'no_v1';
            if (strpos($params, 'v1') !== false) {
                $v1 = explode('v1=', $params)[1];
            }
            if (strpos($v1, '&') !== false) {
                $v1 = explode('&', $v1)[0];
            }
            return $v1;

        }
        return 'no_v1';
    }

    public function getValuesParams(Contact $contact)
    {
        if($contact->params){
            return $contact->params;
        }elseif($contact->click_id){
            $params = base64_encode('tr_id='.$contact->click_id);
            return $params;
        }
        return 'no_params';
    }
}