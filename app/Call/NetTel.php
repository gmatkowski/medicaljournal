<?php namespace App\Call;

use anlutro\cURL\cURL;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Validator;

/**
 * Class NetTel
 * @package App\Call
 */
class NetTel implements CallCenter {

    /**
     * @var cURL
     */
    protected $curl;

    /**
     * @var string
     */
    protected static $url = 'http://api.net-tel.it/ploutsourcing/import2gad4kursy.php';

    /**
     *
     */
    public function __construct()
    {
        $this->curl = new cURL();
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function validate($data)
    {
        return Validator::make($data, [
            'telefon1' => 'required',
            'list_id'  => 'required|numeric'
        ]);
    }


    /**
     * @param array $data
     * @return object
     * @throws \Exception
     */
    public function send($data = [])
    {
        $data = collect($data);
        $validator = $this->validate($data->toArray());

        if ($validator->fails())
        {
            throw new \Exception($validator->errors()->first());
        }

        $data = $this->prepareData($data);

        $result = $this->call(self::$url, $data->toArray());

        return $this->parse($result);
    }

    /**
     * @param $data
     * @return array
     */
    protected function prepareData(Collection $data)
    {
        $data->put('auth_code', md5("PL0" . preg_replace("/[^0-9]/", "", $data->get('telefon1'))));
        $data->put('jsontab', true);

        return $data;
    }

    /**
     * @param $response
     * @return object
     */
    protected function parse($response)
    {
        return json_decode($response->body);
    }

    /**
     * @param $url
     * @param $data
     * @return \anlutro\cURL\Response
     */
    protected function call($url, $data)
    {
        return $this->curl->post($url, $data);
    }

}