<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
/**
 * Class Order
 * @package App
 */
class ExternalClient extends Model implements Transformable {

    use TransformableTrait;

    protected $table = 'external_clients';

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'token'
    ];

    public function contacts() {
        return $this->hasMany('App\Contact', 'ext_client_id');
    }

    public function orders() {
        return $this->hasMany('App\Order', 'ext_client_id');
    }
}
