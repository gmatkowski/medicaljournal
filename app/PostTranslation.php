<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class PostTranslation
 * @package App
 */
class PostTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'post_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['name', 'description'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
            return $this->belongsTo('App\Post');
    }

}
