<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FaqTranslation
 * @package App
 */
class FaqTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'faq_translations';

    /**
     * @var bool
     */
    public $timestamps = false;
    /**
     * @var array
     */
    public $fillable = ['name', 'description'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function faq()
    {
        return $this->belongsTo('App\Faq');
    }

}
