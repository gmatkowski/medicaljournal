<?php
Route::any('managepanel', ['as' => 'css.home', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('admin', function () {
    return redirect('managepanel');
});
Route::controller('order-transfer', 'TransferOrderController', [
    'postStatus' => 'transfer.order.status',
    'anyTest'    => 'transferuj.test'
]);

Route::controller('mailgun/events', 'MailgunEventController', [
    'postEvent' => 'mailgun.event.post',
]); 

Route::post('order/get-options', ['as' => 'order.options', 'uses' => 'OrderController@postGetOptions']);

Route::controller('test', 'TestController');

$subdomainRoutes = function () {
    Route::get('masker-hitam', function () {
        return redirect('http://promo-goodies.com/masker-hitam');
    });
    Route::get('masker-hitam1', function () {
        return redirect('http://promo-goodies.com/masker-hitam');
    });
    Route::get('masker-hitam2', function () {
        return redirect('http://promo-goodies.com/masker-hitam');
    });
    Route::get('masker-hitam2_main', function () {
        return redirect('http://promo-goodies.com/masker-hitam');
    });
//    Route::get('thai', ['as' => 'page.maskPreOrderThai', 'uses' => 'WelcomeController@maskPreOrderThai']);

    Route::get('garcinia-cambogia_test', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia1');
    });
    Route::get('garcinia-cambogia_main', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia1');
    });
    Route::get('garcinia-cambogia', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia1');
    });
    Route::get('garcinia-cambogia1', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia1');
    });
    Route::get('garcinia-cambogia1_main', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia1');
    });
    Route::get('garcinia-cambogia2', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia2');
    });
    Route::get('garcinia-cambogia2pop', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia2');
    });
    Route::get('garcinia-cambogia2_main', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia2');
    });
//    Route::get('garcinia-cambogia2_mainpop', ['as' => 'page.garciniaCambogia2_mainpop', 'uses' => 'WelcomeController@garciniaCambogia2_mainpop']);
//    Route::get('garcinia-cambogia2popup', ['as' => 'page.garciniaCambogia2popup', 'uses' => 'WelcomeController@garciniaCambogia2popup']);
    Route::get('garcinia-cambogia2_main1', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia2');
    });
    Route::get('garcinia-cambogia3', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia2');
    });
//    Route::get('garcinia', ['as' => 'page.intro', 'uses' => 'WelcomeController@intro']);
    Route::get('garcinia2', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia2');
    });
    Route::get('garcinia2_main', function () {
        return redirect('http://promo-goodies.com/garcinia-cambogia2');
    });
//    Route::get('anti_oksidan_alami', ['as' => 'page.garcinia_forte', 'uses' => 'WelcomeController@garcinia_forte']);
//    Route::get('garcinia/composition', ['as' => 'page.introcomposition', 'uses' => 'WelcomeController@introcomposition']);
//    Route::get('garcinia/faq', ['as' => 'page.introfaq', 'uses' => 'WelcomeController@introfaq']);
//    Route::get('garcinia/terms-regulations', ['as' => 'page.introterms', 'uses' => 'WelcomeController@introterms']);
//    Route::get('garcinia/terms-cookies', ['as' => 'page.introcookies', 'uses' => 'WelcomeController@introcookies']);
//    Route::get('drhallux/terms-regulations', ['as' => 'page.halluterms', 'uses' => 'WelcomeController@halluterms']);
//    Route::get('drhallux/terms-cookies', ['as' => 'page.hallucookies', 'uses' => 'WelcomeController@hallucookies']);
//    Route::get('garcinia/metamorphosis', ['as' => 'page.intrometamorphosis', 'uses' => 'WelcomeController@intrometamorphosis']);
    Route::get('drhallux', function () {
        return redirect('http://promo-goodies.com/drhallux');
    });
    Route::get('drhallux2', function () {
        return redirect('http://promo-goodies.com/drhallux');
    });
    Route::get('drhallux_1', function () {
        return redirect('http://promo-goodies.com/drhallux');
    });

    Route::get('predrhallux_main', function () {
        return redirect('http://promo-goodies.com/drhallux');
    });

    Route::get('privacy-policy', ['as' => 'page.privacyPolicy', 'uses' => 'WelcomeController@privacyPolicy']);

    Route::get('leadslist', ['as' => 'page.leadslist', 'uses' => 'WelcomeController@leadslist']);
    Route::get('garciniaicc', ['as' => 'page.garciniaicc', 'uses' => 'WelcomeController@garciniaicc']);
    Route::get('blackmaskicc', ['as' => 'page.blackmaskicc', 'uses' => 'WelcomeController@blackmaskicc']);
    Route::post('internalicc/{id}', ['uses' => 'WelcomeController@internaliccForm']);

    Route::get('externalicc', ['as' => 'page.externalicc', 'uses' => 'WelcomeController@externalicc']);
    Route::post('externalicc/{id}', ['uses' => 'WelcomeController@externaliccForm']);

    Route::get('invalidphones', ['as' => 'page.invalidphones', 'uses' => 'WelcomeController@invalidPhones']);

    Route::get('missingareas', ['as' => 'page.missingareas', 'uses' => 'WelcomeController@missingAreas']);

//    Route::get(LaravelLocalization::transRoute('routes.order'), ['as' => 'page.order', 'uses' => 'PageController@orderForm']);
//    Route::get('/order_test', ['as' => 'page.order.test', 'uses' => 'PageController@orderFormTest']);

    Route::any('order/newsletter', ['as' => 'order.newsletter', 'uses' => 'OrderController@postNewsletter']);

    Route::any('confirm', ['as' => 'order.confirm', 'uses' => 'OrderController@postCheckout']);
//    Route::get('/typage', ['as' => 'page.typ', 'uses' => 'PageController@orderTyp']);
    Route::get('checkout/{product}', ['as' => 'page.checkout', 'uses' => 'PageController@orderCheckout']);
    Route::post('checkout/form', ['as' => 'page.checkout.form', 'uses' => 'PageController@postOrderCheckout']);
    Route::get('confirm/{product}/{id}', ['as' => 'page.confirm', 'uses' => 'PageController@orderConfirm']);
//    Route::get('confirm/{product}/{id}&contact_id={contact}', ['as' => 'page.confirm', 'uses' => 'PageController@orderConfirm']);
    Route::post('orderAddress/edit', ['as' => 'page.editOrderAddress', 'uses' => 'PageController@editOrderAddress']);

    Route::get('usersForNewPanel', ['as' => 'get.usersForNewPanel', 'uses' => 'Admin\ExportController@usersForNewPanel']);
//    Route::get('areas', ['as' => 'get.areas', 'uses' => 'Admin\ExportController@areas']);
//    Route::get('mails', ['as' => 'get.mails', 'uses' => 'Admin\ExportController@getMailsFromFile']);
//    Route::get('couriers', ['as' => 'get.couriers', 'uses' => 'Admin\ExportController@getCouriersReport']);
    Route::get('allmails', ['as' => 'get.allMails', 'uses' => 'Admin\ExportController@getMailReport']);
//    Route::get('soldmails', ['as' => 'get.soldMails', 'uses' => 'Admin\ExportController@getMailReportSold']);
//    Route::get('unsoldmails', ['as' => 'get.unsoldMails', 'uses' => 'Admin\ExportController@getMailReportUnsold']);
//    Route::get('params', ['as' => 'get.params', 'uses' => 'Admin\ExportController@getParams']);

    Route::get('api/contact/test/connection', ['as' => 'api.test', 'uses' => 'ExternalClientController@testConnection']);

    Route::post('api/token/generate', ['as' => 'api.generateToken', 'uses' => 'ExternalClientController@generateToken']);
    Route::post('api/make/contact/{product_name?}', ['as' => 'api.makeOrder', 'uses' => 'ExternalClientController@makeContact']);
    Route::post('api/make/contacts', ['as' => 'api.makeOrder', 'uses' => 'ExternalClientController@makeContacts']);
//    Route::post('api/post/status/multiple', ['as' => 'api.postStatusMultiple', 'uses' => 'ExternalClientController@postStatusMultiple']);

    Route::get('api/get/externalCli', ['as' => 'api.makeOrder', 'uses' => 'ExternalClientController@getExternalCli']);

    Route::get('api/send/postBacks', ['as' => 'api.postBacks', 'uses' => 'ExternalClientController@postBacks']);
    Route::post('api/send/postBacks', ['as' => 'api.postBacks', 'uses' => 'ExternalClientController@sendPostBacks']);

    Route::get('api/delivery/{id}', ['as' => 'api.deliveryId', 'uses' => 'Admin\OrderController@getDelivery']);

    Route::post('api/ninja/hook', ['as' => 'api.ninjaHook', 'uses' => 'Admin\OrderController@postNinja']);

//   Route::get('codOrderMade', ['as' => 'get.codOrderMade', 'uses' => 'Admin\ExportController@codOrderMade']);
//   Route::get('codOrderSent', ['as' => 'get.codOrderSent', 'uses' => 'Admin\ExportController@codOrderSent']);
//   Route::get('codOrderDelivered', ['as' => 'get.codOrderDelivered', 'uses' => 'Admin\ExportController@codOrderDelivered']);
//
//   Route::get('noncodOrderMade', ['as' => 'get.noncodOrderMade', 'uses' => 'Admin\ExportController@noncodOrderMade']);
//   Route::get('noncodOrderSent', ['as' => 'get.noncodOrderSent', 'uses' => 'Admin\ExportController@noncodOrderSent']);
//   Route::get('noncodOrderDelivered', ['as' => 'get.noncodOrderDelivered', 'uses' => 'Admin\ExportController@noncodOrderDelivered']);
};

// korting-harga.com -> http://promos-seru.com
Route::group(['domain' =>  env('PROD_URL')], $subdomainRoutes);
// http://penawaran-super.com
Route::group(['domain' =>  env('PROD_URL2')], $subdomainRoutes);

Route::post('pelajaran-sample', ['as' => 'page.try.send', 'uses' => 'WelcomeController@tryLesson']);

// medical-jurnal.com -> http://agenda-kesehatan.com
Route::group(['middleware' => ['promotion']], function () {
    Route::group(
        [
            'prefix'     => LaravelLocalization::setLocale(),
            'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localize']
        ],
        function () {
            Route::any('/', ['as' => 'page.index', 'uses' => 'WelcomeController@index']);

            Route::get('penemuan-yang-menonaktifkan-gen-obesitas_test', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });
            Route::get('penemuan-yang-menonaktifkan-gen-obesitas', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });
            Route::get('penemuan-yang-menonaktifkan-gen-obesitas_main', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });
            Route::get('penemuan-yang-menonaktifkan-gen-obesitas_d', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });
            Route::get('penemuan-yang-menonaktifkan-gen-obesitas-{letter}', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });
            Route::get('perkelahian-antara-wanita', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });

            Route::get('jajananlaki', function () {
                return redirect('http://promo-goodies.com/penirum_id');
            });
            Route::get('cara-memperbesar-alat-vital-tanpa-meninggalkan-rumah', function () {
                return redirect('http://promo-goodies.com/penirum_id');
            });
            Route::get('cara-memperbesar-alat-vital-tanpa-meninggalkan-rumah_a', ['as' => 'page.adcash', 'uses' => 'WelcomeController@adcash']);
            Route::get('rahasia-kepuasan-wanita', function () {
                return redirect('http://promo-goodies.com/penirum_id');
            });
            Route::get('masker-hitam', ['as' => 'page.mask', 'uses' => 'WelcomeController@blackMask']);
//            Route::get('thaiPre', ['as' => 'page.mask', 'uses' => 'WelcomeController@blackMaskThai']);
//            Route::get('rahasia-menurunkan-berat-badan', ['as' => 'page.rahasia', 'uses' => 'WelcomeController@rahasia']);
//            Route::get('rahasia-menurunkan-berat-badan_main', ['as' => 'page.rahasia_main', 'uses' => 'WelcomeController@rahasia_main']);
//            Route::get('bobot-tubuh-arya-permana-mulai-menyusut', ['as' => 'page.bobot', 'uses' => 'WelcomeController@bobot']);
//            Route::get('kisah-nyata-cowok-bali', ['as' => 'page.kisah', 'uses' => 'WelcomeController@kisah']);
//            Route::get('senso-duo', ['as' => 'page.sensoduo', 'uses' => 'WelcomeController@sensoduo']);
//            Route::get('teknologi-fbi-dan-kgb-sekarang-digunakan-oleh-orang-biasa', ['as' => 'page.teknologi', 'uses' => 'WelcomeController@teknologi']);
//            Route::get('cinta-sejati-cowok-playboy', ['as' => 'page.cinta', 'uses' => 'WelcomeController@cinta']);
//            Route::get('terobosan-dalam-memerangi-keriput', ['as' => 'page.terobosan', 'uses' => 'WelcomeController@terobosan']);
//            Route::get('terobosan-dalam-memerangi-keriput_test', ['as' => 'page.terobosan_test', 'uses' => 'WelcomeController@terobosan_test']);
            Route::get('predrhallux', function () {
                return redirect('http://promo-goodies.com/drhallux');
            });

//            Route::get('aynish', ['as' => 'page.aynish', 'uses' => 'WelcomeController@aynish']);
//            Route::get('aynish_main', ['as' => 'page.aynish_main', 'uses' => 'WelcomeController@aynish_main']);

            Route::get('garcinia-cambogia', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });
            Route::get('garcinia3', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });
            Route::get('garcinia3_main', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });

//            Route::get('anda_ingin_makanan_sehat', ['as' => 'page.andaingin', 'uses' => 'WelcomeController@andaingin']);
            Route::get('garcinia_cambogia_tumbuh', function () {
                return redirect('http://promo-goodies.com/garcinia-cambogia1');
            });

//            Route::get('amerika-memiliki-rekor-dalam', ['as' => 'page.amerika', 'uses' => 'WelcomeController@amerika']);
//            Route::get('amerika-memiliki-rekor', ['as' => 'page.amerika_lite', 'uses' => 'WelcomeController@amerika_lite']);

//            Route::get('diet-ala-selebriti', ['as' => 'page.dietAlaSelebriti', 'uses' => 'WelcomeController@dietAlaSelebriti']);

            Route::get('privacy-policy', ['as' => 'page.privacyPolicy', 'uses' => 'WelcomeController@privacyPolicy']);

            Route::get(LaravelLocalization::transRoute('routes.contact'), ['as' => 'page.contact', 'uses' => 'PageController@getContact']);
            Route::get(LaravelLocalization::transRoute('routes.composition'), ['as' => 'page.composition', 'uses' => 'PageController@getComposition']);
            Route::get(LaravelLocalization::transRoute('routes.questions'), ['as' => 'page.questions', 'uses' => 'PageController@getQuestions']);
        }
    );

    Route::controller('order', 'OrderController', [
        'postPlaceOrder' => 'order.place',
        'getDownload'    => 'order.download',
        'postCheckout'   => 'order.checkout.save',
        'getRecreate'    => 'order.recreate',
        'getSpecial'     => 'order.special'
    ]);

    Route::get('reset-promotion/{token}', ['as' => 'promotion.reset', 'uses' => 'CommonController@getResetPromotion']);

    //Route::controller('common', 'CommonController');

    Route::group(['prefix' => 'managepanel'], function () {
        Route::group(['middleware' => ['auth']], function () {

            Route::get('/', ['as' => 'admin.home', 'uses' => 'Admin\HomeController@index']);

            Route::get('logout', ['as' => 'admin.logout', 'uses' => 'Auth\AuthController@getLogout']);

            Route::group(['prefix' => 'consultant', 'middleware' => ['role:consultant']], function () {
                Route::get('/', ['as' => 'consultant.home', 'uses' => 'Admin\Consultant\DashboardController@index']);
                Route::controller('orders', 'Admin\Consultant\OrderController', [
                    'getIndex'     => 'consultant.orders',
                    'getDelete'    => 'consultant.orders.delete',
                    'getDetails'   => 'consultant.orders.details',
                    'postEditable' => 'consultant.orders.editable',
                    'getCreate'    => 'consultant.orders.create',
                    'postStore'    => 'consultant.orders.store',
                ]);

            });

            Route::controller('orders', 'Admin\OrderController', [
                'getIndex'   => 'admin.orders',
                'getDelete'  => 'admin.orders.delete',
                'getDetails' => 'admin.orders.details',
                'getResetIp' => 'admin.orders.reset',
                'getReport'  => 'admin.orders.report',

                'getCreate'                  => 'orders.create',
                'postStore'                  => 'orders.store',
                'getEdit'                    => 'admin.orders.edit',
                'postUpdate'                 => 'admin.orders.update',
                'postEditable'               => 'admin.orders.editable',
                'anyStatusList'              => 'admin.orders.status.list',
                'postExport'                 => 'admin.orders.export',
                'getInvoice'                 => 'admin.orders.invoice',
                'postInvoices'               => 'admin.orders.invoices',
//                'postExportInvoices'         => 'admin.orders.exportInvoices',
                'getToCod'                   => 'admin.orders.to.cod',
                'getCodToConfirm'            => 'admin.orders.to.confirm',
                'getCodArchived'             => 'admin.orders.archived',
                'getCodArchive'              => 'admin.orders.archive',
                'getCodRestore'              => 'admin.orders.restore',
                'getCodDuplicated'           => 'admin.orders.attempts',
                'getAfterCod'                => 'admin.orders.after.cod',
                'getBankTransfer'            => 'admin.orders.bank.transfer',
                'getBankTransferAfterCod'    => 'admin.orders.bank.transfer.after.cod',
                'getShipmentSend'            => 'admin.orders.shipment.send',
                'getShipmentUnsend'          => 'admin.orders.shipment.unsend',
                'postShipmentSendMultiple'   => 'admin.orders.shipment.send.multiple',
                'getShipmentMark'            => 'admin.orders.shipment.mark',
                'getShipmentConfirm'         => 'admin.orders.shipment.confirm',
                'getShipmentToNotSent'       => 'admin.orders.shipment.tonotsent',
                'getBankTransferConfirm'     => 'admin.orders.bank.transfer.confirm',
                'getCallsSave'               => 'admin.orders.calls.save',
                'getEmailSave'               => 'admin.orders.email.save',

                'getToList'                  => 'admin.orders.to.list',

                'getInvoices' => 'admin.orders.invoices',
                'getCustomerData' => 'admin.orders.cdata',
            ]);

            Route::controller('contacts', 'Admin\ContactController', [
                'getIndex'   => 'admin.contacts',
                'getDelete'  => 'admin.contacts.delete',
                'postExport' => 'admin.contacts.export',
                'getContacts' => 'admin.contacts.contacts'
            ]);

            Route::controller('external', 'Admin\ExternalClientController', [
                'getIndex'   => 'admin.external.clients',
                'postExport' => 'admin.external.export'
            ]);

            Route::controller('externalstats', 'Admin\ExternalStatsController', [
                'getIndex'   => 'admin.externalstats.clients'
            ]);

            Route::controller('personalstats', 'Admin\ExportController', [
                'getIndex'   => 'admin.personalstats.index'
            ]);

            Route::get('FunCPA', ['as' => 'page.externaliccFunCPA', 'uses' => 'WelcomeController@externaliccFunCPA']);
            Route::get('affninja', ['as' => 'page.externaliccAffninja', 'uses' => 'WelcomeController@externaliccAffninja']);

            Route::controller('blog', 'Admin\PostController', [
                'getIndex'  => 'admin.blog',
                'getDelete' => 'admin.blog.delete',

                'getEdit'    => 'admin.blog.edit',
                'postUpdate' => 'admin.blog.update',

                'getCreate' => 'admin.blog.create',
                'postStore' => 'admin.blog.store',
            ]);

            Route::group(['middleware' => ['role:admin|ccadmin|headcc']], function () {
                Route::controller('users', 'Admin\UserController', [
                    'getIndex'   => 'admin.users',
                    'getDelete'  => 'admin.users.delete',
                    'getEdit'    => 'admin.users.edit',
                    'getCreate'  => 'admin.users.create',
                    'postStore'  => 'admin.users.store',
                    'postUpdate' => 'admin.users.update',
                ]);
            });

            Route::group(['middleware' => ['role:admin|ccadmin']], function () {
                Route::controller('pages', 'Admin\PageController', [
                    'getIndex'  => 'admin.pages',
                    'getDelete' => 'admin.pages.delete',

                    'getEdit'    => 'admin.pages.edit',
                    'postUpdate' => 'admin.pages.update',

                    'getCreate' => 'admin.pages.create',
                    'postStore' => 'admin.pages.store',
                ]);

                Route::controller('products', 'Admin\LanguageController', [
                    'getIndex'  => 'admin.languages',
                    'getDelete' => 'admin.languages.delete',

                    'getEdit'    => 'admin.languages.edit',
                    'postUpdate' => 'admin.languages.update',

                    'getCreate'           => 'admin.languages.create',
                    'postStore'           => 'admin.languages.store',
                    'getPackages'         => 'admin.languages.packages',
                    'postPackagesSave'    => 'admin.languages.store.package',
                    'getRenewLinks'       => 'admin.languages.renew.links',
                    'getRenewLinksCreate' => 'admin.languages.renew.links.create',
                ]);

                Route::controller('product/packages', 'Admin\LanguagePackageController', [
                    'getIndex' => 'admin.languages.packages',
                    'postSave' => 'admin.languages.store.package',
                ]);

                Route::controller('product/renew-links', 'Admin\LanguageRenewLinkController', [
                    'getIndex'  => 'admin.languages.renew.links',
                    'getCreate' => 'admin.languages.renew.links.create',
                ]);

                Route::controller('product-files', 'Admin\LanguageFileController', [
                    'getList'   => 'admin.language.files',
                    'getDelete' => 'admin.language.files.delete',

                    'getEdit'    => 'admin.language.files.edit',
                    'postUpdate' => 'admin.language.files.update',

                    'getCreate' => 'admin.language.files.create',
                    'postStore' => 'admin.language.files.store',
                ]);

                Route::controller('jobs', 'Admin\JobController', [
                    'getIndex'  => 'admin.jobs',
                    'getDelete' => 'admin.jobs.delete',

                    'getEdit'    => 'admin.jobs.edit',
                    'postUpdate' => 'admin.jobs.update',

                    'getCreate' => 'admin.jobs.create',
                    'postStore' => 'admin.jobs.store',
                ]);

                Route::controller('admin', 'Admin\OpinionController', [
                    'getIndex'  => 'admin.opinions',
                    'getDelete' => 'admin.opinions.delete',

                    'getEdit'    => 'admin.opinions.edit',
                    'postUpdate' => 'admin.opinions.update',

                    'getCreate' => 'admin.opinions.create',
                    'postStore' => 'admin.opinions.store',
                ]);

                Route::controller('records', 'Admin\RecordController', [
                    'getIndex'  => 'admin.records',
                    'getDelete' => 'admin.records.delete',

                    'getEdit'    => 'admin.records.edit',
                    'postUpdate' => 'admin.records.update',

                    'getCreate' => 'admin.records.create',
                    'postStore' => 'admin.records.store',
                ]);

                Route::controller('lessons', 'Admin\LessonController', [
                    'getIndex'  => 'admin.lessons',
                    'getDelete' => 'admin.lessons.delete',

                    'getEdit'    => 'admin.lessons.edit',
                    'postUpdate' => 'admin.lessons.update',

                    'getCreate' => 'admin.lessons.create',
                    'postStore' => 'admin.lessons.store',
                ]);

                Route::controller('faqs', 'Admin\FaqController', [
                    'getIndex'  => 'admin.faqs',
                    'getDelete' => 'admin.faqs.delete',

                    'getEdit'    => 'admin.faqs.edit',
                    'postUpdate' => 'admin.faqs.update',

                    'getCreate' => 'admin.faqs.create',
                    'postStore' => 'admin.faqs.store',
                ]);

                Route::controller('stats', 'Admin\StatController', [
                    'getIndex'  => 'admin.stats',
                    'getDelete' => 'admin.stats.delete',

                    'getEdit'    => 'admin.stats.edit',
                    'postUpdate' => 'admin.stats.update',

                    'getCreate' => 'admin.stats.create',
                    'postStore' => 'admin.stats.store',
                ]);
                /*
                Route::controller('orders', 'Admin\OrderController', [
                    'getIndex'                 => 'admin.orders',
                    'getDelete'                => 'admin.orders.delete',
                    'getDetails'               => 'admin.orders.details',
                    'getResetIp'               => 'admin.orders.reset',

                    'getEdit'                  => 'admin.orders.edit',
                    'postUpdate'               => 'admin.orders.update',
                    'postEditable'             => 'admin.orders.editable',
                    'anyStatusList'            => 'admin.orders.status.list',
                    'getExport'                => 'admin.orders.export',
                    'getToCod'                 => 'admin.orders.to.cod',
                    'getAfterCod'              => 'admin.orders.after.cod',
                    'getShipmentSend'          => 'admin.orders.shipment.send',
                    'postShipmentSendMultiple' => 'admin.orders.shipment.send.multiple',
                    'getShipmentMark'          => 'admin.orders.shipment.mark',
                ]);
                */
                Route::controller('settings', 'Admin\SettingController', [
                    'getIndex'   => 'admin.settings',
                    'postUpdate' => 'admin.settings.update',
                ]);

                Route::controller('headers', 'Admin\HeaderController', [
                    'getIndex'  => 'admin.headers',
                    'getDelete' => 'admin.header.delete',

                    'getEdit'    => 'admin.header.edit',
                    'postUpdate' => 'admin.header.update',

                    'getCreate' => 'admin.header.create',
                    'postStore' => 'admin.header.store',

                    'getSwitch' => 'admin.header.switch',
                ]);

                Route::controller('inventory', 'Admin\InventoryController', [
                    'getIndex'   => 'admin.inventory',
                    'postUpdate' => 'admin.inventory.update'
                ]);

            });
        });

        Route::group(['middleware' => ['guest']], function () {
            Route::controller('/', 'Auth\AuthController', [
                'getLogin'  => 'admin.signin',
                'postLogin' => 'admin.signin.post',
            ]);
        });
    });
});