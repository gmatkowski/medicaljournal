<?php

namespace App\Http;

use App\Http\Middleware\CommonViews;
use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel {

    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \App\Http\Middleware\CacheKiller::class,
        CommonViews::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth'                  => \App\Http\Middleware\Authenticate::class,
        'auth.basic'            => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest'                 => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'csrf'                  => \App\Http\Middleware\VerifyCsrfToken::class,
        'localize'              => \Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRoutes::class,
        'localizationRedirect'  => \Mcamara\LaravelLocalization\Middleware\LaravelLocalizationRedirectFilter::class,
        'localeSessionRedirect' => \Mcamara\LaravelLocalization\Middleware\LocaleSessionRedirect::class,
        'promotion'             => \App\Http\Middleware\Promotion::class,
        'role'                  => \Zizaco\Entrust\Middleware\EntrustRole::class,
        'permission'            => \Zizaco\Entrust\Middleware\EntrustPermission::class,
        'ability'               => \Zizaco\Entrust\Middleware\EntrustAbility::class,
    ];
}
