<?php

namespace App\Http\Controllers;

use App\Exceptions\PageException;
use App\Forms\BuyForm;
use App\Jobs\CollectContactData;
use App\Jobs\CreateOrder;
use App\Order;
use Illuminate\Http\Request;
use App\Repositories\OrderRepository as ORD;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request as RQ;
//use App\Forms\BuyAddressForm;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Repositories\DistrictRepository as DR;
use App\Repositories\LanguageRepository as LR;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use DrewM\MailChimp\MailChimp;

/**
 * Class OrderController
 * @package App\Http\Controllers
 */
class OrderController extends Controller {

    use DispatchesJobs, FormBuilderTrait;

    private $orderRepo;
    private $request;
    private $districtRepo;
    private $languageRepo;

    public function __construct(ORD $ord, RQ $rq, DR $dr, LR $lr)
    {
        $this->orderRepo = $ord;
        $this->request = $rq;
        $this->districtRepo = $dr;
        $this->languageRepo = $lr;
    }

    public function postCheckout(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $form = $this->form(BuyForm::class, [
                'data' => [
                    'includeAddress' => $this->request->input('method') == 2,
                    'isPost'         => true
                ]
            ]);

//            $form = $form->compose(BuyAddressForm::class);

            if (!$form->isValid())
            {
                return redirect()
                    ->back()
                    ->withErrors($form->getErrors())
                    ->withInput($this->request->except(['province_id']));
            }

//            try
//            {
//                $this->districtRepo->find($this->request->input('district_id'));
//            } catch (\Exception $e)
//            {
//                throw new \Exception(trans('languages.no.district.selected'));
//            }

            $contact = $this->dispatch(new CollectContactData($this->request->all()));

            $this->request->merge([
                'payment' => 2
            ]);

            $language = $this->languageRepo->findWhere([['id', '=', $this->request->get('productId')]])->first();

            $languages = collect();
            $languages->push([
                'id'    => $language->id,
                'price' => $this->request->has('defaultPrice') ? $this->request->get('defaultPrice') : $language->languageTranslation->first()->price,
                'qty'   => 1
            ]);

            $order = $this->dispatch(new CreateOrder($this->request, $contact ? $contact->id : null, false, $languages));

            $order->to_confirm = 1;
            $order->save();

            DB::commit();

            Session::set('order_flash', $order);
            Session::set('product', $language->id);

            //  Start Mailchimp
            if(app()->environment() == 'production' && $request->has('email'))
            {
                try
                {
                    $MailChimp = new MailChimp('1f1b636463a2f5fe786653a4cd1e098e-us18');

                    $list_id = '789cc36bbc';
                    $r = $MailChimp->post("lists/$list_id/members", [
                        'email_address' => $request->get('email'),
                        'merge_fields' => [
                            'FNAME' => $contact->first_name ?: '',
                            'LNAME' => $contact->last_name ?: '',
                            'PHONE' => $contact->phone ?: '',
                            'ORDER' => '',
                        ],
                        'status' => 'subscribed',
                    ]);

                } catch (\Exception $e) {
                    if ($request->has('webapi')) {
                        return $e->getMessage();
                    } else {
                        return response()->json($e->getMessage());
                    }
                }
            }
            //  End Mailchimp
            
            switch ($language->id) {
                case 1:
                    $confirmUrl = route('page.confirm', ['product' => 'cambogia', 'id' => $order->id]);
                    break;
                case 2:
                    $confirmUrl = route('page.confirm', ['product' => 'blackmask', 'id' => $order->id]);
                    break;
                case 3:
                    $confirmUrl = route('page.confirm', ['product' => 'hallupro', 'id' => $order->id]); // old style Confirm
//                    $confirmUrl = route('page.checkout', ['product' => 'hallupro']); // test Checkout
//                    $this->dontSendToCcUNtilFinish($order);
                    break;
                case 4:
                    $confirmUrl = route('page.confirm', ['product' => 'waist', 'id' => $order->id]);
                    break;
                case 5:
                    $confirmUrl = route('page.confirm', ['product' => 'detoclean', 'id' => $order->id]);
                    break;
                case 6:
                    $confirmUrl = route('page.confirm', ['product' => 'coffee', 'id' => $order->id]);
                    break;
                case 7:
                    $confirmUrl = route('page.confirm', ['product' => 'joint', 'id' => $order->id]);
                    break;
                case 8:
                    $confirmUrl = route('page.confirm', ['product' => 'posture', 'id' => $order->id]);
                    break;
                case 9:
                    $confirmUrl = route('page.confirm', ['product' => 'bra', 'id' => $order->id]);
                    break;
                case 10:
                    $confirmUrl = route('page.confirm', ['product' => 'powerbank', 'id' => $order->id]);
                    break;
                case 11:
                    $confirmUrl = route('page.confirm', ['product' => 'ultraslim', 'id' => $order->id]);
                    break;

                case 12:
                    $confirmUrl = route('page.confirm', ['product' => 'bustiere', 'id' => $order->id]);
                    break;
                case 13:
                    $confirmUrl = route('page.confirm', ['product' => 'vervalen', 'id' => $order->id]);
                    break;
                case 14:
                    $confirmUrl = route('page.confirm', ['product' => 'claireinstantwhitening', 'id' => $order->id]);
                    break;
                case 15:
                    $confirmUrl = route('page.confirm', ['product' => 'manuskin', 'id' => $order->id]);
                    break;

                case 20:
                    $confirmUrl = route('page.confirm', ['product' => 'flexaplus', 'id' => $order->id]);
                    break;
                case 21:
                    $confirmUrl = route('page.confirm', ['product' => 'detoxic', 'id' => $order->id]);
                    break;
                case 22:
                    $confirmUrl = route('page.confirm', ['product' => 'dietbooster', 'id' => $order->id]);
                    break;

                case 23:
                    $confirmUrl = route('page.confirm', ['product' => 'imove', 'id' => $order->id]);
                    break;

                case 24:
                    $confirmUrl = route('page.confirm', ['product' => 'dominator', 'id' => $order->id]);
                    break;

                case 25:
                    $confirmUrl = route('page.confirm', ['product' => 'nutrilashpromo', 'id' => $order->id]);
                    break;
                case 26:
                    $confirmUrl = route('page.confirm', ['product' => 'leferyacrpromo', 'id' => $order->id]);
                    break;
                case 27:
                    $confirmUrl = route('page.confirm', ['product' => 'forsopromo', 'id' => $order->id]);
                    break;
                case 28:
                    $confirmUrl = route('page.confirm', ['product' => 'ultrahairpromo', 'id' => $order->id]);
                    break;

                case 29:
                    $confirmUrl = route('page.confirm', ['product' => 'musclegain', 'id' => $order->id]);
                    break;
            }

//            switch ($language->id) {
//                case 1:
//                    $confirmUrl = route('page.confirm', ['product' => 'cambogia', 'id' => $order->id, 'contact' => $contact->id]);
//                    break;
//                case 2:
//                    $confirmUrl = route('page.confirm', ['product' => 'blackmask', 'id' => $order->id, 'contact' => $contact->id]);
//                    break;
//                case 3:
//                    $confirmUrl = route('page.confirm', ['product' => 'hallupro', 'id' => $order->id, 'contact' => $contact->id]); // old style Confirm
////                    $confirmUrl = route('page.checkout', ['product' => 'hallupro']); // test Checkout
////                    $this->dontSendToCcUNtilFinish($order);
//                    break;
//                case 4:
//                    $confirmUrl = route('page.confirm', ['product' => 'waist', 'id' => $order->id, 'contact' => $contact->id]);
//                    break;
//                case 5:
//                    $confirmUrl = route('page.confirm', ['product' => 'detoclean', 'id' => $order->id, 'contact' => $contact->id]);
//                    break;
//                case 6:
//                    $confirmUrl = route('page.confirm', ['product' => 'coffee', 'id' => $order->id, 'contact' => $contact->id]);
//                    break;
//                case 7:
//                    $confirmUrl = route('page.confirm', ['product' => 'joint', 'id' => $order->id, 'contact' => $contact->id]);
//                    break;
//                case 8:
//                    $confirmUrl = route('page.confirm', ['product' => 'posture', 'id' => $order->id, 'contact' => $contact->id]);
//                    break;
//            }

            return redirect($confirmUrl);

        } catch (\Exception $e)
        {
            DB::rollBack();

            return redirect()
                ->back()
                ->withErrors($form->getErrors())
                ->withInput($this->request->except(['province_id']))
                ->with([
                    'message' => [
                        'type'    => 'error',
                        'message' => $e->getMessage()
                    ],
                ]);
        }
    }

    private function dontSendToCcUNtilFinish(Order $order)
    {
        $order->contact->call_center = 1;
        $order->contact->save();
    }

    public function postNewsletter(Request $request)
    {
        $request->merge([
            'first_name' => explode('@', $request->get('email'))[0],
            'phone' => null
        ]);

        $this->dispatch(new CollectContactData($request->all()));

        return redirect(route('page.confirm', ['product' => 'garcinia']));
    }

    public function postGetOptions()
    {
        try
        {
            switch ($this->request->input('method'))
            {
                case 'postalcode':
                    $postalcodes = app('App\Repositories\PostCodeRepository');
                    $postalcode = $postalcodes->find($this->request->input('id'));
                    $items = collect($postalcode->subDistricts->toArray());
                    $items->push([
                        'id'   => 0,
                        'name' => trans('languages.buy.subdistrict')
                    ]);
                    break;
                case 'subdistrict':
                    $subdistricts = app('App\Repositories\SubDistrictRepository');
                    $subdistrict = $subdistricts->find($this->request->input('id'));
                    $items = collect();
                    $items->push([
                        'id'   => $subdistrict->district->id,
                        'name' => $subdistrict->district->name
                    ]);
                    $items->push([
                        'id'   => $subdistrict->district->city->id,
                        'name' => $subdistrict->district->city->name
                    ]);
                    $items->push([
                        'id'   => $subdistrict->district->city->province->id,
                        'name' => $subdistrict->district->city->province->name
                    ]);
                    break;
                default:
                    throw new PageException('Something goes wrong !');
            }

            return response()->json([
                'status' => 'ok',
                'items'  => $items
            ]);

        } catch (\Exception $e)
        {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }
}
