<?php

namespace App\Http\Controllers\Asp;

use App\Forms\ContactShortForm;
use App\Language;
use App\Repositories\HeaderRepository;
use App\Repositories\JobRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\OpinionRepository;
use App\Repositories\PageRepository;
use App\Repositories\StatRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Repositories\ContactRepository;
use App\Jobs\SendContactReminder;
use App\Http\Controllers\ViewController;

/**
 * Class AspController
 * @package App\Http\Controllers
 */
class AspController extends ViewController
{

    use FormBuilderTrait;

    /**
     * @var PageRepository
     */
    protected $pages;

    /**
     * AspController constructor.
     * @param PageRepository $pageRepository
     * @param HeaderRepository $header
     * @param LanguageRepository $languages
     * @param ContactRepository $contacts
     */
    public function __construct(PageRepository $pageRepository, HeaderRepository $header, LanguageRepository $languages, ContactRepository $contacts)
    {
        parent::__construct();
        $this->pages = $pageRepository;
        $this->header = $header;
        $this->languages = $languages;
        $this->contacts = $contacts;
    }

    /**
     * @param JobRepository $jobRepository
     * @param OpinionRepository $opinionRepository
     * @param Request $request
     * @param StatRepository $statRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getWelcome(JobRepository $jobRepository, OpinionRepository $opinionRepository, Request $request, StatRepository $statRepository)
    {
        if ($request->has('result')) {
            return redirect()
                ->route('page.index')
                ->with([
                    'ga_event' => [
                        'category' => 'Checkout',
                        'action'   => 'Payment',
                        'label'    => $request->get('result'),
                    ],
                    'message'  => [
                        'type'    => $request->get('result') == 'ok' ? 'success' : 'error',
                        'message' => trans('order.status.message.' . $request->get('result'))
                    ],
                ]);
        }

        $form = $this->form(ContactShortForm::class, [
            'method' => 'POST',
            'url'    => route('page.contact.short.send')
        ]);

        $header = $this->header->getActive();
        if ($header->count() > 0) {
            $header = $header->random();
        } else {
            $header = $header->first();
        }

        if($request->hasCookie('10days')){

        }

        

        $data = [
            'form'         => $form,
            'pages'        => $this->pages->getWelcomeSet(),
            'jobs'         => $jobRepository->stats(),
            'mainOpinions' => $opinionRepository->opinions(1),
            'stats'        => $statRepository->stats(),
            'header'       => $header,
        ];

        $request->session()->put('asp', 1);

        return view('asp.1.welcome', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getClip()
    {
        return view('asp.1.clip');
    }

    /**
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function getTrial(Request $request, ContactRepository $contactRepository)
    {
        if (!$request->session()->has('contact-id')) {
            return redirect()->route('asp.welcome')->with([
                'popup' => 'try'
            ]);
        }

        $contact = $contactRepository->find($request->session()->get('contact-id'));
        if (!$contact) {
            $request->session()->forget(['contact-id', 'try']);

            return redirect()->route('asp.welcome')->with([
                'popup' => 'try'
            ]);
        }

        try {
            if ($contact->reminded == 0) {
                $this->dispatch(
                    new SendContactReminder($contact)
                );
            }
        } catch (\Exception $e) {

        }

        $english = $this->languages->findByField('symbol', 'en')->first();

        $contact = $this->contacts->find($request->session()->get('contact-id'));

        $data = [
            'english'  => $english,
            'page'     => $this->pages->getById(30),
            'page2'    => $this->pages->getById(31),
            'page3'    => $this->pages->getById(32),
            'discount' => $english,
        ];

        $request->session()->put('user_checkout_data', [
            'first_name' => $contact->name,
            'last_name'  => '',
            'email'      => $contact->email,
            'phone'      => $contact->phone,
            'terms'      => null,
            'languages'  => [0 => $english->id],
        ]);


        return view('asp.1.trial', $data);
    }

}
