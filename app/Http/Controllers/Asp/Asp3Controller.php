<?php

namespace App\Http\Controllers\Asp;

use App\Http\Controllers\ViewController;
use Illuminate\Support\Facades\Input;

class Asp3Controller extends ViewController
{


    public function getIndex (){
        if (Input::has('utm_source') && Input::has('utm_source') && Input::has('utm_source')){
            return redirect(route('page.index',
                'utm_source='.Input::get('utm_source').
                '&utm_medium='.Input::get('utm_medium').
                '&utm_campaign='.Input::get('utm_campaign')
            ))
                ->withCookie(cookie()->make('10days', 'true'));
        } else {
            return redirect(route('page.index'))->withCookie(cookie()->make('10days', 'true'));
        }
        

    }

}
