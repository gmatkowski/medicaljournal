<?php

namespace App\Http\Controllers\Asp;

use App\Forms\ContactShortForm;
use App\Language;
use App\Repositories\HeaderRepository;
use App\Repositories\JobRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\OpinionRepository;
use App\Repositories\PageRepository;
use App\Repositories\StatRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Repositories\ContactRepository;
use App\Jobs\SendContactReminder;
use App\Http\Controllers\ViewController;


class Asp7Controller extends ViewController
{

    use FormBuilderTrait;


    protected $pages;


    public function __construct(PageRepository $pageRepository, HeaderRepository $header, LanguageRepository $languages, ContactRepository $contacts)
    {
        parent::__construct();
        $this->pages = $pageRepository;
        $this->header = $header;
        $this->languages = $languages;
        $this->contacts = $contacts;
    }


    public function getIndex(JobRepository $jobRepository, OpinionRepository $opinionRepository, Request $request, StatRepository $statRepository)
    {

        if ($request->get('utm_campaign') == 'kompas' || $request->get('utm_campaign') == 'tribun')
        {
            return redirect(route('asp6.global'));
        }

        
        if ($request->has('result')) {
            return redirect()
                ->route('page.index')
                ->with([
                    'ga_event' => [
                        'category' => 'Checkout',
                        'action'   => 'Payment',
                        'label'    => $request->get('result'),
                    ],
                    'message'  => [
                        'type'    => $request->get('result') == 'ok' ? 'success' : 'error',
                        'message' => trans('order.status.message.' . $request->get('result'))
                    ],
                ]);
        }

        $form = $this->form(ContactShortForm::class, [
            'method' => 'POST',
            'url'    => route('page.contact.short.send')
        ]);

        $header = $this->header->getActive();
        if ($header->count() > 0) {
            $header = $header->random();
        } else {
            $header = $header->first();
        }

        if ($request->hasCookie('10days')) {

        }


        $data = [
            'form'         => $form,
            'pages'        => $this->pages->getWelcomeSet(),
            'jobs'         => $jobRepository->stats(),
            'mainOpinions' => $opinionRepository->opinions(1),
            'stats'        => $statRepository->stats(),
            'header'       => $header,
        ];

        $request->session()->put('asp', 7);

        return view('asp.7.welcome', $data);
    }
}