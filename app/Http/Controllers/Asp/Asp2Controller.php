<?php

namespace App\Http\Controllers\Asp;

use App\Forms\ContactShortForm;
use App\Language;
use App\Repositories\HeaderRepository;
use App\Repositories\JobRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\OpinionRepository;
use App\Repositories\PageRepository;
use App\Repositories\StatRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Repositories\ContactRepository;
use App\Jobs\SendContactReminder;
use App\Http\Controllers\ViewController;

class Asp2Controller extends ViewController
{
    public function __construct(PageRepository $pageRepository, HeaderRepository $header, LanguageRepository $languages, ContactRepository $contacts)
    {
        parent::__construct();
        $this->pages = $pageRepository;
        $this->header = $header;
        $this->languages = $languages;
        $this->contacts = $contacts;
    }

    public function getStep1 (){
        return view('asp.2.step1');
    }

    public function getStep2(){
        $english = $this->languages->findByField('symbol', 'en')->first();

        $data = [
            'english'  => $english,
        ];

        return view('asp.2.step2', $data);
    }
}
