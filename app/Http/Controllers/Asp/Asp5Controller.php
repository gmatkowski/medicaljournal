<?php

namespace App\Http\Controllers\Asp;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Forms\ContactShortForm;
use App\Language;
use App\Repositories\HeaderRepository;
use App\Repositories\JobRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\OpinionRepository;
use App\Repositories\PageRepository;
use App\Repositories\StatRepository;

use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Repositories\ContactRepository;
use App\Jobs\SendContactReminder;
use App\Http\Controllers\ViewController;

class Asp5Controller extends ViewController
{
    use FormBuilderTrait;

    public function __construct(PageRepository $pageRepository, HeaderRepository $header, LanguageRepository $languages, ContactRepository $contacts)
    {
        parent::__construct();
        $this->pages = $pageRepository;
        $this->header = $header;
        $this->languages = $languages;
        $this->contacts = $contacts;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex(JobRepository $jobRepository, OpinionRepository $opinionRepository, Request $request, StatRepository $statRepository)
    {
        if ($request->has('result')) {
            return redirect()
                ->route('page.index')
                ->with([
                    'ga_event' => [
                        'category' => 'Checkout',
                        'action'   => 'Payment',
                        'label'    => $request->get('result'),
                    ],
                    'message'  => [
                        'type'    => $request->get('result') == 'ok' ? 'success' : 'error',
                        'message' => trans('order.status.message.' . $request->get('result'))
                    ],
                ]);
        }

        $form = $this->form(ContactShortForm::class, [
            'method' => 'POST',
            'url'    => route('page.contact.short.send')
        ]);

        $header = $this->header->getActive();
        if ($header->count() > 0) {
            $header = $header->random();
        } else {
            $header = $header->first();
        }

        if($request->hasCookie('10days')){

        }



        $data = [
            'form'         => $form,
            'pages'        => $this->pages->getWelcomeSet(),
            'jobs'         => $jobRepository->stats(),
            'mainOpinions' => $opinionRepository->opinions(1),
            'stats'        => $statRepository->stats(),
            'header'       => $header,
        ];

        $request->session()->put('asp', 1);

        return view('asp.5.welcome', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
