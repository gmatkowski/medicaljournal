<?php

namespace App\Http\Controllers\Asp;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class Asp8Controller extends Controller
{
    public function getIndex()
    {
        return view('asp.8.global');
    }
}
