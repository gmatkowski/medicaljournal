<?php

namespace App\Http\Controllers\Asp;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;
use Jenssegers\Agent\Facades\Agent;
use Illuminate\Support\Facades\Input;

class Asp4Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        /*if (!Cookie::has('redir'))
        {*/
            /*if (Agent::isMobile()){*/
                $routes = ['http://sains-jurnal.com/global', 'http://sains-jurnal.com/'];
                $route = $routes[rand(0,1)];
                //$route = 'http://sains-jurnal.com/';

                if ($route == 'asp6.global'){
                    if (Input::has('utm_source') && Input::has('utm_medium') && Input::has('utm_campaign')){
                        return redirect(route($route,
                            'utm_source='.Input::get('utm_source').
                            '&utm_medium='.Input::get('utm_medium').
                            '&utm_campaign='.Input::get('utm_campaign')
                        ))/*->withCookie(cookie()->make('redir', 'true'))*/;
                    } else {
                        return redirect(route($route, 'utm_source=display&utm_medium=cpc&utm_campaign=hello'))
                            /*->withCookie(cookie()->make('redir', 'true'))*/;
                    }
                } else {
                    if (Input::has('utm_source') && Input::has('utm_medium') && Input::has('utm_campaign')){
                        return redirect($route.
                            '?utm_source='.Input::get('utm_source').
                            '&utm_medium='.Input::get('utm_medium').
                            '&utm_campaign='.Input::get('utm_campaign')
                        )/*->withCookie(cookie()->make('redir', 'true'))*/;
                    } else {
                        return redirect($route.'?utm_source=display&utm_medium=cpc&utm_campaign=hello');
                    }
                }
            /*}*/
        /*}*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
