<?php

namespace App\Http\Controllers;

use App;
use App\Contact;
use App\ExternalClient;
use anlutro\cURL\cURL;
use App\Helpers\StrHelper;
use Illuminate\Http\Request;
use App\Jobs\CollectContactData;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use App\Repositories\ContactRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\ExternalClientRepository;
use DrewM\MailChimp\MailChimp;

/**
 * Class ExternalClientController
 * @package App\Http\Controllers
 */
class ExternalClientController extends Controller {

    private $languageRepo;
    private $extRepo;
    private $contacts;
    private $curl;
    private $testUri;

    public function __construct(
        LanguageRepository $languageRepository,
        ExternalClientRepository $externalClientRepository,
        ContactRepository $contactRepository
    )
    {
        $this->languageRepo = $languageRepository;
        $this->extRepo = $externalClientRepository;
        $this->contacts = $contactRepository;
        $this->curl = new cURL();
        $this->testUri = app()->environment() == 'production' ? 'https://promos-seru.com' : 'http://medical.app';
    }

    public function testConnection()
    {
        $client = $this->curl->post($this->testUri.'/api/token/generate', [
            'name' => 'ziarek'
        ]);

        $response = $this->curl->post($this->testUri.'/api/make/contact', [
            'first_name' => 'testziarka',
            'last_name' => 'testziarka',
            'phone' => '1234567890',
            'click_id' => '11111111111',
            'pub_id' => '997',
            'base_url' => 'http://test.com',
            'ext_price' => '490000idr',
            'product' => '7e4d6fd9-1bb9-4e8d-b6da-e782d7a7fbe5',
            'token' => explode('"',$client->body)[7]
        ]);

//        $response = $this->curl->post($this->testUri.'/api/make/contacts', [
//            'contacts' => [
//                [
//                    'first_name' => '',
//                    'last_name' => '',
//                    'phone' => '1234567890',
//                    'click_id' => '123456',
//                    'pub_id' => 997,
//                    'base_url' => 'optional string',
//                    'ext_price' => 'optional string',
//                    'product' => 'detoclean'
//                ],
//                [
//                    'first_name' => '',
//                    'last_name' => '',
//                    'phone' => '1234567890',
//                    'click_id' => '654321',
//                    'pub_id' => 997,
//                    'base_url' => 'optional string',
//                    'ext_price' => 'optional string',
//                    'product' => 'detoclean'
//                ]
//            ],
//            'token' => explode('"',$client->body)[7]
//        ]);

//        $response = $this->curl->post('http://promos-seru.com/api/post/status/multiple',
//            [
//                'contacts_ids' => '508589-1021018,510655-1023121,510778-1023368,513655-1028340,515069-1030674,515445-1031320,516312-1033126,520748-1041141,532598-1059495,534662-1063593,535644-1064630,536285-1066091,538003-1069443,538050-1069481,539408-1072722',
//                'token' => '587db64847ff3'
//            ]
//        );

        return $response;
    }

    public function generateToken(Request $request)
    {
        $partnerName = $request->get('name');
        $externalClient = $this->extRepo->findWhere([['name', '=', $partnerName]])->first();
        if(!$externalClient){
            $externalClient = $this->extRepo->create([
                'name' => $partnerName,
                'token' => uniqid()
            ]);
        }
        return response()->json([
            'status' => 'ok',
            'token' => $externalClient->token
        ]);
    }

    public function makeContact(Request $request)
    {
        if($this->checkToken($request)===true){
            $foundClient = $this->findClient($request);
            if($foundClient instanceof ExternalClient)
            {
                $request->merge([
                    'ext_client_id' => $foundClient->id
                ]);
                    if($this->checkRequest($request)===true)
                    {
                        $contact = $this->make($request);
                        
                        if($contact instanceof Contact)
                        {
                            //  Start Mailchimp
                            if(app()->environment() == 'production' && $request->has('email'))
                            {
                                try
                                {
                                    $MailChimp = new MailChimp('1f1b636463a2f5fe786653a4cd1e098e-us18');

                                    $list_id = '789cc36bbc';
                                    $r = $MailChimp->post("lists/$list_id/members", [
                                        'email_address' => $request->get('email'),
                                        'merge_fields' => [
                                            'FNAME' => $contact->first_name ?: '',
                                            'LNAME' => $contact->last_name ?: '',
                                            'PHONE' => $contact->phone ?: '',
                                            'ORDER' => '',
                                        ],
                                        'status' => 'subscribed',
                                    ]);

                                } catch (\Exception $e) {
                                    if ($request->has('webapi')) {
                                        return $e->getMessage();
                                    } else {
                                        return response()->json($e->getMessage());
                                    }
                                }
                            }
                            //  End Mailchimp

                            return response()->json([
                                'status' => 'contact: '.$contact->id.' created',
                                'id' => $contact->id,
                                'click_id' => $contact->click_id
                            ]);
                        }
                        else
                        {
                            if($request->has('webapi'))
                            {
                                return $contact;
                            }
                            else
                            {
                                return response()->json($contact);
                            }
                        }
                    }
                    else
                    {
                        return response()->json($this->checkRequest($request));
                    }
            } else {
                return $this->findClient($request);
            }
        } else {
            return $this->checkToken($request);
        }
    }

    public function makeContacts(Request $request)
    {
        if($this->checkToken($request)===true){
            $foundClient = $this->findClient($request);
            if($foundClient instanceof ExternalClient)
            {
                $request->merge([
                    'ext_client_id' => $foundClient->id
                ]);
                $response = collect();
                    foreach($request->get('contacts') as $contact)
                    {
                        if($this->checkRequest($contact)===true)
                        {
                            $request->merge($contact);
                            $contactCreated = $this->make($request);
                            
                            if($contactCreated instanceof Contact)
                            {
                                //  Start Mailchimp
                                if(app()->environment() == 'production' && $request->has('email'))
                                {
                                    try
                                    {
                                        $MailChimp = new MailChimp('1f1b636463a2f5fe786653a4cd1e098e-us18');

                                        $list_id = '2276f65826';
                                        $r = $MailChimp->post("lists/$list_id/members", [
                                            'email_address' => $request->get('email'),
                                            'merge_fields' => [
                                                'FNAME' => $contactCreated->first_name ?: '',
                                                'LNAME' => $contactCreated->last_name ?: '',
                                                'PHONE' => $contactCreated->phone ?: '',
                                                'ORDER' => '',
                                            ],
                                            'status' => 'subscribed',
                                        ]);

                                    } catch (\Exception $e) {
                                        if($request->has('webapi'))
                                        {
                                            return $e->getMessage();
                                        }
                                        else
                                        {
                                            return response()->json($e->getMessage());
                                        }
                                    }
                                }
                                //  End Mailchimp

                                $response->push([
                                    'status' => 'contact: '.$contactCreated->id.' created',
                                    'id' => $contactCreated->id,
                                    'click_id' => $contactCreated->click_id
                                ]);
                            }
                            else
                            {
                                $response->push(
                                    array_merge(
                                        $contactCreated,
                                        [
                                            'click_id' => $contact['click_id']
                                        ]
                                    )
                                );
                            }
                        }
                        else
                        {
                            $response->push([
                                array_merge(
                                    $this->checkRequest($request),
                                    [
                                        'click_id' => $contact['click_id']
                                    ]
                                )
                            ]);
                        }
                    }
                return response()->json($response);
            } else {
                return $this->findClient($request);
            }
        } else {
            return $this->checkToken($request);
        }
    }

    public function postStatusMultiple(Request $request)
    {
        if($this->checkToken($request)===true){
            $foundClient = $this->findClient($request);
            if($foundClient instanceof ExternalClient)
            {
                $response = collect();

                $contacts_ids = explode(',', $request->get('contacts_ids'));
                foreach($contacts_ids as $contactFromAPI)
                {
                    $contacts_idsArr[] = explode('-', $contactFromAPI)[0];
                    $clicks_idsArr[] = explode('-', $contactFromAPI)[1];
                }

                $contacts = $foundClient->contacts()->getQuery()
                    ->whereIn('contacts.id', $contacts_idsArr)
                    ->whereIn('contacts.click_id', $clicks_idsArr)
                    ->get(['id', 'click_id', 'classifier_name', 'cc_sell', 'cc_reject', 'cc_hold']);

                foreach($contacts_idsArr as $contact_id)
                {
                    $contactFound = $contacts->first(function ($key, $value) use ($contact_id)
                    {
                        return
                            $value->id == $contact_id;
                    });
                    if($contactFound)
                    {
                        $response->push([
                            'contact_id' => $contactFound->id,
                            'click_id' => $contactFound->click_id,
                            'status' => $contactFound->getStatus(),
                            'call_comment' => $contactFound->getCallComment()
                        ]);
                    }
                    else
                    {
                        $response->push([
                            'contact_id' => $contact_id,
                            'status' => 'contact not found'
                        ]);
                    }
                }

                return response()->json($response);
            }
            else
            {
                return $this->findClient($request);
            }
        }
        else
        {
            return $this->checkToken($request);
        }
    }

    protected function checkToken(Request $request)
    {
        if($request->has('token')){
            return true;
        }
        return response()->json([
            'status' => 'token missing'
        ]);
    }

    protected function findClient(Request $request)
    {
        $foundClient = $this->extRepo
            ->findWhere([['token', '=', $request->get('token')]], ['id'])
            ->first();
        if($foundClient)
        {
            return $foundClient;
        }
        else
        {
            return response()->json([
                'status' => 'client not found, token mismatch'
            ]);
        }
    }

    protected function checkRequest($request)
    {
        if($request instanceof Request)
        {
            if(
                   !$request->has('first_name')
                || !$request->has('last_name')
                || !$request->has('phone')
                || !$request->has('click_id')
                || !$request->has('product')
            ){
                return [
                    'status' => 'required fields missing'
                ];
            }
        }
        if(is_array($request) &&
            (
               !array_key_exists('first_name', $request)
            || !array_key_exists('last_name', $request)
            || !array_key_exists('phone', $request)
            || !array_key_exists('click_id', $request)
            || !array_key_exists('product', $request)
            )
        ){
            return [
                'status' => 'required fields missing'
            ];
        }
        return true;
    }

    protected function make(Request $request)
    {
        switch ($request->get('product')) {
            case 'cambogia':
            case '965d28dd-dc2a-4a6a-97f4-8602b8bc9f77':
                $productId = 1;
                break;
            case 'blackmask':
            case 'dfd06ea2-f459-4754-8c06-34720b483322':
                $productId = 2;
                break;
            case 'hallupro':
            case '387b6aa2-1120-47d9-b3a0-591df640ee1b':
                $productId = 3;
                break;
            case 'waist':
            case 'fd606c5e-acd0-4054-889e-27a3bfb893c1':
                $productId = 4;
                break;
            case 'detoclean':
            case '7e4d6fd9-1bb9-4e8d-b6da-e782d7a7fbe5':
                $productId = 5;
                break;
            case 'coffee':
            case '2d11025f-778c-4419-98b1-e5a51ac71f29':
                $productId = 6;
                break;
            case 'joint':
            case '6590e74d-4575-4ec3-b5b7-f3a9cc21207e':
                $productId = 7;
                break;
            case 'posture':
            case 'bbc8e290-8293-427c-88c6-97a460546199':
                $productId = 8;
                break;
            case 'bra':
            case '77a63631-59db-4602-9142-c1db9e373526':
                $productId = 9;
                break;
            case 'powerbank':
            case 'e4f8aa4a-2090-4ff3-862f-0e83e6a58025':
                $productId = 10;
                break;
            case 'ultraslim':
            case '3f74be50-f76e-4548-b27e-7b49cca3f870':
                $productId = 11;
                break;

            case 'bustiere':
            case '90147575-1308-4f14-b601-c8030d29d269':
                $productId = 12;
                break;
            case 'vervalen':
            case 'a543801a-3a18-4486-95bc-1ca704ce1be3':
                $productId = 13;
                break;
            case 'claireinstantwhitening':
            case '2c138441-4b8c-4993-ae79-0261e639c101':
                $productId = 14;
                break;
            case 'manuskin':
            case '55e7d87b-2697-47a1-8d19-3aa601044b99':
                $productId = 15;
                break;

            case 'flexaplus':
            case '49473744-051c-44fa-a356-25481e520c53':
                $productId = 20;
                break;
            case 'detoxic':
            case 'a47aef28-7850-453c-baa3-079f37001e34':
                $productId = 21;
                break;
            case 'dietbooster':
            case '91c61152-df54-4880-950e-421305a0321b':
                $productId = 22;
                break;

            case 'imove':
            case '60d72f29-41f2-4aaa-bcc0-ce6129298d7d':
                $productId = 23;
                break;

//            case 'dominator':
//                $productId = 24;
//                break;
//
            case 'nutrilashpromo':
            case '0cc178b8-d4f5-498e-91f8-2eb43400a0db':
                $productId = 25;
                break;
            case 'leferyacrpromo':
            case 'a9074e61-2dfa-4d75-b770-a3fd7a7cbad6':
                $productId = 26;
                break;
            case 'forsopromo':
            case '6facfdb3-4402-4565-b7cd-3e346c7cd611':
                $productId = 27;
                break;
            case 'ultrahairpromo':
            case 'f0cb269b-4305-4370-8b57-935cb078126c':
                $productId = 28;
                break;
            case 'musclegain':
            case '1c42efd6-793a-4d2d-b12c-305317818d01':
                $productId = 29;
                break;
            default:
                return [
                    'status' => 'invalid product name'
                ];
        }

        $request->merge([
            'email' => null,
            'productId' => $productId
        ]);

        if($request->get('phone')!='1234567890') {
            $valid = StrHelper::validatePhone($request->get('phone'));
            if (!$valid) {
                return [
                    'status' => 'invalid phone format'
                ];
            }

            $double = $this->contacts->searchByPhone($request->get('phone'));
            $doubleValid = $this->contacts->searchByPhone($valid);

            $request->replace(array_merge($request->all(), ['phone' => $valid]));
        }
        else
        {
            $double = false;
            $doubleValid = false;
        }

        if($double || $doubleValid){
            return [
                'status' => 'double phone'
            ];
        } else {
            $contact = $this->createContact($request);
        }
        if($request->has('webapi'))
        {
            return view('page.contact_webapi_confirm', ['contact' => $contact]);
        }
        return $contact;
    }

    private function createContact(Request $request)
    {
        $contact = $this->dispatch(new CollectContactData($request->all()));
        return $contact;
    }

    public function getExternalCli()
    {
        $clients = collect();
        $externalClients = $this->extRepo->all();
        foreach ($externalClients as $externalClient) {
            $externalClient->setVisible([
                'id', 'name', 'token'
            ]);

            $clients->push($externalClient);
        }
        return response()->json([
            $clients
        ]);
    }

    public function checkDupliactesFromCsv()
    {
        $phones = collect();
        $file_path = storage_path('app/partners.csv');
        Excel::load($file_path, function ($reader) use($phones) {
            $results = $reader->all();
            $results->each(function ($sheet) use($phones) {
                $object = $sheet->all();

                $phone = $object['phone'];
                $phones->push($phone);
            });
        });
        $unique = $phones->unique();
        $uniques = $unique->values()->all();
        return $uniques;
    }

    public function postBacks()
    {
        return view('admin.contact.externalPostBacks');
    }

    public function sendPostBacks(Request $request)
    {
        $clickIds = $request->get('phones');
        if($clickIds)
        {
            $clickIdsArr = explode("\r\n", $clickIds);

        foreach($clickIdsArr as $click_id){
            $contact = $this->contacts->findByField('click_id', $click_id)->first();
            if($contact)
            {
                file_get_contents('https://funcpa.ru/api/asia/?id='.$click_id.'&status=sale&phone='.$contact->phone.'&sign='.md5($click_id.'sale'.$contact->phone.'asia'));
                Log::info('FunCPA self Post Back sent', ['contact_id' => $contact->id]);
            }
        }

            return back()->with('message', [
                'type'    => 'success',
                'message' => 'Action done'
            ]);
        } else {
            return back()->with('message', [
                'type'    => 'error',
                'message' => 'Action fail'
            ]);
        }
    }
}
