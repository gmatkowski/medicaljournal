<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use App\Forms\SignInForm;

class AuthController extends Controller {

    use AuthenticatesAndRegistersUsers, ThrottlesLogins, FormBuilderTrait;


    public function __construct()
    {
        $this->loginPath = route('admin.signin');
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    protected function authenticated(Request $request, User $user)
    {
        return redirect()->route('admin.home');
    }

    public function getLogin()
    {
        $form = $this->form(SignInForm::class, [
            'method' => 'POST',
            'url'    => route('admin.signin.post')
        ]);

        return view('auth.login', [
            'form' => $form
        ]);
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'     => 'required|max:255',
            'email'    => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

}
