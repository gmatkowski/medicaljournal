<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

/**
 * Class MailgunEventController
 * @package App\Http\Controllers
 */
class MailgunEventController extends Controller {

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postEvent(Request $request)
    {

        return response()->json([
            'status' => 'ok'
        ]);
    }
}
