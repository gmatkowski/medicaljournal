<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\RecordForm;
use App\Jobs\Admin\CreateLanguage;
use App\Jobs\Admin\CreateRecord;
use App\Jobs\Admin\SaveLanguage;
use App\Jobs\Admin\SaveRecord;
use App\Jobs\SavePackage;
use App\Repositories\LanguagePackageRepository;
use App\Repositories\RecordRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Config;
use Auth;

/**
 * Class RecordController
 * @package App\Http\Controllers\Admin
 */
class RecordController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var RecordRepository
     */
    protected $languages;


    /**
     * @param RecordRepository $recordRepository
     */
    public function __construct(RecordRepository $recordRepository)
    {
        $this->records = $recordRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $data = [
            'objects' => $this->records->newest(50),
        ];

        return view('admin.record.list', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        $object = $this->records->find($id);

        $form = $this->form(RecordForm::class, [
            'method' => 'POST',
            'url'    => route('admin.records.update'),
            'model'  => $object
        ]);

        $data = [
            'object' => $object,
            'form'   => $form,
            'mode'   => trans('admin.common.edit')
        ];

        return view('admin.record.form', $data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request)
    {
        try
        {
            $form = $this->form(RecordForm::class);

            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = $this->records->find($request->get('id'));
            $this->dispatch(
                new SaveRecord($object, $request)
            );

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $form = $this->form(RecordForm::class, [
            'method' => 'POST',
            'url'    => route('admin.records.store'),
        ]);

        $data = [
            'form' => $form,
            'mode' => trans('admin.common.add')
        ];

        return view('admin.record.form', $data);
    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request)
    {
        try
        {
            $form = $this->form(RecordForm::class);
            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $this->dispatch(
                new CreateRecord($request)
            );

            return redirect()->route('admin.records')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        $object = $this->records->find($id);
        $object->delete();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }
}
