<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\HeaderForm;
use App\Jobs\Admin\CreateHeader;
use App\Jobs\Admin\SaveHeader;
use App\Repositories\HeaderRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Header;
use Config;
use Auth;


/**
 * Class HeaderController
 * @package App\Http\Controllers\Admin
 */
class HeaderController extends Controller
{
    use FormBuilderTrait;

    /**
     * @var HeaderRepository
     */
    protected $headers;


    /**
     * HeaderController constructor.
     * @param HeaderRepository $headerRepository
     */
    public function __construct(HeaderRepository $headerRepository)
    {
        $this->header = $headerRepository;
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $data = [
            'objects' => $this->header->all()
        ];

        return view('admin.header.list', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate()
    {
        $form = $this->form(HeaderForm::class, [
            'method' => 'POST',
            'url'    => route('admin.header.store'),
        ]);

        $data = [
            'form' => $form,
            'mode' => 'Dodanie'
        ];

        return view('admin.header.form', $data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request)
    {
        try {
            $form = $this->form(HeaderForm::class);

            if (!$form->isValid()) {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = new Header();

            foreach ($request->input('translation') as $lang => $value) {
                if (!empty($value['text'])) {
                    $object->fill([
                        $lang => [
                            'text'  => $value['text'],
                            'text2' => $value['text2'],
                            'text3' => $value['text3'],
                        ]
                    ]);
                }
            }

            $object->save();

            return redirect()->route('admin.headers')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e) {
            throw new AdminException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getEdit($id)
    {

        $object = $this->header->find($id);

        $form = $this->form(HeaderForm::class, [
            'method' => 'POST',
            'url'    => route('admin.header.update'),
            'model'  => $object
        ]);

        $data = [
            'object' => $object,
            'form'   => $form,
            'mode'   => 'Edycja'
        ];

        return view('admin.header.form', $data);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete($id)
    {
        $object = $this->header->find($id);
        $object->delete();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }

    /**
     * @param Request $request
     * @return $this
     * @throws AdminException
     */
    public function postUpdate(Request $request, Header $header)
    {
        try {
            $form = $this->form(HeaderForm::class);

            if (!$form->isValid()) {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = $header->find($request->get('id'));

            foreach ($request->input('translation') as $lang => $value) {
                if (!empty($value['text'])) {
                    $object->fill([
                        $lang => [
                            'text' => $value['text'],
                            'text2' => $value['text2'],
                            'text3' => $value['text3'],
                        ]
                    ]);
                }
            }

            $object->save();

            return redirect()->route('admin.headers')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e) {
            throw new AdminException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getSwitch($id)
    {
        $object = $this->header->find($id);
        if ($object->active) {
            $object->active = 0;
        } else {
            $object->active = 1;
        }
        $object->save();
        return redirect()->back();
    }
}
