<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\FaqForm;
use App\Jobs\Admin\CreateFaq;
use App\Jobs\Admin\SaveFaq;
use App\Repositories\FaqRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Config;
use Auth;

/**
 * Class FaqController
 * @package App\Http\Controllers\Admin
 */
class FaqController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var FaqRepository
     */
    protected $faqs;


    /**
     * @param FaqRepository $faqRepository
     */
    public function __construct(FaqRepository $faqRepository)
    {
        $this->faqs = $faqRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $data = [
            'objects' => $this->faqs->newest(50),
        ];

        return view('admin.faq.list', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        $object = $this->faqs->find($id);

        $form = $this->form(FaqForm::class, [
            'method' => 'POST',
            'url'    => route('admin.faqs.update'),
            'model'  => $object
        ]);

        $data = [
            'object' => $object,
            'form'   => $form,
            'mode'   => trans('admin.common.edit')
        ];

        return view('admin.faq.form', $data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request)
    {
        try
        {
            $form = $this->form(FaqForm::class);

            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = $this->faqs->find($request->get('id'));
            $this->dispatch(
                new SaveFaq($object, $request)
            );

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $form = $this->form(FaqForm::class, [
            'method' => 'POST',
            'url'    => route('admin.faqs.store'),
        ]);

        $data = [
            'form' => $form,
            'mode' => trans('admin.common.add')
        ];

        return view('admin.faq.form', $data);
    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request)
    {
        try
        {
            $form = $this->form(FaqForm::class);
            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $this->dispatch(
                new CreateFaq($request)
            );

            return redirect()->route('admin.faqs')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        $object = $this->faqs->find($id);
        $object->delete();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }
}
