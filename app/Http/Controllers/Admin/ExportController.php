<?php

namespace App\Http\Controllers\Admin;

use Carbon\Carbon;
use App\Helpers\StrHelper;
use Illuminate\Http\Request;
use App\Repositories\ContactRepository;
use App\Repositories\CountryRepository;
use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use App\Repositories\PostCodeRepository;
use App\Repositories\UserRepository;
use Excel;
use Mail;

/**
 * Class AreaController
 * @package App\Http\Controllers\Admin
 */
class ExportController extends Controller {

    protected $country;
    protected $postal;
    protected $orders;
    protected $contacts;
    protected $users;

    public function __construct(
        CountryRepository $countryRepository,
        PostCodeRepository $postCodeRepository,
        OrderRepository $orderRepository,
        ContactRepository $contactRepository,
        UserRepository $userRepository
    )
    {
        $this->country = $countryRepository;
        $this->postal = $postCodeRepository;
        $this->orders = $orderRepository;
        $this->contacts = $contactRepository;
        $this->users = $userRepository;
    }

    public function usersForNewPanel()
    {
        $users = $this->users->with('role')->all();
        $rows = collect();
        foreach($users as $user)
        {
            switch ($user->role->first()->name)
            {
                case 'consultant':
                    $role = 'agent';
                    break;
                case 'admin':
                    $role = 'su';
                    break;
                case 'css':
                    $role = 'supervisor';
                    break;
                case 'ccadmin':
                    $role = 'ccadmin';
                    break;
                case 'headcc':
                    $role = 'ccadmin';
                    break;
                case 'editor':
                    $role = 'spectator';
                    break;
                case 'viewer':
                    $role = 'spectator';
                    break;
                default:
                    $role = null;
            }

            $rows->push([
                'name' => $user->name,
                'signature' => $user->signature,
                'email' => $user->email,
                'password' => $user->password,
                'role' => $role
            ]);
        }

        return Excel::create('IndonesiaUsers', function ($excel) use ($rows)
        {
            $excel->sheet('IndonesiaUsers', function ($sheet) use ($rows)
            {
                $sheet->fromArray($rows->toArray());

            });
        })->export('csv');
    }

    public function getIndex(Request $request)
    {
//        Excel::load(storage_path('app/ninjabanktransfer.csv'), function ($reader) {
//            $results = $reader->all();
//            $arr = [];
//            foreach($results as $result)
//            {
//                $postcode = (int)$result['postcode'];
//                $arr[$postcode] = 'case '.$postcode.':';
//
//            }
//            foreach($arr as $value)
//            {
//                echo $value.'<br>';
//            }
//        });

        if($request->has('range')){
            $range = explode(' - ', $request->get('range'));
            $orders = $this->orders->personalStats($range);
            $rows = collect();

            foreach($orders as $order)
            {
                if($order->sex || $order->age)
                {
                    $rows->push([
                        'order_id' => $order->id,
                        'created_at' => $order->created_at,
                        'product' => $order->languages_names,
                        'age' => $order->age,
                        'city' => isset($order->address) ? $order->address->city : '',
                        'sex' => $order->sex,
                    ]);
                }
            }

            return Excel::create('personalStats', function ($excel) use ($rows)
            {
                $excel->sheet('personalStats', function ($sheet) use ($rows)
                {
                    $sheet->fromArray($rows->toArray());

                });
            })->export('csv');

        }

        return view('admin.contact.personalStats', [
            'range' => $request->get('range', Carbon::now()->subWeek(1)->format('d/m/Y').' - '.Carbon::now()->format('d/m/Y')),
        ]);
    }

    public function areas()
    {
//        $postCodeRepo = $this->postal;
//        $rows = collect();
//        Excel::load(storage_path('app/newcouriers.csv'), function ($reader) use ($postCodeRepo, $rows) {
//            $results = $reader->all();
//            $results->each(function ($sheet) use ($postCodeRepo, $rows) {
//                $object = $sheet->all();
//
//                $postCodeNumber = strtolower($object['postcode']);
//
//                $postCode = $postCodeRepo->findByField('name', $postCodeNumber)->first();
//                if($postCode){
//                    $rows->push([
//                        'postal' => $postCode->name,
//                        'jayon' => $postCode->jayon,
//                        'ninja' => $postCode->ninja,
//                        'rpx' => $postCode->rpx,
//                        'asp' => $postCode->asp,
//                        'sap' => $postCode->sap
//                    ]);
//                }
//            });
//        });

//        $indonesia = $this->country->findByField('name', 'Indonesia')->first();
//        $provinces = $indonesia->provinces;
//        $rows = collect();
//        foreach($provinces as $province)
//        {
//            foreach($province->cities as $city)
//            {
//                foreach($city->districts as $district)
//                {
//                    foreach($district->subDistricts as $subDistrict){
////                        if($subDistrict->postcode->shipping_provider_name=='JAYON' || $subDistrict->postcode->shipping_provider_name=='NINJA' || $subDistrict->postcode->shipping_provider_name=='RPX'){
//                            $rows->push([
//                                'province' => $province->name,
//                                'city' => $city->name,
//                                'district' => $district->name,
//                                'subdistrict' => $subDistrict->name,
//                                'postcode' => $subDistrict->postcode->name
//                            ]);
////                        }
//                    }
//                }
//            }
//        }

        $chunks = $rows->chunk(50000);
        set_time_limit(0);
        foreach ($chunks as $key => $chunk) {
            return Excel::create('medical'.$key, function ($excel) use ($chunk)
            {
                $excel->sheet('medical', function ($sheet) use ($chunk)
                {
                    $sheet->fromArray($chunk->toArray());

                });
            })->export('csv');
        }
    }

    public function getMailsFromFile()
    {
        $file = file_get_contents(public_path('mails.txt'));

        $all = explode(':', $file);

        $emails = collect();
        foreach($all as $item)
        {
            if(strpos($item, '@') !== false){
                $emails->push($item);
            }
        }

        $rows = collect();

        foreach($emails as $email){
            $rows->push([
                'Email' => $email
            ]);
        }

        $chunks = $rows->chunk(100000);
        set_time_limit(0);
        foreach ($chunks as $key => $chunk) {
            Excel::create('Mails'.$key, function ($excel) use ($chunk)
            {
                $excel->sheet('Mails', function ($sheet) use ($chunk)
                {
                    $sheet->fromArray($chunk->toArray());

                });
            })->store('xls', false, true);
        }
    }

    public function getCouriersReport()
    {
        $orders = $this->orders->aftercodWithAddress();
        $rows = collect();
        $unpopular = collect();
        foreach($orders as $order)
        {
            if($order->sex || $order->age)
            {
                $rows->push([
                    'id' => $order->id,
                    'created_at' => $order->created_at,
                    'status' => $order->status,
                    'sex' => $order->sex,
                    'age' => $order->age,
                    'price' => $order->price,
                    'city' => $order->address->city,
                ]);
                if($order->address->district->city && ($order->status===4 || $order->status===6 || $order->status===8))
                {
                    $unpopular->push([
                        'city' => $order->address->city
                    ]);
                }
            }
        }
        $city = $rows->sortBy('city')->groupBy('city')->max();
        $unpopularCity = $unpopular->sortBy('city')->groupBy('city')->max();
        $sexes = $rows->groupBy('sex');
        $ages = $rows->sortBy('age')->groupBy('age');
        $rows->push([]);
        $rows->push([
            'Most popular city => '.$city[0]['city']
        ]);
        $rows->push([]);
        $rows->push([
            'Most unpopular city => '.$unpopularCity[0]['city']
        ]);
        $rows->push([]);
        $rows->push([
            $sexes['female']->count().' females',
            $sexes['male']->count().' males'
        ]);
        $rows->push([]);
        foreach($ages as $key => $age)
        {
            $rows->push([
                'age of '.$key.' => '.$age->count(),
            ]);
        }
        return Excel::create('codPromosSeruReport', function ($excel) use ($rows)
        {
            $excel->sheet('codPromosSeruReport', function ($sheet) use ($rows)
            {
                $sheet->fromArray($rows->toArray());

            });
        })->export('csv');
    }

    public function getMailReport()
    {
        $contacts = $this->contacts->currentYear();
        $rows = collect();
        foreach($contacts as $contact)
        {
            $rows->push([
                'name' => $contact->name,
                'email' => $contact->email,
                'created_at' => $contact->created_at,
            ]);
        }

        $rows = $rows->unique('email');

        $chunks = $rows->chunk(100000);
        set_time_limit(0);
//        foreach ($chunks as $key => $chunk) {
            return Excel::create('medical', function ($excel) use ($rows)
            {
                $excel->sheet('mails', function ($sheet) use ($rows)
                {
                    $sheet->fromArray($rows->toArray());

                });
            })->export('csv');
//        }
    }

    public function getMailReportSold()
    {
        $rows = collect();
        $contacts = $this->contacts->searchSold();
        foreach($contacts as $contact)
        {
            if($contact->name && $contact->email && $contact->product)
            {
                $rows->push([
                    'name' => $contact->name,
                    'email' => $contact->email,
                    'product' => $contact->product,
                    'created_at' => $contact->created_at,
                ]);
            }
        }

        $rows = $rows->unique('email');

        return Excel::create('SoldMails', function ($excel) use ($rows)
        {
            $excel->sheet('SoldMails', function ($sheet) use ($rows)
            {
                $sheet->fromArray($rows->toArray());

            });
        })->export('csv');
    }

    public function getMailReportUnsold()
    {
        $rows = collect();
        $orders = $this->orders->beforecodWithAddress();
        foreach($orders as $order)
        {
            if($order->name && $order->email && $order->languages_names)
            {
                $rows->push([
                    'name' => $order->name,
                    'email' => $order->email,
                    'product' => $order->languages_names,
                    'created_at' => $order->created_at,
                ]);
            }
        }

        $rows = $rows->unique('email');

        return Excel::create('UnsoldMails', function ($excel) use ($rows)
        {
            $excel->sheet('UnsoldMails', function ($sheet) use ($rows)
            {
                $sheet->fromArray($rows->toArray());

            });
        })->export('csv');
    }

    public function getParams()
    {
        set_time_limit(0);
        $contactRepo = $this->contacts;
        $rows = collect();
        Excel::load(storage_path('app/params.csv'), function ($reader) use ($rows, $contactRepo) {
            $results = $reader->all();
            $results->each(function ($sheet) use ($rows, $contactRepo) {
                $object = $sheet->all();

                $id = strtolower($object['id']);
                $phone = strtolower($object['phone']);

                $phone = StrHelper::validatePhone($phone);
                $contact = $contactRepo->findWhere([['phone', '=', $phone], ['ext_client_id', '=', 10]])->first();
                if($contact){
//                    $params = base64_decode($contact->params);
                    $decodedParams = explode('&', base64_decode($contact->params));
//                    if(strpos($params, 'campaign_id') !== false)
//                    {

                        $data = [
                            'id' => $id,
                            'phone' => $contact->phone,
                            'click_id' => $contact->click_id,
                            'pub_id' => $contact->pub_id,
                            'created_at' => $contact->created_at,
                            'status' => $contact->getStatus()
                        ];
                        $rows->push($data);

//                        foreach($decodedParams as $decodedParam)
//                        {
//                            if($decodedParam!='')
//                            {
//                                $param = explode('=', $decodedParam);
//                                $data['params_'.$param[0]] = $param[1];
//                            }
//                            else
//                            {
//                                $data[] = '';
//                            }
//                        }
//                    $rows->push($data);
//                    }
                }
            });
        });

        Excel::create('FoxyAdsMedical', function ($excel) use ($rows)
        {
            $excel->sheet('FoxyAdsMedical', function ($sheet) use ($rows)
            {
                $sheet->fromArray($rows->toArray());

            });
        })->export('csv');
    }

//    public function codOrderMade()
//    {
//        $codTestOrder = $this->orders->find(89677);
//        $orderMailer = app('App\Mail\OrderMailer');
//        $orderMailer->codOrderMade($codTestOrder);
//    }
//
//    public function codOrderSent()
//    {
//        $codTestOrder = $this->orders->find(89677);
//        $orderMailer = app('App\Mail\OrderMailer');
//        $orderMailer->codOrderSent($codTestOrder);
//    }
//
//    public function codOrderDelivered()
//    {
//        $codTestOrder = $this->orders->find(89677);
//        $orderMailer = app('App\Mail\OrderMailer');
//        $orderMailer->codOrderDelivered($codTestOrder);
//    }
//
//    public function noncodOrderMade()
//    {
//        $nonCodTestOrder = $this->orders->find(89677);
//        $orderMailer = app('App\Mail\OrderMailer');
//        $orderMailer->noncodOrderMade($nonCodTestOrder);
//    }
//
//    public function noncodOrderSent()
//    {
//        $nonCodTestOrder = $this->orders->find(89677);
//        $orderMailer = app('App\Mail\OrderMailer');
//        $orderMailer->noncodOrderSent($nonCodTestOrder);
//    }
//
//    public function noncodOrderDelivered()
//    {
//        $nonCodTestOrder = $this->orders->find(89677);
//        $orderMailer = app('App\Mail\OrderMailer');
//        $orderMailer->noncodOrderDelivered($nonCodTestOrder);
//    }

//    public function leadtime()
//    {
//        set_time_limit(0);
//        $rows = collect();
//        Excel::load(storage_path('app/leadtime.csv'), function ($reader) use ($rows) {
//            $results = $reader->all();
//            $results->each(function ($sheet) use ($rows) {
//                $object = $sheet->all();
//
//                $province = strtolower($object['province']);
//                $city = strtolower($object['city']);
//                $district = strtolower($object['district']);
//                $subdistrict = strtolower($object['subdistrict']);
//                $postcode = strtolower($object['postcode']);
//                $leadtime = strtolower($object['leadtime']);
//
//                if($leadtime!='#n/d')
//                {
//                    if(strpos($leadtime, '-') !== false) {
//                        $leadTimeExploded = explode('-', $leadtime);
//                        $from = (int)$leadTimeExploded[0];
//                        $to = (int)$leadTimeExploded[1];
//                        $from+=1;
//                        $to+=1;
//                        $leadtime = $from.'-'.$to;
//                    } else {
//                        $leadtime = (int)$leadtime+1;
//                    }
//
//                    $data = [
//                        'province' => $province,
//                        'city' => $city,
//                        'district' => $district,
//                        'subdistrict' => $subdistrict,
//                        'postcode' => $postcode,
//                        'leadtime' => $leadtime,
//                    ];
//                    $rows->push($data);
//                }
//            });
//        });
//
//        Excel::create('leadtime', function ($excel) use ($rows)
//        {
//            $excel->sheet('leadtime', function ($sheet) use ($rows)
//            {
//                $sheet->fromArray($rows->toArray());
//
//            });
//        })->export('csv');
//    }

}
