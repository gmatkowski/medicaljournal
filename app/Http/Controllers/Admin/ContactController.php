<?php

namespace App\Http\Controllers\Admin;

use App\Forms\GoToPageForm;
use App\Jobs\CreateZipFile;
use App\Repositories\ContactRepository;
use App\Repositories\JobRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\File;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Excel;
use Entrust;

/**
 * Class ContactController
 * @package App\Http\Controllers\Admin
 */
class ContactController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var JobRepository
     */
    protected $contacts;

    protected $products = [
        'cambogia', 'blackmask', 'hallupro'
    ];

    /**
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository)
    {
        $this->contacts = $contactRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        if (Entrust::ability('admin', 'list-contacts'))
        {
            $product = $request->get('product');
            $page = $request->get('page', 1);

            Paginator::currentPageResolver(function () use ($page)
            {
                return $page;
            });

            if (!$request->has('query'))
            {
                if (Entrust::hasRole('css')){
                    $contacts = collect();
                } else {
                    $contacts = $this->contacts->newest(50, $product);
                }
                $data = [
                    'objects' => $contacts,
                    'mode'    => 'list',
                    'time'    => [
                        'now'     => Carbon::now()->format('d/m/Y'),
                        'yesterday' => Carbon::yesterday()->format('d/m/Y')
                    ]

                ];
            }
            else
            {
                $query = $request->get('query');
                $data = [
                    'objects' => $this->contacts->search($query, $product),
                    'mode'    => 'search',
                    'query'   => $query,
                    'time'    => [
                        'now'     => Carbon::now()->format('d/m/Y'),
                        'yesterday' => Carbon::yesterday()->format('d/m/Y')
                    ]
                ];
            }

            $data['prod'] = $product;
            $data['summaryRange'] = $request->get('summaryRange') ? $request->get('summaryRange') : Carbon::yesterday()->format('d/m/Y').' - '.Carbon::now()->format('d/m/Y');
            $data['counters'] = [
                'today' => $this->contacts->todays($product),
                'yesterday' => $this->contacts->yesterdays($product),
                'specificDay' => $this->contacts->specificDate($product, $data['summaryRange'])
            ];
            $productSymbol = $request->get('product');
            $data['goToPageForm'] = $this->form(GoToPageForm::class);
            $data['productSymbol'] = $productSymbol;

            return view('admin.contact.list', $data);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function postExport(Request $request)
    {
        if(Entrust::ability('admin', 'export-contacts')) {

            $languages = collect();

            $range = $request->get('range');

            if (!preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4} - [0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $range)) {
                return redirect()->back();
            } else {

                $range = explode(' - ', $range);
                $dt1 = Carbon::createFromFormat('d/m/Y', $range[0])->startOfDay();
                $dt2 = Carbon::createFromFormat('d/m/Y', $range[1])->startOfDay();

                $contacts = $this->contacts->getByDateRange($dt1, $dt2, $request->get('product'), false);
                if($contacts->count()==0)
                {
                    return back();
                }

                foreach ($contacts as $language) {
//                    $language->setVisible([
//                        'name', 'email', 'phone', 'age', 'type_name', 'language_name', 'created_at'
//                    ]);

                    $language = [
                        'name' => $language->name,
                        'email'  => $language->email,
                        'phone'  => $language->phone,
                        'age'  => $language->age,
                        'type_name' => $language->type_name,
                        'language_name'  => $language->language_name,
                        'created_at' => $language->created_at,
                        'params' => base64_decode($language->params)
                    ];

                    $languages->push($language);
                }

                $chunks = $languages->chunk(2000);

                ob_end_clean();
                ob_start();

                $files = collect();

                foreach ($chunks as $key => $chunk) {
                    $info = Excel::create('ContactList-' . Carbon::now()->format('Y-m-d') . '-part-' . ($key + 1), function ($excel) use ($chunk) {
                        $excel->sheet('contacts', function ($sheet) use ($chunk) {
                            $sheet->fromArray($chunk->toArray());
                        });
                    })->store('xls', false, true);

                    $files->push($info['full']);
                    unset($info);
                }

                ob_end_flush();

                $filename = 'ContactList-' . Carbon::now()->format('Y-m-d') . '-all.zip';
                $file_path = storage_path('exports/' . $filename);

                $response = $this->dispatch(
                    new CreateZipFile($files->toArray(), $file_path)
                );

                if($response){
                    foreach ($files as $file) {
                        File::delete($file);
                    }

                    return response()->download($file_path);
                }
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        if(Entrust::ability('admin', 'delete-orders')){

            $object = $this->contacts->find($id);
            $object->delete();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);
        }
    }

    public function getContacts()
    {
        $rows = collect();

        $dt1 = Carbon::createFromFormat('d/m/Y', '01/01/2017')->startOfDay();
        $dt2 = Carbon::now();

        foreach($this->products as $product){
            $contacts = $this->contacts->getByDateRange($dt1, $dt2, $product, false);

            foreach ($contacts as $language) {
                $language->setVisible([
                    'email'
                ]);

                if($language->email){
                    $rows->push($language);
                }
            }
        }

        return Excel::create('ContactsMedical', function ($excel) use ($rows)
        {
            $excel->sheet('ContactsMedical', function ($sheet) use ($rows)
            {
                $sheet->fromArray($rows->toArray());

            });
        })->export('csv');
    }
}
