<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\PostForm;
use App\Jobs\Admin\CreatePost;
use App\Jobs\Admin\SavePost;
use App\Repositories\PostRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Config;
use Auth;
use Entrust;

/**
 * Class PostController
 * @package App\Http\Controllers\Admin
 */
class PostController extends Controller
{

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var PostRepository
     */
    protected $posts;


    /**
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->posts = $postRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        $data = ['objects' => $this->posts->all()];

        return view('admin.blog.list', $data);
    }


    public function getEdit($id)
    {
        if (Entrust::ability('admin', 'edit-posts')) {
            $object = $this->posts->find($id);

            $form = $this->form(PostForm::class, [
                'method' => 'POST',
                'url'    => route('admin.blog.update'),
                'model'  => $object
            ]);

            $data = [
                'object' => $object,
                'form'   => $form,
                'mode'   => trans('admin.blog.edit')
            ];

            return view('admin.blog.form', $data);
        } else {
            throw new AdminException('Permission denied.');
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request)
    {
        try {
            $form = $this->form(PostForm::class);

            if (!$form->isValid()) {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = $this->posts->find($request->get('id'));
            $this->dispatch(
                new SavePost($object, $request)
            );

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e) {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }


    public function getCreate()
    {
        if (Entrust::ability('admin', 'create-posts')) {
            $form = $this->form(PostForm::class, [
                'method' => 'POST',
                'url'    => route('admin.blog.store'),
            ]);

            $data = [
                'form' => $form,
                'mode' => trans('admin.blog.add')
            ];

            return view('admin.blog.form', $data);
        } else {
            throw new AdminException('Permission denied.');
        }
    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request)
    {
        try {
            $form = $this->form(PostForm::class);
            if (!$form->isValid()) {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $this->dispatch(
                new CreatePost($request)
            );

            return redirect()->route('admin.blog')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e) {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }


    public function getDelete($id)
    {
        if (Entrust::ability('admin', 'delete-posts')) {
            $object = $this->posts->find($id);
            $object->delete();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);
        } else {
            throw new AdminException('Permission denied.');
        }
    }
}
