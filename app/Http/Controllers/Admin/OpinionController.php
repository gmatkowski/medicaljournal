<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\OpinionForm;
use App\Jobs\Admin\CreateOpinion;
use App\Jobs\Admin\SaveOpinion;
use App\Repositories\OpinionRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Config;
use Auth;

/**
 * Class OpinionController
 * @package App\Http\Controllers\Admin
 */
class OpinionController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var OpinionRepository
     */
    protected $opinions;


    public function __construct(OpinionRepository $opinionRepository)
    {
        $this->opinions = $opinionRepository;
    }


    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $data = [
            'objects' => $this->opinions->newest(50)
        ];

        return view('admin.opinion.list', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        $object = $this->opinions->find($id);

        $form = $this->form(OpinionForm::class, [
            'method' => 'POST',
            'url'    => route('admin.opinions.update'),
            'model'  => $object
        ]);

        $data = [
            'object' => $object,
            'form'   => $form,
            'mode'   => trans('admin.common.edit')
        ];

        return view('admin.opinion.form', $data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request)
    {
        try
        {
            $form = $this->form(OpinionForm::class);

            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = $this->opinions->find($request->get('id'));
            $this->dispatch(
                new SaveOpinion($object, $request)
            );

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $form = $this->form(OpinionForm::class, [
            'method' => 'POST',
            'url'    => route('admin.opinions.store'),
        ]);

        $data = [
            'form' => $form,
            'mode' => trans('admin.common.add')
        ];

        return view('admin.opinion.form', $data);
    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request)
    {
        try
        {
            $form = $this->form(OpinionForm::class);
            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $this->dispatch(
                new CreateOpinion($request)
            );

            return redirect()->route('admin.opinions')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        $object = $this->opinions->find($id);
        $object->delete();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }
}
