<?php

namespace App\Http\Controllers\Admin;

use App\Forms\GoToPageForm;
use App\Repositories\OrderInvoiceRepository;
use App\Repositories\PostCodeRepository;
use App\Shipping\JayonExpressProvider;
use App\Shipping\NinjaProvider;
use Illuminate\Contracts\Filesystem\Factory as FileSystem;
use Illuminate\Support\Facades\File;
use App\Jobs\CreateZipFile;
use App\Helpers\InvoiceHelper;
use App\Exceptions\AdminException;
use App\Forms\Admin\SearchForm;
use App\Forms\Admin\OrderForm;
use App\Order;
use App\Repositories\CityRepository;
use App\Repositories\ProvinceRepository;
use App\Jobs\CreateOrder;
use App\Jobs\Admin\SaveOrder;
use App\Repositories\Criteria\UserCriteria;
use App\Repositories\Criteria\OrderCriteria;
use App\Repositories\LanguageRepository;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Shipping\ShippingProvider;
use App\Exceptions\PageException;
use App\Shipping\RPXProvider;
use Barryvdh\Snappy\Facades\SnappyPdf;
use Auth;
use Excel;
use Entrust;

/**
 * Class orderController
 * @package App\Http\Controllers\Admin
 */
class OrderController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var OrderRepository
     */
    protected $orders;

    /**
     * @var OrderInvoiceRepository
     */
    protected $invoices;

    /**
     * @var LanguageRepository
     */
    protected $languages;

    /**
     * @var InvoiceHelper
     */
    protected $invoice;

    /**
     * @var FileSystem
     */
    protected $fileSystem;

    protected $postCodes;

    /**
     * @param OrderRepository $orderRepository
     */
    public function __construct(
        OrderRepository $orderRepository,
        OrderInvoiceRepository $orderInvoiceRepository,
        LanguageRepository $languageRepository,
        InvoiceHelper $invoiceHelper,
        FileSystem $fileSystem,
        PostCodeRepository $postCodeRepository
    )
    {
        $this->orders = $orderRepository;
        $this->invoices = $orderInvoiceRepository;
        $this->languages = $languageRepository;
        $this->invoice = $invoiceHelper;
        $this->fileSystem = $fileSystem;
        $this->postCodes = $postCodeRepository;
    }

    /**
     * @param Request $request
     * @param $query
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $productSymbol = $request->get('product');
        $page = $request->get('page', 1);
        $searchForm = $this->form(SearchForm::class, ['route'  => 'admin.orders']);

        if (!$request->has('query'))
        {
            $user = $request->user();
            if (!$user->ability('admin', 'list-all-orders'))
            {
                $this->orders->pushCriteria(
                    new UserCriteria($user)
                );
            }

            $orders = $this->orders->listAll(10, $this->getProductId($productSymbol));
            if($orders->lastPage()!=0 && $page > $orders->lastPage()){
                return redirect(route('admin.orders', ['product' => $productSymbol]));
            }
        } else {
            $query = $request->get('query');
            $orders = $this->orders->search($query, $this->getProductId($productSymbol));
        }
        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $order = new Order();

        if (!$request->has('query'))
        {
            $data = [
                'orders'   => $orders,
                'mode'     => 'list',
                'productSymbol'  => $productSymbol,
                'methods'  => $order->methods,
                'statuses' => $order->getStatuses(),
                'time'     => [
                    'now'  => Carbon::now()->format('d/m/Y'),
                    'past' => Carbon::now()->subWeek(1)->format('d/m/Y')
                ]
            ];
        }
        else
        {
            if (!$searchForm->isValid())
            {
                return redirect()->back()->withErrors($searchForm->getErrors())->withInput();
            }
            $data = [
                'orders'   => $orders,
                'mode'     => 'search',
                'productSymbol'  => $productSymbol,
                'query'    => $query,
                'methods'  => $order->methods,
                'statuses' => $order->getStatuses(),
                'time'     => [
                    'now'  => Carbon::now()->format('d/m/Y'),
                    'past' => Carbon::now()->subWeek(1)->format('d/m/Y')
                ]
            ];
        }

        $data['summaryRange'] = $request->has('summaryRange') ? $request->get('summaryRange') : Carbon::now()->format('d/m/Y');
        $data['counters'] = [
            'orders'      => !$request->has('query') ? $data['orders']->total() : $data['orders']->count(),
            'today'       => $this->orders->todays($this->getProductId($productSymbol)),
            'yesterday'   => $this->orders->yesterdays($this->getProductId($productSymbol)),
            'specificDay' => $this->orders->specificDay($this->getProductId($productSymbol), $data['summaryRange'])
        ];
        $data['goToPageForm'] = $this->form(GoToPageForm::class);
        $data['searchForm'] = $searchForm;

        return view('admin.order.list', $data);
    }

    /**
     * @param ProvinceRepository $provinces
     * @param CityRepository $cities
     * @param FormBuilder $formBuilder
     * @param $id
     * @return mixed
     */
    public function getEdit(PostCodeRepository $postalCodes, CityRepository $cities, ProvinceRepository $provinces, FormBuilder $formBuilder, $id)
    {
        if (Entrust::ability('admin', 'edit-order'))
        {
            $order = $this->orders->find($id);

            $form = $formBuilder->create('App\Forms\Admin\OrderForm', [
                'method' => 'POST',
                'url'    => route('admin.orders.update'),
                'model'  => $order
            ]);

            if(isset($order->address) && $order->address->district){
               $postalCode = $order->getPostalCode();
               $cityId = $order->address->district->city->id;
               $provinceId = $order->address->district->city->province->id;

               $finalPostalCode = $postalCodes->findByField('name', $postalCode)->first();
               $districtList = $cities->find($cityId)->districts->sortBy('name')->lists('name', 'id')->toArray();
               $cityList = $provinces->find($provinceId)->cities->sortBy('name')->lists('name', 'id')->toArray();
               $provinceList = $provinces->all()->sortBy('name')->lists('name', 'id')->toArray();

               $form->modify('subdistrict', 'select', [
                    'choices' => $finalPostalCode ? $finalPostalCode->subDistricts->sortBy('name')->lists('name', 'id')->toArray() : [],
               ]);

               $form->modify('district_id', 'select', [
                    'choices' => $districtList,
               ]);

               $form->modify('city_id', 'select', [
                    'choices' => $cityList,
               ]);

               $form->modify('province_id', 'select', [
                    'choices' => $provinceList,
               ]);
            }

            $data = [
                'productSymbol' => request()->get('product'),
                'order'     => $order,
                'form'      => $form,
                'mode'      => trans('admin.common.edit'),
                'languages' => $this->languages->all(),
            ];

            return view('admin.order.form', $data);
        }
        else
        {
            throw new AdminException('Permission denied.');
        }
    }

    protected function getProductId($productSymbol)
    {
        if($productSymbol){
            return $this->languages->findWhere([['symbol', '=', $productSymbol]])->first()->id;
        }
        return 1;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request)
    {
        if (Entrust::ability('admin', 'edit-order'))
        {
            try
            {
                $request->merge([
                    'district_id' => $request->get('hiddenDistrict'),
                    'city_id' => $request->get('hiddenCity'),
                    'province_id' => $request->get('hiddenProvince')
                ]);

                $form = $this->form(OrderForm::class);

                if (!$form->isValid())
                {
                    return redirect()->back()->withErrors($form->getErrors())->withInput();
                }

                $order = $this->orders->find($request->get('id'));

                if(isset($order->address)==false){
                    $order->address()->create([
                        'full_name'   => $order->last_name ? $order->first__name.' '.$order->last_name : $order->first__name,
                        'first_name'  => $order->first__name,
                        'last_name'   => $order->last_name ? $order->last_name : $order->first__name,

                        'postal_code' => $request->get('postal_code'),
                        'address'     => $request->get('address'),
                        'province' => $request->get('province_id'),
                        'city' => $request->get('city_id'),
                        'district_id' => $request->get('district_id'),
                        'subdistrict' => $request->get('subdistrict'),
                    ]);
                }

                if(!isset($order->invoice) && $order->shipped===false)
                {
                    $this->dispatch(
                        new SaveOrder($order, $request)
                    );

                    return redirect()->route('admin.orders.to.confirm', ['?product='.$request->get('product')])->with('message', [
                        'type'    => 'success',
                        'message' => trans('translations.action_done')
                    ]);
                }

                return back()->with('message', [
                    'type'    => 'error',
                    'message' => trans('translations.edit_invoice_or_shipped')
                ]);

            } catch (\Exception $e)
            {
                throw new AdminException($e->getMessage(), $e->getCode());
            }
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyStatusList()
    {
        $order = new Order();

        $statuses = $order->getStatuses();
        $result = collect();
        foreach ($statuses as $key => $status)
        {
            $result->push([
                'value' => $key,
                'text'  => $status
            ]);
        }

        return response()->json($result);
    }

    /**
     * @param string $filter
     * @param int $status
     * @return mixed
     */
    public function postExport(Request $request, $filter = 'all', $status = -1)
    {
        if (Entrust::ability('admin', 'export-orders'))
        {

            //return $request->input('range'). " " .$filter. " " . $status;

            $range = $request->get('range');

            if (!preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4} - [0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $range))
            {
                return redirect()->back();
            }
            else
            {

                $range = explode(' - ', $range);
                $dt0 = Carbon::createFromFormat('d/m/Y', $range[0])->subMonth(1)->format('Y-m-d H:i:s');
                $dt1 = Carbon::createFromFormat('d/m/Y', $range[0])->format('Y-m-d H:i:s');
                $dt2 = Carbon::createFromFormat('d/m/Y', $range[1])->format('Y-m-d H:i:s');

                if (Entrust::hasRole('css'))
                {
                    $dt1 = Carbon::now()->subWeek()->format('Y-m-d H:i:s');
                    $dt2 = Carbon::now()->format('Y-m-d H:i:s');
                }

                if (in_array($status, [1, 2, 3]))
                {
                    $status = [1, 2, 3];
                }

                switch ($filter)
                {
                    case 'all':
                        $orders = $this->orders->getByDateRange($dt1, $dt2, $this->getProductId($request->get('productSymbol')));
                        break;
                    default:
                        $orders = $this->orders->byMethod($filter, $status, $dt1, $dt2, $this->getProductId($request->get('productSymbol')));
                }

                $history = [];
                foreach ($this->orders->getByDateRange($dt0, $dt1, $this->getProductId($request->get('productSymbol'))) as $object)
                {
                    $history[] = $object->email;
                    $history[] = $object->phone;
                }

                $rows = collect();

                foreach ($orders as $order)
                {
                    $rows->push([
                        'Order #'                         => 'CM' . $order->id,
                        'Order Date'                      => $order->created_at_formated->format('d/m/Y'),
                        'Courier'                         => $order->getCourier(),
                        'Email'                           => $order->email,
                        'Shipping Addressee'              => $order->name,
                        'Shipping Address Line1'          => isset($order->address) ? $order->address->address : '',
                        'Shipping Address Line2'          => '',
                        'Shipping Address City'           => isset($order->address->district->city) ? $order->address->district->city->name : '',
                        'Shipping Address State/Province' => isset($order->address->district->city->province) ? $order->address->district->city->province->name : '',
                        'Shipping Address Country'        => isset($order->address->district->city->province->country) ? $order->address->district->city->province->country->name : '',
                        'Shipping Address Postal Code'    => $order->getPostalCode(),
                        'Shipping Address Phone'          => $order->phone,
                        'Item ID'                         => $order->languages->implode('language_id'),
                        'Item Qty'                        => 'USB1',
                        'Gross Total'                     => $order->price,
                        'Payment Type'                    => strtoupper($order->method_name),
                        'Shipping Type'                   => 'Default Shipping',
                        'Created at'                      => $order->created_at,
                        'Created by'                      => is_null($order->user['name']) ? 'customer' : $order->user['name'],
                        'Duplicated'                      => in_array($order->phone, $history) || in_array($order->email, $history) ? 'DUPLICATED' : ''

                    ]);
                }

                ob_end_clean();
                ob_start();

                return Excel::create('Orders-' . Carbon::now()->format('Y-m-d'), function ($excel) use ($rows)
                {
                    $excel->sheet('Orders', function ($sheet) use ($rows)
                    {
                        $sheet->fromArray($rows->toArray());

                    });
                })->export('csv');

            }
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    public function postEditable(Request $request)
    {
        $order = $this->orders->find($request->get('pk'));
        $order->{$request->input('name')} = $request->input('value');
        $order->save();

        return 'ok';
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails($id)
    {
        $order = $this->orders->find($id);

        $data = [
            'order' => $order,
        ];

        return view('admin.order.details', $data);
    }

    public function getInvoice($id, $invoiceType)
    {
        $data = $this->invoice->create($id, $invoiceType);

        $pdf = SnappyPdf::loadView('pdf.invoice', $data);

        return $pdf->download('invoice-'.$id.'.pdf');
    }

    public function postInvoices($title, $currentPage, $invoiceType, $productSymbol)
    {
        if(!$this->fileSystem->disk('public')->exists('exports')) {
            $this->fileSystem->disk('public')->makeDirectory('exports');
        }

        Paginator::currentPageResolver(function () use ($currentPage)
        {
            return $currentPage;
        });
        switch ($title)
        {
            case 'COD (not sent)':
                $orders = $this->orders->toNotSent(10, $this->getProductId($productSymbol));
                break;
            case 'COD (sent)':
                $orders = $this->orders->aftercod(10, $this->getProductId($productSymbol));
                break;
        }

        $files = collect();
        foreach($orders as $order){
            $data = $this->invoice->create($order->id, $invoiceType);
            $pdf = SnappyPdf::loadView('pdf.invoice', $data);
            $filepath = storage_path('exports/invoice-'.$order->id.'.pdf');
            $pdf->save($filepath);
            $files->push($filepath);
        }

        $download_path = 'exports/invoices-page-'.$currentPage.'-'.$productSymbol.'.zip';

        $response = $this->dispatch(
            new CreateZipFile($files->toArray(), public_path($download_path))
        );

        if($response){
            foreach ($files as $file) {
                File::delete($file);
            }
        }

        return redirect($download_path);
    }

//    public function postExportInvoices(Request $request)
//    {
//        if($request->has('type') && $request->has('range') && $request->has('product')){
//            $type = $request->get('type');
//            $range = $request->get('range');
//            $productSymbol = $request->get('product');
//            $dt = explode(' - ', $range);
//            $dt1 = Carbon::createFromFormat('d/m/Y', $dt[0])->startOfDay();
//            $dt2 = Carbon::createFromFormat('d/m/Y', $dt[1])->startOfDay();
//            $orders = $this->orders->getByDateRange($dt1, $dt2, $this->getProductId($productSymbol));
//
//            if(count($orders)==0){
//                return redirect('admin.orders.invoices', ['product' => $productSymbol]);
//            }
//
//            $files = collect();
//            foreach($orders as $order){
//                $data = $this->invoice->create($order->id, $type);
//                $pdf = SnappyPdf::loadView('pdf.invoice', $data);
//                $filepath = storage_path('exports/invoice-'.$order->id.'.pdf');
//                $pdf->save($filepath);
//                $files->push($filepath);
//            }
//
//            $zip_filepath = 'exports/invoices-'.str_replace('/', '.', $dt[0]).'-'.str_replace('/', '.', $dt[1]).'.zip';
//            $zip_path = public_path($zip_filepath);
//            if(!$this->fileSystem->disk('public')->exists('exports')) {
//                $this->fileSystem->disk('public')->makeDirectory('exports');
//            }
//
//            $response = $this->dispatch(
//                new CreateZipFile($files->toArray(), $zip_path)
//            );
//
//            if($response){
//                foreach ($files as $file) {
//                    File::delete($file);
//                }
//
//                return '/'.$zip_filepath;
//            }
//        } else {
//            return back();
//        }
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        if (Entrust::ability('admin', 'delete-order'))
        {
            $order = $this->orders->find($id);
            $order->delete();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getToCod(Request $request)
    {
        $productSymbol = $request->get('product');
        $page = $request->get('page', 1);
        $searchForm = $this->form(SearchForm::class, ['route'  => 'admin.orders.to.cod']);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        if (!$request->has('query')) {
            $user = $request->user();
            if (!$user->ability('admin', 'list-all-orders'))
            {
                $this->orders->pushCriteria(
                    new UserCriteria($user)
                );
            }

            $this->orders->pushCriteria(
                new OrderCriteria($request->input('f'))
            );

            $orders = $this->orders->toNotSent(10, $this->getProductId($productSymbol));
            if($orders->lastPage()!=0 && $page > $orders->lastPage()){
                return redirect(route('admin.orders.to.cod', ['product' => $productSymbol]));
            }

            $data = [
                'currentPage' => $orders->currentPage(),
                'orders' => $orders,
                'mode' => 'list',
                'title'  => trans('admin.menu.codtosend'),
                'summaryAction' => route('admin.orders.to.cod', ['product' => $productSymbol]),
            ];
        } else {
            if (!$searchForm->isValid())
            {
                return redirect()->back()->withErrors($searchForm->getErrors())->withInput();
            }

            $query = $request->get('query');
            $orders = $this->orders->searchNotSent($this->getProductId($productSymbol), $query);
            $data = [
                'currentPage' => 1,
                'orders'   => $orders,
                'mode'     => 'search',
                'query'    => $query,
                'time'     => [
                    'now'  => Carbon::now()->format('d/m/Y'),
                    'past' => Carbon::now()->subWeek(1)->format('d/m/Y')
                ]
            ];
        }

        $data['title'] = trans('admin.menu.codtosend');
        $data['productSymbol'] = $productSymbol;
        $data['goToPageForm'] = $this->form(GoToPageForm::class);
        $data['searchForm'] = $searchForm;


        return view('admin.order.list_cod', $data);
    }

//    public function getCodDuplicated(Request $request)
//    {
//        $productSymbol = $request->get('product');
//        $page = $request->get('page', 1);
//
//        $user = $request->user();
//        if (!$user->ability('admin', 'list-all-orders'))
//        {
//            $this->orders->pushCriteria(
//                new UserCriteria($user)
//            );
//        }
//
//        $this->orders->pushCriteria(
//            new OrderCriteria($request->input('f'))
//        );
//
//        $orders = $this->orders->duplicated(10, $this->getProductId($productSymbol));
//        if($orders->lastPage()!=0 && $page > $orders->lastPage()){
//            return redirect(route('admin.orders.attempts', ['product' => $productSymbol]));
//        }
//
//        Paginator::currentPageResolver(function () use ($page)
//        {
//            return $page;
//        });
//
//        $data = [
//            'productSymbol' => $productSymbol,
//            'orders' => $orders,
//            'title'  => trans('admin.menu.cod.duplicated'),
//
//            'summaryAction' => route('admin.orders.attempts'),
//
//            'goToPageForm' => $this->form(GoToPageForm::class)
//        ];
//
//        return view('admin.order.list_cod', $data);
//    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getCodToConfirm(Request $request)
    {
//        $productSymbol = $request->get('product');
//        $page = $request->get('page', 1);

//        $user = $request->user();
//        if (!$user->ability('admin', 'list-all-orders'))
//        {
//            $this->orders->pushCriteria(
//                new UserCriteria($user)
//            );
//        }

//        $this->orders->pushCriteria(
//            new OrderCriteria($request->input('f'))
//        );

//        $orders = $this->orders->toConfirm(10, $this->getProductId($productSymbol));
//        if($orders->lastPage()!=0 && $page > $orders->lastPage()){
//            return redirect(route('admin.orders.to.confirm', ['product' => $productSymbol]));
//        }

//        Paginator::currentPageResolver(function () use ($page)
//        {
//            return $page;
//        });

//        $data = [
//            'productSymbol' => $productSymbol,
//            'orders' => $orders,
//            'title'  => trans('admin.menu.codtoconfirm'),
//        ];
//        $data['goToPageForm'] = $this->form(GoToPageForm::class);


        $productSymbol = $request->get('product');
        $page = $request->get('page', 1);
        $searchForm = $this->form(SearchForm::class, ['route'  => 'admin.orders.to.confirm']);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        if (!$request->has('query')) {
            $user = $request->user();
            if (!$user->ability('admin', 'list-all-orders'))
            {
                $this->orders->pushCriteria(
                    new UserCriteria($user)
                );
            }

            $this->orders->pushCriteria(
                new OrderCriteria($request->input('f'))
            );

            $orders = $this->orders->toConfirm(10, $this->getProductId($productSymbol));
            if($orders->lastPage()!=0 && $page > $orders->lastPage()){
                return redirect(route('admin.orders.to.confirm', ['product' => $productSymbol]));
            }

            $data = [
                'currentPage' => $orders->currentPage(),
                'orders' => $orders,
                'mode' => 'list',
                'summaryAction' => route('admin.orders.to.cod', ['product' => $productSymbol]),
            ];
        } else {
            if (!$searchForm->isValid())
            {
                return redirect()->back()->withErrors($searchForm->getErrors())->withInput();
            }

            $query = $request->get('query');
            $orders = $this->orders->searchToConfirm($this->getProductId($productSymbol), $query);
            $data = [
                'currentPage' => 1,
                'orders'   => $orders,
                'mode'     => 'search',
                'query'    => $query,
                'time'     => [
                    'now'  => Carbon::now()->format('d/m/Y'),
                    'past' => Carbon::now()->subWeek(1)->format('d/m/Y')
                ]
            ];
        }

        $data['title'] = trans('admin.menu.codtoconfirm');
        $data['productSymbol'] = $productSymbol;
        $data['goToPageForm'] = $this->form(GoToPageForm::class);
        $data['searchForm'] = $searchForm;

        return view('admin.order.list_cod_tc', $data);
    }

    public function getCodArchived(Request $request)
    {
        $productSymbol = $request->get('product');
        $page = $request->get('page', 1);

        $user = $request->user();
        if (!$user->ability('admin', 'list-all-orders'))
        {
            $this->orders->pushCriteria(
                new UserCriteria($user)
            );
        }

        $orders = $this->orders->archived(10, $this->getProductId($productSymbol));
        if($orders->lastPage()!=0 && $page > $orders->lastPage()){
            return redirect(route('admin.orders.archived', ['product' => $productSymbol]));
        }

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $data = [
            'productSymbol' => $productSymbol,
            'orders' => $orders,
            'title'  => trans('admin.menu.cod-archived'),
        ];
        $data['goToPageForm'] = $this->form(GoToPageForm::class);

        return view('admin.order.archived', $data);
    }

    public function getCodArchive($id)
    {
        try
        {
            $order = $this->orders->find($id);

            $order->archived = 1;
            $order->status = 6;
            $order->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getAfterCod(Request $request)
    {
        $productSymbol = $request->get('product');
        $page = $request->get('page', 1);
        $searchForm = $this->form(SearchForm::class, ['route'  => 'admin.orders.after.cod']);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        if (!$request->has('query')) {
            $user = $request->user();
            if (!$user->ability('admin', 'list-all-orders'))
            {
                $this->orders->pushCriteria(
                    new UserCriteria($user)
                );
            }

            $this->orders->pushCriteria(
                new OrderCriteria($request->input('f'))
            );

            $orders = $this->orders->aftercod(10, $this->getProductId($productSymbol));
            if($orders->lastPage()!=0 && $page > $orders->lastPage()){
                return redirect(route('admin.orders.after.cod', ['product' => $productSymbol]));
            }

            $data = [
                'currentPage' => $orders->currentPage(),
                'orders' => $orders,
                'mode' => 'list',
                'title'  => trans('admin.menu.codtosend'),
                'summaryAction' => route('admin.orders.to.cod', ['product' => $productSymbol]),
            ];
        } else {
            if (!$searchForm->isValid())
            {
                return redirect()->back()->withErrors($searchForm->getErrors())->withInput();
            }

            $query = $request->get('query');
            $orders = $this->orders->searchAfterCod($this->getProductId($productSymbol), $query);
            $data = [
                'currentPage' => 1,
                'orders'   => $orders,
                'mode'     => 'search',
                'query'    => $query,
            ];
        }

        $data['productSymbol'] = $productSymbol;
        $data['time'] = [
            'now'  => Carbon::now()->format('d/m/Y'),
            'past' => Carbon::now()->subWeek(1)->format('d/m/Y')
        ];
        $data['goToPageForm'] = $this->form(GoToPageForm::class);
        $data['title'] = trans('admin.menu.codsent');
        $data['summaryAction'] = route('admin.orders.after.cod');
        $data['searchForm'] = $searchForm;

        return view('admin.order.list_cod', $data);
    }

    public function getBankTransfer(Request $request)
    {
        $productSymbol = $request->get('product');
        $searchForm = $this->form(SearchForm::class, ['route'  => 'admin.orders.bank.transfer']);

        if (!$request->has('query')) {
            $user = $request->user();
            if (!$user->ability('admin', 'list-all-orders'))
            {
                $this->orders->pushCriteria(
                    new UserCriteria($user)
                );
            }

            $this->orders->pushCriteria(
                new OrderCriteria($request->input('f'))
            );

            $orders = $this->orders->bankTransfer(10, $this->getProductId($productSymbol));
            if($orders->lastPage()!=0 && $request->get('page') > $orders->lastPage()){
                return redirect(route('admin.orders.bank.transfer', ['product' => $productSymbol]));
            }
            $data = [
                'productSymbol' => $productSymbol,
                'orders' => $orders,
                'title'  => trans('admin.menu.banktransfer'),
                'goToPageForm' => $this->form(GoToPageForm::class),
                'mode' => 'list'
            ];
        } else {
            if (!$searchForm->isValid())
            {
                return redirect()->back()->withErrors($searchForm->getErrors())->withInput();
            }

            $query = $request->get('query');
            $orders = $this->orders->searchBankTransfer($this->getProductId($productSymbol), $query);
            $data = [
                'currentPage' => 1,
                'orders'   => $orders,
                'mode'     => 'search',
                'query'    => $query,
            ];
        }

        $data['searchForm'] = $searchForm;
        $data['productSymbol'] = $productSymbol;
        $data['title'] = trans('admin.menu.banktransfer');

        return view('admin.order.list_bank_transfer', $data);
    }

    public function getBankTransferAfterCod(Request $request)
    {
        $productSymbol = $request->get('product');
        $searchForm = $this->form(
            SearchForm::class, ['route'  => 'admin.orders.bank.transfer.after.cod']
        );

        if (!$request->has('query')) {
            $user = $request->user();
            if (!$user->ability('admin', 'list-all-orders'))
            {
                $this->orders->pushCriteria(
                    new UserCriteria($user)
                );
            }

            $this->orders->pushCriteria(
                new OrderCriteria($request->input('f'))
            );

            $orders = $this->orders->bankTransferAftercod(10, $this->getProductId($productSymbol));
            if($orders->lastPage()!=0 && $request->get('page') > $orders->lastPage()){
                return redirect(route('admin.orders.bank.transfer', ['product' => $productSymbol]));
            }
            $data = [
                'productSymbol' => $productSymbol,
                'orders' => $orders,
                'title'  => trans('admin.menu.banktransfer'),
                'goToPageForm' => $this->form(GoToPageForm::class),
                'mode' => 'list'
            ];
        } else {
            if (!$searchForm->isValid())
            {
                return redirect()->back()->withErrors($searchForm->getErrors())->withInput();
            }

            $query = $request->get('query');
            $orders = $this->orders->searchBankTransferAfterCod($this->getProductId($productSymbol), $query);
            $data = [
                'currentPage' => 1,
                'orders'   => $orders,
                'mode'     => 'search',
                'query'    => $query,
            ];
        }

        $data['searchForm'] = $searchForm;
        $data['productSymbol'] = $productSymbol;
        $data['title'] = trans('admin.menu.banktransfer');

        return view('admin.order.list_bank_transfer', $data);
    }

    public function getCodRestore($id)
    {
        try
        {
            $order = $this->orders->find($id);

            $order->archived = 0;
            $order->to_confirm = 1;
            $order->status = 5;
            $order->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);

        }
    }

    public function getShipmentUnsend($id)
    {
        if (Entrust::ability('admin', 'send-order'))
        {

            try
            {
                $order = $this->orders->find($id);

                $order->package_number = null;
                $order->shipped = 0;
                $order->status = 0;
                $order->save();

                return redirect()->back()->with('message', [
                    'type'    => 'success',
                    'message' => trans('translations.action_done')
                ]);

            } catch (\Exception $e)
            {
                return redirect()->back()->with('message', [
                    'type'    => 'danger',
                    'message' => $e->getMessage()
                ]);

            }
        }
    }

    /**
     * @param $id
     * @param ShippingProvider $shippingProvider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getShipmentSend($id, ShippingProvider $shippingProvider)
    {
        if (Entrust::ability('admin', 'send-order'))
        {

            try
            {
                $order = $this->orders->find($id);
                if ($order->shipped)
                {
                    throw new \Exception('To zamówienie zostało już wysłane');
                }
                Log::info('Trying to send order', ['id' => $order->id]);
                $response = $shippingProvider->sale($order);

                if($response['status']!=200 && $response['status']!=202){
                    return redirect()->back()->with('message', [
                        'type'    => 'danger',
                        'message' => $response['message']
                    ]);
                }

                if ($response instanceof Collection)
                {
                    Log::info('Writing errors');
                    Log::info('Send errors', $response->toArray());
                    if ($response->has('error') && isset($response->get('error')->message))
                    {
                        return redirect()->back()->with('message', [
                            'type'    => 'danger',
                            'message' => $response->get('error')->message
                        ]);
                    }
                }

                $order->package_number = isset($response['package_number']) ? $response['package_number'] : null;
                $order->shipped = 1;
                $order->status = 2;
                $order->save();

                return redirect()->back()->with('message', [
                    'type'    => 'success',
                    'message' => trans('translations.action_done')
                ]);

            } catch (\Exception $e)
            {
                return redirect()->back()->with('message', [
                    'type'    => 'danger',
                    'message' => $e->getMessage()
                ]);
            }
        }
        else
        {
            Log::info('Trying to send order without permissions', ['user' => Auth::user()->toArray()]);
        }
    }

    /**
     * @param Request $request
     * @param ShippingProvider $shippingProvider
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postShipmentSendMultiple(Request $request, ShippingProvider $shippingProvider)
    {
        $orders = $this->orders->byIds($request->get('orders'));

        if ($request->input('delete-button'))
        {
            if (Entrust::ability('admin', 'delete-order'))
            {
                foreach ($orders as $order)
                {
                    $this->orders->delete($order->id);
                }

            }
        }
        elseif ($request->input('to-confirm-button'))
        {
            foreach ($orders as $order)
            {
                $order->to_confirm = 1;
                $order->save();
            }
        }
        else
        {
            if (Entrust::ability('admin', 'send-order'))
            {

                foreach ($orders as $order)
                {
                    try
                    {
                        if ($order->shipped)
                        {
                            throw new \Exception('To zamówienie zostało już wysłane');
                        }

                        $response = $shippingProvider->sale($order);

                        if($response['status']!=200 && $response['status']!=202){
                            return redirect()->back()->with('message', [
                                'type'    => 'danger',
                                'message' => $response['message']
                            ]);
                        }

                        if ($response instanceof Collection)
                        {
                            Log::info('Writing errors');
                            Log::info('Send errors', $response->toArray());
                            if ($response->has('error') && isset($response->get('error')->message))
                            {
                                return redirect()->back()->with('message', [
                                    'type'    => 'danger',
                                    'message' => $response->get('error')->message
                                ]);
                            }
                        }

                        $order->package_number = isset($response['package_number']) ? $response['package_number'] : null;
                        $order->shipped = 1;
                        $order->status = 2;
                        $order->save();
                    } catch (\Exception $e)
                    {
                        Log::error($e->getMessage(), ['id' => $order->id]);
                    }
                }
            }

        }

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getShipmentConfirm($id)
    {
        try
        {
            $order = $this->orders->find($id);
            if ($order->shipped)
            {
                throw new \Exception($order->id.' is already sent.');
            }

            $order->to_confirm = 1;
            $order->status = 5;
            $order->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);

        }
    }

    public function getShipmentToNotSent($id)
    {
        try
        {
            $order = $this->orders->find($id);
            if ($order->shipped)
            {
                throw new \Exception($order->id.' is already sent.');
            }

            $order->to_confirm = 0;
            $order->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);

        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function getBankTransferConfirm($id)
    {
        try
        {
            $order = $this->orders->find($id);
            $order->method = 7;
            $order->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getShipmentMark($id)
    {
        try
        {
            $order = $this->orders->find($id);
            if ($order->shipped)
            {
                throw new \Exception('To zamówienie zostało już wysłane');
            }

            $order->shipped = 1;
            $order->status = 2;
            $order->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);

        }
    }

    public function getToList($id)
    {
        try
        {
            $order = $this->orders->find($id);

            $order->method = 2;
            $order->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);

        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getResetIp($id)
    {
        $order = $this->orders->find($id);
        $order->download_ip = null;
        $order->save();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }

    public function getDelivery($id)
    {
        $order = $this->orders->find($id);

        if($order->status!=7){

            $courierName = $order->address->subDistrict->postcode->shipping_provider_name;

            switch ($courierName) {
                case 'JAYON':
                    $shipper = app(JayonExpressProvider::class);
                    break;
                case 'NINJA':
                    $shipper = app(NinjaProvider::class);
                    break;
                default:
                    $shipper = null;
                    break;
            }

            if($shipper){
                $status = $shipper->status($order);
                if($status['status']=='D'){
                    $message = 'Order '.$order->ref.' status updated successfully to '.$status['statusText'];
                } else {
                    $message = 'Order '.$order->ref.' status not Delivered yet';
                }
            } else {
                $message = $courierName.' DOES NOT PROVIDE MANUALLY TRACKING YET';
            }

        } else {
            $message = 'Order '.$order->ref.' already Delivered';
        }

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => $message
        ]);
    }

    function postNinja(Request $request)
    {
        $ninjaId = json_decode($request->all())->response->id;

        $order = $this->orders->findWhere([['id_ninja', '=', $ninjaId]])->first();
        if($order)
        {
            $order->status = 7;
            $order->save();

            Log::notice('Received a Ninja Webhook: ',[
                'changed Order status to Delivered' => $order->id
            ]);
        }
    }

    public function getReport(Request $request)
    {
        if (Entrust::ability('admin', 'export-orders'))
        {
            $report = $request->get('report');
            $range = $request->get('range');

            $range = explode(' - ', $range);

            $dt1 = Carbon::createFromFormat('d/m/Y', $range[0])->startOfDay()->format('Y-m-d H:i:s');
            $dt2 = Carbon::createFromFormat('d/m/Y', $range[1])->endOfDay()->format('Y-m-d H:i:s');

            $invoices = $this->invoices->salesReport($dt1, $dt2);

            $rows = collect();

            foreach ($invoices as $invoice)
            {
                if($report=='Orders' && $invoice->invoice_no!=NULL){
                    $rows->push([
                        'Date'                       => $invoice->created_at,
                        'Invoice No.'                => $invoice->invoice_no,
                        'Order No.'                  => 'CM' . $invoice->order->id,
                        'Name'                       => $invoice->order->first_name.' '.$invoice->order->last_name,
                        'Phone'                      => $invoice->order->phone,
                        'Email'                      => $invoice->order->email,
                        'Payment'                    => $invoice->order->method_name,
                        'Amount'                     => $invoice->order->price,
                    ]);
                }
                if($report=='Credit Note' && $invoice->credit_no!=NULL){
                    $rows->push([
                        'Date'                       => $invoice->created_at,
                        'Credit No.'                 => $invoice->credit_no,
                        'Order No.'                  => 'CM' . $invoice->order->id,
                        'Name'                       => $invoice->order->first_name.' '.$invoice->order->last_name,
                        'Phone'                      => $invoice->order->phone,
                        'Email'                      => $invoice->order->email,
                        'Payment'                    => $invoice->order->method_name,
                        'Amount'                     => $invoice->order->price,
                    ]);
                }
            }

            ob_end_clean();
            ob_start();

            return Excel::create($report.'-'.Carbon::now()->format('Y-m-d'), function ($excel) use ($rows, $report)
            {
                $excel->sheet($report, function ($sheet) use ($rows)
                {
                    $sheet->fromArray($rows->toArray());

                });
            })->export('xls');
        }
    }

    public function postStore(Request $request)
    {
        try
        {
            $request->merge([
                'district_id' => $request->get('hiddenDistrict'),
                'city_id' => $request->get('hiddenCity'),
                'province_id' => $request->get('hiddenProvince')
            ]);

            $form = $this->form(OrderForm::class);

            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $request->merge([
                'payment' => 2,
                'nocod'   => 0,
                'version' => 2
            ]);

            $languages = collect();
            foreach ($request->get('languages') as $key => $value)
            {
                if (isset($value['id']))
                {
                    $languages->push([
                        'id'    => $value['id'],
                        'price' => $value['price'],
                        'qty'   => isset($value['qty']) ? (int)$value['qty'] : 1,
                        'symbol'=> $value['symbol']
                    ]);
                }
            }

            if ($languages->count() < 1)
            {
                dd('Choose at least one product');
            }

            $this->dispatch(
                new CreateOrder($request, null, false, $languages)
            );

            return redirect()->route('admin.orders', ['product' => $languages->last()['symbol']])->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage());
        }
    }

    /**
     * @param FormBuilder $formBuilder
     * @return mixed
     */
    public function getCreate(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(OrderForm::class, [
            'method' => 'POST',
            'url'    => route('orders.store'),
        ]);

        $postCodesList = $this->postCodes->all();
        $postCodesList = $postCodesList->filter(function ($value) {
            return $value->code_covered===false;
        });

        $data = [
            'form'      => $form,
            'languages' => $this->languages->all(),
            'mode'      => 'Create',
            'bank_transfer_areas' => $postCodesList->lists('id')
        ];

        return view('admin.order.create', $data);
    }

    public function getInvoices(Request $request)
    {
        $data = [
            'range' => Carbon::now()->subWeek(1)->format('d/m/Y').' - '.Carbon::now()->format('d/m/Y'),
            'productSymbol' => $request->get('product')
        ];

        return view('admin.order.invoices', $data);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getCustomerData(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $total = $this->orders->has('address')->with('address')->getFiltered($request)->all()->count();

        $data = [
            'orders' => $this->orders->has('address')->with('address')->getFiltered($request)->paginate(50),
            'total'  => $total,
        ];

        return view('admin.order.cdata', $data);
    }

    public function getCallsSave($id, $text)
    {
        $order = $this->orders->find($id);

        $order->update([
            'calls' => $text
        ]);
    }

    public function getEmailSave($id, $email)
    {
        $order = $this->orders->find($id);

        $order->update([
            'email' => $email
        ]);
    }
}
