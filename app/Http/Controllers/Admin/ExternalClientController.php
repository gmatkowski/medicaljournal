<?php

namespace App\Http\Controllers\Admin;

use App;
use Excel;
use Carbon\Carbon;
use App\Jobs\CreateZipFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Repositories\ContactRepository;
use App\Repositories\JobRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Http\Controllers\Controller;
use App\Repositories\LanguageRepository;
use Entrust;
use App\Forms\GoToPageForm;
use App\Forms\Admin\SearchForm;

//use anlutro\cURL\cURL;

/**
 * Class FocusClientController
 * @package App\Http\Controllers
 */
class ExternalClientController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    use App\Console\Commands\SendContactsTrait;

    /**
     * @var JobRepository
     */
    protected $contacts;

    protected $languageRepository;

    /**
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository, LanguageRepository $languageRepository)
    {
        $this->contacts = $contactRepository;
        $this->languageRepository = $languageRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
//        $contacts = '508589-1021018,510655-1023121,510778-1023368,513655-1028340,515069-1030674,515445-1031320,516312-1033126,520748-1041141,532598-1059495,534662-1063593,535644-1064630,536285-1066091,538003-1069443,538050-1069481,539408-1072722';
//        $contacts_ids = explode(',', $contacts);
//        foreach($contacts_ids as $contactFromAPI)
//        {
//            $contacts_idsArr[] = explode('-', $contactFromAPI)[0];
//        }
//        foreach($contacts_idsArr as $contact_id)
//        {
//            $contact = $this->contacts->find($contact_id);
//            if($contact)
//            {
//                $contact->postback = 0;
//                $contact->cc_reject = 0;
//                $contact->cc_sell = 1;
//                $contact->save();
//            }
//        }

//        $curl = new cURL();
//        $products = [
//            'cambogia', 'blackmask', 'hallupro'
//        ];
//        $cc = app('App\Call\Focus');
//        $rows = collect();
//        foreach($products as $product){
//            $contacts = $this->contacts->getByDateRange(Carbon::now()->startOfMonth(), Carbon::tomorrow(), $product, true);
//            foreach($contacts as $contact)
//            {
//                $rows->push([
//                    'cc_id' => $contact->cc_id,
//                    'phone' => $contact->phone,
//                    'client_name' => $contact->external_name,
//                    'source' => $cc->getSource($contact),
//                    'params' => $contact->params,
//                    'product' => $contact->order && $contact->order->isPriceDefault() ? $contact->product.' promo' : $contact->product,
//                    'created_at' => $contact->created_at
//                ]);
//
//                $request = $curl->newJsonRequest('post', 'http://affbay.asia/api/v1/leads/store', [
//                    'values.UNIQUE_ID' => '',
//                    'values.SOURCE' => 'affilator',
//                    'values.EXT_CLIENT_NAME' => 'no_ext_client_name',
//                    'values.PHONE' => '0'.$contact->phone,
//                    'values.PARAMS' => $contact->params
//                ])
//                    ->setHeader('Authorization', 'Tn7WS4rzA8WNVye9y2TgzJ25RT92W1aTpSucbMZfZ0yxeQ6cAD');
//                $request->send();
//            }
//        }
//        return Excel::create('medical', function ($excel) use ($rows)
//        {
//            $excel->sheet('medical', function ($sheet) use ($rows)
//            {
//                $sheet->fromArray($rows->toArray());
//
//            });
//        })->export('csv');

//            $cc = app('App\Call\Focus');
//            $campaigns = $cc->getCampaignList();
//            $campaigns = json_decode($campaigns->body);
//            $test = 'test';
//            $cc->getLeads();

//        $cc->setActiveCampaing('medical');
//        $contacts = $this->contacts->findWhere([['id', '=', 469293]])->all();
//        $this->sendContacts($cc, collect($contacts));
//        $test = 'test';

        if (Entrust::ability('admin', 'list-contacts'))
        {
            $productSymbol = $request->get('product');
            $page = $request->get('page', 1);

            Paginator::currentPageResolver(function () use ($page)
            {
                return $page;
            });

            if (Entrust::hasRole('css')){
                $contacts = collect();
            } else {
                $contacts = $this->contacts->searchPaginateExternals(50, $productSymbol);
            }

            switch ($productSymbol) {
                case 'cambogia':
                    $currency = $this->languageRepository->find(1)->currency;
                    break;
                case 'blackmask':
                    $currency = $this->languageRepository->find(2)->currency;
                    break;
                case 'hallupro':
                    $currency = $this->languageRepository->find(3)->currency;
                    break;
                default:
                    $currency = 'IDR';
            }

            if (!$request->has('query')) {
                if (Entrust::hasRole('css')){
                    $contacts = collect();
                } else {
                    $contacts = $this->contacts->searchPaginateExternals(50, $productSymbol);
                }

                $data = [
                    'objects' => $contacts,
                    'mode'    => 'list',
                    'time'    => [
                        'now'     => Carbon::now()->format('d/m/Y'),
                        'yesterday' => Carbon::yesterday()->format('d/m/Y')
                    ]
                ];

            } else {
                $query = $request->get('query');
                $data = [
                    'objects' => $this->contacts->search($query, $productSymbol),
                    'mode'    => 'search',
                    'query'   => $query,
                    'time'    => [
                        'now'     => Carbon::now()->format('d/m/Y'),
                        'yesterday' => Carbon::yesterday()->format('d/m/Y')
                    ]
                ];
            }

            $data['currency'] = $currency;
            $data['productSymbol'] = $productSymbol;
            $data['summaryRange'] = $request->has('summaryRange') ? $request->get('summaryRange') : Carbon::yesterday()->format('d/m/Y').' - '.Carbon::now()->format('d/m/Y');
            $data['counters'] = [
                'all' => $this->contacts->searchAllExternals($productSymbol)->count(),
                'specificDate' => $this->contacts->specificDate($productSymbol, $data['summaryRange'], true),
                'soldInSpecificDate' => $this->contacts->specificDate($productSymbol, $data['summaryRange'], true, true)
            ];
            $data['productSymbol'] = $request->get('product');
            $data['goToPageForm'] = $this->form(GoToPageForm::class);
            $data['searchForm'] = $this->form(SearchForm::class);

            return view('admin.contact.externaList', $data);
        }
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function postExport(Request $request)
    {
        if(Entrust::ability('admin', 'export-contacts')) {

            $languages = collect();

            $notSold = $request->get('notSold');
            $range = $request->get('range');

            if (!preg_match('/^[0-9]{2}\/[0-9]{2}\/[0-9]{4} - [0-9]{2}\/[0-9]{2}\/[0-9]{4}$/', $range)) {
                return redirect()->back();
            } else {

                if($notSold) {
                    $contacts = $this->contacts->searchNotSoldExternals($request->get('product'));
                } else {
                    $range = explode(' - ', $range);
                    $dt1 = Carbon::createFromFormat('d/m/Y', $range[0])->startOfDay();
                    $dt2 = Carbon::createFromFormat('d/m/Y', $range[1])->startOfDay();

                    $contacts = $this->contacts->getByDateRange($dt1, $dt2, $request->get('product'), true);
                }

                foreach ($contacts as $language) {
                    if($language->external_name){
                        $language->setVisible([
                            'id', 'cc_id', 'pub_id', 'click_id', 'name', 'phone', 'call_center', 'cc_sell', 'cc_reject', 'cc_price', 'external_name', 'created_at'
                        ]);

                        $languages->push($language);
                    }
                }

                $chunks = $languages->chunk(2000);

                ob_end_clean();
                ob_start();

                $files = collect();

                foreach ($chunks as $key => $chunk) {
                    $info = Excel::create('ContactList-' . Carbon::now()->format('Y-m-d') . '-part-' . ($key + 1), function ($excel) use ($chunk) {
                        $excel->sheet('contacts', function ($sheet) use ($chunk) {
                            $sheet->fromArray($chunk->toArray());
                        });
                    })->store('xls', false, true);

                    $files->push($info['full']);
                    unset($info);
                }

                ob_end_flush();

                $filename = 'ContactList-' . Carbon::now()->format('Y-m-d') . '-all.zip';
                $file_path = storage_path('exports/' . $filename);

                $response = $this->dispatch(
                    new CreateZipFile($files->toArray(), $file_path)
                );

                if($response){
                    foreach ($files as $file) {
                        File::delete($file);
                    }

                    return response()->download($file_path);
                } else {
                    return back();
                }
            }
        }
    }
}
