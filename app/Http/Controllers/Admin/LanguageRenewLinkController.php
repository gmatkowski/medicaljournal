<?php

namespace App\Http\Controllers\Admin;

use App\Repositories\LanguageRenewLinkRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

/**
 * Class LanguageRenewLinkController
 * @package App\Http\Controllers\Admin
 */
class LanguageRenewLinkController extends Controller {

    protected $links;

    public function __construct(LanguageRenewLinkRepository $languageRenewLinkRepository)
    {
        $this->links = $languageRenewLinkRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex()
    {
        $data = [
            'links' => $this->links->listing(50)
        ];

        return view('admin.language.links', $data);
    }

    /**
     *
     */
    public function getCreate()
    {

        DB::transaction(function ()
        {
            foreach (range(0, 50) as $row)
            {
                $this->links->create([
                    'token' => str_random(16)
                ]);
            }
        });

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);

    }
}
