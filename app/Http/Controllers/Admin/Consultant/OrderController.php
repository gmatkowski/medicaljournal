<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Exceptions\AdminException;
use App\Jobs\CreateOrder;
use App\Repositories\Criteria\UserCriteria;
use App\Repositories\LanguageRepository;
use App\Repositories\OrderRepository;
use App\Shipping\ShippingProvider;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilder;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Forms\Admin\Consultant\OrderForm;

/**
 * Class orderController
 * @package App\Http\Controllers\Admin
 */
class OrderController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var OrderRepository
     */
    protected $orders;
    /**
     * @var LanguageRepository
     */
    protected $languages;


    /**
     * @param OrderRepository $orderRepository
     * @param LanguageRepository $languageRepository
     */
    public function __construct(OrderRepository $orderRepository, LanguageRepository $languageRepository)
    {
        $this->orders = $orderRepository;
        $this->languages = $languageRepository;
    }


    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $this->orders->pushCriteria(
            new UserCriteria($request->user())
        );

        $data = [
            'orders' => $this->orders->with(['languages'])->newest(10)
        ];

        return view('admin.consultant.order.list', $data);
    }

    /**
     * @param FormBuilder $formBuilder
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate(FormBuilder $formBuilder)
    {
        $form = $formBuilder->create(OrderForm::class, [
            'method' => 'POST',
            'url'    => route('consultant.orders.store'),
        ]);

        $data = [
            'form'      => $form,
            'languages' => $this->languages->all(),
            'mode'      => 'Create'
        ];

        return view('admin.consultant.order.form', $data);
    }

    /**
     * @param Request $request
     * @param ShippingProvider $shippingProvider
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request, ShippingProvider $shippingProvider)
    {
        try
        {
            $form = $this->form(OrderForm::class);

            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $request->merge([
                'payment' => 2,
                'nocod'   => 0,
                'version' => 2
            ]);

            $languages = collect();
            foreach ($request->get('languages') as $key => $value)
            {
                if (isset($value['id']))
                {
                    $languages->push([
                        'id'    => $value['id'],
                        'price' => $value['price']
                    ]);
                }
            }

            if ($languages->count() == 0)
            {
                throw new \Exception('Choose at least one product');
            }

            $order = $this->dispatch(
                new CreateOrder($request, null, false, $languages)
            );

            $order->user()->associate($request->user());
            $order->save();

            /*$shippingProvider->sale($order);

            $order->shipped = 1;
            $order->save();*/


            return redirect()->route('consultant.orders')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage());
        }

    }


    /**
     * @param Request $request
     * @return string
     */
    public function postEditable(Request $request)
    {
        $order = $this->orders->find($request->get('pk'));
        $order->{$request->input('name')} = $request->input('value');
        $order->save();

        return 'ok';
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getDetails($id)
    {
        $order = $this->orders->find($id);

        $data = [
            'order' => $order,
        ];

        return view('admin.consultant.order.details', $data);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getDelete(Request $request, $id)
    {
        try
        {
            $order = $this->orders->find($id);

            if ($order->user_id != $request->user()->id)
            {
                throw new \Exception('You can\'t delete this order');
            }

            $order->delete();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);
        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'danger',
                'message' => $e->getMessage()
            ]);
        }
    }

}
