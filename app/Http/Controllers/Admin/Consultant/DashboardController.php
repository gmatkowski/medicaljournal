<?php

namespace App\Http\Controllers\Admin\Consultant;

use App\Repositories\OrderRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class DashboardController extends Controller {

    protected $orders;
    protected $users;

    public function __construct(OrderRepository $orderRepository, UserRepository $userRepository)
    {
        $this->orders = $orderRepository;
        $this->users = $userRepository;
    }

    public function index(Request $request)
    {
        $data = [

        ];

        return view('admin.consultant.dashboard',$data);
    }
}
