<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\LanguageForm;
use App\Jobs\SavePackage;
use App\Repositories\LanguagePackageRepository;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;

class LanguagePackageController extends Controller
{
    use FormBuilderTrait;

    public function getIndex(LanguagePackageRepository $languagePackageRepository)
    {
        $data = [
            'packages' => $languagePackageRepository->all()->keyBy('in_pack')
        ];

        return view('admin.language.packages', $data);
    }

    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postSave(Request $request)
    {
        try
        {
            $form = $this->form(LanguageForm::class);
            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $this->dispatch(
                new SavePackage($request)
            );

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }
    }
}
