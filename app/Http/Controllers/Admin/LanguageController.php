<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\LanguageForm;
use App\Jobs\Admin\CreateLanguage;
use App\Jobs\Admin\SaveLanguage;
use App\Jobs\SavePackage;
use App\Repositories\LanguagePackageRepository;
use App\Repositories\LanguageRenewLinkRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\StockRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Config;
use Auth;

/**
 * Class PageController
 * @package App\Http\Controllers\Admin
 */
class LanguageController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var LanguageRepository
     */
    protected $languages;

    protected $stocks;

    /**
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageRepository $languageRepository, StockRepository $stockRepository)
    {
        $this->languages = $languageRepository;
        $this->stocks = $stockRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $data = [
            'objects' => $this->languages->newest(50),
        ];

        return view('admin.language.list', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        $object = $this->languages->find($id);

        $form = $this->form(LanguageForm::class, [
            'method' => 'POST',
            'url'    => route('admin.languages.update'),
            'model'  => $object
        ]);

        $data = [
            'object' => $object,
            'form'   => $form,
            'mode'   => trans('admin.common.edit')
        ];

        return view('admin.language.form', $data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request)
    {
        try
        {
            $form = $this->form(LanguageForm::class);

            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = $this->languages->find($request->get('id'));
            $this->dispatch(
                new SaveLanguage($object, $request, $this->stocks)
            );

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate()
    {
        $form = $this->form(LanguageForm::class, [
            'method' => 'POST',
            'url'    => route('admin.languages.store'),
        ]);

        $data = [
            'form' => $form,
            'mode' => trans('admin.common.add')
        ];

        return view('admin.language.form', $data);
    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request)
    {
        try
        {
            $form = $this->form(LanguageForm::class);
            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $this->dispatch(
                new CreateLanguage($request)
            );

            return redirect()->route('admin.languages')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        $object = $this->languages->find($id);
        $object->delete();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }
}
