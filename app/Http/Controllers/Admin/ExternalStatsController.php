<?php

namespace App\Http\Controllers\Admin;

use App;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Repositories\ContactRepository;
use App\Repositories\JobRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use App\Http\Controllers\Controller;
use App\Repositories\LanguageRepository;
use App\Repositories\ExternalClientRepository;
use Entrust;
use App\Forms\GoToPageForm;

/**
 * Class FocusClientController
 * @package App\Http\Controllers
 */
class ExternalStatsController extends Controller {

    use FormBuilderTrait, DispatchesJobs;

    /**
     * @var JobRepository
     */
    protected $contacts;

    protected $languageRepository;

    protected $externals;

    /**
     * @param ContactRepository $contactRepository
     */
    public function __construct(ContactRepository $contactRepository, LanguageRepository $languageRepository, ExternalClientRepository $externals)
    {
        $this->contacts = $contactRepository;
        $this->languageRepository = $languageRepository;
        $this->externals = $externals;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        if (Entrust::ability('admin', 'list-contacts'))
        {
            $client = $request->get('client');
            $productSymbol = $request->get('product');
            $range = $request->get('range', Carbon::now()->subWeek(1)->format('d/m/Y').' - '.Carbon::now()->format('d/m/Y'));
            $status = $request->get('status', 'all');
            $page = $request->get('page', 1);

            Paginator::currentPageResolver(function () use ($page)
            {
                return $page;
            });

            if (Entrust::hasRole('css')){
                $contacts = collect();
            } else {
                if($request->has('range')){
                    $contacts = $this->contacts->searchExternalsWithDateAndStatus($productSymbol, $range, $status, $client)->paginate(15);
                } else {
                    $contacts = collect();
                }
            }

            switch ($productSymbol) {
                case 'cambogia':
                    $currency = $this->languageRepository->find(1)->currency;
                    break;
                case 'blackmask':
                    $currency = $this->languageRepository->find(2)->currency;
                    break;
                case 'hallupro':
                    $currency = $this->languageRepository->find(3)->currency;
                    break;
                default:
                    $currency = 'IDR';
            }

            $data = [
                'client' => $client,
                'externals' => $this->externals->all(),
                'objects' => $contacts,
                'soldCounter' => $this->contacts->searchSoldExternals($productSymbol)->count(),
                'onHoldCounter' => $this->contacts->searchOnHoldExternals($productSymbol)->count(),
                'onHoldTooOldCounter' => $this->contacts->searchOnHoldExternals($productSymbol, true)->count(),
                'closedCounter' => $this->contacts->searchRejectExternals($productSymbol)->count(),
//                'duplicatedCounter' => $this->contacts->searchDuplicateExternals($productSymbol)->count(),
                'mode'    => 'list',
                'productSymbol'    => $productSymbol,
                'status' => $status,
                'range' => $range,
                'currency' => $currency
            ];

            $data['counters'] = [
                'all' => $request->has('range') ? $this->contacts->searchExternalsWithDateAndStatus($productSymbol, $range, $status, $client)->all()->count() : 0
            ];
            $data['goToPageForm'] = $this->form(GoToPageForm::class);
            $data['productSymbol'] = $productSymbol;

            return view('admin.contact.externalStats', $data);
        }
    }
}
