<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\UserForm;
use App\Repositories\Criteria\ConsultantCriteria;
use App\Repositories\RoleRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Auth;
use Entrust;

/**
 * Class UserController
 * @package App\Http\Controllers\Admin
 */
class UserController extends Controller
{

    use FormBuilderTrait;

    /**
     * @var
     */
    protected $users;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->users = $userRepository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page) {
            return $page;
        });

        if (Entrust::hasRole('ccadmin')){
            $this->users->pushCriteria(app(ConsultantCriteria::class));
        }

        $data = [
            'users' => $this->users->with(['roles'])->newest(50)
        ];

        return view('admin.user.list', $data);
    }

    /**
     * @return \Illuminate\View\View
     */
    public function getCreate(RoleRepository $roles)
    {
        $form = $this->form(UserForm::class, [
            'method' => 'POST',
            'url'    => route('admin.users.store'),
        ]);
        
        if(Entrust::hasRole('ccadmin')){
            $consultant = $roles->findByField('name', 'consultant')->first();
            $css = $roles->findByField('name', 'css')->first();
            $form->remove('role');
            $form->add('role', 'select',
                ['rules' => 'required',
                 'label' => trans('translations.role'),
                 'choices' => [
                     $consultant->id => $consultant->display_name,
                     $css->id => $css->display_name
                 ]
            ]);
        }

        if(Entrust::hasRole('headcc')){
            $consultant = $roles->findByField('name', 'consultant')->first();
            $css = $roles->findByField('name', 'css')->first();
            $form->remove('role');
            $form->add('role', 'select',
                ['rules' => 'required',
                    'label' => trans('translations.role'),
                    'choices' => [
                        $consultant->id => $consultant->display_name,
                        $css->id => $css->display_name
                    ]
                ]);
        }

        $data = [
            'form' => $form,
            'mode' => trans('admin.common.add')
        ];

        return view('admin.user.form', $data);
    }


    /**
     * @param Request $request
     * @param RoleRepository $roleRepository
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request, RoleRepository $roleRepository)
    {
        try {
            $form = $this->form(UserForm::class);
            if (!$form->isValid()) {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $role = $roleRepository->find($request->input('role'));

            if (Entrust::hasRole('ccadmin')) {
                if ($role->name != 'consultant' && $role->name != 'css'){
                    return redirect()->back();
                }
            }

            $user = $this->users->create([
                'name'      => $request->get('name'),
                'email'     => $request->get('email'),
                'signature' => $request->get('signature'),
                'password'  => bcrypt($request->get('password')),
            ]);

            $user->attachRole($role);
            $user->save();

            return redirect()->route('admin.users')->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e) {
            throw new AdminException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id, RoleRepository $roles)
    {
        $user = $this->users->find($id);
        $user->role = $user->roles->first();
        $user->setHidden(['password']);

        if (Entrust::hasRole('ccadmin')) {
            if ($user->role->name != 'consultant' && $user->role->name != 'css'){
                return redirect()->back();
            }
        }

        $form = $this->form(UserForm::class, [
            'method' => 'POST',
            'url'    => route('admin.users.update'),
            'model'  => $user->toArray()
        ])
            ->modify('password', 'password', ['rules' => ''], true)
            ->modify('password_confirmation', 'password', ['rules' => ''], true);

        if (Entrust::hasRole('ccadmin')){
            $consultant = $roles->findByField('name', 'consultant')->first();
            $form->remove('role');
            $form->add('role', 'select',
                ['rules' => 'required',
                 'label' => trans('translations.role'),
                 'choices' => [
                     $consultant->id => $consultant->display_name
                 ]
                ]);
        }

        $data = [
            'user' => $user,
            'form' => $form,
            'mode' => trans('admin.common.edit')
        ];

        return view('admin.user.form', $data);
    }


    /**
     * @param Request $request
     * @param RoleRepository $roleRepository
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request, RoleRepository $roleRepository)
    {
        try {
            $user = $this->users->find($request->get('id'));
            $form = $this->form(UserForm::class);

            $rules = [
                'name'                  => 'required|min:3',
                'email'                 => 'required|unique:users,email,' . $user->id,
                'password'              => 'confirmed|min:5',
                'password_confirmation' => 'min:5',
                'role'                  => 'required'
            ];

            $form->validate($rules);

            $role = $roleRepository->find($request->input('role'));

            if (Entrust::hasRole('ccadmin')) {
                if ($role->name != 'consultant'){
                    return redirect()->back();
                }
            }

            if (!$form->isValid()) {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $user->update([
                'name'      => $request->get('name'),
                'signature' => $request->get('signature'),
                'email'     => $request->get('email'),
                'show_stock' => $request->get('show_stock'),
            ]);

            $user->detachRoles($user->roles);
            $user->attachRole($role);


            if ($request->has('password') & $request->has('password_confirmation')) {
                if ($request->get('password') != $request->get('password_confirmation')) {
                    throw new \Exception(trans('exceptions.not_the_same_password'));
                }
                $user->password = bcrypt($request->input('password'));
            }

            $user->save();

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e) {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function getDelete($id)
    {
        $user = $this->users->find($id);
        if ($user->id == Auth::id()) {
            throw new AdminException(trans('translations.user_self_deleting'));
        }

        $role = $user->roles->first();

        if (Entrust::hasRole('ccadmin')) {
            if ($role->name != 'consultant'){
                return redirect()->back();
            }
        }

        $user->delete();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);

    }
}
