<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\OrderRepository;
use Illuminate\Http\Response;
use Khill\Lavacharts\Lavacharts;

class HomeController extends Controller {

    private $orders;

    public function __construct(OrderRepository $orders)
    {
        $this->orders = $orders;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
//        $lava = new Lavacharts();
//        $reasons = $lava->DataTable();
//
//        $reasons
//            ->addStringColumn('Reasons')
//            ->addNumberColumn('Count');
//
//        $productNames = [
//            'Garcinia-cambogia' => 1,
//            'Black-mask' => 2
////            'Dr. Hallux' => 3
//        ];
//
//        foreach($productNames as $productName => $productId){
//            $leadSold = $this->orders->getConsultantsCounterFromMarchUntilNow($productId);
//            $allLeads = $this->orders->getCustomersCounterFromMarchUntilNow($productId);
//
//            $reasons
//                ->addRow(array($productName.' sold (put by consultants)', $leadSold > 0 ? $leadSold : 0))
//                ->addRow(array($productName.' all (put by customers)', $allLeads > 0 ? $allLeads : 0));
//        }
//
//        $lava->ColumnChart('Garcinia', $reasons, [
//            'title' => 'Leads starting from 1 March until now.'
//        ]);

        return view('admin.dashboard', [
//            'consultantsOrders' => $this->orders->getConsultantsCounterFromToday(),
//            'lava' => $lava
        ]);
    }
}
