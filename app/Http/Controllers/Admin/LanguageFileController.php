<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\AdminException;
use App\Forms\Admin\LanguageFileForm;
use App\Jobs\Admin\CreateLanguageFile;
use App\Jobs\Admin\SaveLanguageFile;
use App\Repositories\LanguageFileRepository;
use App\Repositories\LanguageRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Config;
use Auth;

/**
 * Class PageController
 * @package App\Http\Controllers\Admin
 */
class LanguageFileController extends Controller {

    use FormBuilderTrait, DispatchesJobs;


    /**
     * @var LanguageFileRepository
     */
    protected $files;

    /**
     * @var LanguageRepository
     */
    protected $languages;

    /**
     * @param LanguageFileRepository $languageFileRepository
     * @param LanguageRepository $languageRepository
     */
    public function __construct(LanguageFileRepository $languageFileRepository, LanguageRepository $languageRepository)
    {
        $this->files = $languageFileRepository;
        $this->languages = $languageRepository;
    }


    /**
     * @param Request $request
     * @param $language_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getList(Request $request, $language_id)
    {
        $language = $this->languages->find($language_id);

        $page = $request->get('page', 1);

        Paginator::currentPageResolver(function () use ($page)
        {
            return $page;
        });

        $data = [
            'language' => $language,
            'objects'  => $language->files()->paginate(50)
        ];

        return view('admin.language.file.list', $data);
    }

    /**
     * @param $id
     * @return \Illuminate\View\View
     */
    public function getEdit($id)
    {
        $object = $this->files->find($id);

        $form = $this->form(LanguageFileForm::class, [
            'method' => 'POST',
            'url'    => route('admin.language.files.update'),
            'model'  => $object
        ]);

        $data = [
            'object' => $object,
            'form'   => $form,
            'mode'   => trans('admin.common.edit')
        ];

        return view('admin.language.file.form', $data);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postUpdate(Request $request)
    {
        try
        {
            $form = $this->form(LanguageFileForm::class);

            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $object = $this->files->find($request->get('id'));
            $this->dispatch(
                new SaveLanguageFile($object, $request)
            );

            return redirect()->back()->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }


    /**
     * @param $language_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getCreate($language_id)
    {
        $language = $this->languages->find($language_id);

        $model = new \stdClass();
        $model->language_id = $language->id;

        $form = $this->form(LanguageFileForm::class, [
            'method' => 'POST',
            'url'    => route('admin.language.files.store'),
            'model'  => $model
        ]);

        $data = [
            'form' => $form,
            'mode' => trans('admin.common.add')
        ];

        return view('admin.language.file.form', $data);
    }


    /**
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse
     * @throws AdminException
     */
    public function postStore(Request $request)
    {
        try
        {
            $form = $this->form(LanguageFileForm::class);
            if (!$form->isValid())
            {
                return redirect()->back()->withErrors($form->getErrors())->withInput();
            }

            $language = $this->languages->find($request->input('language_id'));

            $this->dispatch(
                new CreateLanguageFile($request, $language)
            );

            return redirect()->route('admin.language.files', ['language_id' => $language->id])->with('message', [
                'type'    => 'success',
                'message' => trans('translations.action_done')
            ]);

        } catch (\Exception $e)
        {
            throw new AdminException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function getDelete($id)
    {
        $object = $this->files->find($id);
        $object->delete();

        return redirect()->back()->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }
}
