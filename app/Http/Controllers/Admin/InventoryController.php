<?php

namespace App\Http\Controllers\Admin;

use App\Forms\Admin\StockForm;
use App\Helpers\StrHelper;
use App\Repositories\LanguageRepository;
use App\Repositories\StockRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kris\LaravelFormBuilder\FormBuilderTrait;

/**
 * Class InventoryController
 * @package App\Http\Controllers\Admin
 */
class InventoryController extends Controller {

    use FormBuilderTrait;

    private $stocks;
    private $languages;

    public function __construct(StockRepository $stockRepository, LanguageRepository $languageRepository)
    {
        $this->stocks = $stockRepository;
        $this->languages = $languageRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getIndex(Request $request)
    {
        $productSymbol = $request->get('product');
        $range = $request->get('range', Carbon::now()->subWeek(1)->format('d/m/Y').' - '.Carbon::now()->format('d/m/Y'));

        $stocks = $this->stocks->searchPaginateStocksWithDate(15, $productSymbol, $range);

        $form = $form = $this->form(StockForm::class, [
            'method' => 'POST',
            'route'  => 'admin.inventory.update'
        ]);

        return view('admin.inventory.index', [
            'productSymbol' => $productSymbol,
            'range' => $range,
            'stocks' => $stocks,
            'form' => $form
        ]);
    }

    public function postUpdate(Request $request)
    {
        $id = $request->get('id');

        $stock = $this->stocks->find($id);
        $in = $request->get('in');
        $out = $request->get('out');

        $productSymbol = $request->get('productSymbol');
        $language = $this->languages->findWhere([['symbol', '=', $productSymbol]])->first();
        $nextStocks = $this->stocks->searchNextStocks($productSymbol, $stock->created_at);
        foreach($nextStocks as $key => $stock)
        {
            if($key==0)
            {
                $data = [
                    'open' => StrHelper::mustBeMoreThanZero($stock->open),
                    'in' => (int)$in,
                    'out' => (int)$out,
                    'close' => StrHelper::mustBeMoreThanZero($stock->close ? $stock->open+($in-$out) : null)
                ];
            }
            else
            {
                $data = [
                    'open' => StrHelper::mustBeMoreThanZero($nextStocks[$key-1]->close),
                    'close' => StrHelper::mustBeMoreThanZero($stock->close ? $nextStocks[$key-1]->close+($stock->in-$stock->out) : null)
                ];
            }
            $stock->update($data);
            if($nextStocks[count($nextStocks) - 1]==$stock){
                $language->amount = StrHelper::mustBeMoreThanZero($stock->open);
                $language->save();
                $stock->close = null;
                $stock->save();
            }
        }

        return redirect()->route('admin.inventory', ['product' => $productSymbol])->with('message', [
            'type'    => 'success',
            'message' => trans('translations.action_done')
        ]);
    }

}
