<?php

namespace App\Http\Controllers;

use App\Forms\BuyForm;
use App\Forms\NewsletterForm;
use App\Helpers\StrHelper;
use App\Repositories\DistrictRepository;
use App\Repositories\OrderAddressRepository;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;
//use App\Forms\BuyAddressForm;
use App\Jobs\CollectContactData;
use App\Repositories\LanguageRepository;
use Illuminate\Support\Facades\Session;
use App\Repositories\ContactRepository;
use Kris\LaravelFormBuilder\FormBuilderTrait;

/**
 * Class WelcomeController
 * @package App\Http\Controllers
 */
class WelcomeController extends Controller {

    use FormBuilderTrait;

    protected $session;

    /**
     * @var LanguageRepository
     */
    protected $languages;

    /**
     * @var LanguageRepository
     */
    protected $languageRepository;

    protected $orderRepo;

    protected $contactRepo;

    protected $orderAdressRepo;

    protected $districtRepo;

    protected $curl;

    /**
     * @param LanguageRepository $languageRepository
     */
    public function __construct(
        LanguageRepository $languageRepository,
        OrderRepository $orderRepository,
        ContactRepository $contactRepository,
        OrderAddressRepository $orderAddressRepository,
        DistrictRepository $districtRepository
    )
    {
        $this->languageRepository = $languageRepository;
        $this->orderRepo = $orderRepository;
        $this->contactRepo = $contactRepository;
        $this->orderAdressRepo = $orderAddressRepository;
        $this->districtRepo = $districtRepository;
    }

    public function index(Request $request)
    {
        $root = $request->root();

        switch ($root) {
            case 'http://latihan-fungsional.com':
                $redirect = redirect('http://blog.latihan-fungsional.com');
                break;
            default:
                $redirect = redirect('http://health.medical-jurnal.com');
        }

        return $redirect;
    }

//    public function detik()
//    {
//        return view('page.detik');
//    }

//    public function cambogiare($letter)
//    {
//        switch ($letter)
//        {
//            case 'h':
//                $medium = 'Health';
//                break;
//            case 'l':
//                $medium = 'Lifestyle';
//                break;
//            case 'b':
//                $medium = 'Biznes';
//                break;
//            case 'n':
//                $medium = 'News';
//                break;
//            case 's':
//                $medium = 'Showbiz';
//                break;
//        }
//
//        return view('page.cambogiare', compact('medium'));
//    }

//    public function cambogia(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/a83d8144-8aae-44dd-ae34-97d4fd820c95');
//        }
//        Session::has('ga-test') ? Session::remove('ga-test') : null;
//        return view('page.cambogia');
//    }

//    public function cambogia_main()
//    {
//        return view('page.cambogia_main');
//    }

//    public function cambogiaTest(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/a83d8144-8aae-44dd-ae34-97d4fd820c95');
//        }
//        Session::set('ga-test', 'UA-83521033-3');
//        return view('page.cambogia');
//    }

//    public function perkelahian()
//    {
//        return view('page.perkelahian');
//    }

//    public function penirum(Request $request)
//    {
//        Session::has('ga-test') ? Session::remove('ga-test') : null;
//        if(strpos($request->getUri(), 'voluum') !== false){
//            $landingPage = 'http://331nq.voluumtrk.com/click/1';
//        } else {
//            return redirect('http://331nq.voluumtrk.com/4adde6f1-6669-4119-bf29-c8282f2c771c');
//        }
//        return view('page.penirum', ['landingPage' => $landingPage]);
//    }
//
//    public function penirumTest()
//    {
//        Session::has('ga-test') ? Session::remove('ga-test') : null;
//        return view('page.penirumTest');
//    }
//
//    public function adcash()
//    {
//        return view('page.adcash');
//    }

//    public function preorder(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/baa895e3-0ae4-469d-82bc-641b6e2a481e');
//        }
//
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click/2' : route('order.confirm')
//        ]);
////            ->compose(BuyAddressForm::class);
//
//        Session::has('ga-test') ? Session::remove('ga-test') : null;
//
//        return view('page.preorder', [
//            'form' => $form
//        ]);
//    }

//    public function garcinia1(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/53f64f69-5279-4642-8f1d-2c1882383cd7');
//        }
//
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click/2' : route('order.confirm')
//        ]);
//
//        return view('page.garcinia1', [
//            'form' => $form
//        ]);
//    }

//    public function garcinia1_main(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/53f64f69-5279-4642-8f1d-2c1882383cd7');
//        }
//
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click/1' : route('order.confirm')
//        ]);
//
//        return view('page.garcinia1', [
//            'form' => $form
//        ]);
//    }

//    public function garciniaCambogia2(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/35fbcfd7-a848-4094-9129-5874fe4c081c');
//        }
//
//        return view('page.garciniaCambogia2', [
//            'voluum' => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click/2' : route('order.confirm')
//        ]);
//    }

//    public function garciniaCambogia2pop()
//    {
//        return view('page.garciniaCambogia2pop', [
//            'voluum' => route('order.confirm')
//        ]);
//    }
//
//    public function garciniaCambogia2_main(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/35fbcfd7-a848-4094-9129-5874fe4c081c');
//        }
//
//        return view('page.garciniaCambogia2_main', [
//            'voluum' => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click' : route('order.confirm')
//        ]);
//    }

//    public function garciniaCambogia2_mainpop()
//    {
//        return view('page.garciniaCambogia2_mainpop', [
//            'voluum' => route('order.confirm')
//        ]);
//    }
//
//    public function garciniaCambogia2popup()
//    {
//        return view('page.garciniaCambogia2popup');
//    }
//
//    public function garciniaCambogia2_main1()
//    {
//         return view('page.garciniaCambogia2_main1', [
//            'order' => route('order.confirm')
//        ]);
//    }
//
//    public function garciniaCambogia3()
//    {
//        return view('page.garciniaCambogia3');
//    }

//    public function preorderTest(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false) {
//            return redirect('http://331nq.voluumtrk.com/baa895e3-0ae4-469d-82bc-641b6e2a481e');
//        }
//
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click' : route('order.confirm')
//        ]);
//
//        Session::has('ga-test') ? null : Session::set('ga-test', 'UA-85228073-1');
//        return view('page.preorder', [
//            'form' => $form
//        ]);
//    }

//    public function preorderMain()
//    {
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => route('order.confirm')
//        ]);
//
//        Session::has('ga-test') ? null : Session::set('ga-test', 'UA-85228073-1');
//        return view('page.preorder_main', [
//            'form' => $form
//        ]);
//    }

//    public function bukuhariansaya(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false) {
//            return redirect('http://331nq.voluumtrk.com/06317a24-b58d-4143-ac19-fecd7876b5e3');
//        }
//
//        return view('page.bukuhariansaya', ['voluum' => 'http://331nq.voluumtrk.com/click/1']);
//    }

//    public function garcinia3(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false) {
//            return redirect('http://331nq.voluumtrk.com/52244063-e048-47d3-b812-e2068e08e32d');
//        }
//
//        return view('page.garcinia3', ['voluum' => 'http://331nq.voluumtrk.com/click/1']);
//    }

//    public function garcinia3_main()
//    {
//        return view('page.garcinia3_main', ['landing' => 'http://promos-seru.com/garcinia-cambogia2_main1']);
//    }

//    public function intro()
//    {
//        return view('page.intro');
//    }

//    public function andaingin()
//    {
//        $form = $this->form(NewsletterForm::class, [
//            'method' => 'POST',
//            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click' : route('order.newsletter')
//        ]);
//
//        return view('page.andaingin', [
//            'form' => $form
//        ]);
//    }
//
//    public function tumbuh()
//    {
//        return view('page.tumbuh');
//    }

//    public function amerika()
//    {
//        return view('page.amerika', [
//            'voluum' => 'http://331nq.voluumtrk.com/click/1'
//        ]);
//    }
//
//    public function amerika_lite()
//    {
//        $form = $this->form(NewsletterForm::class, [
//            'method' => 'POST',
//            'url'    => route('order.newsletter')
//        ]);
//
//        return view('page.amerika_lite', [
//            'form' => $form
//        ]);
//    }

//    public function dietAlaSelebriti()
//    {
//        return view('page.dietAlaSelebriti', ['voluum' => 'http://331nq.voluumtrk.com/click/1']);
//    }

    public function garcinia2(Request $request)
    {
        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false) {
            return redirect('http://331nq.voluumtrk.com/7e34e4b5-1ce5-44fe-a0fd-9b54a8e51d2e');
        }

        $form = $this->form(BuyForm::class, [
            'method' => 'POST',
            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click/2' : route('order.confirm')
        ]);

        return view('page.garcinia2', ['form' => $form]);
    }

    public function garcinia2_main(Request $request)
    {
        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false) {
            return redirect('http://331nq.voluumtrk.com/7e34e4b5-1ce5-44fe-a0fd-9b54a8e51d2e');
        }

        $form = $this->form(BuyForm::class, [
            'method' => 'POST',
            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click' : route('order.confirm')
        ]);

        return view('page.garcinia2', ['form' => $form]);
    }

    public function garcinia_forte()
    {
        $form = $this->form(BuyForm::class, [
            'method' => 'POST',
            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click' : route('order.confirm')
        ]);

        return view('page.garcinia_forte', ['form' => $form]);
    }

    public function introcomposition()
    {
        return view('page.introcomposition');
    }

    public function introfaq()
    {
        return view('page.introfaq');
    }

    public function introterms()
    {
        return view('page.introterms');
    }

    public function introcookies()
    {
        return view('page.introcookies');
    }

    public function halluterms()
    {
        return view('page.halluterms');
    }

    public function hallucookies()
    {
        return view('page.hallucookies');
    }

    public function intrometamorphosis()
    {
        return view('page.intrometamorphosis');
    }

//    public function prehallupro(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/38041732-1975-4004-8d80-b32b1a06230d');
//        }
//        return view('hallupro.prehallupro', ['volum' => 'http://331nq.voluumtrk.com/click/1']);
//    }

//    public function prehallupro_main()
//    {
//        return view('hallupro.prehallupro_main');
//    }

//    public function aynish(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/f6cdc715-884b-4bd8-8e2f-671a2c4fe27f');
//        }
//        return view('page.aynish', ['voluum' => 'http://331nq.voluumtrk.com/click/1']);
//    }
//
//    public function aynish_main()
//    {
//        return view('page.aynish_main');
//    }

//    public function hallupro(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/b108b4d0-615d-4752-931d-64b4ded4e86e');
//        }
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click/2' : route('order.confirm'),
//        ]);
//
//        return view('hallupro.landingpage', [
//            'form' => $form
//        ]);
//    }
//
//    public function hallupro2(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/9588a256-4f40-4c48-be96-71d19ad3abd6');
//        }
//        return view('hallupro.hallupro2', ['volum' => 'http://331nq.voluumtrk.com/click/1']);
//    }
//
//    public function hallupro1()
//    {
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => 'http://331nq.voluumtrk.com/click',
//        ]);
//
//        return view('hallupro.landingpage1', [
//            'form' => $form
//        ]);
//    }

    public function leadslist()
    {
        return view('page.leadslist', [
            'leadsSent' => $this->contactRepo->allLeadsSentFromToday(),
            'sold' => $this->contactRepo->allLeadsSoldFromToday()
        ]);
    }

    public function garciniaicc()
    {
        return view('page.garciniaicc', ['orders' => $this->orderRepo->getGarciniaFromFebruary()]);
    }

    public function blackmaskicc()
    {
        return view('page.blackmaskicc', ['orders' => $this->orderRepo->getBlackMaskFromFirstFebruary()]);
    }

    public function internaliccForm(Request $request, $id)
    {
        $order = $this->orderRepo->find($id);
        if($order){
           $order->icc_comment = $request->get('comment');
           $order->save();
        }
        return back();
    }

    public function externalicc()
    {
        $contacts = $this->contactRepo->searchAllPaginateExternals(50);
        return view('page.externalicc', ['contacts' => $contacts]);
    }

    public function externaliccFunCPA()
    {
        $price = 0;
        $sold = $this->contactRepo->searchFunCPAExternalsCurrentMonth();
        foreach($sold as $contact)
        {
            $price+=(int)$contact->cc_price;
        }

        $contacts = $this->contactRepo->searchExternals(4);
        return view('page.externaliccFunCPA', [
            'contacts' => $contacts,
            'thisMonthSold' => StrHelper::dotsInPrice($price),
            'thisMonthCounter' => $sold->count()
        ]);
    }

    public function externaliccAffninja()
    {
        $contacts = $this->contactRepo->searchExternals(6);
        return view('page.externaliccAffninja', [
            'contacts' => $contacts
        ]);
    }

    public function externaliccForm(Request $request, $id)
    {
        $contact = $this->contactRepo->find($id);
        if($contact){
            $contact->icc_comment = $request->get('comment');
            $contact->save();
        }
        return back();
    }

    public function missingAreas()
    {
        $areas = collect();
        $noDistrictId = $this->orderAdressRepo->noDistrictId();
        foreach($noDistrictId as $value){
            $areas->push(
                [
                    'province' => $value->order->old_province,
                    'city' => $value->order->old_city,
                    'district' => $value->order->old_district,
                    'courier' => $value->order->old_courier
                ]
            );
        }
        $unique = $areas->unique('district');
        $missingAreas = $unique->values()->all();
        return view('page.missingareas', compact('missingAreas'));
    }

    public function invalidPhones()
    {
        return view('page.invalidphones', ['contacts' => $this->contactRepo->getInvalidPhones()]);
    }

//    public function blackMask(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/fe0af855-8235-47c5-8e7f-ce2220528d2e');
//        }
//
//        return view('black.index', ['volum' => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click/1' : route('page.maskPreOrder')]);
//    }

    public function blackMaskThai()
    {
        return view('black.indexThai', ['uri' => route('page.maskPreOrder')]);
    }

//    public function maskPreOrder(Request $request)
//    {
//        if(app()->environment() == 'production' && strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/264fee52-472f-465a-b59f-9e69cf60375c');
//        }
//
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => app()->environment() == 'production' ? 'http://331nq.voluumtrk.com/click' : route('order.confirm'),
//        ]);
//
//        return view('black.maskPreOrder', [
//            'form' => $form
//        ]);
//    }
//
//    public function maskPreOrder1()
//    {
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => 'http://331nq.voluumtrk.com/click/2',
//        ]);
//
//        return view('black.maskPreOrder', [
//            'form' => $form
//        ]);
//    }
//
//    public function maskPreOrder2()
//    {
//        $form = $this->form(BuyForm::class, [
//            'method' => 'POST',
//            'url'    => 'http://331nq.voluumtrk.com/click/2',
//        ]);
//
//        return view('black.maskPreOrder2', [
//            'form' => $form
//        ]);
//    }

    public function maskPreOrder2Main()
    {
        $form = $this->form(BuyForm::class, [
            'method' => 'POST',
            'url'    => 'http://331nq.voluumtrk.com/click',
        ]);

        return view('black.maskPreOrder2_main', [
            'form' => $form
        ]);
    }

    public function maskPreOrderThai()
    {
        $form = $this->form(BuyForm::class, [
            'method' => 'POST',
            'url'    => route('order.confirm'),
        ]);

        return view('black.maskPreOrderThai', [
            'form' => $form
        ]);
    }

//    public function rahasia(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/20812c1f-ea02-4ad0-8385-4d89f477a3f3');
//        }
//
//        return view('page.rahasia', ['voluum' => 'http://331nq.voluumtrk.com/click/1']);
//    }

//    public function rahasia_main()
//    {
//        return view('page.rahasia_main');
//    }

//    public function bobot()
//    {
//          return redirect(route('page.cambogia').'?utm_source=email&utm_medium=email&utm_campaign=1');
////        return view('page.bobot');
//    }

//    public function kisah()
//    {
//        return view('page.kisah');
//    }

//    public function sensoduo()
//    {
//        return view('page.sensoduo');
//    }

//    public function teknologi()
//    {
//        return view('page.teknologi');
//    }

//    public function cinta(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/e46219d9-31d2-4e9e-97bb-210acc690c2b');
//        }
//        return view('page.cinta', ['voluum' => 'http://331nq.voluumtrk.com/click/1']);
//    }

//    public function terobosan(Request $request)
//    {
//        if(strpos($request->getUri(), 'voluum') == false && strpos(app('Illuminate\Routing\UrlGenerator')->previous(), 'voluum') == false){
//            return redirect('http://331nq.voluumtrk.com/7f87ae7d-12b5-43e5-9091-c77f23c139ec');
//        }
//        return view('page.terobosan');
//    }
//
//    public function terobosan_test()
//    {
//        return view('page.terobosan_test');
//    }

    public function tryLesson(Request $request)
    {
        try
        {
            $this->dispatch(
                new CollectContactData($request->all())
            );

            return redirect()
                ->back()
                ->with([
                    'ga_event' => [
                        'category' => 'Contact',
                        'action'   => 'New',
                        'label'    => $request->input('product')
                    ]
                ])
                ->withCookie(cookie('welcomePopup' . ucfirst($request->input('product')), true, (60 * 24 * 31)));

        } catch (\Exception $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function privacyPolicy()
    {
        return view('privacyPolicy');
    }
}
