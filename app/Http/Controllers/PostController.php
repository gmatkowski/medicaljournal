<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PostController extends Controller
{

    /**
     * @var PostRepository
     */
    protected $posts;


    /**
     * @param PostRepository $postRepository
     */
    public function __construct(PostRepository $postRepository)
    {
        $this->posts = $postRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        $data = [ 'objects' => $this->posts->all() ];

        return view('blog.list', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getShow($id)
    {
        $post = $this->posts->findByField('id', $id)->first();

        if (!$post)
        {
            throw new NotFoundHttpException;
        }
        else{

            $list = $this->posts->most_viewed();
            $data = [
                'post' => $post,
                'list' => $list
            ];

            $post->update(['views' => $post->views+1], $post->id);

            return view ('blog.post', $data);
        }

    }

}
