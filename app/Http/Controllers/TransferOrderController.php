<?php

namespace App\Http\Controllers;

use App\Exceptions\PageException;
use App\Payment\PaymentProvider;
use App\Repositories\OrderRepository;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Storage;

/**
 * Class TransferOrderController
 * @package App\Http\Controllers
 */
class TransferOrderController extends Controller {

    use DispatchesJobs;

    /**
     * @param Request $request
     * @param PaymentProvider $paymentProvider
     * @param OrderRepository $orderRepository
     * @return string
     */
    public function postStatus(Request $request, PaymentProvider $paymentProvider, OrderRepository $orderRepository)
    {
        try
        {
            return $paymentProvider->status($request, $orderRepository);
        } catch (PageException $e)
        {
            Storage::append('response.txt', json_encode([
                'request' => $request->all(),
                'message' => $e->getMessage()
            ]));

            return $e->getMessage();
        }
    }
}
