<?php

namespace App\Http\Controllers;

use App;

/**
 * Class ViewController
 * @package App\Http\Controllers
 */
abstract class ViewController extends Controller {

    /**
     * @var
     */
    protected $pages;

    /**
     * @var
     */
    protected $opinions;

    /**
     * @var
     */
    protected $languages;

    /**
     * @var
     */
    protected $settings;

    /**
     *
     */
    public function __construct()
    {
        $this->pages = App::make('App\Repositories\PageRepository');
        $this->opinions = App::make('App\Repositories\OpinionRepository');
        $this->languages = App::make('App\Repositories\LanguageRepository');
        $this->settings = App::make('App\Repositories\SettingRepository');

        $data = [
            'footer_pages'        => $this->pages->getFooterSet(),
            'opinions'            => $this->opinions->opinions(),
            'languages'           => $this->languages->slider(),
            'settings'            => $this->settings->settings(),
            'languagesWithSample' => $this->languages->withSample()
        ];

        view()->share($data);
    }
}
