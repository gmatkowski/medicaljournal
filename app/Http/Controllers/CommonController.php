<?php

namespace App\Http\Controllers;

use App\Jobs\ResetPromotion;
use App\Repositories\LanguageRenewLinkRepository;

/**
 * Class CommonController
 * @package App\Http\Controllers
 */
class CommonController extends Controller {

    /**
     * @param LanguageRenewLinkRepository $languageRenewLinkRepository
     * @param $token
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getResetPromotion(LanguageRenewLinkRepository $languageRenewLinkRepository, $token)
    {
        $link = $languageRenewLinkRepository->findByField('token', $token)->first();
        if (!$link || $link->active == 0)
        {
            return redirect()->route('page.index');
        }

        $languageRenewLinkRepository->setInactive($link);

        $this->dispatch(
            new ResetPromotion()
        );

        return redirect()->route('page.index');
    }
}
