<?php
namespace App\Http\Controllers;

use App\Call\CallCenter;
use App\Call\NetTelLists;
use Event;
use App\Events\OrderWasPayed;
use App\Repositories\ContactRepository;
use App\Shipping\ShippingProvider;
use Carbon\Carbon;
use App\Shipping\JayonExpressProvider;
use App\Shipping\ProviderSwitcher;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use PulkitJalan\GeoIP\Facades\GeoIP;

/**
 * Class TestController
 * @package App\Http\Controllers
 */
class TestController extends Controller {

    public function getFocus(CallCenter $callCenter)
    {
        return $callCenter->getCampaignList();
    }

}
