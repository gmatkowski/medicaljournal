<?php

namespace App\Http\Controllers;

use App\Payment\PaymentProvider;
use App\Payment\VeritransTransferProvider;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\Cookie;
use Mail;
use App\Exceptions\PageException;
use App\Forms\BuyForm;
use App\Forms\ContactForm;
use App\Forms\ContactShortForm;
use App\Forms\DataForm;
use App\Jobs\SendContact;
use App\Jobs\CollectContactData;
use App\Jobs\SendContactReminder;
use App\Repositories\ContactRepository;
use App\Repositories\FaqRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\LanguagePackageRepository;
use App\Repositories\LessonRepository;
use App\Repositories\PageRepository;
use App\Repositories\RecordRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilderTrait;
use Illuminate\Support\Facades\Session;
use App\Repositories\OrderAddressRepository;
use App\Repositories\CityRepository;
use App\Repositories\DistrictRepository;
use App\Forms\CheckoutForm;

/**
 * Class PageController
 * @package App\Http\Controllers
 */
class PageController extends ViewController {

    use FormBuilderTrait;

    /**
     * @var PageRepository
     */
    protected $pages;

    /**
     * @var LanguageRepository
     */
    protected $languageRepository;

    protected $orderAddress;

    protected $veritransProvider;

    protected $orderRepository;

    /**
     * @param PageRepository $pageRepository
     */
    public function __construct(
        PageRepository $pageRepository,
        LanguageRepository $languageRepository,
        OrderAddressRepository $orderAddress,
        VeritransTransferProvider $veritransProvider,
        OrderRepository $orderRepository
    )
    {
        parent::__construct();
        $this->pages = $pageRepository;
        $this->languageRepository = $languageRepository;
        $this->orderAddress = $orderAddress;
        $this->veritransProvider = $veritransProvider;
        $this->orderRepository = $orderRepository;
    }

    /**
     * @param RecordRepository $recordRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function records(RecordRepository $recordRepository)
    {
        $data = [
            'page'    => $this->pages->getById(16),
            'records' => $recordRepository->records()
        ];

        return view('page.records', $data);
    }

    /**
     * @param LessonRepository $lessonRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function lessons(LessonRepository $lessonRepository)
    {
        $data = [
            'lessons' => $lessonRepository->lessons(),
            'page'    => $this->pages->getById(22)
        ];

        return view('page.lessons', $data);
    }

    /**
     * @param LanguagePackageRepository $languagePackageRepository
     * @param null $symbol
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function languages(LanguagePackageRepository $languagePackageRepository, FaqRepository $faqRepository, $symbol = null)
    {
        $languagesJson = collect();

        $languages = $this->languages->slider();

        foreach ($languages as $language)
        {
            $languagesJson->push([
                'id'        => $language->id,
                'name'      => $language->name,
                'price'     => $language->price,
                'price_old' => $language->price_old,
                'currency'  => $language->currency
            ]);
        }

        $lang = $languages->first();
        if (!is_null($symbol))
        {
            $lang = $this->languages->findByField('symbol', $symbol)->first();
            if (!$lang)
            {
                return redirect()
                    ->route('page.index')
                    ->with([
                        'message' => [
                            'type'    => 'error',
                            'message' => trans('languages.language.not.found')
                        ],
                    ]);
            }

        }

        $form = $this->form(BuyForm::class, [
            'method' => 'POST',
            'url'    => route('order.checkout.save'),
            'data'   => [
                'isPost' => false
            ]
        ])->remove('terms');

        $data = [
            'lang'          => $lang,
            'pages'         => $this->pages->getLanguageseSet(),
            'packages'      => $languagePackageRepository->packages(),
            'languagesJson' => $languagesJson->keyBy('id')->toJson(),
            'faqs'          => $faqRepository->faqs(1),
            'buyForm'       => $form
        ];

        return view('page.languages', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function opinions()
    {
        $data = [];

        return view('page.opinions', $data);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function contact()
    {
        $form = $this->form(ContactForm::class, [
            'method' => 'POST',
            'url'    => route('page.contact.send')
        ]);

        $data = [
            'form' => $form,
            'page' => $this->pages->getById(19)
        ];

        return view('page.contact', $data);
    }

    /**
     * @param $form
     * @param Request $request
     * @param null $redirect
     * @return $this|\Illuminate\Http\RedirectResponse|null
     */
    protected function sendContact($form, Request $request, $redirect = null)
    {
        if (!$form->isValid())
        {
            return redirect()->back()->withErrors($form->getErrors())->withInput();
        }

        $contact = $this->dispatch(
            new SendContact($request->all())
        );

        $request->session()->put('contact-id', $contact->id);

        if (is_null($redirect))
        {
            return redirect()
                ->route('page.index')
                ->with([
                    'ga_event' => [
                        'category' => 'Data',
                        'action'   => 'New contact',
                        'label'    => $request->get('first_name') . ' ' . $request->get('last_name') . ' ' . $request->get('email'),
                    ],
                    'message'  => [
                        'type'    => 'success',
                        'message' => trans('contact.sucess')
                    ]
                ]);
        }
        else
        {
            return $redirect;
        }
    }

    /**
     * @param Request $request
     * @return $this|PageController|\Illuminate\Http\RedirectResponse
     */
    public function postInactiveLanguage(Request $request)
    {
        $form = $this->form(DataForm::class);

        return $this->sendContact($form, $request);
    }

    /**
     * @param Request $request
     * @return $this|PageController|\Illuminate\Http\RedirectResponse
     */
    public function postContact(Request $request)
    {
        $form = $this->form(ContactForm::class);

        return $this->sendContact($form, $request);
    }

    /**
     * @param Request $request
     * @return $this|PageController|\Illuminate\Http\RedirectResponse
     */
    public function postContactShort(Request $request)
    {
        $form = $this->form(ContactShortForm::class);

        return $this->sendContact($form, $request, app('App\Http\Controllers\OrderController')->postCheckout($request));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function postTryLesson(Request $request)
    {
        try
        {
            $form = $this->form(DataForm::class);

            if (!$form->isValid())
            {
                return redirect()
                    ->back()
                    ->withErrors($form->getErrors())
                    ->withInput()
                    ->with([
                        'popup' => 'try'
                    ]);
            }

            $contact = $this->dispatch(
                new CollectContactData($request->all())
            );

            $request->session()->put('try', $request->input('language'));
            $request->session()->put('contact-id', $contact->id);

            if ($request->session()->has('asp'))
            {
                return redirect()->route('asp.trial');
            }
            else
            {
                return redirect()->route('page.try');
            }

        } catch (PageException $e)
        {
            return redirect()->back()->with('message', [
                'type'    => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function terms()
    {
        $data = [
            'page' => $this->pages->getById(24)
        ];

        return view('page.page', $data);
    }

    /**
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function trylesson(Request $request, ContactRepository $contactRepository)
    {

        if (!$request->session()->has('contact-id'))
        {
            return redirect()->route('page.index')->with([
                'popup' => 'try'
            ]);
        }

        $contact = $contactRepository->find($request->session()->get('contact-id'));
        if (!$contact)
        {
            $request->session()->forget(['contact-id', 'try']);

            return redirect()->route('page.index')->with([
                'popup' => 'try'
            ]);
        }

        try
        {
            if ($contact->reminded == 0)
            {
                $this->dispatch(
                    new SendContactReminder($contact)
                );
            }
        } catch (\Exception $e)
        {

        }

        $data = [
            'page' => $this->pages->getById(26)
        ];

        return view('page.try', $data);
    }

    /**
     * @param Request $request
     * @param ContactRepository $contactRepository
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function trylesson2(Request $request, ContactRepository $contactRepository)
    {

        if (!$request->session()->has('contact-id'))
        {
            return redirect()->route('page.index')->with([
                'popup' => 'try'
            ]);
        }

        $contact = $contactRepository->find($request->session()->get('contact-id'));
        if (!$contact)
        {
            $request->session()->forget(['contact-id', 'try']);

            return redirect()->route('page.index')->with([
                'popup' => 'try'
            ]);
        }

        try
        {
            if ($contact->reminded == 0)
            {
                $this->dispatch(
                    new SendContactReminder($contact)
                );
            }
        } catch (\Exception $e)
        {

        }

        $data = [
            'page'     => $this->pages->getById(30),
            'page2'    => $this->pages->getById(31),
            'page3'    => $this->pages->getById(32),
            'discount' => $this->languages->active()->first(),
        ];

//        return view('page.try', $data);
        return view('page.try2', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dimapage()
    {
        $data = [
            'page' => $this->pages->getById(29)
        ];

        return view('page.dimapage', $data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function dimapage2()
    {
        $data = [
            'page' => $this->pages->getById(30)
        ];

        return view('page.page', $data);
    }

    /**
     * @param LanguageRepository $languages
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mobile(LanguageRepository $languages)
    {
        $english = $languages->findByField('symbol', 'en')->first();
        $form = $this->form(DataForm::class, [
            'method' => 'POST',
            'url'    => route('page.try.send'),
        ])
            ->modify('name', 'text', ['label' => 'Type your name'])
            ->modify('email', 'email', ['label' => 'Type your email'])
            ->modify('phone', 'text', ['label' => 'Type your phone number']);

        $data = [
            'form'    => $form,
            'english' => $english
        ];

        return view('page.mobile', $data);
    }

    public function orderTyp()
    {
        Mail::send('email.confirm', [], function ($m) {
            $m->to(Session::get('email'))->subject('Konfirmasi pemesanan!');
        });
        $data = [
            'order' => Session::get('order_flash')
        ];
        return view('page.typ', $data);
    }

    public function editOrderAddress(Request $request)
    {
        $input = $request->all();
        $orderAddress = $this->orderAddress->find($request->get('id'));
        $orderAddress->update($input);

        return redirect(route('page.confirm'));
    }

    public function orderCheckout($product)
    {
        $data = [
            'order' => Session::get('order_flash'),
            'form' => $this->form(CheckoutForm::class, [
                'method' => 'POST',
                'url'    => route('page.checkout.form'),
            ])
        ];

        switch ($product) {
            case 'garcinia':
                $view = view('page.garcinia_order_checkout', $data);
                break;
            case 'hallupro':
                $view = view('hallupro.order_checkout', $data);
                break;
            default:
                $view = view('page.garcinia_order_checkout', $data);
                break;
        }

        return $view;
    }

    public function postOrderCheckout(Request $request, PaymentProvider $paymentProvider, CityRepository $cityRepository, DistrictRepository $districtRepository)
    {
        $order = Session::get('order_flash');
        $full_name = $request->get('first_name').' '.$request->get('surname');
        $order->name = $full_name;
        $order->email = $request->get('email');
        $order->first_name = $request->get('first_name');
        $order->last_name = $request->get('surname');
        $price = str_replace(".", "", $request->get('price'));
        $order->price = $price;
        $district_id = $districtRepository->findWhere([['name', '=', $request->get('district')]])->first();
        $city = $cityRepository->find($request->get('city'));
        if($city){
            $province = $city->province;
        }

        $order->address()->first()->update([
            'full_name'   => $full_name,
            'first_name'  => $request->get('first_name'),
            'last_name'   => $request->get('surname'),
            'province'    => $province ? $province->name : null,
            'city'        => $city ? $city->name : null,
            'address'     => $request->get('address'),
            'postal_code' => $request->get('postcode'),
            'subdistrict' => $request->get('subdistrict'),
            'district_id' => $district_id ? $district_id->id : null
        ]);
        $order->languages->first()->qty = $request->get('qty');
        $order->languages->first()->price = $price/$request->get('qty');
        $order->languages->first()->save();
        $order->save();

        $method = $request->get('pembayaran');
        if($method=='Veritrans'){
           $order->method=1;
           $order->save();
           return $paymentProvider->order($order);
        }

        $order->contact->call_center = 0;
        $order->contact->save();

        $product = Session::get('product');
        switch ($product) {
            case 1:
                $confirmUrl = route('page.confirm', ['product' => 'garcinia', 'id' => $order->id, 'email' => $request->get('email')]);
                break;
            case 2:
                $confirmUrl = route('page.confirm', ['product' => 'blackmask', 'id' => $order->id, 'email' => $request->get('email')]);
                break;
            case 3:
                $confirmUrl = route('page.confirm', ['product' => 'hallupro', 'id' => $order->id, 'email' => $request->get('email')]);
                break;
        }

        return redirect($confirmUrl);
    }

//    public function orderConfirm($product)
//    {
////        $emailRegex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
////        $email = Session::get('email');
//        $data = [
////            'email' => $email,
//            'order' => Session::get('order_flash')
//        ];
//
//        switch ($product) {
//            case 'blackmask':
////                $emailTo = 'email.black_confirm';
//                $view = view('black.order_confirm', $data);
//                break;
//            case 'hallupro':
//                $view = view('hallupro.order_confirm', $data);
//                break;
//            default:
////                $emailTo = 'email.garcinia_confirm';
//                $view = view('page.garcinia_order_confirm', $data);
//                break;
//        }
//
////        if(preg_match($emailRegex, $email)) {
////            Mail::send($emailTo, [], function ($m) use($email) {
////                $m->to($email)->subject('Konfirmasi pemesanan!');
////            });
////        }
//
//        return $view;
//    }

    public function orderConfirm($product, $id)
    {
//        $emailRegex = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';

        $cookieName = 'orderCheckout';
        $value = Cookie::get($cookieName);
        if ($value && $value == $id) {
            return redirect(route('page.index'));
        }

        $order = $this->orderRepository->findWhere(['id' => $id])->first();
        $fb_pixel = null;
        $tracking = null;
        if ($order && $order->contact()->first()) {
            $params = $order->contact()->first()->params;
            $decoded = base64_decode($params);
            if (strpos($decoded, 'fb_pixel') !== false) {
                $fb_pixel = explode('fb_pixel=', $decoded)[1];
                if (strpos($fb_pixel, '&') !== false) {
                    $fb_pixel = explode('&', $fb_pixel)[0];
                }
            }
            if (strpos($decoded, 'tracking') !== false) {
                $tracking = explode('tracking=', $decoded)[1];
                if (strpos($tracking, '&') !== false) {
                    $tracking = explode('&', $tracking)[0];
                }
            }
        }

        $data = [
            'order' => $order,
            'product' => $product,
            'fb_pixel' => $fb_pixel ? base64_decode($fb_pixel) : null,
            'tracking' => $tracking ? base64_decode($tracking) : null
        ];

//        switch ($product) {
//            case 'garcinia':
//                $emailTo = 'email.order_confirm';
//                break;
//            default:
//                $emailTo = null;
//                break;
//        }
//
//        if($emailTo && preg_match($emailRegex, $email)) {
//            Mail::send($emailTo, $data, function ($m) use($email) {
//                $m->to($email)->subject('Konfirmasi pemesanan!');
//            });
//        }

        Session::flush();

        Cookie::queue(Cookie::make($cookieName, $id, 15));
        $view = view('page.order_confirm', $data);

        return $view;
    }

    public function getContact()
    {
        return view('contact');
    }

    public function getComposition()
    {
        return view('composition');
    }

    public function getQuestions()
    {
        return view('questions');
    }
}
