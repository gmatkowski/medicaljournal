<?php

namespace App\Http\Middleware;

use App\Repositories\LanguageRepository;
use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class CommonViews
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $data = Cache::remember('users', 10080, function() {
            return [
                'product1' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT1_SYMBOL'))->first(),
                'product2' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT2_SYMBOL'))->first(),
                'product3' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT3_SYMBOL'))->first(),
                'product4' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT4_SYMBOL'))->first(),
                'product5' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT5_SYMBOL'))->first(),
                'product6' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT6_SYMBOL'))->first(),
                'product7' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT7_SYMBOL'))->first(),
                'product8' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT8_SYMBOL'))->first(),
                'product9' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT9_SYMBOL'))->first(),
                'product10' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT10_SYMBOL'))->first(),
                'product11' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT11_SYMBOL'))->first(),
                'product12' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT12_SYMBOL'))->first(),
                'product13' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT13_SYMBOL'))->first(),
                'product14' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT14_SYMBOL'))->first(),
                'product15' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT15_SYMBOL'))->first(),

                'product17' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT17_SYMBOL'))->first(),
                'product18' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT18_SYMBOL'))->first(),
                'product19' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT19_SYMBOL'))->first(),

                'product20' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT20_SYMBOL'))->first(),
                'product21' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT21_SYMBOL'))->first(),
                'product22' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT22_SYMBOL'))->first(),

                'product23' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT23_SYMBOL'))->first(),

                'product24' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT24_SYMBOL'))->first(),

                'product25' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT25_SYMBOL'))->first(),
                'product26' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT26_SYMBOL'))->first(),
                'product27' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT27_SYMBOL'))->first(),
                'product28' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT28_SYMBOL'))->first(),

                'product29' => app(LanguageRepository::class)->findByField('symbol', env('PRODUCT29_SYMBOL'))->first(),
            ];
        });

//        Joint Cure -> Flexa Plus
//        Detoclean -> Detoxic
//        Ultra Slim -> Diet Booster

        $queries = $request->query();
        $params = '';
        foreach($queries as $key => $value){
            if($key!='voluumdata' && $value){
                $params.=$key.'='.$value;
                if($value!=end($queries)){
                    $params.='&';
                }
            }
        }

        View::share(array_merge($data, [
            'uriParams' => base64_encode($params)
        ]));

        return $next($request);
    }
}
