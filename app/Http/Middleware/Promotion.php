<?php

namespace App\Http\Middleware;

use App\Jobs\ResetPromotion;
use Closure;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\View;

class Promotion {

    use DispatchesJobs;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $isPromotion = true;
        if (isset($_COOKIE['pCookie']))
        {
            $isPromotion = (int)$_COOKIE['pCookie'] > time();

            if (!$isPromotion)
            {
                $this->dispatch(
                    new ResetPromotion()
                );

                return redirect(URL::current());
            }
        }

        View::share([
            'isPromotion' => $isPromotion
        ]);

        $request->session()->put('isPromotion', $isPromotion);

        return $next($request);
    }
}
