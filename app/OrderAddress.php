<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class OrderAddress
 * @package App
 */
class OrderAddress extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'order_addresses';

    /**
     * @var array
     */
    protected $fillable = [
        'first_name', 'full_name', 'last_name', 'address', 'postal_code', 'province', 'city', 'district_id', 'subdistrict', 'sub_district_id', 'order_id'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'full_name'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo('App\Order', 'order_id');
    }

    /**
     * @return mixed
     */
    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

    /**
     * @return mixed
     */
    public function subDistrict()
    {
        return $this->belongsTo('App\SubDistrict', 'sub_district_id');
    }

    /**
     * @return string
     */
    public function getFullNameAttribute()
    {
        return implode(' ', [$this->first_name, $this->last_name]);
    }

    /**
     * @return string
     */
    public function getAddressFullStateAttribute()
    {
        $subdistrict = $this->subDistrict;
        if($subdistrict){
            $postCode = $subdistrict->postcode;
        }

        $district = $this->district;
        $city = $district->city;
        $province = $city->province;
        $country = $province->country;

        if($subdistrict){
            return implode(', ', [$country->name, $province->name, $city->name, $district->name, $subdistrict->name, isset($postCode) ? $postCode->name : null]);
        }

        return implode(', ', [$country->name, $province->name, $city->name, $district->name]);
    }

}
