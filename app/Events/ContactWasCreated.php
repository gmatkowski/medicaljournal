<?php

namespace App\Events;

use App\Contact;
use Illuminate\Queue\SerializesModels;

class ContactWasCreated extends Event
{
    use SerializesModels;

    /**
     * @var Contact
     */
    protected $contact;

    public function __construct(Contact $contact)
    {
        $this->contact = $contact;
    }

    public function contact(): Contact
    {
        return $this->contact;

    }
}