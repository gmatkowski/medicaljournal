<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class BankTransferUnique
 * @package App
 */
class BankTransferUnique extends Model {

    /**
     * @var string
     */
    protected $table = 'bank_transfer_uniques';

    /**
     * @var array
     */
    protected $fillable = [
        'unique_number'
    ];
}
