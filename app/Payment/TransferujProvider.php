<?php namespace App\Payment;

use App\Exceptions\PageException;
use App\Forms\PaymentForm;
use App\Repositories\OrderRepository;
use Config;
use App\Order;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Kris\LaravelFormBuilder\FormBuilderTrait;

/**
 * Class TransferujProvider
 * @package App\Payment
 */
class TransferujProvider implements PaymentProvider {

    use DispatchesJobs, FormBuilderTrait;

    /**
     * @param Order $order
     * @param array $options
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function order(Order $order, $options = [])
    {
        $model = new \stdClass();
        $model->opis = trans('order.buy.payment.title');
        $model->crc = $order->token;
        $model->kwota = number_format($order->price, 2);
        $model->email = $order->email;
        $model->telefon = $order->phone;
        $model->imie = $order->first_name;
        $model->nazwisko = $order->last_name;

        if (isset($options['payment']) && $options['payment'] == 'paypal')
        {
            $model->kanal = 106;
        }

        $data = [
            'order' => $order,
            'form'  => $this->form(PaymentForm::class, [
                'method' => 'POST',
                'url'    => Config::get('transferuj.url'),
                'model'  => $model
            ])
        ];

        return view('order.transferuj.place', $data);
    }

    /**
     * @param Request $request
     * @param OrderRepository $orderRepository
     * @return string
     * @throws PageException
     */
    public function status(Request $request, OrderRepository $orderRepository)
    {
        if (!$request->has(['tr_id', 'tr_amount', 'tr_crc',]) || $request->getClientIp() != Config::get('transferuj.ip') || $request->input('tr_status') != true)
        {
            throw new PageException('no data');
        }

        $order = $orderRepository->findByField('token', $request->input('tr_crc'))->first();
        if ($order && $order->status == 0)
        {
            $md5sum = md5(Config::get('transferuj.id') . $request->input('tr_id') . $request->input('tr_amount') . $request->input('tr_crc') . Config::get('transferuj.security_code'));
            if ($md5sum != $request->input('md5sum'))
            {
                $order->status = 4;
                $order->save();

                throw new PageException('md5sum check failed');
            }

            if ($request->input('tr_error') == 'surcharge')
            {
                $order->status = 4;
                $order->save();

                throw new PageException('bad amount');
            }

            $order->status = 1;
            $order->tr_id = $request->input('tr_id');
            $order->payed_at = Carbon::now();
            $order->save();

            return 'TRUE';
        }
        else
        {
            throw new PageException('Order not found');
        }
    }
}