<?php namespace App\Payment;

use App\Exceptions\PageException;
use App\Repositories\OrderRepository;
use App\Repositories\SubDistrictRepository;
use App\Shipping\ShippingProvider;
use Config;
use App\Order;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Kris\LaravelFormBuilder\FormBuilderTrait;

/**
 * Class CodProvider
 * @package App\Payment
 */
class VeritransCodProvider implements CodProvider
{

    use DispatchesJobs, FormBuilderTrait;

    /**
     * @var SubDistrictRepository
     */
    protected $subDistricts;

    /**
     * @var ShippingProvider
     */
    protected $shipping;

    /**
     * VeritransCodProvider constructor.
     * @param SubDistrictRepository $subDistrictRepository
     * @param OrderRepository $orderRepository
     */
    public function __construct(SubDistrictRepository $subDistrictRepository/*, ShippingProvider $shippingProvider*/, OrderRepository $orderRepository)
    {
        $this->subDistricts = $subDistrictRepository;
        $this->orders = $orderRepository;
        //$this->shipping = $shippingProvider;
    }


    /**
     * @param Order $order
     * @param array $options
     * @return \Illuminate\Http\RedirectResponse
     * @throws PageException
     */
    public function order(Order $order, $options = [])
    {
        $message = [];

        $order->done = 1;

        if ($this->orders->getDuplicate($order)->count() > 1) {
            $order->duplicated = 1;
            $message = [
                'type'    => 'error',
                'message' => trans('order.status.message.cod.fail')
            ];
        } else {
            $message = [
                'type'    => 'success',
                'message' => trans('order.status.message.cod')
            ];
        }

        $order->save();

        /*
        try
        {
            $this->shipping->sale($order);
        } catch (\Exception $e)
        {
            Log::error('ShippingError: ', $e->getMessage());
        }
        */

        return redirect()
            ->route('page.index')
            ->with([
                'ga_event'      => [
                    'category' => 'Checkout',
                    'action'   => 'Payment-COD',
                    'label'    => 'ok',
                ],
                'ga_conversion' => [
                    'amount'   => $order->price,
                    'currency' => trans('common.current.fb.symbol')
                ],
                'message'       => $message,
            ]);
    }

    /**
     * @param Request $request
     * @param OrderRepository $orderRepository
     * @return string
     * @throws PageException
     */
    public function status(Request $request, OrderRepository $orderRepository)
    {

    }

}