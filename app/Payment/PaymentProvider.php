<?php namespace App\Payment;

use App\Order;
use App\Repositories\OrderRepository;
use Illuminate\Http\Request;

/**
 * Interface PaymentProvider
 * @package App\Payment
 */
interface PaymentProvider {

    /**
     * @param Order $order
     * @param array $options
     * @return mixed
     */
    public function order(Order $order, $options = []);

    /**
     * @param Request $request
     * @param OrderRepository $orderRepository
     * @return mixed
     */
    public function status(Request $request, OrderRepository $orderRepository);

}