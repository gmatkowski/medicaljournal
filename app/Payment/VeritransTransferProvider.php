<?php namespace App\Payment;

use App\Exceptions\PageException;
use App\Repositories\OrderRepository;
use Carbon\Carbon;
use Config;
use App\Order;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Kris\LaravelFormBuilder\FormBuilderTrait;

/**
 * Class VeritransProvider
 * @package App\Payment
 */
class VeritransTransferProvider implements PaymentProvider {

    use DispatchesJobs, FormBuilderTrait;

    /**
     *
     */
    public function __construct()
    {
        \Veritrans_Config::$serverKey = Config::get('veritrans.server_key');
        \Veritrans_Config::$clientKey = Config::get('veritrans.client_key');
        \Veritrans_Config::$isProduction = env('PAYMENT_PRODUCTION_MODE', false);
    }

    protected function bankDirect(Order $order, $items, $customer_details)
    {
        try
        {
            $transaction = array(
                'payment_type'        => 'bank_transfer',
                "bank_transfer"       => array(
                    "bank" => 'permata',
                ),
                'customer_details'    => $customer_details,
                'item_details'        => $items,
                'transaction_details' => array(
                    'order_id'     => $order->id,
                    'gross_amount' => $order->price,
                )
            );

            $result = \Veritrans_VtDirect::charge($transaction);

            $order->done = 1;
            $order->save();

            return response()->json([
                'status' => 'ok',
                'data'   => $result
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @param Order $order
     * @param $items
     * @param $customer_details
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    protected function bankWeb(Order $order, $items, $customer_details)
    {
        $transaction = array(
            'payment_type'        => 'vtweb',
            "vtweb"               => array(
                "credit_card_3d_secure" => true,
            ),
            'customer_details'    => $customer_details,
            'item_details'        => $items,
            'transaction_details' => array(
                'order_id'     => $order->id,
                'gross_amount' => $order->price,
            )
        );

        $vtweb_url = \Veritrans_VtWeb::getRedirectionUrl($transaction);

        $order->done = 1;
        $order->save();

        return redirect($vtweb_url . '?locale=id');
    }

    /**
     * @param Order $order
     * @param $items
     * @param $customer_details
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function creditCard(Order $order, $items, $customer_details)
    {
        $transaction_data = array(
            'payment_type'        => 'credit_card',
            'credit_card'         => [
                'token_id' => $order->token_id,
                'bank'     => 'bni'
            ],
            'customer_details'    => $customer_details,
            'item_details'        => $items,
            'transaction_details' => [
                'order_id'     => $order->id,
                'gross_amount' => round($order->price, 0),
            ]
        );

        $result = \Veritrans_VtDirect::charge($transaction_data);

        if (in_array($result->status_code, [200, 201]))
        {
            if ($result->transaction_status == 'capture' && $result->fraud_status == 'accept')
            {
                $order->status = 1;
                $order->tr_id = $result->transaction_id;
            }
            $order->done = 1;
            $order->save();

            return redirect()
                ->route('page.index')
                ->with([
                    'ga_event'      => [
                        'category' => 'Checkout',
                        'action'   => 'Payment',
                        'label'    => 'ok',
                    ],
                    'ga_conversion' => [
                        'amount'   => $result->gross_amount,
                        'currency' => trans('common.current.fb.symbol')
                    ],
                    'message'       => [
                        'type'    => 'success',
                        'message' => trans('order.status.message.ok')
                    ],
                ]);
        }
        else
        {
            return redirect()->route('page.index')->with([
                'message' => [
                    'type'    => 'error',
                    'message' => trans('order.status.message.error')
                ],
            ]);
        }
    }

    protected function mandiriClick(Order $order, $items, $customer_details, $options = [])
    {
        $card_number = str_replace(' ', '', $options['mandiri_card_number']);
        $token = str_replace(' ', '', $options['mandiri_token']);

        $transaction_data = array(
            'payment_type'        => 'mandiri_clickpay',
            'mandiri_clickpay'    => array(
                'card_number' => $card_number,
                'input1'      => substr($card_number, -10),
                'input2'      => $order->price,
                'input3'      => $options['mandiri_input3'],
                'token'       => $token
            ),
            'item_details'        => $items,
            'transaction_details' => [
                'order_id'     => $order->id,
                'gross_amount' => round($order->price, 0),
            ],
            'customer_details'    => $customer_details
        );

        $result = \Veritrans_VtDirect::charge($transaction_data);

        if ($result->transaction_status == "settlement")
        {
            $order->status = 1;
            $order->tr_id = $result->transaction_id;
            $order->payed_at = Carbon::now();
            $order->done = 1;

            $response = redirect()
                ->route('page.index')
                ->with([
                    'ga_event' => [
                        'category' => 'Checkout',
                        'action'   => 'Payment',
                        'label'    => 'ok',
                    ],
                    'ga_conversion' => [
                        'amount'   => $result->gross_amount,
                        'currency' => trans('common.current.fb.symbol')
                    ],
                    'message'  => [
                        'type'    => 'success',
                        'message' => trans('order.status.message.ok')
                    ],
                ]);

        }
        else
        {
            $order->status = 4;

            $response = redirect()->route('page.index')->with([
                'message' => [
                    'type'    => 'error',
                    'message' => trans('order.status.message.error')
                ],
            ]);
        }

        $order->save();

        return $response;
    }

    protected function mandiriATM(Order $order, $items, $customer_details, $options = [])
    {
        try
        {
            $transaction_data = array(
                'payment_type'        => 'echannel',
                'echannel'            => array(
                    'bill_info1' => "Payment for",
                    'bill_info2' => "debt"
                ),
                'item_details'        => $items,
                'transaction_details' => [
                    'order_id'     => $order->id,
                    'gross_amount' => round($order->price, 0),
                ],
                'customer_details'    => $customer_details
            );

            $result = \Veritrans_VtDirect::charge($transaction_data);

            $order->done = 1;
            $order->save();

            return response()->json([
                'status' => 'ok',
                'data'   => $result
            ]);
        } catch (\Exception $e)
        {
            return response()->json([
                'status'  => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    public function order(Order $order, $options = [])
    {
        $name = parseToFullname($order->name);

        $items = [];

        foreach ($order->languages as $language)
        {
            $items[] = [
                'id'       => $language->id,
                'price'    => $order->price,
                'quantity' => 1,
                'name'     => $language->language->name
            ];
        }

        $customer_details = array(
            'first_name' => $name['first_name'],
            'last_name'  => $name['last_name'],
            'email'      => $order->email,
            'phone'      => $order->phone
        );

        switch ($order->method)
        {
            case 1:
                if (Config::get('veritrans.vt_web') == true)
                {
                    return $this->bankWeb($order, $items, $customer_details);
                }
                else
                {
                    return $this->bankDirect($order, $items, $customer_details);
                }
                break;
            case 3:
                return $this->creditCard($order, $items, $customer_details);
                break;
            case 5:
                return $this->mandiriClick($order, $items, $customer_details, $options);
                break;
            case 6:
                return $this->mandiriATM($order, $items, $customer_details, $options);
                break;
            default:
                throw new PageException('404');
        }


    }

    /**
     * @param Request $request
     * @param OrderRepository $orderRepository
     * @return string
     * @throws PageException
     */
    public function status(Request $request, OrderRepository $orderRepository)
    {
        try
        {
            $notif = new \Veritrans_Notification();
            $order = $orderRepository->find($notif->order_id);

            if ($notif->transaction_status == 'capture')
            {
                if ($notif->payment_type == 'credit_card')
                {
                    if ($notif->fraud_status == 'accept')
                    {
                        $order->status = 1;
                        $order->tr_id = $notif->transaction_id;
                        $order->payed_at = Carbon::now();

                    }
                    else
                    {
                        $order->status = 0;
                    }
                }
            }
            else if ($notif->transaction_status == 'settlement')
            {
                $order->status = 1;
                $order->tr_id = $notif->transaction_id;
                $order->payed_at = Carbon::now();
            }
            else if ($notif->transaction_status == 'pending')
            {
                $order->status = 0;
            }
            else if ($notif->transaction_status == 'deny')
            {
                $order->status = 4;
            }
            else if ($notif->transaction_status == 'cancel')
            {
                $order->status = 4;
            }

            $order->save();

            Log::notice('Veritrans order '.$order->id.' status changed: '.$order->status);

            return 'OK';

        } catch (\Exception $e)
        {
            Log::notice('Veritrans payment error: ' . $e->getMessage());

            return $e->getMessage();
        }
    }

}