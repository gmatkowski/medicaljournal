<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Config;

/**
 * Class SendContact
 * @package App\Jobs
 */
class SendContact extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var array
     */
    protected $data;

    /**
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function handle()
    {

        $contact = $this->dispatch(
            new CollectContactData($this->data)
        );

        if ($this->data['type'] == 1)
        {
            $this->collectData();

            $this->dispatch(
                new SendEmail(trans('contact.email.target'), trans('contact.email.subject'), 'email.contact', [
                    'data' => $this->data
                ])
            );
        }

        return $contact;
    }

    /**
     * @return array
     */
    protected function collectData()
    {
        if (isset($this->data['language']))
        {
            $language = app('App\Repositories\LanguageRepository');
            $this->data['language'] = $language->find($this->data['language']);
        }
    }
}
