<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class ResetPromotion
 * @package App\Jobs
 */
class ResetPromotion extends Job implements SelfHandling
{
    /**
     *
     */
    public function __construct()
    {

    }

    /**
     *
     */
    public function handle()
    {
        setcookie("pCookie", "", time() - 3600, '/');
    }
}
