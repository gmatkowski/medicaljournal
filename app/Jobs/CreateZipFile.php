<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class CreateZipFile
 * @package App\Jobs
 */
class CreateZipFile extends Job implements SelfHandling {

    /**
     * @var
     */
    protected $files;
    /**
     * @var
     */
    protected $filename;
    /**
     * @var
     */
    protected $destination;

    /**
     * @param $files
     * @param $destination
     */
    public function __construct($files, $destination)
    {
        $this->files = $files;
        $this->destination = $destination;
    }


    /**
     * @return bool
     */
    public function handle()
    {
        //vars
        $valid_files = array();
        //if files were passed in...
        if (is_array($this->files))
        {
            //cycle through each file
            foreach ($this->files as $file)
            {
                //make sure the file exists
                if (file_exists($file))
                {
                    $valid_files[] = $file;
                }
            }
        }

        //if we have good files...
        if (count($valid_files))
        {
            //create the archive
            $zip = new \ZipArchive();

            if ($zip->open($this->destination, \ZIPARCHIVE::CREATE | \ZIPARCHIVE::OVERWRITE) !== true)
            {
                return false;
            }
            //add the files
            foreach ($valid_files as $file)
            {
                $zip->addFile($file, basename($file));
            }
            //close the zip -- done!
            $zip->close();

            //check to make sure the file exists
            return file_exists($this->destination);
        }
        else
        {
            return false;
        }
    }
}
