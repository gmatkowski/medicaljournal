<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Opinion;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class CreateOpinion
 * @package App\Jobs\Admin
 */
class CreateOpinion extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * @return Opinion
     */
    function handle()
    {
        $files = collect($this->request->files->get('translation'));

        $opinion = new Opinion();

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $opinion->fill([
                    $lang => [
                        'name'        => $value['name'],
                        'description' => $value['description']
                    ]
                ]);

                if ($files->has($lang))
                {
                    if (isset($files->get($lang)['icon']) && $files->get($lang)['icon'] instanceof UploadedFile)
                    {
                        $file = $files->get($lang)['icon'];

                        $filename = str_random(32) . '.' . $file->getClientOriginalExtension();

                        $this->dispatch(
                            new ResizeImage($file, public_path('opinions/icons'), $filename)
                        );

                        $opinion->fill([
                            $lang => [
                                'icon' => $filename
                            ]
                        ]);
                    }
                }
            }
        }

        $opinion->fill([
            'main' => $this->request->input('main', 0)
        ]);

        $opinion->save();

        return $opinion;
    }
}
