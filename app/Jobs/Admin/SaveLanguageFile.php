<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\LanguageFile;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

/**
 * Class SaveJob
 * @package App\Jobs\Admin
 */
class SaveLanguageFile extends Job implements SelfHandling {

    /**
     * @var
     */
    protected $file;

    /**
     * @var Request
     */
    protected $request;


    /**
     * @param LanguageFile $languageFile
     * @param Request $request
     */
    public function __construct(LanguageFile $languageFile, Request $request)
    {
        $this->file = $languageFile;
        $this->request = $request;
    }

    /**
     * @return LanguageFile
     * @throws AdminException
     */
    function handle()
    {
        if (!$this->request->hasFile('file'))
        {
            throw new AdminException('Nie wybrałeś żadnego pliku');
        }

        $filename = str_random(32) . '.' . $this->request->file('file')->getClientOriginalExtension();
        $path = storage_path('app/languages/' . $this->file->language->id);
        if (!File::exists($path))
        {
            File::makeDirectory($path, 755, true);
        }

        $this->request->file('file')->move($path, $filename);

        $this->file->fill([
            'file' => $filename
        ]);
        $this->file->save();

        return $this->file;
    }
}
