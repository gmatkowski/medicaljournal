<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Page;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class CreatePage
 * @package App\Jobs\Admin
 */
class CreatePage extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    /**
     * @return Page
     */
    function handle()
    {
        $page = new Page();

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['title']))
            {
                $page->fill([
                    $lang => [
                        'title'   => $value['title'],
                        'content' => $value['content']
                    ]
                ]);
            }
        }

        if ($this->request->hasFile('icon')) {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('pages/icons'), $filename)
            );

            $page->icon = $filename;
        }

        $page->save();

        return $page;
    }
}
