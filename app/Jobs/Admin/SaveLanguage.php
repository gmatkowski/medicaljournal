<?php

namespace App\Jobs\Admin;

use App\Helpers\StrHelper;
use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Language;
use App\Repositories\StockRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use File;

/**
 * Class SaveLanguage
 * @package App\Jobs\Admin
 */
class SaveLanguage extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Language
     */
    protected $language;

    /**
     * @var Request
     */
    protected $request;

    protected $stocks;

    /**
     * @param Language $language
     * @param Request $request
     */
    public function __construct(Language $language, Request $request, StockRepository $stockRepository)
    {
        $this->language = $language;
        $this->request = $request;
        $this->stocks = $stockRepository;
    }

    /**
     * @return Page
     */
    function handle()
    {

        if ($this->request->hasFile('sample'))
        {
            $filename = str_random(32) . '.' . $this->request->file('sample')->getClientOriginalExtension();

            if (!File::exists(public_path('languages/sample')))
            {
                File::makeDirectory(public_path('languages/sample'), 755, true);
            }

            $this->request->file('sample')->move(public_path('languages/sample'), $filename);

            $this->language->sample = $filename;
        }

        if ($this->request->hasFile('image'))
        {
            $filename = str_random(32) . '.' . $this->request->file('image')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('image'), public_path('languages/images'), $filename, [226, null])
            );

            $this->language->image = $filename;
        }

        if ($this->request->hasFile('image_10days'))
        {
            $filename = str_random(32) . '.' . $this->request->file('image_10days')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('image_10days'), public_path('languages/images'), $filename, [226, null])
            );

            $this->language->image_10days = $filename;

        }

        if ($this->request->hasFile('icon'))
        {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('languages/icons'), $filename, [33, 33])
            );

            $this->language->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $this->language->fill([
                    $lang => [
                        'name'              => $value['name'],
                        'name_short'        => $value['name_short'],
                        'description_short' => $value['description_short'],
                        'description_try'   => $value['description_try'],
                        'price'             => $value['price'],
                        'price_old'         => $value['price_old'],
                        'price_spec'        => $value['price_spec'],
                        'currency'          => $value['currency']
                    ]
                ]);
            }
        }

        if($this->request->get('amount')>=0 && $this->request->get('min_amount')>=0)
        {
            $this->language->fill($this->request->only(['symbol', 'is_active', 'shipment_item_id', 'amount', 'min_amount']));
            $this->language->save();

            $stocksBehind = $this->stocks->searchStocksBehind($this->language->symbol, Carbon::now());
            foreach($stocksBehind as $key => $stockBehind){
                $stockBehind->update([
                    'open' => StrHelper::mustBeMoreThanZero($key==0 ? $this->language->amount : $stocksBehind[$key-1]->open-($stockBehind->in-$stockBehind->out)),
                    'close' => StrHelper::mustBeMoreThanZero($key==0 ? null : $stocksBehind[$key-1]->open)
                ]);
            }
        } else {
            $this->language->fill($this->request->only(['symbol', 'is_active', 'shipment_item_id']));
            $this->language->save();
        }

        return $this->language;
    }
}
