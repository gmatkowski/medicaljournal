<?php

namespace App\Jobs\Admin;

use App\Post;
use App\Jobs\Job;
use App\Jobs\ResizeImage;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class CreatePost
 * @package App\Jobs\Admin
 */
class CreatePost extends Job implements SelfHandling
{

    use DispatchesJobs;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $post = new Post();


        if ($this->request->hasFile('icon')) {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('posts/icons'), $filename, [730, 413])
            );
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('posts/icons/thumbs'), $filename, [55, 55])
            );

            $post->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value) {
            if (!empty($value['title'])) {
                $post->fill([
                    $lang => [
                        'title' => $value['title'],
                        'article' => $value['article']
                    ]
                ]);
            }
        }

        $post->author = $this->request->input('author');
        $post->save();

        return $post;
    }
}
