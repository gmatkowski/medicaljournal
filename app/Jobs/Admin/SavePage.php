<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Page;
use App\Repositories\PageRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use App\Jobs\ResizeImage;

/**
 * Class SavePage
 * @package App\Jobs\Admin
 */
class SavePage extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var
     */
    protected $page;

    /**
     * @var Request
     */
    protected $request;


    /**
     * @param Page $page
     * @param Request $request
     */
    public function __construct(Page $page, Request $request)
    {
        $this->page = $page;
        $this->request = $request;
    }

    /**
     * @return Page
     */
    function handle()
    {

        if ($this->request->hasFile('icon')) {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('pages/icons'), $filename)
            );

            $this->page->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['title']))
            {
                $this->page->fill([
                    $lang => [
                        'title'   => $value['title'],
                        'content' => $value['content']
                    ]
                ]);
            }
        }

        $this->page->save();

        return $this->page;
    }
}
