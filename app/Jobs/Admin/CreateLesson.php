<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Lesson;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class CreateLesson
 * @package App\Jobs\Admin
 */
class CreateLesson extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $lesson = new Lesson();


        if ($this->request->hasFile('icon'))
        {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('lessons/icons'), $filename, [55, 55])
            );

            $lesson->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $lesson->fill([
                    $lang => [
                        'name'        => $value['name'],
                        'description' => $value['description']
                    ]
                ]);
            }
        }

        $lesson->fill([
            'level' => $this->request->input('level')
        ]);

        $lesson->save();

        return $lesson;
    }
}
