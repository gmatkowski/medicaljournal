<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Language;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use File;

/**
 * Class CreateLanguage
 * @package App\Jobs\Admin
 */
class CreateLanguage extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $language = new Language();

        if ($this->request->hasFile('image'))
        {
            $filename = str_random(32) . '.' . $this->request->file('image')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('image'), public_path('languages/images'), $filename, [226, null])
            );

            $language->image = $filename;

        }

        if ($this->request->hasFile('image_10days'))
        {
            $filename = str_random(32) . '.' . $this->request->file('image_10days')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('image_10days'), public_path('languages/images'), $filename, [226, null])
            );

            $language->image_10days = $filename;

        }

        if ($this->request->hasFile('sample'))
        {
            $filename = str_random(32) . '.' . $this->request->file('sample')->getClientOriginalExtension();

            if (!File::exists(public_path('languages/sample')))
            {
                File::makeDirectory(public_path('languages/sample'), 755, true);
            }

            $this->request->file('sample')->move(public_path('languages/sample'), $filename);

            $this->language->sample = $filename;
        }

        if ($this->request->hasFile('icon'))
        {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('languages/icons'), $filename, [33, 33])
            );

            $language->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $language->fill([
                    $lang => [
                        'name'              => $value['name'],
                        'name_short'        => $value['name_short'],
                        'description_short' => $value['description_short'],
                        'description_try'   => $value['description_try'],
                        'price'             => $value['price'],
                        'price_old'         => $value['price_old'],
                        'price_spec'         => $value['price_spec'],
                        'currency'          => $value['currency']
                    ]
                ]);
            }
        }

        $language->fill($this->request->only(['symbol', 'is_active', 'shipment_item_id']));
        $language->save();

        return $language;
    }
}
