<?php

namespace App\Jobs\Admin;

use App\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;

/**
 * Class SaveJob
 * @package App\Jobs\Admin
 */
class SaveJob extends Job implements SelfHandling {

    /**
     * @var
     */
    protected $job;

    /**
     * @var Request
     */
    protected $request;


    /**
     * @param Job $job
     * @param Request $request
     */
    public function __construct(Job $job, Request $request)
    {
        $this->job = $job;
        $this->request = $request;
    }


    /**
     * @return Job
     */
    function handle()
    {
        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $this->job->fill([
                    $lang => [
                        'name'      => $value['name'],
                        'price_new' => $value['price_new'],
                        'price_old' => $value['price_old'],
                        'currency' => $value['currency']
                    ]
                ]);
            }
        }

        $this->job->save();

        return $this->job;
    }
}
