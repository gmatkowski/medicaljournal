<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Repositories\DistrictRepository;
use App\Repositories\SubDistrictRepository;
use App\Repositories\OrderLanguageRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;
use App\Order;

class SaveOrder extends Job implements SelfHandling
{
    protected $order;

    protected $request;

    public function __construct(Order $order, Request $request)
    {
        $this->order = $order;
        $this->request = $request;
    }

    public function handle(
        OrderLanguageRepository $orderLanguageRepository,
        DistrictRepository $districtRepository,
        SubDistrictRepository $subDistrictRepository
    )
    {
        $checked = collect();
        $unchecked = collect();
        foreach ($this->request->get('languages') as $key => $value) {
            if (isset($value['id'])) {
                $checked->push([
                    'id'    => $value['id'],
                    'qty'   => $value['qty'],
                    'price' => $value['price']
                ]);
            } else {
                $unchecked->push($key);
            }
        }

        if ($checked->count() == 0)
        {
            dd('Choose at least one product');
        }

        $this->order->update($this->request->except(['_token']));

        $districtId = $this->request->input('district_id');
        if($districtId){
            $district = $districtRepository->find($districtId);
        }
        $subDistrictId = $this->request->input('subdistrict');
        if($subDistrictId){
            $subDistrict = $subDistrictRepository->find($subDistrictId);
        }
        $this->order->address()->update([
            'first_name'  => $this->request->input('first_name'),
            'last_name'   => $this->request->input('last_name'),
            'address'     => $this->request->input('address'),
            'province'    => isset($district) ? $district->city->province->name : null,
            'city'        => isset($district) ? $district->city->name : null,
            'postal_code' => (isset($subDistrict) && isset($subDistrict->postcode)) ? $subDistrict->postcode->name : null,
            'district_id' => $districtId,
            'subdistrict' => isset($subDistrict) ? $subDistrict->name : null,
            'sub_district_id' => $subDistrictId
        ]);

        foreach($unchecked as $id){
            $languageToDelete = $orderLanguageRepository->findWhere([['language_id', '=', $id], ['order_id', '=', $this->order->id]])->first();
            if($languageToDelete){
               $languageToDelete->delete();
            }
        }

        foreach ($checked as $language) {
            $this->order->languages()->updateOrCreate([
                'language_id' => $language['id']
            ], [
                'qty'         => $language['qty'],
                'price'       => $language['price']
            ]);
        }
        $price = $this->order->summary_price;

        $this->order->update([
            'price' => ceil($price),
            'name'  => $this->request->input('first_name') . ' ' . $this->request->input('last_name'),
        ]);

    }
}