<?php

namespace App\Jobs\Admin;

use App\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;

/**
 * Class CreateJob
 * @package App\Jobs\Admin
 */
class CreateJob extends Job implements SelfHandling {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function handle()
    {
        $job = new Job();

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $job->fill([
                    $lang => [
                        'name'      => $value['name'],
                        'price_new' => $value['price_new'],
                        'price_old' => $value['price_old'],
                        'currency'  => $value['currency']
                    ]
                ]);
            }
        }

        $job->save();

        return $job;
    }
}
