<?php

namespace App\Jobs\Admin;

use App\Faq;
use App\Jobs\Job;
use App\Jobs\ResizeImage;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class SaveFaq
 * @package App\Jobs\Admin
 */
class SaveFaq extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Faq
     */
    protected $faq;


    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Faq $faq
     * @param Request $request
     */
    public function __construct(Faq $faq, Request $request)
    {
        $this->faq = $faq;
        $this->request = $request;
    }

    /**
     * @return Page
     */
    function handle()
    {

        if ($this->request->hasFile('icon'))
        {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('faqs/icons'), $filename, [55, 55])
            );

            $this->faq->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $this->faq->fill([
                    $lang => [
                        'name'        => $value['name'],
                        'description' => $value['description'],
                    ]
                ]);
            }
        }

        $this->faq->type = $this->request->input('type');

        $this->faq->save();

        return $this->faq;
    }
}
