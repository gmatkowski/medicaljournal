<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Jobs\SaveRecord;
use App\Record;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class CreateRecord
 * @package App\Jobs\Admin
 */
class CreateRecord extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $files = collect($this->request->files->get('translation'));

        $record = new Record();

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $record->fill([
                    $lang => [
                        'name' => $value['name']
                    ]
                ]);

                if ($files->has($lang))
                {
                    if (isset($files->get($lang)['record']) && $files->get($lang)['record'] instanceof UploadedFile)
                    {
                        $file = $files->get($lang)['record'];
                        $filename = str_random(32) . '.' . $file->getClientOriginalExtension();

                        $this->dispatch(
                            new SaveRecord($file, $filename)
                        );

                        $record->fill([
                            $lang => [
                                'record' => $filename
                            ]
                        ]);
                    }
                }

            }
        }

        $record->language_id = $this->request->input('language_id');
        $record->save();

        return $record;
    }
}
