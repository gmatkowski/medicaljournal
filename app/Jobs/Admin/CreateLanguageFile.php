<?php

namespace App\Jobs\Admin;

use App\Exceptions\AdminException;
use App\Jobs\Job;
use App\Language;
use App\Repositories\LanguageFileRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

/**
 * Class CreateJob
 * @package App\Jobs\Admin
 */
class CreateLanguageFile extends Job implements SelfHandling {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Language
     */
    protected $language;

    /**
     * @param Request $request
     * @param Language $language
     */
    public function __construct(Request $request, Language $language)
    {
        $this->request = $request;
        $this->language = $language;
    }


    public function handle(LanguageFileRepository $languageFileRepository)
    {
        if (!$this->request->hasFile('file'))
        {
            throw new AdminException('Nie wybrałeś żadnego pliku');
        }

        $filename = str_random(32) . '.' . $this->request->file('file')->getClientOriginalExtension();
        $path = storage_path('app/languages/' . $this->language->id);
        if (!File::exists($path))
        {
            File::makeDirectory($path, 755, true);
        }

        $part = 1;
        $files = $this->language->files;
        if ($files->count() > 0)
        {
            $part = $files->max('part') + 1;
        }

        $this->request->file('file')->move($path, $filename);

        $file = $languageFileRepository->create([
            'file' => $filename,
            'part' => $part
        ]);
        $file->language()->associate($this->language);
        $file->save();

        return $file;
    }
}
