<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Lesson;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class SaveLesson
 * @package App\Jobs\Admin
 */
class SaveLesson extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Lesson
     */
    protected $lesson;


    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Lesson $lesson
     * @param Request $request
     */
    public function __construct(Lesson $lesson, Request $request)
    {
        $this->lesson = $lesson;
        $this->request = $request;
    }

    /**
     * @return Page
     */
    function handle()
    {

        if ($this->request->hasFile('icon'))
        {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('lessons/icons'), $filename, [55, 55])
            );

            $this->lesson->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $this->lesson->fill([
                    $lang => [
                        'name'        => $value['name'],
                        'description' => $value['description'],
                    ]
                ]);
            }
        }

        $this->lesson->fill([
            'level' => $this->request->input('level')
        ]);

        $this->lesson->save();

        return $this->lesson;
    }
}
