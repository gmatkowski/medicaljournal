<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Opinion;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Jobs\ResizeImage;

/**
 * Class SaveOpinion
 * @package App\Jobs\Admin
 */
class SaveOpinion extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var
     */
    protected $opinion;

    /**
     * @var Request
     */
    protected $request;


    /**
     * @param Opinion $opinion
     * @param Request $request
     */
    public function __construct(Opinion $opinion, Request $request)
    {
        $this->opinion = $opinion;
        $this->request = $request;
    }


    /**
     * @return Opinion
     */
    function handle()
    {

        $files = collect($this->request->files->get('translation'));

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $this->opinion->fill([
                    $lang => [
                        'name'        => $value['name'],
                        'description' => $value['description']
                    ]
                ]);

                if ($files->has($lang))
                {

                    if (isset($files->get($lang)['icon']) && $files->get($lang)['icon'] instanceof UploadedFile)
                    {
                        $file = $files->get($lang)['icon'];

                        $filename = str_random(32) . '.' . $file->getClientOriginalExtension();

                        $this->dispatch(
                            new ResizeImage($file, public_path('opinions/icons'), $filename)
                        );

                        $this->opinion->fill([
                            $lang => [
                                'icon' => $filename
                            ]
                        ]);
                    }
                }

            }
        }

        $this->opinion->fill([
            'main' => $this->request->input('main', 0)
        ]);

        $this->opinion->save();

        return $this->opinion;
    }
}
