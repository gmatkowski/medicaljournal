<?php

namespace App\Jobs\Admin;

use App\Post;
use App\Jobs\Job;
use App\Jobs\ResizeImage;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class SavePost
 * @package App\Jobs\Admin
 */
class SavePost extends Job implements SelfHandling
{

    use DispatchesJobs;

    /**
     * @var Post
     */
    protected $post;


    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Post $post
     * @param Request $request
     */
    public function __construct(Post $post, Request $request)
    {
        $this->post = $post;
        $this->request = $request;
    }

    /**
     * @return Page
     */
    function handle()
    {

        if ($this->request->hasFile('icon')) {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('posts/icons'), $filename, [730, 413])
            );
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('posts/icons/thumbs'), $filename, [55, 55])
            );

            $this->post->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value) {
            if (!empty($value['title'])) {
                $this->post->fill([
                    $lang => [
                        'title' => $value['title'],
                        'article' => $value['article']
                    ]
                ]);
            }
        }

        $this->post->author = $this->request->input('author');
        $this->post->save();

        return $this->post;
    }
}
