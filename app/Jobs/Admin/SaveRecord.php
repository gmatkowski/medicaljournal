<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Opinion;
use App\Record;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use App\Jobs\ResizeImage;

/**
 * Class SaveRecord
 * @package App\Jobs\Admin
 */
class SaveRecord extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var
     */
    protected $record;

    /**
     * @var Request
     */
    protected $request;


    /**
     * @param Record $record
     * @param Request $request
     */
    public function __construct(Record $record, Request $request)
    {
        $this->record = $record;
        $this->request = $request;
    }


    /**
     * @return Record
     */
    function handle()
    {

        $files = collect($this->request->files->get('translation'));

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $this->record->fill([
                    $lang => [
                        'name' => $value['name']
                    ]
                ]);

                if ($files->has($lang))
                {

                    if (isset($files->get($lang)['record']) && $files->get($lang)['record'] instanceof UploadedFile)
                    {
                        $file = $files->get($lang)['record'];

                        $filename = str_random(32) . '.' . $file->getClientOriginalExtension();

                        $this->dispatch(
                            new \App\Jobs\SaveRecord($file, $filename)
                        );

                        $this->record->fill([
                            $lang => [
                                'record' => $filename
                            ]
                        ]);
                    }
                }

            }
        }

        $this->record->language_id = $this->request->input('language_id');
        $this->record->save();

        return $this->record;
    }
}
