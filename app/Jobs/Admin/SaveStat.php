<?php

namespace App\Jobs\Admin;

use App\Jobs\Job;
use App\Jobs\ResizeImage;
use App\Stat;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class SaveStat
 * @package App\Jobs\Admin
 */
class SaveStat extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Stat
     */
    protected $stat;


    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Stat $stat
     * @param Request $request
     */
    public function __construct(Stat $stat, Request $request)
    {
        $this->stat = $stat;
        $this->request = $request;
    }

    /**
     * @return Page
     */
    function handle()
    {

        if ($this->request->hasFile('icon'))
        {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('stats/icons'), $filename)
            );

            $this->stat->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $this->stat->fill([
                    $lang => [
                        'name'  => $value['name'],
                        'value' => $value['value'],
                    ]
                ]);
            }
        }

        $this->stat->save();

        return $this->stat;
    }
}
