<?php

namespace App\Jobs\Admin;

use App\Faq;
use App\Jobs\Job;
use App\Jobs\ResizeImage;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;

/**
 * Class CreateFaq
 * @package App\Jobs\Admin
 */
class CreateFaq extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function handle()
    {
        $faq = new Faq();


        if ($this->request->hasFile('icon'))
        {
            $filename = str_random(32) . '.' . $this->request->file('icon')->getClientOriginalExtension();
            $this->dispatch(
                new ResizeImage($this->request->file('icon'), public_path('faqs/icons'), $filename, [55, 55])
            );

            $faq->icon = $filename;
        }

        foreach ($this->request->input('translation') as $lang => $value)
        {
            if (!empty($value['name']))
            {
                $faq->fill([
                    $lang => [
                        'name'        => $value['name'],
                        'description' => $value['description']
                    ]
                ]);
            }
        }

        $faq->type = $this->request->input('type');

        $faq->save();

        return $faq;
    }
}
