<?php

namespace App\Jobs;

use App\Repositories\ContactRepository;
use App\Repositories\LanguageRepository;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class SendNetTelRequest
 * @package App\Jobs
 */
class CollectContactData extends Job implements SelfHandling {

    /**
     * @var array
     */
    protected $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param ContactRepository $contactRepository
     * @param LanguageRepository $languageRepository
     * @return mixed
     */
    public function handle(
        ContactRepository $contactRepository,
        LanguageRepository $languageRepository
    )
    {
        try
        {
            $compare = [];

            if (isset($this->data['type']))
            {
                $compare = [
                    'type' => $this->data['type']
                ];
            }

            $compare['popup'] = isset($this->data['popup']) ? 1 : 0;

            if(isset($this->data['productId'])){
                switch ($this->data['productId']) {
                    case 1:
                        $compare['product'] = 'cambogia';
                        $this->data['product'] = 'cambogia';
                        break;
                    case 2:
                        $compare['product'] = 'blackmask';
                        $this->data['product'] = 'blackmask';
                        break;
                    case 3:
                        $compare['product'] = 'hallupro';
                        $this->data['product'] = 'hallupro';
                        break;
                    case 4:
                        $compare['product'] = 'waist';
                        $this->data['product'] = 'waist';
                        break;
                    case 5:
                        $compare['product'] = 'detoclean';
                        $this->data['product'] = 'detoclean';
                        break;
                    case 6:
                        $compare['product'] = 'coffee';
                        $this->data['product'] = 'coffee';
                        break;
                    case 7:
                        $compare['product'] = 'joint';
                        $this->data['product'] = 'joint';
                        break;
                    case 8:
                        $compare['product'] = 'posture';
                        $this->data['product'] = 'posture';
                        break;
                    case 9:
                        $compare['product'] = 'bra';
                        $this->data['product'] = 'bra';
                        break;
                    case 10:
                        $compare['product'] = 'powerbank';
                        $this->data['product'] = 'powerbank';
                        break;
                    case 11:
                        $compare['product'] = 'ultraslim';
                        $this->data['product'] = 'ultraslim';
                        break;

                    case 12:
                        $compare['product'] = 'bustiere';
                        $this->data['product'] = 'bustiere';
                        break;
                    case 13:
                        $compare['product'] = 'vervalen';
                        $this->data['product'] = 'vervalen';
                        break;
                    case 14:
                        $compare['product'] = 'claireinstantwhitening';
                        $this->data['product'] = 'claireinstantwhitening';
                        break;
                    case 15:
                        $compare['product'] = 'manuskin';
                        $this->data['product'] = 'manuskin';
                        break;

                    case 20:
                        $compare['product'] = 'flexaplus';
                        $this->data['product'] = 'flexaplus';
                        break;
                    case 21:
                        $compare['product'] = 'detoxic';
                        $this->data['product'] = 'detoxic';
                        break;
                    case 22:
                        $compare['product'] = 'dietbooster';
                        $this->data['product'] = 'dietbooster';
                        break;

                    case 23:
                        $compare['product'] = 'imove';
                        $this->data['product'] = 'imove';
                        break;

                    case 24:
                        $compare['product'] = 'dominator';
                        $this->data['product'] = 'dominator';
                        break;

                    case 25:
                        $compare['product'] = 'nutrilashpromo';
                        $this->data['product'] = 'nutrilashpromo';
                        break;
                    case 26:
                        $compare['product'] = 'leferyacrpromo';
                        $this->data['product'] = 'leferyacrpromo';
                        break;
                    case 27:
                        $compare['product'] = 'forsopromo';
                        $this->data['product'] = 'forsopromo';
                        break;
                    case 28:
                        $compare['product'] = 'ultrahairpromo';
                        $this->data['product'] = 'ultrahairpromo';
                        break;
                    case 29:
                        $compare['product'] = 'musclegain';
                        $this->data['product'] = 'musclegain';
                        break;
                }
            }

            if (isset($this->data['language']))
            {
                $language = $languageRepository->find($this->data['language']);

                $this->data['language_id'] = $language->id;

                $compare['language_id'] = $language->id;
            }

            if ((!isset($this->data['name']) || empty($this->data['name'])) && array_key_exists('first_name', $this->data) && array_key_exists('last_name', $this->data))
            {
                $this->data['name'] = $this->data['first_name'] . ' ' . $this->data['last_name'];
            } else {
                $this->data['name'] = $this->data['first_name'];
            }

            $compare += ['email' => $this->data['email'] ?? null, 'phone' => $this->data['phone']];

            $this->data['token'] = str_random(8);

            if (array_key_exists('other_lang', $this->data))
            {
                $this->data['other_lang'] = implode(', ', $this->data['other_lang']);
            }

            $contact = $contactRepository->createOrUpdateByPhone($compare , $this->data);

            if (isset($this->data['ext_client_id']))
            {
                $contact->externalClient()->associate($this->data['ext_client_id']);
            }
            $contact->save();

            return $contact;

        } catch (\Exception $e)
        {
            return null;
        }
    }
}
