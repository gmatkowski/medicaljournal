<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Order;
use App\Repositories\OrderFileRepository;
use Illuminate\Contracts\Bus\SelfHandling;

/**
 * Class CreateOrderFiles
 * @package App\Jobs
 */
class CreateOrderFiles extends Job implements SelfHandling {

    /**
     * @var Order
     */
    protected $order;

    /**
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }


    /**
     * @param OrderFileRepository $orderFileRepository
     */
    public function handle(OrderFileRepository $orderFileRepository)
    {
        $this->order->files()->delete();
        foreach ($this->order->languages as $orderLanguage)
        {
            foreach ($orderLanguage->language->files as $file)
            {
                $orderFile = $orderFileRepository->create([
                    'hash' => str_random(16)
                ]);
                $orderFile->order()->associate($this->order);
                $orderFile->file()->associate($file);
                $orderFile->save();
            }
        }
    }
}
