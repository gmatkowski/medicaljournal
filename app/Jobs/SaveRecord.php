<?php

namespace App\Jobs;

use App\Exceptions\AdminException;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;

/**
 * Class SaveRecord
 * @package App\Jobs
 */
class SaveRecord extends Job implements SelfHandling {

    private $mimes = ['audio/mp3', 'audio/ogg'];
    protected $dir;

    /**
     * @var UploadedFile
     */
    protected $file;
    /**
     * @var
     */
    protected $filename;

    /**
     * @param UploadedFile $file
     * @param $filename
     */
    public function __construct(UploadedFile $file, $filename)
    {
        $this->file = $file;
        $this->filename = $filename;
        $this->dir = public_path('records');
    }


    /**
     *
     */
    public function handle()
    {
        if (!in_array($this->file->getClientMimeType(), $this->mimes))
        {
            throw new AdminException('Niedozwolony format pliku, dozwolone: ' . implode(', ', $this->mimes));
        }

        if (!File::exists($this->dir))
        {
            File::makeDirectory($this->dir, $mode = 0755, true);
        }

        File::copy($this->file, $this->dir . '/' . $this->filename);
    }
}
