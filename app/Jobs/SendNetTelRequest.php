<?php

namespace App\Jobs;

use anlutro\cURL\cURL;
use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Config;

/**
 * Class SendNetTelRequest
 * @package App\Jobs
 */
class SendNetTelRequest extends Job implements SelfHandling {

    /**
     *
     */
    const URL = 'http://api.net-tel.it/ploutsourcing/import2gad4kursy.php';

    /**
     * @var
     */
    protected $list_id;
    /**
     * @var array
     */
    protected $data;

    /**
     * @param $id
     * @param array $data
     */
    public function __construct($id, array $data)
    {
        $this->list_id = $id;
        $this->data = $data;
    }


    /**
     * @return bool
     * @throws \Exception
     */
    public function handle()
    {
        if (Config::get('app.debug') == true)
        {
            return false;
        }

        $name = explode(' ', $this->data['name']);

        $data = [
            'list_id' => $this->list_id
        ];

        if (isset($name[0]))
        {
            $data['imie'] = $name[0];
        }

        if (isset($name[1]))
        {
            $data['nazwisko'] = $name[1];
        }

        if (isset($this->data['phone']))
        {
            $data['telefon1'] = $this->data['phone'];
            $data['auth_code'] = md5("PL0" . preg_replace("/[^0-9]/", "", $data['telefon1']));
        }

        if (isset($this->data['email']))
        {
            $data['email'] = $this->data['email'];
        }

        $curl = new cURL();
        $response = $curl->post(self::URL, $data);

        $response->body = (object)text_to_array($response->body);

        if ($response->statusCode != 200)
        {
            throw new \Exception($response->statusText);
        }

        /*
                if (isset($response->body->status) && trim($response->body->status) == 'error')
                {
                    throw new \Exception(trim($response->body->reason));
                }
        */

        return true;
    }
}
