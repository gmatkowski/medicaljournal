<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Bus\SelfHandling;
use Image;
use Storage;
use File;

/**
 * Class ResizeImage
 * @package App\Jobs
 */
class ResizeImage extends Job implements SelfHandling {

    /**
     * @var
     */
    protected $file;
    /**
     * @var
     */
    protected $dir;
    /**
     * @var
     */
    protected $filename;
    /**
     * @var null
     */
    protected $sizes;

    /**
     * @param $file
     * @param $dir
     * @param $filename
     * @param null $sizes
     */
    public function __construct($file, $dir, $filename, $sizes = null)
    {
        $this->file = $file;
        $this->dir = $dir;
        $this->filename = $filename;
        $this->sizes = $sizes;
    }


    /**
     *
     */
    public function handle()
    {
        $img = Image::make($this->file);
        if (!is_null($this->sizes))
        {
            if (in_array(null, $this->sizes))
            {
                $img->resize($this->sizes[0], $this->sizes[1], function ($constraint)
                {
                    $constraint->aspectRatio();
                });
            }
            else
            {
                $img->fit($this->sizes[0], $this->sizes[1], null, 'top');
            }
        }

        if (!File::exists($this->dir))
        {
            File::makeDirectory($this->dir, $mode = 0755, true);
        }

        $img->save($this->dir . '/' . $this->filename);
    }
}
