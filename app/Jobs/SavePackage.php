<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Repositories\LanguagePackageRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;

class SavePackage extends Job implements SelfHandling {

    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }


    public function handle(LanguagePackageRepository $languagePackageRepository)
    {
        $packages = $languagePackageRepository->all();

        foreach ($packages as $pack)
        {
            $pack->delete();
        }

        if ($this->request->has('packages'))
        {
            foreach ($this->request->get('packages') as $in_pack => $pack)
            {
                $languagePackageRepository->create([
                    'in_pack' => $in_pack,
                    'percent' => $pack['percent'] > 0 ? $pack['percent'] : 0
                ]);
            }
        }

    }
}
