<?php

namespace App\Jobs;

use App\Jobs\SendEmail;
use App\Order;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class SendStatusReminder
 * @package App\Jobs
 */
class SendOrderReminder extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var bool
     */
    protected $repeated;

    /**
     * @param Order $order
     * @param bool|false $repeated
     */
    public function __construct(Order $order, $repeated = false)
    {
        $this->order = $order;
        $this->repeated = $repeated;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function handle()
    {
        switch ($this->order->status)
        {
            case 0:
                $title = trans('emails.order.pending.title');
                $template = 'email.order_pending';
                break;
            case 4:
                $title = trans('emails.order.failure.title');
                $template = 'email.order_failure';
                break;
            default:
                throw new \Exception('no reminder defined');
        }

        if ($this->repeated === false)
        {
            $this->dispatch(
                new SendEmail($this->order->email, $title, $template, ['order' => $this->order])
            );
        }

        if ($this->order->reminders->count() > 0)
        {
            $this->order->reminders->last()->increment('repeated');
        }
        else
        {
            $this->order->reminders()->create([
                'status'   => $this->order->status,
                'repeated' => 1
            ]);
        }

        return true;
    }
}
