<?php

namespace App\Jobs;

use App\Repositories\BankTransferUniqueRepository;
use App\Sms\ZenzivaSms;
use App\Exceptions\PageException;
use App\Repositories\ContactRepository;
use App\Repositories\DistrictRepository;
use App\Repositories\LanguagePackageRepository;
use App\Repositories\LanguageRepository;
use App\Repositories\OrderRepository;
use App\Repositories\PostCodeRepository;
use App\Repositories\SubDistrictRepository;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use LaravelLocalization;
use DB;

/**
 * Class CreateOrder
 * @package App\Jobs
 */
class CreateOrder extends Job implements SelfHandling {

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var null
     */
    protected $contact_id;

    /**
     * @var
     */
    protected $is_promotion;

    /**
     * @var null
     */
    protected $custom_languages;

    protected $orderMailer;

    /**
     * @param Request $request
     * @param null $contact_id
     * @param bool|false $promotion
     * @param null $languages
     */
    public function __construct(Request $request, $contact_id = null, $promotion = false, $languages = null)
    {
        $this->request = $request;
        $this->contact_id = $contact_id;
        $this->is_promotion = $promotion;
        $this->custom_languages = $languages;
        $this->orderMailer = app('App\Mail\OrderMailer');
    }


    public function handle(
        OrderRepository $orderRepository,
        LanguageRepository $languageRepository,
        LanguagePackageRepository $languagePackageRepository,
        ContactRepository $contactRepository,
        DistrictRepository $districtRepository,
        SubDistrictRepository $subDistrictRepository,
        PostCodeRepository $postCodeRepository,
        BankTransferUniqueRepository $bankTransferUniqueRepository
    )
    {
        $token = str_random(8);

        try
        {
            DB::beginTransaction();

            if (is_null($this->custom_languages))
            {
                $ids = (array)$this->request->input('languages');
                if (count($ids) == 0)
                {
                    throw new PageException(trans('order.no.languages'));
                }
                $languages = $languageRepository->byIds($ids);

                $sum = $languages->sum($this->is_promotion ? $this->request->cookie('special') ? 'price_spec' : 'price' : 'price_old');
                $packages = $languagePackageRepository->packages()->keyBy('in_pack');
                if (isset($packages[$languages->count()]))
                {
                    $package = $packages[$languages->count()];
                    $sum = round($sum - ($sum * $package->percent / 100), 2);
                }

            }
            else
            {
                $sum = $this->custom_languages->sum($this->request->cookie('special') ? 'price_spec' : 'price');
            }

            if($this->request->has('postal_code'))
            {
                $postCode = $postCodeRepository->find($this->request->input('postal_code'));
                if($postCode->code_covered)
                {
                    $method = $this->request->input('payment');

                } else {
                    $method = 7;
                }
            } else {
                $method = $this->request->input('payment');
            }

            $order = $orderRepository->create([
                'name'       => $this->request->input('last_name') ? $this->request->input('first_name') . ' ' . $this->request->input('last_name') : $this->request->input('first_name'),
                'first_name' => $this->request->input('first_name'),
                'last_name'  => $this->request->input('last_name') ? $this->request->input('last_name') : '',
                'email'      => $this->request->input('email'),
                'phone'      => $this->request->input('phone'),
                'size'       => $this->request->input('size') ? $this->request->input('size') : '',
                'age'        => $this->request->input('age'),
                'sex'        => $this->request->input('sex'),
                'other_lang' => $this->request->input('other_lang'),
                'notes'      => $this->request->input('notes'),
                'comment'    => $this->request->input('comment'),
                'method'     => $method,
                'nocod'      => $this->request->input('nocod', 0),
                'token'      => $token,
                'lang'       => LaravelLocalization::getCurrentLocale(),
                'price'      => $sum,
                'token_id'   => $this->request->input('token_id', null),
                'version'    => $this->request->input('version', 1),
                'to_confirm' => $this->request->input('to_confirm', 0),
                'bank_transfer' => $this->request->input('bank_transfer', 0),
                'unique_number' => 0,
            ]);

            if (is_numeric($this->contact_id))
            {
                $contact = $contactRepository->find($this->contact_id);
                if ($contact)
                {
                    $order->contact()->associate($contact);
                }
            }

            if ($user = $this->request->user())
            {
                $order->user()->associate($user);
            }

            if ($order->method == 2 || $order->method == 7)
            {
                $districtId = $this->request->input('district_id');
                if($districtId){
                   $district = $districtRepository->find($districtId);
                }
                $subDistrictId = $this->request->input('subdistrict');
                if($subDistrictId){
                    $subDistrict = $subDistrictRepository->find($subDistrictId);
                }
                $order->address()->create([
                    'full_name'   => $this->request->input('last_name') ? $this->request->input('first_name').' '.$this->request->input('last_name') : $this->request->input('first_name'),
                    'first_name'  => $this->request->input('first_name'),
                    'last_name'   => $this->request->input('last_name') ? $this->request->input('last_name') : $this->request->input('first_name'),
                    'postal_code' => $this->request->input('postal_code'),
                    'address'     => $this->request->input('address'),
                    'province'    => isset($district) ? $district->city->province->name : null,
                    'city'        => isset($district) ? $district->city->name : null,
                    'district_id' => $districtId,
                    'subdistrict' => isset($subDistrict) ? $subDistrict->name : null,
                    'sub_district_id' => $this->request->input('subdistrict')
                ]);

            }

            if (is_null($this->custom_languages))
            {
                foreach ($languages as $language)
                {
                    $order->languages()->create([
                        'language_id' => $language->id,
                        'price'       => $language->{$this->is_promotion ? $this->request->cookie('special') ? 'price_spec' : 'price' : 'price_old'},
                    ]);
                }
            }
            else
            {
                foreach ($this->custom_languages as $language)
                {
                    $order->languages()->create([
                        'language_id' => $language['id'],
                        'price'       => $this->request->cookie('special') ? $language['price_spec'] : $language['price'],
                        'qty'         => isset($language['qty']) ? (int)$language['qty'] : 1,
                    ]);
                }
            }

            $order->price = $order->summary_price;

            if (Auth::check())
            {
                $order->ref = 'CM' . $order->id . (Auth::user()->hasRole('consultant') ? '-' . Auth::user()->signature : '');
            }/* else {
                $order->ref = 'CM' . $order->id;
            }*/

            if($this->request->input('bank_transfer')==1){
                $order->method = 7;
            }

            if($order->method==7)
            {
                $unique = $bankTransferUniqueRepository->first();
                if($unique)
                {
                    $unique->unique_number = $unique->unique_number===999 ? 1 : $unique->unique_number + 1;
                    $unique->save();
                    $unique_number = $unique->unique_number;
                }
                else
                {
                    $unique_number = 1;
                    $bankTransferUniqueRepository->create([
                        'unique_number' => $unique_number
                    ]);
                }
                $order->unique_number = $unique_number;
            }

            $order->save();

            DB::commit();

            if($order->ref)
            {
                ZenzivaSms::smsConfirm($order);

                if($order->method == 2)
                {
                    $this->orderMailer->codOrderMade($order);
                }
                if($order->method != 2)
                {
                    $this->orderMailer->noncodOrderMade($order);
                }
            }

        } catch (\Exception $e)
        {
            DB::rollBack();
            throw new PageException($e->getMessage(), $e->getCode());
        }

        return $order;
    }
}
