<?php

namespace App\Jobs;

use App\Contact;
use App\Jobs\Job;
use App\Repositories\LanguageRepository;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class SendContactReminder
 * @package App\Jobs
 */
class SendContactReminder extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Contact
     */
    protected $contact;

    /**
     * @var bool
     */
    protected $reminded;


    /**
     * @param Contact $contact
     * @param bool|false $repeated
     */
    public function __construct(Contact $contact, $repeated = false)
    {
        $this->contact = $contact;
        $this->reminded = $repeated;
    }


    /**
     * @param LanguageRepository $languageRepository
     * @return bool
     * @throws \Exception
     */
    public function handle(LanguageRepository $languageRepository)
    {
        switch ($this->contact->type)
        {
            case 1:
            case 2:
                $title = trans('emails.contact.data.title');
                $template = 'email.contact_data';
                break;
            case 3:

                switch ($this->contact->reminded)
                {
                    case 0:
                        $title = trans('emails.contact.try.lesson.title_1', ['name' => $this->contact->name]);
                        $template = 'email.contact_try_lesson_1';
                        break;
                    case 1:
                        $title = trans('emails.contact.try.lesson.title_2', ['name' => $this->contact->name]);
                        $template = 'email.contact_try_lesson_2';
                        break;
                    case 2:
                        $title = trans('emails.contact.try.lesson.title_3');
                        $template = 'email.contact_try_lesson_3';
                        break;
                    default:
                        return false;
                }

                break;
            default:
                throw new \Exception('no reminder defined');
        }

        if ($this->reminded === false)
        {
            $this->dispatch(
                new SendEmail($this->contact->email, $title, $template, ['contact' => $this->contact, 'language' => $languageRepository->active()->first()])
            );
        }

        $this->contact->reminded++;
        $this->contact->save();

        return true;
    }
}
