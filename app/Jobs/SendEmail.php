<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Support\Facades\Config;

/**
 * Class SendEmail
 * @package App\Jobs
 */
class SendEmail extends Job implements SelfHandling {

    use SerializesModels;

    /**
     * @var
     */
    protected $template;
    /**
     * @var
     */
    protected $data;
    /**
     * @var
     */
    protected $email;
    /**
     * @var
     */
    protected $subject;

    /**
     * @var array
     */
    protected $options;

    /**
     * @param $email
     * @param $subject
     * @param $template
     * @param array $data
     * @param array $options
     */
    public function __construct($email, $subject, $template, $data = [], $options = [])
    {
        $this->email = $email;
        $this->subject = $subject;
        $this->template = $template;
        $this->data = $data;
        $this->options = $options;
    }


    /**
     * @param Mailer $mailer
     */
    public function handle(Mailer $mailer)
    {
        if (isset($this->options['raw']) && $this->options['raw'] == true)
        {
            return $this->plain($mailer);
        }
        else
        {
            return $this->template($mailer);
        }
    }

    /**
     * @param $message
     */
    private function setHeaders($message)
    {
        if (Config::get('mail.driver') == 'mailgun')
        {
            $swiftMessage = $message->getSwiftMessage();
            $headers = $swiftMessage->getHeaders();
            $headers->addTextHeader('x-mailgun-native-send', 'true');
        }
    }

    /**
     * @param Mailer $mailer
     */
    protected function plain(Mailer $mailer)
    {
        $mailer->raw($this->template, function ($message)
        {
            $message->to($this->email)->subject($this->subject);
            $this->setHeaders($message);
        });
    }

    /**
     * @param Mailer $mailer
     */
    protected function template(Mailer $mailer)
    {
        $mailer->send($this->template, $this->data, function ($message)
        {
            $message->to($this->email)->subject($this->subject);
            $this->setHeaders($message);
        });
    }
}
