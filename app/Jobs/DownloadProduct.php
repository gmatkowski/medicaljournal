<?php

namespace App\Jobs;

use App\Exceptions\PageException;
use App\Jobs\Job;
use App\Order;
use Illuminate\Contracts\Bus\SelfHandling;
use Config;
use Carbon\Carbon;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

/**
 * Class DownloadProduct
 * @package App\Jobs
 */
class DownloadProduct extends Job implements SelfHandling {

    use DispatchesJobs;

    /**
     * @var Order
     */
    protected $order;
    /**
     * @var
     */
    protected $hash;

    /**
     * @param Order $order
     * @param $hash
     */
    public function __construct(Order $order, $hash, Request $request)
    {
        $this->order = $order;
        $this->hash = $hash;
        $this->request = $request;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     * @throws PageException
     */
    public function handle()
    {

        if (Carbon::parse($this->order->created_at)->timestamp < Carbon::now()->subDays(Config::get('order.timeout'))->timestamp)
        {
//            Log::info('Order date is invalid', ['id' => $this->order->id]);
            throw new PageException(trans('order.download.timeout'));
        }

        if (is_null($this->order->download_ip))
        {
            $this->order->download_ip = $this->request->ip();
        }
        elseif ($this->order->download_ip != $this->request->ip())
        {
            //Log::info('Order ip is invalid', ['id' => $this->order->id, 'ip' => $this->request->ip(), 'order_id' => $this->order->download_ip]);
            //throw new PageException(trans('order.download.invalid_ip'));
        }


        if ($this->hash == 'all')
        {
            $filename = 'order-' . $this->order->token . '-all.zip';
            $file_path = storage_path('app/languages/' . $filename);
            if (!is_file($file_path))
            {
                $files = collect();

                foreach ($this->order->files as $file)
                {
                    $files->push($file->file->file_path);
                }

                $this->dispatch(
                    new CreateZipFile($files->toArray(), $file_path)
                );
            }

            $this->order->downloads_full++;
        }
        else
        {

            $file = $this->order->files->where('hash', $this->hash)->first();
            if (!$file)
            {
                Log::info('Order file hash id is invalid', ['id' => $this->order->id, 'hash' => $this->hash]);
                throw new PageException(trans('order.download.incorrect.url'));
            }

            $file_path = $file->file->file_path;
            $filename = 'order-' . $this->order->token . '-part-' . $file->file->part . '.zip';

            $file->downloads++;
            $file->save();
        }

        $this->order->status = 3;
        $this->order->downloads++;
        $this->order->save();
        ob_end_clean();

        return response()->download($file_path, $filename);
    }
}
