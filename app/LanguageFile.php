<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class LanguageFile
 * @package App
 */
class LanguageFile extends Model implements Transformable {

    use TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'language_files';

    /**
     * @var array
     */
    protected $fillable = [
        'file', 'part'
    ];

    /**
     * @var array
     */
    protected $appends = [
        'file_path'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language');
    }

    /**
     * @return string
     */
    public function getFilePathAttribute()
    {
        return storage_path('app/languages/' . $this->language_id . '/' . $this->file);
    }
}
