<?php

namespace App\Helpers;

use App\Repositories\OrderRepository;
use App\Repositories\OrderInvoiceRepository;
use Carbon\Carbon;

class InvoiceHelper
{
    /**
     * @var OrderRepository
     */
    protected $orders;

    /**
     * @var OrderInvoiceRepository
     */
    protected $invoices;

    public function __construct(
        OrderRepository $orderRepository,
        OrderInvoiceRepository $orderInvoiceRepository
    )
    {
        $this->orders = $orderRepository;
        $this->invoices = $orderInvoiceRepository;
    }

    public function create($id, $invoiceType)
    {
        $order =$this->orders->with('address')->find($id);

        $price = $order->price;
        $price_before_vat = round($price/1.10, 2);
        $vat = $price-$price_before_vat;

        if(isset($order->address) && $order->isCOD() && !is_null($order->address->district_id)){
            if(isset($order->address->address)){
                $address = $order->address->address.', '.$order->address->address_full_state;
            } else {
                $address = $order->address->address_full_state;
            }
        } else {
            $address = '';
        }

        $numbers = InvoiceNumberHelper::getNumbers($this->invoices, $order, $invoiceType);

        switch ($invoiceType) {
            case 'cancel':
                $title = 'Credit Note';
                $invoice_number_title = 'Credit Note';
                $invoice_number='CN'.$numbers['credit_note_number'];
                $invoice_date_label = 'Credit Note Issue';
                $price=-$price;
                $price_before_vat=-$price_before_vat;
                $vat=-$vat;
                break;
            case 'get':
                $title = 'RECEIPT / TAX INVOICE';
                $invoice_number_title = 'Invoice';
                $invoice_number='ID/DCI/'.$order->getInvoicePrefix().'/'.$numbers['invoice_number'];
                $invoice_date_label = 'Invoice';
                break;
        }

        $data = [
            'title' => $title,
            'name_to' => $order->first_name,
            'surname_to' => $order->last_name,
            'address_to' => $address,
            'phone_to' => $order->phone,
            'email_to' => $order->email,
//            'tax_id' => '0105559148597',
            'order_date' => $order->created_at_formated->format('d/m/y'),
            'order_number' => $order->id,
            'invoice_number_title' => $invoice_number_title,
            'invoice_number' => $invoice_number,
            'invoice_date_label' => $invoice_date_label,
            'invoice_date' => Carbon::now(),
            'price_before_vat_html' => trans('common.currency.symbol', [], null, $order->lang).'<br>'.$price_before_vat,
            'vat_html' =>trans('common.currency.symbol', [], null, $order->lang).'<br>'.$vat,
            'total_price_html' => trans('common.currency.symbol', [], null, $order->lang).'<br>'.$price,
            'orders' => $order->languages,
            'price_before_vat' => $price_before_vat,
            'vat' => $vat,
            'total_price' => $price,
        ];

        return $data;
    }
}
