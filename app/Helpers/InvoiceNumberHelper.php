<?php

namespace App\Helpers;

use App\Order;
use App\Repositories\OrderInvoiceRepository;

class InvoiceNumberHelper
{
    public static function getNumbers(OrderInvoiceRepository $orderInvoiceRepository, Order $order, $invoiceType)
    {
        $lastNumbers = $orderInvoiceRepository->findWhere([['order_id', '=', NULL]])->first();
        $orderInvoice = $orderInvoiceRepository->findByField('order_id', $order->id)->first();
        if($orderInvoice){

            if(!isset($orderInvoice->credit_no) && $invoiceType=='cancel'){
                $lastNumbers->credit_no++;
                $orderInvoice->credit_no = $lastNumbers->credit_no;
            }

            if(!isset($orderInvoice->invoice_no) && $invoiceType=='get'){
                $lastNumbers->invoice_no++;
                $orderInvoice->invoice_no = $lastNumbers->invoice_no;
            }

            $lastNumbers->save();
            $orderInvoice->save();

            $credit_note_number = $orderInvoice->credit_no;
            $invoice_number = $orderInvoice->invoice_no;
            $invoice_date = $orderInvoice->created_at;

        } else {

            switch ($invoiceType) {
                case 'cancel':
                    $lastNumbers->credit_no++;
                    $credit_note_number = $lastNumbers->credit_no;
                    break;
                case 'get':
                    $lastNumbers->invoice_no++;
                    $invoice_number = $lastNumbers->invoice_no;
                    break;
            }

            $lastNumbers->save();

            $invoice_number = isset($invoice_number) ? $invoice_number : null;
            $credit_note_number = isset($credit_note_number) ? $credit_note_number : null;

            $orderInvoice = $orderInvoiceRepository->create([
                'order_id'   => $order->id,
                'credit_no'  => $credit_note_number,
                'invoice_no' => $invoice_number
            ]);
            $invoice_date = $orderInvoice->created_at;
        }

        $numbers=self::addLeadingZeroes($invoice_number, $credit_note_number);

        return [
            'invoice_number' => $numbers['invoice_number'],
            'credit_note_number' => $numbers['credit_note_number'],
            'invoice_date' => $invoice_date
        ];
    }

    private static function addLeadingZeroes($invoice_number, $credit_note_number)
    {
        $numbers=[
            'invoice_number' => $invoice_number,
            'credit_note_number' => $credit_note_number
        ];
        foreach($numbers as $number_name => $number){
            $number_length = strlen((string)$number);
            if($number_length<5){
                $how_many_zeroes = 3 - $number_length;
                for ($i = 0; $i < $how_many_zeroes; $i++) {
                    $numbers[$number_name]='0'.$numbers[$number_name];
                }
            }
        }

        return $numbers;
    }
}
