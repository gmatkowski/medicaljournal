<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

/**
 * Class Record
 * @package App
 */
class Record extends Model implements Transformable {

    use Translatable, TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'records';

    /**
     * @var array
     */
    protected $translatedAttributes = ['name', 'record'];

    /**
     * @var array
     */
    protected $fillable = ['name', 'record', 'language_id'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated', 'record_path'];

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        return Carbon::parse($this->created_at);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language')->with(['translations']);
    }

    /**
     * @return string
     */
    public function getRecordPathAttribute()
    {
        return asset('records/' . $this->record);
    }

}
