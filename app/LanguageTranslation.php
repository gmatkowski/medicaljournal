<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @package App
 */
class LanguageTranslation extends Model {

    /**
     * @var string
     */
    public $table = 'language_translations';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public $fillable = ['name', 'description_short', 'price', 'price_old', 'price_spec', 'currency', 'description_try'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function language()
    {
        return $this->belongsTo('App\Language', 'language_id');
    }

}
