<?php namespace App\Observers;

use Cache;

/**
 * Class LanguageObserver
 * @package App\Observers
 */
class LanguageObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['languages::list', 'language::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['languages::list', 'language::' . $model->id])->flush();
    }
}