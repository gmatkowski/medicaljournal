<?php namespace App\Observers;

use Cache;

/**
 * Class LessonObserver
 * @package App\Observers
 */
class HeaderObserver
{


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['headers::list', 'header::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['headers::list', 'header::' . $model->id])->flush();
    }
}