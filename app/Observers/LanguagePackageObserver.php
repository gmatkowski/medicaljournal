<?php namespace App\Observers;

use Cache;

/**
 * Class LanguageObserver
 * @package App\Observers
 */
class LanguagePackageObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['languages::packages::list', 'language::package::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['languages::packages::list', 'language::package::' . $model->id])->flush();
    }
}