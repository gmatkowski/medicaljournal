<?php namespace App\Observers;

use Cache;

/**
 * Class SettingObserver
 * @package App\Observers
 */
class SettingObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['settings', 'setting::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['settings', 'setting::' . $model->id])->flush();
    }
}