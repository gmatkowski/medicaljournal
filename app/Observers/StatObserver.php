<?php namespace App\Observers;

use Cache;

/**
 * Class StatObserver
 * @package App\Observers
 */
class StatObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['stats::list', 'stat::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['stats::list', 'stat::' . $model->id])->flush();
    }
}