<?php namespace App\Observers;

use App\Call\NetTelLists;
use App\Events\OrderWasCreated;
use App\Events\OrderWasPayed;
use Carbon\Carbon;
use Config;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Event;
use Illuminate\Support\Facades\Log;

/**
 * Class OrderObserver
 * @package App\Observers
 */
class OrderObserver
{
    use DispatchesJobs;

    private $orderMailer;

    public function __construct()
    {
        $this->orderMailer = app('App\Mail\OrderMailer');
    }

    public function created($model)
    {
         /*
        if (!$model->isCOD())
        {
            try
            {
                $model->called = true;
                $model->save();

                $callCenter = app('App\Call\CallCenter');
                $response = $callCenter->send([
                    'list_id'  => NetTelLists::$PENDING_ORDER,
                    'imie'     => $model->first_name,
                    'nazwisko' => $model->last_name,
                    'email'    => $model->email,
                    'telefon1' => $model->phone
                ]);

                if (isset($response->status) && $response->status == 'error')
                {
                    throw new \Exception($response->reason);
                }

            } catch (\Exception $e)
            {
                Log::notice('CallCenter (Creating) Error:', ['message' => $e->getMessage()]);
            }
        }
        */
    }

    /**
     * @param $model
     */
    public function saved($model)
    {
        if($model->isDirty('status')) {
            $sent = Carbon::parse($model->sended_at);
            $minAgo = Carbon::now()->subMinute();
            $now = Carbon::now();
            if(!is_null($model->sended_at) && $sent->between($minAgo, $now)) {
                Log::notice('Firing OrderWasPayed stopped - links were sent less than a minute ago! Order:'.$model->id);
                return;
            }

            if($model->status == 1 && $model->version == 1 && in_array($model->method, [1, 3, 5, 6])) {
                Event::fire(new OrderWasPayed($model));
            }

            if($model->status == 7 && $model->method == 2)
            {
                $this->orderMailer->codOrderDelivered($model);
            }

            if($model->status == 7 && $model->method != 2)
            {
                $this->orderMailer->noncodOrderDelivered($model);
            }
        }

        if($model->isDirty('done') && $model->is_done == true) {
            Event::fire(new OrderWasCreated($model));
        }

    }

    /**
     * @param $model
     */
    public function deleted($model)
    {

    }

}