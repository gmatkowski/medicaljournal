<?php namespace App\Observers;

use Cache;

/**
 * Class JobObserver
 * @package App\Observers
 */
class JobObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['jobs::list', 'job::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['jobs::list', 'job::' . $model->id])->flush();
    }
}