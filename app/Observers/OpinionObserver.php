<?php namespace App\Observers;

use Cache;

/**
 * Class JobObserver
 * @package App\Observers
 */
class OpinionObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['opinions::list', 'opinions::status' . $model->main, 'opinion::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['opinions::list', 'opinions::status' . $model->main, 'opinion::' . $model->id])->flush();
    }
}