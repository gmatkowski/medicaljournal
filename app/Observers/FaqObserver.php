<?php namespace App\Observers;

use Cache;

/**
 * Class LessonObserver
 * @package App\Observers
 */
class FaqObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['faqs::list', 'faq::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['faqs::list', 'faq::' . $model->id])->flush();
    }
}