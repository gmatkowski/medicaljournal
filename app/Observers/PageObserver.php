<?php namespace App\Observers;

use Cache;

/**
 * Class PageObserver
 * @package App\Observers
 */
class PageObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['pages::list', 'page::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['pages::list', 'page::' . $model->id])->flush();
    }
}