<?php namespace App\Observers;

use App\Events\ContactWasCreated;
use App\Sms\ZenzivaSms;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Event;

/**
 * Class StatusObserver
 * @package App\Observers
 */
class ContactObserver
{
    use DispatchesJobs;

    private $orderMailer;

    public function __construct()
    {
        $this->orderMailer = app('App\Mail\OrderMailer');
    }

    public function created($model)
    {
        if($model->phone!='1234567890')
        {
            Event::fire(new ContactWasCreated($model));
            ZenzivaSms::smsAlpha($model);
            if($model->product=='cambogia')
            {
                $this->orderMailer->contactMade($model);
            }
        }
    }

    /**
     * @param $model
     */
    public function saved($model)
    {

    }

    /**
     * @param $model
     */
    public function deleted($model)
    {

    }

}