<?php namespace App\Observers;

use Cache;

/**
 * Class LessonObserver
 * @package App\Observers
 */
class LessonObserver {


    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['lessons::list', 'lesson::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['lessons::list', 'lesson::' . $model->id])->flush();
    }
}