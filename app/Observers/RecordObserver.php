<?php namespace App\Observers;

use Cache;

/**
 * Class RecordObserver
 * @package App\Observers
 */
class RecordObserver {

    /**
     * @param $model
     */
    public function saved($model)
    {
        Cache::tags(['records::list', 'record::' . $model->id])->flush();
    }

    /**
     * @param $model
     */
    public function deleted($model)
    {
        Cache::tags(['records::list', 'record::' . $model->id])->flush();
    }
}