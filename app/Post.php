<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Carbon\Carbon;

class Post extends Model implements Transformable
{
    use Translatable, TransformableTrait;

    /**
     * @var string
     */
    protected $table = 'posts';

    /**
     * @var array
     */
    protected $translatedAttributes = ['title', 'article'];

    /**
     * @var array
     */
    protected $fillable = ['title', 'article', 'author', 'icon', 'views'];

    /**
     * @var array
     */
    protected $appends = ['created_at_formated', 'icon_path', 'icon_thumb_path'];

    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
    }

    /**
     * @return static
     */
    public function getCreatedAtFormatedAttribute()
    {
        $dt = Carbon::parse($this->created_at);
        return $dt->formatLocalized('%B, %Y');
    }

    /**
     * @return string
     */
    public function getIconPathAttribute()
    {
        return asset('posts/icons/' . $this->icon);
    }

    public function getIconThumbPathAttribute()
    {
        return asset('posts/icons/thumbs/' . $this->icon);
    }

}
