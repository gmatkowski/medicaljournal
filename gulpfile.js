var elixir = require('laravel-elixir');
var del = require('del');
var gulp = require('gulp');
var runSequence = require('run-sequence');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task('del', function () {
    return del(['public/build/**']);
});

gulp.task('copy', function () {
    return elixir(function (mix) {
        mix
            .copy("resources/assets/js/**", "public/build/js/vendor")
            .copy("resources/assets/images/**", "public/build/images")
            .copy("resources/assets/ext/**", "public/build/ext");
    });
});

gulp.task('merge', function () {
    return elixir(function (mix) {
        mix
            .scripts(['html5support/html5shiv.min.js', 'html5support/respond.min.js'
            ], 'public/js/html5support')

            .styles(['frontend/reset.css', 'frontend/medicreporter-fonts.css', 'frontend/medicreporter.css', 'frontend/cbb.css', 'frontend/jquery.modal.css',
            ], 'public/css/frontend')

            .scripts(['frontend/*.js'
            ], 'public/js/frontend')

            .styles(['frontend/reset.css', 'frontend/medicreporter-fonts.css', 'frontend/penirium.css', 'frontend/penirium-cbb.css', 'frontend/jquery.modal.css'
            ], 'public/css/penirium')

            .styles(['frontend/reset.css', 'frontend/fonts.css', 'frontend/odchudzanie-bez-JOJO.css', 'frontend/jquery.modal.css', 'frontend/normalize.css', 'frontend/bootstrap.css', 'frontend/garcinia-cambogia.css'
            ], 'public/css/pre')

            // .styles(['frontend/reset.css', 'frontend/fonts.css', 'frontend/odchudzanie-bez-JOJO.css', 'frontend/order.css'
            // ], 'public/css/order')

            .styles(['frontend/blackMask.css'
            ], 'public/css/bmask')

            .styles(['frontend/ComebackerAlert.css', 'frontend/css_002.css', 'frontend/app.css', 'frontend/css.css', 'frontend/bootstrap.css', 'frontend/my.css'
            ], 'public/css/bmask2')

            .styles(['blackmask-confirm/*.css'
            ], 'public/css/blackmask-confirm')

            .styles(['maskerHitam2/*.css'
            ], 'public/css/maskerHitam2')

            .styles(['frontend/bootstrap.css', 'frontend/normalize.css', 'rahasia/custom.css'
            ], 'public/css/rahasia')

            .styles(['sensoduo/fbstyle.css', 'sensoduo/font.css', 'sensoduo/index.css', 'sensoduo/nstyle.css', 'sensoduo/other.css', 'sensoduo/styles-extra.css'
            ], 'public/css/sensoduo')

            .styles(['frontend/bootstrap.css', 'teknologi/custom.css'
            ], 'public/css/teknologi')

            .styles(['frontend/bootstrap.css', 'cinta/custom.css'
            ], 'public/css/cinta')

            .styles(['frontend/bootstrap.css', 'terobosan/custom.css'
            ], 'public/css/terobosan')

            .styles(['perkelahian/*.css'
            ], 'public/css/perkelahian')

            .styles(['garcinia/*.css'
            ], 'public/css/garcinia')

            .styles(['confirm/*.css'
            ], 'public/css/confirm')

            .styles(['garcinia-confirm/*.css'
            ], 'public/css/garcinia-confirm')

            .styles(['garcinia-checkout/*.css'
            ], 'public/css/garcinia-checkout')

            .styles(['hallupro-checkout/*.css'
            ], 'public/css/hallupro-checkout')

            .styles(['prehallupro/*.css',
            ], 'public/css/prehallupro')

            .styles(['hallupro/*.css'
            ], 'public/css/hallupro')

            .styles(['hallupro2/*.css'
            ], 'public/css/hallupro2')

            .styles(['hallupro-confirm/*.css'
            ], 'public/css/hallupro-confirm')

            .styles(['aynish/*.css'
            ], 'public/css/aynish')

            .styles(['amerika/*.css'
            ], 'public/css/amerika')

            .styles(['bukuhariansaya/*.css'
            ], 'public/css/bukuhariansaya')

            .styles(['garcinia1/custom.css', 'garcinia1/jquery.pnotify.default.css', 'garcinia1/styles.css', 'garcinia1/rwd.css'
            ], 'public/css/garcinia1')

            .styles(['garcinia2/*.css'
            ], 'public/css/garcinia2')

            .styles(['garciniaCambogia2/*.css'
            ], 'public/css/garciniaCambogia2')

            .styles(['garciniaCambogia2popup/*.css'
            ], 'public/css/garciniaCambogia2popup')

            .styles(['garcinia2-mobile/*.css'
            ], 'public/css/garcinia2-mobile')

            .styles(['garcinia3/custom.css', 'garcinia3/order_me.css', 'garcinia3/production.css-201607261908.css', 'garcinia3/media.css'
            ], 'public/css/garcinia3')

            .styles(['dietAlaSelebriti/*.css'
            ], 'public/css/dietAlaSelebriti')

            .scripts(['frontend/bmask/menu_toggle.js', 'frontend/bmask/jquery_002.js', 'frontend/bmask/ComebackerAlert.js', 'frontend/bmask/plugins.js', 'frontend/bmask/detect.js', 'frontend/bmask/jquery-ui.js', 'frontend/bmask/home.js', 'frontend/bmask/jquery_003.js', 'frontend/bmask/main.js'
            ], 'public/js/bmask2')

            .styles(['admin/**/*.css'
            ], 'public/css/admin')

            .scripts(['admin/bootstrap.min.js', 'admin/chart.min.js', 'admin/bootstrap-progressbar.min.js', 'admin/jquery.nicescroll.min.js', 'admin/icheck.min.js', 'admin/bootbox.min.js', 'admin/bootstrap-wysiwyg.js', 'admin/jquery.hotkeys.js', 'admin/prettify.js', 'admin/summernote.min.js', 'admin/select2.full.js', 'admin/bootstrap-editable.min.js', 'admin/custom.js', 'admin/chart.js'
            ], 'public/js/admin')

            .scripts(['jquery.min.js', 'frontend/typPopups.js'
            ], 'public/js/typ')

            .sass([
                'typ/app.scss'
            ], 'public/css/typ')

            .version([
                'js/html5support/all.js',
                'css/frontend/all.css',
                'js/frontend/all.js',
                'css/penirium/all.css',
                'css/pre/all.css',
                'css/bmask/all.css',
                'css/bmask2/all.css',
                'css/blackmask-confirm/all.css',
                'css/maskerHitam2/all.css',
                'css/rahasia/all.css',
                'css/sensoduo/all.css',
                'css/teknologi/all.css',
                'css/cinta/all.css',
                'css/terobosan/all.css',
                'css/perkelahian/all.css',
                'css/garcinia/all.css',
                'css/confirm/all.css',
                'css/garcinia-confirm/all.css',
                'css/garcinia-checkout/all.css',
                'css/hallupro-checkout/all.css',
                'css/prehallupro/all.css',
                'css/hallupro/all.css',
                'css/hallupro2/all.css',
                'css/hallupro-confirm/all.css',
                'css/aynish/all.css',
                'css/amerika/all.css',
                'css/bukuhariansaya/all.css',
                'css/garcinia1/all.css',
                'css/garcinia2/all.css',
                'css/garciniaCambogia2/all.css',
                'css/garciniaCambogia2popup/all.css',
                'css/garcinia2-mobile/all.css',
                'css/garcinia3/all.css',
                'css/dietAlaSelebriti/all.css',
                'js/bmask2/all.js',
                // 'css/order/all.css',
                'css/admin/all.css',
                'js/admin/all.js',
                'js/typ/all.js'
            ]);
    });
});

gulp.task('default', function () {
    runSequence('del', 'copy', 'merge');
});
