<?php return [
    'key'      => env('AFFILATOR_KEY'),
    'url'      => env('AFFILATOR_API_URL'),
    'lifetime' => [
        'hours'   => 0,
        'minutes' => 30
    ],
    'models'   => [
        'contact' => \App\Contact::class
    ]
];