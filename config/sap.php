<?php return [
    'garciiUsername'      => env('SAP_GARCII_USERNAME'),
    'garciiPassword'      => env('SAP_GARCII_PASSWORD'),
    'blackUsername'      => env('SAP_BLACK_USERNAME'),
    'blackPassword'      => env('SAP_BLACK_PASSWORD')
];