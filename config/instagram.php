<?php

/*
 * This file is part of Laravel Instagram.
 *
 * (c) Vincent Klaiber <hello@vinkla.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default'     => 'main',

    /*
    |--------------------------------------------------------------------------
    | Instagram Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main'        => [
            'client_id'     => '1d154a8d075c47eb8f7e581fff3c4757',
            'client_secret' => '1f690e289a4048bca7f93fe3a6ebf38e',
            'callback_url'  => 'http://krebs-method.eu/',
            'access_token'  => '2284112270.1d154a8.fc802abb9aeb438499d64e14f7ca0545'
        ],

        'alternative' => [
            'client_id'     => 'your-client-id',
            'client_secret' => null,
            'callback_url'  => null,
        ],

    ],

];
