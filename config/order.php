<?php return [
    'limit'   => 10,
    'timeout' => 14,
    'from'    => 'Kortiga-Harga',
    'address' => '795 Freedom Ave, Suite 600',
    'city'    => 'New York, CA 94107',
    'phone'   => '1 (804) 123-9876',
    'email'   => 'info@ironadmin.com'
];