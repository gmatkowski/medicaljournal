<?php return [
    'merchant_id' => env('VERITRANS_MERCHANT_ID'),
    'client_key'  => env('VERITRANS_CLIENT_KEY'),
    'server_key'  => env('VERITRANS_SERVER_KEY'),
    'vt_web'      => env('VERITRANS_VT_WEB', true),
    'production'  => env('PAYMENT_PRODUCTION_MODE', false),
    'price'       => 1250000
];