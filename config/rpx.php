<?php return [
    'username'          => env('RPX_USERNAME'),
    'password'          => env('RPX_PASSWORD'),
    'shipper_id'        => env('RPX_SHIPPER_ID'),
    'sandboxUsername'   => env('RPX_SANDBOX_USERNAME'),
    'sandboxPassword'   => env('RPX_SANDBOX_PASSWORD'),
    'sandboxShipper_id' => env('RPX_SANDBOX_SHIPPER_ID')
];