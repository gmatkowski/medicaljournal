<?php return [
    'username'     => env('ACOMMERCE_USERNAME'),
    'apiKey'       => env('ACOMMERCE_API_KEY'),
    'channel'      => env('ACOMMERCE_CHANNEL'),
    'partner_id'   => env('ACOMMERCE_PARTNER_ID'),
    'auth_url'     => env('ACOMMERCE_AUTH_URL'),
    'shipping_url' => env('ACOMMERCE_SHIPPING_URL'),
    'sales_url'    => env('ACOMMERCE_SALES_URL')
];