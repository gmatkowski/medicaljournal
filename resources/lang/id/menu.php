<?php return [
    'homepage'  => 'Beranda',
    'records'   => 'Rekaman',
    'lessons'   => 'Materi',
    'languages' => 'Bahasa',
    'opinions'  => 'Opini',
    'contact'   => 'Kontak',
    'terms'     => 'Istilah',
    'try'       => 'Coba sekarang',
    'buy-now'   => 'Beli sekarang',
    'blog'      => 'Blog',
];