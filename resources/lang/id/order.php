<?php return [
    'buy.payment.title'       => 'Pembelian Kursus Bahasa',
    'email.payed.title'       => 'Tautan ke kursus yang sudah dibeli',
    'email.created.title'     => 'Terima Kasih atas Pembelian Anda',
    'email.created.cod.title' => 'Kortiga-Harga: Pemesanan Anda diterima',
    'created.email.content'   => 'Isi email tentang ucapan terimakasih.',
    'click.to.download'       => 'Klik untuk mengunduh produk yang dibeli',
    'download.limit'          => 'Anda sudah melebihi batas unduhan untuk pembelian kali ini.',
    'download.timeout'        => 'Anda sudah melebihi batas waktu untuk pembelian kali ini.',
    'download.invalid_ip'     => 'Alamat IP yang tidak valid',
    'not.found'               => 'Transaksi tidak ditemukan',
    'status.message.ok'       => 'Terima Kasih. Anda akan segera menerima email berisi tautan kursus yang sudah Anda beli.',
    'status.message.error'    => 'Ada yang salah. Silahkan kontak admin bila seharusnya ini tidak terjadi..',
    'no.languages'            => 'Belum ada bahasa yang dipilih',
    'part'                    => 'kehormatan',
    'status.message.cod'      => 'Terima kasih. Pemesanan Anda telah diterima dan terpenuhi.',
    'status.message.cod.fail' => 'Anda telah melakukan pemesanan. Silahkan menghubungi Customer service kami jika menginginkan pemesanan baru. \nNo. Telp: +62 888 01000488 \nEmail: contact@medical-jurnal.com',
    'download.incorrect.url'  => 'Link ke file tidak valid'
];