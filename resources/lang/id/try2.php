<?php return [
    'lessons'   => 'Penjelasan Sampel Pelajaran',
    'discount'  => 'HANYA HARI INI!',
    'discount2' => 'Hanya hari ini!',
    'discount3' => 'Diskon',
    'discount4' => 'Kursus Bahasa asing Emil Krebs diskon - hanya',
    'discount5' => 'selain',
    'discount6' => 'Mulailah belajar Bahasa',
    'discount7' => 'Pesanlah hari ini, hematlah',
    'discount8' => 'dan terimalah gwaransi hasil yang bagus. Besok promosi sudah tidak berlaku.',
    'opinions'  => 'Pendapat siswa kami',
    'whatever'  => 'Klik pada "Mulai belajar bahasa", dan mengisi formulir di halaman berikutnya. Versi digital akan masuk ke email Anda dalam beberapa menit atau jika memesan versi CD fisik - akan dikirimkan ke rumah Anda dalam beberapa hari. Setelah itu Anda sendiri yang akan mengevaluasi.',

];