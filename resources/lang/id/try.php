<?php return [
    'name'                      => 'Nama...',
    'email'                     => 'Email ...',
    'phone'                     => 'No Telepon ...',
    'age'                       => 'Umur ...',
    'submit'                    => 'Kirim',
    'write.to.get'              => 'Isi untuk dapatkan',
    'free.lesson'               => 'Sampel Pelajaran!',
    'step.1'                    => 'Langkah 1: Pilih bahasa yang diinginkan.',
    'step.2'                    => 'Langkah 2: Dengar sample pelajaran yang dipilih.',
    'step.3'                    => 'Langkah 3: Lihat video ini untuk mengerti cara kerja kursus.',
    'text.interested'           => 'Tertarik? Ingin tahu lebih lanjut? Telepon!',
    'text.answer.all.questions' => 'Lanjutkan! Kami akan jawab setup pertanyaan!',
    'or'                        => 'atau',
    'leave.data'                => 'Isi data dan kami akan kontak Anda!'
];