<?php return [
    'levels'               => [
        1 => 'Dasar',
        2 => 'Tingkat menengah',
        3 => 'Tingkat lanjutan'
    ],
    'list'                 => 'Pilihan pelajaran',
    'congratulations'      => 'Selamat!',
    'congratulations.text' => 'Anda sudah mempelajari istilah dan frasa dasar. Mulai sekarang setiap 10 pelajaran kita akan mengulang 9 pelajaran sebelumnya. Ingat : Kunci keberhasilan adalah pengulangan seluruh kalimat dan frasa. Hanya melalui pengulangan anda mampu mengingat bahasa manapun. Keterbiasaan berperan sangat penting. Bila Anda berpikir untk mengulang salah satu pelajaran, lakukanlah. Jangan terburu-buru.',
    'are.you.interested'   => 'Tertarik?',
    'go.buy'               => 'Klik untuk lanjut ke halaman pembayaran'
];