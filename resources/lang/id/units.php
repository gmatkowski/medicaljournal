<?php

return array(
    "kedua"  => "sekunda|sekund|sekund",
    "menit"  => "minuta|minuty|minut",
    "jam"    => "godzina|godziny|godzin",
    "hari"   => "dzien|dni|dni",
    "minggu" => "tydzień|tygodni|tygodni",
    "bulan"  => "miesiąc|miesięcy|miesięcy",
    "tahun"  => "rok|lata|lata",
);
