<?php return [
    'order.pending.title'        => 'Metode Emil Krebs',
    'order.failure.title'        => 'Metode Emil Krebs',
    'contact.data.title'         => 'Metode Emil Krebs',
    'contact.try.lesson.title_1' => ':name, bagaimana sampel pelajaran Anda?',
    'contact.try.lesson.title_2' => ':name, bagaimana sampel pelajaran Anda?',
    'contact.try.lesson.title_3' => 'Kami perpanjang promo. Tapi hanya hari ini!  '
];