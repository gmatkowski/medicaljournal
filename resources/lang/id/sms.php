<?php return [
    'after.order'         => 'Selamat Anda telah membeli Kursus Metode Krebs. Silahkan lakukan pembayaran dalam 24 jam sebelum diskon berakhir. Kendala? Kontak : 85574670030',
    'almost.failed.order' => 'Ingat! Anda hanya punya 3 jam untuk membayar dan memperoleh diskon! Kendala? Kontak : 85574670030'
];