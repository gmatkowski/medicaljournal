<?php return [
    'currency.symbol'   => 'Rp',
    'current.fb.symbol' => 'IDR',
    'phone.prefix'      => 'No. Telepon',
    'phone.number'      => '+62 888 01000488',
    '404'               => 'Halaman tak tersedia',
    'method'            => 'Metode',
    'emil.krebs'        => 'Emil Krebs|Emila Krebsa',
    'go.up'             => 'UP',
    'choosen.language'  => 'Bahasa yang dipilih',
    'try.language.now'  => 'Coba pelajaran sekarang!',
    'contact.data'      => 'Detail kontak',
    'information'       => 'informasi',
    'please.wait'       => 'Silahkan tunggu ...',
    'no'                => 'No'
];