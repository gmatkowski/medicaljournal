<?php return [
    'payment'     => 'payment',
    'order'       => 'order',
    'confirm'     => 'confirm',
    'contact'     => 'contact',
    'composition' => 'composition',
    'questions'   => 'questions',
];