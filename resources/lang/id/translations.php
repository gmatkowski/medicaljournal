<?php
return [
    'password'              => 'Password',
    'signin'                => 'Sign in',
    'action_done'           => 'Langkah selesai',
    'email'                 => 'Email',
    'user_self_deleting'    => 'Anda tidak dapat menghapus akun',
    'name'                  => 'Nama Pengguna',
    'password_confirmation' => 'Konfirmasi Sandi',
    'not_the_same_password' => 'Sandi tidak cocok',
    'send'                  => 'Kirim'
];