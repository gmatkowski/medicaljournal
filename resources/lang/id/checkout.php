<?php return [
    'format'                     => 'Pilih versi kursus:',
    'format.electronic'          => 'Format digital',
    'format.electronic.delivery' => 'PENGIRIMAN DALAM 5 MENIT',
    'format.post'                => 'Versi CD',
    'format.post.delivery'       => 'PENGIRIMAN DALAM 5-6 HARI',
    'payment.method'             => 'Pilih metode pembayaran',
    'shipment.address'           => 'Masukkan alamat pengiriman',
    'payment.cc.details'         => 'Masukkan detail kartu kredit',
    'payment.transfer.details'   => 'Transfer Bank',
    'payment.mandiri.atm'        => 'Mandiri ATM',
    'bca.klikpay.text'           => 'Anda memilih pembayaran via BCA Klikpay, konfirmasi untuk melanjutkan',
    'bca.mandiri.text'           => 'Anda memilih pembayaran via Mandiri, konfirmasi untuk melanjutkan',
    'accept.rules'               => 'Dengan ini saya menyetujui aturan pembayaran untuk kursus KrebsMethod.com',
    'question.text.p1'           => 'Jika ada masalah pada penyelesaian pembayaran',
    'question.text.p2'           => 'atau pertanyaan seputar kursus – HUBUNGI KAMI!',
    'question.phone.number'      => '+62 888 01000488',
    'available'                  => 'TERSEDIA',
    'sold'                       => 'TERJUAL',
    'basic.data'                 => 'Data utama',
    'order.details'              => 'Detail pesanan',
    'curse.name'                 => 'Nama kursus',
    'price'                      => 'Harga',
    'order.final.details'        => 'Ringkasan pesanan',
    'shipment'                   => 'Pengiriman',
    'gratis'                     => 'Gratis',
    'cc.number'                  => 'Nomor Kartu',
    'cc.owner.name'              => 'Nama di Kartu',
    'cc.year'                    => 'YYYY',
    'cc.month'                   => 'MM',
    'cc.ccv'                     => 'CCV / CVV',
    'total'                      => 'Total',
    'subtotal'                   => 'Subtotal',
    'last.bought'                => 'Pembelian terakhir',
    'empty.value'                => '-- Memilih --',
    'from'                       => 'dari',
    'buyers'                     => implode('|', [
        'Ibu Syifa',
        'Pak Denny',
        'Ibu Dewi',
        'Ibu Vania',
        'Pak Gilang',
        'Pak Ansel',
        'Ami Shintall',
        'Elsa Rahma',
        'Fadhly Saury',
        'Ardy Firansya',
        'Fidria Usman',
        'Cut Meyriska',
        'Dony Fit',
        'Jhonn Nugroho',
        'Gerri Chia',
        'Reyka Geovani',
        'Suano Ingka',
        'Alya Morin',
        'Rissa Lesbom',
        'Deavy Heart',
        'Lutfy Dryan',
        'Dinda Dewi',
        'Tonky Pirlo',
        'Ari Tiger',
        'Desy Sastra',
        'Suningsih Neng',
        'Aditya Indrawan',
        'Ono Zifanalia',
        'Cimenk Bang Jono',
        'Sayuti Gopad',
        'Daryaman Munthe',
        'Herman Jalli',
        'Mirai Nakahira',

    ]),
    'cities'                     => implode('|', [
        'Jakarta',
        'Surabaya',
        'Bandung',
        'Bekasi',
        'Medan',
        'Batam',
        'Makassar',
        'Depok',
        'Padang',
        'Malang',
        'Denpasar',
        'Sumarinda',
        'Balikpapan',
        'Bogor',
    ]),
    'products'                   => implode('|', [
        0 => 'Bahasa Inggris'
    ]),
    'mandiri.card.number'        => 'Numor Kartu...',
    'mandiri.token'              => 'Challenge Token',
    'mandiri.atm.head.text'      => 'Mandiri Bill Payment',
    'mandiri.atm.p.text'         => 'Silahkan melakukan pembayaran melalui Mandiri echannel dengan data berikut',
    'bank.head.text'             => 'Bank Transfer',
    'bank.p.text'                => 'Silahkan transfer ke Bank Permata dengan No rekening:',
    'bank.text.1'                => '1. Anda memiliki waktu 24 jam untuk melakukan pembayaran, sebelum transaksi tersebut dibatalkan',
    'bank.text.2'                => '2. Jika anda menemui kendala, silahkan hubungi kami di 021-22334456',
];