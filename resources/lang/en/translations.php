<?php
return [
    'password'              => 'Password',
    'signin'                => 'Sign in',
    'action_done'           => 'Action done',
    'email'                 => 'Email',
    'user_self_deleting'    => 'You cannot delete yourself',
    'name'                  => 'Name',
    'showStock'              => 'Show Stock Report on Email',
    'password_confirmation' => 'Confirm password',
    'not_the_same_password' => 'Passwords are not the same',
    'send'                  => 'Send',
    'role'                  => 'Role',
    'edit_invoice_or_shipped'    => 'You cannot edit Order with Invoice or Shipped one',
];