<?php

return [

    'menu.dashboard'      => 'Dashboard',
    'menu.blog'           => 'Blog',
    'menu.headers'        => 'Headers (A/B)',
    'menu.pages'          => 'Text pages',
    'menu.recordings'     => 'Recordings',
    'menu.products'       => 'Products',
    'menu.professions'    => 'Occupations',
    'menu.lessons'        => 'Lessons',
    'menu.packages'       => 'Packages',
    'menu.renewal'        => 'Renewal links',
    'menu.faq'            => 'FAQ',
    'menu.stats'          => 'Statistics',
    'menu.opinions'       => 'Opinions',
    'menu.contacts'       => 'Contacts',
    'menu.external'       => 'External Contacts',
    'menu.external.stats' => 'External Stats',
    'menu.personal.stats' => 'Personal Stats',
    'menu.orders'         => 'Orders',
    'menu.list'           => 'List',
    'menu.codtosend'      => 'COD (not sent)',
    'menu.codsent'        => 'COD (sent)',
    'menu.codtoconfirm'   => 'COD (to confirm)',
    'menu.cod-archived'   => 'COD (archived)',
    'menu.cod.duplicated' => 'COD (attempts)',
    'menu.banktransfer'   => 'Bank Transfer (not sent)',
    'menu.banktransfer.sent'   => 'Bank Transfer (sent)',
    'menu.invoices'       => 'All Invoices',
    'menu.cdata'          => 'Customer Data',
    'menu.settings'       => 'Settings',
    'menu.inventory'      => 'Inventory',
    'menu.users'          => 'Users',

    'button.search'            => 'Search',
    'button.delete'            => 'Delete',
    'button.export'            => 'Export',
    'button.details'           => 'Details',
    'button.exportcsv'         => 'Export to CSV',
    'button.sendmarked'        => 'Send marked',
    'button.delete-marked'     => 'Delete marked',
    'button.send'              => 'Send',
    'button.unsend'            => 'Mark as unsend',
    'button.markassent'        => 'Mark as sent',
    'button.resetip'           => 'IP Reset',
    'button.to-confirm'        => 'To confirm',
    'button.to-confirm-marked' => 'To confirm marked',
    'button.to-bank-transfer'  => 'To bank transfer',
    'button.edit'              => 'Edit',
    'button.archive'           => 'Archive',
    'button.restore'           => 'Restore',

    'common.list'         => 'List',
    'common.id'           => 'Id',
    'common.date'         => 'Date',
    'common.actions'      => 'Actions',
    'common.edit'         => 'Edit',
    'common.delete'       => 'Delete',
    'common.addnew'       => 'Add new',
    'common.add'          => 'Add',
    'common.save'         => 'Save',
    'common.title'        => 'Title',
    'common.content'      => 'Content',
    'common.image'        => 'Image',
    'common.language'     => 'Language',
    'common.name'         => 'Name',
    'common.icon'         => 'Icon',
    'common.price'        => 'Price',
    'common.currency'     => 'Currency',
    'common.stock'        => 'Stock',
    'common.minStock'     => 'Minimum Stock (Alert)',
    'common.files'        => 'Files',
    'common.active'       => 'Active',
    'common.yes'          => 'Yes',
    'common.no'           => 'No',
    'common.token'        => 'Token',
    'common.link'         => 'Link',
    'common.by'           => 'By',
    'common.by.d'         => 'Person who created the order',
    'common.customer'     => 'customer',
    'common.desc'         => 'Description',
    'common.type'         => 'Type',
    'common.popup'        => 'From Popup',
    'common.value'        => 'Value',
    'common.homepage'     => 'Home page',
    'common.email'        => 'Email',
    'common.phone'        => 'Phone',
    'common.action'       => 'Action',
    'common.course'       => 'Course',
    'common.size'         => 'Size',
    'common.status'       => 'Status',
    'common.courier'      => 'Courier',
    'common.sent'         => 'Sent/Not sent',
    'common.amount'       => 'Amount',
    'common.timesdown'    => 'DC',
    'common.download.d'   => 'Downloads count',
    'common.payment'      => 'Payment',
    'common.shipment'     => 'Shipment',
    'common.are-you-sure' => 'Are you sure?',
    'common.calls'        => 'Calls',
    'common.age'          => 'Age',
    'common.l.id'         => 'Indonesian',
    'common.l.en'         => 'English',
    'common.unique.number' => 'Unique Number',
    'common.ref'          => 'Ref',
    'common.confirmation' => 'Confirmation',
    'common.other'        => 'Other lang. preferred',
    'common.sendedcc'     => 'Sended to Call Center',
    'common.extClName'    => 'External Client Name',

    'orders.pending'    => 'Pending',
    'orders.paid'       => 'Paid',
    'orders.sent'       => 'Sent',
    'orders.not-sent'   => 'Not sent',
    'orders.rejected'   => 'Rejected',
    'orders.all'        => 'All',
    'orders.located'    => 'Located',
    'orders.notlocated' => 'Not Located',
    'orders.action'     => 'Not Located',
    'orders.order'      => 'Order',
    'orders.qty'        => 'Qty.',
    'orders.product'    => 'Product',
    'orders.productid'  => 'Product ID',
    'orders.price'      => 'Price',
    'orders.phone'      => 'Phone:',
    'orders.date'       => 'Date:',
    'orders.orderid'    => 'Order ID',
    'orders.paidtodate' => 'Paid to Date',
    'orders.linksent'   => 'Link sent',
    'orders.total'      => 'Total',

    'blog.blog'     => 'Blog',
    'blog.articles' => 'Articles',
    'blog.list'     => 'List',
    'blog.add'      => 'Add new',
    'blog.id'       => 'Id',
    'blog.title'    => 'Title',
    'blog.date'     => 'Date',
    'blog.actions'  => 'Actions',
    'blog.content'  => 'Content',
    'blog.author'   => 'Author',
    'blog.image'    => 'Main picture',
    'blog.save'     => 'Save',
    'blog.edit'     => 'Edit',
    'blog.delete'   => 'Delete',

    'headers.headers'    => 'Headers (A/B)',
    'headers.list'       => 'List',
    'headers.text'       => 'Text',
    'headers.activate'   => 'Activate',
    'headers.deactivate' => 'Deactivate',
    'headers.text1'      => 'Text 1',
    'headers.text2'      => 'Text 2',
    'headers.text3'      => 'Text 3',

    'pages.add' => 'Add new page',

    'recordings.recording' => 'Recording',
    'recordings.add'       => 'Add new recording',

    'products.add'          => 'Add new product',
    'products.trial'        => 'Trial lesson',
    'products.short'        => 'Short name',
    'products.shortdesc'    => 'Short description (list)',
    'products.trialdesc'    => 'Trial lesson description',
    'products.old'          => 'Old price (crossed out)',
    'products.symbol'       => 'Symbol',
    'products.icon'         => 'Icon (flag)',
    'products.prodid'       => 'Shipment product ID',
    'products.files'        => 'Product files',
    'products.list'         => 'List of files',
    'products.addfile'      => 'Add new file',
    'products.file'         => 'File',
    'products.package'      => 'Package',
    'products.percent'      => 'Percent off',
    'products.generate'     => 'Generate new',
    'products.packageof'    => 'Package of',
    'products.languages'    => 'languages',
    'products.links'        => 'Links',
    'products.image_10days' => 'Image (10 days version)',

    'occupations.add'       => 'Add new Occupation',
    'occupations.newsalary' => 'New salary',
    'occupations.oldsalary' => 'Old salary',

    'lessons.level' => 'Level',
    'lessons.add'   => 'Add new lesson',

    'faqs.add' => 'Add new FAQ',

    'stats.add'  => 'Add new Statistics',
    'stats.icon' => 'Icon (flag)',

    'opinions.add'      => 'Add new Opinion',
    'opinions.opinion'  => 'Opinion',
    'opinions.homepage' => 'Publish on home page',

    'settings.left'      => 'Left to sell',
    'settings.sold'      => 'Sold',
    'settings.interval'  => 'Quantity decrease interval [seconds]',
    'settings.threshold' => 'Quantity decrease threshold',

    'users.add'       => 'Add new User',
    'users.email'     => 'Email',
    'users.role'      => 'Role',
    'users.pass'      => 'Password',
    'users.confirm'   => 'Confirm password',
    'users.signature' => 'Signature',

];
