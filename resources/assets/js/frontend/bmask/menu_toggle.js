﻿jQuery( document ).ready(function() {

    jQuery(document).on('click touchstart', function(event){
        if( jQuery(event.target).closest("#top_menu").length )
            return;
        jQuery("nav ul#smoothMenu").removeClass('open');
        jQuery(".menu_btn").removeClass('active');
        event.stopPropagation();
    });
    jQuery('.menu_btn').on('click', function() {
        jQuery("nav ul#smoothMenu").toggleClass('open');
        jQuery(this).toggleClass('active');
        return false;
    });

});