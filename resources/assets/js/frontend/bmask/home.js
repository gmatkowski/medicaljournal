﻿/**
 * Created by maksimp on 11/28/14.
 */
jQuery( document ).ready(function() {
    /**
     * Detect Mobile Devices
     */
    var isMobile = {
        Android: function() {
            return navigator.userAgent.match(/Android/i);
        },
        BlackBerry: function() {
            return navigator.userAgent.match(/BlackBerry/i);
        },
        iOS: function() {
            return navigator.userAgent.match(/iPhone|iPad|iPod/i);
        },
        Opera: function() {
            return navigator.userAgent.match(/Opera Mini/i);
        },
        Windows: function() {
            return navigator.userAgent.match(/IEMobile/i);
        },
        any: function() {
            return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
        }
    };
    var viewPortWidth = jQuery('html').width();
    /** Detect Mobile Devices (The End) **/

    /**
     * Detect MSIE
     */
    jQuery.browser={};
    (function(){
        jQuery.browser.msie = false;
        jQuery.browser.version=0;
        if(navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
            jQuery.browser.msie = true;
            jQuery.browser.version = RegExp.$1;
        }
    })();
    var bs = document.body.style, isIE11=false;
    if ('msTransition' in bs) {
        isIE11 = true;
    }
    /** Detect MSIE (The End) **/

    /**
     * Claim Arrow Show and Hide function
     */
    if(!isMobile.any() && viewPortWidth >= 960) {
        var claimArrowFocus = false;
        var formSection = jQuery('.form');
        var formInputs = jQuery('.form input');
        formInputs.focus(function(){
            jQuery('.claim_arrow', this).css({'display' : 'block'});
            claimArrowFocus = true;
        });
        formSection.hover(
            function(){
                if (!claimArrowFocus) {
                    jQuery('.claim_arrow', this).fadeIn(200);
                }
            },
            function(){
                if (!claimArrowFocus) {
                    jQuery('.claim_arrow', this).stop().fadeOut(200);
                }
            });
    }
    /** Claim Arrow Show and Hide function (The End) **/

    /**
     * Active Menu
     */
    var sectionId = [];
    var s = 0;
    var smoothMenuHeight = 80;
    jQuery('body section').each(function(){
        sectionId[s] = jQuery(this).attr('id');
        s++;
    });
    /** Active Menu (The End) **/

    /**
     * Scroll function
     */
    jQuery(window).scroll(function(e) {
        if(sectionId != 0) {
            var smoothMenuLink = jQuery('#smoothMenu li');
            for(var i = 0; i < sectionId.length - 1; i++) {
                if(jQuery(window).scrollTop() >= jQuery('#' + sectionId[i]).offset().top - smoothMenuHeight && jQuery(window).scrollTop() <= jQuery('#' + sectionId[i+1]).offset().top - smoothMenuHeight) {
                    smoothMenuLink.find('a').removeClass('active');
                    smoothMenuLink.find('a[href="#'+sectionId[i]+'"]').addClass('active');
                }
            }
        }

        var nav = jQuery('.nav_wp');
        if (nav.length != 0){
            if(jQuery(window).scrollTop() >= nav.offset().top){
                nav.find('#top_menu').addClass('fixed');
            } else {
                nav.find('#top_menu').removeClass('fixed');
            }
        }
    });
    /** Scroll function (The End) **/

    /**
     * Smooth Scroll
     */
    var hasBeenClicked = false,
        floatingMenuHeight;
    jQuery('.button, .get_free_btn, #top_menu .last a').on('click', function () {
        hasBeenClicked = true;
    });
    jQuery('.main__form button[type="submit"]').on('click', function() {
        hasBeenClicked = false;
    });
    if (viewPortWidth < 767) {
        floatingMenuHeight = 35;
    } else {
        floatingMenuHeight = 50;
    }
    jQuery('[data-scroll]').on('click', function(){
        var currentOffSet = jQuery(''+ jQuery(this).attr('href') +'').offset().top - floatingMenuHeight;
        jQuery('html,body').animate({ scrollTop : currentOffSet }, 700, 'easeInOutCubic', function() {
            if(hasBeenClicked) {
                jQuery( "#firstname" ).focus();
                hasBeenClicked = false;
            }
        });
        return false;
    });
    /** Smooth Scroll (The End) **/

    /**
     * Add class active if menu on minitablet and mobile is opened
     */
    jQuery('.menu_btn').on('click', function(){
        jQuery(this).toggleClass('active');
    });
    /** Add class active if menu on minitablet and mobile is opened (The End) **/

}); /** Document Ready (The End) **/