App = {
    address: {
        selects: $('#addressOrder select.selectable'),
        clear: function (sequence) {
            this.selects.each(function () {
                var obj = $(this);
                if (obj.data('sequence') > sequence) {
                    $(this).html('').prop('disabled', true);
                }
            });
        },
        init: function () {
            $('#addressOrder select.selectable').on('change', function () {

                var object = $(this);
                var value = object.val();
                var form = object.parents('form');

                if (value > 0) {

                    var data = {
                        'method': object.data('name'),
                        'id': value
                    }
                    /*this.clear(object.data('sequence'));*/
                    $.ajax(
                        {
                            type: "POST",
                            dataType: 'json',
                            url: '/order/get-options',
                            data: data,
                            success: function (response) {
                                if (response.status == 'ok') {
                                    if (object.hasClass('last')) {

                                        form.find('input[name="nocod"]').val(0);
                                        form.find('input[type="submit"]').prop('disabled', false);
                                        form.find('input[type="submit"]').next('p').remove();
                                        /*
                                         if (response.item.cod == false) {
                                         form.find('input[name="nocod"]').val(1);

                                         form.find('input[type="submit"]').prop('disabled', true);
                                         if (form.find('input[type="submit"]').next('p').length == 0) {
                                         //form.find('input[type="submit"]').after('<p>' + Lang.get('languages.buy.no.cod.available') + '</p>')
                                         App.message(Lang.get('languages.buy.no.cod.available'), 'error')
                                         }


                                         }
                                         else {
                                         form.find('input[name="nocod"]').val(0);
                                         form.find('input[type="submit"]').prop('disabled', false);
                                         form.find('input[type="submit"]').next('p').remove();
                                         }
                                         */
                                    }
                                    else {
                                        var $select = object.next('select');

                                        var values = [];
                                        $.each(response.items, function (key, value) {
                                            values.push({
                                                id: value.id,
                                                name: value.name
                                            });
                                        });

                                        var values = values.sort(function (a, b) {
                                            var textA = a.name.toUpperCase();
                                            var textB = b.name.toUpperCase();
                                            return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
                                        });

                                        for (var key in values) {
                                            var value = values[key];
                                            if (value.id == 0) {
                                                $select.prop('disabled', false).append('<option disabled selected value=' + value.id + '>' + value.name + '</option>');
                                            }
                                            else {
                                                $select.prop('disabled', false).append('<option value=' + value.id + '>' + value.name + '</option>');
                                            }
                                        }
                                    }
                                }
                                else {
                                    App.message(response.message, response.status);
                                }
                            },
                            error: function (e) {
                                console.log(e);
                            }
                        })
                }
                else {
                    /*this.clear(object.data('sequence'));*/
                }
            });
        }
    },
    message: function (message, type) {

        if (typeof type == 'undefined') {
            type = 'success'
        }

        swal({title: 'informasi', text: message, type: type, confirmButtonText: "OK"});
    },
    gaEvent: function (category, action, label) {
        ga('send', {
            hitType: 'event',
            eventCategory: category,
            eventAction: action,
            eventLabel: typeof label != 'undefined' ? label : null
        });
    }
};

$.ajaxSetup({
    data: {
        '_token': $('meta[name="csrf-token"]').attr('content')
    }
});


App.address.init();
