@extends('layout.app')

@section('content')


    <section class="faq row" style="padding-top: 50px">
        <div class="container col-md-8 col-md-offset-2">
            <h1>Pertnanyaan Umum (FAQ)</h1>
            <p>Kami mengumpulkan beberapa pertanyaan yang sering diajukan pelanggan kami. Kami mendorong Anda untuk
                membaca, Anda akan menemukan banyak jawaban untuk pertanyaan yang mungkin belum jelas bagi Anda. Jika
                Anda tidak menemukan jawaban untuk pertanyaan Anda, silakan hubungi kami melalui formulir kontak.</p>

            <div class="faq">
                <div class="ask"><span></span>Apakah ada efek samping setelah menggunakan Penirium?</div>
                <div style="display: none;" class="answer"><p>Tidak ada efek samping diketahui atau kontra untuk
                        menggunakan Penirium. Suplemen makanan Penirium mengandung 100% bahan alami, jadi penggunaanya
                        aman. Penirium diproduksi di Uni Eropa – oleh karena itu, Anda dapat yakin bahwa produk ini
                        diperiksa pada setiap tahap produksi, dan memenuhi semua persyaratan keamanan yang berlaku di
                        Indonesia.</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Apa itu Penirium?</div>
                <div class="answer"><p>Penirium adalah suplemen makanan dalam kapsul yang benar-benar alami. Semua
                        komponen dipilih oleh para ahli dengan teliti, untuk membantu meningkatkan fungsi corpus
                        cavernosum, yaitu ukuran penis Anda.</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Bagaimana cara kerjanya?</div>
                <div class="answer"><p>Penggunaan secara biasa mempengaruhi suplai darah corpus cavernosum, yang
                        bertanggung jawab untuk mengubah ukuran penis Anda saat ereksi. Anda akan melihat bahwa penis
                        Anda akan menjadi lebih besar dan lebih keras pada saat gairah seksual, dan ereksi Anda akan
                        menjadi lebih kuat. Jangan lupa bahwa ukuran penis Anda juga penting untuk kualitas orgasme
                        pasangan Anda!</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Bagaimana cara mengambil Penirium?</div>
                <div class="answer"><p>Harus mengambil 1 tablet sehari. Dianjurkan untuk mengambil di pagi hari, 15
                        menit sebelum sarapan, dengan segelas air putih.</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Dapatkah saya minum alkohol selama pengobatan Penirium?</div>
                <div class="answer"><p>Tidak ada penurunan efek Penirium pada orang yang minum alkohol, dan tidak ada
                        efek samping.</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Apakah Penirium dapat digunakan pada semua usia?</div>
                <div class="answer"><p>Ya. Tidak ada batasan usia untuk pria dewasa. Penirium dapat digunakan oleh pria
                        muda maupun pria tua. Untuk Anda yang memiliki masalah dengan hipertensi, sangat baik saja jika
                        Anda berkonsultasi dengan dokter Anda.</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Apakah efeknya permanen?</div>
                <div class="answer"><p>Hasil yang dicapai dengan menggunakan Penirium juga dapat dilihat setelah
                        menggunakannya. Sekarang Anda bisa mendapatkan kembali rasa percaya diri Anda, dan memuaskan
                        wanita yang paling menuntut kesempurnaan seks.</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Apakah saya dapat menikmati anonimitas lengkap?</div>
                <div class="answer"><p>Tentu saja, kami memberikan keamanan dan anonimitas lengkap – kami sangat peduli
                        dengan kenyamanan pelanggan kami. Kami kemas pengiriman dengan baik dan memiliki pengalaman yang
                        kaya dalam kemasan, sehingga Anda tidak perlu khawatir tentang kemasan. Selain itu, barang
                        tersebut dikirim dengan kurir, dan akan disampaikan langsung kepada Anda pribadi.</p></div>
            </div>
            <div class="faq">
                <div class="ask"><span></span>Apakah terbukti khasiatnya?</div>
                <div class="answer"><p>Pelanggan yang puas adalah sumber referensi terbaik, dan hasilnya bisa dilihat oleh semua pelanggan kami.</p></div>
            </div>
            <div class="more">
                Jika Anda masih memiliki pertanyaan, <a href="http://www.penirium.com/contact">hubungi kami</a>.
            </div>

        </div>

    </section>



    <section class="block-10 row">
        <div class="container col-md-8 col-md-offset-2">
            <h1>PESAN PENIRIUM, HIDUP ANDA AKAN BERUBAH</h1>
            <div class="row col-md-12">
                <div class="info col-md-6">
                    <ul>
                        <li>Obat yang alami dan aman untuk memperbesar <br>penis Anda hingga 7,5 cm lebih panjang</li>
                        <li>Anda menyimpan uang 610 000 IDR – 1 100 000 IDR <br>harga bekas, beli sekarang dengan harga
                            baru 490 000 IDR
                        </li>
                        <li>Tidak ada ongkos ekstra – pengiriman gratis</li>
                        <li>Pembelian tanpa resiko – Anda akan mendapat<br> 60 hari garansi atau uang kembali</li>
                    </ul>
                </div>
                <div class="p_package col-md-6">
                    <img
                        src="{{ asset('build/ext/penis/3/p_product_img.jpg') }}"
                        alt="Penirum - package img">
                    <div class="price">
                        <span class="old-price">1.100.000 IDR</span>
                        <span class="new-price">490.000 IDR</span>
                    </div>
                    <div class="order-button-big">
                        <a href="/order">Beli sekarang</a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script type="text/javascript">
        $(function () {
            $('div.ask').click(function () {
                parent = $(this).parent();
                answer = $(parent).find('.answer');
                if (answer.css('display') === 'block') {
                    answer.hide();
                    $(this).removeClass('selected');
                } else {
                    answer.show();
                    $(this).addClass('selected');
                }
            });
        });
    </script>

@endsection