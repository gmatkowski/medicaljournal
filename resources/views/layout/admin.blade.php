<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token()  }}">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ Config::get('app.name') }} :: Administration</title>
	<link rel="shortcut icon" href="{{asset('build/images/frontend/medical.ico')}}">

	<link rel="stylesheet" href="{{ elixir('css/admin/all.css') }}">
	<script src="{{ asset('build/js/vendor/jquery.min.js') }}"></script>
</head>
<body class="nav-md">
<div class="container body">
	<div class="main_container">
		@section('admin.menuandtop')
			@include('admin.menu')
			@include('admin.top')
		@show
		@yield('content')
	</div>
</div>

<script src="{{ elixir('js/admin/all.js') }}"></script>

@yield('scripts')

<!--[if lt IE 9]>
<script src="{{ asset('build/js/vendor/html5shiv.min.js') }}"></script>
<script src="{{ asset('build/js/vendor/respond.min.js') }}"></script>
<![endif]-->

</body>
</html>
