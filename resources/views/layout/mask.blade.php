<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<title>{{ isset($title) ? $title : '' }}</title>
	<meta name="description" content="{{ $meta_description }}">
	<meta name="keywords" content="{{ $meta_keywords }}">
	@section('ico')@show
	@section('styles')@show
	@section('analytics')@show
	@section('trackConversions')@show
	@section('chat')@show
</head>
<body class="page-article">
	@yield('content')
	@section('scripts')@show
</body>
</html>