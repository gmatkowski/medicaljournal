<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta name="robots" content="noindex, nofollow">
	<meta name="poptm" content="98c1fc7c468b6f60a59dcbe45297dae5" />
	<title>{{ isset($title) ? $title : '' }}</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="shortcut icon" href="{{asset('build/images/frontend/medical.ico')}}">
	@section('styles')
		<link rel="stylesheet" href="{{ elixir('css/frontend/all.css') }}">
	@show
	<script src="{{ asset('build/js/vendor/jquery.min.js') }}"></script>
	<script src="{{ asset('build/js/vendor/jquery.form-validator.min.js') }}"></script>
	@section('analytics')@show
	@section('trackConversions')
	@show
	@section('pixel')
	@show
	@section('scripts')
	@show
	@section('chat')
		@include('part.chat')
	@show
</head>
<body>

	@yield('content')

@if(Session::has('ga_event'))
	<script type="text/javascript">
		ga('send', {
			hitType: 'event',
			eventCategory: '{{ Session::get('ga_event.category') }}',
			eventAction: '{{ Session::get('ga_event.action') }}',
			eventLabel: '{{ Session::get('ga_event.label') }}'
		});
	</script>
@endif

<!--[if lt IE 9]>
	<script src="{{ elixir('js/html5support/all.js') }}"></script>
	<![endif]-->

	@section('google')
		@include('part.google')
	@show
	@section('remarketing')
		@include('part.remarketing')
	@show
	@section('endBodyConversion') @show
	@section('endBodyScripts') @show
</body>
</html>