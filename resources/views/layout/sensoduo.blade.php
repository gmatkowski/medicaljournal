<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Senso Duo</title>
    <base target="_blank" />
    <link rel="icon" type="image/ico" href="//st01.worldglobalnews.com/59/39/60a48e42759074024ea130f3369b/favicon.ico"/>

    <link rel="stylesheet" href="{{ elixir('css/sensoduo/all.css') }}">

    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="//st01.worldglobalnews.com/3f/fd/ce269222b1aeffbd84f873acc4ae/ie7.css" />
    <![endif]-->
    <!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="//st01.worldglobalnews.com/3f/fd/ce269222b1aeffbd84f873acc4ae/ie8.css" />
    <![endif]-->
    <!--[if gte IE 9]>
    <style type="text/css">
        div.info .header {
            filter: none;
        }
    </style>
    <![endif]-->

</head>
<body>
    @yield('content')
</body>
</html>