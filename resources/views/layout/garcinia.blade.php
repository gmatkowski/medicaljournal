<html lang="pl" class="">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ isset($title) ? $title : '' }}</title>
    <meta name="google-site-verification" content="_lLKC4UHor30_eBxaIOxNiGpvo4oRHYT41tI8Plnbug">

    <link href="{{asset('build/images/frontend/garcinia.ico')}}" rel="icon" type="image/x-icon">

    @section('styles')@show

<body class="pl">

    <header id="main">
        <div class="nav-top">
            <div class="page-container">
                <span>Hubungi kami: 0888 01000 488</span>

                <span>Jumlah pengguna online: <strong id="usersOnline">memuat...</strong></span>
            </div>
        </div>
        <div class="page-container">
            <a href="#"><img src="{{ asset('build/images/frontend/logo-top.png') }}" alt="Piperine Forte Logo"></a>
        </div>
    </header>

    @yield('content')

    <footer>
        <div class="page-container">
            <div class="row">
                <div class="logo">
                    <img src="{{ asset('build/images/frontend/logo-footer.png') }}" alt="Piperine Forte footer">
                </div>
                <ul>
                    <li><a href="{{ route('page.introcomposition') }}">Komposisi</a></li>
                    <li><a href="{{ route('page.intrometamorphosis') }}">Testimoni</a></li>
                    <li><a href="#">Kontak</a></li>
                </ul>
                <ul>
                    <li><a href="{{ route('page.introfaq') }}">Pertanyaan Umum</a></li>
                    <li><a href="#" onclick="window.open('/garcinia/terms-regulations', 'windowname1', 'width=670, height=765, scrollbars=1'); return false;">Syarat & Ketentuan</a></li>
                    <li><a href="#" onclick="window.open('/garcinia/terms-cookies', 'windowname1', 'width=670, height=765, scrollbars=1'); return false;">Kebijakan Privasi</a></li>
                </ul>
                <div class="contact">
                    ADA PERTANYAAN? HUBUNGI KAMI!<br><br>
                    0888 01000 488<br><br>
                    <table>
                        <tbody><tr>
                            <td>Senin-Jumat</td>
                            <td>8:00 - 21:00</td>
                        </tr>
                        <tr>
                            <td>Sabtu</td>
                            <td>9:00 - 17:00</td>
                        </tr>
                        </tbody></table>
                </div>
            </div>
            <p>Hak Cipta © 2017. Semua Hak Cipta Terpelihara.</p>
        </div>
    </footer>

</body>
</html>