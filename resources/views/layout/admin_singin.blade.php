<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<title>{{ Config::get('app.name') }} :: Administration</title>

	<link rel="shortcut icon" href="{{asset('build/images/frontend/medical.ico')}}">
    <link rel="stylesheet" href="{{ elixir('css/admin/all.css') }}">

</head>
<body style="background:#F7F7F7;">
@yield('content')

<!--[if lt IE 9]>
<script src="{{ elixir('js/html5support/all.js') }}"></script>
<![endif]-->

</body>
</html>
