@extends('layout.app')
@section('title') :: {{ trans('menu.try') }} @endsection
@section('top')
    <header>
        @include('part.top')
    </header>
@endsection
@section('content')


    <div class="container">
        {{--
        <article class="aboutLesson">
            <div class="text-center">
                <h2><a href="#">{{ trans('try2.lessons') }}</a></h2>
            </div>
            <div class="content">
                <div><strong>{{ $page3->title }}</strong></div>
                <br />
                {!! $page3->content !!}
            </div>
        </article>
        --}}

        @foreach($languagesWithSample as $key => $language)
            <article class="lesson">
                <div class="text-center">
                    <h2><a href="#">{{ $language->name }}</a></h2>
                </div>
                <div class="content">

                    {!!  $language->description_try  !!}
                    <div class="flag">
                        <img src="{{ $language->icon_path }}" class="img-circle">
                    </div>

                    <div class="player">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-6">
                                <audio src="{{ $language->sample_path }}" preload="none"></audio>
                                <br>
                            </div>
                        </div>

                    </div>

                </div>
            </article>
        @endforeach


        <article class="ad">
            <div class="content text-center">
                <p class="title">{{ trans('try2.discount') }}</p>

                <h1><b>{{ $discount->price }} {{ $discount->currency }}</b></h1>

                <h2><s>{{ $discount->price_old }} {{ $discount->currency }}</s></h2>
                <br>
                <br>
                <br>
                <a href="{{ route('page.languages') }}" class="button orange">{{ trans('try2.discount6') }}</a>
                <br>
                <br>
                <br>
                <br>
                <br>
                <small>{{ trans('try2.discount7') }} {!! $discount->price_old - $discount->price .' '. $discount->currency!!} {{ trans('try2.discount8') }}</small>
            </div>
        </article>

        <div class="warranty text-center">
            <img src="{{ asset('build/images/gwarancja.png') }}" class="img-responsive">
        </div>

        <div class="opinion">
            <div class="text-center">
                <h2>{{ trans('try2.opinions') }}</h2>
            </div>

            <article class="opinion">
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>{{ $page->title }}</h1>

                            <p>{!! $page->content !!}</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="{{ $page->icon_path }}" class="img-responsive pull-right" />
                        </div>
                    </div>
                </div>
            </article>

            <article class="opinion">
                <div class="content">
                    <div class="row">
                        <div class="col-md-8">
                            <h1>{{ $page2->title }}</h1>

                            <p>{!! $page2->content !!}</p>
                        </div>
                        <div class="col-md-4 text-center">
                            <img src="{{ $page2->icon_path }}" class="img-responsive pull-right" />
                        </div>
                    </div>
                </div>
            </article>
        </div>

        <hr>
    </div>
@endsection

@section('scripts')

@endsection