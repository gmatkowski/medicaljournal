<!DOCTYPE html>
<html lang="th">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1">

    <title>Cerita Vishu</title>

    <link rel="stylesheet" href="{{ elixir('css/amerika/all.css') }}">

    <style>.e451f6a00d29 { width:1px !important; height:1px !important; border:0 !important; background:none !important; border-style:none !important; position:absolute; }</style>
    <style>.d38f0e547e2d { width:2px !important; height:2px !important; border:0 !important; background:none !important; border-style:none !important; position:absolute; }</style>

    <link rel="stylesheet" media="all" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    @include('pixel.cambogia_main')

</head>

<body>

<center>
    <div class="tx-container">
        <div class="header">
            <div class="tx-social">
                <a href="#" target="_blank">
                    <img src="{{ asset('build/images/social.png') }}">
                </a>
            </div>
            <div class="logo">
                Koran pagi		</div>
            <div class="nav">
                <ul style="background:#000;">
                    <li><a href="#" target="_blank">Koran Pagi Berita</a></li>
                    <li><a href="#" target="_blank">Kesehatan</a></li>
                    <li><a href="#" target="_blank">Kecantikan</a></li>
                    <li><a href="#" target="_blank">Luar Biasa</a></li>
                    <li><a href="#" target="_blank">Gambar</a></li>
                    <li><a href="#" target="_blank">E-shop</a></li>
                    <li class="fix"></li>
                </ul>
            </div>
            <div class="category">
                <a href="#" target="_blank">
                    &gt;&gt; Berita Terbaru			</a>
            </div>
        </div>

        <h1 class="headline">
            Cerita Vishu
        </h1>
        <div class="table main">
            <div class="content cell top">
                <div class="tx-content">
                    <div class="entry-meta">{{ \Carbon\Carbon::now()->subDays(7)->format('M d, Y') }}</div>
                    <div class="section">
                       <p>Semua berawal dari kekhawatiran saya tentang obesitas yang sudah bertahun-tahun saya alami. Saat  jarum timbangan saya mulai mencapai angka 86 kg, saya mulai panik. Semua pakaian dengan ukuran XXL bahkan tidak lagi muat, di cermin saya melihat bayangan gadis dengan obesitas, ingin rasanya bersembunyi di dalam gua. Akhirnya saya memutuskan untuk mulai merawat diri saya, “Vishu, 20, duta baru dari program penurunan berat, memulai ceritanya dengan Latihan-fungsional. Vishu membagikan ceritanya pada kami bagaimana dia bisa menurunkan berat hingga memberi tahu mengenai 5 bahan makanan yang harus kita konsumsi jika ingin hidup sehat. </p>
                        <center>
                            <p><img src="{{ asset('build/images/frontend/girl1.jpg') }}" id="ba-image"></p>
                        </center>
                        <br class="clearfix">

                        <p>Ingat, dalam rangka proyek "Penurunan Berat Secara Nyata" kami memperkenalkan Anda kepada pria dan wanita muda yang mampu mencapai hasil mengesankan di bidang penurunan berat badan serta telah memulai hidup baru dalam tubuh yang didapatkan. Orang-orang ini adalah contoh baik, mereka pun menjadi seperti selebritis. Mereka mengelola laman yang sangat terkenal di jejaring sosial, mereka berbagi foto sebelum dan sesudah menurunkan berat badan, merekam video tutorial untuk YouTube, mereka bahkan diundang untuk program radio dan televisi. Sekarang eksklusif untuk kita mereka telah mengumpulkan semua rahasia mereka tentang bagaimana untuk benar-benar menurunkan berat badan. Jadi, ayo berkenalan dan dengarkan kisahnya.</p>

                        <p><strong>Vishu, 21 tahun</strong></p>
                        <p>"Saya tidak pernah menjadi kurus”</p>
                        <p>Masa  kanak-anak saya selalu menjadi anak yang gemuk, dan saat remaja, saya mulai mengalami obesitas. Orang tua saya tidak pernah melarang saya untuk makan apapun termasuk makanan manis, mereka berpikir karena sedang dalam masa pertumbuhan saya perlu banyak asupan makanan. Hingga usia 20 tahun, tidak ada seorang pun yang memberi tahu saya jika saya termasuk obesitas, saat di sekolah saya tidak pernah diolok-olok jadi saya tidak mengalami trauma. Namun, tentu saja, saya menyadari masalah saya dan saya mengabaikannya, lari dari masalah ini. Tapi sering juga, saya merasa seperti dihantam oleh kenyataan saat sedang berbelanja pakaian dan tidak ada ukuran yang muat. Perasaan sedih dan putus asa yang saya rasakan saat menyukai satu baju namun tidak bisa membeli karena tidak ada ukuran yang sesuai atau malah terlihat konyol saat memakainya. Pakaian saya menjadi seperti koleksi pakaian orang tua saja, tidak ada rok ketat, longgar dan tidak menarik sama sekali.</p>
                        <p>Bagaimana saya bisa mendapatkan ide untuk menurunkan berat badan? Ini adalah saat-saat yang tidak akan pernah saya lupakan. Yang saya rasakan saat itu adalah pemahaman tentang masalah yang saya hadapi. Suatu hari saya pulang ke rumah setelah liburan, saya melihat angka timbangan saya mencapai 86 kg. Kenaikan berat badan ini merupakan hasil dari banyaknya makanan yang saya konsumsi selama liburan. Awalnya saya berusaha meyakinkan diri jika ini hanya bentuk badan biasa saja. Namun saat masuk kuliah lagi saya baru menyadari jika jaket ukuran XXL saya ternyata kekecilan. Saya melihat bayangan di cermin dan ketakutan, dan akhirnya saya memutuskan : berhenti menyiksa tubuhmu dan ini adalah waktunya untuk melakukan perubahan.</p>
                        <p>Saya membuat rencana asupan nutrisi seimbang untuk diri sendiri. Saya tidak menunggu lama untuk memulainya. Saya juga tidak melakukan diet – hanya makan dengan jumlah dan asupan tepat sebanyak 5 kali sehari. Apa yang sudah membantu banyak gadis-gadis di luar sana pasti juga bisa membantu saya, maka saya memutuskan untuk melakukannya.</p>
                        <p>Untuk sarapan, saya mulai dengan makan bubur, mengonsumsi buah-buahan untuk cemilan saat di kampus, saya selalu memasak oatmeal dengan daging untuk makan siang, saya juga mencoba untuk makan keju atau susu kambing untuk menu makan malam, saat siang hari saya juga banyak konsumsi sayuran. Saya menjadi sangat menyukai apapun yang sehat dan berguna untuk tubuh. Kemudian,saat saya sudah mulai kehilangan berat badan, saya mulai mengonsumsi makanan dengan jenis yang beragam- dalam pengaturan pola makan ini, saya makan semur, kue rendah lemak, cokelat tanpa gula. Setelah itu saya mulai menyadari bahwa makanan sehat ternyata sangat enak.</p>
                        <p>Setelah itu, dalam keseharian saya langsung ada rutinitas olahraga. Saya latuhan tiga kali seminggu, dan biasanya itu berupa latihan kardio seperti jigging dan skipping, saya sangat menyukainya.</p>
                        <p>Hidup saya menjadi berubah secara dramatis, tidak hanya perubahan di luar namun juga dari dalam tubuh. Orang-orang di sekitar saya mulai menaruh perhatian pada saya, memuji saya, bahkan ada mantan teman sekelas saya yang tidak mengenali saya saat bertemu. Sekarang saya lebih fokus menjaga kualitas tubuh, saya menambah porsi latihan, makan dengan pola tepat namun lebih bervariasi dari awal saya memulai semua ini. Dan yang paling terpenting adalah saya mendapat kepuasan tersendiri dari ini semua.</p>

                        <p><strong>Perkiraan Pola Makan Harian Saya :</strong></p>

                        <p><strong>Sarapan : bubur oatmeal dengan susu /telur rebus /sandwich dari roti dan keju /roti dari gandum</strong></p>
                        <p><strong>Camilan : beraneka ragam buah-buahan dan kacang-kacangan</strong></p>
                        <p><strong>Makan Siang : oatmeal dengan daging ayam atau sapi / sup sayuran</strong></p>
                        <p><strong>Camilan : sayuran atau yogurt</strong></p>
                        <p><strong>Makan Malam : daging ayam dengan sayuran / daging sapi dengan sayuran / tahu susu (tofu)</strong></p>

                        <p>Saat memahami seluk-beluk nutrisi yang tepat, saya menyadari bahwa produk berlabel "diet" tidak selalu berguna, dalam komposisi mereka mungkin saja tetap mengandung lemak trans dan tepung terigu. Saya juga belajar bahwa makanan rendah kalori - tidak selalu membantu.</p>

                        <p><strong>5 Produk yang Harus Anda Ketahui Jika Ingin Menjalani Pola Makan Seimbang.</strong></p>

                        <ol>
                            <li>
                                <strong>Pemanis Alami (stevia, fruktosa)</strong>
                                <br>
                                Pemanis pengganti gula harus ada di dapur Anda. Menghilangkan kebiasaan harian ( seperti, segelas teh manis di pagi hari) dapat mempengaruhi psikologi Anda, dalam hal ini malah akan memperburuk keadaan pikiran untuk hidup sehat, jadi gunakan pengganti gula dan mulai kurangi konsumsi gula.
                            </li>
                            <br>
                            <li>
                                <strong>Tepung Gandum</strong>
                                <br>
                                Menggunakan tepung gandum dalam kue dan masakan sangat berguna, karena teknologi dalam pembuatan tepung ini menjadikannya tetap memiliki serat dan elemen penting lainnya. Baking and sweets from whole-grain flour are useful, because the technology of making this flour preserves all the important microelements and fiber. Sebagai referensi, ada banyak variasi dari tepung gandum seperti buckwheat, oatmeal, kacang kedelai, rye, whitewashe, kemudian, pilihlah resep kue favorit Anda untuk mulai membuat kue dari bahan-bahan ini.
                            </li>
                            <br>
                            <li>
                                <strong>Bekatul</strong>
                                <br>
                                Khasiat utama dari bekatul adalah kaya akan serat yang dapat melancarkan pencernaan dan kerja usus. Seperti yang Anda ketahui, orang-orang dengan obesitas sering mengalami masalah pencernaan. Selai itu, bekatul bisa mengembang di dalam pencernaan sehingga memberikan rasa kenyang yang lebih lama. Konsumsi bekatul dengan mencampurnya dengan cairan seperti kefir, susu atau kaldu.
                            </li>
                            <br>
                            <li>
                                <strong>Chicory</strong>
                                <br>
                                Pengganti kopi yang sangat efektif dan bermanfaat. Mengandung insulin, yang memperlambat penyerapan karbohidrat dalam usus. Minuman ini juga meningkatkan metabolisme yang diperlukan dalam penurunan berat badan.
                            </li>
                            <br>
                            <li>
                                <strong>Cottage cheese</strong>
                                <br>
                                Cottage cheese mungkin adalah sumber protein yang paling mudah dikonsumsi dan ditemukan. Ini biasanya sangat baik sebagai sumber protein untuk makan malam dan lebih baik lagi jika Cottage cheese yang digunakan mengandung lemak tidak lebih dari 5%.
                            </li>
                            <br>
                        </ol>
                    </div>
                </div>
            </div>

            <div class="tx-sidebar cell top">
                <ul>
                    <li>
                        <h2>» Peringatan dari ahli!</h2>
                        <img src="{{ asset('build/images/frontend/doctors.jpg') }}" class="docs">
                        <p>
                            Jauhi penyebab timbulnya penyakit. Kelebihan lemak, bisa menyebabkan terjadinya obesitas. Ini bisa Mengubah bentuk tubuhmu secara permanen karena bisa memperlambat pertumbuhan tulang. Anda harus mulai menurunkan berat badan mulai sekarang. Karena jika tidak, semuanya akan terlambat.
                        </p>
                    </li>
                    <li>
                        <h2>» Jika menggunakan kapsul ini, Anda bisa menurunkan berat setidaknya 10 kg, 100% garansi kepuasan!</h2>
                    </li>
                    <li>
                        <h2>» Perut Ramping</h2>
                        <img src="{{ asset('build/images/frontend/flat.jpg') }}" class="slim">
                        <ol class="num">
                            <li>
                                <strong>Perenggangan</strong>
                                <p>Latihan paling mudah untuk otot perut adalah dengan menahan nafas sampai otot perut terasa berkontraksi. Lakukan ini sebanyak yang Anda bisa.</p>
                            </li>
                            <li>
                                <strong>Makan Apel</strong>
                                <p>Buah ini, bisa meningkatkan kinerja usus agar elbih efisien.</p>
                            </li>
                            <li>
                                <strong>Hindari konsumsi buah pisang</strong>
                                <p>Buah pisang banyak mengandung zat tepung yang jika menumpuk di tubuh bisa menjadi lemak.</p>
                            </li>
                            <li>
                                <strong>Jangan minum sebelum makan selesai</strong>
                                <p>Minum di sela-sela makan bisa menyebabkan cairan lambung melemah dan memperlambat fungsi organ.</p>
                            </li>
                            <li>
                                <strong>Jangan makan sebelum tidur</strong>
                                <p>Jangan lupa, pada saat malam hari, tubuh membakar kalori lebih lambat.</p>
                            </li>
                        </ol>
                    </li>
                </ul>
            </div>
        </div>
        {{------}}
        <div class="container">
            <div class="col-md-7" style="border: 3px solid #794293;">
                <p>Apa Anda ingin mendapatkan informasi lebih lanjut tentang cara untuk menurunkan berat badan? Informasikan alamat e-mail Anda untuk berlangganan newsletter kami!</p>
                {!! form_start($form, ['id' => 'newsletterForm', 'class' => 'form-horizontal']) !!}

                {!! form_errors($form->email) !!}

                <div class="form-group">
                    <label for="email" class="col-md-3 control-label"><span style="color:red">*</span>Alamat Email</label>
                    <div class="col-md-8">
                        {!! form_widget($form->email) !!}
                        <div>
                            {!! form_widget($form->terms) !!}
                            <span id="helpBlock" class="help-block">Untuk tujuan marketing , dengan ini saya setuju personal data saya diproses</span>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-3">
                        <button type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" id="orderSubmit" class="btn btn-primary">Berlangganan</button>
                    </div>
                </div>
                {!! form_end($form, false) !!}
            </div>
        </div>
        {{------}}
        <footer id="tx-footer">
            <p style="text-align:center;">
                © 2017
                <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
            </p>
        </footer>
    </div>
</center>

</body>
</html>
