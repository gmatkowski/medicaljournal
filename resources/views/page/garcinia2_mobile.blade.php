@extends('layout.app',[
    'title' => 'Garcinia Cambogia Forte'
])

@section('styles')
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>

    <link rel="stylesheet" href="{{ elixir('css/garcinia2-mobile/all.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,300italic,600italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
@endsection

@section('content')
    <div class="hideJs">
        <div class="block block1 with-gradient">
            <div class="limit clearfix">
                <a class="logo for-event" >Garcinia Cambogia Forte</a>
                <div class="content">
                    <p class="title1 ttu">TURUNKAN BERAT BADAN BERLEBIH
                    </p>
                    <p class="title2">CEPAT DAN MUDAH DENGAN Garcinia Cambogia Forte
                    </p>
                    <img alt="" src="{{ asset('build/images/frontend/prod.png') }}"/>
                    <a class="btn btn-primary for-event scroll">Pesan sekarang
                    </a>
                    <img alt="" class="body" src="{{ asset('build/images/frontend/block1-1.png') }}" style="left: 0;"/>
                    <div class="list">
                        <span class="ttu">CARA MENURUNKAN BERAT BADAN YANG MUDAH DAN MENYENANGKAN DENGAN Garcinia Cambogia Forte:</span>
                        <ul>
                            <li>Pembakar lemak cepat dan alami
                            </li>
                            <li>Mengurangi nafsu makan secara sehat
                            </li>
                            <li>Penampilan menarik dan tetap sehat
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block2">
            <div class="limit">
                <div class="row">
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-1.jpg') }}"/>
                        <span>Mengecilkan pipi berisi
                          </span>
                    </div>
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-2.jpg') }}"/>
                        <span>Pinggang langsing
                          </span>
                    </div>
                </div>
                <div class="row">
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-3.jpg') }}"/>
                        <span>Panggul ramping
                          </span>
                    </div>
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-5.jpg') }}"/>
                        <span>Kaki elegan
                          </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block3 with-gradient">
            <div class="limit">
                <p class="title ttu">
                    <span>OBESITAS</span>
                    1950-2010
                </p>
                <div class="graph">
                    <img alt="" src="{{ asset('build/images/frontend/block3-graph.png') }}"/>
                </div>
                <div class="info">
                    Menurut laporan WHO yang dipresentasikan di AS pada Maret 2010, jumlah orang dengan kelebihan berat badan terus meningkat.
                </div>
            </div>
        </div>
        <div class="block block4">
            <div class="limit">
                <p class="title">
                        <span class="ttu">DAPATKAN PINGGANG SEMPURNA!
                        </span>
                    <em>Buat tubuh Anda ramping dengan cara cepat dan sederhana
                    </em>
                </p>
            </div>
        </div>
        <div class="block block5">
            <div class="limit">
                <img alt="" src="{{ asset('build/images/frontend/block4-light.png') }}" width="200"/>
                <p class="label">
                    <span>ANDA DAPAT MAKAN MAKANAN FAVORIT ANDA SETIAP HARI!</span>
                </p>
                <ol>
                    <li>Tanpa pantangan makanan - tanpa stres!
                    </li>
                    <li>Tanpa diet ketat - tanpa penderitaan!
                    </li>
                    <li>Penurunan berat alami dan hasilnya bertahan selamanya!
                    </li>
                </ol>
            </div>
        </div>
        <div class="block block6 with-gradient">
            <div class="limit">
                <p class="title1 ttu">TURUNKAN
                    <br/> BERAT BERLEBIH
                </p>
                <p class="title2">Hanya satu paket cukup untuk mencapai hasil yang diharapkan
                </p>
                <img alt="" src="{{ asset('build/images/frontend/prod.png') }}"/>
                <p class="title3 ttu">INSTITUT NUTRISI KESEHATAN MEREKOMENDASIKAN METODE AMPUH MENURUNKAN BERAT BADAN
                </p>
                <div class="description">
                    <p>Garcinia Cambogia Forte mengandung konsentrat yang secara aktif menghancurkan timbunan lemak di dalam tubuh. Meningkatkan fungsi metabolisme, detoksifikasi tubuh Anda, dan membuang radikal bebas.
                    </p>
                    <p>Anti oksidan alami menjaga kesehatan dan membuat penampilan menjadi makin menarik
                    </p>
                </div>
                <a class="btn btn-primary for-event scroll">PESAN SEKARANG
                </a>
            </div>
        </div>
        <div class="block block7">
            <div class="limit">
                <p class="title1 ttu">Garcinia Cambogia Forte
                </p>
                <p class="title2">OBAT PENURUN LEMAK TANPA LAPAR DAN OLAHRAGA YANG MELELAHKAN
                </p>
                <div class="row">
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block8-1.jpg') }}"/>
                            <div>
                                <span>PENGARUH KE JARINGAN TUBUH</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block8-2.jpg') }}"/>
                            <div>
                                <span>PEMBAKARAN LEMAK</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block8-3.jpg') }}"/>
                            <div>
                                <span>PEMBERSIHAN TUBUH</span>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block8-4.jpg') }}"/>
                            <div>
                                <span>PENINGKATAN PENAMPILAN</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block8">
            <div class="dark-title">
                <div class="limit">
                    <div class="table">
                        <span class="ttu label">RAHASIA KEAMPUHAN Garcinia Cambogia Forte</span>
                        <span class="value">Komposisi Garcinia Cambogia Forte: ekstrak dari buah Malabar Tamarind</span>
                    </div>
                </div>
            </div>
            <div class="limit">
                <div class="item">
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-1.jpg') }}"/>
                    </div>
                    <div class="description with-label">
                        <span>KONSENTRASI</span>
                        <p>Tertinggi 95% Garcinia dalam Garcinia Cambogia Forte sudah tersedia di pasaran Indonesia.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-2.jpg') }}"/>
                    </div>
                    <div class="description with-label">
                        <span>JENIS</span>
                        <p>Ekstrak 95% Garcinia Cambogia diambil dari buah Malabar Tamarind, berkualitas dan bernilai tinggi.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-3.jpg') }}"/>
                    </div>
                    <div class="description with-label">
                        <span>CARA KERJA</span>
                        <p>Meningkatkan asam lambung dan metabolisme tubuh, memecah sel-sel lemak dan menghambat proses pengendapan lemak.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block9 with-gradient">
            <div class="limit">
                <p class="title1 ttu">PENURUNAN BERAT CEPAT DENGAN Garcinia Cambogia Forte:
                </p>
                <p class="title2 ttu">HASIL MENAKJUBKAN!
                </p>
                <p class="title3">Perbandingan sebelum dan sesudah:
                    Turunnya lingkar lengan atas, panggul, dan pinggang secara berkelanjutan!
                </p>
                <div class="compare clearfix">
                    <div class="side before">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block9-before.png') }}"/>
                            <span class="weight">
                              83 KG
                            </span>
                        </div>
                        <span class="tooltip tooltip-shoulder">
                            Lengan atas - 19 inci
                          </span>
                        <span class="tooltip tooltip-waist">
                            Pinggang - 43 inci
                          </span>
                        <span class="tooltip tooltip-hip">
                            Panggul - 52 inci
                          </span>
                    </div>
                    <div class="side after">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block9-after.png') }}"/>
                            <span class="weight">
                              55 KG
                            </span>
                        </div>
                        <span class="tooltip tooltip-shoulder">
                            Lengan atas - 11 inci
                          </span>
                        <span class="tooltip tooltip-waist">
                            Pinggang - 28 inci
                          </span>
                        <span class="tooltip tooltip-hip">
                            Panggul -36 inci
                          </span>
                    </div>
                </div>
                <p class="title4 ttu">Garcinia Cambogia Forte
                </p>
                <p class="title5 ttu">
                    PEMBAKAR LEMAK
                </p>
                <p class="title6">
                    dari bahan alami
                </p>
                <div class="row chain">
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block9-info1.png') }}"/>
                    </div>
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block9-info2.png') }}"/>
                    </div>
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block9-info4.png') }}"/>
                    </div>
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block9-info5.png') }}"/>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block10">
            <div class="limit">
                <p class="title1 ttu">Garcinia Cambogia Forte
                </p>
                <p class="title2">Rahasia penurunan berat tanpa diet dan olahraga dari ukuran 14 ke 8
                </p>
                <div class="row">
                    <div class="before">
                        <div>
                            <span>
                              SEBELUM
                            </span>
                            <img alt="" src="{{ asset('build/images/frontend/block12-before1.jpg') }}"/>
                        </div>
                    </div>
                    <div class="after">
                        <div>
                            <span>
                              SESUDAH
                            </span>
                            <img alt="" src="{{ asset('build/images/frontend/block12-after1.jpg') }}"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block11">
            <div class="limit">
                <p class="title ttu">KISAH NYATA PENURUNAN BERAT BADAN
                </p>
                <div class="content slider">
                    <div class="item">
                        <div class="image">
                            <span class="minus">-15 kg</span>
                            <img alt="" src="{{ asset('build/images/frontend/block13-1.jpg') }}"/>
                            <p class="result">
                              <span class="before">
                                SEBELUM: 74 kg
                              </span>
                                <span class="after">
                                SESUDAH: 59 kg
                              </span>
                            </p>
                        </div>
                        <p class="label">BERAT BERLEBIH HILANG! SAYA TERKEJUT!
                        </p>
                        <p class="duration">PERIODE PENGGUNAAN: 3 BULAN
                        </p>
                        <p class="name">ANDITYA M, 29:
                        </p>
                        <p class="story">Selama hidup saya mungkin sudah mencoba ratusan cara diet dan tidak ada yang berhasil. Berat saya selalu stabil -74 kg. Kadang saya merasa kalau minum air saja berat badan saya naik! Untungnya, rekan saya merekomendasikan untuk mencoba Garcinia Cambogia Forte. Saya kagum dengan hasilnya! Hanya perlu waktu 3 minggu untuk turun 10 kg dan 5 kg sisanya turun tidak lama setelahnya. Saya tidak henti-hentinya mengagumi tubuh baru saya! Tambahan lagi - beratnya "tidak kembali lagi".
                        </p>
                    </div>
                    <div class="item">
                        <div class="image">
                            <span class="minus">-26 kg</span>
                            <img alt="" src="{{ asset('build/images/frontend/block13-2.png') }}"/>
                            <p class="result">
                              <span class="before">
                                SEBELUM 78 KG
                              </span>
                                <span class="after">
                                SESUDAH 52 KG
                              </span>
                            </p>
                        </div>
                        <p class="label">PENURUNAN BERAT LUAR BIASA... 26 KG!
                        </p>
                        <p class="duration">PERIODE PENGGUNAAN: 6 BULAN
                        </p>
                        <p class="name">SINTA H, 31
                        </p>
                        <p class="story">
                            Saya bahkan tidak sadar bagaimana berat saya bisa sampai 78 kg. Saya putuskan untuk berubah. Sayangnya, semua diet yang saya lakukan hasilnya mengecewakan. Semuanya tidak berguna! Lalu rekan saya memberitahu saya tentang Garcinia Cambogia Forte. Sekarang saya merekomendasikannya kepada siapa saja karena saya telah merasakan langsung keampuhannya. Akhirnya berat badan saya turun! Berat saya turun 10 kg dalam bulan pertama dan perlahan-lahan sisanya terus turun. Kini kesehatan saya jauh membaik dan saya menjadi sangat bugar dibanding sebelumnya!
                        </p>
                    </div>
                    <div class="item">
                        <div class="image">
                            <span class="minus">-21 kg</span>
                            <img alt="" src="{{ asset('build/images/frontend/block13-3.jpg') }}"/>
                            <p class="result">
                                <span class="before">SEBELUM: 64 KG</span>
                                <span class="after">SESUDAH: 43 KG</span>
                            </p>
                        </div>
                        <p class="label">SIAPA LAGI YANG BERNIAT TURUN 21 KG?
                        </p>
                        <p class="duration">PERIODE PENGGUNAAN: 2 BULAN
                        </p>
                        <p class="name">AMEL A, 20:
                        </p>
                        <p class="story">
                            Saya ingin memiliki bentuk tubuh impian dan sudah mencoba segala macam metode modern: diet, pil, teh, krim, dan mesin olahraga. Saya berhasil menurunkan beberapa kilogram berat badan namun kemudian berat badan saya naik lagi dengan cepat. Garcinia Cambogia Forte membuat saya kagum dengan keampuhannya! Hanya dalam beberapa bulan konsumsi penampilan saja meningkat dengan signifikan: kulit muka saya menjadi kencang, pinggang semakin ramping, dan semua selulit hilang! Saya lalu mencoba minum kopi ini tanpa diet, tenyata tetap berhasil!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="warning">
            <div class="limit">
                <p>
                    Hati-Hati dengan produk palsu yang di jual di website lain karena produk asli hanya dari website kami dan call center kami. kami tidak bertanggung jawab atas produk palsu yag di beli d pasaran atau website lain
                </p>
            </div>
        </div>
        <div class="block block12 with-gradient">
            <div class="limit">
                <p class="title1">99% YANG MENGUJICOBA Garcinia Cambogia Forte MENGAKUI KEAMPUHANNYA
                </p>
                <p class="title2 ttu">RAHASIA PENURUNAN BERAT JENNIFER LOVE HEWITT
                </p>
                <img alt="" src="{{ asset('build/images/frontend/block14-jenny.png') }}"/>
                <div class="story">Akhir-akhir ini, aktris Hollywood ternama, Jennifer Love Hewitt muncul kembali di hadapan publik setelah lama tidak muncul. Dia membuat banyak kejutan: setelah melahirkan anak keduanya, Jennifer berhasil menurunkan semua berat kehamilannya dan menjadi langsing kembali! Sebelumnya, Daily Mail melaporkan bahwa selama beberapa tahun terakhir, Jennifer telah mengikuti diet ketat secara konsisten, tetapi harus berhenti sejenak selama masa kehamilannya. Aktris ini bahkan bercanda dan mengatakan bahwa tubuhnya menjadi “sebesar gajah”. Tiga tahun setelah kelahiran putri pertamanya, Love Hewitt kembali cantik seperti sebelumnya. Bahkan, media mencurigai bahwa aktris ini memiliki anoreksia. Tetapi, dia menghapus semua kecurigaan itu dan tertawa, “Apa menurut kalian anoreksia atau diet hanyalah satu-satunya cara untuk menurunkan berat? Aku tidak sakit dan aku tidak melakukan diet ketat apa pun. Aku terlalu sibuk untuk melakukannya.” Rahasia penurunan berat Jennifer yang cepat telah diungkap oleh ahli nutrisi pribadinya: olahraga rutin, diet sehat yang seimbang, dan konsumsi pembakar lemak yang berasal dari tumbuhan.
                </div>

                <div class="form form-bottom">
                    <p class="title1 ttu">PESAN SEKARANG JUGA</p>
                    <p id="request-form-hash" class="title2 ttu">Garcinia Cambogia Forte</p>

                    {!! form_start($form, ['id' => 'request-form"', 'class' => 'form']) !!}
                    {!! form_errors($form->first_name) !!}
                    {{--{!! form_errors($form->last_name) !!}--}}
                    {!! form_errors($form->phone) !!}
                    <div class="fields">
                        <div class="form-group">
                            {!! form_widget($form->first_name) !!}
                        </div>
                        {{--<div class="form-group">--}}
                            {{--{!! form_widget($form->last_name) !!}--}}
                        {{--</div>--}}
                        <div class="form-group">
                            {!! form_widget($form->phone) !!}
                        </div>
                        <div class="price">
                            <s>{{ StrHelper::spaceInPrice($product1->price_old)}} {{$product1->currency}}</s>
                            <span>HARGA BARU: {{ StrHelper::spaceInPrice($product1->price)}} {{$product1->currency}}</span>
                        </div>
                        {!! form_widget($form->productId, ['value' => $product1->id]) !!}
                        <div class="form-group buttons">
                            <button class="ttu btn btn-primary btn-block js_submit" type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();">Pesan</button>
                        </div>
                    </div>
                    {!! form_end($form, false) !!}

                </div>
            </div>
        </div>
    </div>
@endsection
