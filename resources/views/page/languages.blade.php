@extends('layout.app')
@section('title') :: {{ trans('menu.languages') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')

	<main>
		<section class="faq">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-md-8">
						<section class="current-lang">
							<h1><strong>{{ $lang->name }}</strong></h1>

							<div class="opinion">
				                <span class="img">
				                  <img src="{{ asset('build/images/krebs.png') }}" alt="author"/>
				                </span>

								@if($pages->find(18))
									<p class="content">
										{!! $pages->find(18)->content !!}
										<span>- Emil Krebs</span>
									</p>
								@endif
							</div>
							<table class="preview">
								<tbody>
								<tr>
									<td class="hidden-xs">
										<img src="{{ $lang->image_path }}" alt="{{ $lang->name }}"/>
									</td>
									<td class="hidden-xs about" style="color:#111;">
										{!! Cookie::has('10days')?str_replace('30 hari','10 hari',$lang->description_short):$lang->description_short !!}
									</td>
									<td class="price">
										@if($isPromotion)
											<p>
												<span>{{ $lang->currency }}</span> {{ number_format($lang->price,0,',','.') }}
											</p>
											<span class="old">{{ $lang->currency }} {{ number_format($lang->price_old,0,',','.') }}</span>
											<strong class="info">{{ trans('languages.promotion.text') }}</strong>
										@else
											<p>
												<span>{{ $lang->currency }}</span> {{ number_format($lang->price_old,0,',','.') }}
											</p>
										@endif
									</td>

								</tr>
								</tbody>
							</table>

						</section>
						<h2>FAQ</h2>

						<ul class="ico-list">
							@foreach($faqs as $key => $faq)
								<li>
						            <span class="img">
						              <img src="{{ $faq->icon_path }}" alt="ico" class="img-responsive"/>
						            </span>

									<div class="content">
										<h3>

										@if (Session::has('asp') && Session::get('asp') == 7)
											{{ str_replace('Seberapa jauh yang saya dapat setelah 30 hari?','Apa yang saya dapat setelah menyelesaikan seluruh

materi pelajaran?',$faq->name) }}
										@else
											{{ $faq->name }}
										@endif
										</h3>

										<p>{!! $faq->description !!}</p>
									</div>
								</li>
							@endforeach
						</ul>

					</div>

					<aside class="col-xs-12 col-md-4">
						<section class="order">
							<a name="buy"></a>
							{!! form_start($buyForm,['class' => 'buy-form','data-container' => 'buy-languages-form']) !!}
							@if($isPromotion)
								<div id="promotion-holder">
									<div class="promo">
										<h3>{{ trans('welcome.slider.promition.header') }}</h3>

										<div id="timer" class="timer-wrapper-time"></div>
										<table>
											<tbody>
											<tr>
												<td>{{ trans('welcome.slider.promition.header.hours') }}</td>
												<td>{{ trans('welcome.slider.promition.header.minutes') }}</td>
												<td>{{ trans('welcome.slider.promition.header.seconds') }}</td>
											</tr>
											</tbody>
										</table>
									</div>
									<div class="avb" data-container="available-gone" data-available-free="{{ $settings->available_free }}" data-available-gone="{{ $settings->available_gone }}" data-available-treshold-from="{{ $settings->available_treshold_from }}" data-available-treshold-to="{{ $settings->available_treshold_to }}">
										<p>
											<span data-container="available-free-number"><b></b> {{ trans('checkout.available') }}</span>
											<span class="separator"> / </span>
											<span class="sold" data-container="available-gone-number"><b></b> {{ trans('checkout.sold') }}</span>
										</p>
									</div>
								</div>
							@endif

							<div class="options" id="options">
								<h3>{{ trans('languages.order.form') }}</h3>
								<h4>{{ trans('languages.choose.language') }}</h4>
								<ul class="language-choose">
									@foreach($languages as $language)
										@if($language->is_active)
											<li>
												<label for="lang-{{ $language->id }}-package-a">
													<img src="{{ $language->icon_path }}" alt="{{ $language->name_short }}"/>
													{!! Form::radio('languages[]',$language->id,isset($lang) && $lang->id == $language->id,['id' => 'lang-'.$language->id.'-package-a']) !!}
													<p>{{ $language->name_short }}</p>
												</label>
											</li>
										@endif
									@endforeach
								</ul>
								<p id="current"></p>
							</div>

							<div class="info">
								{!! form_widget($buyForm->first_name,['attr' => ['class' => '']]) !!}
								{!! form_widget($buyForm->last_name,['attr' => ['class' => '']]) !!}
								{!! form_widget($buyForm->email,['attr' => ['class' => '']]) !!}
								{!! form_widget($buyForm->phone,['attr' => ['class' => '']]) !!}
							</div>

							<div class="action">
								{!! form_widget($buyForm->submit,['attr' => ['class' => '']]) !!}
							</div>
							{!! form_end($buyForm, false) !!}
						</section>
					</aside>

				</div>
			</div>
		</section>

		<section class="faq">
			<div class="container">
				<div class="col-xs-12">
					<ul class="list-inline actions">
						@if($lang->is_active)
							<li>
								{{--
								<a href="{{ route('page.try') }}" type="button" class="trigger">{{ trans('welcome.slider.try.lesson') }}</a>
								--}}
								<a href="#" class="trigger" type="button" data-toggle="modal" data-target="#{{ !Session::has('try')?'try':'test-lesson' }}">{{ trans('welcome.slider.try.lesson') }}</a>

							</li>
							<li>
								<a href="#buy" data-action="buy-languages-multi-button" data-id="{{ $language->id }}">{{ trans('welcome.slider.order') }}</a>
							</li>
						@else
							<li>
								<a href="#" class="trigger" type="button" data-toggle="modal" data-target="#inactive">{{ trans('welcome.slider.try.lesson') }}</a>
							</li>
							<li>
								<a href="#" type="button" data-target="#inactive" data-toggle="modal">{{ trans('welcome.slider.order') }}</a>
							</li>
						@endif
					</ul>
				</div>
			</div>
		</section>

		@include('part.fb_comments')
	</main>

@endsection

@section('scripts')
	<script type="text/javascript">
		$(function () {
			App.buy.available.init();
		});
	</script>
@endsection