@extends('layout.mask',[
    'title' => 'Kisah Nyata Cowok Bali',
    'meta_description' => 'Kisah Nyata Cowok Bali',
    'meta_keywords' => 'Kisah Nyata Cowok Bali'
])

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bmask/all.css') }}" media="all">
    <link rel="stylesheet" href="{{ elixir('css/rahasia/all.css') }}">
@endsection

@section('analytics')
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-83521033-7', 'auto');
        ga('send', 'pageview');

    </script>

    <!-- Segment Pixel - INDO - Digital Commerce - Garcinia - DO NOT MODIFY -->
    <img src="‪https://secure.adnxs.com/seg?add=7124299&t=2‬" width="1" height="1" />
    <!-- End of Segment Pixel -->
@endsection

@section('content')
    <div id="header" style="background: none; height:50px; padding-top:0px;">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="http://health.medical-jurnal.com">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="http://health.medical-jurnal.com">Fitness & Olahraga</a></li>
                            <li><a href="http://health.medical-jurnal.com">Tips Kesehatan</a></li>
                            <li><a href="http://health.medical-jurnal.com">Makanan & Diet Sehat</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    {{--<div class="header-main">--}}
        {{--<a class="logo" href="{{ route('page.maskPreOrder') }}">StarHit</a>--}}
        {{--<div class="header-news">--}}
            {{--<div class="item">--}}
                {{--<a href="http://health.medical-jurnal.com/2016/08/18/tanpa-harus-merasa-lapar">--}}
                    {{--<img alt="" src="{{ asset('build/images/frontend/8-1-696x464.jpg') }}" class="photo">--}}
                    {{--<span class="title">Tanpa Harus Merasa Lapar</span>--}}
                {{--</a>--}}
                {{--<div class="info">Penelitian menunjukkan bahwa...</div>--}}
            {{--</div>--}}
            {{--<div class="item">--}}
                {{--<a href="http://health.medical-jurnal.com/2016/09/27/masker-buah-tomat-untuk-kulit-wajah">--}}
                    {{--<img alt="" src="{{ asset('build/images/frontend/bagaimana.jpg') }}" class="photo">--}}
                    {{--<span class="title">Buah Tomat Untuk Kulit Wajah</span>--}}
                {{--</a>--}}
                {{--<div class="info">Bagaimana cara membuat masker?</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="header-banner">
        <div title="" id="adriver_banner_316775540"></div>
    </div>
    <div class="wrap">
        <div class="content">

            <div class="primary">
                <div class="main" style="width:70%;">
                    <div class="crumbs">
                        <a href="http://health.medical-jurnal.com" class="crumbs-main">Homepage</a>  /
                        <a href="http://health.medical-jurnal.com" class="crumbs-section">Tips Kesehatan</a>  /
                        <span class="crumbs-section">Kisah Nyata Cowok Bali: Cara Mudah Hasilkan Uang Dari Orang Bule!</span>
                    </div><br>
                    <h1 class="article-title">
                        Kisah Nyata Cowok Bali: Cara Mudah Hasilkan Uang Dari Orang Bule!
                    </h1>
                    <h2 class="article-announce"> </h2>
                    <h2 class="article-announce"> </h2>
                    <div class="article-social-line">
                        <span class="article-date">2016-10-17 08:00</span>
                    </div>
                    <div class="article-container">
                        {{--<div class="article-side">--}}
                            {{--<div class="block article-new-block">--}}
                                {{--<div class="title"><span>Bamboo Charcoal Black Mask</span></div>--}}
                                {{--<ul class="list">--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">100% komposisi alami</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Vitamin-vitamin 8 kali lebih kuat</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Jaminan kepuasan</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Efektivitas maksimal</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Tanpa efek samping</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="article-body">
                            <div class="figure">

                            </div><br>

                            <p><b>Kisah yang Anda baca dibawah ini adalah nyata dan terjadi di Bali.</b></p>

                            <a href="https://krebsmethod.co.id" class="button" style="display:block!important; text-align:center; background: #D20272;">COBA Bahasa Inggris Sampel Pelajaran GRATIS</a>

                            <p></p><center><img src="{{ asset('build/images/frontend/kisah6.jpg') }}" width="600" alt=""></center><p></p>

                            <p>Setiap tahun jutaan orang bule datang ke pulau Bali untuk mencari matahari dan kesenangan. <a href="https://krebsmethod.co.id">Kemampuan bahasa Inggris yang sangat baik</a> adalah aset yang bernilai, jika Anda tahu <a href="https://krebsmethod.co.id">bagaimana menginvestasikannya dengan baik.</a></p>

                            <p></p><center><img src="{{ asset('build/images/frontend/kisah2.jpg') }}" width="600" alt=""></center><p></p>

                            <p>Bagi Anda yang ingin tahu bagaimana cara mudah hasilkan uang dari orang bule, berikut ini wawancara dengan Indra Sudirman, seorang instruktur surfing berusia 27 tahun. Indra menceritakan tentang <a href="https://krebsmethod.co.id">fenomena cara sederhana: apa yang membuat orang bule jauh lebih terbuka</a> dan bagaimana cara membuka... dompet mereka? Kenapa mereka terus saja berdatangan ke Sekolah Surfing Indra Sudirman? Ada apa di cowok ini? Apa daya tariknya? </p>

                            <p><img src="{{ asset('build/images/frontend/kisah3.jpg') }}" width="350" alt="" align="left" style="margin:10px;">

                            <p><b>Redaksi:</b> Sebagai instruktur surfing, Anda sudah mengajar 714 gadis bule, apakah itu benar?</p>

                            <b>Indra Sudirman:</b> Lumayan ya… Semua laki-laki buaya, cuma aku yang komodo! Haha… Ya, itu benar. Sebenarnya, ada banyak instruktur surfing yang sehari-harinya mencari nafkah di pantai di Kuta atau Seminyak, tapi hanya aku yang tahu gimana cara mudah hasilkan uang dari orang bule!</p>
                            <p>(Dia tertawa. Pelancong bule mulai berjemur di Pantai Kuta. Mereka hanya mengenakan celana dalam dan kutang. Ada juga yang tidur berjemur tanpa menggunakan bra dengan posisi tengkurap. Aktivitas seperti itu sudah biasa di Bali.)</p>

                            <p><b>Redaksi:</b> Bagaimana caranya? Kita semua tahu bahwa tidak semua gadis bule bersedia untuk membuka dompet mereka untuk belajar surfing. Pasti Anda punya <a href="https://krebsmethod.co.id">pattern yang unik untuk</a> mengajak orang bule belajar dengan Anda…</p>

                            <p><b>Indra Sudirman:</b> Biasanya aku merayu para gadis di cafe. Aku mendekati gadis yang kelihatan sendirian, sedikit berbasa-basi…</p>

                            <p><b>Redaksi:</b> Dalam bahasa Inggris?</p>

                            <p><b>Indra Sudirman:</b> Tentu aja! Aku <a href="https://krebsmethod.co.id">bisa lancar ngomong Inggris</a>. Dengan bahasa Inggris, aku membuat para gadis lebih terbuka ya, aku membuat para gadis terpesona, karena aku berbicara dalam bahasa mereka! Lalu perlahan berganti topik dan aku mulai menceritakan tentang surfing ya. Kadang-kadang aku mengajar orang Indonesia, tapi biasanya aku „menggoda” turis bule.</p>

                            <p><b>Redaksi:</b> Kenapa?</p>

                            <p><b>Indra Sudirman:</b> Aku lebih suka dikencani bule aja… Haha… Aku membuat mereka terkesan dengan <a href="https://krebsmethod.co.id">keterampilan aku dalam bahasa Inggris</a>. Setelah selesai „merayu”, aku mulai lakukan pendekatan yang lebih lagi. Biasanya mengajak gadis tersebut keluar berdua, dan lalu mengajaknya pergi ke pantai, ke Sekolah Surfing Indra Sudirman! </p>

                            <p></p><center><img src="{{ asset('build/images/frontend/kisah4.jpg') }}" width="600" alt=""></center><p></p>

                            <p><b>Redaksi:</b> Dengan mudah?</p>

                            <p><b>Indra Sudirman:</b> Mudah sekali, mereka menerimaku dengan tangan terbuka, dengan dompet terbuka, karena <a href="https://krebsmethod.co.id">bahasa Inggrisku lancar dan fasih!</a> Ini bukan sihir, ini hasil nyata: semakin lancar bahasa Inggrisku semakin mudah hasilkan uang! Nilai seseorang akan lebih tinggi apabila <a href="https://krebsmethod.co.id">menguasai bahasa Inggris!</a></p>

                            <p><b>Redaksi:</b> Memang ada… Tapi seperti kita ketahui, biasanya, setelah ribuan jam belajar bahasa Inggris, kita masih saja merasa sangat sulit untuk bercakap-cakap dalam bahasa Inggris… Dan banyak pelajar mengaku masih kerap menghadapi permasalahan ketika mempelajari bahasa Inggris... </p>

                            <p><b>Indra Sudirman:</b> Memang harus aku akui kemampuan bicara dalam bahasa Inggris masyarakat Indonesia masih kurang. Biasanya orang Indonesia merasa malu berbicara dalam bahasa Inggris. Yang paling penting, mereka perlu <a href="https://krebsmethod.co.id">cara untuk mengatasi takut berbicara!</a> Hal ini adalah hal yang sangat wajar. Kemampuan untuk bercakap-cakap dalam bahasa Inggris perlu dilatih.</p>

                            <p><b>Redaksi:</b> Ya, itu usul baik. Tapi kebanyakan orang tak punya waktu untuk belajar bahasa Inggris. Atau uang untuk guru les.</p>

                            <p><b>Indra Sudirman:</b> Ya, sebenarnya menurut aku, kursus itu nggak akan efektif. Apalagi kursus tradisional atau guru les harganya sangat mahal sehingga tidak terjangkau oleh masyarakat kebanyakan.</p>
                            <p>(Dia diam beberapa detik.)</p>
                            <p>Kita sama-sama orang Indonesia, kita dapat mengatakan dengan hati terbuka... <a href="https://krebsmethod.co.id">Aku merekomendasikan metode Emil Krebs</a>, karena jauh lebih mudah, cepat dan murah daripada kursus lain atau guru les apapun!</p>

                            <p><b>Redaksi:</b> Maksudnya, inilah senjata rahasia Anda?</p>

                            <p><b>Indra Sudirman:</b> Ya, inilah <a href="https://krebsmethod.co.id">rahasia kesuksesan</a> aku. Dengan <a href="https://krebsmethod.co.id">metode Krebs</a> aku tak hanya belajar bahasa Inggris, melainkan belajar cara berbicara dengan dunia! Misalnya, dengan 714 gadis bule itu. <a href="https://krebsmethod.co.id">Metode Krebs</a> membantu mengatasi takut berbicara!</p>
                            <p>(Dia tertawa, lalu diam beberapa detik.)</p>
                            <p>Sulit dipercaya tapi memang ada: aku berhasil <a href="https://krebsmethod.co.id">menguasai bahasa Inggris dalam 40 pelajaran</a> tanpa buku, tanpa pengulangan membosankan, tanpa usaha apa pun! <a href="https://krebsmethod.co.id">Metode Krebs</a> adalah cara ampuh.</p>

                            <p><b>Redaksi:</b> Sangat menarik! Untuk berkomunikasi dengan lancar dalam bahasa Inggris… Apakah tersedia di Indonesia?</p>

                            <p><b>Indra Sudirman:</b> Ya! Aku, pacarku, semua temenku sudah <a href="https://krebsmethod.co.id">jatuh cinta pada metode Krebs</a>, karena <a href="https://krebsmethod.co.id">belajar dengan mudah dan cepat!</a> Yang harus Anda lakukan adalah duduk dengan nyaman dan mendengarkan, santai seperti di pantai…</p>
                            <p>(Kami tertawa. Tiba-tiba seorang bule bertanya kepada kami: „Hi guys, where's Jalan Poppies?” Indra menjawab <a href="https://krebsmethod.co.id">dalam bahasa Inggris dengan sempurna!</a>)</p>

                            <p><b>Redaksi:</b> Waduh, bahasa Inggris Anda memang <a href="https://krebsmethod.co.id">lancar dan fasih!</a> Indra, terima kasih atas wawancara ini dan semoga sukses dalam kehidupan pribadi dan pekerjaan Anda!</p>

                            <p><b>Indra Sudirman:</b> Sama-sama! Jangan lupa coba <a href="https://krebsmethod.co.id">metode Krebs</a>, Anda akan terkejut dengan hasilnya!</p>

                            <p class="clear"></p>

                            <p></p><center><img src="{{ asset('build/images/frontend/kisah5.jpg') }}" width="600" alt=""></center><p></p>

                            <a href="https://krebsmethod.co.id" class="button" style="display:block!important; text-align:center; background: #D20272;">COBA Bahasa Inggris Sampel Pelajaran GRATIS</a>

                            <div class="comments-block">
                                <div class="title">Komentar-komentar</div>
                                <div class="container">
                                    <ul class="list">

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Nany</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Ini bisa dipakai anak-anak dan dewasa kan?</p></div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Ami</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Banyak orang mempelajari macam macam perbendaharaan kata tapi akhirnya hanya menggunakan sebagian dan lupa yang lain. Kita tidak memakai frasa dan sinonim yang sulit, kita hanya menggunakan yang termudah.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Sobari</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Gua selalu pengen belajar bahasa Inggris yang serius. Bukan buat keperluan kantor sih, tapi pengen tau aja rasanya bisa baca web bahasa Inggris atau nonton film serial, tau kan biar kerasa lebih internasional dan tau lebih tentang dunia. Tapi, gua harus akuin : Gua agak males. Atau emang gua ga mampu buat fokus dan konsentrasi ke buku pelajaran. Gua udah cobain beberapa pelajaran bahasa yang inovatif seperti Callan, metode 1000 kata, metode 5S, dll...</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Wan Nainggolan</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>saya sudah pesan daam bentuk CD</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rizkha</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Saya beli kursus ini denganmu. Sangat puas sekali!</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rakel</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>kak aku mu tanya ,aku tertarik dengan kursus ini .. apakah kursus ini efektif?</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rendy</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>gmn perkembangannya gan?ane mau beli klo mengesankan</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">anton123</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Ya!!rekomendasikan! Cara ampuh!</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">IesHa</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Ada yang udah coba?</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Melly Felicia</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Tadi aku udah download filenya. Baru aja selesai pelajaran pertama bener-bener gampang masuknya. Awalnya agak skeptis sih, tapi beneran ini bikin surprise.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Putri putra</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Bagus sekali!!!</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Irwan Taslim</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Saya juga sudah download dan sangaaaat puaas. Meskipun saya hanya mempelajarinya sesekali ketika ada liburan beberapa hari, saya masih ingat pelajaran terakhir yang saya dapat.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Muhamad</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Luarr biasa, saya tertarik untuk membeli dan mempelajarinya</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Karl</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Sangat me-Motivasi saya untuk mempelajari banyak Bahasa lagi, Selain 3 Bahasa yang saya bisa! Metode Tradisional ini tidak jauh berbeda dengan metode lainnya,Namun yang ini lebih Efektif,Cepat,dan Praktis! </p>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <center><a href="https://krebsmethod.co.id" class="button" style="background: #D20272;">COBA Metode Emil Krebs Sampel Pelajaran GRATIS</a></center>
                        </div>
                    </div>
                </div>
                <div style="float:right; width:29%;">
                    <a href="https://krebsmethod.co.id">
                        <img src="{{ asset('build/images/frontend/kisah_dalam.jpg') }}" height="250" width="365" style="border-radius:25px;" />
                    </a>
                </div>
                <a href="{{ route('page.maskPreOrder') }}"></a>
                <div class="right">
                    <div class="right-cell dontMiss">

                        {{--<table cols="1" style="margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr></tr>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: 1px solid rgb(109, 172, 216); padding: 0px;" align="left" valign="top">--}}
                        {{--<div class="abouttext">--}}
                        {{--<center></center>--}}
                        {{--</div>--}}
                        {{--<center>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 2px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v1.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Саня</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Хех. Неплохо выглядит Салтыкова)))--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--только что--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v2.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Валера</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Не думал, что ей уже 50 )--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--32 минуты назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v3.jpg') }}" width="50px">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Ниночка Ростина</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--У меня с лицом все впорядке, но знакомым покажу, хватает у меня друзей с прыщиками.--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--54 минуты назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v4.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Виктор Аников</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Брат в Америке живет, так вот говорит что там действительно ОЧЕНЬ популярен эта черная маска...--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v5.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Катёна Шантурова</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Впечатляет! Я бы тоже такую маску прикупила. Хочу лицо в порядок привести--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v6.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Ирина Лонгина</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Заказа себе небольшой курс, буду пробовать)--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}

                        {{--</center>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}

                    </div>
                    <div class="block banner-side-mid">
                    </div>
                </div><div class="clear"></div>
            </div>
        </div>
        {{--<div class="header">--}}
            {{--<div class="header-menu">--}}
                {{--<ul>--}}
                    {{--<li class="item novosti">--}}
                        {{--<a href="http://health.medical-jurnal.com"><span>HOMEPAGE</span></a>--}}
                    {{--</li>--}}
                    {{--<li class="item eksklusiv active">--}}
                        {{--<div><a href="http://health.medical-jurnal.com"><span>FITNESS & OLAHRAGA</span></a></div>--}}
                    {{--</li>--}}
                    {{--<li class="item interview">--}}
                        {{--<a href="http://health.medical-jurnal.com"><span>TIPS KESEHATAN</span></a>--}}
                    {{--</li>--}}
                    {{--<li class="item photoistorii">--}}
                        {{--<a href="http://health.medical-jurnal.com"><span>MAKANAN & DIET SEHAT</span></a>--}}
                    {{--</li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        {{--</div>--}}
    </div>
    @include('part.google')
@endsection