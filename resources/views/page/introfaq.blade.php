@extends('layout.garcinia')

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/garcinia/all.css') }}">
@endsection

@section('content')
    <div class="bar metamorphosis-bar">
        <div class="content">
            <h1>Mengenai Garcinia Cambogia Forte</h1>
        </div>
    </div>

    <section class="faq page-container">
        <h2>Mengenai Garcinia Cambogia Forte</h2>
        <div class="question">
            <div class="content">
                Apa fenomena terapi kuruskan badan dengan Garcinia Cambogia Forte?
            </div>
            <p class="answer hide">Ekstrak dari buah Malabar Tamarind meningkatkan asam lambung dan metabolisme tubuh. Garcinia Cambogia memecah sel-sel lemak dan memblokir proses pengendapan lemak, sehingga tubuh Anda membakar lemak dan kalori dengan mudah.</p>
        </div>
        <div class="question">
            <div class="content">
                Bagaimana cara membentuk kontur tubuh dengan Garcinia Cambogia Forte?
            </div>
            <p class="answer hide">Salah satu sifat dari Garcinia Cambogia adalah meningkatkan stamina tubuh. Selain itu, ekstrak dari buah Malabar Tamarind mengurangi kelelahan otot dan mempercepat regenerasinya. Dengan membakar lemak Garcinia Cambogia meningkatkan pertumbuhan otot. Oleh karena itu, suplementasi dengan Garcinia Cambogia direkomendasikan baik untuk orang yang mengalami kelebihan berat badan atau obesitas, yang ingin turun, maupun untuk orang yang berolahraga, yang ingin membentuk kontur tubuh.</p>
        </div>
        <div class="question">
            <div class="content">
                Apakah pemakaian Garcinia Cambogia Forte aman?
            </div>
            <p class="answer hide">Garcinia Cambogia Forte terdiri dari ekstrak tumbuhan-tumbuhan dan buah-buahan alami. Oleh karena itu, pemakaiannya alami dan aman untuk kesehatan tubuh manusia.</p>
        </div>
        <div class="question">
            <div class="content">
                Apakah ada kontraindikasi untuk digunakan?
            </div>
            <p class="answer hide">Jika ada alaergi terhadap komposisinya, jangan gunakan obat tersebut. Untuk Anda yang memiliki masalah dengan hipertensi, sangat baik saja jika Anda berkonsultasi dengan dokter Anda, sebelum menggunakan Garcinia Cambogia Forte.</p>
        </div>
        <div class="question">
            <div class="content">
                Bagaimana cara mengambil Garcinia Cambogia Forte?
            </div>
            <p class="answer hide">Harus mengambil 1 tablet sehari. Dianjurkan untuk mengambil di pagi hari, 15 menit sebelum sarapan, dengan segelas air putih. Obat tidak harus diambil 5 jam sebelum pergi tidur, karena guarana.</p>
        </div>
        <div class="question">
            <div class="content">
                Beraba lama terapi dengan Garcinia Cambogia Forte?
            </div>
            <p class="answer hide">Komposisinya terdiri dari Garcinia Cambogia tertinggi (95%), oleh karena itu waktu disarankan untuk terapi maksimal 3 bulan. Setelah terapi tersebut, direkomendasikan 2 bulan beristirahat. Lalu terapi dapat diulang untuk memperkuat hasilnya.</p>
        </div>
        <div class="question">
            <div class="content">
                Apakah ada efek samping setelah menggunakan Garcinia Cambogia Forte?
            </div>
            <p class="answer hide">Cara kerjanya alami dan tidak ada efek samping apapun. Penggunaanya aman, selain itu, obat tersebut dapat membantu untuk: mecegah rambut rontok, meningkatkan elastisitas kulit dan menhilangkan selulit.</p>
        </div>
        <h2>Cara pengiriman dan pembayaran</h2>
        <div class="question">
            <div class="content">
                Bagaimana cara melakukan pemesanan?
            </div>
            <p class="answer hide">Untuk melakukan pemesanan, silahkan isi formulir di website kami atau hubungi Call Center kami: 0888 01000 488</p>
        </div>
        <div class="question">
            <div class="content">
                Ongkos pengiriman Garcinia Cambogia Forte berapa?
            </div>
            <p class="answer hide">Ongkos pengiriman gratis. Tidak ada ongkos ekstra atau biaya apapun untuk pengiriman produk tersebut.</p>
        </div>
        <div class="question">
            <div class="content">
                Bagaimana dengan waktu pengiriman?
            </div>
            <p class="answer hide">Pengiriman Garcinia Cambogia Forte dilakukan melalui kurir atau pos dalam beberapa hari kedepan.</p>
        </div>
        <div class="question">
            <div class="content">
                Apakah saya dapat menikmati anonimitas lengkap?
            </div>
            <p class="answer hide">Kami memberikan keamanan dan anonimitas lengkap – kami sangat peduli dengan kenyamanan pelanggan kami. Kami kemas pengiriman dengan baik dan memiliki pengalaman yang kaya dalam kemasan, sehingga Anda tidak perlu khawatir tentang kemasan.</p>
        </div>
        <div class="question">
            <div class="content">
                Apakah Garcinia Cambogia Forte tersedia di apotek?
            </div>
            <p class="answer hide">Kemasan asli Garcinia Cmbogia Forte tersedia hanya melalui website kami resmi. Pada saat ini, obat tersebut dapat dipesan hanya melalui toko online kami.</p>
        </div>
        <div class="bottom">
        </div>
    </section>

    <script src="{{ asset('build/js/vendor/jquery.min.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('button.toggle-menu').click(function() {
                $(this).toggleClass('active');
                $('header#main > div.page-container nav').toggleClass('active');
            });
        });

        $('section.faq div.content').click(function() {
            $(this).parent().find('p.answer').toggleClass('show hide');
        });
    </script>

@endsection