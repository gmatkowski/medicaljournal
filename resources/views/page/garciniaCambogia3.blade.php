<html class="" lang="id">
<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">
    <meta name="poptm" content="98c1fc7c468b6f60a59dcbe45297dae5">
    <title>Cara menurunkan berat badan</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <link rel="stylesheet" href="{{ elixir('css/garciniaCambogia2/all.css') }}">

    @include('pixel.cambogia_main')

</head>
<body>
    <div class="widget visible">
        <div class="widgetWrap"><p class="sale-text"> diskon <span>50%</span></p>

            <div class="countdown"><span id="countdown-text">periode diskon terbatas:</span> <span id="hours">00</span>:<span id="min">46</span>:<span id="sec">46</span></div>
            <a href="#orderFormBorder" class="btn red">untuk memesan</a></div>
    </div>
    <div class="wrapper index2 container">
        <div class="row">
            <div class="content"><h1 class="uppercase">Anda bisa menurunkan berat hingga 15 KG DALAM 2 MINGGU, tanpa diet,
                    olahraga dan stres</h1>

                <div class="metamorphosis"><span class="title">„Tubuh ramping itu cantik -</span> <img class="right" src="{{ asset('build/images/frontend/7.jpg') }}" alt="Paulina Ochojska">

                    <div class="row">
                        <div class="left-side"><p>Kita semua tahu bahwa sosok yang indah dan ramping adalah faktor yang
                                mempengaruhi daya tarik. Tidak heran bahwa obesitas menyebabkan rasa malu... Dengan metode
                                ini saya mendapatkan bentuk tubuh ideal dan kepercayaan diri kembali, metode ini membuat
                                saya merasa seperti seorang wanita. Saya sudah kehilangan 24 kg, sekarang saya mencintai
                                hidup saya dan sangat sering menerima pujian. Saya senang ketika saya melihat refleksi di
                                cermin. Sekarang saya tahu bahwa mendapatkan bentuk tubuh ideal itu lebih mudah daripada
                                yang Anda bayangkan.”</p><span>- Nurfitria Dwisyanto, 29 thn, Singapura</span></div>
                    </div>
                </div>
                <section class="block-1 margin-top-20"><img src="{{ asset('build/images/frontend/spec-1-3.jpg') }}" class="block-1-img" alt="Radosław Matuszak">

                    <p class="margin-top-10">Nama saya Robert Anderson. Selama 14 tahun saya bekerja sebagai dokter dan
                        saya sudah membantu ratusan ribu pria dan wanita untuk menurunkan berat badan, mengurangi selulit
                        dan mendapatkan kembali tubuh langsing dan kuat.</p>

                    <p class="margin-top-10">Selama banyak tahun pengalaman, saya telah belajar hal yang sangat penting.
                        Untuk kehilangan kilo yang tidak diinginkan, dibutuhkan motivasi kuat dan kemauan untuk mengubah
                        gaya hidup. Mengapa?</p>

                    <p class="margin-top-15">Kebenaran mungkin mengejutkan Anda. Memang benar bahwa mengurangi makan atau
                        detoks diet – semuanya nihil untuk menurunkan berat badan Anda secara permanen. Anda perlu sesuatu
                        yang lebih kuat dan efektif untuk membuangkan kalori yang tidak diinginkan dalam tubuh, suatu cara
                        penurunan berat tubuh yang permanen dan aman. Saya telah mencoba banyak cara.</p>

                    <p class="margin-top-20">Coba satu cara yang inovatif untuk menurunkan berat badan Anda:</p>
                    <ul>
                        <li>turun hingga <b>21-35 kg</b> dalam 1 bulan</li>
                        <li>memblokir proses pengendapan lemak</li>
                        <li>memecah sel-sel lemak dan membentuk kontur tubuh</li>
                        <li>membantu tubuh membakar lemak hingga 82 persen</li>
                        <li>meningkatkan metabolisme tubuh</li>
                        <li>menghilangkan selulit dan stretch mark</li>
                    </ul>
                    <p class="margin-top-20">Hasil ini bisa disebabkan karena penemuan saya bekerja dalam 3 cara, yaitu:</p>

                    <h2 class="one-line margin-top-20 fronth1">MEMPERCEPAT METABOLISME TUBUH ANDA</h2>

                    <p class="bordered">Tidak ada program penurunan berat badan yang lengkap tanpa menjaga kesehatan sistem
                        pencernaan. Diet ketat bukan solusi terbaik, karena sel-sel dalam tubuh Anda membutuhkan gula
                        sebagai sumber energi, dan lemak membantu tubuh Anda tetap hangat. Yang harus dilakukan untuk
                        menyingkirkan obesitas adalah mempercepat metabolisme tubuh Anda efektif dan permanen.</p>

                    <h2 class="margin-top-20 fronth1">MENCEGAH PERTUMBUHAN SEL-SEL LEMAK BARU</h2>

                    <p class="bordered">Tubuh manusia diprogram untuk menyimpan lemak dan kalori sebagai cadangan energi,
                        ketika udara sekitar dingin atau kekurangan makanan. Anda tidak pernah berhasil menurunkan bobot
                        tubuh, kalau cadangan lemak tubuh tetap tidak terpakai. Penemuan saya mengganggu fungsi gen yang
                        membentuk lemak dan mencegah pertumbuhan sel-sel lemak baru (agar berat badan Anda tetap ideal dan
                        tidak mengalami sindrom yoyo ). Dengan cara ini, sel-sel lemak akan kehabisan tempat, dan lemak yang
                        tidak diinginkan akan dibakar, sehingga tubuh Anda tidak lagi menyimpan cadangan energi berupa
                        lemak. Tubuh Anda akan mulai kehilangan beberapa kilogram dan kembali ke bentuk tubuh ramping.</p>

                    <h2 class="one-line margin-top-20 fronth1">MENGURANGI SELULIT DAN MENGENCANGKAN KULIT TUBUH</h2>

                    <p class="bordered">Ketika Anda kehilangan beberapa kilogram berat badan, kulit akan longgar dan
                        mengendur. Penemuan saya memperbaiki kapasitas fisik tubuh dan tonus otot, dan efektif memecah
                        gumpalan lemak, sehingga mengurangi selulit yang ada di tubuh. Oleh karena itu, cara ini akan
                        membuat badan Anda lebih kuat hingga 40-46 persen - hasil dapat dilihat dalam 8 hari penggunaan.</p>

                    <p class="margin-top-20">Anda mungkin bertanya-tanya, apa ini, penemuan yang luar biasa? Ini adalah
                        ekstrak tumbuhan alami, dari Indonesia...</p>

                    <p class="margin-top-20">Untuk menurunkan berat badan secara cepat, berkhasiat dan hasilnya tahan lama,
                        harus menggunakan {{ $product1->name }} yang dimurnikan hingga kemurnian tinggi, tetapi aman.</p>

                    <p class="margin-top-20">Saya menghubungi produsen yang memasok kualitas tinggi murni {{ $product1->name }}
                        dari 95% ekstrak. Murni ini dijual dalam bentuk kapsul 520 mg, dengan nama dagang {{ $product1->name }}
                        Forte, dan mempunyai kualitas yang lebih baik dan tinggi dibandingkan dengan suplemen lain yang
                        tersedia di pasar.</p>

                    <p class="margin-top-20">Inilah ekstrak buah yang Anda makan setiap hari, Malabar Tamarind yang ada di
                        pasar. Zat yang dihasilkan oleh buah tersebut adalah {{ $product1->name }} - yang memiliki properti
                        penurunan berat badan. {{ $product1->name }} dari buah Malabar Tamarind, dengan dosis yang tepat, akan
                        membuat pencernaan menjadi lebih efektif hingga 89%.</p>

                    <p class="margin-top-20">{{ $product1->name }} membuat penurunan berat badan menjadi:</p>

                    <h2 class="one-line margin-top-20 fronth1">MUDAH</h2>

                    <p class="bordered">Sekarang Anda bisa tau cara langsing tanpa harus diet ketat, tanpa harus latihan
                        keras, bahkan Anda tidak mengubah pilihan makanan Anda. Dengan hanya 1 kapsul per hari, Anda akan
                        melihat penurunan berat badan terlihat, dalam beberapa hari.</p>

                    <h2 class="one-line margin-top-20 fronth1">ALAMI DAN AMAN</h2>

                    <p class="bordered">Ekstrak alami {{ $product1->name }} dari buah Malabar Tamarind, yang berasal dari India,
                        memiliki sifat menurunkan berat badan. Selain itu, {{ $product1->name }} juga memiliki beberapa manfaat
                        lain: menjaga kesehatan kondisi tubuh, meningkatkan kinerja fisik, membantu untuk mengurangi
                        kolesterol tinggi, mengatur tekanan darah, memfasilitasi penyerapan nutrisi dan vitamin dalam tubuh
                        Anda.</p>

                    <p class="margin-top-20">Mungkin Anda belum menyadari bahwa ini adalah cara yang paling efektif untuk
                        menurunkan berat badan dengan cara sehat dan hasilnya tahan lama, dan membuat sosok yang indah dan
                        ramping. {{ $product1->name }} adalah produk asli yang didukung oleh tes laboratorium, yang efektivitas
                        telah dikonfirmasi oleh 300 orang yang menggunakan {{ $product1->name }}.</p>

                    <div class="metamorphosis-alt" style="margin-top: 24px;"><span class="title">„Turun 14 kg tanpa diet apapun!”</span>
                        <img src="{{ asset('build/images/frontend/metamorphosis-alt.jpg') }}" id="photo">

                        <p>„Kebanyakan cara cepat menurunkan berat badan hanya memberikan efek sementara. Saya telah mencoba
                            begitu banyak metode penurunan berat badan tapi gagal dan gagal. Menurunkan bobot badan sulit
                            karena sindrom yoyo. Tatpi sekarang itu berbeda. Dengan {{ $product1->name }} saya telah kehilangan
                            14 kg. Hari ini menandai enam bulan sejak saat itu dan saya tidak mengalami sindrom ini. Saya
                            merasa cantik dan menarik, bahkan saya tidak obsesif menghitung kalori lagi.”</p>

                        <div class="clear-both"></div>
                        <div class="sign"> Tuti Putri Munas<br>dia turun 14 kg dalam 1,5 bulan. Sekarang dia menggunakan
                            pakaian kecil dan ketat.
                        </div>
                    </div>
                    <br>

                    <p>Memang, kita harus menyadari bahwa cara ini tidak sama untuk semua orang. Sebagian kecil orang yang
                        tidak boleh menggunakan {{ $product1->name }} karena masalah genetik atau sakit.</p>

                    <p class="margin-top-20">Namun, kami yakin {{ $product1->name }} Forte adalah cara yang paling efektif dan
                        aman untuk menurunkan berat badan, oleh karena itu kami memberikan Jaminan Kepuasan 100%.</p>

                    <h3>Satu-satunya cara yang memberikan jaminan kepuasan 100%</h3>

                    <p class="center">Kami memberikan Jaminan 100% kepuasaan hasil. Anda mendapatkan jaminan bahwa hasil yang akan Anda dapatkan pasti memuaskan, selama Anda menggunakan produk sesuai dengan aturan pemakaian yang berlaku.</p>

                    <h3>Bagaimana cara memesan {{ $product1->name }} Forte dan menghemat {!! StrHelper::spaceInPrice($product1->price_spec, $product1->price_old) !!} {{ $product1->currency }}?</h3>

                    <p>Kualitas terbaik ekstrak {{ $product1->name }} sulit untuk mendapat, jadi harganya tidak bisa murah.
                        Namun, produsen telah memberikan diskon khusus bagi pelanggan yang pertama kali melakukan pembelian
                        {{ $product1->name }} Forte. Anda cukup memesan hari ini, maka Anda akan mendapatkan diskon spesial
                        -57%, untuk alami dan kemurnian tinggi ekstrak {{ $product1->name }} dalam kapsul.</p>

                    <p class="margin-top-15">Hari ini ada promosi: kemasan {{ $product1->name }} Forte asli HANYA HARI INI
                        dijual dengan harga <span style="text-decoration: line-through;">{!! StrHelper::spaceInPrice($product1->price_old) !!} {{ $product1->currency }}</span> {!! StrHelper::spaceInPrice($product1->price_spec) !!} {{ $product1->currency }}
                        saja.</p>

                    <p class="margin-top-15">Besok harga akan naik!</p>

                    <p>Anda cukup klik pada tombol order dan mengisi formulir online dengan benar. {{ $product1->name }} Forte
                        akan kami kirimkan ke alamat Anda dalam waktu maks. 2 hari. Sekarang, apa yang Anda lakukan, adalah
                        mencoba {{ $product1->name }} ini.</p>

                    <p class="margin-top-20">Secara pribadi, saya yakin bahwa Anda akan sepenuhnya puas. Sebernanya saya
                        mengasumsi Anda akan menurunkan berat badan lebih cepat dari yang Anda bayangkan.</p>

                    <div style="margin-top: 20px; text-align: right;"><p>Semoga sukses!<br>Robert Anderson.</p></div>
                </section>
                <section>
                    <div class="product-order"><p><b>Kemasan asli</b>,<br>murni ekstrak {{ $product1->name }} <b>95%</b></p>

                        <div class="product-image"><img src="{{ asset('build/images/frontend/garciniacambogia.jpg') }}" alt="{{ $product1->name }} Forte">

                            <div class="price"><span>{!! StrHelper::spaceInPrice($product1->price_old) !!} {{ $product1->currency }}</span> {!! StrHelper::spaceInPrice($product1->price_spec) !!} {{ $product1->currency }}</div>
                        </div>
                        <p>Anda akan menghemat uang Anda jika membayar pada hari ini: mendapatkan diskon langsung sebesar
                            {{ $product1->currency }} {!! StrHelper::spaceInPrice($product1->price_spec, $product1->price_old) !!}, pengiriman gratis dan Jaminan Keputusan. Hanya hari ini!</p>

                        <div class="container">
                            <div id="orderFormBorder" class="col-md-7">
                                <p>Ya! Saya ingin mendapatkan Garcinia Cambogia Forte dengan promo dan jaminan kepuasan, HANYA
                                    <b>{{ $product1->currency }} {!! StrHelper::spaceInPrice($product1->price_spec) !!}</b> .. Promo TERBATAS!!!!</p>

                                <p>Pengiriman Garcinia Cambogia Forte dilakukan melalui kurir atau pos, dalam beberapa hari
                                    kedepan, secara gratis. Metode pembayaran: COD (Pembayaran di Tempat).</p><br>

                                <form method="POST" action="{{ route('order.confirm') }}" id="buyForm" class="form-horizontal">
                                    <div class="form-group">
                                        <label for="first_name" class="col-md-3 control-label">Nama Depan</label>
                                        <div class="col-md-8">
                                            <input class="form-control" placeholder="Raden" name="first_name" value="" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="email" class="col-md-3 control-label">Alamat Email</label>
                                        <div class="col-md-8">
                                            <input class="form-control" placeholder="radene@example.com" pattern="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}" data-validation="required" required="required" name="email" type="text">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="telpon" class="col-md-3 control-label"><span style="color: red;">*</span>Nomor Telpon</label>
                                        <div class="col-md-8">
                                            <input class="form-control" placeholder="0217364234" name="phone" value="" type="text">
                                            <span id="helpBlock" class="help-block">Diperlukan untuk melakukan pesanan</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-8 col-md-offset-3">
                                            <button type="submit" id="orderSubmit" class="btn btn-primary red"> Klik di sini
                                                untuk mendapatkan
                                            </button>
                                        </div>
                                    </div>
                                    <input id="productId" name="productId" type="hidden" value="{{ $product1->id }}">
                                    <input id="defaultPrice" name="defaultPrice" type="hidden" value="{{ $product1->price_spec }}">
                                    @include('part.params', ['params' => $uriParams])
                                </form>

                            </div>
                        </div>
                    </div>
                </section>
                <footer id="footer">
                    <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
                    <a href="#">Syarat &amp; Ketentuan</a>
                    <a href="#">Kontak</a>
                </footer>
            </div>
            <aside>
                <div class="effect"><img src="{{ asset('build/images/frontend/1.jpg') }}" alt="Effect 1 photo">

                    <div class="txt"><h4>Saya berhasil turun berat badan tanpa mengalami efek yoyo!</h4>

                        <p>Banyak kali saya sudah mencoba menurunkan berat badan namum tidak berhasil. Saya sudah mencoba
                            berbagai macam diet, sudah berusaha diet ketat, dan banyak obat dan suplemen penurun berat…
                            Berkat teman saya mulai tertarik menggunakan {{ $product1->name }}. Ibu temanku tinggal di Amerika
                            dan dokter di Amerika merekomendasikan {{ $product1->name }} diet ini. Ibu temanku berhasil
                            menurunkan berat badan dalam beberapa bulan. Solusi yang bagus! Sekarang berat badan saya 58 kg,
                            saya merasa cantik!</p>

                        <p class="end">Eli Wahyu Subekti<br>Yogyakarta, umur 18 tahun</p></div>
                </div>
                <div class="effect"><img src="{{ asset('build/images/frontend/2.jpg') }}" alt="Effect 2 photo">

                    <div class="txt"><h4>Saya mendapatkan tubuh ideal, yang disukai wanita</h4>

                        <p>Selama 2 tahun saya selalu berjuang melawan berat badan saya, dan sudah mencoba olahraga yang
                            ditawarkan, tetapi semua ini terlalu melelahkan. Pelatih saya merekomendasikan penggunaan
                            {{ $product1->name }}, dan dalam waktu hanya beberapa hari, tubuh saya menjadi lebih kuat, dan saya
                            melakukan latihan fisik yang baik dan benar, 60 menit tanpa berhenti. Saya turun 7 kg dalm 35
                            hari dan tubuh saya menjadi lebih berotot. {{ $product1->name }} diet ini kalo memang Anda mau
                            mendapatkan badan yang bagus.</p>

                        <p class="end">Muhammad Sasmi<br>Makassar, umur 29 tahun</p></div>
                </div>
                <div class="effect"><img src="{{ asset('build/images/frontend/3.jpg') }}" alt="Effect 3 photo">

                    <div class="txt"><h4>Tetap cantik setelah melahirkan!</h4>

                        <p>Saat hamil berat saya naik sampai 14 kg... Sebelum menggunakan {{ $product1->name }}, saya
                            berkonsultasi dengan dokter. Dia menegaskan bahwa {{ $product1->name }} alami dan efeknya aman. Pada
                            bulan pertama saya mengecilkan perut, nanti saya menghilangkan selulit dan stretch mark, dan
                            tubuh saya menjadi lebih kuat. Hari ini menandai 4 bulan setelah melahirkan, berat saya 54 kg
                            dan saya merasa seksi kembali! Bagus.</p>

                        <p class="end">Tiara Maharany<br>Surabaya, umur 35 tahun</p></div>
                </div>
                <div class="effect"><img src="{{ asset('build/images/frontend/4.jpg') }}" alt="Effect 4 photo">

                    <div class="txt"><h4>Menurunkan Berat Badan Tanpa Usaha Keras!</h4>

                        <p>Saya sibuk, tidak punya waktu untuk ini atau itu, tidak punya waktu untuk berolahraga. Teman saya
                            bekerja sebagai pelatih fitness, dia memberi saya hadiah. {{ $product1->name }} membantu saya
                            menurunkan berat badan dengan cepat. Saya turun 9 kg dalam 6 minggu, saya menghilangkan lemak di
                            perut dan paha, dan tubuh saya menjadi lebih kuat dan elastis. Sekarang saya memiliki sosok
                            ramping dan indah. Saya menjadi fit kembali!</p>

                        <p class="end">Asvinely Aufa, Denpasar, umur 17 tahun</p></div>
                </div>
                <div class="effect"><img src="{{ asset('build/images/frontend/5.jpg') }}" alt="Effect 5 photo">

                    <div class="txt"><h4>Saya sering mendengar "CEWEK CANTIK!"</h4>

                        <p>Dulu saya malu ketika mencoba pakaian di depan cermin. Sekarang berat saya 61 kg dan saya senang.
                            Hidup saya berubah jika saya berubah! Sosok ramping membuat saya percaya diri, saya merasa
                            cantik. Saya mendengar banyak pujian tentang bagaimana saya berpakaian dan betapa sangat indah
                            dilihat. Saya berhasil menurunkan berat badan!</p>

                        <p class="end">Devi Ismawati, Jakarta, umur 19 tahun</p></div>
                </div>
                <div class="effect"><img src="{{ asset('build/images/frontend/6.jpg') }}" alt="Effect 6 photo">

                    <div class="txt"><h4>Saya menghilangkan lemak di perut dalam 3 minggu</h4>

                        <p>Saya tidak percaya kepada suplemen, tetapi {{ $product1->name }} adalah cara yang sebenarnya bekerja.
                            Setelah dua bulan penggunaan {{ $product1->name }}, saya yakin. Saya menghilangkan lemak di perut,
                            sekarang saya sudah turun 11 kg. Saya senang, karena {{ $product1->name }} membantu mengurangi kadar
                            kolesterol juga. Sekarang saya merasa lebih sehat dan bugar.</p>

                        <p class="end">Sagita Siregar<br>Palembang, umur 23 tahun</p></div>
                </div>
            </aside>
        </div>
        <footer id="mobile-footer">
            <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
            <a href="#">Syarat &amp; Ketentuan</a>
            <a href="#">Kontak</a>
        </footer>
        <br>

        <div class="disclaimer"><p>Produk ini adalah suplemen diet, bukan obat untuk pengobatan penyakit, suplemen makanan
                tidak boleh digunakan sebagai pengganti makanan yang bervariasi dan seimbang. Sebuah diet yang bervariasi
                dan seimbang dan gaya hidup sehat yang dianjurkan. Paket tersebut berisi 15 kapsul dan cukup untuk 2 minggu
                terapi.</p>

            <p>Untuk menyediakan layanan yang efisien, dan kepentingan keselamatan pemilik website ini berhak untuk
                melakukan perubahan terhadap Kebijakan Privasi. Tindakan ini bertujuan untuk menjaga anonimitas
                pelanggan-pelanggan. Untuk meningkatkan kepuasan dengan layanan yang diberikan kepada pelanggan-pelanggan,
                namanya telah diubah demi keamanan dan privasi. Setiap kemiripan dengan orang-orang yang nyata adalah murni
                kebetulan.</p></div>
    </div>

@include('part.countdown')

</body>
</html>