@extends('layout.app',[
    'title' => 'Garcinia Cambogia Forte'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/garcinia2/all.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,300italic,600italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet">
@endsection

@section('pixel')
    @include('pixel.cambogia_main')
@endsection

@section('content')
    <div class="cont-wrapper hideJs" id="body-first">
        <div class="block block1">
            <div class="limit clearfix">
                <a href="#request_form_hash" class="logo" data-scroll="true">Garcinia Cambogia Forte
                </a>
                <div class="content">
                    <p class="title1 ttu light"> TURUNKAN BERAT BADAN BERLEBIH
                    </p>
                    <p class="title2 ttu light"> CEPAT DAN MUDAH DENGAN Garcinia Cambogia Forte
                    </p>
                    <div class="list">
        <span> CARA MENURUNKAN BERAT BADAN YANG MUDAH DAN MENYENANGKAN DENGAN Garcinia Cambogia Forte:
                      </span>
                        <ul>
                            <li> Pembakar lemak cepat dan alami
                            </li>
                            <li> Mengurangi nafsu makan secara sehat
                            </li>
                            <li> Penampilan menarik dan tetap sehat
                            </li>
                        </ul>
                        <a href="#request_form_hash" class="btn btn-transparent btn-with-arrow scroll"> PESAN SEKARANG
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block2">
            <div class="limit clearfix">
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-1.jpg') }}">
                    <div>
        <span> Mengecilkan pipi berisi
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-2.jpg') }}">
                    <div>
        <span> Pinggang langsing
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-3.jpg') }}">
                    <div>
        <span> Panggul ramping
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-4.jpg') }}">
                    <div>
        <span> Lengan kencang
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-5.jpg') }}">
                    <div>
        <span> Kaki elegan
                      </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block3">
            <div class="limit">
                <p class="title1 ttu"> PENURUNAN BERAT DENGAN NUTRISI YANG TEPAT ADALAH METODE UTAMA PENURUNAN BERAT BADAN MASA KINI
                </p>
                <div class="clearfix">
                    <div class="l-graph">
                        <p class="title ttu">
        <span> OBESITAS
                        </span>
                            1950-2010
                        </p>
                        <div class="content">
        <span class="line line1"> Obesitas dan kelebihan berat badan pada 20-74
                        </span>
                            <span class="line line2">
                          Obesitas pada 20-74
                        </span>
                            <span class="line line3">
                          kelebihan berat badan pada 20-74
                        </span>
                            <span class="line line4">kelebihan berat badan pada 12-19
                        </span>
                            <span class="line line5">kelebihan berat badan pada 6-11
                        </span>
                        </div>
                        <div class="info">
                            Menurut laporan WHO yang dipresentasikan di AS pada Maret 2010, jumlah orang dengan kelebihan berat badan terus meningkat.
                            <span> Lebih dari seperempat populasi penduduk Amerika menderita masalah kelebihan berat badan dan obesitas.
                        </span>
                        </div>
                    </div>
                    <div class="r-graph">
                        <div class="title clearfix">
                            <p class="ttu"> METODE PENURUNAN BERAT YANG DIPILIH
                            </p>
                            <ul>
                                <li> pria
                                </li>
                                <li> wanita
                                </li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="row clearfix">
                                <div class="label">
                                    Diet sederhana dan suplemen nutrisi
                                </div>
                                <div class="scale">
        <span class="man" style="width: 100%">
        </span>
                                    <span class="woman" style="width: 97%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Minum/ makan suplemen nutrisi
                                </div>
                                <div class="scale">
        <span class="man" style="width: 94%;">
        </span>
                                    <span class="woman" style="width: 55%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Memperbaiki pola makan
                                </div>
                                <div class="scale">
        <span class="man" style="width: 83%;">
        </span>
                                    <span class="woman" style="width: 36%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Aktivitas olahraga yang intens
                                </div>
                                <div class="scale">
        <span class="man" style="width: 40%;">
        </span>
                                    <span class="woman" style="width: 36%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Diet ketat saja
                                </div>
                                <div class="scale">
        <span class="man" style="width: 47%;">
        </span>
                                    <span class="woman" style="width: 36%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Diet ketat dan olahraga intensif
                                </div>
                                <div class="scale">
        <span class="man" style="width: 32%;">
        </span>
                                    <span class="woman" style="width: 18%;">
        </span>
                                </div>
                            </div>
                        </div>
                        <div class="info">
                            Menurut statistik penelitian yang dilaporkan WHO, 70% warga Amerika lebih suka menjaga berat badan mereka dengan diet sederhana.
                            <span> Namun, mereka seringkali lalai dalam melakukan diet tersebut karena dibutuhkan tingkat disiplin dan usaha yang tinggi. Oleh sebab itu, metode penurunan berat badan ini menjadi tidak efektif.
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block5">
            <div class="limit clearfix">
                <div class="side l-side">
                    <p class="title1 light ttu"> TURUNKAN BERAT BERLEBIH
                    </p>
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/prod.png') }}" width="308">
                    </div>
                </div>
                <div class="side r-side">
                    <div class="description">
                        <p> Garcinia Cambogia Forte mengandung konsentrat yang secara aktif menghancurkan timbunan lemak di dalam tubuh. Meningkatkan fungsi metabolisme, detoksifikasi tubuh Anda, dan membuang radikal bebas.
                        </p>
                        <p> Anti oksidan alami menjaga kesehatan dan membuat penampilan menjadi makin menarik
                        </p>
                    </div>
                    <a href="#request_form_hash" class="btn btn-transparent btn-with-arrow scroll"> PESAN SEKARANG
                    </a>
                </div>
                <div class="center"> Garcinia Cambogia Forte tidak hanya membantu Anda menurunkan berat badan, tetapi juga mengandung banyak zat bermanfaat. Murni, tanpa zat-zat tambahan berbahaya dan dengan konsentrasi ekstrak yang maksimal.
                </div>
            </div>
        </div>
        <div class="block block7">
            <div class="limit">
                <p class="title ttu light">SEBAB DAN MEKANISME PEMBENTUKAN LEMAK BAWAH KULIT
                </p>
                <div class="content">
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-1.png') }}">
                        </div>
                        <span class="label ttu">LANGKAH 1
                      </span>
                        <p> Diet yang tidak sehat dan tidak teratur, makanan berbahaya dengan kandungan kalori tinggi, gaya hidup pasif dan stres yang konstan menyebabkan akumulasi energi tak terpakai.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-2.png') }}">
                        </div>
                        <span class="label ttu"> LANGKAH 2
                      </span>
                        <p class="sec">Lemak terus-menerus terkumpul dalam tubuh, tidak terhancurkan dan tidak terbuang. Lemak berlebih diserap melalui darah, membentuk apa yang disebut "timbunan lemak" di badan kita, dan tetap berada di sana selamanya.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-3.png') }}">
                        </div>
                        <span class="label ttu">LANGKAH 3
                      </span>
                        <p>Seiring berjalannya waktu "timbunan lemak" ini semakin besar dan menjadi semakin susah untuk membakar lemak yang terakumulasi.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-4.png') }}">
                        </div>
                        <span class="label ttu"> LANGKAH 4
                      </span>
                        <p>Lemak secara bertahap menembus lapisan bawah kulit (subkutan), merobek strukturnya dan menghalangi sirkulasi darah normal sehingga menimbulkan selulit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block12">
            <div class="limit clearfix">
                <div class="content clearfix">
                    <div class="side l-side">
                        <div class="row clearfix">
        <span class="label">
                          NAMA PRODUK:
                        </span>
                            <span class="value">
                          Garcinia Cambogia Forte
                        </span>
                        </div>
                        <div class="row clearfix">
        <span class="label">
                          ISI:
                        </span>
                            <span class="value">
                          1 paket berisi 30 kapsul, masing-masing 500 mg
                        </span>
                        </div>
                        <div class="row clearfix">
        <span class="label">
                          KANDUNGAN:
                        </span>
                            <span class="value">
                          Ekstrak Garcinia Cambogia Forte Alami
                        </span>
                        </div>
                        <div class="row clearfix">
        <span class="label">
                          PENGGUNAAN:
                        </span>
                            <span class="value">
                          Minum satu kapsul 2 atau 3 kali sehari di antara waktu makan. Masa penggunaan minimal selama 2 minggu.
                        </span>
                        </div>
                    </div>
                    <div class="side r-side">
                        <div class="row clearfix">
        <span class="label">
                          CARA KERJA:
                        </span>
                            <span class="value">
                          Garcinia Cambogia Forte secara efektif menghancurkan timbunan lemak dan menghambat terbentuknya timbunan lemak baru, meningkatkan fungsi saluran pencernaan untuk membuang cairan berlebih dan racun dari tubuh. Vitamin kompleks dan anti-oksidan alami meremajakan tubuh dan membuatnya lebih sehat.
                          <br>
        <br>
                          Anda dapat meminum kapsul ini setiap saat, Anda juga dapat meminumnya saat bepergian tanpa harus merasakan rasa pahit di lidah. Kualitas bahan baku kapsul dikontrol ketat, dan ekstraknya diekstraksi dan diperkaya dalam laboratorium.
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="warning">
            <div class="limit">
                <p>Hati-Hati dengan produk palsu yang di jual di website lain karena produk asli hanya dari website kami dan call center kami. kami tidak bertanggung jawab atas produk palsu yag di beli d pasaran atau website lain</p>
            </div>
        </div>
        <div class="block block15">
            <div class="limit clearfix">
                <div class="content form-bottom">
                    <div class="title1 ttu">PESAN SEKARANG JUGA</div>
                    <div id="request_form_hash" class="title2 ttu">Garcinia Cambogia Forte</div>

                        {!! form_start($form, ['id' => 'request-form"', 'class' => 'form']) !!}

                        {!! form_errors($form->first_name) !!}
                        {{--{!! form_errors($form->last_name) !!}--}}
                        {!! form_errors($form->email) !!}
                        {!! form_errors($form->phone) !!}

                        <div class="form-group">
                            {!! form_widget($form->first_name) !!}
                        </div>
                        {{--<div class="form-group">--}}
                            {{--{!! form_widget($form->last_name) !!}--}}
                        {{--</div>--}}
                        <div class="form-group">
                            {!! form_widget($form->email) !!}
                        </div>
                        <div class="form-group">
                            {!! form_widget($form->phone) !!}
                        </div>
                        <div class="price">
                            <s>{{ StrHelper::spaceInPrice($product1->price_old) }} {{ $product1->currency }}</s>
                            <span>HARGA BARU: {{ StrHelper::spaceInPrice($product1->price) }} {{ $product1->currency }}</span>
                        </div>
                        {!! form_widget($form->productId, ['value' => $product1->id]) !!}
                        <div class="form-group">
                            <button class="ttu btn btn-primary btn-block js_submit" type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();">PESAN</button>
                        </div>
                        {!! form_end($form, false) !!}

                </div>
            </div>
        </div>
    </div>

    <div class="ac_footer">
        <span>© 2017 Copyright. All rights reserved.</span><br>
        <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
        {{--| <a href="#request_form_hash">Report</a>--}}
    </div>

@endsection
