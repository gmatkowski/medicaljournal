@extends('layout.app')

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/confirm/all.css') }}">
@endsection

@section('analytics')
    @if(app()->environment() == 'production')
        @if(isset($tracking))
            @include('ga.affbay', ['tracking' => $tracking])
        @endif
    @endif
@endsection

@section('trackConversions')
    @if(app()->environment() == 'production')
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KRCHZ7X');</script>
        <!-- End Google Tag Manager -->
    @endif
@endsection

@section('pixel')
    @if(app()->environment() == 'production')

        @include('pixel.doubleClickGTMhead')

            @if(strpos(URL::previous(), 'promo-gooodies.com') !== false
            || strpos(URL::previous(), 'goood-promo.com') !== false
            || strpos(URL::previous(), 'supergoood-promos.com') !== false
            || strpos(URL::previous(), 'best-deal-now.com') !== false
            || strpos(URL::previous(), 'good-deal-now.com') !== false
            || strpos(URL::previous(), 'hooot-promo.com') !== false
            || strpos(URL::previous(), 'supergoood-deals.com') !== false
            || strpos(URL::previous(), 'catch-that-deal.com') !== false
            || strpos(URL::previous(), 'better-buynow.com') !== false
            || strpos(URL::previous(), 'indonesian-stories.com') !== false
            || strpos(URL::previous(), 'supergoood-deals.com') !== false
            || strpos(URL::previous(), 'gooood-deal.com') !== false
            || strpos(URL::previous(), 'hot-daily-promo.com') !== false
            || strpos(URL::previous(), 'cosmetics-sales.com') !== false)

            @if($contact->product=='blackmask')
                @include('pixel.LP-63')
            @endif
            @if($contact->product=='hallupro')
                @include('pixel.LP-64')
            @endif
            @if($contact->product=='cambogia')
                @include('pixel.LP-71')
            @endif
            @if($contact->product=='joint')
                @include('pixel.LP-190')
            @endif
            @if($contact->product=='coffee')
                @include('pixel.LP-193')
            @endif
            @if($contact->product=='detoclean')
                @include('pixel.LP-199')
            @endif
			@if($contact->product=='waist')
              @include('pixel.waist_lead')
          @endif
            @if($contact->product=='flexaplus')
                @include('pixel.flexaplus')
            @endif
            @if($contact->product=='dietbooster')
                @include('pixel.LPM-495')
            @endif
        @else
            @if(isset($fb_pixel))
                @include('pixel.affbay', ['fb_pixel' => $fb_pixel])
            @endif
            @if($contact->product=='manuskin')
                @include('pixel.PIX-71')
            @endif
            @if($contact->product=='vervalen')
                @include('pixel.PIX-77')
            @endif
            @if($contact->product=='joint')
                @include('pixel.LP-190')
            @endif
            @if($contact->product=='hallupro')
                @include('pixel.PIX-78')
            @endif
            @if($contact->product=='imove')
                @include('pixel.LP-634')
            @endif
        @endif
    @endif
@endsection

@section('content')

    @if(app()->environment() == 'production')
        @include('pixel.doubleClickGTMbody')
    @endif

    <div class="mod wrap_block_success">
        <div class="block_success">
            <h2>SELAMAT! PESANAN ANDA TELAH DITERIMA!</h2>
            <p class="success">
                Selanjutnya, operator pusat layanan informasi kami akan segera menghubungi Anda untuk mengkonfirmasi pesanan Anda. Pastikan ponsel Anda dalam kondisi aktif.
            </p>
            <h3>Silakan periksa informasi yang telah Anda masukkan</h3>
            <div class="wrap_list_info">
                <ul class="list_info">
                    <li><span>Nama: </span>
                        <text class="js-name">{{ $contact ? $contact->name : '' }}</text>
                    </li>
                    <li><span>Telepon: </span>
                        <text class="js-phone">{{ $contact ? $contact->phone : '' }}</text>
                    </li>
                </ul>
            </div>
            <p class="fail">
                <a href="#" onclick="history.go(-1); return false;">Apabila Anda membuat kesalahan dalam pengisian formulir, silakan kembali dan mengisi ulang</a></p>
        </div>
    </div>
    <script>
        dataLayer.push({
            'event': 'virtual page',
            'virtualPageURL': '/action/form-submit-complete/{{$contact->product}}', // np.: /action/form-submit/joint_healer
            'eventCategory': 'Form',
            'eventAction': 'Submit',
            'eventLabel': '{{$contact->product}}', //np.: joint_healer
            'nonInteraction': true
        });
    </script>
@endsection
