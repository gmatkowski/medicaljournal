@extends('layout.app')
@section('title') :: {{ trans('menu.try') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')
	<main>

		<section class="test-lesson">
			<div class="container">
				<div class="row">
					<div class="col-md-5">
						@if($page)
							<h2>{!! $page->title !!}</h2>
							<div class="well">
								{!! $page->content !!}
							</div>
						@endif
					</div>
					<div class="col-md-7 steps">
						<h2>{{ trans('welcome.slider.try.lesson') }}</h2>

						<div class="well">
							<h3 class="title">{{ trans('try.step.1') }}</h3>
							<ul class="langs">
								@foreach($languagesWithSample as $key => $language)
									<li>
										<label for="lang-{{ $language->id }}" @if($key == 0) class="active" @endif>
											<img src="{{ $language->icon_path }}" alt="">

											<p>{{ $language->name_short }}</p>
										</label>
										<input type="radio" @if($key == 0) checked @endif name="language" data-action="on-change-switch" data-target="audio-" id="lang-{{ $language->id }}" value="{{ $language->id }}"/>
									</li>
								@endforeach
							</ul>
						</div>
						<div class="well">
							<h3 class="title">{{ trans('try.step.2') }}</h3>
							@foreach($languagesWithSample as $key => $language)
								<div data-container="audio-on-change-switch" data-id="audio-{{ $language->id }}" id="" @if($key > 0) style="display:none;" @endif >
									<strong class="lang">{{ $language->name }}</strong>
									<audio src="{{ $language->sample_path }}" preload="none"></audio>
								</div>
							@endforeach
						</div>
						<div class="well">
							<h3 class="title">{{ trans('try.step.3') }}</h3>

							<div class="video-container">
								<div id="player" data-id="MebhOoawIM8"></div>
							</div>
						</div>
					</div>
					<div class="col-xs-12">
						<div class="well contact">
							<h3>{{ trans('try.text.interested') }}</h3>
							<h4>{{ trans('try.text.answer.all.questions') }}</h4>
							<a href="phone:{{ trans('checkout.question.phone.number') }}" class="phone"><img src="{{ asset('build/images/tel.png') }}" alt="">{{ trans('checkout.question.phone.number') }}
						</div>
					</div>
				</div>
			</div>
		</section>

	</main>

@endsection