@extends('layout.app')
@section('title') :: {{ trans('menu.opinions') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')

	<main>
		@include('part.fb_comments')
		<section class="wow fadeIn opinions-txt">
			<div class="container">
				<h1>{{ trans('opinions.happy.clients') }}</h1>

				<div id="txt-opinions">
					@foreach($opinions as $opinion)
						<div class="item">
							<strong>{{ $opinion->name }}</strong>

							<p>{!! $opinion->description !!}</p>
						</div>
					@endforeach
				</div>
			</div>
		</section>

		<a href="{{ route('page.languages') }}" class="check-courses">{{ trans('opinions.go.course') }}</a>

		@include('part.footer_small')
	</main>

@endsection
@section('fb::footer')@endsection