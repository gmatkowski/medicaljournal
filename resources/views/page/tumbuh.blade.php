@extends('layout.perkelahian',[
    'title' => 'Health Journal'
])

@section('content')
    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <h1><a><img src="{{ asset('build/images/frontend/perkelahian_logo.png') }}" style="width:200px;" alt=""></a></h1>
            <nav class="links">
                <ul>
                    <li><a>HOME</a></li>
                    <li><a>DIET</a></li>
                    <li><a>FIT & OLAHRAGA</a></li>
                    <li><a>PSIKOLOGI</a></li>
                    <li><a>DI RANJANG</a></li>
                    <li><a>KECANTIKAN</a></li>
                    <li><a>KESEHATAN</a></li>
                </ul>
            </nav>
        </header>

        <!-- Main -->
        <div id="main">

            <!-- Post -->
            <article class="post">
                <header>
                    <div class="title">
                        <h2>Garcinia Cambogia</h2>
                    </div>
                    <div class="meta">
                        <time class="published" datetime="2015-11-01">{{ \Carbon\Carbon::now()->format('M d, Y') }}</time>
                        <a class="author"><span class="name">Daryono</span><img src="{{ asset('build/images/frontend/avatar.jpg') }}" alt="" /></a>
                    </div>
                </header>
                <span class="image featured">
                    <img src="{{ asset('build/images/frontend/garcinia25x162.jpg') }}" alt="" style="width:100%" />
                </span>
                <p>Garcinia Cambogia tumbuh di iklim tropis dan lembap di Asia Tenggara serta Afrika bagian tengah dan barat. Pada zaman dahulu, orang-orang menggunakan buahnya tidak hanya untuk masakan, melainkan juga untuk merawat macam-macam penyakit, gangguan hormonal, dan saluran pencernaan. <b>Garcinia Cambogia yang dipakai untuk menurunkan berat badan dapat memperbaiki pencernaan dan mempercepat penyerapan, serta membantu mengurangi kelebihan berat badan.</b></p>

                <h1>Khaziat bermanfaat Garcinia Cambogia
                    Meluruhkan lemak</h1>

                    <p>Komponen mendasar yang bermanfaat dari buahnya adalah hydroxycitric acid (HCA) yang diekstrak dari kulit buahnya. <b>Fungsi utama HCA adalah mengurangi enzim-enzim yang bertanggung jawab memproses karbohidrat menjadi lemak. </b>Jika tingkatnya berkurang, maka produksi sel-sel lemak pun berkurang. Proses penyimpanan kelebihan kolesterol dipercepat, kelebihan lemak tidak cepat menumpuk, sekalian membersihkan dan membuangnya bersama ampas kolesterol.</p>
                    <p>HCA menstabilkan tingkat gula darah dengan cara mempercepat proses glukosa menjadi energi dan mengurangi cara kerja enzim-enzim yang mengubah kelebihan karbohidrat dalam lemak di area perut. HCA menjadi biokorektor yang dapat dipakai untuk mencegah diabetes.</p>

                <h1>Mengurangi rasa lapar</h1>

                <p>Berkat khaziat-khaziatnya, Garcinia Cambogia yang dipakai untuk mengurangi berat badan memberikan nutrisi dan mikroelemen yang diperlukan sel-sel tubuh. Hydroxycitric acid dalam darah mengurangi kinerja hormon leptin yang bertanggung jawab atas rasa lapar.</p>
                <p>Penelitian menunjukkan bahwa komposisi Garcinia Cambogia mempengaruhi enzim serotonin. Terdapat pembuktian adanya hubungan antara tingkat serotonin dan volume makanan. Semakin tinggi serotonin dalam plasma darah, semakin sedikit hasrat untuk makan, sehingga berat badan turun dan cadangan energi meningkat. Mengingat sel-sel tubuh menerima nutrisi yang diperlukan, enzim-enzim yang telah diseimbangkan memperbaiki suasana hati dan rasa lapar pun berkurang.</p>

                <h1>Kontraindikasi</h1>

                <p>Garcinia Cambogia tidak dianjurkan untuk wanita hamil dan menyusui, penderita diabetes, pengidap penyakit Alzheimer, dan mereka yang punya gangguan psikiatris dan saraf. Penggunaan obat ketika iskemia dan hipertensi arteri memerlukan pengawasan khusus.</p>

                <h1>Tempat membelinya</h1>

                <p>Garcinia Cambogia untuk menurunkan berat badan dapat dipesan melalui internet, yaitu di situs resmi pembuatnya. Tidak dijual di toko olahraga dan nutrisi tambahan serta apotek. </p>

            </article>

        </div>

        <!-- Footer -->
        <section id="footer">
            <p class="copyright">&copy; Medical Jurnal</p>
        </section>

    </div>
@endsection
