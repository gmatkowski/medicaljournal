@extends('layout.app')
@section('title') :: {{ trans('menu.lessons') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')

	<main>
		<section class="lessons">
			<div class="container">
				<h1>{{ trans('lessons.list') }}</h1>
				<ul>
					@foreach($lessons as $key => $lesson)
						<li class="wow fadeInLeft {{ $lesson->level_class_name }}">
							<div class="lvl">
				                <span class="ico">
				                  <span class="img">
					                  @if(!empty($lesson->icon))
						                  <img src="{{ $lesson->icon_path }}" alt=""/>
					                  @endif
				                  </span>
				                  <p>{{ $lesson->level_name }}</p>
				                  <span class="counter">{{ $key+1 }}.</span>
				                </span>
							</div>
							<div class="desc">
								<h2>{{ $lesson->name }}</h2>

								<p>{!! $lesson->description !!}</p>
							</div>
						</li>

						@if($page && $key == 5)
							<li class="wow fadeInLeft note">
								<h3>{{ $page->title }}</h3>

								<p>{!! $page->content !!}</p>
							</li>
						@endif

					@endforeach


				</ul>
			</div>
		</section>

		@include('part.footer_small')
	</main>

@endsection