@extends('layout.app',[
    'title' => 'Mengurangi diet. Diet ala selebriti, HALLO! Indonesia'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/garcinia3/all.css') }}" media="all">

    <link href="https://fonts.googleapis.com/css?family=Raleway:300" rel="stylesheet" type="text/css">
@endsection

@section('content')
    <main class="column__container page">
        <div class="top-menu">
            <div class="top-menu__ads">
                <a class="logo logo_top-menu" href="{{ $voluum }}"><span class="logo-sign">  Indonesia  </span></a>
            </div>
            <ul class="menu menu_top-menu">
                <li class="menu__link menu__link_top-menu"><a class="menu__link-item" href="{{ $voluum }}">
                        Beranda</a></li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Bintang</a>
                </li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Monarki</a>
                </li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Fashion</a>
                </li>
                <li class="menu__link menu__link_top-menu menu__link_active">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Tubuh</a>
                </li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Pernikahan</a>
                </li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Anak</a>
                </li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Kecantikan</a>
                </li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Gaya hidup</a>
                </li>
                <li class="menu__link menu__link_top-menu">
                    <a class="menu__link-item" href="{{ $voluum }}" title=""> Bioskop</a>
                </li>
            </ul>
            <div class="menu__hot-wrapper">
                <span class="menu__hot-link menu__hot-link_grey">  Berita hot:  </span>
                <nav class="menu__hot">
                    <a class="menu__hot-link" href="{{ $voluum }}"> Anak sang bintang</a>
                    <a class="menu__hot-link" href="{{ $voluum }}"> Cristiano Ronaldo</a>
                    <a class="menu__hot-link" href="{{ $voluum }}"> Kate Middleton</a>
                    <a class="menu__hot-link" href="{{ $voluum }}"> Makan sehat</a>
                    <a class="menu__hot-link" href="{{ $voluum }}"> Resep</a>
                    <a class="menu__hot-link" href="{{ $voluum }}"> Ramalan mingguan</a>
                </nav>
            </div>
        </div>
        <div class="column__wrapper column__wrapper_pad-t_20">
            <div class="column column_680 column_pad-r_20">
                <h1> Cara saya menurunkan 40% berat tubuh: kisah seorang pria yang turun 56 kg </h1>
                <div class="image-wrapper"><img alt="" src="{{ asset('build/images/frontend/g31.jpg') }}">
                </div>
                <p>
                    Ini adalah kisah dari Dimas Aldrian — seorang pria dengan berat badan 119 kg usianya yang ke-30 dan sangat
                    dekat dengan masalah kesehatan yang tidak bisa disembuhkan.
                </p>
                <p>
                    Dimas menyadari bahwa dia tidak ingin menjadi pria gemuk yang diabetes. Dia memilih jalan yang lain.
                </p>
                <p>
                    Saya tahu saya harus melakukan sesuatu yang ekstrem. Saya harus menormalkan berat badan saya. Saya merasa
                    kecewa. Saya bahkan kesulitan menutup risleting celana panjang XXXXXL saya dengan ukuran pinggang
                    112-118 cm. Bahkan ukuran celana saya ini akan segera kekecilan. Saya mengganti kancing secara rutin karena tidak
                    tahan dengan bebannya.
                </p>
                <p>
                    Saya sangat sulit mencari pakaian yang cocok. Kaus XXL tidak cukup; bagian bawah pakaian sangat ketat
                    saat saya duduk. Saya tidak nyaman saat tidur. Saat saya berbaring di sofa atau kasur, saya merasa
                    seperti paus yang terdampar.
                </p>
                <div class="image-wrapper"><img alt="" src="{{ asset('build/images/frontend/g32.jpg') }}">
                </div>
                <p>
                    Saya bisa menjadi seperti ini karena beberapa hal dari masa kanak-kanak saya.
                </p>
                <p>
                    Saya tidak pernah berlari 1,5 km nonstop tanpa istirahat dan saya berjalan sangat lambat sepanjang
                    hidup saya.
                </p>
                <p>
                    Di tahun 2015, berat badan saya mencapai 119 kg dan hampir terkena diabetes.
                </p>
                <p>
                    Saya akhirnya memutuskan untuk mengubah hidup saya. Saya sangat khawatir dengan istri saya. Saya tidak
                    ingin membuatnya menjanda.
                </p>
                <p>
                    Saya mulai dengan diet seperti biasa.
                </p>
                <p>
                    Prinsip kerja semua diet itu sama: kalau kamu mengonsumsi lebih sedikit kalori dari kalori
                    yang dibakar, beratmu akan turun.
                </p>
                <p>
                    Tapi, karena alasan yang tidak diketahui, berat badan kembali dan bahkan bertambah.
                </p>
                <p>
                    Setelah beberapa bulan melakukan diet, saya menyadari bahwa penurunan kalori saja tidak cukup. Saya
                    membutuhkan sesuatu yang lain. Yang lebih aktif dan efektif.
                </p>
                <p>
                    Jadi, saya pergi ke gym. Tetapi, saya juga belum mendapatkan hasil yang saya inginkan. Terlalu banyak
                    siksaan fisik dan psikologi. Terlalu banyak larangan untuk hal-hal yang saya sukai – dan bahkan tanpa
                    hasil. Semua ini membuat saya depresi.
                </p>
                <p>
                    Saya mulai sering minum minuman keras dan berat badan yang berusaha saya turunkan dengan kerja keras pun
                    kembali, malah bertambah 8 kg.
                </p>
                <p>
                    Segalanya sudah jelas. Semuanya tidak bertahan lama dan saat istri saya pergi meninggalkan rumah, saya
                    sadar kalau saya harus berjuang untuk diri sendiri.
                </p>
                <p>
                    Hidup saya diawali dengan pertemuan dengan ahli psikoterapi. Saya membayangkan betapa depresinya saya
                    dulu: seseorang dengan berat 120 kg menangis dan gemuk. Dia menghapus air matanya dengan sapu tangan,
                    mengeluhkan betapa menyedihkannya dia.
                </p>
                <p>
                    Tentu saja, semua masalah saya sudah jelas. Tanpa dokter pun, saya tahu saya menderita secara psikologi
                    karena kelebihan berat badan. Tapi, saya membutuhkan solusi untuk masalah saya.
                </p>
                <p>
                    Dokter favorit saya, Dr. Putra Gunawan memberikan solusinya. Tidak, bukan pelatihan psikoterapi.
                    Solusi dari semua masalah saya adalah sebotol kapsul <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a>.
                </p>
                <div class="image-wrapper">
                    <a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/prod.png') }}"></a>
                </div>
                <p>
                    Saat saya menerima hadiah ini, saya tidak membayangkan kalau <strong> semuanya sangatlah mudah</strong>.
                </p>
                <p>
                    Tentu saja, saya meminta Dr. Putra untuk menjelaskan produk ini. Singkatnya, <a class="url-product" href="{{ $voluum }}">
                        Garcinia Cambogia Forte</a> - <strong> adalah produk baru di bidang penurunan berat.</strong> Kamu harus mengonsumsinya sesuai instruksi.
                    Dan kamu juga tidak perlu melakukan diet apa pun. (Kamu bisa menemukan
                    informasi lebih lengkap <a class="url-product" href="{{ $voluum }}"> di situs web resmi
                        produsennya</a>).
                </p>
                <div class="order-box"><a class="order-now" href="{{ $voluum }}"> pelajari lebih lanjut</a></div>
                <p>
                    Setelah itu, saya mulai mencari informasi tentang produk ini dan menemukan <strong> artikel
                        ilmiah</strong> dengan penjelasan detail. Saya tidak ragu lagi.
                </p>
                <p>
                    Saya tidak pernah berpikir bahwa proses menurunkan berat badan  bisa sangat mudah dan cepat!
                </p>
                <p>
                    Komposisi dari <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a> sangatlah unik!
                    Karena kerjanya  yang mempercepat proses metabolisme dan pembersihan, berat badanmu turun dengan sangat cepat.
                    Malahan, hanya produk ini yang saya butuhkan.
                </p>
                <p>
                    Kamu hanya bisa membeli <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a> secara online.
                    Produk ini belum dijual untuk umum.
                </p>
                <p>
                    Saya mengunjungi situsnya dan memesan satu botol (selain botol yang diberikan dokter) <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a>. Sangat mudah karena tidak ada
                    pembayaran di awal sehingga semua lebih mudah.
                </p>
                <p>
                    Tapi, saya belum membuka botol keduanya. Botol pertama saja sudah cukup.
                </p>
                <div class="image-wrapper"><img alt="" src="{{ asset('build/images/frontend/g33.jpg') }}">
                </div>
                <p>
                    Saya tidak ingin menyembunyikan fakta bahwa saya terus berolahraga seminggu sekali – untuk menjaga kesehatan
                    saya (saya tidak ingin punya gejala diabetes karena dengan <a class="url-product" href="{{ $voluum }}">Garcinia Cambogia Forte</a>, saya
                    bisa makan apa pun yang saya inginkan).
                </p>
                <p>
                    <strong> Hanya setelah 2 minggu, saya terkejut dengan hasilnya – berat badan saya turun 8,3 kg!</strong> Saya tidak
                    percaya ini terjadi pada tubuh saya. <strong> Saya turun berkilo-kilo</strong> dan tubuh saya menjadi
                    lebih kecil serta <strong> bentuk badan berubah di depan mata saya!</strong> Napas yang pendek pun tidak
                    terjadi lagi. <strong> Saya menjadi populer diantara wanita</strong>. Sebenarnya, ini rahasia kalau
                    <strong> kejantananmu juga akan meningkat berkali lipat!</strong> Selain itu, saya sama sekali tidak berbohong!!!
                </p>
                <p>
                    Hanya orang malas yang bisa turun berat seperti ini!
                </p>
                <p>
                    Saya menghabiskan banyak uang untuk membeli pakaian. <strong> Ukuran saya berubah setiap 2 minggu.
                        Awalnya saya memakai kaus XXL, sekarang cukup dengan ukuran M</strong>.
                </p>
                <p>
                    12 April 2016 <strong> Berat saya 67 kg – turun 52 kg</strong> dibandingkan sebelum saya depresi.
                    <strong> Sekali lagi, semua ini berkat <a class="url-product" href="{{ $voluum }}">Garcinia Cambogia Forte</a> sehingga saya turun 56 kg</strong>.
                </p>
                <p> Luar biasa, bukan? </p>
                <div class="image-wrapper"><img alt="" src="{{ asset('build/images/frontend/g37.jpg') }}">
                </div>
                <p>
                    Pengalaman menurunkan berat badan saya ini mengajarkan saya bahwa saya bisa mencapai apapun. Saya merasa berada di puncak
                    dunia.
                    <strong> Saya tidak pernah merasa lebih nyaman dan bahagia dibandingkan sekarang!</strong> Satu-satunya tujuan
                    dalam hidup saya adalah memperbaiki kesehatan fisik. Yah, sekarang sudah tercapai.
                </p>
                <p>
                    Saya masih belum memutuskan masalah apa yang akan berada dalam daftar saya selanjutnya.
                </p>
                <p>
                    Saya ingin berkata kepada kalian yang membaca bahwa kalian sebaiknya tidak menunda-nunda untuk menurunkan berat
                    badan, karena kalian tidak akan menyadari kenaikan berat 10 kg dalam setahun. Saat kalian menyadarinya, semua
                    sudah terlambat. Selain itu, turun berat dengan <a class="url-product" href="{{ $voluum }}">
                        Garcinia Cambogia Forte</a> sangat mudah sampai-sampai kamu tidak akan menyadari “hilangnya” berat itu.
                </p>
                <p>
                    Kamu bisa memesan <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a> yang asli
                    hanya di situs web <a class="url-product" href="{{ $voluum }}"> RESMI</a> produsennya. <a class="url-product" href="{{ $voluum }}"> Ini tautan ke situs web mereka</a>.
                </p>
                <div class="order-box"><a class="order-now big-butt-on" href="{{ $voluum }}"> Pesan</a></div>
                <!--end article-->
                <!--tags-->
                <table class="page__table page__table_grey page_margin-b_30">
                    <tbody>
                    <tr class="page__table-tr">
                        <td class="page__table-td page__table-td_top"> Tag</td>
                        <td class="page__table-td">
                            <a class="search__item" href="{{ $voluum }}" title=""> Turun berat</a>
                            <a class="search__item" href="{{ $voluum }}" title=""> Kesehatan</a></td>
                    </tr>
                    </tbody>
                </table>
                <!--end tags-->
                <!--Comments-->
                <div class="comments">
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment1.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> David </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Dimas KEREN! Ceritamu benar-benar jadi motivasi yang kuat! Ngomong-ngomong, aku juga
                                    pesan dari situs web ini.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment2.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Wawan </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Kau benar-benar pria yang hebat! Contoh yang baik dan harapan untuk banyak anak-anak di
                                    dunia. Mungkin saya juga akan pesan <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment3.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Elina </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Ya, kapsul ini juga membantuku! Aku juga bangga mengonsumsinya!
                                    <br><img alt="" src="{{ asset('build/images/frontend/155kg.jpg') }}">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment4.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Erika </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Keren! Aku juga akan pesan <a class="url-product" href="{{ $voluum }}">Garcinia Cambogia Forte</a> ! Kalau mau mengubah dunia, mulai dari dirimu sendiri! Aku akan mulai sekarang!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment5.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Rifky </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Dimas, kamu emang keren! Aku juga konsumsi <a class="url-product" href="{{ $voluum }}">Garcinia Cambogia Forte</a> dan turun berat, tapi tidak secepat kamu.
                                    <br><img alt="" src="{{ asset('build/images/frontend/118kg.png') }}">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment6.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Danu </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Aku sudah minum <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a>
                                    sekitar 2 bulan. Beratku turun 35 kg!!!
                                    <br><img alt="" src="{{ asset('build/images/frontend/9108978412186004kg.jpg') }}">
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment7.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Putri15 </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Produk hebat!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment8.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Mulya </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Ha! Bayangkan wajah istrinya sekarang! Kurasa dia tidak menyangka kalau suaminya akan
                                    turun berat secepat itu!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment10.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Kurnia </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Cerita yang benar-benar menginspirasi. Sekarang, aku akan membeli <a class="url-product" href="{{ $voluum }}">
                                        Garcinia Cambogia Forte</a> ini!
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="comment">
                        <div class="comm-avatar"><img alt="" src="{{ asset('build/images/frontend/comment9.jpg') }}">
                        </div>
                        <div class="comm-area">
                            <div class="comm-header">
                                <p> Nita88 </p>
                            </div>
                            <div class="comm-body">
                                <p>
                                    Aku sudah berhasil menurunkan berat dengan kapsul ini selama 2 bulan. Hasilnya berat saya turun hampir 30 kg!
                                    Benar-benar menarik karena ada cerita yang mirip dengan kisahku. Aku tidak punya alasan
                                    khusus untuk turun berat. Tapi, waktu umurku 20 lebih sedikit, aku berkaca dan melihat
                                    PEREMPUAN gendut!!! Aku tampak mengerikan, apalagi berat pacarku hanya 60 kg! Bayangkan
                                    bagaimana kalau kami berjalan bersam...? Aku yakin <a class="url-product" href="{{ $voluum }}">Garcinia Cambogia Forte</a>
                                    bisa membantu dan aku langsung menggunakannya. Pacarku sangat mendukung.
                                    Kami memesan kapsul ini dan tidak percaya karena aku turun 5 kg dalam minggu pertama!
                                    Aku senang sekali tidak perlu menyiksa diri dengan diet, makan bubur dan yoghurt, dkk.
                                    <a class="url-product" href="{{ $voluum }}"> Garcinia Cambogia Forte</a> adalah dietku
                                    satu-satunya dan aku tdk percaya produk lain. Jadi, kusarankan kalian semua yang punya
                                    berat berlebih – jangan putus asa. Ada solusinya yaitu <a class="url-product" href="{{ $voluum }}">
                                        Garcinia Cambogia Forte.</a> Dimas, terima kasih untuk ceritanya!
                                    <br><img alt="" src="{{ asset('build/images/frontend/foto23a.png') }}">
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--End Comments-->
            </div>
            <aside class="column column_300 column_border_left">
                <div style="text-align:center;"><a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/prod.png') }}"></a>
                </div>
                <aside class="subscribe">
                    <h3 class="page__header-mini page__header_center"> Berlangganan <span>   HALLO   </span></h3>
                    <h3 class="page__header-mini page__header_center"><span>  Terima kasih!  </span> <br> Kami akan terus
                        <br> menghubungi Anda! </h3>
                    <form action="" id="form_subscribe-1470135103" method="post">
                        <fieldset class="fieldset fieldset_subscribe">
                            <input class="input input_subscribe" name="email" placeholder="Email Anda" required="true" type="email">
                            <input class="input input_subscribe-submit" placeholder="" type="submit" value=""><span class="page__arrow-mini social__item_arrow-1-right"></span>
                        </fieldset>
                        <input type="hidden" name="time_zone" value="1"></form>
                </aside>
                <aside class="page__aside-top5">
                    <h3 class="page__header-mini page__header-mini_subscribe"> TOP <span>5</span></h3>
                    <ol class="top5">
                        <li class="top5__item"><a href="{{ $voluum }}"> Mario Teguh Rugi Sampai Rp 7 Miliar, Ini
                                Kata Deddy Corbuzier</a></li>
                        <li class="top5__item"><a href="{{ $voluum }}"> Baru 1 Bulan Bersama, Akhirnya Justin
                                Bieber - Sofia Richie Putus</a></li>
                        <li class="top5__item"><a href="{{ $voluum }}"> Bayi Adam Levine Akan Lahir, Konser Maroon
                                5 Dibatalkan</a></li>
                    </ol>
                    <div><a class="next-button" href="{{ $voluum }}"> Lainnya</a></div>
                </aside>
                <br>
                <h3 class="page__header-mini page__header-mini_aside"> Paling <span>  Menarik  </span></h3>
                <div class="most-intresting">
                    <div class="intresting-event">
                        <a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/teaser1.jpg') }}"></a>
                        <p><a href="{{ $voluum }}">Hasil saya: -16 kg dalam 20 hari. Di pagi hari, saya minum
                                segelas…</a></p>
                    </div>
                    <div class="intresting-event">
                        <a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/teaser2.jpg') }}"></a>
                        <p><a href="{{ $voluum }}">Pembakar lemak terhebat ini melangsingkan tubuh saya dalam 4
                                minggu! ULASAN</a></p>
                    </div>
                    <div class="intresting-event">
                        <a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/teaser3.jpg') }}"></a>
                        <p><a href="{{ $voluum }}">Hati-hati! Pembakar lemak ini dapat menyebabkan anoreksia!
                                Hanya minum…</a></p>
                    </div>
                    <div class="intresting-event">
                        <a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/teaser4.jpg') }}"></a>
                        <p><a href="{{ $voluum }}">Dokter menyarankan 3 produk ini untuk membantu turun berat!
                                Baca</a></p>
                    </div>
                    <div class="intresting-event">
                        <a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/teaser5.jpg') }}"></a>
                        <p><a href="{{ $voluum }}">Lemon adalah musuh terbesar dari lemak! Kamu bisa turun 16 kg
                                sebulan. Baca…</a></p>
                    </div>
                    <div class="intresting-event">
                        <a href="{{ $voluum }}"><img alt="" src="{{ asset('build/images/frontend/teaser6.gif') }}"></a>
                        <p><a href="{{ $voluum }}">Berat saya 95 kg, lalu turun 2 kg sehari! Setiap hari, saya
                                minum segelas…</a></p>
                    </div>
                </div>
            </aside>
        </div>
    </main>
    <footer class="footer column__container">
        <nav class="menu menu_menu-footer">
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}"> Beranda</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Bintang</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Monarki</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Fashion</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Pernikahan</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Anak</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Kecantikan</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Gaya hidup</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Bioskop</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}" title=""> Tubuh</a>
            <a class="menu__link menu__link_menu-footer" href="{{ $voluum }}"> Galeri</a>
        </nav>
        <div class="footer__inner">
            <div class="footer__logo"><a class="logo logo_footer" href="{{ $voluum }}"><span class="logo-sign">  Indonesia  </span></a>
            </div>
        </div>
        <span class="footer__gotop" style="display: block;"></span>
    </footer>
@endsection
