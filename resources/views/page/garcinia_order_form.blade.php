<div class="container">
    <div id="orderFormBorder" class="col-md-7">
        <p>{!! $header1 !!}</p>
        <p>{{ $header2 }}</p>
        <br>
        {!! form_start($form, ['id' => 'buyForm', 'class' => 'form-horizontal']) !!}

            {!! form_errors($form->first_name) !!}
            {{--{!! form_errors($form->last_name) !!}--}}
            {!! form_errors($form->email) !!}
            {!! form_errors($form->phone) !!}

            <div class="form-group">
                {!! form_label($form->first_name) !!}
                <div class="col-md-8">
                    {!! form_widget($form->first_name) !!}
                </div>
            </div>
            {{--<div class="form-group">--}}
                {{--{!! form_label($form->last_name) !!}--}}
                {{--<div class="col-md-8">--}}
                    {{--{!! form_widget($form->last_name) !!}--}}
                {{--</div>--}}
            {{--</div>--}}
            <div class="form-group">
                <label for="email" class="col-md-3 control-label"><span style="color:red">*</span>Alamat Email</label>
                <div class="col-md-8">
                    {!! form_widget($form->email) !!}
                </div>
            </div>
            <div class="form-group">
                <label for="telpon" class="col-md-3 control-label"><span style="color:red">*</span>Nomor Telpon</label>
                <div class="col-md-8">
                    {!! form_widget($form->phone) !!}
                    <span id="helpBlock" class="help-block">Diperlukan untuk melakukan pesanan</span>
                </div>
            </div>

            {!! form_widget($form->productId, ['value' => $productId]) !!}

        {!! form_widget($form->params, ['value' => $uriParams]) !!}

            <div class="form-group">
                <div class="col-md-8 col-md-offset-3">
                    <button type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" id="orderSubmit" class="btn btn-primary" style="background:#794293 !important;border:none;">Klik di sini untuk mendapatkan {{ $productName }}</button>
                </div>
            </div>
        {!! form_end($form, false) !!}
    </div>
</div>

