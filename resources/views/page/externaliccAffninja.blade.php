@extends('layout.admin_singin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

    <!-- page content -->
    <div class="right_col" role="main">

        @include('admin.errors')

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                {{--<div class="container-fluid">--}}
                    {{--<div class="col-md-6">--}}
                        {{--<div class="x_title">--}}
                            {{--<h2>--}}
                                {{--This month Sold :  {{ $thisMonthCounter }} Leads, {{ $thisMonthSold }} IDR--}}
                                {{--<small>{{ trans('admin.menu.list') }}</small>--}}
                            {{--</h2>--}}

                            {{--<div class="clearfix"></div>--}}
                        {{--</div>--}}
                {{--</div>--}}
                <div class="x_content">

                    <table class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Call Center Id</th>
                                <th class="column-title">{{trans('admin.common.name')}}</th>
                                <th class="column-title">{{trans('admin.common.phone')}}</th>
                                <th class="column-title">click_id</th>
                                <th class="column-title">{{trans('admin.common.status')}}</th>
                                <th class="column-title">{{trans('admin.common.sendedcc')}}</th>
                                <th class="column-title">{{trans('admin.common.date')}}</th>
                                <th class="column-title">Comment :</th>
                                <th class="column-title">Put Comment :</th>
                                <th class="column-title">Save Comment :</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($contacts as $contact)
                            {!! Form::open(['url' => 'externalicc/'.$contact->id]) !!}
                                <tr class="even pointer">
                                    <td class=" ">{{ $contact->cc_id ? $contact->cc_id : '-' }}</td>
                                    <td class=" ">{{ $contact->name }}</td>
                                    <td class=" ">{{ $contact->phone }}</td>
                                    <td class=" " style="max-width: 11em; word-wrap: break-word;">{{ $contact->click_id ? $contact->click_id : '--' }}</td>
                                    @if($contact->getStatus()=='SOLD')
                                        <td class=" " style="color:green">
                                    @elseif($contact->getStatus()=='CLOSED')
                                        <td class=" " style="color:red">
                                    @else
                                        <td class=" " style="color:blue">
                                    @endif
                                        {{ $contact->getStatus() }}</td>
                                    @if($contact->call_center==1)
                                        @if($contact->cc_id)
                                            <td class=" " style="color:green">
                                                {{ trans('admin.common.yes') }}
                                        @else
                                            <td class=" " style="color:red">
                                                {{ trans('admin.common.yes').' (But not called yet!)' }}
                                        @endif
                                    @else
                                            <td class=" " style="color:green">
                                                {{ trans('admin.common.no') }}
                                    @endif
                                    </td>
                                    <td class=" ">{{ $contact->created_at }}</td>
                                    <td class="break-words">{{ $contact->icc_comment ? $contact->icc_comment : 'no comment' }}</td>
                                    <td class=" ">
                                        {!! Form::textarea('comment', null, ['cols' => 10, 'rows' => 5]) !!}
                                    </td>
                                    <td class="  last">
                                        {!! Form::submit('Save Comment' ,['class' => 'btn btn-success']) !!}
                                    </td>
                                </tr>
                            {!! Form::close() !!}
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>

@endsection
