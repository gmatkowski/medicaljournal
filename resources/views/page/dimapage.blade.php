@extends('layout.app')
@section('title') - {{ $page->title }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')
	<main>
		<section class="contact">
			<div class="container">
				<h1 class="wow fadeIn">{!! $page->title !!}</h1>


				<div class="video">
					<div class="video-bg">
						<div id="player" data-id="MebhOoawIM8"></div>
					</div>
				</div>
				<br/><br/>

				<div class="row">
					<div class="wow col-md-12">
						{!! $page->content !!}
					</div>
				</div>
			</div>
		</section>

		@include('part.bottom_with_links')
	</main>

@endsection