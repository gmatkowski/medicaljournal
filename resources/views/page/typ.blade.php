@extends('layout.typ')

@section('analytics')
    @if(Session::has('ga-test'))
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter40118545 = new Ya.Metrika({ id:40118545, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript>
            <div><img src="https://mc.yandex.ru/watch/40118545" style="position:absolute; left:-9999px;" alt="" /></div>
        </noscript>
        <!-- /Yandex.Metrika counter -->

        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', '{{ Session::get('ga-test') }}', 'auto');
            ga('send', 'pageview');
        </script>
    @endif
    <!-- Conversion Pixel - INDO - Digital Commerce - Convertion Tag - Garcinia - DO NOT MODIFY -->
    <img src="‪https://secure.adnxs.com/px?id=788269&t=2‬" width="1" height="1" />
    <!-- End of Conversion Pixel -->
@endsection

@section('google')
    @if(Session::has('ga-test')==false)
        <!-- Google Code for Zakup Conversion Page -->
        <script type="text/javascript"> /* <![CDATA[ */ var google_conversion_id = 870757327; var google_conversion_language = "en"; var google_conversion_format = "3"; var google_conversion_color = "ffffff"; var google_conversion_label = "i91CCKr15WoQz-eanwM"; var google_remarketing_only = false; /* ]]> */ </script>
        <script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"> </script>
        <noscript>
            <div style="display:inline;">
                <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/870757327/?label=i91CCKr15WoQz-eanwM&amp;guid=ON&amp;script=0"/>
            </div>
        </noscript>
    @endif
@endsection

@section('content')
    <div class="typ-container">
        <div class="thumb">
            <img src="{{ asset('build/images/frontend/woman.png') }}" alt=""/>
        </div>
        <h1>Terima Kasih Atas<br/>Pemesanan Anda!</h1>

        <p>
            Tim kami akan segera menghubungi Anda melalui telpon<br/>
            untuk mengkonfirmasi pemesanan Anda dan alamat<br/>
            pengiriman.
        </p>

        <p class="grey">
            Alamat: <br />
            {{ $order->address->address }},<br/>
            {{ $order->getPostalCode() }} {{ $order->address->subdistrict }}
        </p>

        <p>
            Setelah melakukan konfirmasi melalui telpon,<br/>
            tim kami akan langsung mengirimkan barang Anda.
        </p>

        <p>
            Silahkan klik tombol <strong>"Ya, saya mengkonfirmasikan"</strong> untuk
            mengkonfirmasi atau klik tombol <strong>"Tidak, saya ingin<br/>
            merubah"</strong> untuk merubah data pemesanan Anda atau<br/>
            memperbaiki alamat pengiriman.
        </p>

        <div class="triggers">
            <a href="{{ route('page.confirm') }}" class="accept">YA, <br /> Saya Mengkonfirmasikan</a>
            <a href="" data-action="show-popup-form" class="address">TIDAK, <br /> Saya Ingin Merubah</a>
        </div>

        {!! Form::model($order, ['route' => ['page.editOrderAddress', 'id' => $order->address->id], 'data-container' => 'popup-form', 'class' => 'typ-form', 'enctype' => 'multipart/form-data']) !!}
            <div class="form-row">
                <label>Kelurahan</label>
                {!! Form::text('subdistrict', $order->address->subdistrict) !!}
            </div>

            <div class="form-row">
                <label>Alamat</label>
                {!! Form::text('address', $order->address->address) !!}
            </div>

            <div class="form-row">
                <label>Kode pos</label>
                {!! Form::text('postal_code', $order->getPostalCode()) !!}
            </div>

            <div class="form-row">
                <button type="submit">Saya Ingin Merubah</button>
            </div>
        {!! Form::close() !!}

        <p class="short">
            <strong>Terima kasih.</strong><br/>
            Semoga sukses!<br/>
            Salam,<br/>
            Tim Garcinia
        </p>
    </div>
@endsection
