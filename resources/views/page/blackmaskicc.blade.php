@extends('layout.admin_singin')
@section('content')

<!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>Black Mask Leads from 01 February 2017</h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Call Center Id :</th>
                            <th class="column-title">First Name :</th>
                            <th class="column-title">Phone :</th>
                            <th class="column-title">Created At :</th>
                            <th class="column-title">Comment :</th>
                            <th class="column-title">Put Comment :</th>
                            <th class="column-title">Save Comment :</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($orders as $order)
                            {!! Form::open(['url' => 'internalicc/'.$order->id]) !!}
                                <tr class="even pointer">
                                    <td class=" ">{{ $order->contact['cc_id'] ? $order->contact['cc_id'] : 'probably empty name/wrong phone number' }}</td>
                                    <td class=" ">{{ $order->first_name }}</td>
                                    <td class=" ">{{ $order->phone }}</td>
                                    <td class=" ">{{ $order->created_at }}</td>
                                    <td class="break-words">{{ $order->icc_comment ? $order->icc_comment : 'no comment' }}</td>
                                    <td class="  ">
                                        {!! Form::textarea('comment', null, ['cols' => 10, 'rows' => 5]) !!}
                                    </td>
                                    <td class="  last">
                                        {!! Form::submit('Save Comment' ,['class' => 'btn btn-success']) !!}
                                    </td>
                                </tr>
                            {!! Form::close() !!}
                        @endforeach
                    </tbody>
                </table>

                <div class="text-right">
                    @if ($orders->count() > 0)
                        {!! $orders->render() !!}
                    @endif
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection