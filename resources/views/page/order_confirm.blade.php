@extends('layout.app')

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/confirm/all.css') }}">
@endsection

@section('analytics')
    @if(app()->environment() == 'production')
        @include('pixel.doubleClickGTMhead')
        @if(strpos(URL::previous(), 'promos-seru.com/garcinia-cambogia2_main1') !== false)
            @include('ga.seru_garciniaCambogia2_main1')
        @endif

        @if(strpos(URL::previous(), 'promo-keren.com/garcinia_cambogia1') !== false)
            @include('ga.keren_garcinia_cambogia1')
        @endif

        @if(strpos(URL::previous(), 'promo-heboh.com/garcinia-cambogia_main') !== false)
            @include('ga.heboh_garcinia-cambogia_main')
        @endif

        @if(strpos(URL::previous(), 'promo-heboh.com/garcinia_cambogia1') !== false)
            @include('ga.heboh_garcinia_cambogia1')
        @endif

        @if(Session::has('ga-test'))
            <script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

                ga('create', '{{ Session::get('ga-test') }}', 'auto');
                ga('send', 'pageview');
                ga('require', 'ecommerce');
            </script>

            <iframe src="http://cootrking.com/p.ashx?a=177&e=227&t=TRANSACTION_ID" height="1" width="1" frameborder="0" style="display:none" ></iframe>
        @else
            @parent
            @include('ga.garcinia')
            @include('pixel.garcinia_conversion')
        @endif

        @if(isset($tracking))
            @include('ga.affbay', ['tracking' => $tracking])
        @endif
    @endif
@endsection

@section('trackConversions')
    @if(app()->environment() == 'production')
        @if(Session::has('ga-test'))
            <!-- Google Tag Manager -->
            <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                        'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
                })(window,document,'script','dataLayer','GTM-P8K73C');
            </script>
            <!-- End Google Tag Manager -->
        @endif
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-KRCHZ7X');</script>
        <!-- End Google Tag Manager -->
    @endif
@endsection

@section('pixel')
    @if(app()->environment() == 'production')
        @if(strpos(URL::previous(), 'promos-seru.com') !== false || strpos(URL::previous(), 'korting-harga.com') !== false || strpos(URL::previous(), 'penawaran-super.com') !== false || strpos(URL::previous(), 'medical-jurnal.com') !== false)
            @if($product=='garcinia')
                @include('pixel.cambogia_lead')
                @include('google.garcinia')
            @endif
            @if($product=='blackmask')
                @include('pixel.maskerhitamConfirm')
           @endif
           @if($product=='hallupro')
               @include('pixel.hallupro_lead')
               @include('google.hallux')
           @endif
       @else
           @if(isset($fb_pixel))
               @include('pixel.affbay', ['fb_pixel' => $fb_pixel])
           @endif
           @if($product=='posture')
               @include('pixel.posture_lead')
           @endif
       @endif
   @endif
@endsection

@section('content')

   @if(app()->environment() == 'production')
       @include('pixel.doubleClickGTMbody')
   @endif

   <div class="mod wrap_block_success">
       <div class="block_success">
           <h2>SELAMAT! PESANAN ANDA TELAH DITERIMA!</h2>
           <p class="success">
               Selanjutnya, operator pusat layanan informasi kami akan segera menghubungi Anda untuk mengkonfirmasi pesanan Anda. Pastikan ponsel Anda dalam kondisi aktif.
           </p>
           <h3>Silakan periksa informasi yang telah Anda masukkan</h3>
           <div class="wrap_list_info">
               <ul class="list_info">
                   <li><span>Nama: </span>
                       <text class="js-name">{{ $order ? $order->name : '' }}</text>
                   </li>
                   <li><span>Telepon: </span>
                       <text class="js-phone">{{ $order ? $order->phone : '' }}</text>
                   </li>
               </ul>
           </div>
           <p class="fail">
               <a href="#" onclick="history.go(-1); return false;">Apabila Anda membuat kesalahan dalam pengisian formulir, silakan kembali dan mengisi ulang</a></p>
       </div>
   </div>
@endsection

@section('endBodyConversion')
   @if($order)
       <script type="text/javascript">
           ga('send', {
               hitType: 'event',
               eventCategory: 'Checkout',
               eventAction: 'Zakup Garcinia2',
               eventLabel: 'ok',
               eventValue: '{{ $order->price }}'
           });
       </script>
   @endif
@endsection
