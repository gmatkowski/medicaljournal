@extends('layout.app',[
    'title' => 'Cara cepat melangsingkan tubuh secara alami'
])

@section('chat')@endsection

@section('content')

    <div class="page-container">
        <header id="main-header">
            <div class="data">
                <h1 class="logo">
                    <a href="{{ route('page.index') }}"><img src="{{asset('build/images/frontend/fi6WL4S/logo.png')}}" alt="Medic Reporters"></a>
                </h1>
                <div class="button facebook-box">
                    <div class="ribbon-3">
                        <a href="javascript: void(0)" onclick="return false;">Suka</a>
                    </div>
                </div>
                <form method="POST" accept-charset="utf-8">
                    <input required="required" type="text" placeholder="Cari..." name="q">
                    <button type="submit" name="search_submit"><i class="icons icon-search"></i></button>
                </form>
            </div>
            <button id="toggle-menu">
                <span></span>
                <span></span>
                <span></span>
            </button>
            <nav>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">HOME</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">DIET</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">FIT & OLAHRAGA</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">PSIKOLOGI</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">DI RANJANG</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">KECANTIKAN</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">KESEHATAN</a>
            </nav>
        </header>

        <ol class="breadcrumb">
            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display" itemprop="url">
                    <span itemprop="title">Home</span>
                </a>
            </li>
            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display" itemprop="url">
                    <span itemprop="title">Kesehatan</span>
                </a>
            </li>
            <li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
                <span itemprop="title">Penemuan yang menonaktifkan gen obesitas!</span>
            </li>
        </ol>
        <div class="page-view">
            <div class="left-side">
                <article class="main">
                    <header>
                        <h1>Penemuan yang menonaktifkan gen obesitas! Mengapa metode memecah sel-sel lemak didiamkan hampir selama 200 tahun?</h1>
                        <p>Metode penuruman berat badan yang luar biasa – untuk menurunkan hingga 8 kg lemak dalam 14 hari. Dengan cara ini bisa mengurangi berat badan secara permanen dalam 2 minggu. Mengapa penemuan tahun 1819, penemuan yang begitu inovatif, terjadi hanya hari ini? Semua ini sudah jelas: paten seorang ahli kimia Hans Christian Ørste, sangat menarik bagi dokter ahli gizi, penemuan ini luar biasa saja.</p>
                    </header>
                    <div class="body">
                        <h2>Mengapa cara Ørsted berhasil untuk melangsingkan tubuh?</h2>
                        <figure>
                            <div id="leftphoto">
                                <img src="{{asset('build/images/frontend/christian.jpg')}}" alt="Hans Christian Ørsted">
                                <figcaption>Hans Christian Ørsted</figcaption>
                            </div>
                            <div id="rightdesc">
                                <p>Hans Christian Ørsted adalah seorang penemu {{$product1->name}}, zat untuk pelangsing tubuh, yang sangat efektif. Dia adalah seorang ahli kimia terkenal dari Denmark. Dia bekerja sebagai apoteker yang berupaya menemukan solusi inovatif. Penemuan {{$product1->name}} ditemukan pertama kali oleh dia. {{$product1->name}} adalah zat yang <span class="yellowbg">membakar kalori dengan cepat</span> dan memecah sel-sel lemak secara permanen. Ørsted lama mempelajari efek zat {{$product1->name}} dari buah Malabar Tamarind, yang sekarang terkenal sebagai cara efektif untuk <b>memerangi obesitas</b>, di seluruh dunia. {{$product1->name}} adalah zat yang didiamkan selama 200 tahun, zat ini dapat membantu mengurangi berat badan secara cepat – dengan membakar lemak di tubuh, terutama di bagian paha dan perut, terlepas dari berat badan Anda sebelumnya.</p>
                            </div>
                            <p>Dengan cara yang lain, yaitu cara untuk menurunkan berat badan yang tradisional, Anda harus diet ketat, inilah metode yang membutuhkan waktu yang lama, dan Anda sering mengalami sindrom yoyo, yang berbahaya bagi kesehatan Anda. Dengan cara Ørsted, Anda akan berhasil menurunkan berat badan, karena cara ini memecah sel-sel lemak, pada tingkat seluler, jadi Anda tidak perlu motivasi, dan tidak perlu memiliki kemauan yang kuat.</p>
                        </figure>
                        <h2>Pelangsing badan secara cepat 24 jam sehari</h2>
                        <p>Karena {{$product1->name}} meningkatkan metabolisme dan membakar lemak 4 kali lebih cepat, 1 kapsul {{$product1->name}} cukup untuk membakar <u>hingga 950 kalori setiap hari</u>. Anda perlu menyadari, bahwa jika pencernaan menjadi lebih efektif (pada usus halus, pankreas dan perut), pengurangan lemak tubuh berlangsung <b>secara otomatis dan alami</b>. Anda tidak perlu menderita dengan melakukan program diet ketat, Anda tidak perlu berhenti makan makanan favorit Anda. {{$product1->name}} mengganggu fungsi gen yang membentuk lemak dan mencegah pertumbuhan sel-sel lemak baru, oleh karena itu Anda bisa <span class="yellowbg">menurunkan berat badan secara cepat</span>, tanpa perlu melakukan perubahan kebiasaan makan secara radikal. Apakah para ahli gizi sengaja menutup mata pada penemuan ini karena takut bahwa pasar akan didominasi oleh metode {{$product1->name}} yang paling efektif?</p>
                        <h2>Pendapat orang Indonesia tentang {{$product1->name}} Forte:</h2>
                        <p class="italic">"Saya tidak berharap saya akan memiliki sosok ramping dan indah. Dengan {{$product1->name}} Forte mimpi saya menjadi kenyataan. Saya turun 11 kg dalam 3 minggu! Saya senang, tak lagi malu untuk pergi kemana-mana. Sekarang tubuh langsing saya mampu membuat iri banyak wanita."</p>
                        <p>Penelitian menunjukkan jelas bahwa untuk menurunkan lemak tubuh, Anda perlu diet yang benar dan hanya {{$product1->name}} 95% dapat membantu Anda. Selama bertahun-tahun seorang penjual suplemen berusaha mengulang sukses ini 200 tahun kemudian. Mereka berusaha untuk mencapai {{$product1->name}} yang sangat tinggi konsentrasinya. Akhirnya mereka berhasil, dan menempatkan produk di pasar. Mereka memecah keheningan, mulai berbicara tentang <u>cara menurunkan berat badan yang sangat efektif</u> – {{$product1->name}} Forte.</p>
                        <h2>Wujudkanlah impian Anda untuk memiliki tubuh ideal, dengan mudah!</h2>
                        <p>Paha tanpa selulit, perut yang langsing, dan pinggang yang sempit. Lebih dari 27 900 wanita Indonesia sudah wujudkan impian mereka untuk memiliki tubuh langsing. Anda juga akan menggunakan pakaian kecil dan ketat, dan menjadi senang dengan <span class="yellowbg">tubuh langsing tanpa lemak</span>. <u>Semua teman Anda akan kagum</u> dengan sosok Anda! Mereka tidak akan bisa percaya bahwa Anda mendapatkan sosok langsing kembali hanya dalam 14 hari.</p>
                        <p>Ini waktu yang tepat untuk mengurus diri sendir – Anda akan melihat refleksi di cermin dengan senang hati, dan <b>medapatkan kepercayaan diri</b>. Dengan cara ini Anda akan menjadi menarik dan membuat orang lain tertarik pada Anda, dan tidak akan merasa malu untuk bertemu orang lain. Anda akan terkesan, karena Anda akan mengubah hidup Anda dengan mudah – cukup 2 minggu.</p>
                        <h2>Apa efek menggunakannya dalam hari-hari pertama?</h2>
                        <p>Dengan metode Ørsted, biasanya dalam 3-4 hari pertama Anda akan melihat hasilnya – <span class="yellowbg">mengecilkan lingkar perut</span> sekitar 1 cm. Dalam 1 minggu menggunakan {{$product1->name}} Forte, selain menghilangkan selulit, Anda akan mengecilkan perut buncit. Setelah 14 hari menggunakanya, Anda akan senang dan terkesan, karena Anda akan kehilangan berat badan dengan mudah.</p>
                        <p>Yakin bahwa tubuh langsing membuat Anda lebih percaya diri dan menarik dalam penampilan, membuat Anda merasa seperti wanita. Setelah 2 minggu menggunakan {{$product1->name}} Forte, Anda akan mendapat hasil yang sama dengan diet ketat, misalnya 30 hari Atkins Diet, yaitu - paha ramping, perut yang langsing dan tidak buncit, keberadaan bentuk dan ukuran pinggang lebih dikecilkan. Anda harus ingat, bahwa kami memberikan <b>Jaminan Kepuasan</b>. Kalau Anda gagal dalam menurunkan berat badan dengan {{$product1->name}} Forte, kami mengirim kembali uang Anda, secara cepat dan tanpa pertanyaan apapun.</p>
                        <p>Oleh karena itu, Anda bisa mencoba cara menguruskan badan dengan cepat, tanpa resiko apapun. Dengan mengklik pada link di bawah, Anda menerima diskon -70% dan mendapatkan kamasan asli {{$product1->name}} Forte yang paling tinggi konsentrasinya dibandingkan dengan suplemen lain yang tersedia di pasar, yaitu 95%. Penawaran ini hanya berlaku sampai {{ Carbon\Carbon::now()->format('d.m.Y') }}.</p>
                        <div class="link-box">
                            <p><span>PERHATIAN!</span> Hanya produk asli yang produk efektif!</p>

                            <p><a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">KLIK DISNI UNTUK ORDER ONLINE KAMASAN ASLI {{$product1->name}} FORTE 95% DENGAN PROMO</a></p>
                        </div>
                    </div>
                    <section class="comments">
                        <h3 class="std">Komentar:</h3>
                        <div class="comment">
                            <div class="title"><span>~simanjuntak</span></div>
                            <p>siapa yang menggunakannya? Bekerja?</p>
                        </div>
                        <div class="comment" style="padding-left: 20px;">
                            <div class="title"><span>~Merri Nura</span></div>
                            <p>Alhamdulillah, aku nggak perlu turun :D tapi ibu saya senang dengan garcinia cambogia karena bagus dn jauh dari efek samping berbahaya</p>
                        </div>
                        <div class="comment" style="padding-left: 20px;">
                            <div class="title"><span>~Andi Putra</span></div>
                            <p>@simanjuntak: tak ada jawaban, harus coba sendiri, lebih menarik daripada mengobrol di internet;)</p>
                        </div>
                        <div class="comment" style="padding-left: 20px;">
                            <div class="title"><span>~Elsa Novriyani</span></div>
                            <p>Assalammualaikum sume, aku guna tapi hanya 1 minggu;) ku turun 2 kg, oke</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Revie Tariyo</span></div>
                            <p>aku beli untuk suamiku;) selama 2 thn dia punya perut buncit dn tak bolek kecilkannyaaa dengan cara lain</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~erwin handoyo</span></div>
                            <p>Sulit dipercaya tapi benar ada?mungkin perlu masakan yang menggunakan malabar tamarind aja untuk membuat semua wanita langsing dn gembira;]</p>
                        </div>
                        <div class="comment" style="padding-left: 20px;">
                            <div class="title"><span>~Rahmadillaa</span></div>
                            <p>bodoh, berpikirlah lebih banyak, bacalah tentang ini- bukan malabar tamarind yang menurunkan berat badan -- ini garcinia cambogia yg menurunkannya, zat yang diisolasi dalam laboratorium</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Aimelda Satrini</span></div>
                            <p>dimana bisa belinya? bisa belinya di toko aja atau farmasi???</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Himelda Hazirah Amarullah</span></div>
                            <p>artikelnya sangat bermanfaat...apakah ini benar?</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Darmi Imanina</span></div>
                            <p>menurut saya bagus sekali <3 sdh gunakan selama 10 hari dn turun 4,5 kg udah :) :) :) apa yaang ku harapkan serius ini berat bdann 50 kg:)</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Neneng Zulkarian</span></div>
                            <p>oke harus coba :) sdh beli dn tunggu kurier :)</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Wahyuni Armianti</span></div>
                            <p>ya saya menggunakan garcinia cambogia ini setelah melahirkan, karena berat badan naik banyak saat hamil, skng tak ada kg ini, saya berhasil, bagus</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Rahma Widodo</span></div>
                            <p>Akhirnya ada di Indonesia!! Adik saya tinggal di London dan di sana selama beberapa bulan bisa dibeli;P</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Muhammad Istiq</span></div>
                            <p>olahraga lebih enak dr pada suplementasi, dn aku rekomendasi makan sedikit, lebih sehat- cara yang paling bagus</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Aufa Wandani</span></div>
                            <p>apakah ini benar garcinia cambogia menghilangkan selulit secara cepat? sayaa tertarik, tak perlu tutunkan bb, tapi kulit saya menjadi kasar seperti kulit jeruk…</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Ayu Moet</span></div>
                            <p>udah beli untuk ibu saya, dia senang dengan hasilnya, jadi saya rekomendasi :)</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Susmarita Sari</span></div>
                            <p>aku mahasiswa, belajar kedokteran, di universitas saya sudah mempelajari hal tersebut secara teori, semua mahasiswa kedokteran mengetahui hal ini:D</p>
                        </div>
                        <div class="comment" style="padding-left: 20px;">
                            <div class="title"><span>~Lidya Putri</span></div>
                            <p>tapi ntk orang lain ini informasinya baru;) pertama kali mendengar :D</p>
                        </div>
                        <div class="comment">
                            <div class="title"><span>~Yanti</span></div>
                            <p>aku belum pernah menggunakan suplemen, tapi udah mendengr bahwa suplementasi selalu bekerja secara berbeda pada orang yang berbeda, ada orang yang turunkan berat badan 3 kg, ada orang yg lain yg turunkan 8 kg. Sulit untuk generalisasi, harus coba sendiri</p>
                        </div>
                        <h3 class="std">Poskan komentar:</h3>
                        <form method="POST">
                            <textarea name="comment_text" cols="40" rows="10" placeholder="Poskan komentar" required></textarea>
                            <input type="text" name="comment_user" placeholder="Nama" required>
                            <input type="submit" name="comment_submit" value="Poskan komentar">
                        </form>
                    </section>
                </article>

                <aside class="hidden-xs">
                    <a href="http://www.accuweather.com/id/id/jakarta/208971/weather-forecast/208971" class="aw-widget-legal">
                        <!--
						By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
						-->
                        </a><div id="awcc1472489642912" class="aw-widget-current"  data-locationkey="208971" data-unit="c" data-language="id" data-useip="false" data-uid="awcc1472489642912"></div>
                        <script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
                    <div class="facebook-box"><div class="ribbon-1"><a href="javascript: void(0)" onclick="return false;"></a></div><div class="ribbon-2"><a href="javascript: void(0)" onclick="
																					return false;">Medical Jurnal</a></div><div class="ribbon-3"><a href="javascript: void(0)" onclick="
																							return false;">Suka</a></div><div class="ribbon-4"><p>2314 lainnya menyukai ini <a href="javascript: void(0)" onclick="
																									return false;">Medical Jurnal</a></p></div><div class="ribbon-5"></div><div class="ribbon-6"><a>Plugin sosial Facebook</a></div></div>
                </aside>
            </div>
        </div>
    </div>

    <footer id="main-footer">
        <div class="page-container">
            <a href="{{ route('page.index') }}"><img class="footer-logo" src="{{asset('build/images/frontend/fi6WL4S/logo.png')}}" alt="Medic Reporters"></a>
            <nav>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">Home</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">Diet</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">Fit & Olahraga</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">Psikologi</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">Di ranjang</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">Kecantikan</a>
                <a href="{{ route('page.preorder') }}?utm_source=artykul&utm_medium=liputan{{ $medium }}&utm_campaign=Display">Kesehatan</a>
            </nav>
        </div>
        <div class="page-container copy">
            <hr>
            HAK CIPTA 2016 SEMUA HAK CIPTA TERPELIHARA
        </div>
    </footer>

    <div id="popup" style="display: none;">
        <div class="window">
            <h1>Terima kasih!</h1>
            <p>Komentar Anda telah dikirim dan sedang menunggu moderasi.</p>
            <button class="close" onclick="$('#popup').hide();">TUTUP</button>
        </div>
    </div>

    <script src="{{ elixir('js/frontend/all.js') }}"></script>

@endsection