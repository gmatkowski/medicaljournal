@extends('layout.app',[
    'title' => 'Cara menurunkan berat badan'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/pre/all.css') }}">
@endsection

@section('pixel')
    @include('pixel.cambogia_main')
@endsection

@section('analytics')
    @if(Session::has('ga-test'))
        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter40118545 = new Ya.Metrika({ id:40118545, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript>
            <div><img src="https://mc.yandex.ru/watch/40118545" style="position:absolute; left:-9999px;" alt="" /></div>
        </noscript>
        <!-- /Yandex.Metrika counter -->

        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-P8K73C');</script>
        <!-- End Google Tag Manager -->

        @include('ga.garcinia_test')

        <iframe src="http://cootrking.com/p.ashx?a=177&e=227&t=TRANSACTION_ID" height="1" width="1" frameborder="0"></iframe>
    @else
        @include('ga.garcinia')
    @endif
@endsection

@section('content')
    <!-- Google Tag Manager (noscript) -->
    <noscript>
        <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8K73C" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <!-- End Google Tag Manager (noscript) -->

    <div class="wrapper  index2">
        <div class="row">
            <div class="content" style="width: 100%;">
                <h1>TURUN <strong>BERAT BADAN BERLEBIH</strong>.<br>CEPAT DAN MUDAH DENGAN GARCINIA CAMBOGIA</h1>

                <section class="block-1 margin-top-20">
                    <img src="{{ asset('build/images/frontend/spec-1-2-lite.jpg') }}" id="mobileFruit" class="block-1-img" alt="Radosław Matuszak">

                    <p class="margin-top-10">Garcinia Cambogia mengandung konsentrat yang secara aktif menghancurkan timbunan lemak di dalam tubuh. Meningkatkan fungsi metabolisme, detoksifikasi tubuh Anda, dan membuang radikal bebas.</p>
                    <p class="margin-top-10">Anti oksidan alami menjaga kesehatan dan membuat penampilan menjadi makin menarik</p>
                    <p class="margin-top-20">Hasil ini bisa disebabkan karena penemuan saya bekerja dalam 3 cara, yaitu:</p>

                    <h2 class="one-line margin-top-20 fronth1">MEMPERCEPAT METABOLISME TUBUH ANDA</h2>
                    <h2 class="margin-top-20 fronth1">MENCEGAH PERTUMBUHAN SEL-SEL LEMAK BARU</h2>
                    <h2 class="one-line margin-top-20 fronth1">MENGURANGI SELULIT DAN MENGENCANGKAN KULIT TUBUH</h2>

                    <p class="margin-top-20">Murni ini dijual dalam bentuk kapsul 520 mg, dengan nama dagang {{$product1->name}} Forte, dan mempunyai kualitas yang lebih baik dan tinggi dibandingkan dengan suplemen lain yang tersedia di pasar.</p>
                    <p class="margin-top-20">Inilah ekstrak buah yang Anda makan setiap hari, Malabar Tamarind yang ada di pasar. Zat yang dihasilkan oleh buah tersebut adalah {{$product1->name}} - yang memiliki properti penurunan berat badan. {{$product1->name}} dari buah Malabar Tamarind, dengan dosis yang tepat, akan membuat pencernaan menjadi lebih efektif hingga 89%.</p>

                    {{--<h3>Satu-satunya cara yang memberikan jaminan kepuasan 100%</h3>--}}
                    {{--<p class="center">Garansi uang kembali bila gagal atau tidak puas dengan hasilnya. Kalau Anda gagal dalam menurunkan berat badan 4 kg dalam 1 bulan, kirimkan bungkus kosong kembali pada kami, dan kami mengirim kembali uang Anda.</p>--}}
                    <h3>Bagaimana cara memesan {{$product1->name}} Forte dan menghemat {!! StrHelper::spaceInPrice($product1->price, $product1->price_old) !!} {{$product1->currency}}?</h3>
                    <p>Kualitas terbaik ekstrak {{$product1->name}} sulit untuk mendapat, jadi harganya tidak bisa murah. Namun, produsen telah memberikan diskon khusus bagi pelanggan yang pertama kali melakukan pembelian {{$product1->name}} Forte. Anda cukup memesan hari ini, maka Anda akan mendapatkan diskon spesial -57%, untuk alami dan kemurnian tinggi ekstrak {{$product1->name}} dalam kapsul.</p>
                    <p class="margin-top-15">Hari ini ada promosi: kemasan {{$product1->name}} Forte asli HANYA HARI INI dijual dengan harga <span style="text-decoration: line-through;">{!! StrHelper::spaceInPrice($product1->price_old) !!} {{$product1->currency}}</span> {!! StrHelper::spaceInPrice($product1->price) !!} {{$product1->currency}} saja.</p>
                    <p>Anda cukup klik pada tombol order dan mengisi formulir online dengan benar. {{$product1->name}} Forte akan kami kirimkan ke alamat Anda dalam waktu maks. 2 hari.
                       {{--Sekarang, apa yang Anda lakukan, adalah mencoba {{$product1->name}} ini. Jika Anda tidak puas dengan hasilnya, tidak peduli untuk alasan apa, kirim bungkus kosong kembali pada kami saja, dan kami mengirim kembali uang Anda, secara cepat dan tanpa pertanyaan apapun.--}}
                    </p>
                </section>

                <section>
                    <div class="product-order">
                        <p><b>Kemasan asli</b>,<br> murni ekstrak {{$product1->name}} <b>95%</b></p>
                        <div class="product-image">
                            <img src="{{ asset('build/images/frontend/garciniacambogia.jpg') }}" alt="{{$product1->name}} Forte">
                            <div class="price">
                                <span>{!! StrHelper::spaceInPrice($product1->price_old) !!} {{ $product1->currency }}</span>
                                {!! StrHelper::spaceInPrice($product1->price) !!} {{ $product1->currency }}
                            </div>
                        </div>
                        <p>Anda akan menghemat uang Anda jika membayar pada hari ini: mendapatkan diskon langsung sebesar {{ $product1->currency }} {!! StrHelper::spaceInPrice($product1->price, $product1->price_old) !!}, pengiriman gratis dan Jaminan Keputusan. Hanya hari ini!</p>
                        {{--<a class="greenBtn" href="{{ url('/order') }}">PESAN SEKARANG</a>--}}
                        @include('page.garcinia_order_form', [
                             'form' => $form,
                             'header1' => 'Tinggalkan nomor telepon anda,consultant kami akan segera menghubungi anda dan memberikan konsultasi gratis untuk anda.',
                             'header2' => 'Pengiriman Garcinia Cambogia Forte dilakukan melalui kurir atau pos, dalam beberapa hari kedepan, secara gratis. Metode pembayaran: COD (Pembayaran di Tempat).',
                             'productId' => $product1->id,
                             'productName' => $product1->display_name,
                             'width' => 'width: 80%;'
                         ])
                    </div>
                </section>

                <!-- WhatsApp Garcinia Block Start -->
                <div id="wa-wrapper-grey">

                    <div id="webwhatsapp">
                        <img src="{{ asset('build/images/frontend/wa-icon.png') }}" alt="">
                        <p>Order by WhatsApp</p>
                        <p class="green">{{ Config::get('whatsapp.number2') }}</p>
                    </div>

                    <div id="mobilewhatsapp">
                        <a href="whatsapp://send?text=whatsapp">
                            <img src="{{ asset('build/images/frontend/wa-icon.png') }}" alt="">
                            <p>Order by WhatsApp</p>
                            <p class="green">{{ Config::get('whatsapp.number2') }}</p>
                        </a>
                        <a href="intent://send/{{ str_replace(")", "", str_replace("(", "", str_replace(" ", "", Config::get('whatsapp.number2')))) }}#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end">Open WhatsApp chat window</a>
                    </div>

                </div>
                <!-- WhatsApp Garcinia Block End -->

                <footer id="footer">
                    <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
                </footer>
            </div>
        </div>

        <footer id="mobile-footer">
            <a href="#">Kebijakan Privasi</a>
        </footer>
        <br>
        <div class="disclaimer" style="width:100%;">
            <div style="text-align:center">
                <h2>Hubungi kami melalui</h2>
                <h3>nomor: 0888 01000 488</h3>
                <h3>email: contact@medical-jurnal.com</h3>
            </div>
            <p>Produk ini adalah suplemen diet, bukan obat untuk pengobatan penyakit, suplemen makanan tidak boleh digunakan sebagai pengganti makanan yang bervariasi dan seimbang. Sebuah diet yang bervariasi dan seimbang dan gaya hidup sehat yang dianjurkan. Paket tersebut berisi 15 kapsul dan cukup untuk 2 minggu terapi.</p>
            <p>Untuk menyediakan layanan yang efisien, dan kepentingan keselamatan pemilik website ini berhak untuk melakukan perubahan terhadap Kebijakan Privasi. Tindakan ini bertujuan untuk menjaga anonimitas pelanggan-pelanggan. Untuk meningkatkan kepuasan dengan layanan yang diberikan kepada pelanggan-pelanggan, namanya telah diubah demi keamanan dan privasi. Setiap kemiripan dengan orang-orang yang nyata adalah murni kebetulan.</p>
        </div>
    </div>

    <script src="{{ asset('build/js/vendor/frontend/jquery.modal.min.js') }}"></script>
    {{--@include('part.preorderPopup', ['productSymbol' => 'cambogia'])--}}

    <script type="text/javascript" src="{{ elixir('js/frontend/all.js') }}"></script>

@endsection