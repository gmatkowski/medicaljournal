@extends('layout.admin_singin')
@section('title') :: missing Areas @endsection
@section('content')

    <!-- page content -->
    <div class="right_col" role="main">

        @include('admin.errors')

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="container-fluid">
                    <div class="col-md-6">
                        <div class="x_title">
                            <h2>missing Areas</h2>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>

                <div class="x_content">

                    <table class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Id.</th>
                                <th class="column-title">Province:</th>
                                <th class="column-title">City:</th>
                                <th class="column-title">District:</th>
                                <th class="column-title">Courier:</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($missingAreas as $key => $missingArea)
                                <tr class="even pointer">
                                    <td class=" ">
                                        {{ ++$key }}
                                    </td>
                                    <td class=" ">
                                        {{ $missingArea['province'] }}
                                    </td>
                                    <td class=" ">
                                        {{ $missingArea['city'] }}
                                    </td>
                                    <td class=" ">
                                        {{ $missingArea['district'] }}
                                    </td>
                                    <td class="  last">
                                        {{ $missingArea['courier'] }}
                                    </td>
                                </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>

@endsection
