@extends('layout.app')
@section('title') :: {{ trans('menu.terms') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')
	<main>
		<section class="contact">
			<div class="container">
				<h1 class="wow fadeIn">{!! $page->title !!}</h1>

				<div class="row">
					<div class="wow col-md-12">
						{!! $page->content !!}
					</div>
				</div>
			</div>
		</section>
	</main>

@endsection