@extends('layout.admin_singin')
@section('content')

<!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>All Leads Summary from today ({{ \Carbon\Carbon::now()->format('d/m/Y') }})</h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Leads sent to cc :</th>
                            <th class="column-title">Sold :</th>
                            <th class="column-title">Sold (%) :</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr class="even pointer">
                            <td class=" ">{{ $leadsSent }}</td>
                            <td class=" ">{{ $sold }}</td>
                            <td class="  last">{{ $leadsSent !== 0 ? round(($sold / $leadsSent) * 100) : 0 }} %</td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection