@extends('layout.app',[
    'title' => 'KISAH SAYA TENTANG BAGAIMANA SAYA BERHASIL MENURUNKAN 27 KILO AKAN MEMBUAT MULUT ANDA TERNGANGA | Buku harian saya'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/bukuhariansaya/all.css') }}">
@endsection

@section('content')
    <div id="outer-wrap">
        <div id="inner-wrap">
            <div id="td-mobile-nav">
                <div class="td-mobile-close">
                    <a href="{{ $voluum }}">TUTUP </a>
                    <div class="td-nav-triangle"></div>
                </div>
                <div class="td-mobile-content">
                    <div class="menu-belleza-container">
                        <ul class="" id="menu-belleza">
                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-first td-no-down-arrow td-not-mega menu-item-25824" id="menu-item-25824">
                                <a href="{{ $voluum }}"><span class="menu_icon td-sp td-sp-ico-home"></span><span class="menu_hidden">   Laman Depan   </span></a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega menu-item-25811" id="menu-item-25811">
                                <a href="{{ $voluum }}">
                                    MAKEUP </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega menu-item-25819" id="menu-item-25819">
                                <a href="{{ $voluum }}">
                                    RAMBUT </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent td-not-mega menu-item-25812" id="menu-item-25812">
                                <a href="{{ $voluum }}">
                                    TUBUH </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega menu-item-25822" id="menu-item-25822">
                                <a href="{{ $voluum }}">
                                    KRIM </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega menu-item-25813" id="menu-item-25813">
                                <a href="{{ $voluum }}">
                                    SELEBRITI </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega menu-item-31403" id="menu-item-31403">
                                <a href="{{ $voluum }}">
                                    MAKANAN YANG INDAH </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent td-not-mega menu-item-25821" id="menu-item-25821">
                                <a href="{{ $voluum }}">
                                    KEBUGARAN </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega menu-item-25820" id="menu-item-25820">
                                <a href="{{ $voluum }}">
                                    PARFUM </a>
                            </li>
                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega menu-item-25823" id="menu-item-25823">
                                <a href="{{ $voluum }}">
                                    TUTORIAL </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="td-header-menu-wrap">
                <div class="container">
                    <div class="row">
                        <div class="span9">
                            <div class="td_data_time">
                                <script type="text/javascript">
                                    dtime_nums(-1, true);
                                </script>09.02.2017
                            </div>
                            <div class="menu-top-container">
                                <ul class="top-header-menu" id="menu-menu-principal">
                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-first td-not-mega menu-item-7408 hidden-element" id="menu-item-7408">
                                        <a href="{{ $voluum }}">
                                            HALAMAN DEPAN </a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-11253 hidden-element" id="menu-item-11253">
                                        <a href="{{ $voluum }}">
                                            Hadiah dan pers </a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-14112 hidden-element" id="menu-item-14112">
                                        <a href="{{ $voluum }}">
                                            Kontak </a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-28630 hidden-element" id="menu-item-28630">
                                        <a href="{{ $voluum }}">
                                            Laura Wijaya </a>
                                    </li>
                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-30847 hidden-element" id="menu-item-30847">
                                        <a href="{{ $voluum }}">
                                            Wawancara </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="span3"></div>
                    </div>
                </div>
            </div>
            <div class="td-header-bg">
                <div class="container header-style-3">
                    <div class="row">
                        <div class="span12 td-full-logo" role="banner">
                            <div class="td-grid-wrap">
                                <div class="container-fluid">
                                    <a href="{{ $voluum }}">
                                        <h1><span>CATATAN HARIANKU</span></h1>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="td-menu-placeholder">
                <div class="td-menu-background">
                    <div class="container td-menu-wrap" style="position: relative;">
                        <div class="row-fluid td-menu-header">
                            <div class="span11">
                                <div id="td-top-mobile-toggle">
                                    <ul class="sf-menu">
                                        <li>
                                            <a href="{{ $voluum }}">
                                                <span class="menu_icon td-sp td-sp-ico-menu"></span> </a>
                                        </li>
                                    </ul>
                                </div>
                                <div id="td-top-menu" role="navigation">
                                    <div class="menu-belleza-container">
                                        <ul class="sf-menu sf-js-enabled" id="menu-belleza-1">
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-first td-no-down-arrow td-not-mega menu-item-25824">
                                                <a href="{{ $voluum }}"><span class="menu_icon td-sp td-sp-ico-home"></span><span class="menu_hidden">   Laman Depan   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega td-mega-menu menu-item-25811 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    MAKEUP <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega td-mega-menu menu-item-25819 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    RAMBUT <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent td-not-mega td-mega-menu menu-item-25812 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    TUBUH <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega td-mega-menu menu-item-25822 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    KRIM <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega td-mega-menu menu-item-25813 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    SELEBRITI <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega td-mega-menu menu-item-31403 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    MAKANAN YANG INDAH <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category current-post-ancestor current-menu-parent current-post-parent td-not-mega td-mega-menu menu-item-25821 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    KEBUGARAN <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega td-mega-menu menu-item-25820 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    PARFUM <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                            <li class="menu-item menu-item-type-taxonomy menu-item-object-category td-not-mega td-mega-menu menu-item-25823 hidden-element">
                                                <a class="sf-with-ul" href="{{ $voluum }}">
                                                    TUTORIAL <span class="sf-sub-indicator">    »   </span></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="span1" id="td-top-search">
                                <div class="header-search-wrap">
                                    <div class="dropdown header-search">
                                        <a class="dropdown-toggle" data-toggle="dropdown" href="{{ $voluum }}" id="search-button" role="button"><span class="td-sp td-sp-ico-search"></span></a>
                                        <div aria-labelledby="search-button" class="dropdown-menu">
                                            <form action="{{ $voluum }}" class="td-search-form" role="search">
                                                <div class="td-head-form-search-wrap">
                                                    <input autocomplete="off" class="needsclick" id="td-header-search" name="s" type="text" value="">
                                                    <input class="wpb_button wpb_btn-inverse btn link" id="td-header-search-top" type="submit" value="Buscar">
                                                </div>
                                                <input type="hidden" name="time_zone" value="1"></form>
                                            <div id="td-aj-search"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container header-content-rec">
                <div class="row">
                    <div class="span12"></div>
                </div>
            </div>
            <div class="container td-page-wrap">
                <div class="row">
                    <div class="span12">
                        <div class="td-grid-wrap">
                            <div class="container-fluid">
                                <div class="row-fluid">
                                    <div class="span8 column_container" itemprop="mainContentOfPage" role="main">
                                        <div class="entry-crumbs">
                                            <a class="entry-crumb" href="{{ $voluum }}" itemprop="breadcrumb"> Laman Depan </a> <span class="td-sp td-sp-breadcrumb-arrow td-bread-sep"></span> <a class="entry-crumb" href="{{ $voluum }}" itemprop="breadcrumb"> Tubuh </a> <span class="td-sp td-sp-breadcrumb-arrow td-bread-sep"></span> <a class="entry-crumb" href="{{ $voluum }}" itemprop="breadcrumb"> Diet </a>
                                        </div>
                                        <article class="post-38234 post type-post status-publish format-standard has-post-thumbnail hentry category-aceite-cuerpo10 category-salud category-crema-hidratante category-cuerpo10" id="post-38234">
                                            <header>
                                                <h1 class="entry-title" itemprop="name">
                                                    KISAHKU TENTANG BAGAIMANA AKU BERHASIL MENURUNKAN 27 KILO BERAT BADANKU AKAN MEMBUAT ANDA TERNGANGA
                                                </h1>
                                                <div class="meta-info">
                                                    <ul class="td-category">
                                                        <li class="entry-category">
                                                            <a href="{{ $voluum }}">
                                                                Tubuh </a>
                                                        </li>
                                                        <li class="entry-category">
                                                            <a href="{{ $voluum }}">Garcinia Cambogia</a>
                                                        </li>
                                                        <li class="entry-category">
                                                            <a href="{{ $voluum }}">
                                                                Kebugaran </a>
                                                        </li>
                                                    </ul>
                                                    <time class="entry-date updated">
                                                        <script type="text/javascript">
                                                            dtime_nums(-30, true);
                                                        </script>11.01.2017
                                                    </time>
                                                    <meta content="UserComments:4" itemprop="interactionCount">
                                                    <div class="entry-comments-views">
                                                        <span class="td-sp td-sp-ico-comments td-fake-click"></span>152
                                                    </div>
                                                </div>
                                            </header>
                                            <div class="td-post-text-content">
                                                <br>
                                                <img alt="" src="{{ asset('build/images/frontend/photo_1.jpg') }}" style="float: left; margin: 0px 15px 5px 0px;">
                                                <p>
                                                    Hai semua....
                                                </p>
                                                <p>
                                                    Hari ini saya memutuskan untuk menceritakan beberapa rahasia yang hanya
                                                    kutulis di dalam buku harianku kepada Anda semua... saya tidak ingin menceritakan yang
                                                    sebenarnya, tapi saya akan mencoba menceritakan semua kekhawatiran dan
                                                    perasaan saya sehingga Anda semua bisa lebih memahami kisah saya ini nantinya. Jujur
                                                    saja, saya menceritakan kisah yang sangat personal ini bukan untuk mendapatkan lebih banyak
                                                    follower dan menjadi terkenal , saya hanya ingin membantu mereka yang sedang berjuang menghadapi
                                                    masalah yang sama seperti yang pernah saya hadapi sebelumnya.
                                                </p>
                                                <p>
                                                    Tahukah anda bahwa dua tahun yang lalu, saya pernah mengalami gangguan psikologis? Saya merasa
                                                    sangat kesepian dan tertekan, keluarga saya tidak bisa berbuat banyak kecuali
                                                    membawa saya ke rumah sakit untuk memberikan saya perawatan khusus agar saya saya merasa lebih baik.
                                                </p>
                                                <p>
                                                    Sekarang saya mengerti , mengapa dulu saya merasa kesepian… Coba anda lihat foto saya dan pasti anda juga akan mengerti.
                                                </p>
                                                <p>
                                                    Bukan rahasia lagi bahwa jika kita tidak bisa mencintai diri sendiri, tidak akan ada yang mencintai kita.
                                                    Ya… dulu saya sangat membenci diri saya sendiri , saya tidak punya teman, tidak punya pacar ataupun kehidupan
                                                    sosial yang aktif, karena saya benci dan malu dengan penampilan fisik saya.
                                                </p>
                                                <p>
                                                    <img alt="" src="{{ asset('build/images/frontend/photo_2.jpg') }}">
                                                </p>
                                                <p>
                                                    Bayangkan , Teman-teman memanggilku dengan sebutan babi, gendut , sapi, paus yang terdampar, anjing laut,... dan
                                                    percayalah semua julukan itu masih ringan dibandingkan dengan yang pernah saya dapatkan. Tidak heran jika akhirnya saya merasa
                                                    depresi dan berakhir di rumah sakit jiwa...
                                                </p>
                                                <p>
                                                    Setelah dua bulan berada di rumah sakit, akhirnya saya kembali ke rumah, tapi setiap minggunya saya tetap harus menemui
                                                    Dr. Kevin untuk  membahas semua ketakutan saya dan mencari cara untuk mengatasi semua itu. Dengan mudah, dokter langsung
                                                    menyimpulkan bahwa yang menjadi sumber utama permasalahan saya adalah penampilan fisik saya, terutama berat badan saya.
                                                    Dokter Kevin selalu berpesan bahwa saya harus lebih percaya diri dan berusaha menyakinkan saya bahwa kepribadian yang baik
                                                    itu jauh lebih penting daripada kecantikan fisik. Tapi hanya saya sangat tahu apa yang sebenarnya saya butuhkan!
                                                    Saya harus menurunkan berat badan saya.
                                                </p>
                                                <img alt="" src="{{ asset('build/images/frontend/photo_3.jpg') }}" style="float: left; margin: 0px 15px 5px 0px; width: 50%;">
                                                <p>
                                                    Setelah semua upaya yang saya telah lakukan untuk menurunkan berat badan saya, seperti
                                                    jogging dan olahraga lainnya, suatu hari saya bertemu teman lama saya ,Linda.
                                                    Saya sangat terkejut melihat dia sekarang , dia begitu cantik: kakinya jenjang, pinggangnya kecil dengan
                                                    pinggul yang berisi. Dia sangat populer di perusahaan tempatnya bekerja...
                                                    Para pria tidak bisa berhenti menatapnya.
                                                </p>
                                                <p>
                                                    Saat kami berjalan-jalan dan masuk ke sebuah toko makanan ringan dan cokelat, saya
                                                    terheran-heran saat melihat Linda membeli semua makanan itu . Dalam pikiran saya
                                                    pastinya wanita dengan tubuh sesempurna dia pasti tidak akan makan makanan yang tidak
                                                    sehat seperti ini, bagaimana mungkin dia berhasil mempertahankan tubuhnya tetap
                                                    seksi dan kencang tapi di satu sisi dia juga mengkonsumsi semua makanan sampah itu.
                                                </p>
                                                <p>
                                                    Sayapun akhirnya  bercerita kepada dia bahwa saya sangat ingin seperti dia , membeli
                                                    permen, kripik dan coklat lalu mengonsumsi semuanya, tapi itu adalah pilihan yang sangat
                                                    sulit buat saya. Hampir semua orang di sekitar saya tahu bahwa saya pernah mengalami gangguan
                                                    psikologis  karena berat badan saya dan mereka selalu bergosip tentang hal itu.
                                                </p>
                                                <img alt="" class="img_content" src="{{ asset('build/images/frontend/photo_4.jpg') }}" style="float: right; margin: 0px 0px 15px 15px;width: 100%; max-width: 300px;">
                                                <p>
                                                    Dan akhirnya, dia pun membocorkan rahasianya selama ini: bahwa setiap
                                                    hari dia mengkonsumsi <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a>.
                                                    Saya belum pernah mendengar tentang produk ini sebelumnya... Kemudian dia menjelaskan
                                                    kepada saya bahwa produk ini adalah obat tradisional yang sangat membantu kita untuk menurunkan
                                                    berat badan kita, tetapi kita masih  tetap bisa makan semua yang kita mau.
                                                    Terdengar mustahil, bukan? Ya…saya tahu…Saya pun berpikir demikian pada awalnya.
                                                    Saya pikir produk ini mungkin memiliki efek samping buruk pada kesehatan saya, tapi kok
                                                    Linda selalu tampil bugar dan suasana hatinya pun selalu baik. Dan saya ? Seringkali
                                                    saya merasa lelah dan marah, tapi Linda selalu tampak berenergi dan ceria. Ya Tuhan!
                                                    Saya benar-benar iri!
                                                </p>
                                                <p>
                                                    Melihat bukti nyata dari Linda, tanpa berpikir dua kali, saya pun segera mencoba produk ini...
                                                    Dan wow… setelah 14 hari, berat badan saya turun 22,5 kilo. Saya tidak percaya dengan apa yang saya lihat!
                                                    Lemak di tubuh saya menghilang dengan sangat cepat, walaupun saya tetap makan semua kue, es krim, keripik, selai kacang, dll.
                                                </p>
                                                <p>
                                                    Sekarang setelah berat badan saya turun 22,5 kg, saya menjadi jauh lebih percaya diri dan, tentu saja, semua ini membawa
                                                    dampak positif pada kehidupan sosial saya. Saya lebih banyak keluar rumah untuk bersenang-senang dengan teman-teman saya
                                                    dan hhmm... seorang pria mulai mengajak saya berkencan......
                                                </p>
                                                <p>
                                                    <img alt="" class="aligncenter" src="{{ asset('build/images/frontend/photo_5.jpg') }}">
                                                </p>
                                                <p>
                                                    Izinkan saya memberitahu Anda semua lebih lanjut tentang <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a>.
                                                    Produk ini mengklaim mampu membantu menurunkan berat badan, menetralkan racun dari dalam tubuh dan mengurangi stres.
                                                    Produk ini terbuat dari bahan-bahan alami yang akan mendorong atau meningkatkan efektivitas bahan-bahan lainnya yang juga terkandung
                                                    dalam produk ini. <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a>
                                                    mengandung 4 bahan alami: konsentrat alami <a href="{{ $voluum }}"><strong>garcinia cambogia</strong></a>, anggur merah, Fucus, vitamin B6 dan B1. Untuk mendapatkan hasil
                                                    terbaik cukup gunakan produk ini 3 kali sehari yaitu sebelum sarapan, makan siang dan makan malam.
                                                </p>
                                                <a href="{{ $voluum }}">
                                                    <img alt="" src="{{ asset('build/images/frontend/prod.png') }}" style="float: left; margin: 0px 25px 5px 0px;" width="220">
                                                </a>
                                                <p>
                                                    Saya benar-benar bangga dengan hasil yang saya dapatkan: saya sudah
                                                    mengunggah beberapa foto sebagai bukti bahwa produk ini benar-benar
                                                    memberikan hasil yang nyata. Saya sangat berterima kasih kepada produk
                                                    ini karena produk ini mampu membantu orang-orang menyelesaikan masalah
                                                    mereka dan membuat hidup mereka menjadi lebih berarti.
                                                </p>
                                                <p>
                                                    Pesan saya , anda sebaiknya berhati-hati! Ada banyak produk sejenis yang palsu yang
                                                    dijual di situs-situs lain... Di Indonesia hanya ada satu pemasok resmi.
                                                    Ini <strong><a href="{{ $voluum }}">
                                                            tautan situsnya</a></strong>!
                                                </p>
                                                <p>
                                                    Terima kasih sudah membaca postingan ini, teman-teman!
                                                </p>
                                                <p>
                                                    Sampai jumpa lagi!
                                                </p>
                                                <div align="center">
                                                    <a class="ord_button" href="{{ $voluum }}">
                                                        PESAN Garcinia Cambogia</a>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                            <footer>
                                                <div class="td-social-sharing">
                                                    <div class="td-tags-and-social-wrapper-box td-tags-and-social-wrapper-box-bottom-line">
                                                        <a class="td-social-sharing-buttons td-social-twitter" href="{{ $voluum }}">
                                                            <div class="td-sp td-sp-share-twitter"></div>
                                                            <div class="td-social-but-text">
                                                                Twitter
                                                            </div>
                                                        </a>
                                                        <a class="td-social-sharing-buttons td-social-facebook" href="{{ $voluum }}">
                                                            <div class="td-sp td-sp-share-facebook"></div>
                                                            <div class="td-social-but-text">
                                                                Facebook
                                                            </div>
                                                        </a>
                                                        <a class="td-social-sharing-buttons td-social-google" href="{{ $voluum }}">
                                                            <div class="td-sp td-sp-share-google"></div>
                                                            <div class="td-social-but-text">
                                                                Google +
                                                            </div>
                                                        </a>
                                                        <a class="td-social-sharing-buttons td-social-pinterest" href="{{ $voluum }}">
                                                            <div class="td-sp td-sp-share-pinterest"></div>
                                                            <div class="td-social-but-text">
                                                                Pinterest
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </footer>
                                        </article>
                                        <div class="comments" id="comments">
                                            <div class="comments-title-wrap">
                                                <h4 class="block-title">
                                                    <span>   KOMENTAR: 17 of 152   </span>
                                                </h4>
                                            </div>
                                            <ol class="comment-list">
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_1.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Klara </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">11.01.2017 - 12:09</span></time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Singkatnya, PRODUK INI BENAR-BENAR BEKERJA. Sangat bagus!
                                                                Produk ini sangat membantu untuk mengontrol nafsu makan saya.
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_2.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Ria </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">12.01.2017 - 01:23</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Saya mulai mencoba obat ini setelah membaca sekian banyak
                                                                penelitian dan mempertimbangkan pendapat keluarga saya. Ibu
                                                                saya sudah menggunakan obat ini selama 2 bulan dan berhasil
                                                                menurunkan ukuran bajunya hingga 2 ukuran. Sepupu saya juga
                                                                menggunakan obat ini sedikit lebih lama dari ibu saya dan
                                                                berat badannya turun hingga 9 kg.
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_3.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Gita </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">12.01.2017 - 17:09</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Kisah Anda sangat memotivasi! Saya sudah memesan paket
                                                                pertama saya. Saya tidak sabar untuk segera mengonsumsi
                                                                dan melihat hasilnya!
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_4.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Maria </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">13.01.2017 - 11:33</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Akhir-akhir ini saya mendengar bahwa <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> telah membantu banyak orang
                                                                untuk menurunkan berat badan. Saya juga
                                                                akan segera memesannya setelah menerima gaji saya.
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_5.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Lia </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">14.01.2017 - 08:42</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Instruktur pribadi saya menyarankan saya untuk mengonsumsi
                                                                <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> sebelum makan untuk
                                                                mempercepat program penurunan berat badan saya. Dalam 2
                                                                minggu, saya akan memberitahu Anda semua berapa kilo yang
                                                                berhasil dilenyapkan obat ini.
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_6.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Winda </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">15.01.2017 - 08:42</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Halo! Kisah saya sangat mirip dengan kisah Anda. Di sekolah
                                                                teman-teman mengejek saya gemuk, tapi semua itu hanyalah
                                                                masa lalu. Lima tahun yang lalu, saya memutuskan untuk
                                                                memiliki tubuh yang sempurna, tidak peduli seberapa sulitnya
                                                                untuk mencapai semua itu. Saya sudah mencoba bermacam-macam
                                                                diet, olahraga dll, tapi tidak ada yang berhasil.
                                                            </p>
                                                            <p>
                                                                <img alt="" class="aligncenter" src="{{ asset('build/images/frontend/review_1.jpg') }}" width="500">
                                                            </p>
                                                            <p><a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> adalah penyelamat saya -
                                                                berat badan saya turun 13 kilo hanya dalam 13 hari! Saya
                                                                sangat suka dengan penampilan saya yang sekarang dan semua
                                                                pria mengagumi tubuh saya. Saya sangat senang! Semoga
                                                                beruntung bagi semua yang baru saja memulai perjalanan
                                                                mereka untuk menurunkan berat badan :) Menjadi langsing dan
                                                                cantik itu mudah!
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_7.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Silvia </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">16.01.2017 - 11:40</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Untuk saya, <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> adalah sesuatu yang tak
                                                                tergantikan sejak saya mendapatkan pekerjaan baru saya,
                                                                karena saya terlalu sibuk untuk olahraga di gym. Saya
                                                                menggunakan obat ini empat kali sehari sebelum makan dan
                                                                obat ini benar-benar membantu saya tetap bugar.
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_8.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Ana </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">17.01.2017 - 17:43</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Anda benar! Apa yang Anda rasakan di dalam hati Anda memang
                                                                jauh lebih penting! Yang paling penting, Anda harus
                                                                mencintai diri sendiri. Tidak ada yang lebih penting dari
                                                                itu!
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_9.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Wulan </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">19.01.2017 - 02:56</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Halo semuanya! Nama saya Wulan dan saya seorang ahli gizi
                                                                dengan pengalaman lebih dari 20 tahun. Tampaknya
                                                                <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> mengandung beberapa komponen
                                                                yang sangat aktif untuk menghancurkan lemak seperti ekstrak
                                                                <a href="{{ $voluum }}"><strong>garcinia cambogia</strong></a>, anggur merah dan fucus. Kandungan vitamin B1 dan B6 dalam
                                                                produk ini juga akan membuat Anda merasa lebih baik. Saya
                                                                sangat merekomendasikan produk ini!
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_10.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Echa </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">20.01.2017 - 15:28</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Tadi malam saya sudah memesan langsung di situs web tapi
                                                                tidak ada yang menelepon saya :( :( :(
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_11.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Garcinia Cambogia (ID) </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">22.01.2017 - 07:23</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Hai Echa! Sayangnya, call center kami sangat sibuk dan tidak
                                                                bisa menghubungi semua orang karena begitu banyaknya jumlah
                                                                pesanan yang kami terima. Kami sudah mengatasi masalah ini.
                                                                Silakan coba pesan sekali lagi hari ini dan salah satu agen kami
                                                                akan menghubungi Anda sesegera mungkin. Kami mohon maaf atas
                                                                ketidaknyamanan ini.
                                                                <br> Salam hangat, agen resmi <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a>.

                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_12.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Amelinda </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">24.01.2017 - 02:50</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Teman saya selalu makan banyak tapi tetap langsing. Ternyata
                                                                rahasianya sangat sederhana: dia juga menggunakan <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> :) :) :) :)
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_13.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Karlina </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">26.01.2017 - 01:54</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Teman-teman, produk ini bak sebuah keajaiban! Setelah
                                                                menggunakan produk ini berat badan saya BENAR-BENAR turun
                                                                sekian kilo! Saya sudah menggunakan produk ini selama 2
                                                                bulan dan hasilnya sungguh luar biasa. Lihatlah foto-foto
                                                                saya:...
                                                            </p>
                                                            <p>
                                                                <img alt="" class="aligncenter" src="{{ asset('build/images/frontend/review_2.jpg') }}" width="500">
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_14.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Rara </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">28.01.2017 - 04:43</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Saya memesan <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> melalui situs web resmi
                                                                sebagai hadiah ulang tahun untuk ibu saya. Berat badan ibu
                                                                saya turun 9 kg dalam 7 hari! Ibu saya sangat bahagia:
                                                            </p>
                                                            <p>
                                                                <img alt="" class="aligncenter" src="{{ asset('build/images/frontend/buku_review_3.jpg') }}" width="500">
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_15.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Natalia </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">30.01.2017 - 11:22</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Saya memang pernah mendengar ada banyak orang yang berhasil
                                                                menurunkan berat badan dengan bantuan <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a>. Tapi apa tidak berbahaya jika
                                                                berat badan kita turun sebanyak itu dalam waktu yang relatif
                                                                sangat cepat?
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_11.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Garcinia Cambogia (ID) </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">01.02.2017 - 22:00</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Selamat pagi! <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> benar-benar aman untuk kesehatan
                                                                Anda karena kandungan bahan-bahannya yang sangat alami.
                                                                Produk ini telah lulus uji klinis dan memiliki sertifikat
                                                                mutu. Selain itu, <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a> telah diuji oleh 100 relawan
                                                                yang berhasil menurunkan berat badan dalam jangka waktu yang
                                                                relatif cepat dan mereka tetap merasa bugar selama
                                                                percobaan. Jangan menggunakan produk sejenis yang palsu yang
                                                                bisa membahayakan tubuh Anda. Obat yang asli hanya bisa
                                                                dipesan <a href="{{ $voluum }}"><strong>Garcinia Cambogia</strong></a>. Salam hangat, agen resmi <strong><a href="{{ $voluum }}">Garcinia Cambogia di Indonesia</a></strong>.</p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                                <li class="comment" id="li-comment-568118">
                                                    <div class="avatar">
                                                        <img alt="" src="{{ asset('build/images/frontend/buku_comm_15.jpg') }}">
                                                    </div>
                                                    <article>
                                                        <footer>
                                                            <cite> Natalia </cite>
                                                            <a class="comment-link" href="{{ $voluum }}">
                                                                <time pubdate="1437292549"><span class="rdate">04.02.2017 - 12:42</span>
                                                                </time>
                                                            </a>
                                                        </footer>
                                                        <div class="comment-content">
                                                            <p>
                                                                Terima kasih atas jawaban Anda!
                                                            </p>
                                                        </div>
                                                        <div class="comment-meta" id="comment-568118"></div>
                                                    </article>
                                                </li>
                                            </ol>
                                            <div class="comment-pagination">
                                                <ul class="more-comments">
                                                    <li><a class="active" href="{{ $voluum }}">1</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ $voluum }}">2</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ $voluum }}">3</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ $voluum }}">4</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ $voluum }}">5</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ $voluum }}">...</a>
                                                    </li>
                                                    <li>
                                                        <a href="{{ $voluum }}">
                                                            &gt;|</a></li>
                                                </ul>
                                            </div>
                                            <div align="center">
                                                <a class="ord_button" href="{{ $voluum }}">
                                                    PESAN Garcinia Cambogia</a>
                                            </div>
                                            <div class="comment-respond" id="respond">
                                                <h3 class="comment-reply-title" id="reply-title">
                                                    Tinggalkan komentar:
                                                    <small><a href="{{ $voluum }}" id="cancel-comment-reply-link" rel="nofollow" style="display: none;"> Batalkan </a></small>
                                                </h3>
                                                <form action="{{ $voluum }}" class="comment-form" id="commentform" method="post">
                                                    <div class="clearfix"></div>
                                                    <p class="comment-form-input-wrap">
                                                        <textarea aria-required="true" class="needsclick" cols="45" id="comment" placeholder="Komentar:" rows="3"></textarea>
                                                    </p>
                                                    <p class="comment-form-input-wrap">
                                                        <span class="comment-req-wrap needsclick"><input class="" id="author" placeholder="Nama:" size="30" type="text" value=""></span>
                                                    </p>
                                                    <p class="form-submit">
                                                        <input class="submit link" id="submit" type="submit" value="POSKAN KOMENTAR">
                                                    </p>
                                                    <p style="display: none;"></p>
                                                    <p style="display: none;"></p>
                                                    <input type="hidden" name="time_zone" value="1"></form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="span4 column_container" role="complementary">
                                        <aside class="widget widget_text">
                                            <div class="block-title">
                                                <span>   Tentang saya   </span>
                                            </div>
                                            <div class="textwidget">
                                                <a href="{{ $voluum }}"><img src="{{ asset('build/images/frontend/10475864_10153163694913232_4905600949797075094_n.jpg') }}" style="display: block; margin: 0 auto;">
                                                </a>
                                            </div>
                                        </aside>
                                        <aside class="widget widget_text">
                                            <div class="textwidget">
                                                <div style="padding: 10px 0px;">
                                                    <a href="{{ $voluum }}">
                                                        <img src="{{ asset('build/images/frontend/img-coffee.jpg') }}">
                                                    </a>
                                                </div>
                                            </div>
                                        </aside>
                                        <aside class="widget widget_text">
                                            <div class="block-title">
                                                <span>   Dapatkan Buku Harian Saya di kotak masuk Anda:   </span>
                                            </div>
                                            <div class="textwidget">
                                                <form action="{{ $voluum }}" method="post" style="border: 0px solid rgb(204, 204, 204); padding: 3px; text-align: center;">
                                                    <p>
                                                        Masukkan alamat email Anda:
                                                    </p>
                                                    <p>
                                                        <input style="width: 140px;" type="text" value="">
                                                    </p>
                                                    <input class="link" type="submit" value="Kirim">
                                                    <input type="hidden" name="time_zone" value="1"></form>
                                            </div>
                                        </aside>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="td-sub-footer-wrap">
                <div class="container">
                    <div class="row">
                        <div class="span12">
                            <div class="td-grid-wrap">
                                <div class="container-fluid">
                                    <div class="row-fluid">
                                        <div class="span4 td-sub-footer-copy"></div>
                                        <div class="span8 td-sub-footer-menu">
                                            <div class="menu-menu-principal-container">
                                                <ul class="" id="menu-menu-principal-1">
                                                    <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-first td-not-mega menu-item-7408">
                                                        <a href="{{ $voluum }}">
                                                            HALAMAN DEPAN </a>
                                                    </li>
                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-11253">
                                                        <a href="{{ $voluum }}">
                                                            Hadiah dan pers </a>
                                                    </li>
                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-14112">
                                                        <a href="{{ $voluum }}">
                                                            Kontak </a>
                                                    </li>
                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-28630">
                                                        <a href="{{ $voluum }}">
                                                            Laura Wijaya </a>
                                                    </li>
                                                    <li class="menu-item menu-item-type-post_type menu-item-object-page td-not-mega menu-item-30847">
                                                        <a href="{{ $voluum }}">
                                                            Wawancara </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="td-sp td-scroll-up"></div>

        <div class="ac_footer">
            <a href="{{ $voluum }}">Report</a>
        </div>
@endsection