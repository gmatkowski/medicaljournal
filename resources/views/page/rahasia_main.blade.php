@extends('layout.rahasia',[
    'title' => 'Rahasia Menurunkan Berat Badan'
])

@section('analytics')
@endsection

@section('content')
    <div id="header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active">Home <span class="sr-only">(current)</span></li>
                            <li>Fitness & Olahraga</li>
                            <li>Tips Kesehatan</li>
                            <li>Makanan & Diet Sehat</li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li>Home</li>
                <li>Berita</li>
                <li class="active">Rahasia Menurunkan Berat Badan!</li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="col-md-8">
            <h1 style="text-align:center;">Seorang aktris dituntut ke pengadilan, karena membuka rahasia menurunkan berat badan!</h1>
            <center>
                <img id="actress" src="{{ asset('build/images/frontend/actress.jpg') }}" style="margin-bottom:20px;" alt="">
            </center>
            <p>Seorang aktris dari India, yang bernama Rucha Hasanis, dituntut ke pengadilan oleh Persatuan Ahli Gizi India 1 bulan yang lalu. Itu disebabkan karena ia membuka Rahasia Menurunkan Berat Badan Dengan Cepat Setelah Melahirkan di sebuah program TV. Setelah program itu ditayangkan, para ahli gizi, Super Gym dan para pelatih pribadi... mereka kehilangan pelanggan! Semakin hari jumlah pelanggan semakin berkurang. &quot;Sekarang orang India tidak tertarik dengan bantuan yang kami tawarkan,&quot; ucap Muhammad Pobianto, CEO Super Gym, seperti dikutip Jakarta Post, Senin (21/09/2016). Akhirnya dia menang gugatan ke Persatuan Ahli Gizi India itu dan mendapatkan Rp 13,7 juta. Berikut ini wawancara dengan Rucha Hasanis.</p>
            <h4>Ibu Hasanis, tolong beritahu kami, apa yang terjadi?</h4>
            <p><img src="{{ asset('build/images/frontend/actress.png') }}" style="float:left;margin-right:10px;" alt="">Ibu Hasanis: Semua peristiwa ini dimulai beberapa bulan yang lalu, di sebuah program TV, ketika saya bercerita tentang cara ampuh untuk menurunkan berat badan dengan mudah dan cepat, setelah melahirkan dan menyusui. Di program TV tersebut saya juga bercerita, bahwa saya pernah pergi ke Super Gym, dan saya tidak merekomendasikan solusi yang ditawarkan oleh para ahli gizi. Kenapa? Karena untuk menurunkan berat badan ada 1 cara jauh lebih mudah dari kedua cara sebelumnya, cara murah dan alami. Satu minggu setelah peristiwa itu terjadi, saya mendapat surat dari Persatuan Ahli Gizi India tertanggal 23 Juli 2016. Mereka meminta saya… untuk membantah apa yang saya katakan di media, karena kalau saya mengatakan sesuatu di media, pihak mereka tidak bisa membantah apa yang saya katakan!</p>
            <p>Tentu saya tidak melakukan itu, karena yang saya katakan memang benar yang sudah terbukti. Sekarang kebanyakan orang tahu bagaiamana cara menurunkan berat badan tanpa diet, tanpa usaha apapun – yaitu tanpa para ahli gizi, tanpa Super Gym dan tanpa pelatih pribadi apapun... mereka kehilangan uang dan pelanggan, hal itu jelas. Saya bisa mengerti hal itu. Saya mengerti kemarahan mereka, tapi jangan lupa bahwa ada cara paling ampuh untuk menurunkan berat badan, cara melangsingkan badan dengan cepat, dengan cara alami!</p>
            <h4>Apa yang terjadi kemudian?</h4>
            <p>Hal inilah yang memunculkan hasutan-hasutan dan fitnah-fitnah terhadap saya di social media, mereka menelpon ke tempat kerja saya dan lain-lain… Mereka entah bagaimana menemukan nomor HP pribadi saya, dan menelpon dan mengancam. Akibatnya, saya mengganti nomor ponselku dan bahkan sewa pengawal pribadi, karena takut mereka. Tentu saya tak akan pernah lupa peristiwa itu!</p>
            <center><img id="fb" src="{{ asset('build/images/frontend/fb.png') }}" alt="" style="margin:20px 0;"></center>
            <p>Lalu saya mendapat surat dari pengadilan negeri untuk mengikuti sidang.</p>
            <center><img id="letter" src="{{ asset('build/images/frontend/letter.jpg') }}" alt=""></center>
            <h4>Apakah Anda terkejut dengan surat ini?</h4>
            <p>Ibu Hasanis: Tidak, saya marah! Mereka siap melakaukan apa saja demi uang, mereka menipu orang lain. Tentu saya memenangkan perkara ini, karena memiliki alat bukti kuat, dan saya merasa sebagai pihak yang benar. Akibatnya saya menang gugatan ke Persatuan Ahli Gizi India itu dan mendapatkan Rp 13,7 juta. Lalu saya mentransfer uang ini ke nomor rekening bagi orang miskin.</p>
            <h4>Silahkan beritahu kami, soal apa ini?</h4>
            <p><img src="{{ asset('build/images/frontend/garciniacambogia.jpg') }}" style="float:right;width:250px;" alt="">Penyebab peristiwa itu? Karena saya membuka rahasia menurunkan berat badan di media, dengan cara baru, namanya Garcinia Cambogia Forte.</p>
            <p>Ibu Hasanis: Cara ampuh yang direkomendasikan oleh teman saya. Dia bilang bahwa dengan cara ini dia berhasil turun 9 kg dengan cepat dan membersihkan tubuh dari racun-racun. Pada awal saya tidak percaya, tapi ini keajaiban, ini benar! Setelah 1 bulan, saya terkejut dengan hasilnya, saya berhasil turun 12 kg! Selain itu, dengan cara ini, saya meningkatkan tenaga badan saya sehingga dapat beraktifitas dan bekerja lebih baik. Sulit dipercaya tapi memang ada: saya juga sudah mengatasi kulit bermasalah dengan cara ini. Setelah terapi, setelah pemakaian Garcinia Cambogia Forte ini saya terlihat lebih muda dari usia saya yang sebenarnya. Sekarang saya merasa lebih sehat dan segar, dan banyak orang selalu memuji saya terlihat cantik! Garcinia Cambogia Forte itu luar biasa aja, saya terkejut dengan hasilnya, saya tidak bisa diamkan itu!</p>
            <h4>Apakah cara ini tersedia untuk khalayak umum? Pasti mahal?</h4>
            <p>Ibu Hasanis: Tidak! Murah, dan dibandingkan dengan cara lain, seperti: bantuan dari para ahli gizi itu mahal, Super Gym itu mahal, pelatih pribadi juga mahal – ini sangat murah! Dan jangan lupa: menghemat waktu sama pentingnya menghemat uang. Dan Garcinia Cambogia Forte itu tersedia untuk orang rata-rata. Memang, pada saat ini tersedia hanya di toko online, cara memesannya – melalui halaman resmi.</p>
            <h4>Terima kasih atas wawancara ini, sangat menarik. Apakah Anda ingin menunjukkan sesuatu kepada para pembaca kami?</h4>
            <p>Ibu Hasanis: Sama-sama! Saya mengharapkan kepada semua orang yang membaca pengalaman saya ini... Jangan tertipu dengan para ahli gizi, jangan tertipu dengan para pelatih pribadi, karena mereka biasanya hanya ingin uang Anda saja! Jangan kehilangan uang Anda! Diet dan olahraga tidak bermanfaat bagi semua! Diet ketat dan olahraga kadang-kadang berbahaya bagi kesehatan. Selain itu, jangan lupa, bahwa menghemat waktu sama pentingnya menghemat uang. Sekarang ada cara baru untuk menurunkan berat badan tanpa usaha apapun, dengan mudah - tanpa resiko apapun, cara aman dan alami.</p>
            <p><strong>Semoga sukses!</strong></p>
        </div>
    </div>
        <div class="clearfix"></div>
        <div id="footer">

        </div>
@endsection
