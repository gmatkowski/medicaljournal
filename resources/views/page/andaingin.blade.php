@extends('layout.perkelahian',[
    'title' => 'Health Journal'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/pre/all.css') }}">
    @parent
@endsection

@section('pixel')
    @include('pixel.cambogia_main')
@endsection

@section('content')
    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <h1><a><img src="{{ asset('build/images/frontend/perkelahian_logo.png') }}" style="width:200px;" alt=""></a></h1>
            <nav class="links">
                <ul>
                    <li><a>HOME</a></li>
                    <li><a>DIET</a></li>
                    <li><a>FIT & OLAHRAGA</a></li>
                    <li><a>PSIKOLOGI</a></li>
                    <li><a>DI RANJANG</a></li>
                    <li><a>KECANTIKAN</a></li>
                    <li><a>KESEHATAN</a></li>
                </ul>
            </nav>
        </header>

        <!-- Main -->
        <div id="main">

            <!-- Post -->
            <article class="post">
                <header>
                    <div class="title">
                        <h2>Anda ingin makanan sehat, tetapi tidak tahu mulai dari mana? Berikut ini 10 aturan penting:</h2>
                    </div>
                    <div class="meta">
                        <time class="published">{{ \Carbon\Carbon::now()->format('M d, Y') }}</time>
                        <a class="author"><span class="name">Daryono</span><img src="{{ asset('build/images/frontend/avatar.jpg') }}" alt="" /></a>
                    </div>
                </header>
                <p>Anda sering bertanya-tanya bagaimana mengubah kebiasaan makan dengan mudah, bagaimana mengatasi kebiasaan yang tidak sehat. Tidah ada satu jawaban, oleh karena itu berikut ini kita akan membahas 10 aturan penting mengenai makanan sehat dan bergizi. Jangan ragu untuk membaca!</p>

                <h1>1.Makan banyak sayur</h1><br/>

                    <span class="image featured">
                        <img src="{{ asset('build/images/frontend/anda-1.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Seperti yang Anda mungkin sudah tahu, makanan sehat adalah menutrisi tubuh dan memberikan kecukupan kebutuhan tubuh, secara teratur, dalam jumlah dan proporsi yang tepat. Sayuran mengandung banyak bahan yang baik, oleh karena itu sayur adalah prinsip dasar diet sehat. Sayur adalah sumber vitamin terbaik, serat dan fitokimia. Dengan sayur Anda akan:</p>

                <ul>
                    <li>mengurangi resiko beberapa kanker,</li>
                    <li>menguragi resiko diabetes, aterosklerosis, dan penyakit jantung,</li>
                    <li>mengurangi tekanan darah dan kadar kolesterol tinggi</li>
                </ul>

                    <p>Menurut program diet terbaru, sayur dan buah harus dimakan 9 kali sehari. Artinya, makanan kita harus terdiri dari sayur-sayuran. Di website saya tersedia banyak aturan makan sehat - Anda bisa menemukan ratusan resep makanan yang lezat dan bergizi, dan teridi dari sayur-sayuran.</p>

                <h1>2.Makan secara teratur, 5 kali sehari yang terbaik</h1><br/>

                    <span class="image featured">
                        <img src="{{ asset('build/images/frontend/anda-2.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Cobalah untuk tetap memulai hari Anda dengan sarapan yang baik dan sehat. Dengan demikian, Anda dapat meningkatkan stamina tubuh Anda dan mengurangi jumlah kalori yang Anda konsumsi setiap harinya. Makan secara teratur setiap 3-4 jam, untuk menstabilkan kadar gula darah dan mempercepat metabolisme Anda. Dengan cara makan sehat ini, Anda dapat meningkatkan kadar energi tubuh Anda, mencegah rasa lapar dan mengatasi rasa lelah.</p>

                <h1>3.Memperkaya diet Anda dengan bahan yang bervariasi</h1><br/>

                    <span class="image featured">
                            <img src="{{ asset('build/images/frontend/anda-3.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Berilah nutrisi yang baik pada tubuhAnda, berilah nutrisi yang seimbang dari semua grup makanan. Ada enam grup makanan utama yang dapat dikonsumsi untuk mencapai nutrisi seimbang: produk biji-bijian (roti, sereal, pasta), susu dan produk susu (kefir, keju, yoghurt, buttermilk), buah-buahan dan sayuran. Grup yang lain merupakan daging, ikan dan telur. Ada juga grup lemak nabati, yaitu minyak, dan lemak hewani, yaitu mentega dan lemak babi. Grup yang lain adalah gula dan produk manis yang diproses secara industrial, harus menghindari atau mengurangi makanan produk tersebut.</p>

                <h1>4.Hindari produk dengan bahan-bahan buatan</h1><br/>

                    <span class="image featured">
                            <img src="{{ asset('build/images/frontend/anda-4.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Hindari fast food dan makanan instan yang tersedia di toko. Produk-produk yang diproses secara industrial ini tidak baik - karena erdiri dari bahan kimia, zat pewarna, pemanis, pengawet, dan penyedap rasa (misalnya, aspartam, sirup glukosa, fruktosa atau monosodium glutamate - MSG). Selain itu, bahan-bahan tersebut menggunakan jumlah lemak dan garam yang berlebihan,
                        yang tidak baik untuk kesehatan. Makanan yang diproses secara industrial hampir tidak memiliki vitamin dan mineral.</p>

                <h1>5.Minum banyak air</h1><br/>

                    <span class="image featured">
                            <img src="{{ asset('build/images/frontend/anda-5.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Ada banyak manfaat air untuk tubuh kita, air bertanggung jawab dalam banyak proses, bertanggung jawab untuk mengangkut darah dan nutrisi ke seluruh tubuh. Secara umum orang dewasa membutuhkan 1,5 - 2 liter air putih perhari - totalnya 3 - 3,5 liter perhari termasuk air yang ada di makanan. Kenapa? Setiap hari kita kehilangan rata-rata 2,6 liter air, karena proses metabolisme tubuh kita, yaitu proses respirasi, ekskresi dan berkeringat. Kita kehilangan lebih banyak air pada hari panas juga atau setelah latihan yang intens. Agar tubuh kita berfungsi dengan baik, penting untuk mengisi tubuh dengan air minum secara teratur.</p>

                <h1>6.Hindari produk manis yang diproses secara industrial</h1><br/>

                    <span class="image featured">
                            <img src="{{ asset('build/images/frontend/anda-6.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Produk manis yang diproses secara industrial adalah makanan yang mengandung kalori tinggi (gula, tepung putih, lemak terhidrogenasi), oleh karena itu produkmani tersebut mengisi tubuh dengan energi kelebihan yang menjadi lemak tubuh.
                        Produk manis yang diproses secara industrial hampir tidak bergizi dan bernutrisi. Terlaly banyak gula adalah penyebab obesitas atau kegemukan, masalah gigi, dan masalah gula darah, yaitu diabetes. Oleh karena itu, harus mengurangi jumlah produk manis yang diproses secaraindustrial yand dimakan, atau memilih produk manis alami, seperti stewia.</p>

                <h1>7.Kurangi garam saat memasak</h1><br/>

                    <span class="image featured">
                            <img src="{{ asset('build/images/frontend/anda-7.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Menurut aturan WHO (World Health Organization), orang dewasa membutuhkan 5 g (yaitu 1 sendok teh). Ternyata bahwa rata-rata orand Inodnesia makan 15 g sehari! Hal ini disebabkan oleh kelebihan garam dalam makanan yang diproses secara industrial atau sebagai bahan alami. Namun, Anda dapat mengurangi garam saat memasak. Jadi, coba kurangi garam dan gunakan rempah-rempah lainnya. Hindari produk-produk yang diproses secara industrial dan produk kaleng.</p>

                <h1>8.Masak lebih baik dari panggang</h1><br/>

                <span class="image featured">
                            <img src="{{ asset('build/images/frontend/anda-8.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Kita semua suka panggang - akhirnya, inilah cara tercepat untuk mempersiapkan makanana (terisitimewa daging).</p>

                    <p>Namun, cara cepat bukan cara sehat:</p>

                    <ul>
                        <li>Satu sendok minyak merupakan 90 kalori yang tidak diperlukan oleh tubuh Anda. Berapa sendok tersebut Anda menggunakan saat memasak?</li>
                        <li>Proses panggang dapat bersifat sangat karsinogenik atau menimbulkan kanker. Semakin hitam kulit, semakin banyak zat-zat karsinogenik tersebut.</li>
                        <li>Kebanyakan orang tidak bisa panggang dengan baik dan benar, dan minyak apa yang baik dan bagaimana cara menggunakannya.    </li>
                    </ul>

                <h1>9.Kurangi makanan daging</h1><br/>

                    <span class="image featured">
                            <img src="{{ asset('build/images/frontend/anda-9.jpg') }}" alt="" style="width:100%" />
                    </span>

                    <p>Manfaat dari makanan banyak daging sangat kecil atau tidak ada sama sekali, dibandingkan dengan kerusakan disebabkan oleh makanan banyak daging. Yang pertama, makanan banyak daging meningkatkan resiko aterosklerosis dan mengembangkan gangguan lipid, resiko kanker, penyakit jantung atau keasaman tubuh. Di Indonesia kita makan banyak daging yang diproses secara industrial atau produk instan berkualitas rendah, tak cocok untuk kesehatan kita (atau berbahaya). Tentu saja, Anda tidak perlu menjadi vegetarianin. Jika Anda menyukai daging dan tidak bisa hidup tanpa daging, coba mengurangi jumlah daging yang dimakan setiap hari dan mencoba daging berkualitas tinggi.</p>

                {{------}}
                <div class="container">
                    <div id="orderFormBorder" class="col-md-7">
                        <p>Apa Anda ingin mendapatkan informasi lebih lanjut tentang cara untuk menurunkan berat badan? Informasikan alamat e-mail Anda untuk berlangganan newsletter kami!</p>
                        {!! form_start($form, ['id' => 'newsletterForm', 'class' => 'form-horizontal']) !!}

                        {!! form_errors($form->email) !!}

                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label"><span style="color:red">*</span>Alamat Email</label>
                            <div class="col-md-8">
                                {!! form_widget($form->email) !!}
                                <div>
                                    {!! form_widget($form->terms) !!}
                                    <span id="helpBlock" class="help-block">Untuk tujuan marketing , dengan ini saya setuju personal data saya diproses</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-3">
                                <button type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" id="orderSubmit" class="btn btn-primary">Berlangganan</button>
                            </div>
                        </div>
                        {!! form_end($form, false) !!}
                    </div>
                </div>
                {{------}}
                <div class="content">
                    <footer id="footer">
                        <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
                    </footer>
                </div>
            </article>

        </div>

        <!-- Footer -->
        <section id="footer">
            <p class="copyright">&copy; Medical Jurnal</p>
        </section>

    </div>
@endsection
