@extends('layout.app',[
    'title' => 'Cara cepat melangsingkan tubuh secara alami'
])

@section('chat')@endsection

@section('analytics')
@endsection

@section('content')

	<div class="page-container">
		<header id="main-header">
			<div class="data">
				<h1 class="logo">
					<a href="#"><img src="{{asset('build/images/frontend/fi6WL4S/logo.png')}}" alt="Medic Reporters"></a>
				</h1>
				<div class="button facebook-box">
					<div class="ribbon-3">
						<a href="#">Suka</a>
					</div>
				</div>
				<form method="POST" accept-charset="utf-8">
					<input required="required" type="text" placeholder="Cari..." name="q">
					<button type="submit" name="search_submit"><i class="icons icon-search"></i></button>
				</form>
			</div>
			<button id="toggle-menu">
				<span></span> <span></span> <span></span>
			</button>
			<nav>
				<a href="#">HOME</a>
				<a href="#">DIET</a>
				<a href="#">FIT & OLAHRAGA</a>
				<a href="#">PSIKOLOGI</a>
				<a href="#">DI RANJANG</a>
				<a href="#">KECANTIKAN</a>
				<a href="#">KESEHATAN</a>
			</nav>
		</header>
		
		<ol class="breadcrumb">
			<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="#" itemprop="url">
					<span itemprop="title">Home</span> </a>
			</li>
			<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="#" itemprop="url">
					<span itemprop="title">Kesehatan</span> </a>
			</li>
			<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<span itemprop="title">Penemuan yang menonaktifkan gen obesitas!</span>
			</li>
		</ol>
		<div class="page-view">
			<div class="left-side">
				<article class="main">
					<header>
						<h1>Penemuan yang menonaktifkan gen obesitas! Mengapa metode memecah sel-sel lemak didiamkan hampir selama 200 tahun?</h1>
						<p>Dengan cara ini bisa mengurangi berat badan secara permanen dalam 2 minggu. Mengapa penemuan tahun 1819, penemuan yang begitu inovatif, baru terjadi hari ini? Semua ini sudah jelas: paten seorang ahli kimia Hans Christian Ørste, sangat menarik bagi dokter ahli gizi, penemuan ini luar biasa saja.</p>
					</header>
					<div class="body">
						<h2>Mengapa cara Ørsted berhasil untuk melangsingkan tubuh?</h2>
						<figure>
							<div id="leftphoto">
								<img src="{{asset('build/images/frontend/christian.jpg')}}" alt="Hans Christian Ørsted">
								<figcaption>Hans Christian Ørsted</figcaption>
							</div>
							<div id="rightdesc">
								<p>Hans Christian Ørsted adalah seorang penemu {{$product1->name}}, zat untuk pelangsing tubuh, yang sangat efektif. Dia adalah seorang ahli kimia terkenal dari Denmark. Dia bekerja sebagai apoteker yang berupaya menemukan solusi inovatif. Penemuan {{$product1->name}} ditemukan pertama kali oleh dia. {{$product1->name}} adalah zat yang
									membakar kalori dengan cepat dan memecah sel-sel lemak secara permanen. Ørsted lama mempelajari efek zat {{$product1->name}} dari buah Malabar Tamarind, yang sekarang terkenal sebagai cara efektif untuk
									<b>memerangi obesitas</b>, di seluruh dunia. {{$product1->name}} adalah zat yang didiamkan selama 200 tahun, zat ini dapat membantu mengurangi berat badan secara cepat – dengan membakar lemak di tubuh, terutama di bagian paha dan perut, terlepas dari berat badan Anda sebelumnya.
								</p>
							</div>
							<p>Dengan cara yang lain, yaitu cara untuk menurunkan berat badan yang tradisional, Anda harus diet ketat, inilah metode yang membutuhkan waktu yang lama, dan Anda sering mengalami sindrom yoyo, yang berbahaya bagi kesehatan Anda. Dengan cara Ørsted, Anda akan berhasil menurunkan berat badan, karena cara ini memecah sel-sel lemak, pada tingkat seluler, jadi Anda tidak perlu motivasi, dan tidak perlu memiliki kemauan yang kuat.</p>
						</figure>
						<h2>Pelangsing badan secara cepat 24 jam sehari</h2>
						<p>Anda perlu menyadari, bahwa jika pencernaan menjadi lebih efektif (pada usus halus, pankreas dan perut), pengurangan lemak tubuh berlangsung
							<b>secara otomatis dan alami</b>. Anda tidak perlu menderita dengan melakukan program diet ketat, Anda tidak perlu berhenti makan makanan favorit Anda. {{$product1->name}} mengganggu fungsi gen yang membentuk lemak dan mencegah pertumbuhan sel-sel lemak baru, oleh karena itu Anda bisa
							menurunkan berat badan secara cepat, tanpa perlu melakukan perubahan kebiasaan makan secara radikal. Apakah para ahli gizi sengaja menutup mata pada penemuan ini karena takut bahwa pasar akan didominasi oleh metode {{$product1->name}} yang paling efektif?
						</p>
					</div>
				</article>
				
				<aside class="hidden-xs">
					<a href="http://www.accuweather.com/id/id/jakarta/208971/weather-forecast/208971" class="aw-widget-legal">
						<!--
						By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
						-->
					</a>
					<div id="awcc1472489642912" class="aw-widget-current" data-locationkey="208971" data-unit="c" data-language="id" data-useip="false" data-uid="awcc1472489642912"></div>
					<script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
				</aside>
			</div>
		</div>
	</div>
	
	<footer id="main-footer">
		<div class="page-container">
			<a href="#"><img class="footer-logo" src="{{asset('build/images/frontend/fi6WL4S/logo.png')}}" alt="Medic Reporters"></a>
			<nav>
				<a href="#">Home</a>
				<a href="#">Diet</a>
				<a href="#">Fit & Olahraga</a>
				<a href="#">Psikologi</a>
				<a href="#">Di ranjang</a>
				<a href="#">Kecantikan</a>
				<a href="#">Kesehatan</a>
			</nav>
		</div>
		<div class="page-container copy">
			<hr>
			HAK CIPTA 2016 SEMUA HAK CIPTA TERPELIHARA
		</div>
	</footer>
	
	<div id="popup" style="display: none;">
		<div class="window">
			<h1>Terima kasih!</h1>
			<p>Komentar Anda telah dikirim dan sedang menunggu moderasi.</p>
			<button class="close" onclick="$('#popup').hide();">TUTUP</button>
		</div>
	</div>
	
	<script src="{{ elixir('js/frontend/all.js') }}"></script>
	
	@include('part.preorderPopup', ['productSymbol' => 'cambogia'])

@endsection