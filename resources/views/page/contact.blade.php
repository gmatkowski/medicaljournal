@extends('layout.app')
@section('title') :: {{ trans('menu.contact') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')
	<main>
		<section class="contact">
			<div class="container">
				<h1 class="wow fadeIn">{!! $page->title !!}</h1>

				<div class="row">
					<div class="wow slideInLeft col-md-6">
						{!! form_start($form,['class' => 'contact-form']) !!}
						{!! form_widget($form->name) !!}
						{!! form_widget($form->email) !!}
						{!! form_widget($form->phone) !!}
						{!! form_widget($form->message) !!}
						{!! form_widget($form->submit) !!}
						{!! form_end($form) !!}
					</div>
					<div class="wow slideInRight col-md-6">
						<p>
							{!! $page->content !!}
						</p>
					</div>
				</div>
			</div>
		</section>
	</main>

@endsection