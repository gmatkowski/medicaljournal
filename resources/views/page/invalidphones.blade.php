@extends('layout.admin_singin')
@section('content')

<!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>All Leads with Invalid Phones</h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                        <tr class="headings">
                            <th class="column-title">Id :</th>
                            <th class="column-title">Name :</th>
                            <th class="column-title">Email :</th>
                            <th class="column-title">Phone :</th>
                            <th class="column-title">Product :</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($contacts as $contact)
                            <tr class="even pointer">
                                <td class=" ">{{ $contact->id }}</td>
                                <td class=" ">{{ $contact->name }}</td>
                                <td class=" ">{{ $contact->email }}</td>
                                <td class="  last">{{ $contact->phone }}</td>
                                <td class="  last">{{ $contact->product }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <div class="text-right">
                    @if ($contacts->count() > 0)
                        {!! $contacts->render() !!}
                    @endif
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection