<!DOCTYPE html>
<html lang="th">

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="initial-scale=1">

    <title>Amerika memiliki rekor dalam penurunan berat badan</title>

    <link rel="stylesheet" href="{{ elixir('css/amerika/all.css') }}">

    <style>.e451f6a00d29 { width:1px !important; height:1px !important; border:0 !important; background:none !important; border-style:none !important; position:absolute; }</style>
    <style>.d38f0e547e2d { width:2px !important; height:2px !important; border:0 !important; background:none !important; border-style:none !important; position:absolute; }</style>
</head>

<body>

<center>
    <div class="tx-container">
        <div class="header">
            <div class="tx-social">
                <a href="{{ $voluum }}" target="_blank">
                    <img src="{{ asset('build/images/social.png') }}">
                </a>
            </div>
            <div class="logo">
                Koran pagi		</div>
            <div class="nav">
                <ul style="background:#000;">
                    <li><a href="{{ $voluum }}" target="_blank">Koran Pagi Berita</a></li>
                    <li><a href="{{ $voluum }}" target="_blank">Kesehatan</a></li>
                    <li><a href="{{ $voluum }}" target="_blank">Kecantikan</a></li>
                    <li><a href="{{ $voluum }}" target="_blank">Luar Biasa</a></li>
                    <li><a href="{{ $voluum }}" target="_blank">Gambar</a></li>
                    <li><a href="{{ $voluum }}" target="_blank">E-shop</a></li>
                    <li class="fix"></li>
                </ul>
            </div>
            <div class="category">
                <a href="{{ $voluum }}" target="_blank">
                    &gt;&gt; Berita Terbaru			</a>
            </div>
        </div>

        <h1 class="headline">
            Amerika memiliki rekor dalam penurunan berat badan. Namun sekarang Indonesia telah memecahkan rekor tersebut!
        </h1>
        <div class="table main">
            <div class="content cell top">
                <div class="tx-content">
                    <div class="entry-meta">{{ \Carbon\Carbon::now()->subDays(7)->format('M d, Y') }}</div>
                    <div class="section">
                        <p><strong>Surat kabar asing melaporkan adanya penurunan berat badan yang signifikan pada pasien yang mengalami obesitas. Berkat penemuan penurunan berat badan oleh ahli gizi dari Minnesota. Bahkan untuk pria atau wanita yang tidak tahan diet dan olahraga. Metode ini bisa membantu menurunkan berat badan 10-14 kg dalam satu bulan.</strong></p>
                        <p>Namun, hasil terbaik dalam penurunan berat badan yang tercatat di Indonesia diraih oleh Shinta, 28, di Indonesia tidak ada orang lain yang bisa menurukan berat sebanyak dia. Dia berhasil menurunkan 24 kg dalam waktu kurang dari 2 bulan. Tanpa harus mengurangi makan atau berolahraga! Walaupun dia menurunkan berat dengan cepat, dia tidak mengalami masalah apapun. Dia tetap bugar dan berat yang sudah turun tetap, tidak naik lagi meskipun sudah lebih dari 5 bulan. Dia sangat puas dengan hasilnya.</p>
                        <center>
                            <p><img src="{{ asset('build/images/frontend/girl1.jpg') }}" id="ba-image"></p>
                            <div class="ba-desc">
                                <p style="margin-bottom: 16px; text-align: center;font-weight:bold;">
                                    Shinta, 28 Tahun :				</p>
                                <p style="font-style:italic;text-align:left;font-weight:bold;">
                                    Saya merasa bahwa tubuh saya seperti mesin energi metabolisme. Lemak di paha tiba-tiba meluruh, selulit memudar dan perut mengecil dari hari ke hari.
                                </p>
                            </div>
                        </center>
                        <br class="clearfix">
                        <p>Bagaimana Anda bisa menurunkan berat hingga 24 kg dalam waktu singkat?</p>
                        <p>Metode apakah yang banyak digunakan di Amerika?</p>
                        <p>Komentar Shinta :</p>

                        <p>Gaya hidup saya tidak pernah memungkinkan untuk bisa menurunkan berat badan. Saya menghabiskan sebagian besar waktu saya di kantor. Kadang saya makan camilan di malam hari untuk menambah tenaga, dan saya tidak pernah punya inisiatif untuk pergi ke pusat kebugaran atau untuk merawat diri. Saya bukanlah wanita dengan penampilan yang menarik. Walaupun tunangan saya tidak pernah mengeluh apa-apa, namun saya sadar jika tidak lagi memperhatikan dia seperti dulu. Dia kadang melihat wanita lain dengan badan yang lebih langsing dan menarik. Saya merasa sangat gemuk dan semakin gemuk saja dan saya benar-benar kehilangan kepercayaandiri. Kadang saya mendengar tunangan saya berkata pada temannya, dan dia selalu bilang “Shinta harus menurunkan beratnya paling tidak 20 kg, sekarang dia membuat berat saya naik juga”. Sangat menyebalkan mendengarkan ini, tapi ini tidak salah karena saya memang tidak berusaha merawat diri sama sekali, terutama selama 4 tahun ini. Sudah beberapa kali saya mencoba berbagai cara agar bisa berubah, namun ini terlalu aneh untuk saya lakukan. Harus memilih apa yang bisa dimakan apa yang tidak, menghitung asupan kalori atau pergi ke gym. Saya memakai baju-baju besar dan malas berbelanja pakaian bagus. Saya sudah mencoba beragam cara bahkan saya sempat mengikuti uji coba program penurunan berat, tapi saya sudah tahu hasilnya akhirnya akan seperti apa berdasarkan pengalama yang sudah-sudah. Perubahan terbesar, terjadi pada 13 Januari lalu.
                            Hari itu merupakan hari yang sangat panas, saya masih ingat atmosfer dan di sekitar saya. Suasana saat itu sangat tenang. Tidak ada tanda-tanda bahwa akan ada perubahan besar dari sebotol  ramuan ajaib yang baru saya terima.
                            Saya menerima paket ini dari salah seorang teman saya yang saat ini tinggal di Amerika. Ada kertas kecil berwarna merah muda yang terlampir bersama paket ini. Di situ tertulis “ Apa ini Shinta? Dia masih imut dan manis seperti dulu, tapi dia harus benar-benar mulai mengurus dirinya, semua belum terlambat.”</p>

                        <p><strong>“Keajaiban Penurun Berat”</strong></p>
                        <p>{{ $product1->name }} merupakan kapsul yang dikembangkan sesuai dengan kebutuhan untuk mengecilkan perut serta menurunkan berat dengan bahan-bahan kompleks yang akan membantumu menyingkirkan lemak di perut. Ini alasan saya bisa sukses tanpa diet. Tadinya saya meraguukannya. Saya segera mencari tahu tentang cara mengecilkan tubuh dan produk ini, dan saya membaca ribuan komentar mengenai produk ini di website amerika. Mereka menulis, ini bisa mestimulasi metabolisme tubuh secara efektif, dan yang paling penting tidak membuat ketergantungan. Di situ dikatakan bahwa produk ini bisa didapat dimana saja di US, dan saya bisa mendapatkan 2 produk gratis yang cukup untuk pemakaian selama 2 bulan. Saya mimum 2 tablet sehari sebelum sarapan serta minum lebih banyak air dari biasanya dan saya tidak perlu lagi memikirkan tentang diet dan olahraga sama sekali. Dari awal pemakaian saja, sudah terasa adanya penurunan berat.</p>
                        <p>
                            <strong>
                                “saya merasa tubuh saya seperti menguap, baju-baju saya jadi semakin longgar”.
                            </strong>
                        </p>
                        <p>Saya benar-benar puas melihat tubuh saya bisa membakar lemak seperti ini – perut saya makin mengecil setiap harinya. Otot-otot saya pun terlihat lebih berbentuk. Setelah 8 minggu, berat saya turun menjadi  55 kg. Saya berhasil menurunkan berat sebanyak 24 kg. Dan sekarang, setelah berusaha selama bertahun-tahun, saya terlihat seperti banyak wanita yang memakai baju bagus dan merasa cantik, akhirnya tunangan saya bangga pada saya. Selain itu, saya jadi lebih suka pergi keluar. Saya ingin seluruh dunia melihat bagaimana tubuh saya berubah menjadi lebih baik. Ini menjadi energi positif dari dalam diri yang menjadikan hidup saya 10 kali lebih baik. </p>
                        <div class="notice">Shinta, 28 tahun, Berat badan turun 24 kg dengan menggunakan {{ $product1->name }} 2 kapsul sehari dalam 8 minggu.</div>
                        <img src="{{ asset('build/images/frontend/specialist_cqplus_600_s.jpg') }}" class="doc">
                        <p>Kita akan bertanya pada seorang ahli Gizi bernana Robert Anderson, yang telah menggunakan produk ini untuk mengatasi obesitas pada pasiennya. Inilah rahasia di balik kesuksesan produk ini.</p>
                        <p>Khasiat dari Garcinia Cambogia Forte berasal dari dua bahan utama. Yang pertama berasal dari ekstrak Garcinia,  sumber dari Hydrocarbolic Acid (HCA), yang baik untuk metabolisme dan dapat membantu menghalangi terjadinya penumpukan lemak di tubuh. Bahan kedua adalah ekstrak dari rasberi kering. Dapat menstimulasi suhu tubuh untuk membakar jaringan lemak berlebih dan membantu detox racun dala tubuh. Saat ini, kebanyakan orang tidak mengonsumsi dua bahan ini. Jadi hal apa yang penting saat akan menurunkan berat badan? Orang-orang yang ingin menurunkan berat badannya biasanya akan mengonsumsi makanan rendah kalori, ini memang baik untuk kesehatan, tapi tidak begitu membantu untuk menurunkan berat. Itu karena makanan-makanan ini kadang tidak mengandung dua bahan di atas. </p>

                        <br class="clearfix">
                        <p><strong>Kedua bahan ini membantu menyingkirkan lemak-lemak berlebih di tubuh.</strong></p>
                        <center>
                            <div class="fatburn-wrapper">
                                <div id="fatburn_img">
                                    <table border="0" cellspacing="0" cellpadding="0">
                                        <tbody><tr>
                                            <td class="padright">
                                                Penurunan berat badan menggunakan cara biasa
                                            </td>
                                            <td class="padleft">
                                                Penurunan berat badan menggunakan {{ $product1->name }}
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="middle">
                                                <span style="background:#FFF">
                                                    jaringan lemak (bagian yang susah dihilangkan)
                                                </span>
                                            </td>
                                        </tr>
                                        <tr class="absbot">
                                            <td class="padright top">
                                                Proses pebakaran jaringan lemak lebih lama
                                            </td>
                                            <td class="padleft">
                                                <span class="blue">
									                {{ $product1->name }}
                                                </span>
                                                bekerja dari dalam mempercepat proses pembakaran lemak
                                            </td>
                                        </tr>
                                        </tbody></table>
                                </div>
                            </div>
                        </center>
                        <p>
                            Ada tiga jenis lemak di dalam tubuh. yang termasuk jenis paling susah dihilangkan misalnya lemak di perut, pinggul dan untuk menghilangkannya membutuhkan tambahan ekstrak garcinia dan rasberi kering.  Sekarang tidak banyak makanan yang mengandung dua bahan ini, jadi tubuh kekurangan asupan bahan-bahan ini. Satu-satunya cara untuk membantu Anda menyingkirkan lemak-lemak ini adalah dengan meningkatkan asupan ekstrak garcinia dan rasberi kering ini menggunakan suplemen. Garcina Cambogia Forte mengandung ekstrak Garcinia serta ekstrak rasberi kering dengan kualitas tinggi. Sehingga akan membantu Anda menurunkan berat badan secara mengejutkan.
                        </p>
                        <p><strong>Cara ini 3 kali lebih efektif dari cara lainnya.</strong></p>
                        <a href="{{ $voluum }}" target="_blank" class="rek">
                            <img src="{{ asset('build/images/frontend/cqplus_600_rek.jpg') }}">
                        </a>
                        <p>
                            Ternyata, jika Anda melakukan pola makan yang seimbang ditambah dengan {{ $product1->name }}, akan memberikan hasil 3 kali lebih efektif dari metode lainnya. Artinya, dalam waktu hanya satu minggu saja, Anda bisa menurunkan berat hingga 3 kg, padahal biasanya hanya bisa turun palin banyak 1 kg saja.
                            <br><br>
                            <b>Produk ini terbagi dalam 3 Langkah :</b>
                        </p>
                        <div>

                            <ol>
                                <li>
                                    Langkah pertama adalah, membakar lemak dengan cepat.</li>
                                <li>
                                    Lalu produk ini akan membersihkan seluruh tubuh dari lemak yang menumpuk, membantu Anda membakar semua lemak berlebih di seluruh tubuh.
                                </li>
                                <li>
                                    Langkah terakhir adalah menstimulasi sistem pembakaran lemak untuk mencegah agar timbunan lemak tidak kembali.
                                </li>
                            </ol>


                        </div>

                        <br class="clearfix">
                        <p style="margin-top: 1em;">Di Indonesia Anda bisa membeli {{ $product1->name }} (30 tablet) setelah bergabung dengan member diskon. Produk suplemen penurun berat ini menerima komentar positif dari Nutrition Research Center Pusat Penelitian Gizi.</p>


                    </div>
                </div>
            </div>

            <div class="tx-sidebar cell top">
                <ul>
                    <li>
                        <h2>» Peringatan dari ahli!</h2>
                        <img src="{{ asset('build/images/frontend/doctors.jpg') }}" class="docs">
                        <p>
                            Jauhi penyebab timbulnya penyakit. Kelebihan lemak, bisa menyebabkan terjadinya obesitas. Ini bisa Mengubah bentuk tubuhmu secara permanen karena bisa memperlambat pertumbuhan tulang. Anda harus mulai menurunkan berat badan mulai sekarang. Karena jika tidak, semuanya akan terlambat.
                        </p>
                    </li>
                    <li>
                        <h2>» Jika menggunakan kapsul ini, Anda bisa menurunkan berat setidaknya 10 kg, 100% garansi kepuasan!</h2>
                        <p class="right600">
                            <a href="{{ $voluum }}" target="_blank" style="text-decoration: none; border: 0;margin-bottom: 5px;background: #000;">
                                <img src="{{ asset('build/images/frontend/cqplus_600_black_rek.jpg') }}" style="width: 100%; border: none;margin:10px 0;">
                            </a>
                        </p>

                        <p>
                            Dari hasil penelitian terakhir : Jika Anda kekurangan dua nutrisi penting ini saat diet, dapat memperlambat metabolisme. Ini bisa dilihat dari penelitian kepada banyak orang.
                            Sekarang, bahan-bahan ini tidak bisa ditemukan pada makanan hasil olahan. Ekstrak Garcinia dan ekstrak rasberi kering ini dapat membantu menurunkan berat dengan mengejutkan. Selalu masukkan dua bahan ini untuk menunjang diet dan Anda bisa turun 10-14 kg hanya dalam 4 minggu saja.
                            Produk ini sudah terbukti memiliki kandungan tinggi ekstrak dua bahan penting ini.
                        </p>

                        <p><strong>Tahap 1: </strong></p>
                        <p>
                            Produk Ini. Gunakan link ini untuk gratis pengiriman.
                        </p>
                        <p><strong>Penawaran berakhir : <br> {{ \Carbon\Carbon::now()->format('M d, Y') }}</strong></p>

                        <p><strong>Tahap 2: </strong></p>
                        <p>Konsumsi 2 tablet sehari dan ikuti 5 aturan dalam mengecilkan perut untuk hasil yang lebih baik.</p>

                    </li>
                    <li>
                        <h2>» Perut Ramping</h2>
                        <img src="{{ asset('build/images/frontend/flat.jpg') }}" class="slim">
                        <ol class="num">
                            <li>
                                <strong>Perenggangan</strong>
                                <p>Latihan paling mudah untuk otot perut adalah dengan menahan nafas sampai otot perut terasa berkontraksi. Lakukan ini sebanyak yang Anda bisa.</p>
                            </li>
                            <li>
                                <strong>Makan Apel</strong>
                                <p>Buah ini, bisa meningkatkan kinerja usus agar elbih efisien.</p>
                            </li>
                            <li>
                                <strong>Hindari konsumsi buah pisang</strong>
                                <p>Buah pisang banyak mengandung zat tepung yang jika menumpuk di tubuh bisa menjadi lemak.</p>
                            </li>
                            <li>
                                <strong>Jangan minum sebelum makan selesai</strong>
                                <p>Minum di sela-sela makan bisa menyebabkan cairan lambung melemah dan memperlambat fungsi organ.</p>
                            </li>
                            <li>
                                <strong>Jangan makan sebelum tidur</strong>
                                <p>Jangan lupa, pada saat malam hari, tubuh membakar kalori lebih lambat.</p>
                            </li>
                        </ol>
                    </li>
                </ul>
            </div>

        </div>
        <div id="tx-adds">
            <h3>
                <a href="{{ $voluum }}" target="_blank">
                    Klik di sini untuk mencoba {{ $product1->name }}.
                </a>
            </h3>
            <br><br>
            <span style="color:#000; font-size: 1.2em; font-weight: bold;">
			Kami menjamin bahwa Anda pasti akan menurunkan berat badan. Kami memberi jaminan kepuasan!</span><br>
            <p class="add-label-v">Diskon Festival: diskon anggota, 60% lebih hemat!</p><br>
            <font id="fnt" color="red" style="font-size: 18px; font-weight: 600">Penawaran berakhir pada: {{ \Carbon\Carbon::now()->format('M d, Y') }}<br></font>

        </div>	 <div id="tx-comments">
            <a name="comm"></a>
            <h1>
                Komentar&nbsp;<em style="color: rgb(255, 255, 255); font-size: 12px;">(38/38)</em></h1>
            <div class="descrip">Baca komentar dari artikel “Amerika memiliki rekor dalam penurunan berat badan. Namun sekarang Thailand telah memecahkan rekor tersebut!”</div>
            <div class="comment">
                <div class="padd"></div>
                <span class="text1">Benjamin,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Saya dan teman saya mencari cara untuk menurunkan berat badan, yang hasilnya efektif dan tahan lama. Satu bulan yang lalu Para ilmuwan meneliti ekstrak ini dan kami menunggu hasilnya dengan cemas. Segera, setelah artikel tersebut diterbitkan dan produk ini muncul di pasaran, kami memutuskan untuk membeli. Mereka member 2 produk gratis), dan akhirnya kami berdua berhasil menurunkan berat badan masing-masing 5 kg. Artikel ini menakjubkan. Hal ini jelas bisa mengubah hidup kita. Tanyakan pada siapa saja yang sudah menikmati manfaat dari penawaran fantastis ini. Semoga berhasil.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Piyada,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Saya melihat beritanya di TV beberapa hari lalu, dan sangat terkesan dengan hasil yang saya lihat. Saya akan menikah dalam beberapa bulan lagi, jadi ini adalah saat yang tepat untuk mencobanya. Terima kasih untuk tips yang luar biasa ini.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Ray Wan,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Saya sudah membaca artikelnya, sudah memencet tombol pemesanan dari sekarang. Tidak sabar melihat hasilnya dan sekarang saya mennunggu paket saya datang.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Sai Saen,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Semua teman saya membicarakan produk ini. Saya bahkan mendengar Ibu saya juga membicarakan suplemen ini. Teman saya berhasil menurunkan 12 kg hanya dalam 15 hari! Kalian harus mencoba ini, saya juga sedang memesannya.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Psychic,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Saya sangat puas dengan hasilnya, saya berharap produk ini juga berhasil untuk Anda, semoga beruntung!
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Nipaporn,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Produk ini sangat mengagumkan, teman saya menggunakan produk ini dan beratnya turun beberapa kilogram. Saya tadinya tidak percaya dan mencari informasi lebih lanjut mengenai produk ini, hingga saya menemukan artikel ini. Saya sungguh kagum dengan hasilnya dan mereka member jaminan kepuasaan pada pembeli. Sungguh penemuan luar biasa. Terima kasih.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Sugar,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Selama hidup, saya selalu bermasalah dengan berat badan saya dan saya harap ini solusi terbaik untuk saya. Saya memutuskan untuk menekan tombol pembelian dan mengisi formulirnya. Seorang konsultan segera menelepon saya kembali untuk memberikan konsultasi gratis. Kemudian seseorang menelepon lagi untuk konfirmasi pemesanan dan alamat saya. Mereka betul-betul professional. Sekarang saya juga mendaftar ke pusat kebugaran. Target saya bisa turun 17 kg dan bisa menggunakan pakaian renang saat pergi dengan kekasih.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Ubonwan,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(6)->format('M d, Y') }} </span>
                <div class="text3">
                    Halo, saya sedang menunggu 2 paket saya datang, dan saya tidak sabar untuk segera menggunakan dan melihat hasilnya. Semoga ada petunjuk pemakaiannya di kemasan, terima kasih.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Veerachai,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Istri saya menggunakan produk ini (dia membeli apa saja yang bisa membantunya menurunkan berat). Tadinya saya hanya menertawakan kebiasaannya ini karena tidak ada satu pun produk yang berhasil. Sekarang saya harus mengakui jika saya salah. Dia mendapatkan hasil yang luar biasa. Bentuk tubuhnya berubah, dia menjadi lebih bugar dan aktif, kami menjadi lebih bahagia. Saya mulai ikut menggunakannya juga.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Raviwan,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Teman saya merekomendasikan produk ini sekitar 3 minggu yang lalu, dan produk ini sampai pada saya dalam 3 hari (saya tidak perlu membayar ekstra untuk produk tambahan yang dikirimkan). Hasilnya sangat luar biasa, dan saya tidak sabar melihat hasil selanjutnya setelah 3-4 minggu.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Kanchana,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Pada dasarnya, motivasi sangatlah penting. Namun, saya rasa saya akan mencoba menyelidiki produk yang banyak diperbincangkan di kalangan artis hollywood ini. Saya sedang mencoba 2 paket dan akan member info lagi setelah hasilnya keluar.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Sunisa,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Saya bisa memahami ceritanya, karena saya sendiri juga mengalami hal yang sama.sekarang saya sudah menurunkan berat 10 kg dengan produk ini dan saya merasa sangat luar biasa. Sekarang saya lebih mudah beraktivitas dan menjaga kesehatan saya. Ceritakan pada saya bagaimana hasilnya pada Anda.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Supanee,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Saya ingin melihat komentar-komentar bagus di sini.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Kanya Rat,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Ini merupakan cara paling bagus untuk menurunkan berat pada ibu yang baru saja melahirkan.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Dalat,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Terdengar mustahil, tapi banyak orang berkomentar jika ini benar-benar berhasil. Jadi, saya harus lihat sendiri hasilnya, saya akan mencobanya mulai besok.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Patar,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Saya sudah mencoba beragam cara untuk menurunkan berat, namun selalu gagal pada akhirnya. Saya mencoba untuk diet, dan kadang ini berhasil menurunkan beberapa kilogram, tapi nanti pasti akan naik lagi. Produk ini tidak demikian, saya ingin mengingatkan ini pada Anda.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Weeraya,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Cobalah dan ceritakan hasilnya pada kami, ini sangat membantu saya dan banyak lagi yang lain, tapi belum tentu semua mendapat hasil yang sama.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Asanas,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(5)->format('M d, Y') }} </span>
                <div class="text3">
                    Halo, tanggapan Anda sangat baik, perasaan saya tidak baik tidak juga buruk mengenai ini, tapi apakah benar berhasil? Saya akan membelinya juga sebentar lagi dan ingin mendapatkan hasil yang maksimal serta menurunkan berat sebanyak saya bisa. Saya memang mengalami obesitas, namun tidak banyak orang tahu ini karena sindrom tiroid yang saya miliki. Saya sudah memotong kelenjar tiroid saya 4 tahun yang lalu. Melihat banyak orang yang sukses menurunkan berat mereka membuat saya ingin mencobanya juga, tapi apakah akan sukses juga untuk saya? Saya berencana untuk olahraga rutin juga, kita lihat apakah produk ini dapat mewujudkan keinginan saya. Doakan saya beruntung.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Kalaya,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(3)->format('M d, Y') }} </span>
                <div class="text3">
                    Wow, ini luar biasa! Turun 2 kilo kurang dari seminggu.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Jaruwat,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(3)->format('M d, Y') }} </span>
                <div class="text3">
                    Aku sedang mencoba metodse lain dan tidak ada perubahan yang terjadi. Beritahu aku jika ini benar-benar berhasil untuk kalian.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Kwanruthai,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(3)->format('M d, Y') }} </span>
                <div class="text3">
                    Yaa, saya sudah menerima paket produk ini sejak 2 minggu lalu dan sudah berhasil menurunkan 3 kg. Ini sangat luar biasa, saya sudah melakukan pemesanan 3 paket lagi, salah satunya untuk teman saya. Ini benar-benar bagus, saya sudah mencoba untuk mengontrol makan dengan berbagai cara dan hasilnya selalu mengecewakan. Berhenti membuang-buang uang untuk membeli produk lain dan coba ini sekarang juga!
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Sikarin,</span><span class="text2">{{ \Carbon\Carbon::now()->subDays(3)->format('M d, Y') }} </span>
                <div class="text3">
                    Ini luar biasa, saya turun 1,5 kg hanya dalam 4 hari saja. Hasilnya sangat bagus, mengingat saya harus tinggal di kantor berjam-jam dan tidak memiliki banyak waktu untuk olahraga. Apa ada cara lain yang bisa meningkatkan performa produk ini?
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Broad,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Halo semuanya, saya sudah mencoba produk ini dan berat saya turun 4 kg dalam satu bulan.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Crown,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Terima kasih untuk sarannya, semoga nafsu makan saya bisa berkurang dan saya mendapat hasil sama dengan yang lainnya.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Nat,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya sangat terkejut Anda punya produk seperti ini. Saya tidak merasa lapar awalnya, ini bisa saja membuat berat saya turun bisa juga tidak. Tapi ini tidak masalah, karena hasil pada setiap orang pasti akan berbeda.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Rain,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Sebelum menggunakan produk ini, saya tidak pernah merasa memiliki tenaga seperti sekarang. Saya tetap bugar namun rasa lapar saya menghilang. Saya hanya mengharapkan hasil yang terbaik saja.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Dolphin,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya mencoba produk ini selama satu bulan dan berhasil turun 9 kg, dan sekarang setelah setahun, berat saya tidak kembali naik.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Narumol,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Ini memberi saya semangat untuk terus berusaha mendapat hasil yang terbaik.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Nongluk,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya mendapatkan produk ini sebagai hadiah ulang tahun dari ibu saya. Beliau bercerita jika menggunakan produk penurun berat, dan ingin membaginya dengan saya karena ibu saya tidak ingin melihat anaknya tidak laku. Setelah minum 26 tablet, saya bisa merasakan perubahannya karena celana saya jadi semakin longgar.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Suwana,</span><span class="text2">{{ \Carbon\Carbon::now()->subDay()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya sudah membaca semua komentar ini, jadi saya rasa tidak ada salahnya saya mencobanya. Saya mengisi formulirnya sekarang juga.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Bongkot,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya sudah menggunakan ini selama 2 minggu dan sekarang berat saya sudah berkurang 2 kg. (semoga turun lebih lagi)
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Pueblo Lakes,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya juga berhasil menurunkan berat. Semangat semuanya.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Kanchana,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya berusia 22 tahun, dan target saya bisa menurunkan 20 kg. Awalnya saya ragu, tapi saya sudah mencoba beragam produk dan hasilnya tidak ada yang memuaskan, beberapa malah membuat saya berjerawat. Setelah saya menemukan produk ini, saya mulai menggunakannya saat berat saya 107 kg dan itu sekitar 3 bulan yang lalu. Berat saya turun menjadi 87 kg sekarang. Setiap saya melakukan pemesanan, paket sampai tepat waktu. Customer service sangat professional. Dari pengalaman saya ini, saya merekomendasikan produk ini pada teman-teman dan keluarga saya. Hasilnya memang sangat menakjubkan.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Puckanun,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya mencoba satu paket, dan ini cukup menurunkan berat 6 kg. sekarang saya ingin memesan satu paket lagi untuk menurunkan 12 kg sebelum liburan jadi saya bisa menggunakan bikini. Saya rasa, ini adalah produk terbaik yang pernah saya coba. Tidak ada efek sampingnya. Dan yang paling penting, tidak perlu olahraga karena saya tidak punya waktu untuk itu. Ini sangat cocok untuk saya, saya rekomendasikan Anda mencobanya!
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Pranee,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya seorang wanita 30 tahun yang sudah mencoba berbagai cara untuk menurunkan berat. Namun semua cara itu gagal. Saya mencoba produk ini selama 3 bulan dan pada bulan pertama saya berhasil menurunkan 8 kg dan meningkatkan kebugaran. Bagian terbaiknya adalah produk ini tidak memiliki efek samping seperti gemetar atau susah tidur pada saya. Akhirnya saya berhenti menggunakan produk lain dan berencana membeli paket ini lagi. Saya juga merekomendasikannya untuk orang-orang.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Savitri,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Setiap kali saya berusaha menurunkan berat, selalu jadi usaha yang sia-sia. Saya adalah wanita 32 tahun dan saya tidak suka dengan bentuk tubuh saya semenjak saya melahirkan anak kedua, karena berat saya tidak kembali ke awal lagi. Semua usaha yang saya coba lakukan selalu gagal. Saya sudah mencoba semuanya, obat pembakar lemak, lidah buaya, mengontro makanan dengan diet, namun semua tidak menunjukkan hasil yang memuaskan. Mungkin saja ini karena saya orangnya tidak sabaran. Pada awalnya saya ragu-ragu juga mencoba produk ini, tapi saya berpikir tidak ada salahnya juga dicoba. Kemudian saya memesan produk ini dan datang 2 hari kemudian. Saya mencobanya, dan menunggu hasilnya dengan sabar. Setelah 7 hari, berat saya turun 2 kg, saya juga merasa tubuh semakin bugar. Dengan produk sebelumnya saya merasa tidak bertenaga. Hasilnya sangat berbeda, dan saya tidak sabar untuk melihat hasil akhirnya. Karena setelah ini saya akan wisuda, jadi saya tidak sabar menunjukkan perubaha bentuk tubuh saya pada keluarga.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Jintana,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Sekarang memang saya tidak bisa menunjukkan bagaimana produk ini tidak akan membuat berat naik lagi. Tapi percayakah Anda jika saya bisa turun 3 kg dalam 7 hari dan dalam proses menurunkan 20 kg? Anda harus mencobanya sendiri.
                </div>
            </div><div class="comment">
                <div class="padd"></div>
                <span class="text1">Suda,</span><span class="text2">{{ \Carbon\Carbon::now()->format('M d, Y') }} </span>
                <div class="text3">
                    Saya turun hampir 6 kg setelah pemakaian produk ini. Dan yang palin penting, produk ini tidak berbahaya. Saya merasa bugar dan bersemangat selalu.
                </div>
            </div>
            <h2>
                Komentar      :</h2>
            <div class="cancel-comment-reply"> <small><a style="display: none;" target="_blank" href="#" id="cancel-comment-reply-link" rel="nofollow">
                        คลิกตรงนี้      </a></small> </div>
            <form id="commentform" action="#">
                <p>
                    <input aria-required="true" tabindex="1" size="22" id="author" name="author" type="text">
                    <label for="author"><small>
                            Nama (harus diisi)          </small></label>
                </p>
                <p>
                    <input aria-required="true" tabindex="2" size="22" id="email" name="email" type="text">
                    <label for="email"><small>
                            Alamat Email (tidak akan dimunculkan) (wajib)         </small></label>
                </p>
                <p>
                    <textarea tabindex="4" rows="10" cols="60%" id="comment-form" name="comment"></textarea>
                </p>
                <p>
                    <input value="Konfirmasi Komentar" tabindex="5" id="submit" name="submit" onclick="return addcomm();" type="submit">
                </p>
            </form>
            <br style="clear: both;">
        </div>    <footer id="tx-footer"><p style="text-align:center;">
                © 2017
                <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
            </p></footer>
    </div>
</center>

</body>
</html>