@extends('layout.garcinia')

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/garcinia/all.css') }}">
@endsection

@section('content')
    <hr style="height: 10px" class="metamorphosis">

    <div class="metamorphosis-header">
        <div class="page-container">
            <div class="ng with-desc one-line">
                <h1>KOMPOSISI</h1>
                <p style="font-size:30px;">Garcinia Cambogia Forte memiliki komposisi dan kualitas yang seimbang sempurna.</p>
            </div>
        </div>
    </div>

    <section id="block-6" class="page-container standard composition">
        <div class="row first">
            <div class="left-side">
                <img src="{{ asset('build/images/frontend/block-4-figure.jpg') }}" alt="Ekstrakt piperyny odseparowany z wierzchniej warstwy pieprzu">
                <h2>Ekstrak alami Garcinia Cambogia dari buah Malabar Tamarind</h2>
            </div>
            <div class="right-side">
                <ul>
                    <li class="white">Komposisi 100% ekstra alami dari tumbuhan-tumbuhan dan buah-buahan.</li>
                    <li class="white">Garcinia Cambogia Forte memiliki komposisi dan kualitas yang seimbang sempurna.</li>
                    <li class="white">Komposisinya unik – dibuat bekerja sama degan para ahli gizi dan para ahli fisiologi.</li>
                    <li class="white">Penyerapannya sangat baik karena komposisinya alami, tidak menggunakan bahan pengawet, tanpa zat kimia, tanpa zat pewarna buatan, bebas MSG. </li>
                    <li class="white">Semua bahan berasal dari pemasok bersertifikat, dan jaminan kualitas tertinggi – sangat berharga dan bermanfaat untuk menurunkan berat badan.</li>
                </ul>
            </div>
        </div>
        <h1 style="font-size:35px;"><b>Garcinia Cambogia Forte – komposisi ampuh dan alami</b></h1>
        <div class="row first">
            <h2>GARCINIA CAMBOGIA DAN SIFATNYA</h2>
            <ul>
                <li><strong>Konsentrasi</strong> – 95% Garcinia Cambogia dalam Garcinia Cambogia Forte paling tinggi sudah tersedia di pasaran Indonesia.</li>
                <li><strong>Jenis</strong> – ekstrak tumbuhan murni zat-zat aktif dari buah Malabar Tamarind.</li>
                <li><strong>Asal</strong> – ekstrak 95% Garcinia Cambogia dari perkebunan Malabar Tamarind dari India, berkualitas dan berharga tinggi.</li>
                <li><strong>Cara kerja</strong> – meningkatkan asam lambung, usus dan pankreas, meningkatkan metabolisme tubuh, memecah sel-sel lemak dan memblokir proses pengendapan lemak.</li>
            </ul>
            <p class="m-top">Garcinia Cambogia adalah alkaloid yang terdapat pada buah Malabar Tamarind. Garcinia Cambogia mengganggu fungsi gen yang membentuk lemak dan mencegah pertumbuhan sel-sel lemak baru, oleh karena itu Anda bisa menurunkan berat badan dan menghilangkan lemak dengan mudah dan cepat.</p>
            <p class="m-top">Zat-zat Garcinia Cambogia meningkatkan asam lambung dan metabolisme tubuh. Garcinia Cambogia memecah sel-sel lemak dan memblokir proses pengendapan lemak, sehingga tubuh Anda membakar lemak dan kalori dengan mudah dan cepat. Dengan mengurangi nafsu makan, Garcinia Cambogia mengurangi kadar lemak dan karbohidrat di tubuh secara alami. Direkomendasikan untuk orang yang mengalami kelebihan berat badan atau obesitas, dan orang yang berolahraga untuk membentuk kontur tubuh.</p>
            <ul>
                <li>Meningkatkan asam lambung</li>
                <li>Ekstrak Garcinia Cambogia dari buah Malabar Tamarind</li>
                <li>Tumbuh Tanaman Malabar Tamarind</li>
                <li>Komposisi alami dan efektif</li>
                <li>Garcinia Cambogia</li>
            </ul>
            <br><br>
            <h2>Manfaat Garcinia Cambogia Forte:</h2>
            <ul>
                <li>Meningkatkan tingkat serotonin untuk meningkatkan mood dan meringankan stres</li>
                <li>Menggunakan serotonin untuk membantu tubuh mendapatkan kualitas tidur</li>
                <li>Menekan nafsu makan menggunakan HCA dan memblok produksi lemak</li>
                <li>Membantu mengurangi keinginan untuk mengkonsumsi karbohidrat, gula, dan junk food</li>
                <li>Menurunkan berat badan</li>
                <li>Menurunkan kolesterol jahat dan trigliserida di dalam tubuh</li>
            </ul>
            <br><br>
            <h2>Garcinia Cambogia Forte membantu Anda untuk:</h2>
            <ul>
                <li>Turun hingga 9-12 kg dalam 1 bulan</li>
                <li>Memblokir proses pengendapan lemak</li>
                <li>Memecah sel-sel lemak dan membentuk kontur tubuh</li>
                <li>Membantu tubuh membakar lemak hingga 82 persen</li>
                <li>Meningkatkan metabolisme tubuh</li>
                <li>Menghilangkan selulit dan stretch mark</li>
            </ul>
            <p class="m-top">Ekstrak alami Garcinia Cambogia Forte dari buah Malabar Tamarind memiliki sifat menurunkan berat badan. Selain itu, Garcinia Cambogia Forte juga memiliki beberapa manfaat lain: menjaga kesehatan kondisi tubuh, meningkatkan kinerja fisik, membantu untuk mengurangi kolesterol tinggi, mengatur tekanan darah, memfasilitasi penyerapan nutrisi dan vitamin dalam tubuh Anda.</p>
            {{--<figure id="block-6-1">--}}
                {{--<img src="{{ asset('build/images/frontend/ingredient.png') }}" alt="Ekstrakt piperyny odseparowany z wierzchniej warstwy pieprzu">--}}
            {{--</figure>--}}
        </div>
    </section>

    <div class="banner-composition" onclick="document.location.href='{{ route('page.preorder') }}'">
        <div class="page-container">
            <h1>Ekstrak alami dalam 1 kapsul</h1>
            <p>Komposisi alami dan efektif untuk menurunkan berat badan dengan mudah dan cepat. Turun secara permanen. Mulailah menurunkan berat badan secara alami dengan Garcinia Cambogia Forte.</p>
            <a href="{{ route('page.preorder') }}" class="orange">PESAN SEKARANG</a>
        </div>
    </div>
@endsection