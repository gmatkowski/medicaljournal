@extends('layout.app',[
    'title' => 'Garcinia Cambogia Forte'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/garcinia2/all.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,300italic,600italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet">
@endsection

@section('content')
    <div class="cont-wrapper hideJs" id="body-first">
        <div class="block block1">
            <div class="limit clearfix">
                <a href="#request_form_hash" class="logo" data-scroll="true">Garcinia Cambogia Forte
                </a>
                <div class="content">
                    <p class="title1 ttu light"> TURUNKAN BERAT BADAN BERLEBIH
                    </p>
                    <p class="title2 ttu light"> CEPAT DAN MUDAH DENGAN Garcinia Cambogia Forte
                    </p>
                    <p class="description"> Semua yang Anda perlukan untuk menurunkan berat badan dengan cepat
                    </p>
                    <div class="list">
        <span> CARA MENURUNKAN BERAT BADAN YANG MUDAH DAN MENYENANGKAN DENGAN Garcinia Cambogia Forte:
                      </span>
                        <ul>
                            <li> Pembakar lemak cepat dan alami
                            </li>
                            <li> Mengurangi nafsu makan secara sehat
                            </li>
                            <li> Penampilan menarik dan tetap sehat
                            </li>
                        </ul>
                        <a href="#request_form_hash" class="btn btn-transparent btn-with-arrow scroll"> PESAN SEKARANG
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block2">
            <div class="limit clearfix">
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-1.jpg') }}">
                    <div>
        <span> Mengecilkan pipi berisi
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-2.jpg') }}">
                    <div>
        <span> Pinggang langsing
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-3.jpg') }}">
                    <div>
        <span> Panggul ramping
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-4.jpg') }}">
                    <div>
        <span> Lengan kencang
                      </span>
                    </div>
                </div>
                <div class="item">
                    <img alt="" src="{{ asset('build/images/frontend/block2-5.jpg') }}">
                    <div>
        <span> Kaki elegan
                      </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block3">
            <div class="limit">
                <p class="title1 ttu"> PENURUNAN BERAT DENGAN NUTRISI YANG TEPAT ADALAH METODE UTAMA PENURUNAN BERAT BADAN MASA KINI
                </p>
                <div class="clearfix">
                    <div class="l-graph">
                        <p class="title ttu">
        <span> OBESITAS
                        </span>
                            1950-2010
                        </p>
                        <div class="content">
        <span class="line line1"> Obesitas dan kelebihan berat badan pada 20-74
                        </span>
                            <span class="line line2">
                          Obesitas pada 20-74
                        </span>
                            <span class="line line3">
                          kelebihan berat badan pada 20-74
                        </span>
                            <span class="line line4">kelebihan berat badan pada 12-19
                        </span>
                            <span class="line line5">kelebihan berat badan pada 6-11
                        </span>
                        </div>
                        <div class="info">
                            Menurut laporan WHO yang dipresentasikan di AS pada Maret 2010, jumlah orang dengan kelebihan berat badan terus meningkat.
                            <span> Lebih dari seperempat populasi penduduk Amerika menderita masalah kelebihan berat badan dan obesitas.
                        </span>
                        </div>
                    </div>
                    <div class="r-graph">
                        <div class="title clearfix">
                            <p class="ttu"> METODE PENURUNAN BERAT YANG DIPILIH
                            </p>
                            <ul>
                                <li> pria
                                </li>
                                <li> wanita
                                </li>
                            </ul>
                        </div>
                        <div class="content">
                            <div class="row clearfix">
                                <div class="label">
                                    Diet sederhana dan suplemen nutrisi
                                </div>
                                <div class="scale">
        <span class="man" style="width: 100%">
        </span>
                                    <span class="woman" style="width: 97%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Minum/ makan suplemen nutrisi
                                </div>
                                <div class="scale">
        <span class="man" style="width: 94%;">
        </span>
                                    <span class="woman" style="width: 55%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Memperbaiki pola makan
                                </div>
                                <div class="scale">
        <span class="man" style="width: 83%;">
        </span>
                                    <span class="woman" style="width: 36%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Aktivitas olahraga yang intens
                                </div>
                                <div class="scale">
        <span class="man" style="width: 40%;">
        </span>
                                    <span class="woman" style="width: 36%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Diet ketat saja
                                </div>
                                <div class="scale">
        <span class="man" style="width: 47%;">
        </span>
                                    <span class="woman" style="width: 36%;">
        </span>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="label">
                                    Diet ketat dan olahraga intensif
                                </div>
                                <div class="scale">
        <span class="man" style="width: 32%;">
        </span>
                                    <span class="woman" style="width: 18%;">
        </span>
                                </div>
                            </div>
                        </div>
                        <div class="info">
                            Menurut statistik penelitian yang dilaporkan WHO, 70% warga Amerika lebih suka menjaga berat badan mereka dengan diet sederhana.
                            <span> Namun, mereka seringkali lalai dalam melakukan diet tersebut karena dibutuhkan tingkat disiplin dan usaha yang tinggi. Oleh sebab itu, metode penurunan berat badan ini menjadi tidak efektif.
                        </span>
                        </div>
                    </div>
                </div>
                <div class="block-title">
        <span class="light ttu"> DAPATKAN PINGGANG SEMPURNA!
                    </span>
                    <em> Buat tubuh Anda ramping dengan cara cepat dan sederhana
                    </em>
                </div>
            </div>
        </div>
        <div class="block block4">
            <div class="limit clearfix">
                <div class="side light-side">
                    <ol>
                        <li> Tanpa pantangan makanan - tanpa stres!
                        </li>
                        <li> Tanpa diet
                            <br> ketat - tanpa
                            <br> penderitaan!
                        </li>
                        <li> Penurunan berat alami dan hasilnya bertahan selamanya!
                        </li>
                    </ol>
                    <div class="prompt light ttu">
        <span class="cell">ANDA DAPAT MAKAN MAKANAN FAVORIT ANDA SETIAP HARI!
                      </span>
                    </div>
                    <div class="info">Menurunkan berat badan menggunakan Garcinia Cambogia Forte itu mudah dan menyenangkan
                    </div>
                </div>
                <div class="side dark-side">
                    <ol>
                        <li> Pantangan makanan menyebabkan stres yang berkelanjutan
                        </li>
                        <li> Masakan yang tidak Anda suka bisa menjadi godaan konstan untuk meninggalkan diet Anda
                        </li>
                        <li> Menahan diri berlebihan menyebabkan kelelahan dan membuat kulit Anda jelek
                        </li>
                    </ol>
                    <div class="clearfix ">
                        <div class="prompt light ttu">
        <span class="cell">MELAKUKAN DIET ITU SULIT DAN TIDAK EFEKTIF
                        </span>
                        </div>
                    </div>
                    <div class="info">Menurunkan berat badan
                        <br> dengan cepat dan mudah
                        <br> dengan
                        <br> Garcinia Cambogia Forte!
                    </div>
                </div>
            </div>
        </div>
        <div class="block block5">
            <div class="limit clearfix">
                <div class="side l-side">
                    <p class="title1 light ttu"> TURUNKAN BERAT BERLEBIH
                    </p>
                    <em> Hanya satu paket cukup untuk mencapai hasil yang diharapkan
                    </em>
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/prod.png') }}" width="308">
                        <em class="lefter"> Pembakar Lemak Garcinia Cambogia Forte baru
                        </em>
                    </div>
                </div>
                <div class="side r-side">
                    <p class="title1 ttu"> INSTITUT NUTRISI KESEHATAN MEREKOMENDASIKAN METODE AMPUH MENURUNKAN BERAT BADAN
                    </p>
                    <div class="description">
                        <p> Garcinia Cambogia Forte mengandung konsentrat yang secara aktif menghancurkan timbunan lemak di dalam tubuh. Meningkatkan fungsi metabolisme, detoksifikasi tubuh Anda, dan membuang radikal bebas.
                        </p>
                        <p> Anti oksidan alami menjaga kesehatan dan membuat penampilan menjadi makin menarik
                        </p>
                    </div>
                    <a href="#request_form_hash" class="btn btn-transparent btn-with-arrow scroll"> PESAN SEKARANG
                    </a>
                </div>
                <div class="center"> Garcinia Cambogia Forte tidak hanya membantu Anda menurunkan berat badan, tetapi juga mengandung banyak zat bermanfaat. Murni, tanpa zat-zat tambahan berbahaya dan dengan konsentrasi ekstrak yang maksimal. Garcinia Cambogia Forte memberi manfaat yang ajaib! Tambah lagi, Garcinia Cambogia Forte dalam kapsul ini hemat, ringkas, mudah diminum.
                </div>
            </div>
        </div>
        <div class="block block6">
            <div class="limit">
                <h3 class="title ttu"> PENURUNAN BERAT BADAN YANG CEPAT: HINGGA 36 KG!
                    <span> ANDA MENDEKATI BENTUK TUBUH IDEAL SETIAP HARINYA!
                    </span>
                </h3>
                <div class="row clearfix">
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block6-1.jpg') }}">
                        </div>
                        <div class="info">
                            <p class="label">Hari ke 7
                            </p>
                            <p> Kegemukan menurun, tumpukan lemak mulai hancur. Lingkar pinggang turun 1-3 inci, biasanya didapat dengan lebih dari 7 hari diet.
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block6-2.jpg') }}">
                        </div>
                        <div class="info">
                            <p class="label">  Hari ke 14
                            </p>
                            <p> Perut buncit menjadi rata, berat turun 2-4 kg. Anda merasa lebih ringan dan penuh energi!
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block6-3.jpg') }}">
                        </div>
                        <div class="info">
                            <p class="label">  Hari ke 28
                            </p>
                            <p> Berat badan berkurang secara dramatis, ukuran pakaian 48 turun menjadi 44. Lingkar pinggang turun lebih dari 4 inci, total penurunan berat 9 kg!
                            </p>
                        </div>
                    </div>
                    <div class="item">
                        <div class="image">
                            <img alt="" src="{{ asset('build/images/frontend/block6-4.jpg') }}">
                        </div>
                        <div class="info">
                            <p class="label">Hari ke 60
                            </p>
                            <p> Penurunan berat badan stabil hingga 40 kg. Anda kini dapat memakai pakaian ukuran 42. Kondisi kulit meningkat secara signifikan, terlihat segar dan kencang.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block7">
            <div class="limit">
                <p class="title ttu light">SEBAB DAN MEKANISME PEMBENTUKAN LEMAK BAWAH KULIT
                </p>
                <div class="content">
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-1.png') }}">
                        </div>
                        <span class="label ttu">LANGKAH 1
                      </span>
                        <p> Diet yang tidak sehat dan tidak teratur, makanan berbahaya dengan kandungan kalori tinggi, gaya hidup pasif dan stres yang konstan menyebabkan akumulasi energi tak terpakai.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-2.png') }}">
                        </div>
                        <span class="label ttu"> LANGKAH 2
                      </span>
                        <p class="sec">Lemak terus-menerus terkumpul dalam tubuh, tidak terhancurkan dan tidak terbuang. Lemak berlebih diserap melalui darah, membentuk apa yang disebut "timbunan lemak" di badan kita, dan tetap berada di sana selamanya.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-3.png') }}">
                        </div>
                        <span class="label ttu">LANGKAH 3
                      </span>
                        <p>Seiring berjalannya waktu "timbunan lemak" ini semakin besar dan menjadi semakin susah untuk membakar lemak yang terakumulasi.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block7-4.png') }}">
                        </div>
                        <span class="label ttu"> LANGKAH 4
                      </span>
                        <p>Lemak secara bertahap menembus lapisan bawah kulit (subkutan), merobek strukturnya dan menghalangi sirkulasi darah normal sehingga menimbulkan selulit.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block8">
            <div class="limit">
                <h3 class="ttu clearfix">
        <span class="label1"> Garcinia Cambogia Forte
                    </span>
                    <span class="label2">
                      OBAT PENURUN LEMAK TANPA LAPAR DAN OLAHRAGA YANG MELELAHKAN
                      <em> Penurunan berat badan mudah dari ukuran 14 ke 8
                      </em>
        </span>
                </h3>
                <div class="content line1 clearfix">
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block8-1.jpg') }}">
                        </div>
                        <div class="label">
        <span>PENGARUH KE JARINGAN TUBUH
                        </span>
                        </div>
                        <p>Pembakar lemak Garcinia Cambogia Forte beraksi dalam tubuh pada tingkat jaringan. Komponen aktifnya menemukan lokasi "timbunan lemak", menembusnya dan berakumulasi di sana.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block8-2.jpg') }}">
                        </div>
                        <div class="label">
        <span>PEMBAKARAN LEMAK
                        </span>
                        </div>
                        <p>Konsentratnya berkumpul di di "timbunan lemak" menyebabkan pembakaran lemak yang cepat dan efektif. Lebih dari 80% timbunan lemak dalam tubuh hancur dengan cara seperti ini.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block8-3.jpg') }}">
                        </div>
                        <div class="label">
        <span> PEMBERSIHAN TUBUH
                        </span>
                        </div>
                        <p>Berkat anti oksidan dan serat dari sayur-sayuran, Garcinia Cambogia Forte dengan cepat dan efektif membersihkan tubuh dari racun dan cairan berlebih dengan membuangnya secara alami melalui usus.
                        </p>
                    </div>
                    <div class="item">
                        <div class="image with-label">
                            <img alt="" src="{{ asset('build/images/frontend/block8-4.jpg') }}">
                        </div>
                        <div class="label">
        <span>PENINGKATAN PENAMPILAN
                        </span>
                        </div>
                        <p>Garcinia Cambogia Forte mengandung anti-oksidan alami yang tidak hanya bermanfaat untuk penurunan berat yang cepat tetapi juga meningkatkan proses peremajaan sel kulit. Karena ini, penampilan Anda semakin meningkat setiap hari!
                        </p>
                    </div>
                </div>
                <div class="content line2">
                    <h4 class="block-title">
        <span class="title"> RAHASIA KEAMPUHAN Garcinia Cambogia Forte
                      </span>
                        <span class="description">Komposisi Garcinia Cambogia Forte: ekstrak dari buah Malabar Tamarind
                      </span>
                    </h4>
                    <div class="squad1 clearfix">
                        <div class="item">
                            <div class="image">
        <span>
        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-1.jpg') }}">
        </span>
                            </div>
                            <div class="description with-label">
        <span> KONSENTRASI
                          </span>
                                <p>Tertinggi 95% Garcinia dalam Garcinia Cambogia Forte sudah tersedia di pasaran Indonesia.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
        <span>
        <img alt="" height="99" src="{{ asset('build/images/frontend/block8-squad1-2.jpg') }}">
        </span>
                            </div>
                            <div class="description with-label">
        <span> JENIS
                          </span>
                                <p>Ekstrak 95% Garcinia Cambogia diambil dari buah Malabar Tamarind, berkualitas dan bernilai tinggi.
                                </p>
                            </div>
                        </div>
                        <div class="item">
                            <div class="image">
        <span>
        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-3.jpg') }}">
        </span>
                            </div>
                            <div class="description with-label">
        <span> Cara kerja
                          </span>
                                <p>Meningkatkan asam lambung dan metabolisme tubuh, memecah sel-sel lemak dan menghambat proses pengendapan lemak.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="squad2">
                        <h4>
        <span>
                          Garcinia Cambogia Forte JUGA MENGANDUNG
                        </span>
                        </h4>
                        <div class="row clearfix">
                            <div class="item">
                                <img alt="" src="{{ asset('build/images/frontend/block8-squad2-1.jpg') }}">
                                <div class="story">
        <span class="label ttu"> SERAT
                            </span>
                                    <p> Serat sayuran mengisi lambung sehingga mengurangi nafsu makan dan membuat pencernaan menjadi normal.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <img alt="" src="{{ asset('build/images/frontend/block8-squad2-2.jpg') }}">
                                <div class="story">
        <span class="label ttu"> ELEMEN MIKRO
                            </span>
                                    <p>
                                        ZAT BESI, FOSFOR, SULFUR, POTASIUM, dan zat lainnya yang penting agar tubuh bekerja dengan baik
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <img alt="" src="{{ asset('build/images/frontend/block8-squad2-3.jpg') }}">
                                <div class="story">
        <span class="label ttu">
                              VITAMIN C
                            </span>
                                    <p> Meningkatkan fungsi perlindungan tubuh dan memperkuat dinding pembuluh darah.
                                    </p>
                                </div>
                            </div>
                            <div class="item">
                                <img alt="" src="{{ asset('build/images/frontend/block8-squad2-4.jpg') }}">
                                <div class="story">
        <span class="label ttu"> VITAMIN E
                            </span>
                                    <p> Meningkatkan struktur dan kondisi kulit dan mencegah timbulnya gurat kulit
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block9">
            <div class="limit">
                <p class="title ttu">
                    PENURUNAN BERAT CEPAT DENGAN Garcinia Cambogia Forte:
                    <span>
                      HASIL MENAKJUBKAN!
                    </span>
                </p>
                <p class="description">
                    Perbandingan sebelum dan sesudah:
                    <br>
                    Turunnya lingkar lengan atas, panggul, dan pinggang secara berkelanjutan!
                </p>
                <div class="content">
                    <div class="compare clearfix">
                        <div class="side before">
                            <div class="image">
                                <img alt="" src="{{ asset('build/images/frontend/block9-before.png') }}">
                                <span class="weight">
                            83 kg
                          </span>
                            </div>
                            <span class="tooltip tooltip-shoulder">
                          Lengan atas - 19 inci
                          <i>
        </i>
        </span>
                            <span class="tooltip tooltip-waist">
                          Pinggang - 43 inci
                          <i>
        </i>
        </span>
                            <span class="tooltip tooltip-hip">
                          Panggul - 52 inci
                          <i>
        </i>
        </span>
                        </div>
                        <div class="side after">
                            <div class="image">
                                <img alt="" src="{{ asset('build/images/frontend/block9-after.png') }}">
                                <span class="weight">
                            55 kg
                          </span>
                            </div>
                            <span class="tooltip tooltip-shoulder">
                          Lengan atas - 11 inci
                          <i>
        </i>
        </span>
                            <span class="tooltip tooltip-waist">
                          Pinggang - 28 inci
                          <i>
        </i>
        </span>
                            <span class="tooltip tooltip-hip">
                          Panggul -36 inci
                          <i>
        </i>
        </span>
                        </div>
                    </div>
                    <div class="info clearfix">
                        <div class="label">
                            <p class="title1 ttu">
                                Garcinia Cambogia Forte
                            </p>
                            <p class="title2 ttu">
                                PEMBAKAR LEMAK
                            </p>
                            <p class="title3">
                                dari bahan alami
                            </p>
                        </div>
                        <div class="item">
                            <img alt="" src="{{ asset('build/images/frontend/block9-info1.png') }}">
                        </div>
                        <div class="item">
                            <img alt="" src="{{ asset('build/images/frontend/block9-info2.png') }}">
                        </div>
                        <div class="item">
                            <img alt="" src="{{ asset('build/images/frontend/block9-info3.png') }}">
                        </div>
                        <div class="item">
                            <img alt="" src="{{ asset('build/images/frontend/block9-info4.png') }}">
                        </div>
                        <div class="item">
                            <img alt="" src="{{ asset('build/images/frontend/block9-info5.png') }}">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block10">
            <div class="limit clearfix">
                <div class="side l-side">
                    <h3 class="ttu">
                        SUPLEMEN DAN
                        <br>  PIL DIET LAIN
                    </h3>
                    <p class="description">
                        Kenapa pil diet Anda tidak efektif?
                    </p>
                    <ul>
                        <li> Penurunan berat lambat, berat badan seperti "memantul"
                        </li>
                        <li> Mengandung pencahar
                            <br>  menyebabkan gangguan pencernaan
                        </li>
                        <li> Dapat berpengaruh pada performa
                            <br>  jantung dan hati
                        </li>
                        <li> Dapat berisi komponen berbahaya yang menimbulkan kecanduan
                        </li>
                    </ul>
                </div>
                <div class="side r-side">
                    <h3 class="ttu ttu-rs"> Garcinia Cambogia Forte
                        <br>  YANG UNIK
                    </h3>
                    <p class="description">
                        Manfaat Garcinia Cambogia Forte
                    </p>
                    <ul>
                        <li> Menurunkan berat badan dengan cepat
                            <br>  tanpa naik lagi
                        </li>
                        <li> Tidak menyebabkan
                            <br>  gangguan pencernaan
                        </li>
                        <li> Benar-benar alami, suplemen yang murni secara ekologis berasal dari tanaman asli
                        </li>
                        <li> Diuji secara klinis dan direkomendasikan penuh oleh Institut Nutrisi Nasional
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="block block11">
            <div class="limit clearfix">
                <div class="content">
                    <p class="title1 ttu">
                        PEMBAKAR LEMAK Garcinia Cambogia Forte
                    </p>
                    <p class="title2 ttu"> DARI BAHAN ALAMI
                    </p>
                    <p class="title3"> Obat penurun berat badan tanpa lapar dan olahraga yang melelahkan
                    </p>
                    <p class="title4">Penurunan berat badan yang mudah dari ukuran 14 ke 8
                    </p>
                    <a href="#request_form_hash" class="btn btn-primary scroll" data-scroll="true">                     Pesan sekarang
                    </a>
                    <div class="brands">
                        <img alt="" src="{{ asset('build/images/frontend/block11-brands.png') }}">
                    </div>
                </div>
            </div>
        </div>
        <div class="block block12">
            <div class="limit clearfix">
                <div class="title">
                    <div>
                        <div class="cell label ttu">
        <span> Garcinia Cambogia Forte
                        </span>
                            PEMBAKAR LEMAK
                        </div>
                        <div class="cell description">Rahasia penurunan berat tanpa diet dan olahraga dari ukuran 14 ke 8
                        </div>
                        <div class="cell before step1">
                            <div class="before">
        <span>SEBELUM
                          </span>
                                <img alt="" src="{{ asset('build/images/frontend/block12-before1.jpg') }}">
                            </div>
                            <div class="after">
        <span> SESUDAH
                          </span>
                                <img alt="" src="{{ asset('build/images/frontend/block12-after1.jpg') }}">
                            </div>
                        </div>
                        <div class="cell before step2">
                            <div class="before">
        <span>SEBELUM
                          </span>
                                <img alt="" src="{{ asset('build/images/frontend/block12-before2.jpg') }}">
                            </div>
                            <div class="after">
        <span> SESUDAH
                          </span>
                                <img alt="" src="{{ asset('build/images/frontend/block12-after2.jpg') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content clearfix">
                    <div class="side l-side">
                        <div class="row clearfix">
        <span class="label">
                          NAMA PRODUK:
                        </span>
                            <span class="value">
                          Garcinia Cambogia Forte
                        </span>
                        </div>
                        <div class="row clearfix">
        <span class="label">
                          ISI:
                        </span>
                            <span class="value">
                          1 paket berisi 30 kapsul, masing-masing 500 mg
                        </span>
                        </div>
                        <div class="row clearfix">
        <span class="label">
                          KANDUNGAN:
                        </span>
                            <span class="value">
                          Ekstrak Garcinia Cambogia Forte Alami
                        </span>
                        </div>
                        <div class="row clearfix">
        <span class="label">
                          PENGGUNAAN:
                        </span>
                            <span class="value">
                          Minum satu kapsul 2 atau 3 kali sehari di antara waktu makan. Masa penggunaan minimal selama 2 minggu.
                        </span>
                        </div>
                    </div>
                    <div class="side r-side">
                        <div class="row clearfix">
        <span class="label">
                          CARA KERJA:
                        </span>
                            <span class="value">
                          Garcinia Cambogia Forte secara efektif menghancurkan timbunan lemak dan menghambat terbentuknya timbunan lemak baru, meningkatkan fungsi saluran pencernaan untuk membuang cairan berlebih dan racun dari tubuh. Vitamin kompleks dan anti-oksidan alami meremajakan tubuh dan membuatnya lebih sehat.
                          <br>
        <br>
                          Anda dapat meminum kapsul ini setiap saat, Anda juga dapat meminumnya saat bepergian tanpa harus merasakan rasa pahit di lidah. Kualitas bahan baku kapsul dikontrol ketat, dan ekstraknya diekstraksi dan diperkaya dalam laboratorium.
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block13">
            <div class="limit clearfix">
                <div class="title ttu">
                    KISAH NYATA PENURUNAN BERAT BADAN
                </div>
                <div class="content">
                    <div class="item">
                        <div class="image">
        <span> -15 kg
                        </span>
                            <img alt="" src="{{ asset('build/images/frontend/block13-11.jpg') }}">
                        </div>
                        <p class="label"> BERAT BERLEBIH HILANG! SAYA TERKEJUT!
                        </p>
                        <p class="duration">
                            PERIODE PENGGUNAAN: 3 BULAN
                        </p>
                        <p class="result">
        <span class="before">
                          SEBELUM: 74 kg
                        </span>
                            <span class="after"> SESUDAH: 59 kg
                        </span>
                        </p>
                        <p class="name"> Anditya M, 29:
                        </p>
                        <p class="story">
                            Selama hidup saya mungkin sudah mencoba ratusan cara diet dan tidak ada yang berhasil. Berat saya selalu stabil -74 kg. Kadang saya merasa kalau minum air saja berat badan saya naik! Untungnya, rekan saya merekomendasikan untuk mencoba Garcinia Cambogia Forte. Saya kagum dengan hasilnya! Hanya perlu waktu 3 minggu untuk turun 10 kg dan 5 kg sisanya turun tidak lama setelahnya. Saya tidak henti-hentinya mengagumi tubuh baru saya! Tambahan lagi - beratnya "tidak kembali lagi".
                        </p>
                    </div>
                    <div class="item">
                        <div class="image">
        <span> -26 kg
                        </span>
                            <img alt="" src="{{ asset('build/images/frontend/block13-22.png') }}">
                        </div>
                        <p class="label"> PENURUNAN BERAT LUAR BIASA... 26 kg!
                        </p>
                        <p class="duration"> PERIODE PENGGUNAAN: 6 BULAN
                        </p>
                        <p class="result">
        <span class="before ">
                          SEBELUM 78 kg
                        </span>
                            <span class="after">
                          SESUDAH 52 kg
                        </span>
                        </p>
                        <p class="name ">
                            Sinta H, 31
                        </p>
                        <p class="story">
                            Saya bahkan tidak sadar bagaimana berat saya bisa sampai 78 kg. Saya putuskan untuk berubah. Sayangnya, semua diet yang saya lakukan hasilnya mengecewakan. Semuanya tidak berguna! Lalu rekan saya memberitahu saya tentang Garcinia Cambogia Forte. Sekarang saya merekomendasikannya kepada siapa saja karena saya telah merasakan langsung keampuhannya. Akhirnya berat badan saya turun! Berat saya turun 10 kg dalam bulan pertama dan perlahan-lahan sisanya terus turun. Kini kesehatan saya jauh membaik dan saya menjadi sangat bugar dibanding sebelumnya!
                        </p>
                    </div>
                    <div class="item">
                        <div class="image">
        <span> -21 kg
                        </span>
                            <img alt="" src="{{ asset('build/images/frontend/block13-33.jpg') }}">
                        </div>
                        <p class="label">
                            SIAPA LAGI YANG BERNIAT TURUN 21 kg?
                        </p>
                        <p class="duration"> PERIODE PENGGUNAAN: 2 BULAN
                        </p>
                        <p class="result">
        <span class="before">
                          SEBELUM: 64 kg
                        </span>
                            <span class="after">
                          SESUDAH: 43 kg
                        </span>
                        </p>
                        <p class="name">
                            AMEL A, 20:
                        </p>
                        <p class="story">
                            Saya ingin memiliki bentuk tubuh impian dan sudah mencoba segala macam metode modern: diet, pil, teh, krim, dan mesin olahraga. Saya berhasil menurunkan beberapa kilogram berat badan namun kemudian berat badan saya naik lagi dengan cepat. Garcinia Cambogia Forte membuat saya kagum dengan keampuhannya! Hanya dalam beberapa bulan konsumsi penampilan saja meningkat dengan signifikan: kulit muka saya menjadi kencang, pinggang semakin ramping, dan semua selulit hilang! Saya lalu mencoba minum kopi ini tanpa diet, tenyata tetap berhasil!
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block14">
            <div class="limit">
                <div class="content1">
                    <p class="title1 ttu light">
                        TURUNKAN BERAT BERLEBIH
                    </p>
                    <p class="title2 ttu light"> DENGAN Garcinia Cambogia Forte MUDAH DAN MENYENANGKAN!
                    </p>
                    <p class="description"> Semua yang Anda butuhkan untuk penurunan berat badan dengan cepat
                    </p>
                </div>
                <div class="content2">
                    <p class="title1"> 99% YANG MENGUJICOBA Garcinia Cambogia Forte MENGAKUI KEAMPUHANNYA
                    </p>
                    <p class="title2"> RAHASIA PENURUNAN BERAT JENNIFER LOVE HEWITT
                    </p>
                    <img alt="" src="{{ asset('build/images/frontend/block14-jenny.png') }}">
                    <div class="story">Akhir-akhir ini, aktris Hollywood ternama, Jennifer Love Hewitt muncul kembali di hadapan publik setelah lama tidak muncul. Dia membuat banyak kejutan: setelah melahirkan anak keduanya, Jennifer berhasil menurunkan semua berat kehamilannya dan menjadi langsing kembali! Sebelumnya, Daily Mail melaporkan bahwa selama beberapa tahun terakhir, Jennifer telah mengikuti diet ketat secara konsisten, tetapi harus berhenti sejenak selama masa kehamilannya. Aktris ini bahkan bercanda dan mengatakan bahwa tubuhnya menjadi “sebesar gajah”. Tiga tahun setelah kelahiran putri pertamanya, Love Hewitt kembali cantik seperti sebelumnya. Bahkan, media mencurigai bahwa aktris ini memiliki anoreksia. Tetapi, dia menghapus semua kecurigaan itu dan tertawa, “Apa menurut kalian anoreksia atau diet hanyalah satu-satunya cara untuk menurunkan berat? Aku tidak sakit dan aku tidak melakukan diet ketat apa pun. Aku terlalu sibuk untuk melakukannya.” Rahasia penurunan berat Jennifer yang cepat telah diungkap oleh ahli nutrisi pribadinya: olahraga rutin, diet sehat yang seimbang, dan konsumsi pembakar lemak yang berasal dari tumbuhan.
                    </div>
                </div>
                <div class="content3">
                    <p class="title"> KAMI DIREKOMENDASIKAN OLEH
                    </p>
                    <div class="row content3-img" >
                        <img alt="" src="{{ asset('build/images/frontend/block14-recommend1.png') }}" style="margin: 0 5px">
                        <img alt="" src="{{ asset('build/images/frontend/block14-recommend2.png') }}" style="margin: 0 5px">
                        <img alt="" src="{{ asset('build/images/frontend/block14-recommend3.png') }}" style="margin: 0 5px">
                    </div>
                </div>
            </div>
        </div>
        <div class="warning">
            <div class="limit">
                <p>Hati-Hati dengan produk palsu yang di jual di website lain karena produk asli hanya dari website kami dan call center kami. kami tidak bertanggung jawab atas produk palsu yag di beli d pasaran atau website lain</p>
            </div>
        </div>
        <div class="block block15">
            <div class="limit clearfix">
                <div class="content form-bottom">
                    <div class="title1 ttu">PESAN SEKARANG JUGA</div>
                    <div id="request_form_hash" class="title2 ttu">Garcinia Cambogia Forte</div>

                        {!! form_start($form, ['id' => 'request-form"', 'class' => 'form']) !!}

                        {!! form_errors($form->first_name) !!}
                        {{--{!! form_errors($form->last_name) !!}--}}
                        {!! form_errors($form->phone) !!}

                        <div class="form-group">
                            {!! form_widget($form->first_name) !!}
                        </div>
                        {{--<div class="form-group">--}}
                            {{--{!! form_widget($form->last_name) !!}--}}
                        {{--</div>--}}
                        <div class="form-group">
                            {!! form_widget($form->phone) !!}
                        </div>
                        <div class="price">
                            <s>{{ StrHelper::spaceInPrice($product1->price_old)}} {{$product1->currency}}</s>
                            <span>HARGA BARU: {{ StrHelper::spaceInPrice($product1->price)}} {{$product1->currency}}</span>
                        </div>
                        {!! form_widget($form->productId, ['value' => $product1->id]) !!}
                        <div class="form-group">
                            <button class="ttu btn btn-primary btn-block js_submit" type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();">PESAN</button>
                        </div>
                        {!! form_end($form, false) !!}

                </div>
            </div>
        </div>
    </div>

    <div class="ac_footer">
        <span>© 2017 Copyright. All rights reserved.</span><br>
        <!--        <a href="#request_form_hash">Privacy policy</a> | <a href="#request_form_hash">Report</a>-->
    </div>

@endsection
