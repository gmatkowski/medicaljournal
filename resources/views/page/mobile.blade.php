<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Krebs Mobile</title>
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
    <script src="{{ asset('build/ext/mobile/scripts.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('build/ext/mobile/style.css') }}">
    @include('part.analytics')
</head>
<body>
<header>
    <a href="#" class="logo">
        <img src="{{ asset('build/ext/mobile/img/logo.png') }}" alt="">
    </a>
</header>
<main>
    <div class="top">
        <h1>Do you want speak English in 30 days?</h1>

        <p>Metoda Emila Krebsa pozwoli Ci zacząć mówić komunikatywnie w języku obcym już w 10 dni! Zadziw swoimi
            umiejętnościami znajomych, rodzinę a nawet obcokrajowców! Metoda Krebsa była doskonalona od ponad 40 lat.
            Każda lekcja była opracowywana naukowo tak, aby materiał pozostał na stałe w Twojej głowie - już po jednym
            odsłuchaniu. Wystarczy, że usiądziesz wygodnie i zaczniesz słuchać. Pozwól, aby kurs wykonał całą pracę za
            Ciebie. Wchłoniesz język, jak gąbka wodę! Bez książek, bez żmudnego powtarzania, bez wysiłku!</p>

        <div id="back-to-top" class="back-to-top" style=""><a href="#back-to-top"></a></div>
    </div>
    <div class="trial">
        {!! form_start($form) !!}
        <input type="hidden" value="{{$english->id}}" name="language" />
        {!! form_widget($form->type) !!}
        <ul>
            <li>
                {!! form_label($form->name) !!}
                {!! form_widget($form->name,['attr' => ['placeholder' => false]]) !!}
            </li>
            <li>
                {!! form_label($form->email) !!}
                {!! form_widget($form->email,['attr' => ['placeholder' => false]]) !!}
            </li>
            <li>
                {!! form_label($form->phone) !!}
                {!! form_widget($form->phone,['attr' => ['placeholder' => false]]) !!}
            </li>
            <li>
                <input type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" class="btn" value="TRY TRIAL LESSON FOR FREE" />
            </li>

        </ul>
        {!! form_end($form, $renderRest = false) !!}
        {{--<form method="POST" action="{{ route('page.try.send') }}" accept-charset="UTF-8" class="modal-form">
            <ul>
                <li>
                    <label for="">Type your name</label><input required="true" minlength="3" name="name" type="text"></li>
                <li><label for="">Type your phone number</label><input required="true" minlength="9" name="phone" type="text"></li>
                <li><label for="">Type your email</label><input required="true"  name="email" type="email"></li>
                <li>
                    <input type="submit" class="btn" value="TRY TRIAL LESSON FOR FREE" />
                </li>
            </ul>
        </form>--}}
    </div>
    <div class="boxes">
        <h2>Jak wygląda pełny kurs?</h2>

        <div class="box"><img src="{{ asset('build/ext/mobile/img/ico_headphones.png') }}" alt="" class="ico">
            <h4>40 lekcji audio</h4>

            <p>Nauka polega na totalnej imersji - zanurzeniu się w języku, otoczeniu się nim. Słuchanie i powtarzanie
                konkretnych zdań w określonych odstępach czasu przynosi niebywałe efekty. Krebs wykorzystuje tricki
                pamięci krótkiej i dzięki interwałom pozwala zapamiętać szybko całe wyrażenia czy słowa. Tak jak dziecko
                uczy się przy pomocy naturalnych mechanizmów - automatycznych i nieświadomych</p>
        </div>
        <div class="box"><img src="{{ asset('build/ext/mobile/img/ico_users.png') }}" alt="" class="ico">
            <h4>Działa na każdego</h4>

            <p>Nauka polega na totalnej imersji - zanurzeniu się w języku, otoczeniu się nim. Słuchanie i powtarzanie
                konkretnych zdań w określonych odstępach czasu przynosi niebywałe efekty. Krebs wykorzystuje tricki
                pamięci krótkiej i dzięki interwałom pozwala zapamiętać szybko całe wyrażenia czy słowa. Tak jak dziecko
                uczy się przy pomocy naturalnych mechanizmów - automatycznych i nieświadomych</p>
        </div>
        <div class="box"><img src="{{ asset('build/ext/mobile/img/ico_callendar.png') }}" alt="" class="ico">
            <h4>Po 30 dniach będziesz:</h4>
            <ul>
                <li>- Komunikował się płynniej w typowych sytuacjach życiowych.</li>
                <li>- Wyrazisz swoje emocje, oczekiwania, porozmawiasz a nie wyrecytujesz formułkę z podręcznika.</li>
                <li>- Rozumiał dużo lepiej to, co słyszysz i czytasz.</li>
                <li>- Portafił efektywnie rozwijać swoje kompetencje językowe.</li>
                <li>- Zaskakująco szybko zapamiętywał nowe słówka nawet do kilku języków naraz.</li>
            </ul>
        </div>
    </div>
    <div class="about">
        <h2>Kim był Emil Krebs?</h2>
        <img src="{{ asset('build/ext/mobile/img/face.png') }}" alt="" class="face">

        <p>Emil Krebs był wyjątkowym geniuszem - w ciągu swojego życia władał biegle 68 językami, zarówno w mowie, jak i
            piśmie. Ten niemiecki sinolog, urodzony w 1867 roku na Dolnym Śląski w Świebodzicach, od wczesnej młodości
            przejawiał nieprzeciętne zainteresowanie nauką języków obcych - zdając maturę znał ich już 12. Niektóre
            opanował zupełnie samodzielnie, dalego wykraczał poza ówczesny program szkolny i wymagania nauczycieli.
            Potrafił uczyć się kilku języków równolegle.</p>
    </div>
</main>
<footer>
    <p class="copyright">© 2015 krebsmethod.com. All rights reserved.</p>
</footer>
</body>
</html>