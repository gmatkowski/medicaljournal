@extends('layout.app')
@section('title') :: {{ trans('menu.languages') }} @endsection
@section('top')
	<header class="with-slider">
		@include('part.top')
	</header>
@endsection
@section('content')

	<main>
		<section class="banner single" style="background-image: url('{{ asset("build/images/eng-bg.png") }}');">
			<div class="container">
				<h1 class="wow bounceIn single"> {{ trans('languages.choose.language') }}</h1>

				<div id="slider" class="owl-carousel wow fadeIn">
					@foreach($languages as $language)
						<div class="item" icon="{{ $language->icon_path }}">
							<table>
								<tbody>
								<tr>
									<td class="photo"><img src="{{ $language->image_path }}" alt=""></td>
									<td class="list">
										<h2>{{ $language->name }}</h2>
										{!! $language->description_short !!}
									</td>
									<td class="price">
					                <span class="price">
						                @if($isPromotion)
							                <strong>
								                <span>{{ $language->currency }}</span> {{ number_format($language->price,0,',','.') }}
							                </strong>
							                <p class="old">
								                <span>{{ $language->currency }}</span>
								                {{ number_format($language->price_old,0,',','.') }}
							                </p>
						                @else
							                <strong>
								                <span>{{ $language->currency }}</span> {{ number_format($language->price_old,0,',','.') }}
							                </strong>
						                @endif
					                </span>
									</td>
									@if($isPromotion)
										<td class="timer">
							                <span class="timer-wrapper">
							                  <h3>{{ trans('welcome.slider.promition.header') }}</h3>
							                  <table class="counter">
								                  <tbody>
								                  <tr class="counter">
									                  <td colspan="3">
										                  <div class="timer-wrapper-time"></div>
									                  </td>
								                  </tr>
								                  <tr class="timestamp">
									                  <td colspan="3">
										                  <span>{{ trans('welcome.slider.promition.header.hours') }}</span>
										                  <span>{{ trans('welcome.slider.promition.header.minutes') }}</span>
										                  <span>{{ trans('welcome.slider.promition.header.seconds') }}</span>
									                  </td>
								                  </tr>
								                  </tbody>
							                  </table>
							                </span>
										</td>
									@endif
								</tr>
								</tbody>
							</table>
							<ul class="actions">
								@if($language->is_active)
									<li>
										<a href="#" class="trigger btn btn-blue" type="button" data-toggle="modal" data-target="#{{ !Session::has('try')?'try':'test-lesson' }}">{{ trans('welcome.slider.try.lesson') }}</a>
									</li>
									<li>
										<a href="#buy" data-action="buy-languages-multi-button" data-id="{{ $language->id }}" class="btn btn-green">{{ trans('welcome.slider.order') }}</a>
									</li>
								@else
									<li>
										<a href="#" class="trigger btn btn-blue" type="button" data-toggle="modal" data-target="#inactive">{{ trans('welcome.slider.try.lesson') }}</a>
									</li>
									<li>
										<a href="#" class="btn btn-green" type="button" data-target="#inactive" data-toggle="modal">{{ trans('welcome.slider.order') }}</a>
									</li>
								@endif
							</ul>
						</div>
					@endforeach

				</div>
				<ul class="slider-thumbs"></ul>
			</div>
		</section>

		<section class="faq">
			<div class="container">

				<div class="col-md-offset-2 col-md-8">
					<h2>FAQ</h2>

					<ul class="ico-list">
						@foreach($faqs as $key => $faq)
							{{--
							@if($key + 1 == count($faqs) && $pages->find(23))
								<h2>{!! $pages->find(23)->content !!}</h2>
							@endif
							--}}
							<li>
				            <span class="img">
				              <img src="{{ $faq->icon_path }}" alt="ico" class="img-responsive"/>
				            </span>

								<div class="content">
									<h3>{{ $faq->name }}</h3>

									<p>{!! $faq->description !!}</p>
								</div>
							</li>
						@endforeach
					</ul>

				</div>
			</div>
		</section>
		{{--
		<section class="about-lang">
			<div class="container">
				<div class="row">
					<div class="col-md-offset-2 col-md-8">
						<h2>{{ trans('languages.packet.contains') }}</h2>
						<ul class="info">
							<li class="count">{{ trans('languages.lessons.count') }}</li>
							<li class="records">{{ trans('languages.records.count') }}</li>
							<li class="lvl">{{ trans('languages.levels') }}</li>
							<li class="format">{{ trans('languages.forms.download') }}</li>
						</ul>
						<div class="more">
							<div class="row">

								@if($pages->find(18))
									<div class="col-md-12">
										<h3>{{ $pages->find(18)->title }}</h3>
										{!! $pages->find(18)->content !!}
									</div>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		--}}
		<section class="order">
			<div class="container">
				<div class="row">
					<div class="col-md-offset-2 col-md-8">
						{{--
						<ul class="list-inline actions">
							<li>
								<a href="#" class="trigger" type="button" data-toggle="modal" data-target="#{{ !Session::has('try')?'try':'test-lesson' }}">{{ trans('languages.lesson.try') }}</a>
							</li>
							<li>
								<a href="" data-action="buy-languages-show-order-form-button" class="btn btn-red show-order-form active">{{ trans('languages.buy') }}
									<i class="fa fa-caret-down"></i> </a>
							</li>
						</ul>
						--}}
						<a name="buy"></a>

						{!! form_start($buyForm,['class' => 'buy-form active','data-container' => 'buy-languages-form']) !!}
						<div id="order-form">
{{--
							<div class="order-buttons">

								<div class="col-xs-12 text-center col-sm-4">
									<label for="transfer" class="active">
										<img src="{{ asset("build/images/bank.png") }}" alt="bank"/>
										<input checked type="radio" id="transfer" value="transfer" name="payment"/>
									</label>
								</div>
								<div class="col-xs-12 text-center col-sm-4">
									<label for="cc"> <img src="{{ asset("build/images/kartu.png") }}" alt="bank"/>
										<input type="radio" id="cc" value="cc" name="payment"/> </label>
								</div>

								<div class="col-xs-12 text-center col-sm-4">
									<label for="cod"> <img src="{{ asset("build/images/bayar.png") }}" alt="bank"/>
										<input type="radio" id="cod" value="cod" name="payment"/> </label>
								</div>
								<div class="clearfix"></div>
							</div>
--}}

							<div class="clearfix"></div>

							<div class="col-xs-12 col-sm-6 col-sm-offset-3">
								<h3>{{ trans('languages.payment.contact') }}</h3>

								<div class="info">
									{!! form_widget($buyForm->name,['attr' => ['class' => '']]) !!}
									{!! form_widget($buyForm->email,['attr' => ['class' => '']]) !!}
									{!! form_widget($buyForm->phone,['attr' => ['class' => '']]) !!}
									<ul class="language-choose" style="display:none;">
										@foreach($languages as $language)
											@if($language->is_active)
												<li>
													{!! Form::checkbox('languages[]',$language->id,isset($lang) && $lang->id == $language->id,['id' => 'lang-'.$language->id.'-package-a']) !!}
													<label for="lang-{{ $language->id }}-package-a">
														<img src="{{ $language->icon_path }}" alt="{{ $language->name_short }}"/>
													</label>
												</li>
											@endif
										@endforeach
									</ul>

								</div>

							</div>
							<div class="clearfix"></div>

							<div class="col-xs-12 text-center terms">
								<div class="form-group">
									{!! form_widget($buyForm->terms) !!}
									<label for="terms" class="control-label"><a style="color: #E64445;" href="{{ route('page.terms') }}" target="_blank">{{ trans('languages.accept.terms') }}</a></label>
								</div>
							</div>

							<div class="col-xs-12">
								<div class="action">
									{!! form_widget($buyForm->submit,['attr' => ['class' => '']]) !!}
								</div>
							</div>

						</div>
						{!! form_end($buyForm, false) !!}
					</div>
				</div>
			</div>
		</section>

		@include('part.fb_comments')

		@include('part.footer_small')
	</main>

@endsection