@extends('layout.terobosan',[
    'title' => 'Terobosan Dalam Memerangi Keriput'
])

@section('analytics')
    @include('ga.terobosan')
    @include('pixel.garcinia_segment')
@endsection

@section('content')

    <div class="container">
        <h1>
            <b>3 ilmuwan memenangkan Nobel Kimia atas penemuannya tahun 2009, sebagai terobosan dalam memerangi keriput</b>
            <p class="date-tag">12 December, 2016</p>
        </h1>
    </div>
    <img src="{{ asset('build/images/frontend/awards.jpg') }}" style="width:100%" alt="">

    <div class="caption container">
        <div class="col-md-12">
            <strong style="display:inline-block;padding-left:20px;">V. Ramakrishnan</strong> mendapat Hadiah Nobel untuk penemuannya, sebagai terobosan dalam memerangi keriput
        </div>
    </div>

    {{--<div style="float:left;">--}}
        {{--<a href="http://331nq.voluumtrk.com/click/1">--}}
            {{--<img src="{{ asset('build/images/frontend/lefery-box2.jpg') }}" style="margin-left:20px; margin-right:20px; width:200px; display:block;" alt="">--}}
        {{--</a>--}}
        {{--<div style="text-align:center; font-size:20px;">--}}
            {{--<b>Klik dan Coba</b>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="container main-wrapper" style="padding-top:30px;padding-bottom:30px;">
        <div class="col-md-8">
            <p class="top">Hasil uji klinis, yang dilakukan oleh Prof. Romain Lefery, membuktikan bahwa ada formula, yang membantu untuk menangani keriput muka, kulit yang tidak kenyal, kendur dan kering. <b>Dengan formula ini, wajah dapat kelihatan lebih sehat, <span class="yellowbg">hingga 15 tahun lebih muda.</span></b></p>
            <img src="{{ asset('build/images/frontend/pobrane.jpg') }}" class="pull-left img-thumbnail" style="margin-right:20px;" alt="">
            <p>Sebelum kita masuk lebih detail, di sini ada beberapa fakta mengenai terobosan medis tersebut. Kulit wanita di usia 65-an, setelah aplikasi formula tersebut, dari sudut pandang kedokteran, mulai terlihat seperti kulit wanita di usia 50-an. Lalu, dari sudut pandang kedokteran, kulit wanita di usia 55-an, setelah apikasi formula baru ini, <b>terlihat seperti kulit wanita muda.</b> Akibatnya, usia biologis kulit perempuan 30-35 tahun kembali ke waktu masa lalu, saat-saat mereka paling cantik.</p>
            <p>Memang, terlihat sedikit aneh pada pandangan pertama, terlalu optimis… Tetapi sering kita dengar sebuah pengetahuan baru secara cepat menjadi pengetahuan usang. Jika ada penemuan terbaru dalam ilmu kimia, yang dihargai dengan Nobel, tentang <span class="yellowbg">cara memerangi keriput dan membalikkan proses penuaan, ini jelas harus diperhitungkan.</span></p>
            <p><b>(Laporan langsung dari New York – Anette Yorin)</b> Berlangsung konferensi pers yang mengungkapkan bagaimana profesor Amerika berhasil mengembalikan vitalitas kulit dan melawan usia yang semakin tua. Rahasia yang tersembunyi adalah pendekatan yang baru terhadap masalah penuaan kulit. Berkat penemuan tiga ilmuwan, pemenang Nobel Kimia pada tahun 2009, yaitu V. Ramakrishnan, T. Steitz, A. Yonath, Prof. Romain Lefery menemukan <a href="http://331nq.voluumtrk.com/click/1">solusi untuk memanjakan dan meremajakan kulit secara revolusioner</a>.</p>
            <p><b>Formula ini sudah diuji secara laboratorium di universitas asal, pemeriksaan medis dan tes kosmetik.</b> Uji klinis tersebut telah membuktikan bahwa kita mampu untuk memutar kembali waktu dan mengembalikan peremajaan kulit yang hilang, hanya dalam PERHATIAN: 15-30 hari. Dan peristiwa itu terjadi di depan mata para wartawan yang berkumpul saat konferensi pers - <span class="yellowbg">seorang wanita menjadi lebih muda hanya dalam 5 menit terapi.</span></p>
            <p>Setelah beberapa kata pendahuluan, yang membahas mengenai uji klinis pada perempuan berusia 55 tahun, Prof. Romain Lefery mengidentifikasi beberapa masalah yang dihadapi perempuan berusia 30 hingga 65 tahun. Yang paling penting adalah: <b>keriput di dahi, keriput di sekitar mata dan di sekitar bibir, kerutan di wajah, kulit lembek dan kering, kerutan yang tersisa di sekitar dagu.</b> Lalu dia menunjukkan gambar-gambar klinis mendokumentasikan penemuan luar biasa ini.</p>
            <h3>Efek akan menjadi luar biasa</h3>
            <p>Acara dimulai dengan presentasi campuran dalam stoples, halus dan putih. Profesor mengoleskan campuran ini pada wajah Ibu Jane Smith secara merata, dengan menepuk perlahan diseluruh wajahnya. Formula ini mulai perlahan-lahan mengering dan kemudian menyerap dengan baik di kulit. Pada saat itu Profesor menunjukkan gambar-gambar yang diambil selama proses uji klinis. Kami juga menyaksikan bagaimana setelah 5 menit, kerutan-kerutan dahi Jane Smith hilang, dan <b>wajahnya menjadi lebih cerah dan berseri seperti wajah bayi.</b> Kemudian kami melihat, bagaimana lipatan nasolabial, juga disebut sebagai kurung wajah atau alur bibir-pipi, segera menghilang. Keriput sekitar mata juga menghilang sampai tipis, dan semua itu terjadi di depan mata kami.</p>
            <p>Kulit Jane Smith menjadi lebih lembut untuk disentuh, dari sebelumnya. Ketika ia menatapnya, kami bisa melihat bagian kulit dia - kendur dan keriput sebelumnya - halus dan lembut setelah terapi. Ini bukan akhir cerita. Kami semakin takjub melihat bagaimana setelah menerapkan campuran, <span class="yellowbg">kulitnya menjadi lebih kencang</span>, dia mulai terlihat menarik, seperti wanita berusia 35 tahun. Pada akhir acara, kulit wajah Ibu Jane Smith terlihat kencang, sehat, muda dan berkilau, hingga orang lain tidak bisa percaya bahwa wajah dia rusak, lelah dan kering sebelumnya. Jadi penemuan luar biasa ini merubah Jane Smith, dari wanita berusia 55 tahun, yang kurang menarik, ke wanita berusia 35 tahun, yang sangat menarik. Ini dia bukti, sekarang kami mengetahui bahwa penemuan luar biasa itu benar.</p>
            <h3>Profesor menjelaskan</h3>
            <p>Bagi kebanyakan orang, proses transformasi yang luar biasa ini, tampak seperti keajaiban. Tapi ternyata tidak bagi para peneliti. Para peneliti menyebut fenomena itu bisa dijelaskan dalam penelitian, dan penelitian ini didasarkan pada kenyataan, oleh para dokter dan ilmuwan, yang memenangkan Nobel kimia, yang menemukan hal tersebut dalam struktur kulit DNA. Prof. Lefery melanjutkan: <b>usia otot-otot akan menjadi lebih tipis dan memanjang</b>, dan kelenjar-kelenjar pada kulit mulai menyusut ukurannya, sehingga tampak perubahan kondisi dan kontur wajah. Semakin menua kulit akan menipis dan kehilangan elastisitasnya. Ada beberapa alasan yang menyebabkan kulit kehilangan elastisitasnya, termasuk karena proses penuaan. Ada beberapa faktor yang mempengaruhi proses penuaan, dan tanda-tanda pertama penuaan ini muncul di sekitar mata.</p>
            <p><b>Apa yang membuat sel-sel kulit menjadi rusak?</b> Kurang hidrasi? Menurut perusahaan-perusahaan kosmetik besar, air adalah faktor terpenting dalam menjaga kondisi kelembaban kulit. Tapi jika benar, cukup sabun dan air untuk membuat kita selamanya muda. Pada kenyataannya, kita kehilangan sesuatu yang sama pentingnya, yaitu <span class="yellowbg">bahan-bahan nutrisi utama struktur DNA</span>, yang bertindak pada tingkat sel. Setelah usia 30 tahun kulit berhenti memproduksi bahan-bahan nutrisi tersebut. Dengan usia kulit kita mengalami kehilangan nutrisi yang sangat penting. Dengan kata lain - kulit kita mengalami kekurangan nutrisi sampai mati.</p>
            <h3>Apakah manusia dapat mengubah alam?</h3>
            <p>Prof. Romain Lefery menjelaskan lebih lanjut: bagaimana kalau obat membuktikan bahwa cara yang baru ditemukan dan ditulis, yaitu <a href="http://331nq.voluumtrk.com/click/1">nutrisi struktur DNA</a>, dapat memperbaiki kerusakan kulit, yang disebabkan oleh berlalunya waktu? Bagaimana kalau kulit kita memang bisa <b>mendapatkan kembali penampilan mudanya?</b> Dunia ilmu telah membuktikan bahwa bahan-bahan alami struktur nutrisi DNA dapat mengatasi kulit kering, melembabkan dan menghaluskan kulit wajah dan mendapatkan kembali keindahan alam Anda. Penemuan baru itu akan menutrisi wajah Anda dan menghilangkan keriput dalam waktu yang sangat singkat.</p>
            <p>Ya, telah ditemukan! Penelitian ilmiah berhasil mengungkapkan hal ini, dan hasil ini telah dikonfirmasi dalam uji klinis oleh para ilmuwan. <span class="yellowbg">Cukup aplikasikan struktur nutrisi DNA</span> secara langsung pada kulit wajah yang mengalami penuaan, untuk mendapatkan kembali penampilan mudanya dalam waktu yang sangat singkat. <b>Kerutan atau keriput di wajah, dahi, dan bawah mata menghilang SECARA PERMANEN!</b></p>
            <div class="penulis">
                <div class="col-md-12" style="margin:20px 0;background:#cccccc;padding:15px">
                    <img src="{{ asset('build/images/frontend/lefery.jpg') }}" style="margin-right:20px; width:180px;" class="pull-left" alt="">
                    <h4>Tentang Prof. Romain Lefery</h4>
                    <p>Dia lulus dari Medicine School of Houston. Prof. Romain Lefery adalah mantan anggota United States Hospital in Marsylia dan International Dermatological Clinic in New York, sekarang dia anggota United States Dermal Institute. Dia adalah penulis dan turut menulis banyak publikasi ilmiah tentang segala macam gangguan pada kulit. Dia tinggal dan bekerja di Dallas.</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <center><a href="http://331nq.voluumtrk.com/click/1"><img src="{{ asset('build/images/frontend/lefery-box.jpg') }}" style="width:200px;" alt=""></a></center>
            <p style="font-size:23px;text-align:center;">Promo khusus ini berlaku bagi para pembaca situs portal kami: dengan mengklik pada link di bawah untuk berpartisipasi dalam program promo dan mendapatkan Lefery ACR dengan diskon 50% lebih murah! Penawaran khusus ini hanya berlaku sampai akhir hari ini.</p>
            <center><a href="http://331nq.voluumtrk.com/click/1" class="btn btn-lg btn-warning mobilebutton">Klik di sini untuk mendapatkan formula anti-age Prof. Lefery <br> untuk nutrisi struktur kulit DNA</a></center>
        </div>
        <div class="col-md-4" id="right-side">
            <h3>Berita yang paling dibaca:</h3>
            <ul>
                <li><a href="http://331nq.voluumtrk.com/click/1">4 Manfaat hebat di balik warna-warni paprika</a></li>
                <li><a href="http://331nq.voluumtrk.com/click/1">Bareskrim bongkar kasus vaksin palsu, 5 orang ditangkap</a></li>
                <li><a href="http://331nq.voluumtrk.com/click/1">Lupa sarapan bikin bau mulut?</a></li>
                <li><a href="http://331nq.voluumtrk.com/click/1">5 Cara agar berpuasa dapat dijalani dengan lebih mudah!</a></li>
                <li><a href="http://331nq.voluumtrk.com/click/1">Joe Allen akan segera pulang ke Swansea</a></li>
            </ul>
            <h3 style="margin-top:40px;">Komentar</h3>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo01.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Yanie Rifky</h4>
                    Sudah ada yang mencoba?
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo02.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Erlina Sari</h4>
                    Aku menggunakan selama 3 minggu dan terlihat 5 tahun lebih muda, ya! Semua teman-teman saya terheran-heran, bahkan suami saya! Oke, saya tidak bisa terlihat seperti anak, tapi efek luar biasa. keriput aku sekitar mata sdh hilang, bagus sekali! Terima kasih atas artikel ini!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo03.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Meira Setiawati</h4>
                    Dengan formula anti-age ini saya pertama kali mulai terlihat lebih mudah! sekarang saya merasa cantik dan pintar, dengan formula ini saya bisa percaya diri
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo04.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Liliana Mustika</h4>
                    Apakah sudah ada yang mencoba? Saya tertarik.
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo05.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Mira Muliastuti</h4>
                    Sudah sejak lama saya ingin sekali terlihat lebih muda, dan menghilangkan keriput dibawah mata secara alami, tetapi tanpa berhasil. Saya kehilangan banyak uang untuk krem anti-ageing, serum, dll... Akhirnya cara untuk terlihat lebih mudah, alami dan aman, terima kasih! Formula itu lebih murah daripada salon kecantikan dll. Dalam beberapa minggu ada peristiwa yg penting dalam keluarga saya - perkawinan adik saya. Oleh karena itu, saya sdh beli krim intu, sehingga nanti terlihat cantik. Baiklah!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo06.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Issy Yuliasari</h4>
                    Barusan saya baca artikel di laman ini, tadi pagi saya menggunakan terapi ini di salon kecantikan, tapi di sana dengan harga 4 juta! di laman ini jauh lebih murah, jadi sudah beli 2 krim ini, karena kesempatan bagus dengan harga itu. Saya ngak tahu, ada formula itu di toko online, saya tadi pikir ada hanya di salon kecantikan, bagus!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo07.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Ibu Suriait</h4>
                    Luar biasa saja! Saya ingin sekali formula ini, mau mencoba! aku tak dapat menunggu untuk mulai menggunakannya!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo08.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Lennij Widiarti</h4>
                    Bagus, jadi kalo cara ini bekerja, saya akan beli krim itu untuk istri saya, sebagai kado ulang tahun untuknya. Saya harap dia suka ini dan jangan marah kpd saya ;)
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo09.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Ida Tita</h4>
                    Aku sdh 1 minggu guna Lefery, saya terlihat dan merasa cantik. Kerutan saya sdh mulai hilang, dan itu murah, harganya bagus! kalo mau pergi ke salon kecantikan atau SPA ini mahal! mahal sekali dan suami saya marah, dan sekarang, dengan formula ini- santai :)
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo10.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Ning Farid</h4>
                    Ku sdh beli terapi ini utk Ibu saya dan dia senang sekali, dan menurut ku dia terlihat sekarang jauh lebih bagus. Krim ini sangat eksklusif dan efektif dengan harga baik
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo15.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Muhammad Herlin</h4>
                    aneh juga... maksud ku, setiap orang akan menjadi tua
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo11.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Nurul Pratiwi</h4>
                    takhayul itu - jelas, setiap orang akan menjadi tua, tapi ini adalah cara memperlambat proses penuaan kulit, dan terapi ini sangat efektif, saya senang dengan metode ini
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo13.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">andikha dikha</h4>
                    Tadi malam saya dan istri saya mencoba terapi itu, umur saya sudah 50 tahun, istri saya sama. dan kalau produk itu aman itu bagus, sudah di uji di laboratorium, hasil uji lab, ya- jadi semuanya beres.
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo12.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Rara Devi</h4>
                    barusan temanku kasih tahu tentang metode ini, dan sekarang saya baca artikel ini, menarik! ya, saya tertarik!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo14.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Rosyidatul Khoiroh</h4>
                    Aku pertama kali menggunakan, terapi itu setiap 2 hari, dan efeknya sangat menarik, kerutan saya sudah hilang, wajah saya lebih kencang. BENAR! saya akan beli sekarang 2
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo19.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Dwita Siregar</h4>
                    Saya sudah lama menggunakan terapi ini, setiap 2 hari, dan krim sudah habis! efek terapi ini luar biasa saja, Maksud saya kan beli satu kali lagi, tapi sekarang ku beli 2!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo20.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Sari Faizah</h4>
                    Saya dan adik saya sdh beli krim itu beberapa minggu lalu. Efeknya? luar biasa! wajah saya segar dan cantik, terlihat lebih muda, saya senang dng terapi itu, dan adik saya juga. krim ini yg paling bagus, gak ada lain!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo18.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Lisna Wati</h4>
                    menarik... apakah efeknya itu benar?
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo21.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Ige Azmin</h4>
                    Saya mendengar di Amerika ada terapi seperti ini, yang luar biasa, jadi sekarang ada di negara kita, bagus, saya tidak perlu pergi ke Amerika, tapi sekarang saya tahu treapi ini bisa dibeli di Indonesia, baik, saya mau:)
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo22.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">fany kiki</h4>
                    Dulu saya tinggal di ibu kota, saya sering pergi ke salon kecantikan di jaktim, tapi itu mahal sekali, dan sekarang saya tinggal di desa, dan tidak ada salon kecantikan, jadi ini adalah solusi yang bagus bagi saya, dan murah. saya akan mencoba dan beritahu nanti, mengenai efeknya:)
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo23.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">rany konkmar ciedi</h4>
                    TOLONG TULIS SESUATU YA!
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo16.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Ida Puspita</h4>
                    Klik disini, ada iklan, ya, dan membaca saja, ya.
                </div>
            </div>
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo17.jpg') }}" alt="...">
                    </a>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Rini Handayaningsih</h4>
                    Oke saya sdh beli. Ku beritahu nanti gimana efeknya
                </div>
            </div>
            <a href="http://331nq.voluumtrk.com/click/1" class="btn btn-success btn-sm center-block" style="margin:20px;">Muat 659 komentar lainnya</a>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection