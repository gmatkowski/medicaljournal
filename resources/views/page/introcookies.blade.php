<p><b>KEBIJAKAN PRIVASI – TEKNOLOGI COOKIE</b></p>

<p>1. Kami menggunakan teknologi “cookie” pada situs kami untuk melayani Anda dengan lebih cepat dan dengan kualitas lebih baik. Kami juga menggunakan teknologi “cookie” untuk menganalisis frekuensi dan waktu kunjung Anda ke website kami dan memahami preferensi dari para pengguna website kami demi meningkatkan dan menyesuaikan isi website kami.</p>

<p>2. Selain menggunakan cookies, kami tidak mengumpulkan informasi lain apapun secara otamatis.</p>

<p>3. Kami menggunakan cookies untuk tujuan-tujuan berikut:</p>

a) Memahami dan menyimpan preferensi pengguna untuk masa depan kunjungan.
b) Memahami preferensi dari para pengguna website kami demi meningkatkan dan menyesuaikan isi website kami.
c) Menganalisis frekuensi dan waktu kunjung Anda ke website kami.
d) Kompilasi data agregat tentang lalu lintas situs dan interaksi situs dalam rangka untuk menawarkan penggunaan situs dan aplikasi yang lebih baik di masa depan. Situs juga dapat menggunakan layanan pihak ketiga terpercaya yang melacak informasi ini.

<p>4. Website kami menggunakan cookie jenis berikut ini:</p>

a) Cookie teknis: dibutuhkan agar website kami bisa berjalan dengan seharusnya.
b) Cookie keamanan: untuk meningkatkan pengalaman bagi pengunjung untuk website kami.
c) Cookie analisa: untuk memperoleh wawasan tentang cara pengunjung menggunakan website kami.
d) Cookie fungsional: untuk menyimpan pencarian yang sudah Anda lakukan, dan akomodasi-akomodasi yang sudah Anda lihat sebelumnya. 
e) Cookie komersial: untuk menampilkan iklan di website lain kepada Anda.

<p>5. Anda dapat memilih untuk memblokir cookies dengan mengubah preferensi privasi Anda berkaitan dengan penggunaan cookies di browser Anda. Karena setiap browser berbeda, lihat Menu Bantuan dari browser Anda untuk mempelajari cara yang benar untuk memodifikasi cookies Anda.</p>

<p>6. Namun, Anda dapat mengalami kesulitan dalam menggunakan beberapa layanan jika Anda memblokir cookies.</p>

<p>7. Situs juga dapat menggunakan layanan pihak ketiga terpercaya yang melacak informasi dari cookies tersebut.</p>
