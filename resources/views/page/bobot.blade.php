@extends('layout.mask',[
    'title' => 'Bobot Tubuh Arya Permana Mulai Menyusut'
])

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bmask/all.css') }}" media="all">
@endsection

@section('analytics')
@endsection

@section('content')
    <div class="header-main">
        <a class="logo" href="{{ route('page.maskPreOrder') }}">StarHit</a>
        <div class="header-news">
            <div class="item">
                <a href="http://health.medical-jurnal.com/2016/08/18/tanpa-harus-merasa-lapar">
                    <img alt="" src="{{ asset('build/images/frontend/8-1-696x464.jpg') }}" class="photo">
                    <span class="title">Tanpa Harus Merasa Lapar</span>
                </a>
                <div class="info">Penelitian menunjukkan bahwa...</div>
            </div>
            <div class="item">
                <a href="http://health.medical-jurnal.com/2016/09/27/masker-buah-tomat-untuk-kulit-wajah">
                    <img alt="" src="{{ asset('build/images/frontend/bagaimana.jpg') }}" class="photo">
                    <span class="title">Buah Tomat Untuk Kulit Wajah</span>
                </a>
                <div class="info">Bagaimana cara membuat masker?</div>
            </div>
        </div>
    </div>
    <div class="header-banner">
        <div title="" id="adriver_banner_316775540"></div>
    </div>
    <div class="wrap">
        <div class="content">

            <div class="primary">
                <div class="main">
                    <div class="crumbs">
                        <a href="http://health.medical-jurnal.com" class="crumbs-main">Homepage</a>  /
                        <a href="http://health.medical-jurnal.com" class="crumbs-section">Tips Kesehatan</a>  /
                        <span class="crumbs-section">Alhamdulillah… Bobot Tubuh Arya Permana Mulai Menyusut</span>
                    </div>
                    <h1 class="article-title">Alhamdulillah… Bobot Tubuh Arya Permana Mulai Menyusut</h1>
                    <h2 class="article-announce"> </h2>
                    <h2 class="article-announce"> </h2>
                    <div class="article-social-line">
                        <span class="article-date">2016-10-17 08:00</span>
                    </div>
                    <div class="article-container">
                        {{--<div class="article-side">--}}
                            {{--<div class="block article-new-block">--}}
                                {{--<div class="title"><span>Bamboo Charcoal Black Mask</span></div>--}}
                                {{--<ul class="list">--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">100% komposisi alami</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Vitamin-vitamin 8 kali lebih kuat</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Jaminan kepuasan</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Efektivitas maksimal</a>--}}
                                    {{--</li>--}}
                                    {{--<li class="item">--}}
                                        {{--<a href="{{ route('page.maskPreOrder') }}">Tanpa efek samping</a>--}}
                                    {{--</li>--}}
                                {{--</ul>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="article-body">
                            <div class="figure">

                            </div><br>

                            <p></p><center><img src="{{ asset('build/images/frontend/bobot1.jpg') }}" width="600" alt=""></center><p></p>

                            <p><b>Bobot tubuh Arya Permana (10) yang obesitas mengalami perubahan. Dengan <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a>, sejak dirawat dan ditangani tim dokter Rumah Sakit Hasan Sadikin (RSHS) di Bandung, 4 November lalu, bobot bocah terberat di dunia mengalami penyusutan.</b></p>

                            <p></p><center><img src="{{ asset('build/images/frontend/bobot2.jpg') }}" width="600" alt=""></center><p></p>

                            <p><b>Sekretaris tim dokter yang menangani Arya, dr Noviana Andriani mengatakan, setelah ditimbang, berat badan Arya mengalami penurunan menjadi 186,8 kilogram. Sebelumnya, bobot Arya mencapai 190,5 kilogram. Setelah pemakaian obat baru <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a>, Arya Permana berhasil turun  3,7 kg hanya dalam 4 hari. Berikut ini wawancara dengan Dr Noviana Andriani.</b></p>

                            <p><img src="{{ asset('build/images/frontend/bobot3.jpg') }}" width="350" alt="" align="left" style="margin:10px;">

                            <p><b>Redaksi:</b> Arya Permana sudah mengerti apa keinginannya mau berubah sangat besar karena dampak dari kita semua termasuk masuk ke televisi. Kayaknya semangat menurunkan berat badan cukup besar. Mengapa menurut para ahli kegemukan menjadi masalah kesehatan masyarakat?</p>

                            <b>Dr Noviana Andriani:</b> Saya sudah lama bekerja sebagai sekretaris tim dokter di Rumah Sakit Hasan Sadikin di Bandung. Sebagai dokter ahli saya selalu dekat dan berkomunikasi dengan pasien kami. Kebanyakan orang tidak menyadari bahwa obesitas atau kegemukan merupakan masalah besar. Penelitian telah menunjukkan bahwa 71% orang di seluruh dunia menderita kegemukan. Menurut para ahli dari Organisasi Kesehatan Dunia (WHO) kita menghadapi epidemi obesitas atau kegemukan. Epidemi obesitas di seluruh dunia mencapai rekor baru. Bagi sebagian pasien resiko obesitas atau kegemukan memang menakutkan. </p>

                            <p><b>Redaksi:</b> Arya Permana, bocah obesitas asal Desa Cipurwasari di Jawa Barat, hari ini kembali menjalani pemeriksaan di Rumah Sakit Hasan Sadikin. Bagaimana kondisinya sekarang?</p>

                            <p><b>Dr Noviana Andriani:</b> Kondisi Arya saat ini baik dan lebih tenang. Selama empat hari dirawat berat badannya mengalami penurunan hingga 3,7 kilogram. Sekarang dia bisa menjalani seluruh program yang sudah dirancang tim dokter dengan baik. </p>

                            <p><b>Redaksi:</b> Yaitu?</p>

                            <p><b>Dr Noviana Andriani:</b> Alhamdulillah Arya sudah aktif, mulai mau berjalan-jalan serta olahraga, tapi dia berhasil turun 3,7 kg dalam 4 hari tanpa olahraga, tanpa diet apapun. Sulit dipercaya, tapi memang ada.</p>

                            <p><b>Redaksi:</b> Itu mustahil... Bagaimana caranya?</p>

                            <p><b>Dr Noviana Andriani:</b>  Ini bukan sihir, ini hasil nyata. Dia mengambil 1 kapsul <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> saja, sebelum makan sarapan: 1 kali sehari. <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> membuat tubuh dia membakar lemak dengan cepat, memblokir proses pengendapan lemak dan mengurangi kadar kolesterol jahat.</p>

                            <p></p><center><img src="{{ asset('build/images/frontend/bobot4.jpg') }}" width="600" alt=""></center><p></p>

                            <p><b>Redaksi:</b> Ajaib. Bagaimana dengan diet ketat?</p>

                            <p><b>Dr Noviana Andriani:</b> Alhamdulillah dengan <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a>, Arya tak perlu diet ketat, Arya tak perlu olahraga, karena <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> membuat tubuh dia langsing dan membakar lemak secara otomatis.</p>

                            <p><b>Redaksi:</b> Sangat menarik. Apakah itu benar?</p>

                            <p><b>Dr Noviana Andriani:</b> Saya seorang ilmuwan dan saya selalu percaya pada data empiris dan hasil percobaan. Ini bukan sihir: dengan Garcinia Gambogia Forte setiap orang bisa turun 8 kg dalam 15 hari. Hasilnya telah terbukti secara ilmiah.</p>

                            <p><b>Redaksi:</b> Bagaimana komposisinya?</p>

                            <p><b>Dr Noviana Andriani:</b>  Komposisinya sangat aman: ekstrak alami <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> dari buah Malabar Tamarind yang memiliki sifat menurunkan berat badan. Selain itu, <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> juga memiliki beberapa manfaat lain: menjaga kesehatan kondisi tubuh, meningkatkan kinerja fisik, membantu untuk mengurangi kolesterol tinggi, dan lain-lain.</p>

                            <p></p><center><img src="{{ asset('build/images/frontend/bobot5.jpg') }}" width="600" alt=""></center><p></p>

                            <p><b>Redaksi:</b> Apakah program pengobatan dengan <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> itu akan terus dijalankan?</p>

                            <p><b>RDr Noviana Andriani:</b> Ya, program itu akan terus dijalankan selama Arya dirawat di RSHS. Kami harap program ini bisa dijalankan dengan baik, sehingga bobot tubuh Arya bisa diturunkan secara bertahap sesuai harapan kita semua.</p>

                            <p><b>Redaksi:</b> Ada pertanyaan dari para pembaca kami: apakah cara menurunkan berat badan denga cepat tersedia untuk orang rata-rata? Pasti mahal?</p>

                            <p><b>Dr Noviana Andriani:</b> Tidak, murah, dan dibandingkan dengan cara lain, sangat murah. Kita semua tahu bahwa menghemat waktu sama pentingnya menghemat uang. Dan <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> yang asli sekarang tersedia untuk orang rata-rata juga. Memang, pada saat ini tersedia hanya di toko online, cara memesannya – melalui halaman resmi.</p>

                            <p><b>Redaksi:</b> Terima kasih atas wawancara ini dan semoga sukses dalam pekerjaan.</p>

                            <p><b>Dr Noviana Andriani:</b> Sama-sama. Saya ingin agar orang Indonesia mengerti bahwa sekarang mereka juga dapat menurunkan berat badan secara aman dan cepat, tanpa diet ketat yang sering berbahaya bagi kesehatan. Terapi dengan <a href="{{ route('page.preorder') }}">Garcinia Cambogia Forte</a> dan hasilnya telah terbukti secara ilmiah dan tersedia untuk orang rata-rata. Jadi saya ulangi, ini bukan sihir, ini hasil nyata: rahasia menurunkan berat badan telah ditemukan, dan kami harap program pengobatan untuk Arya Permana bisa dijalankan dengan baik.</p>

                            <p class="clear"></p>

                            <a href="{{ route('page.preorder') }}" class="button" style="display:block!important; text-align:center;">BELI Garcinia Cambogia Forte</a>

                            <div class="comments-block">
                                <div class="title">Komentar-komentar</div>
                                <div class="container">
                                    <ul class="list">

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/15.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Surya Wati</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Arya Permana semoga sukses!!!</p></div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/14.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">pratiwi22</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ya itu benar kegemukan berbahaya.. barusan temanku kata tentang metode ini, dan sekarang aku baca artikel ini, aku tertarik karena aku udh coba turun beberapa kali.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/17.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">sylvi kurniawati</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>artikel sangat menarik saya juga mau turun karena merasa malu, kadag2 takut keluar dari rumah saya. masalah saya kegemukan dan kadar gula tinggi.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/7.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Vellia Fatimah</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>MAU MENGATASI RASA LAPAR, GIMANA CARANYA??? mau turun 5 kg..setelah melahirkan perut saya buncit</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/11.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">dewi</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>saya makan 3 kali makanan utama perhari dan tanpa makanan ringan. Tanpa obat!</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Widia Resdiana</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>@dewi: ya tapi ada yg perlu obat..suamiku berusia 51 dia udah mengubah diet dan kami mencari suplemen yg baik tapi di apotik ada hanya obat mahal</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/10.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Elok Puspitasari Satoto</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>aku sdh pesan obat ini, karena murah dan baru aku mau coba cara sederhana </p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/18.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rizki Jakarta</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>saya berhasil turun dengan obat itu setelah 2 minggu pemakaian, dan kadar kolesterol jahat turun, bagus sekali tanpa olahraga tanpa diet apapun!</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/d7.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Siska</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>gue tertarik krn gue udh bebrapa kali coba beberapa obat dn selalu mengalami efek yo-yo, ada yg udh coba? Gue mau turun dn cepat</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/b8.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Aminatun123</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>mau!! hilangkan lemak di perut dan paha dng cepat. Tak suka diet ketat:( harganya berapa? Ada di toko kota medan??</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/19.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">hudayani</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>SEMOGA SUKSES BRO!! harus coba ya </p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/9.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">NUR35</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>@Aminatun123: baca aja ya pada saat ini obat ini dijual melalui halaman produsen resmi! aku tertarik karena udah lama cari cara ampuh untuk turunkan berat, cara alami.. sulit sekali ya udah coba beberapa obat lain, tapi gagal tunggu cara yg baik</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/21.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">amir</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>hasilnya terlihat setelah pemakaian pertama? Mau coba</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ route('page.maskPreOrder') }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/12.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Suriyati39</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>lumayan ya .. ku mau cara sehat untuk mengatasi kegemukan</p>
                                                </div>
                                            </div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <center><a href="{{ route('page.preorder') }}" class="button">BELI Garcinia Cambogia Forte</a></center>
                        </div>
                    </div>
                </div>
                <a href="{{ route('page.maskPreOrder') }}">
                </a><div class="right">
                    <div class="right-cell dontMiss">

                        {{--<table cols="1" style="margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr></tr>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: 1px solid rgb(109, 172, 216); padding: 0px;" align="left" valign="top">--}}
                        {{--<div class="abouttext">--}}
                        {{--<center></center>--}}
                        {{--</div>--}}
                        {{--<center>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 2px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v1.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Саня</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Хех. Неплохо выглядит Салтыкова)))--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--только что--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v2.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Валера</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Не думал, что ей уже 50 )--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--32 минуты назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v3.jpg') }}" width="50px">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Ниночка Ростина</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--У меня с лицом все впорядке, но знакомым покажу, хватает у меня друзей с прыщиками.--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--54 минуты назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v4.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Виктор Аников</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Брат в Америке живет, так вот говорит что там действительно ОЧЕНЬ популярен эта черная маска...--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v5.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Катёна Шантурова</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Впечатляет! Я бы тоже такую маску прикупила. Хочу лицо в порядок привести--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v6.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder') }}">Ирина Лонгина</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Заказа себе небольшой курс, буду пробовать)--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}

                        {{--</center>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}

                    </div>
                    <div class="block banner-side-mid">
                    </div>
                </div><div class="clear"></div>
            </div>
        </div>
        <div class="header">
            <div class="header-menu">
                <ul>
                    <li class="item novosti">
                        <a href="http://health.medical-jurnal.com"><span>HOMEPAGE</span></a>
                    </li>
                    <li class="item eksklusiv active">
                        <div><a href="http://health.medical-jurnal.com"><span>FITNESS & OLAHRAGA</span></a></div>
                    </li>
                    <li class="item interview">
                        <a href="http://health.medical-jurnal.com"><span>TIPS KESEHATAN</span></a>
                    </li>
                    <li class="item photoistorii">
                        <a href="http://health.medical-jurnal.com"><span>MAKANAN & DIET SEHAT</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection