@extends('layout.app',[
    'title' => 'Garcinia Cambogia'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/garcinia1/all.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/pre/all.css') }}">
@endsection

@section('chat')@endsection

@section('pixel')
    @include('pixel.cambogia_main')
@endsection

@section('content')

    <div class="widget visible">
        <div class="widgetWrap"><p class="sale-text"> diskon <span>50%</span></p>

            <div class="countdown"><span id="countdown-text">periode diskon terbatas:</span> <span id="hours">00</span>:<span id="min">46</span>:<span id="sec">46</span></div>
            <a href="#buyForm" class="btn red">untuk memesan</a></div>
    </div>
    @desktop
        <br><br><br>
    @enddesktop
    <div id="wrapper">
        <div id="post">
            <h1 class="a1">Garansi Kepuasan <br> Turun berat 10 kg hanya dalam 1 bulan</h1>

            <div class="box_1">
                <img src="{{ asset('build/images/frontend/user13-min.jpg') }}" alt="">
                <div class="info">
                    <h3>”Pasti Berubah 100%”</h3>
                    <p>"Dari 70 kg menjadi 51 kg, saya bisa menurunkan berat hingga 19 kg. Saya tetap makan seperti biasa. Sangat luar biasa!"</p>
                    <p class="author">
                        <strong>Jindaratn Kongsan</strong><br>
                        <i></i>
                    </p>
                </div>
            </div>
            <img style="float:right!important;margin:32px 0 0 10px;width:126px;" src="{{ asset('build/images/frontend/specialist_cqplus_600_s-min.jpg') }}">
            <p>Halo</p>
            <p>Nama saya Robert, dan saya seorang ahli gizi. Saya sudah berpengalaman lebih dari 15 tahun dalam bidang penurunan berat badan.</p>
            <p>Sepanjang masa penelitian, saya sudah membantu lebih dari 2000 orang dengan kelebihan berat badan. Pengalaman mereka mengajarkan saya untuk tidak menyepelekan masalah obesitas. </p>
            <p>Buat saya, menurunkan berat hingga 10 kg merupakan hal yang susah. Lebih susah dari yang dibayangkan siapa pun.</p>
            <p>Jadi, apa yang harus kita lakukan? Bagaimana caranya menurunkan kelebihan berat ini?</p>
            <p>Faktanya adalah, untuk bisa melawan obesitas, Anda harus melakukan lebih dari sekadar diet dan mengontrol makanan. Selain berolahraga, Anda juga memerlukan sesuatu yang lebih untuk membantu Anda, seperti senjata yang ampuh untuk melawan kelebihan lemak di tubuh namun harus aman dan efektif.</p>
            <p>Saya mencoba beragam cara, dan ternyata ada satu cara yang bisa mengurangi berat sampai 2 kg dalam seminggu pertama.
                Saya juga bertemu dengan orang yang bisa menurunkan berat 1,5 kg dalam 48 jam saja. Dan banyak yang bisa menurunkan hingga 5 kg dalam 2 minggu.</p>
            <p>Beberapa dari orang-orang yang mengalami obesitas ini juga ada yang turun beratnya hingga <b>20,30,50 kg</b> bahkan lebih.</p>

            <p>Ini berarti, masalah obesitas bisa teratasi dengan mudah :</p>
            <ul class="list_belt">
                <li>Ukuran pinggang turun 15 cm</li>
                <li>Ukuran perut bagian bawah turun 12 cm</li>
                <li>Ukuran pantat turun 10 cm</li>
                <li>Ukuran paha turun 7 cm</li>
                <li>Ukuran pinggul turun 12 cm</li>
            </ul>

            <p>Hasil ini benar-benar terjadi. Dari penemuan saya, cara ini bekerja dalam tiga tingkatan:</p>

            <h3 class="h_benefit">Menghambat Rasa Lapar</h3>
            <p class="p_benefit" style="border-left: 3px solid #dcdcdc; padding-left: 20px; margin-left: 30px;">Anda tidak bisa menurunkan berat dengan diet. Rasa lapar akan menghancurkan diet Anda cepat atau lambat. Dengan ini, Anda tidak akan mudah merasa lapar lagi. Nafsu makan akan berkurang dan meningkatkan proses metabolisme.</p>

            <h3 class="h_benefit">Menambah Energi</h3>
            <p class="p_benefit" style="border-left: 3px solid #dcdcdc; padding-left: 20px; margin-left: 30px;">Selama beberapa hari, tingkatan energi dalam tubuh Anda akan semakin meningkat. Akan terus seperti itu selama 10 hari kemudian tingkatan energi akan konstan dan Anda akan memiliki 80% energy lebih banyak dari biasanya.</p>

            <h3 class="h_benefit">Berat Badan Otomatis Berkurang!</h3>
            <p class="p_benefit" style="border-left: 3px solid #dcdcdc; padding-left: 20px; margin-left: 30px;">Mulai dari saat Anda ikut program ini, berat badan akan otomatis berkurang. Anda tidak perlu lagi khawatir dengan berat berlebih, tanpa rasa lapar, sampai Anda sendiri lupa sedang berusaha menurunkan berat badan.</p>
            <p>Bahan apakah yang terkandung di dalamnya? Mengandung ekstrak Garcinia Cambogia dari buah Malabar Tamarind terbaik.</p>
            <p>Apa yang membuat produk ini lebih unggul dari yang lainnya dan membuat pasien saya bisa menurunkan berat dengan mudah?</p>
            <p>Kandungan bahannya membantu menurunkan nafsu makan serta membuat tidak ingin ngemil. Sangat penting untuk tidak ngemil selama Anda menurunkan berat badan. Selain itu, produk ini dapat membantu meningkatkan sistem metabolisme. Meghancurkan lemak yang menumpuk di hati serta meluruhkan racun-racun di dalam tubuh Anda.</p>
            <p>Sebagai tambahan, kandungan ekstrak Garcinia Cambogia ini sangat efektif bekerja langsung untuk menurunkan berat badan. Garcinia Cambogia bekerja efektif pada metobolisme untuk mengendalikan protein. Karbohidrat dan lemak, serta membantu menyeimbangan pembentukan energi dalam tubuh. Dengan mengubah lemak menjadi energi, maka Anda bisa menurunkan berat badan secara cepat dan mudah. Dan kandungan tinggi ekstrak Garcinia Cambogia ini juga mampu melawan radikal bebas yang bisa menyebabkan kerutan di kulit serta terjadinya penimbunan lemak.</p>
            <p>Hasil penelitian saya, untuk, menurunkan berat secara efektif, Anda memerlukan produk dengan kandungan Garcinia Cambogia aktif dan antioksidan.</p>
            <p>Saya menemukan pemilik usaha yang memproduksi kapsul Garcinia Cambogia dengan manfaat maksimal. Garcinia Cambogia Forte, mengandung ekstrak Garcinia Cambogia dari buah Malabar Tamarind, mampu mengontrol pembakaran lemak dalam tubuh. </p>
            <p>Garcinia Cambogia membuat penurunan berat badan menjadi:</p>

            <h3 class="h_benefit">MUDAH</h3>
            <p class="p_benefit" style="border-left: 3px solid #dcdcdc; padding-left: 20px; margin-left: 30px;">Sekarang Anda bisa tahu cara langsing tanpa harus diet ketat, tanpa harus latihan keras, bahkan Anda tidak mengubah pola makanan Anda. Dengan hanya 1 kapsul per hari, Anda akan melihat penurunan berat badan terlihat, dalam beberapa hari.</p>

            <h3 class="h_benefit">ALAMI DAN AMAN</h3>
            <p class="p_benefit" style="border-left: 3px solid #dcdcdc; padding-left: 20px; margin-left: 30px;">Ekstrak alami {{$product1->name}} dari buah Malabar Tamarind, yang berasal dari India, memiliki sifat menurunkan berat badan. Selain itu, {{$product1->name}} juga memiliki beberapa manfaat lain: menjaga kesehatan kondisi tubuh, meningkatkan kinerja fisik, membantu untuk mengurangi kolesterol tinggi, mengatur tekanan darah, memaksimalkan penyerapan nutrisi dan vitamin dalam tubuh Anda.</p>

            <p>นIni barangkali adalah cara yang paling efektif untuk menurunkan berat badan dengan cara sehat dan hasilnya tahan lama, serta membuat tubuh menjadi indah dan ramping. {{$product1->name}} adalah produk asli yang telah lulus uji laboratorium, yang efektivitasnya telah dikonfirmasi oleh ratusan orang yang telah menggunakan {{$product1->name}}.</p>

            <div class="box_1">
                <img src="{{ asset('build/images/frontend/user14-min.jpg') }}" alt="">
                <div class="info">
                    <h3>Dari Bahan Alami</h3>
                    <p>Banyak cara menurunkan berat yang malah membuat saya kelelahan dan tidak bertenaga. Tapi yang ini berbeda, seperti kehilangan berat secara alami saja. Saya merasa lebih ringan dan sehat dalam waktu yang sama. Saya berhasil menurunkan hingga 24 kg dan Nampak 10 tahun lebih muda dari biasanya.</p>
                    <p class="author">
                        <strong>Nurfitria Dwisyanto</strong><br>
                        <i>Turun 24 kg dalam 2 bulan</i>
                    </p>
                </div>
            </div>

            <p>Harap diperhatikan, jika metode ini sama seperti banyak metode lain yang mungkin memiliki hasil yang berbeda untuk setiap orang. Beberapa orang memiliki kelainan bawaan yang mungkin tidak akan berhasil menggunakan metode ini. Akan tetapi perusahan sangat senang bisa menjamin kesuksesan produknya pada banyak orang.</p>

            <h3>Garansi Unik</h3>
            <p style="text-align: center;font-size:18px;">Kami memberikan Garansi Kepuasan untuk pelanggan dan menjamin hasil maksimal pada setiap pemakaian.</p>

            <h3>Bagaimana cara mendapatkan Garcinia Cambogia Forte dengan harga murah?</h3>
            <p>Kualitas terbaik ekstrak {{$product1->name}} sulit untuk mendapat, jadi harganya tidak bisa murah. Namun, produsen telah memberikan diskon khusus bagi pelanggan yang pertama kali melakukan pembelian {{$product1->name}} Forte. Anda cukup memesan hari ini, maka Anda akan mendapatkan diskon spesial -57%, untuk alami dan kemurnian tinggi ekstrak {{$product1->name}} dalam kapsul.</p>
            <p>Hari ini ada promosi: kemasan {{$product1->name}} Forte asli HANYA HARI INI dijual dengan harga  490 000 IDR saja.</p>
            <p>Besok harga akan naik!</p>

            <p>Anda cukup klik pada tombol order dan mengisi formulir online dengan benar. {{$product1->name}} Forte akan kami kirimkan ke alamat Anda dalam waktu maks. 2 hari.</p>

            <p>Secara pribadi, saya yakin bahwa Anda akan sepenuhnya puas. Sebernanya saya berasumsi Anda akan menurunkan berat badan lebih cepat dari yang Anda bayangkan.</p>

            <p style="text-align: right;">Semoga Sukses! <br> Robert Anderson</p>

            <p style="text-align: center">Kemasan asli, <br> murni ekstrak {{$product1->name}} 95%</p>
            <div id="bottle">
                <img src="{{ asset('build/images/frontend/cqplus_600_small3d-min.png') }}" style="max-height: 190px;">
                <strong><center><span style="text-decoration: line-through;">{!! StrHelper::spaceInPrice($product1->price_old) !!} {{ $product1->currency }}</span> {!! StrHelper::spaceInPrice($product1->price) !!} {{ $product1->currency }}</center></strong>
                <div id="bottle_g"><img src="{{ asset('build/images/frontend/th_satisfaction.png') }}" width="80" height="80"></div>
            </div>
            <p style="font-size: 16px;  text-align: center; line-height: 140%;">Anda akan menghemat uang Anda jika membayar pada hari ini: mendapatkan diskon langsung sebesar {{ $product1->currency }} {!! StrHelper::spaceInPrice($product1->price, $product1->price_old) !!}, pengiriman gratis dan Jaminan Kepuasan. Hanya hari ini!</p>
            <br>
            @include('page.garcinia_order_form', [
                'form' => $form,
                'header1' => 'Tinggalkan nomor telepon anda,consultant kami akan segera menghubungi anda dan memberikan konsultasi gratis untuk anda.',
                'header2' => 'Pengiriman Garcinia Cambogia Forte dilakukan melalui kurir atau pos, dalam beberapa hari kedepan, secara gratis. Metode pembayaran: COD (Pembayaran di Tempat).',
                'productId' => $product1->id,
                'productName' => $product1->display_name
            ])
            {{--<center><a href="#" id="tocart">BELI SEKARANG</a></center>--}}
            <br>
            <br>
            <p><strong>PS :</strong> Jangan Lupa, Promo hanya berlaku untuk Hari ini saja </p>
            <br>

            <div id="footer" style="margin-top:10px;">
                <a onclick="window.open('privacy-policy', 'windowname1', 'width=670, height=765, scrollbars=1'); return false;" href="javascript: void(0)">Kebijakan Privasi</a>
            </div>
            <p style="margin-top: 20px; padding-top: 0; color: #aaa; max-width: 700px; font-size: 11px; text-align: center;   line-height: normal;">Produk ini adalah suplemen diet, bukan obat untuk pengobatan penyakit, suplemen makanan tidak boleh digunakan sebagai pengganti makanan yang bervariasi dan seimbang. Sebuah diet yang bervariasi dan seimbang dan gaya hidup sehat yang dianjurkan. Paket tersebut berisi 15 kapsul dan cukup untuk 2 minggu terapi.</p>
            <p style="margin-top: 0; padding-top: 0; color: #aaa; max-width: 700px; font-size: 11px; text-align: center; line-height: normal;">Untuk menyediakan layanan yang efisien, dan kepentingan keselamatan pemilik website ini berhak untuk melakukan perubahan terhadap Kebijakan Privasi. Tindakan ini bertujuan untuk menjaga anonimitas pelanggan-pelanggan. Untuk meningkatkan kepuasan dengan layanan yang diberikan kepada pelanggan-pelanggan, namanya telah diubah demi keamanan dan privasi. Setiap kemiripan dengan orang-orang yang nyata adalah murni kebetulan.</p>
        </div>
        <div id="sidebar">
            <ul class="testimonials">
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user01-min.jpg') }}" alt="">
                    <h3>Berat Turun 12 kg, Pinggang menyusut 8 cm dan paha mengecil 3 cm</h3>
                    <p>Garcinia Cambogia Forte wajib dicoba. Setelah 1 bulan pemakaian, berat saya turun 12 kg dan saya menjadi lebih ramping.</p>
                    <p class="author"><b>Wichian Boonprasop</b><br> Berat Turun <u> 12 kg dalam 1 bulan </u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user02-min.jpg') }}" alt="">
                    <h3>Produk yang merngubah hidup!</h3>
                    <p>Obesitas = tidak punya kehidupan sosial. Saya berhasil turun 25 kg dan semuanya berubah. Bentuk tubuh indah saya yang sekarang sudah membantu saya mendapatkan banyak teman baru dan meningkatkan rasa percaya diri.</p>
                    <p class="author"><b> Nisha Thapthong </b> <br> Turun<u> 25 kg dalam 3 bulan </u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user03-min.jpg') }}" alt="">
                    <h3>Perut Buncit Hilang, Penurun Berat yang Luar Biasa!</h3>
                    <p>Cara kerjanya dengan menurunkan nafsu makan. Saya tidak merasakan apapun saat berat mulai turun 5, 10, 20 kg sampai akhirnya saya berhasil turun 23 kg. saya sangat puas.</p>
                    <p class="author"><b>Thanawat Gan Gandhar</b> <br>Turun<u> 23 kg dalam 3 bulan </u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user04-min.jpg') }}" alt="">
                    <h3>Hasil yang di luar dugaan!</h3>
                    <p>Anda hanya perlu minum satu kapsul sebelum sarapan dan sebelum makan malam. Setelah itu, tidak perlu lagi khawatir dengan apapun. Anda hanya tinggal menunggu dan lihat hasilnya yang mengagumkan.</p>
                    <p class="author"><b>Tiara Maharany </b><br>Turun <u>17 kg dalam 2 bulan</u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user05-min.jpg') }}" alt="">
                    <h3>Berat Saya Turun dalam Sekejap Mata!</h3>
                    <p>Pada saat bulan pertama saya berhasil turun 10 kg, kemudian bulan kedua saya turun lagi 7 kg. ini adalah produk pelangsing terbaik yang pernah saya temukan!</p>
                    <p class="author"><b>Chulapum Pumma</b><br>Turun <u>17 kg dalam 2 bulan </u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user06-min.jpg') }}" alt="">
                    <h3>Berat Saya Turun!</h3>
                    <p>Seandainya saya tidak pernah mencoba Garcinia Cambogia Forte, merupakan suatu kesalahan besar karena dengan ini saya berhasil turun hingga 16 kg. saya merasa sangat bugar. Bersemangat dan bernergi!</p>
                    <p class="author"><b>Suppranee Choosakul</b><br>Turun <u>16 kg dalam 2 bulan </u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user07-min.jpg') }}" alt="">
                    <h3>Tidak Gemuk Lagi!</h3>
                    <p>นBerat saya turun hingga 15 kg dan tidak naik lagi setelah 6 bulan sekarang. Saya sangat merekomendasikan untuk Anda yang mencari produk aman dan teruji!</p>
                    <p class="author"><b>Narumol Needle Utha</b><br>Turun by <u>15 kg dalam 2 bulan</u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user08-min.jpg') }}" alt="">
                    <h3>Membuat Lemak Menghilang!</h3>
                    <p>Beratku turu 3-4 kg dalam satu minggu, Garcinia Cambogia Forte ini betul-betul mengenyahkan lemak di tubuhku! Kapsul ini betul-betul ajaib!</p>
                    <p class="author"><b>Dimas Aldrian	</b><br>Berat Turun <u>15 kg dalam 2 bulan</u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user09-min.jpg') }}" alt="">
                    <h3>Betul-betul Sukses!</h3>
                    <p>Sebelum saya menemukan Garcinia Cambogia Forte, saya selalu gagal dalam menurunkan berat badan. Dengan ini, hanya dalam 2 bulan saya berhasil menurunkan 20 kg dan akhirnya bisa mewujudkan impian saya.</p>
                    <p class="author"><b>Araya Sukpan </b><br>Berat turun <u>20 kg dalam 2 bulan</u></p>
                </li>
                <li class="opinion">
                    <img src="{{ asset('build/images/frontend/user10-min.jpg') }}" alt="">
                    <h3>Saya Berhasil Menurunkan Berat Setelah Melahirkan!</h3>
                    <p>Saya mengikuti saran teman karena saya ingin bisa menurunkan berat seperti teman saya itu. Entah dengan diet ketat atau olahraga sudah saya coba. Tapi ternyata menggunakan Garcinia Cambogia Forte yang membuat saya bisa menurunkan berat yang naik saat sedang hamil hingga 19 kg. </p>
                    <p class="author"><b>Eli Wahyu Subekti</b><br>Turun <u>19 kg dalam 2 bulan</u></p>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('endBodyScripts')
    @include('part.countdown')
@endsection