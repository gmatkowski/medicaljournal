<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta name="viewport" content="initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <link rel="stylesheet" href="{{ elixir('css/garciniaCambogia2popup/all.css') }}">
</head>
<body>

    <div id="wrapper">
        <div id="content">
            <h1 class="newfont" style="margin-bottom: 20px;">
                Kesempatan Menarik!				</h1>
            <h2 style="width:220px;margin:0 auto;">
                Berakhir pada:
                <span id="counter" class="red hasCountdown">
                    <span class="countdown_row countdown_amount">
                        <span id="hours"></span>:<span id="min"></span>:<span id="sec"></span>
                    </span>
                </span>
            </h2>
            <p>&nbsp;</p>
            <p style="text-align: center">{{ $product1->name }} <br>untuk berpartisipasi dalam member diskon</p>

            <div id="bottle">

                <img src="{{ asset('build/images/frontend/garciniacambogia.jpg') }}">
                <strong><center><span style="text-decoration: line-through;">{!! StrHelper::spaceInPrice($product1->price_old) !!}&nbsp;{{ $product1->currency }}</span> {!! StrHelper::spaceInPrice($product1->price_spec) !!}&nbsp;{{ $product1->currency }}</center></strong>

            </div>

            <p style="font-size: 16px;  text-align: center; line-height: 140%;"><u>Pesan hari ini </u> dan hemat {!! StrHelper::spaceInPrice($product1->price_spec, $product1->price_old) !!}&nbsp;{{ $product1->currency }}, gratis biaya pengiriman dan kepuasan dijamin. Besok promosi hangus.</p>
            <br><center>
                <a href="{{ route('page.garciniaCambogia3') }}" id="tocart">PESAN SEKARANG</a>
            </center><br>
        </div>
        <div id="footer">
            <a onclick="window.open('privacy-policy', 'windowname1', 'width=670, height=765, scrollbars=1'); return false;" href="javascript: void(0)">Kebijakan Privasi</a>
        </div>
    </div>

    @include('part.countdown')

</body>
</html>