@extends('layout.app',[
    'title' => 'Garcinia Cambogia Forte'
])

@section('styles')
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport"/>

    <link rel="stylesheet" href="{{ elixir('css/garcinia2-mobile/all.css') }}">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,700,700italic" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,500&amp;subset=latin,vietnamese,cyrillic-ext,latin-ext,greek-ext,greek,cyrillic" rel="stylesheet"/>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,300italic,600italic,700,700italic&amp;subset=latin,cyrillic" rel="stylesheet" type="text/css"/>
@endsection

@section('pixel')
    @include('pixel.cambogia_main')
@endsection

@section('content')
    <div class="hideJs">
        <div class="block block1 with-gradient">
            <div class="limit clearfix">
                <a class="logo for-event" >Garcinia Cambogia Forte</a>
                <div class="content">
                    <p class="title1 ttu">TURUNKAN BERAT BADAN BERLEBIH
                    </p>
                    <p class="title2">CEPAT DAN MUDAH DENGAN Garcinia Cambogia Forte
                    </p>
                    <img alt="" src="{{ asset('build/images/frontend/prod.png') }}"/>
                    <a class="btn btn-primary for-event scroll">Pesan sekarang
                    </a>
                    <img alt="" class="body" src="{{ asset('build/images/frontend/block1-1.png') }}" style="left: 0;"/>
                    <div class="list">
                        <span class="ttu">CARA MENURUNKAN BERAT BADAN YANG MUDAH DAN MENYENANGKAN DENGAN Garcinia Cambogia Forte:</span>
                        <ul>
                            <li>Pembakar lemak cepat dan alami
                            </li>
                            <li>Mengurangi nafsu makan secara sehat
                            </li>
                            <li>Penampilan menarik dan tetap sehat
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block2">
            <div class="limit">
                <div class="row">
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-1.jpg') }}"/>
                        <span>Mengecilkan pipi berisi
                          </span>
                    </div>
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-2.jpg') }}"/>
                        <span>Pinggang langsing
                          </span>
                    </div>
                </div>
                <div class="row">
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-3.jpg') }}"/>
                        <span>Panggul ramping
                          </span>
                    </div>
                    <div class="item">
                        <img alt="" src="{{ asset('build/images/frontend/block2-5.jpg') }}"/>
                        <span>Kaki elegan
                          </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block3 with-gradient">
            <div class="limit">
                <p class="title ttu">
                    <span>OBESITAS</span>
                    1950-2010
                </p>
                <div class="graph">
                    <img alt="" src="{{ asset('build/images/frontend/block3-graph.png') }}"/>
                </div>
                <div class="info">
                    Menurut laporan WHO yang dipresentasikan di AS pada Maret 2010, jumlah orang dengan kelebihan berat badan terus meningkat.
                </div>
            </div>
        </div>
        <div class="block block6 with-gradient">
            <div class="limit">
                <p class="title1 ttu">TURUNKAN
                    <br/> BERAT BERLEBIH
                </p>
                <img alt="" src="{{ asset('build/images/frontend/prod.png') }}"/>
                <div class="description">
                    <p>Garcinia Cambogia Forte mengandung konsentrat yang secara aktif menghancurkan timbunan lemak di dalam tubuh. Meningkatkan fungsi metabolisme, detoksifikasi tubuh Anda, dan membuang radikal bebas.
                    </p>
                    <p>Anti oksidan alami menjaga kesehatan dan membuat penampilan menjadi makin menarik
                    </p>
                </div>
                <a class="btn btn-primary for-event scroll">PESAN SEKARANG
                </a>
            </div>
        </div>
        <div class="block block8">
            <div class="dark-title">
                <div class="limit">
                    <div class="table">
                        <span class="ttu label">RAHASIA KEAMPUHAN Garcinia Cambogia Forte</span>
                        <span class="value">Komposisi Garcinia Cambogia Forte: ekstrak dari buah Malabar Tamarind</span>
                    </div>
                </div>
            </div>
            <div class="limit">
                <div class="item">
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-1.jpg') }}"/>
                    </div>
                    <div class="description with-label">
                        <span>KONSENTRASI</span>
                        <p>Tertinggi 95% Garcinia dalam Garcinia Cambogia Forte sudah tersedia di pasaran Indonesia.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-2.jpg') }}"/>
                    </div>
                    <div class="description with-label">
                        <span>JENIS</span>
                        <p>Ekstrak 95% Garcinia Cambogia diambil dari buah Malabar Tamarind, berkualitas dan bernilai tinggi.
                        </p>
                    </div>
                </div>
                <div class="item">
                    <div class="image">
                        <img alt="" src="{{ asset('build/images/frontend/block8-squad1-3.jpg') }}"/>
                    </div>
                    <div class="description with-label">
                        <span>CARA KERJA</span>
                        <p>Meningkatkan asam lambung dan metabolisme tubuh, memecah sel-sel lemak dan menghambat proses pengendapan lemak.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="block block12 with-gradient">
            <div class="limit">

                <div class="form form-bottom">
                    <p class="title1 ttu">PESAN SEKARANG JUGA</p>
                    <p id="request-form-hash" class="title2 ttu">Garcinia Cambogia Forte</p>

                    {!! form_start($form, ['id' => 'request-form"', 'class' => 'form']) !!}
                    {!! form_errors($form->first_name) !!}
                    {{--{!! form_errors($form->last_name) !!}--}}
                    {!! form_errors($form->email) !!}
                    {!! form_errors($form->phone) !!}
                    <div class="fields">
                        <div class="form-group">
                            {!! form_widget($form->first_name) !!}
                        </div>
                        {{--<div class="form-group">--}}
                            {{--{!! form_widget($form->last_name) !!}--}}
                        {{--</div>--}}
                        <div class="form-group">
                            {!! form_widget($form->email) !!}
                        </div>
                        <div class="form-group">
                            {!! form_widget($form->phone) !!}
                        </div>
                        <div class="price">
                            <s>{{ StrHelper::spaceInPrice($product1->price_old) }} {{$product1->currency}}</s>
                            <span>HARGA BARU: {{ StrHelper::spaceInPrice($product1->price) }} {{$product1->currency}}</span>
                        </div>
                        {!! form_widget($form->productId, ['value' => $product1->id]) !!}
                        <div class="form-group buttons">
                            <button class="ttu btn btn-primary btn-block js_submit" type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();">Pesan</button>
                        </div>
                    </div>
                    {!! form_end($form, false) !!}

                </div>
            </div>
        </div>
    </div>

    <div style="text-align:center;">
        <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
    </div>
@endsection

