@extends('layout.app',[
    'title' => 'Cara cepat melangsingkan tubuh secara alami'
])

@section('chat')@endsection

@section('analytics')
	@if(Session::has('ga-test'))
		<!-- Yandex.Metrika counter -->
		<script type="text/javascript">
			(function (d, w, c) {
				(w[c] = w[c] || []).push(function () {
					try {
						w.yaCounter40118460 = new Ya.Metrika({
							id: 40118460,
							clickmap: true,
							trackLinks: true,
							accurateTrackBounce: true,
							webvisor: true
						});
					} catch (e) {
					}
				});
				var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () {
					n.parentNode.insertBefore(s, n);
				};
				s.type = "text/javascript";
				s.async = true;
				s.src = "https://mc.yandex.ru/metrika/watch.js";
				if (w.opera == "[object Opera]") {
					d.addEventListener("DOMContentLoaded", f, false);
				} else {
					f();
				}
			})(document, window, "yandex_metrika_callbacks");
		</script>
		<noscript>
			<div><img src="https://mc.yandex.ru/watch/40118460" style="position:absolute; left:-9999px;" alt=""/></div>
		</noscript>
		<!-- /Yandex.Metrika counter -->

		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
					new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
					j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
					'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-P8K73C');</script>
		<!-- End Google Tag Manager -->
	@else
		@parent
		@include('ga.penemuan')
	@endif
@endsection

@section('content')
	<!-- Google Tag Manager (noscript) -->
	<noscript>
		<iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P8K73C" height="0" width="0" style="display:none;visibility:hidden"></iframe>
	</noscript>
	<!-- End Google Tag Manager (noscript) -->

	<div class="page-container">
		<header id="main-header">
			<div class="data">
				<h1 class="logo">
					<a href="{{ route('page.index') }}"><img src="{{asset('build/images/frontend/fi6WL4S/logo.png')}}" alt="Medic Reporters"></a>
				</h1>
				<div class="button facebook-box">
					<div class="ribbon-3">
						<a href="javascript: void(0)" onclick="return false;">Suka</a>
					</div>
				</div>
				<form method="POST" accept-charset="utf-8">
					<input required="required" type="text" placeholder="Cari..." name="q">
					<button type="submit" name="search_submit"><i class="icons icon-search"></i></button>
				</form>
			</div>
			<button id="toggle-menu">
				<span></span> <span></span> <span></span>
			</button>
			<nav>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">HOME</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">DIET</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">FIT & OLAHRAGA</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">PSIKOLOGI</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">DI RANJANG</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">KECANTIKAN</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">KESEHATAN</a>
			</nav>
		</header>
		
		<ol class="breadcrumb">
			<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}" itemprop="url">
					<span itemprop="title">Home</span> </a>
			</li>
			<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}" itemprop="url">
					<span itemprop="title">Kesehatan</span> </a>
			</li>
			<li itemscope="" itemtype="http://data-vocabulary.org/Breadcrumb">
				<span itemprop="title">Penemuan yang menonaktifkan gen obesitas!</span>
			</li>
		</ol>
		<div class="page-view">
			<div class="left-side">
				<article class="main">
					<header>
						<h1>Penemuan yang menonaktifkan gen obesitas! Mengapa metode memecah sel-sel lemak didiamkan hampir selama 200 tahun?</h1>
						<p>Metode penuruman berat badan yang luar biasa – untuk menurunkan hingga 8 kg lemak dalam 14 hari. Dengan cara ini bisa mengurangi berat badan secara permanen dalam 2 minggu. Mengapa penemuan tahun 1819, penemuan yang begitu inovatif, baru terjadi hari ini? Semua ini sudah jelas: paten seorang ahli kimia Hans Christian Ørste, sangat menarik bagi dokter ahli gizi, penemuan ini luar biasa saja.</p>
					</header>
					<div class="body">
						<h2>Mengapa cara Ørsted berhasil untuk melangsingkan tubuh?</h2>
						<figure>
							<div id="leftphoto">
								<img src="{{asset('build/images/frontend/christian.jpg')}}" alt="Hans Christian Ørsted">
								<figcaption>Hans Christian Ørsted</figcaption>
							</div>
							<div id="rightdesc">
								<p>Hans Christian Ørsted adalah seorang penemu {{$product1->name}}, zat untuk pelangsing tubuh, yang sangat efektif. Dia adalah seorang ahli kimia terkenal dari Denmark. Dia bekerja sebagai apoteker yang berupaya menemukan solusi inovatif. Penemuan {{$product1->name}} ditemukan pertama kali oleh dia. {{$product1->name}} adalah zat yang
									<span class="yellowbg">membakar kalori dengan cepat</span> dan memecah sel-sel lemak secara permanen. Ørsted lama mempelajari efek zat {{$product1->name}} dari buah Malabar Tamarind, yang sekarang terkenal sebagai cara efektif untuk
									<b>memerangi obesitas</b>, di seluruh dunia. {{$product1->name}} adalah zat yang didiamkan selama 200 tahun, zat ini dapat membantu mengurangi berat badan secara cepat – dengan membakar lemak di tubuh, terutama di bagian paha dan perut, terlepas dari berat badan Anda sebelumnya.
								</p>
							</div>
							<p>Dengan cara yang lain, yaitu cara untuk menurunkan berat badan yang tradisional, Anda harus diet ketat, inilah metode yang membutuhkan waktu yang lama, dan Anda sering mengalami sindrom yoyo, yang berbahaya bagi kesehatan Anda. Dengan cara Ørsted, Anda akan berhasil menurunkan berat badan, karena cara ini memecah sel-sel lemak, pada tingkat seluler, jadi Anda tidak perlu motivasi, dan tidak perlu memiliki kemauan yang kuat.</p>
						</figure>
						<h2>Pelangsing badan secara cepat 24 jam sehari</h2>
						<p>Karena {{$product1->name}} meningkatkan metabolisme dan membakar lemak 4 kali lebih cepat, 1 kapsul {{$product1->name}} cukup untuk membakar
							<u>hingga 950 kalori setiap hari</u>. Anda perlu menyadari, bahwa jika pencernaan menjadi lebih efektif (pada usus halus, pankreas dan perut), pengurangan lemak tubuh berlangsung
							<b>secara otomatis dan alami</b>. Anda tidak perlu menderita dengan melakukan program diet ketat, Anda tidak perlu berhenti makan makanan favorit Anda. {{$product1->name}} mengganggu fungsi gen yang membentuk lemak dan mencegah pertumbuhan sel-sel lemak baru, oleh karena itu Anda bisa
							<span class="yellowbg">menurunkan berat badan secara cepat</span>, tanpa perlu melakukan perubahan kebiasaan makan secara radikal. Apakah para ahli gizi sengaja menutup mata pada penemuan ini karena takut bahwa pasar akan didominasi oleh metode {{$product1->name}} yang paling efektif?
						</p>
						<h2>Pendapat orang Indonesia tentang {{$product1->name}} Forte:</h2>
						<p class="italic">"Saya tidak berharap saya akan memiliki sosok ramping dan indah. Dengan {{$product1->name}} Forte mimpi saya menjadi kenyataan. Saya turun 11 kg dalam 3 minggu! Saya senang, tak lagi malu untuk pergi kemana-mana. Sekarang tubuh langsing saya mampu membuat iri banyak wanita."</p>
						<p>Penelitian menunjukkan jelas bahwa untuk menurunkan lemak tubuh, Anda perlu diet yang benar dan hanya {{$product1->name}} 95% dapat membantu Anda. Selama bertahun-tahun seorang penjual suplemen berusaha mengulang sukses ini 200 tahun kemudian. Mereka berusaha untuk mencapai {{$product1->name}} yang sangat tinggi konsentrasinya. Akhirnya mereka berhasil, dan menempatkan produk di pasar. Mereka angkat suara, mulai berbicara tentang
							<u>cara menurunkan berat badan yang sangat efektif</u> – {{$product1->name}} Forte.</p>
						<h2>Wujudkanlah impian Anda untuk memiliki tubuh ideal, dengan mudah!</h2>
						<p>Paha tanpa selulit, perut yang langsing, dan pinggang yang sempit. Lebih dari 27 900 wanita Indonesia sudah wujudkan impian mereka untuk memiliki tubuh langsing. Anda juga akan menggunakan pakaian kecil dan ketat, dan menjadi senang dengan
							<span class="yellowbg">tubuh langsing tanpa lemak</span>.
							<u>Semua teman Anda akan kagum</u> dengan sosok Anda! Mereka tidak akan bisa percaya bahwa Anda mendapatkan sosok langsing kembali hanya dalam 14 hari.
						</p>
						<p>Ini waktu yang tepat untuk mengurus diri sendir – Anda akan melihat refleksi di cermin dengan senang hati, dan
							<b>medapatkan kepercayaan diri</b>. Dengan cara ini Anda akan menjadi menarik dan membuat orang lain tertarik pada Anda, dan tidak akan merasa malu untuk bertemu orang lain. Anda akan terkesan, karena Anda akan mengubah hidup Anda dengan mudah – cukup 2 minggu.
						</p>
						<h2>Apa efek menggunakannya dalam hari-hari pertama?</h2>
						<p>Dengan metode Ørsted, biasanya dalam 3-4 hari pertama Anda akan melihat hasilnya –
							<span class="yellowbg">mengecilkan lingkar perut</span> sekitar 1 cm. Dalam 1 minggu menggunakan {{$product1->name}} Forte, selain menghilangkan selulit, Anda akan mengecilkan perut buncit. Setelah 14 hari menggunakanya, Anda akan senang dan terkesan, karena Anda akan kehilangan berat badan dengan mudah.
						</p>
						<p>Yakin bahwa tubuh langsing membuat Anda lebih percaya diri dan menarik dalam penampilan, membuat Anda merasa seperti wanita. Setelah 2 minggu menggunakan {{$product1->name}} Forte, Anda akan mendapat hasil yang sama dengan diet ketat, misalnya 30 hari Atkins Diet, yaitu - paha ramping, perut yang langsing dan tidak buncit, keberadaan bentuk dan ukuran pinggang lebih dikecilkan. Anda harus ingat, bahwa kami memberikan
							<b>Jaminan Kepuasan</b>. Kalau Anda gagal dalam menurunkan berat badan dengan {{$product1->name}} Forte, kami mengirim kembali uang Anda, secara cepat dan tanpa pertanyaan apapun.
						</p>
						<p>Oleh karena itu, Anda bisa mencoba cara menguruskan badan dengan cepat, tanpa resiko apapun. Dengan mengklik pada link di bawah, Anda menerima diskon -70% dan mendapatkan kamasan asli {{$product1->name}} Forte yang paling tinggi konsentrasinya dibandingkan dengan suplemen lain yang tersedia di pasar, yaitu 95%. Penawaran ini hanya berlaku sampai {{ Carbon\Carbon::now()->format('d.m.Y') }}.</p>
						<div class="link-box">
							<p><span>PERHATIAN!</span> Hanya produk asli yang produk efektif!</p>
							
							<p>
								<a href="{{ Session::has('ga-test') ? 'http://331nq.voluumtrk.com/click/1' : 'http://331nq.voluumtrk.com/click/1' }}{{ Session::has('ga-test') ? '' : '?pop=' }}{{ !Session::has('ga-test') ? Cookie::get('welcomePopupCambogia') ? 'false' : 'true' : '' }}">KLIK DISNI UNTUK ORDER ONLINE KAMASAN ASLI {{$product1->name}} FORTE 95% DENGAN PROMO</a>
							</p>
						</div>
					</div>
					<section class="comments">
						<h3 class="std">Komentar:</h3>
						<div class="comment">
							<div class="title"><span>~simanjuntak</span></div>
							<p>siapa yang Sudah coba? Berhasil?</p>
						</div>
						<div class="comment" style="padding-left: 20px;">
							<div class="title"><span>~Merri Nura</span></div>
							<p>Alhamdulillah, aku udah kurus :D tapi ibu saya cocok dengan garcinia cambogia karena bagus dn jauh dari efek samping berbahaya</p>
						</div>
						<div class="comment" style="padding-left: 20px;">
							<div class="title"><span>~Andi Putra</span></div>
							<p>@simanjuntak: no comment, harus coba sendiri, lebih menarik daripada mengobrol di internet;)</p>
						</div>
						<div class="comment" style="padding-left: 20px;">
							<div class="title"><span>~Elsa Novriyani</span></div>
							<p>Assalammualaikum semua, aku pakai tapi hanya 1 minggu;) ku turun 2 kg, oke</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Revie Tariyo</span></div>
							<p>aku beli untuk suamiku;) selama 2 thn perut buncitnya tak bisa dikecilkan dengan cara lain</p>
						</div>
						<div class="comment">
							<div class="title"><span>~erwin handoyo</span></div>
							<p>Sulit dipercaya apa benar ada? Mestinya dibuat masakan yang menggunakan malabar tamarind aja untuk membuat semua wanita langsing dn gembira;]</p>
						</div>
						<div class="comment" style="padding-left: 20px;">
							<div class="title"><span>~Rahmadillaa</span></div>
							<p>bodoh, coba baca lagi tentang ini - bukan malabar tamarind yang menurunkan berat badan -- ini garcinia cambogia yg menurunkannya, zat yang diisolasi dalam laboratorium</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Aimelda Satrini</span></div>
							<p>dimana bisa belinya? bisa belinya di toko aja atau farmasi???</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Himelda Hazirah Amarullah</span></div>
							<p>artikelnya sangat bermanfaat...apakah ini benar?</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Darmi Imanina</span></div>
							<p>menurut saya bagus sekali <3 sdh gunakan selama 10 hari dn turun 4,5 kg udah :) :) :) apa yaang ku harapkan serius ini berat bdann 50 kg:)</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Neneng Zulkarian</span></div>
							<p>oke harus coba :) sdh beli dn tunggu kurier :)</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Wahyuni Armianti</span></div>
							<p>ya saya menggunakan garcinia cambogia ini setelah melahirkan, karena berat badan naik banyak saat hamil, saya berhasil, produk yang bagus</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Rahma Widodo</span></div>
							<p>Akhirnya ada di Indonesia!! Adik saya tinggal di London dan selama saya di sana beberapa bulan pernah membelinya;P</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Muhammad Istiq</span></div>
							<p>olahraga lebih enak dr pada suplementasi, dn aku rekomendasi makan sedikit, lebih sehat- cara yang paling bagus</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Aufa Wandani</span></div>
							<p>apakah ini benar garcinia cambogia menghilangkan selulit secara cepat? sayaa tertarik, tak perlu tutunkan bb, tapi kulit saya menjadi kasar seperti kulit jeruk…</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Ayu Moet</span></div>
							<p>udah beli untuk ibu saya, dia senang dengan hasilnya, jadi saya rekomendasi :)</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Susmarita Sari</span></div>
							<p>aku mahasiswa, belajar kedokteran, di universitas saya sudah mempelajari hal tersebut secara teori, semua mahasiswa kedokteran sudah tahu akan hal ini:D</p>
						</div>
						<div class="comment" style="padding-left: 20px;">
							<div class="title"><span>~Lidya Putri</span></div>
							<p>tapi untuk orang lain ini informasinya baru;) pertama kali mendengar :D</p>
						</div>
						<div class="comment">
							<div class="title"><span>~Yanti</span></div>
							<p>aku belum pernah menggunakan suplemen, tapi udah mendengr bahwa suplementasi selalu bekerja secara berbeda pada orang yang berbeda, ada orang yang turunkan berat badan 3 kg, ada orang yg lain yg turunkan 8 kg. Sulit untuk generalisasi, harus coba sendiri</p>
						</div>
						{{--NEW COMMENTS--}}
						<div class="comment">
							<div class="title"><span>Emak Astuti</span></div>
							<p>Mba, bener gak sih, kok bb aku skr 55 kg sebelum 5 botol Garcinia 70kg</p>
						</div>
						<div class="comment">
							<div class="title"><span>Yosefin Driharsanto</span></div>
							<p>baru 30 hari udah turun 13kg min! 3-4 botol Garcinia cukup tadiny Bbq 69 ni tadi nimbang 65kg. Pdhl aq cm minum 1 kapsul tiap harinya… makasih</p>
						</div>
						<div class="comment">
							<div class="title"><span>eka dwi</span></div>
							<p>obt nya sukseh:) saya sudah turun 12kg pdhl sy minumnya jarang stlh 1 botol Garcinia -3kg tp stlh 3 botol.. 12! kg!! HEBAT! skarang makin pede apa rahasianya? Garcinia Cambogia Forte jawabannya!!</p>
						</div>
						<div class="comment">
							<div class="title"><span>rinawati</span></div>
							<p>banyak yang bilang Garcinia bagus tapi sya gak langsung percaya. Begitu sya coba konsumsi sendiri, ternyata benar, dlm 1 minggu berat badan sya turun 3kg, dlm 4 minggu 16kg. Cmn dengan konsumsi 3 botol Garcinia. Makasih!</p>
						</div>
						<div class="comment">
							<div class="title"><span>angelina35</span></div>
							<p>saya beli pertama kali 3 botol kemudian 3 lagi hasilnya luar biasa saya turun 20 kg. 6 botol garcinia hebat!</p>
						</div>
						<div class="comment">
							<div class="title"><span>Ch.Wibowo</span></div>
							<p>masya Allah:)! Seperti keajaiban sy bisa kurus, pdhl sdh bertahun2 sy gemuk, sangat tdk tdk percaya diri dan minder dekat org lain… tapi 4 botol Garcinia telah mengubah duniaku menjadi jauh lebih indah.. skrg sy lebih percaya diri dan bahagia. Terima kasih garcinia cambogia forte</p>
						</div>
						<div class="comment">
							<div class="title"><span>Sri Mulyati</span></div>
							<p>saya beli bulan lalu, udah turun 10kg stlh 2 botol, tapi akan continue minum garcinia supaya bb tetap stabil, mau order 6 botol garcinia. Pelayanan oke punya, ramah, terpercaya dan sabar banget. Sukses selalu smoga Garcinia Cambogia terus maju agar wanita Indonesia smakin pede dengan tubuh proporsional dan tetap sehat selalu</p>
						</div>
						<div class="comment">
							<div class="title"><span>Santoso42</span></div>
							<p>aku minumnya malam hari sebelum tidur, percaya tidak percaya, aku dalam 10 hari turun 5 kg, dalam 30 hari 14kg!!</p>
						</div>
						<div class="comment">
							<div class="title"><span>Hadi Z.</span></div>
							<p>Ga nyangka loh, jd di lomba in gtu, padahal kan sy kirim kesaksian karena memang beneran 5 botol Garcinia sukses bikin BB sy turun terima kasih!! <3 sy mencoba dengan olah raga, namun hasilnya tidak maksimal:)) 5-6 botol Garcinia hebat!</p>
						</div>
						<div class="comment">
							<div class="title"><span>Yosef</span></div>
							<p>Dulu berat badan saya 69kg sebelum saya memakai Garcinia saya juga sdh mencoba produk lain tp tdk berhasil menurunkan berat badan walau cuma sekilo, sampai saya putus asa. Akan tetapi saat saya buka internet dan melihat produk garcinia, saya memiliki sedikit harapan dan saya langsung mengontak 4 botol Garcinia call center saya baru mencoba sebulan ini Alhamdulillah skarang berat badan saya 53 kg!!</p>
						</div>
						{{--END NEW COMMENTS--}}
						<h3 class="std">Poskan komentar:</h3>
						<form method="POST">
							<textarea name="comment_text" cols="40" rows="10" placeholder="Poskan komentar" required></textarea>
							<input type="text" name="comment_user" placeholder="Nama" required>
							<input type="submit" name="comment_submit" value="Poskan komentar">
						</form>
					</section>
				</article>
				
				<aside class="hidden-xs">
					<a href="http://www.accuweather.com/id/id/jakarta/208971/weather-forecast/208971" class="aw-widget-legal">
						<!--
						By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
						-->
					</a>
					<div id="awcc1472489642912" class="aw-widget-current" data-locationkey="208971" data-unit="c" data-language="id" data-useip="false" data-uid="awcc1472489642912"></div>
					<script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
					<div class="facebook-box">
						<div class="ribbon-1"><a href="javascript: void(0)" onclick="return false;"></a></div>
						<div class="ribbon-2"><a href="javascript: void(0)" onclick="
																					return false;">Medical Jurnal</a>
						</div>
						<div class="ribbon-3"><a href="javascript: void(0)" onclick="
																							return false;">Suka</a>
						</div>
						<div class="ribbon-4"><p>2314 lainnya menyukai ini <a href="javascript: void(0)" onclick="
																									return false;">Medical Jurnal</a>
							</p></div>
						<div class="ribbon-5"></div>
						<div class="ribbon-6"><a>Plugin sosial Facebook</a></div>
					</div>
				</aside>
			</div>
		</div>
	</div>
	
	<footer id="main-footer">
		<div class="page-container">
			<a href="{{ route('page.index') }}"><img class="footer-logo" src="{{asset('build/images/frontend/fi6WL4S/logo.png')}}" alt="Medic Reporters"></a>
			<nav>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">Home</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">Diet</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">Fit & Olahraga</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">Psikologi</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">Di ranjang</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">Kecantikan</a>
				<a href="{{ Session::has('ga-test') ? route('page.preorder.test').'?utm_medium=cpc&utm_source=mgid.com&utm_campaign=Garcinia-cambogia_W2&utm_term={widget_id}' : 'http://331nq.voluumtrk.com/click/1' }}">Kesehatan</a>
			</nav>
		</div>
		<div class="page-container copy">
			<hr>
			HAK CIPTA 2016 SEMUA HAK CIPTA TERPELIHARA
		</div>
	</footer>
	
	<div id="popup" style="display: none;">
		<div class="window">
			<h1>Terima kasih!</h1>
			<p>Komentar Anda telah dikirim dan sedang menunggu moderasi.</p>
			<button class="close" onclick="$('#popup').hide();">TUTUP</button>
		</div>
	</div>
	
	<script src="{{ elixir('js/frontend/all.js') }}"></script>
	
	@include('part.preorderPopup', ['productSymbol' => 'cambogia'])

@endsection