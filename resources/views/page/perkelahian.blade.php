@extends('layout.perkelahian',[
    'title' => 'Single - Future Imperfect by HTML5 UP'
])

@section('analytics')
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-83521033-5', 'auto');
        ga('send', 'pageview');

    </script>
@endsection

@section('content')
    <!-- Wrapper -->
    <div id="wrapper">

        <!-- Header -->
        <header id="header">
            <h1><a href="{{ route('page.index') }}"><img src="{{ asset('build/images/frontend/perkelahian_logo.png') }}" style="width:200px;" alt=""></a></h1>
            <nav class="links">
                <ul>
                    <li><a href="{{ route('page.preorder') }}">HOME</a></li>
                    <li><a href="{{ route('page.preorder') }}">DIET</a></li>
                    <li><a href="{{ route('page.preorder') }}">FIT & OLAHRAGA</a></li>
                    <li><a href="{{ route('page.preorder') }}">PSIKOLOGI</a></li>
                    <li><a href="{{ route('page.preorder') }}">DI RANJANG</a></li>
                    <li><a href="{{ route('page.preorder') }}">KECANTIKAN</a></li>
                    <li><a href="{{ route('page.preorder') }}">KESEHATAN</a></li>
                </ul>
            </nav>
        </header>

        <!-- Main -->
        <div id="main">

            <!-- Post -->
            <article class="post">
                <header>
                    <div class="title">
                        <h2><a href="{{ route('page.preorder') }}">Perkelahian Antara Wanita Indonesia dan Wanita Malaysia!</a></h2>
                        <h3 style="font-size:20px">Banyak goresan dan memar di dahi, bibir dan pipi! Kenapa?</h3>
                        <p style="font-size:14px">Laporan Wartawan Jakarta News, Daryono</p>
                    </div>
                    <div class="meta">
                        <time class="published" datetime="2015-11-01">November 24, 2016</time>
                        <a href="{{ route('page.preorder') }}" class="author"><span class="name">Daryono</span><img src="{{ asset('build/images/frontend/avatar.jpg') }}" alt="" /></a>
                    </div>
                </header>
                <span class="image featured">
                    <img src="{{ asset('build/images/frontend/girls_fight2.jpg') }}" alt="" />
                    <div style="font-size:20px; font-weight:bold">Dewi Suraswanto dari Indonesia (22) diserang oleh Michelle Karin (24) dari Malaysia.</div>
                </span>
                <p style="font-size:25px;">Dalam kasus perkelahian di kampus, polisi menahan dua wanita berumur sekitar 24-an dan 22-an. Kenapa mereka berbicara dengan tinjunya?</p>
                <p>"Saya taruhan dengan seorang mahasiswa dari Malaysia sebesar lima ratus ribu rupiah. Kalau saya tidak berhasil <a href="{{ route('page.preorder') }}"><b>turun 10 kg dalam 15 hari</b></a>, ia menang" kata Dewi Suraswanto, seorang mahasiswa Indonesia. Berikut ini wawancara dengan dia. Sayangnya, Michelle Karin, mahasiswa dari Malaysia tersebut melarikan diri, karena sampai hari ini dia masih tidak bisa menerima kekalahannya. </p>
                    <p style="margin-bottom:0; font-size:20px"><b>Michelle Karin dari Malaysia masih tidak bisa menerima kekalahannya:</b></p>
                    <img src="{{ asset('build/images/frontend/angrymalasia.jpg') }}" alt="" width="49%" style="float:left; margin:10px" />
                    <p><strong>Redaksi:</strong> Dewi Suraswanto, apa yang terjadi? Silahkan beritahu kami. </p>
                    <p><strong>Dewi Suraswanto: </strong>Cewek pintar tidak mau bertaruh pada sesuatu yang beresiko besar. Hal itu jelas! Cewek pintar berorientasi pada masa depan, pada kemajuan dan modernisasi. Sebagai mahasiswa biologi semester 4, saya tahu: <a href="{{ route('page.preorder') }}"><b>turun 10 kg dalam 15 hari sangat mungkin</b></a>. Jadi, saya berani bertaruh dengan mahasiswa Malaysia itu.</p>
                    <p>(Dewi diam beberapa detik.)</p>
                    <p><strong>Redaksi:</strong> Lalu? Kita berdua orang Asia, kita dapat mengatakan dengan hati terbuka!</p>
                    <p><strong>Dewi Suraswanto:</strong> Lalu saya menang! Tapi dia tidak bisa menerima kekalahannya. Dia langsung memukul pipi saya dua kali dengan tinjunya, dan langsung saya balas. Dia memukuli seluruh tubuh saya, juga menendang, menjambak rambut, meludahi wajah saya…</p>
                    <p><strong>Redaksi:</strong> Anda tidak takut?</p>
                    <p><strong>Dewi Suraswanto:</strong> Tidak, saya marah! Saya akan memenangkan perkara ini, karena saya <a href="{{ route('page.preorder') }}"><b>memiliki alat bukti kuat</b></a>, dan saya merasa sebagai pihak yang benar. Saya berhasil <a href="{{ route('page.preorder') }}">turun 10 kg dalam 15 hari!</a> Saya berhasil turun dalam waktu singkat, tanpa diet ketat, tanpa olahraga, tanpa usaha apaun. Saya menang!</p>
                    <p><strong>Redaksi:</strong> Waduh, Anda sangat berani!</p>
                    <p>(Dewi tertawa, lalu diam beberapa detik.)</p>
                    <p><strong>Dewi Suraswanto:</strong> Saya cewek pintar. Saya tahu saya akan menang. Saya seorang ilmuwan dan saya selalu percaya pada data empiris dan hasil percobaan. Saya memulai terapi dengan obat baru saja, <a href="{{ route('page.preorder') }}"><b>obat untuk semua yang ingin kuruskan badan dengan mudah dan cepat!</b></a> Dan saya berhasil: dalam beberapa hari bobot dan tubuh saya mengalami perubahan.</p>
                    <p><strong>Redaksi:</strong> Itu mustahil... Bagaimana caranya?</p>
                    <a href="{{ route('page.preorder') }}">
                        <img src="{{ asset('build/images/frontend/rightfloatingbox.jpeg') }}" alt="" style="float:right;" />
                    </a>
                    <p><strong>Dewi Suraswanto:</strong> Ini bukan sihir, ini hasil nyata. Saya minum 1 kapsul <a href="{{ route('page.preorder') }}"><b>Garcinia Cambogia Forte</b></a> saja, sebelum sarapan: 1 kali sehari. <a href="{{ route('page.preorder') }}"><b>Obat ini</b></a> membuat tubuh saya membakar lemak dengan cepat, memblokir proses pengendapan lemak dan mengurangi kadar kolesterol jahat.</p>
                    <p><strong>Redaksi:</strong> Ajaib. Bagaimana dengan diet ketat?</p>
                    <p><strong>Dewi Suraswanto:</strong> Alhamdulillah dengan <a href="{{ route('page.preorder') }}"><b>obat ini</b></a>, saya tak perlu diet ketat, saya tak perlu olahraga, karena <a href="{{ route('page.preorder') }}"><b>obat tersebut</b></a> membuat tubuh saya langsing dan membakar lemak secara otomatis.</p>
                    <p><strong>Redaksi:</strong> Sangat menarik. Apakah itu benar?</p>
                    <p><strong>Dewi Suraswanto:</strong> Ya, hasilnya telah terbukti secara ilmiah, ini bukan sihir: dengan Garcinia Gambogia Forte setiap orang bisa <a href="{{ route('page.preorder') }}"><b>turun 9-12 kg hanya dalam 2 minggu</b></a>.</p>
                    <p><strong>Redaksi:</strong> Bagaimana komposisinya?</p>
                    <p><strong>Dewi Suraswanto:</strong> Komposisinya sangat aman: <a href="{{ route('page.preorder') }}"><b>ekstrak alami Garcinia Cambogia Forte</b></a> dari buah Malabar Tamarind yang <a href="{{ route('page.preorder') }}"><b>memiliki manfaat menurunkan berat badan</b></a>. Selain itu, <a href="{{ route('page.preorder') }}"><b>obat tersebut</b></a> juga memiliki beberapa manfaat lain: menjaga kesehatan kondisi tubuh, meningkatkan kinerja fisik, membantu untuk mengurangi kolesterol tinggi, dan lain-lain.</p>
                    <p><strong>Redaksi:</strong> Dengan kata lain, inilah senjata rahasia Anda?</p>
                    <p><strong>Dewi Suraswanto:</strong> Ya, inilah <a href="{{ route('page.preorder') }}"><b>rahasia kesuksesan saya</b></a>. Saya merekomendasikan <a href="{{ route('page.preorder') }}"><b>obat ini</b></a> kepada Anda yang ingin kuruskan badan secara cepat, mudah dan alami – yang ingin sukses dalam waktu singkat, karena jauh lebih mudah, cepat dan murah daripada cara lain.</p>
                    <p><strong>Redaksi:</strong>Apakah obat ini tersedia hanya untuk orang kelas atas? Pasti mahal?</p>
                    <p><strong>Dewi Suraswanto:</strong> Tidak mahal! Sangat murah dibandingkan dengan cara lain! Dan jangan lupa ya: menghemat waktu sama pentingnya dengan menghemat uang. Dan <a href="{{ route('page.preorder') }}"><b>Garcinia Cambogia Forte</b></a> itu tersedia untuk semua kalangan. Memang, pada saat ini hanya tersedia di toko online, cara memesannya – <a href="{{ route('page.preorder') }}"><b>melalui halaman resmi</b></a>.</p>
                    <p><strong>Redaksi:</strong> Terima kasih atas wawancara ini, sangat menarik. Apakah Anda ingin menunjukkan sesuatu kepada para pembaca kami?</p>
                    <p><strong>Dewi Suraswanto:</strong> Sama-sama. Kita semua sahabat, saya ingin agar semua, orang Indonesia dan orang Malaysia, mengerti bahwa sekarang mereka juga <a href="{{ route('page.preorder') }}"><b>dapat menurunkan berat badan secara aman dan cepat, tanpa diet ketat</b></a> yang mungkin berbahaya bagi kesehatan. Terapi dengan <a href="{{ route('page.preorder') }}"><b>obat yang saya rekomendasikan</b></a>, hasilnya telah terbukti secara ilmiah dan tersedia untuk semua kalangan, dengan harga terjangkau. Jadi saya ulangi, ini bukan sihir, ini hasil nyata: <a href="{{ route('page.preorder') }}"><b>rahasia menurunkan berat badan telah ditemukan</b></a>.</p>
                    <p>Dewi Suraswanto setuju untuk membuka rahasianya dan menunjukkan link website produsen resmi <a href="{{ route('page.preorder') }}"><b>Garcinia Cambogia Forte</b></a> – cara ampuh bagi semua yang ingin menurunkan berat badan dengan mudah dan cepat: yang ingin sukses dalam waktu singkat, tanpa usaha apapun.</p>
                <center>
                    <a href="{{ route('page.preorder') }}"><img src="{{ asset('build/images/frontend/button.jpg') }}" id="button" alt=""></a>
                </center>

                <div class="comments-block">
                    <div class="title"><b>KOMENTAR-KOMENTAR</b></div>
                    <br>
                    <div class="container">
                        <ul class="list">

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Surya Wati</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>Dewi Suraswanto semoga sukses!!!</p></div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>pratiwi22</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>ya itu benar kegemukan berbahaya.. barusan temanku kata tentang metode ini, dan sekarang aku baca artikel ini, aku tertarik karena aku udh coba turun beberapa kali.</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>sylvi kurniawati</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>artikel yang sangat menarik saya juga mau turun karena merasa malu, kadang2 takut keluar dari rumah saya. masalah saya kegemukan dan kadar gula tinggi. </p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Vellia Fatimah</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>MAU MENGATASI RASA LAPAR, GIMANA CARANYA??? mau turun 5 kg..setelah melahirkan perut saya buncit</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>dewi</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>saya hanya cukup dengan makan 3 kali makanan utama perhari dan tanpa makanan ringan. Tanpa obat!</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Widia Resdiana</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>@dewi: ya tapi ada yg perlu obat..suamiku berusia 51 dia udah mengubah diet dan kami mencari suplemen yg baik tapi di apotik ada hanya obat mahal</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Elok Puspitasari Satoto</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>aku sdh pesan obat ini, karena murah dan baru aku mau coba cara sederhana </p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Rizki Jakarta</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>Berat badan turun dengan obat itu setelah 2 minggu pemakaian, dan kadar kolesterol jahat turun, bagus sekali tanpa olahraga tanpa diet apapun!</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Siska</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>gue tertarik krn gue udh bebrapa kali coba beberapa obat dn selalu mengalami efek yo-yo, ada yg udh coba? Gue mau turun dengan cepat</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Aminatun123</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>mau!! hilangkan lemak di perut dan paha dng cepat. Tak suka diet ketat:( harganya berapa? Ada di toko kota medan??</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>hudayani</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>SEMOGA SUKSES DEWI!! harus coba ya</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>NUR35</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>@Aminatun123: Tuh baca aja, pada saat ini obat ini dijual melalui halaman produsen resmi! aku tertarik karena udah lama cari cara ampuh untuk turunkan berat, cara alami.. sulit sekali ya udah coba beberapa obat lain, selalu gagal menemukan cara yg baik</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>amir</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>hasilnya terlihat setelah pemakaian pertama? Mau coba</p>
                                    </div>
                                </div>
                            </li>

                            <li class="item">
                                <div class="comment">
                                    <div class="comment-info">
                                        <span class="user-name"><b>Suriyati39</b></span>
                                    </div>
                                    <div class="comment-text">
                                        <p>lumayan ya .. ku mau cara sehat untuk mengatasi kegemukan</p>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>

            </article>

        </div>

        <!-- Footer -->
        <section id="footer">
            <p class="copyright">&copy; Medical Jurnal</p>
        </section>

    </div>
@endsection
