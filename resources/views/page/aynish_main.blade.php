@extends('layout.app',[
    'title' => 'Bagaimana berat badan saya turun'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/aynish/all.css') }}">
@endsection

@section('content')
    <header class="header container">
        <div class="row">
            <div class="span12">
                <div id="logo">
                    <span title="Melani Kurniawan's Blog"></span>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="post-194 post type-post status-publish format-standard has-post-thumbnail hentry category-blog tag-basecamp-kledung tag-gunung-sindoro tag-hiking tag-mendaki tag-pendakian tag-sindoro tag-sunrise pin-article span12 full">
                <div class="pin-container">
                    <img alt="" class="attachment-blog wp-post-image" src="{{asset('build/images/frontend/gr-coffee.jpg')}}">
                </div>
                <article class="article">
                    <h1 class="title">
    <span>Bagaimana berat badan saya turun
    </span>
                    </h1>
                    <div class="line">
                        <div class="entry-info viewcomments">
    <span class="entry-date">
    <i class="icon-time"></i>
    <span class="rdate">09.11.2016 - 17:39</span>
    </span>
                        </div>
                    </div>
                    <p>
                        Halo! Semua orang mulai sadar bahwa saya baru saja menurunkan berat badan saya secara drastis dan mulai memborbardir
                        saya dengan pertanyaan. Saya berusaha sebaik-baiknya untuk menjawabpertanyaan untuk membantu kalian semua, tapi gagal
                        – karena banyaknya pesan yang harus saya balas saya harus terus-menerus online. Sehingga, saya memutuskan untuk
                        menulis post ini untuk menjawab pertanyaan : „Bagaimana cara anda bisa menuurunkan 31kg?” (Ini bukan berarti anda
                        tidak bisa menulis kepada saya atau menanyakan sesuatu, hal ini hanya untuk membantu saya).
                    </p>
                    <h3 align="center">Transformasi saya hanya membutuhkan waktu DUA BULAN! HASIL YANG MENAKJUBKAN, BUKAN?</h3>
                    <div class="wp-caption aligncenter" id="attachment_179">
                        <a href="{{ route('page.preorder') }}">
                            <img alt="logistic sindoro" class="size-medium wp-image-179" src="{{asset('build/images/frontend/51711c_5f5e14230a18484db3e0adb19a2d409f.jpg')}}">
                        </a>
                    </div>
                    <h3 style="color: rgb(93, 134, 15);">Latar Belakang</h3>
                    <p></p>
                    <h3 align="center"></h3>
                    <p><b></b></p>
                    <p>
                        Saya memang tidak pernah kurus, namun saya melihat bahwa berat badan saya terus bertambah dan bertambah. Tiga
                        lipatan yang mengerikan muncul pada perut saya, saya terlihat seperti seekor bulldog menggunakan bikini, dan
                        pinggul, kaki, dan bokong saya terus membesar dan membesar! Saya memutuskan untuk memiliki tubuh yang fit dan
                        sehat bagaimanapun caranya, sehingga saya berhenti makan makanan berminyak dan gorengan dan berhenti ngemil pada
                        malam hari. Saya mulai lari di pagi hari dan olahraga di sore hari di gym tapi bukannya berat badan turun, berat
                        badan saya malahan terus bertambah lebih banyak lagi! Setelah sebulan, berat badan saya mencapai nilai yang
                        mengerikan – NYARIS 100KG! Saya mulai membuat diet saya menjadi lebih ketat, pantang memakan daging, roti, kentang
                        goreng dan makanan manis. Pada akhirnya saya hanya memakan buah-buahan dan sayuran dan hanya meminum air putih saja.
                        Saya mencoba beberapa teh dan pil penurun berat badan yang sangat mahal tapi sepertinya tidak ada yang cocok untuk
                        saya. Beberapa waktu kemudian, berat badan yang saya turunkan akan kembali kepadaku.
                    </p>
                    <p><b>Diet dan pil tidak membantu, saya merasa tertipu!</b></p>
                    <p><b>
                            Olahraga sangat berat dan butuh waktu yang sangat lama untuk melihat hasilnya. Jadi apa yang harus saya lakukan?
                        </b></p>
                    <p>
                        Setelah sejumlah diet, pil dan berjam-jam di gym dan jutaan uang saya telah habis untuk instruktur personal
                        akhirnya saya menyerah seluruhnya. Suatu hari saya kebetulan melihat sebuah artikel tentang <b>Garcinia Cambogia</b>
                        dan memutuskan untuk mencobanya. Walaupun saya pernah mendengar Demi Moore, Katy Perry, J-Lo dan banyak artis
                        lainnya menurunkan berat badan dengan Garcinia, awalnya saya skeptis. Namun setelah mencoba segalanya dan sekarang
                        saya putus asa, saya tidak punya pilihan lagi! Di samping itu, saya melihat ulasan tentang produknya dan ulasannya
                        sangat bagus!
                    </p>
                    <div class="clear"></div>
                </article>
                <div style="clear: both;"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@endsection