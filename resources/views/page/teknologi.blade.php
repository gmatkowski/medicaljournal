@extends('layout.teknologi',[
    'title' => 'Teknologi fbi dan kgb sekarang digunakan oleh orang biasa'
])

@section('content')

    <center><img src="{{ asset('build/images/frontend/banner.jpg') }}" alt=""></center>
    <div class="container main-wrapper" style="padding-top:30px;">
        <div class="col-md-8">
            <p style="font-size:34px;line-height:120%;font-family:Georgia;margin-bottom:30px;">Rusia dan Amerika telah menggunakan teknologi ini. Sekarang kita dapat menggunakannya untuk kebutuhan pribadi kita!</p>
            <img src="{{ asset('build/images/frontend/center.png') }}" style="margin-right:20px;" class="pull-left" alt="">
            <p>Pusat pelatihan para agen mata-mata di Novosibirsk, Rusia. Pelatihan mereka termasuk olahraga intensitas hingga kursus bahasa dunia. <span class="yellowbg">Biasanya mereka belajar dengan mudah dan cepat: 1-3 bahasa dalam 30 hari, secara bersamaan, secara otodidak dan otomatis, tanpa usaha apapun.</span></p>
            <p><b>Semakin banyak, semakin baik, tapi juga bagus untuk menguasai minimal satu bahasa lain dengan sangat baik, sebaik pribumi.</b> Saat ini, bahasa dalam permintaan tinggi adalah Inggris, Mandarin, Farsi (Persia), Pashto, Dari, Rusia, dan Arab, yang mencerminkan ketagangan politik dunia saat ini.</p>
            <p>Apa yang menarik: mereka tidak perlu guru. Mereka   belajar sendiri. Guru adalah faktor kunci pembentukan kemampuan mereka, namun telah terbukti bahwa guru bahasa terbaik adalah… otak! <b>Otak ternyta mampu menguasai 10 bahasa dunia dalam waktu cepat.</b> Bagaimana? Otak manusia ternyata memiliki saluran khusus…</p>
            <img src="{{ asset('build/images/frontend/agen.jpg') }}" class="pull-right" alt="">
            <p><b>Agen Soviet legendaris Yan Chernyak (atau lebih dikenal sebagai „Pria tanpa bayangan”) punya kelebihan spesial, ia menguasai 8 bahasa</b> dan pada usia 16 tahun ia bisa berbahasa Jerman, Yiddi, Hungaria, Rumania, Ceko, dan Slovak dengan fasih. Ia kemudian mempelajari bahasa Prancis dan Inggris secara otodidak. <span class="yellowbg">Seorang agen belakangan diketahui sebagai aktris favorit Hitler: Olga Chekhova menguasai 11 bahasa secara sempurna sendirian, dia bahkan bisa belajar beberapa bahasa secara bersamaan.</span></p>
            <p><b>Ini bukan sihir, ini hasil nyata. Bagaimana caranya</b>?</p>
            <p>Mereka memakai <a href="https://krebsmethod.co.id">metode Krebs</a> untuk menguasai bahasa asing dengan mudah dan cepat, karena metode tersebut adalah teknologi pembelajaran bahasa yang paling efektif di seluruh dunia. <span class="yellowbg">Emil Krebs menemukan bahwa sangat mudah untuk menguasai bahasa baru dengan menggunakan beberapa metode yang mendorong pemahaman informasi dalam waktu singkat.</span></p>
            <p><strong>Tugas rahasia: menguasai bahasa apapun tanpa usaha apapun!</strong></p>
            <p>Para agen mata-mata tersebut belajar sendiri, secara otodidak dan otomatis. Dari awal mereka mulai mengulang-ulang kosakata baru, lalu mereka membuat frasa, dan berkat pengulangan ini, mereka mulai dapat berbicara dalam bahasa asing hanya setelah pelajaran pertama, dan <span class="yellowbg">ketika kursus sudah selesai, mereka akan mampu berkomunikasi dalam bahasa baru dengan lancar.</span></p>
            <img src="{{ asset('build/images/frontend/super-method2.png') }}" style="width:100%;" alt="">
            <p>Hal ini jelas dan sangat sederhana: <span class="yellowbg">Emil Krebs menemukan bagaimana belajar kata-kata, kalimat atau frasa secara berurutan - terutama menaikkan interval waktu, akan beralih secara permanen dari memori sementara ke memori permanen.</span> Kursus yang diajarkan dengan metode ini awalnya menghilangkan proses belajar membaca dan tata bahasa, berdasarkan bentuk belajar alami anak-anak yang mendengarkan. Telah terbukti bahwa hanya bentuk inilah paling efektif dan dengan metode ini pendengar memahami informasi baru secara lebih cepat.</p>
            <p><a href="https://krebsmethod.co.id"><b>Jika Anda ingin coba sampel pelajaran gratis klik di sini atau baca lebih lanjut</b></a>:</p>
            <p>Sebuah penelitian ilmiah yang pernah dilakukan seorang ilmuwan spesialis linguistik terapan, Dr. Mahen Kehrshi Ph.D, mengungkapkan bahwa kemampuan otak manusia dalam menguasai bahasa asing sungguh luar biasa.</p>
            <p>„Hal terpenting yang harus diperhatikan saat mempelajari bahasa adalah proses yang alami. Seringkali, seseorang yang bertahun-tahun menuntut ilmu dari buku tertulis tidak mampu berbicara lebih mahir dari mereka yang belajar dari percakapan. <a href="https://krebsmethod.co.id">Metode Emil Krebs</a> menawarkan proses pembelajaran dengan cara cepat – seolah-olah Anda sedangberada di tempat asal bahasa itu. <span class="yellowbg">Sesi 30 menit memaksimalkan konsentrasi dan proses belajar sembari beraktivitas misal: fitness. Pembelajaran semacam ini lebih efisien dari pelajaran tradisional.</span>”</p>
            <p>Dr. Mahen Kehrshi Ph.D</p>
            <img src="{{ asset('build/images/frontend/super-method.jpg') }}" style="width:100%;" alt="">
            <p><b>Metode ini telah mendunia.</b> Biro Intelejen Federal Amerika Serikat (FBI) juga telah menggunakan pendekatan Emil Krebs ini, <span class="yellowbg">telah terbukti bahwa otak manusia memiliki saluran khusus yang dapat mempelajari setiap kosakata baru.</span> Bahkan dari berbagai bahasa yang tidak pernah dikenalnya. Namun, menurut Krebs, selama ini hampir semua manusia belum mengaktifkan saluran otak linguistik tersebut. Kenapa? Sejumlah penelitian terbaru menunjukkan bahwa Anda tidak bisa belajar bahasa asing dengan belajar terlalu banyak informasi dan tata bahasa. Otak kita akan kelebihan beban sehingga tidak mampu menyerap informasi yang kami berikan. </p>
            <p><b>Otak kita perlu metode yang mengaktifkan bagian-bagian otak kita yang memungkinkan informasi masuk ke memori jangka panjang.</b> Penelitian telah menunjukkan bahwa kata-kata/frasa baru yang diulang-ulang pada interval yang tepat, akan tersimpan dalam memori Anda dalam waktu yang lama. <span class="yellowbg">Belajar kata-kata, kalimat atau frasa, terutama dengan menaikkan interval waktu, akan beralih dari memori sementara ke memori permanen untuk selamanya.</span></p>
            <a href="https://krebsmethod.co.id" class="center-block btn btn-success btn-lg">Ingin Coba? Klik Disini Sekarang</a>
        </div>
        <div class="col-md-4">
            <a href="https://krebsmethod.co.id">
                <img src="{{ asset('build/images/frontend/super-method.jpg') }}" style="width:100%;" alt="">
            </a>
            <span class="yellowbg">
                <b>
                    <a href="https://krebsmethod.co.id">Jika Anda ingin coba sampel gratis klik di sini atau baca lebih lanjut.</a>
                </b>
            </span>
        </div>
    </div>

    <div class="comments-block main-wrapper container">
        <div class="container">
            <div class="title"><b>KOMENTAR-KOMENTAR</b></div>
            <br>
            <ul class="list">

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Sobari</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Mau!! Ini bisa dipakai anak-anak dan dewasa kan?</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Ami</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Banyak orang mempelajari macam macam perbendaharaan kata tapi akhirnya hanya menggunakan sebagian dan lupa yang lain. Kita tidak memakai frasa dan sinonim tertentu, kita hanya menggunakan yang termudah.</p>
                        </div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Nany</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Gua selalu pengen belajar bahasa Inggris yang serius. Bukan buat keperluan kantor sih, tapi pengen tau aja rasanya bisa baca web bahasa Inggris atau nonton film serial, tau kan biar kerasa lebih internasional dan tau lebih tentang dunia. Tapi, gua harus akuin : Gua agak males. Atau emang gua ga mampu buat fokus dan konsentrasi ke buku pelajaran. Gua udah cobain beberapa pelajaran bahasa yang inovatif seperti Callan, metode 1000 kata, metode 5S, dll...</p>
                        </div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Wan Nainggolan</b></span>
                        </div>
                        <div class="comment-text">
                            <p>saya sudah pesan daam bentuk CD</p>
                        </div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Rizkha</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Saya beli kursus ini denganmu. Sangat puas sekali!</p>
                        </div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Rakel</b></span>
                        </div>
                        <div class="comment-text">
                            <p>kak aku mu tanya ,aku tertarik dengan kursus ini .. apakah kursus ini efektif?</p>
                        </div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Rendy</b></span>
                        </div>
                        <div class="comment-text">
                            <p>gmn perkembangannya gan?ane mau beli klo mengesankan</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>anton123</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Ya!!recommended! Cara ampuh!</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>IesHa</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Ada yang udah coba?</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Melly Felicia</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Tadi aku udah download filenya. Baru aja selesai pelajaran pertama bener-bener gampang masuknya. Awalnya agak skeptis sih, tapi beneran ini bikin surprise.</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Putri putra</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Bagus sekali!!!</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Irwan Taslim</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Saya juga sudah download dan sangaaaat puaas. Meskipun saya hanya mempelajarinya sesekali ketika ada liburan beberapa hari, saya masih ingat pelajaran terakhir yang saya dapat.</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Muhamad</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Luarr biasa, saya tertarik untuk membeli dan mempelajarinya</p></div>
                    </div>
                </li>

                <li class="item">
                    <div class="comment">
                        <div class="comment-info">
                            <span class="user-name"><b>Karl</b></span>
                        </div>
                        <div class="comment-text">
                            <p>Sangat me-Motivasi saya untuk mempelajari banyak Bahasa lagi, Selain 3 Bahasa yang saya bisa! Metode Tradisional ini tidak jauh berbeda dengan metode lainnya,Namun yang ini lebih Efektif,Cepat,dan Praktis!</p></div>
                    </div>
                </li>

            </ul>
        </div>
    </div>

@endsection