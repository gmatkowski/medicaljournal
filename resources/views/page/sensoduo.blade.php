@extends('layout.sensoduo',[
    'title' => 'Senso Duo'
])

@section('content')
    {{--<div class="hpromo onet_on">Акция</div>--}}

    <div style="width: 859px;padding-bottom:0;" class="wrapper">
        <div id="topsection">
            <div class="fl">
                <a href="http://occasionforyou.com/7/krasota/senso-duo?lid=0&ac=666&aid=768&l=ru_ru&from=6349105556606589979&oloc=ru_RU&uid=295825739530145244&int=LbMzK2yT_cXOADC_MoZg_9-QcC6DAu2L0wxDZZiiD_ohZNElNK-YlI0igKX1QhnOqZoB2TGJ2GXOJe1tfNbBXQ&chk=7" target="_blank">
                    <img src="{{ asset('build/images/frontend/fi6WL4S/logo.png') }}" width="351">
                </a>
            </div>
            <img src="//st01.worldglobalnews.com/3f/fd/ce269222b1aeffbd84f873acc4ae/logos2.png" style="margin-top: 40px; float: right; width: 500px; clear: right;">
            <div class="clearfix"></div>
            <div class="onet_off">
                <ul id="mainnav">

                    <li class="first"><a href="http://health.medical-jurnal.com/">HOMEPAGE</a></li>
                    <li><a href="http://health.medical-jurnal.com/">FITNESS & OLAHRAGA</a></li>
                    <li><a href="http://health.medical-jurnal.com/">TIPS KESEHATAN</a></li>
                    <li class="last"><a href="http://health.medical-jurnal.com/">MAKANAN & DIET SEHAT</a></li>
                    <li class="right">06.11.2016</li>
                </ul>

            </div>

        </div>
        <div class="clearfix"></div></div>
    <div style="width:853px;padding-top:0;" class="wrapper">
        <div id="maincontent">
            <div id="maintext">
                <hgroup class="maintitle">
                    <h2>Para ilmuwan memperingatkan: rambut Anda akan menjadi 67% lebih tebal berkat obat kesehatan baru ini</h2>
                </hgroup>

                <div class="attention">
                    <table>
                        <tr>
                            <td class="annone"><img src="//st01.worldglobalnews.com/3f/fd/ce269222b1aeffbd84f873acc4ae/attention.png"/></td>
                            <td class="annoucment">
                                <div class="border top"></div>
                                <div class="text"><strong>PERHATIAN:</strong>obat kesehatan VSDO untuk pertumbuhan rambut bagi wanita maupun pria yang dijual di toko-toko: stoknya sudah habis, namun pada saat ini: XXX obat kesehatan VSDO masih bisa ditemukan di internet.</div>
                                <div class="border bottom"></div>
                            </td>
                        </tr>
                    </table>
                </div>

                <div class="mainnewsimg">
                    <img alt="" src="{{ asset('build/images/frontend/img-1.jpg') }}" width="150">
                    <span class="dott"></span>
                    <p>Siti Hastuti, jurnalis kami menulis mengenai obat baru untuk pertumbuhan rambut, yang semakin laris di seluruh dunia</p>

                </div>

                <p class="f16">Obat kesehatan baru untuk pertumbuhan rambut bagi wanita maupun pria adalah terobosan luar biasa, solusi yang ditunggu-tunggu banyak orang. Inilah pengobatan untuk kebotakan dan rambut rontok disebabkan oleh menurunnya kesehatan rambut - para ilmuwan Swiss tidak memiliki keraguan tentang itu. Pendekatan inovatif mereka untuk pemulihan rambut, memungkinkan dalam waktu singkat 1 bulan untuk membuat rambut 67% lebih tebal.</p>

                <p>Formula baru ini sangat bermanfaat, karena didasarkan pada analisis mendalam yang telah dilakukan oleh peneliti terhadap penyebab pertumbuhan rambut yang lambat. Para ahli kecantikan dan para ahli rambut atau trichologi (yaitu para ahli dalam bidang pengobatan penyakit rambut dan perawatannya) dari Institut Ilmu Kesehatan dan Kecantikan di Zürich menemukan bahwa proses rambut rontok dimulai dengan kerusakan lapisan kolagen di sekitar akar rambut. Penelitian ini sudah terbukti secara uji klinis. Di Indonesia, obat kesehatan baru ini sudah dicoba para pembaca kami, antara lain Ibu Nur Sartini (53 tahun) dari kota Medan:</p>
                <p class="subtitle blue">„Dulu saya terlihat seperti Nenek Sihir Jahat, merasa malu ketika keluar rumah”</p>
                <p>Kolagen adalah struktur mikroskopis yang merupakan jaringan yang meliputi akar rambut. Struktur tersebut sangat sensitif terhadap kekurangan vitamin, perawatan rambut yang tidak tepat, gangguan hormon dan stress yang dialami oleh orang dewasa. Akibatnya rambut menjadi kering dan mudah rusak. Dengan kolagen, regenerasi rambut bisa terjadi lebih cepat, rambut rontok segera digantikan oleh yang baru, dan akar rambut semakin kuat karena akar yang cukup kolagen. Tetapi kekurangan kolagen menyebabkan rambut kering, rontok dan mudah patah, kekurangan kolagen membuat kita tampak lebih tua.</p>
                <div class="cf"></div>



                <p>Oleh karena itu, kehilangan kolagen menyebabkan rambut rontok, meblokir sel-sel kulit dan rambut, sehingga rambut mendapatkan asupan nutrisi yang kurang. Akibatnya akar rambut menjadi lemah dan akhirnya rambut menjadi lebih tipis dan rontok. Pada pria, rambut rontok biasanya dimulai pada dahi atau di puncak kepala ke belakang. Beberapa pria kehilangan hanya beberapa helai rambut dan hanya memiliki garis rambut surut atau botak kecil di belakang. Orang lain yang mulai botak di usia dini akan kehilangan semua rambut mereka di atas kepala mereka bersama dengan mengalami garis rambut surut (dikenal sebagai androgenetic alopecia atau kebotakan pola pria).</p>

                <p>Para ahli dari Zürich fokus pada bagaimana memberi nutrisi pada rambut, sehingga regenerasi rambut bisa terjadi lebih cepat. Ternyata tiga bahan alami ini yang paling efektif: XXX</p>

                <p>Bahan-bahan tersebut memiliki manfaat beragam: bila diterapkan pada rambut dan kulit kepala, membantu untuk menjaga rambut lembut dan mencegah rambut rontok, membantu mengembalikan keadaan kolagen, sehingga kulit akan dapat nampak lebih kencang dan segar. Dengan demikian, regenerasi rambut bisa terjadi lebih cepat, rambut rontok segera digantikan oleh yang baru, dan akar rambut semakin kuat karena akar yang cukup kolagen – penjelasan oleh Alexander Fischer, direktur di Institut Ilmu Kesehatan dan Kecantikan di Zürich. Kami menemukan bahwa jumlah bahan yang paling ampuh dan efektif adalah VSDO – dia menjelaskan.</p>

                <p>Setelah meningkatkan kekuatan akar rambut, langkah selanjutnya adalah mendorong pertumbuhan rambut yaitu meningkatkan ketebalan rambut Anda. Proses ini dirangsang oleh dua bahan alami yang berada di obat kesehatan VSDO. Bahan pertama adalah kafein yang dapat membantu mempercepat pertumbuhan rambut dan memberikan energi pada folikel rambut. Selain itu, kafein juga telah terbukti mampu merangsang poros perpanjangan rambut, memerpanjang siklus pertumbuhan rambut, dan merangsang jumlah keratin rambut, yaitu sel yang berperan penting untuk pertumbuhan rambut. Bahan kedua adalah Arginin. Salah satu penyebab rambut rontok adalah karena kulit kepala kekurangan asam amino. Arginin merupakan asam amino yang sangat bermanfaat bagi rambut dan kulit kepala manusia, karena meningkatkan sirkulasi di kulit kepala, dapat merontokkan sel-sel mati dari kulit kepala serta meningkatkan kelancaran peredaran darah yang memberikan nutrisi ke rambut.</p>

                <p>Ibu Nur Sartini setuju:</p>

                <p class="subtitle blue">„Rambut saya mulai tumbuh tebal dalam waktu singkat menjadi semakin kuat, lebih sehat dan bernutrisi.”</p>

                <p>VSDO adalah produk baru, komposisi dan efeknya ditemukan beberapa bulan yang lalu. Uji coba dan tes klinis sudah dilakukan pada VSDO, dan hasilnya sudah terbukti secara ilmiah: dalam 1 bulan pemakaian hampir semua 97% pengguna berhasil membuat rambutnya jauh lebih tebal dan meningkatkan kesehatan rambut melalui nutrisi yang diberikan didalam jaringan rambut dan kulit kepala.</p>
                <p>Oleh karena itu, obat kesehatan VSDO untuk pertumbuhan rambut sudah diuji oleh para ahli dermatologi dan terbukti secara klinis. Sekarang obata kesehatan VSDO sangat direkomendasikan oleh para ahli kesehatan dan menjadi semakin laris di pasar Eropa Barat dan Amerika Serikat. Mereka sudah membuktikan bahwa dalam 1 bulan pemakaian rambut menjadi lebih tebal dengan rata-rata 67%.</p>
                <p>Sekarang formula VSDO tersedia juga di Indonesia, dan menjadi semakin laris di pasar Tanah Air. Hasilnya sudah terbukti secara ilmiah, jadi obat kesehatan VSDO untuk pertumbuhan rambut sangat direkomendasikan: baik bagi wanita maupun pria, untuk mengatasi rambut rontok, kering dan mudah patah, dan untuk semua yang mulai botak.<br>Ibu Nur Sartini membuktikan:</p>
                <p class="subtitle blue">„Akhirnya, sekarang ketika saya melihat cermin, saya melihat rambut saya lebih sehat dan bernutrisi, saya senang sekali!”</p>

                <p>Pada saat ini, VSDO stoknya sudah habis di toko-toko. Oleh karena itu, produsen VSDO menyiapkan dan memberikan penawaran khusus sebagai hadiah bagi para pembaca kami. Ada promo: hanya hari ini obat baru ini tersedia di Internet sebagai hadiah club promo baru dengan harga yang lebih rendah.</p>

                <p>100% Jaminan Kepuasan</p>
                <p>&nbsp;</p>

                <div class="info">
                    <img class="satisfaction" src="//st01.worldglobalnews.com/59/39/60a48e42759074024ea130f3369b/ru_satisfaction.png" />
                    <div class="header">
                        <h3 style="">BERITA PENTING</h3>
                    </div>
                    <ol style="padding: 10px 10px 10px 40px;">
                        <li><div><strong>PERHATIAN:</strong> Formula VSDO memberikan „100% Jaminan Kepuasan”. Jika Anda tidak puas dengan hasilnya, tidak peduli untuk alasan apa, kami mengembalikan uang Anda dengan cepat dan tanpa pertanyaan apapun.</div></li>
                        <li class="yellow"><div>Promo hanya hari ini bagi pembaca World of Science: klik link di bawah untuk mendapatkan VSDO dengan diskon XXXX IDR. Promo ini hanya berlaku sampai XXXX.</div></li>
                    </ol>
                </div>

                <p class="hijp" style="font-size: 27px; font-weight: bold;">
                    <a href="http://occasionforyou.com/7/krasota/senso-duo?lid=3&ac=666&aid=768&l=ru_ru&from=6349105556606589979&oloc=ru_RU&uid=295825739530145244&int=LbMzK2yT_cXOADC_MoZg_9-QcC6DAu2L0wxDZZiiD_ohZNElNK-YlI0igKX1QhnOqZoB2TGJ2GXOJe1tfNbBXQ&chk=7" style="text-decoration: underline;">
                        {{--Нажмите здесь, чтобы получить оздоровительный курс Vivèse Senso Duo Oil для роста волос--}}
                    </a>
                </p>
            </div>
            <div id="rightcontent">

                <div class="righttxt2">
                    <div href="http://occasionforyou.com/7/krasota/senso-duo?lid=4&ac=666&aid=768&l=ru_ru&from=6349105556606589979&oloc=ru_RU&uid=295825739530145244&int=LbMzK2yT_cXOADC_MoZg_9-QcC6DAu2L0wxDZZiiD_ohZNElNK-YlI0igKX1QhnOqZoB2TGJ2GXOJe1tfNbBXQ&chk=7" target="_blank" class="fb_right2">

                        <div class="button" style="">
                            <div id="ll"></div>
                            <div id="lm">Suka!</div>
                            <div id="lr"></div>
                        </div>
                        <div class="clearfix"></div>
                        <div id="lr2">Jumlah orang yang menyukai: 828.296</div>
                        <div class="clearfix"></div>
                        <div style="padding:7px 0 0 0; color:#333;line-height:26px;"></div>
                    </div>
                </div>
                <div class="whitespacer"></div>
                <div class="righttxt">
                    <img src="//st01.worldglobalnews.com/3f/fd/ce269222b1aeffbd84f873acc4ae/front.jpg" class="frontPage" />
                    <div class="spacer"></div>
                    <span class="dott"></span>
                    <h3>Formula VSDO sudah terbukti secara ilmiah</h3>

                    <p class="graph">menumbuhkan rambut dan mengatasi rambut rontok</p>
                    <div class="graph-bar item1"><div class="round">97<span>%</span></div></div>
                    <p class="graph">mengatasi rambut kering dan mudah patah</p>
                    <div class="graph-bar item2"><div class="round">99<span>%</span></div></div>
                    <p class="graph">mengatasi kebotakan rambut</p>
                    <div class="graph-bar item3"><div class="round">89<span>%</span></div></div>
                    <p class="graph">membuat rambut lebih sehat dan bernutrisi</p>
                    <div class="graph-bar item4"><div class="round">93<span>%</span></div></div>

                    <p>Hasil dari survey World of Science, 231 pengguna</p>
                </div>
                <div class="whitespacer clearfix"></div>
                <div class="righttxt grey">
                    <span class="dott"></span>
                    <h3 style="margin-bottom:10px;">Testimoni Pengguna obat kesehatan VSDO untuk pertumbuhan rambut:</h3>
                    <div class="watotherssay">
                        <div class="fl"></div>
                        <center>
                            <img style="margin-bottom:15px;" width="215" src="{{ asset('build/images/frontend/img-3.jpg') }}">
                            <h3 class="title2" style="font-style:italic;">Ibu Nur Sartini (Medan, 53 tahun):
                                Saya tak malu lagi, dengan obat ini saya sudah mengatasi rambut rontok, rambut saya menjadi lebih tebal, VSDO membuat rambut saya lebih sehat dan bernutrisi.</h3>
                        </center>
                        <div style="margin:15px 0;"></div>
                        <center>
                            <img style="margin-bottom:15px;" width="215" src="{{ asset('build/images/frontend/img-2.jpg') }}">
                            <h3 class="title2" style="font-style:italic;">Pak Agus Suraswanto (Solo, 46 tahun)
                                Hanya dengan obat kesehatan baru ini saya berhsil mengatasi kebotakan rambut. Dalam 1 bulan rambut rontok saya digantikan oleh rambut yang baru, dan akar rambut saya semakin kuat.</h3>
                        </center>
                    </div>
                </div>
                <div class="whitespacer clearfix"></div>
                <div class="righttxt smallp">
                    <p style="margin: 0; padding-top: 0; padding-bottom: 0;"><strong>Hasil luar biasa ini sudah dibuktikan oleh para pengguna</strong></p>
                    <p><b>9 dari 10</b> pengguna formula VSDO mengaku bahwa hasilnya 100% sesuai dengan apa yang diharapkan</p>

                    <p style="margin: 0; padding-top: 0; padding-bottom: 0;"><strong>Efeknya aman dan alami sudah terbukti secara ilmiah</strong></p>
                    <p>Sebelum memasarkan obat kesehatan VSDO diuji oleh para ahli dermatologi, dan terbukti secara klinis di Universitas Negeri di Scranton (Amerika Serikat). Sudah terbukti secara ilmiah, bahwa obat kesehatan VSDO untuk pertumbuhan rambut sepenuhnya aman dan efektivitasnya sangat tinggi. Efektivitasnya juga sudah terbukti oleh 10 000 orang Indonesia, berusia lebih dari 25 tahun. Hasilnya? Mereka mengaku bahwa obat ini sangat efektif untuk mengatasi rambut rontok, kering dan mudah patah, mereka bahkan mengaku bahwa ini cara ampuh untuk mengatasi kebotakan rambut baik bagi wanita maupun pria.</p>

                    {{--<a href="http://occasionforyou.com/7/krasota/senso-duo?lid=6&ac=666&aid=768&l=ru_ru&from=6349105556606589979&oloc=ru_RU&uid=295825739530145244&int=LbMzK2yT_cXOADC_MoZg_9-QcC6DAu2L0wxDZZiiD_ohZNElNK-YlI0igKX1QhnOqZoB2TGJ2GXOJe1tfNbBXQ&chk=7" style="display: block; text-align: right; padding: 0 10px;">Читать далее &raquo;</a>--}}
                </div>

            </div>
            <div class="clear"></div>
            <br></div>
        <div id="bottom">
            <br>

            <div id="FB_HiddenContainer" style="position:absolute; top:-10000px; width:0px; height:0px;"></div>
            <div id="feedback_1HsYymlsW4NLzXtW1" style="font-family:Tahoma;">
                <div class="fbFeedbackContent" id="uz1cxy_1">
                    <div class="stat_elem">
                        <div class="uiHeader uiHeaderTopBorder uiHeaderNav composerHider">
                            <div class="clearfix uiHeaderTop">
                                <a class="uiHeaderActions rfloat">Tambahkan komentar/a>
                                <div>
                                    <h4 tabindex="0" class="uiHeaderTitle">
                                        <div class="uiSelector inlineBlock orderSelector lfloat uiSelectorNormal">
                                            <div class="wrap">
                                                <a class="uiSelectorButton uiButton uiButtonSuppressed" role="button" aria-haspopup="1" aria-expanded="false" data-label="683 comments" data-length="30" rel="toggle"><span class="uiButtonText">683 komentar</span></a>
                                                <div class="uiSelectorMenuWrapper uiToggleFlyout">

                                                </div>
                                            </div>

                                        </div>
                                        <span class="phm indicator"></span>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="uiList fbFeedbackPosts">


                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Suswano</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">ada yang sudah coba?</div>
                                        </div>
                                    </div>



                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Reuk Ratha</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">aku mengalami botak gimana cara menumbuhkan rambutnya kembali dok? apa bisa tumbuh kembali</div>
                                        </div>
                                    </div>

                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Liliana</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">saya sudah coba beberapa cara lumayan yg membuat rambut saya sedikit lebih tebal tapi menurut saya perlu merawat rambut dengan benar</div>
                                        </div>
                                    </div>


                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">febrina laban</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Rambut saya rusak, sangat rusak n kering serta rontok, dan kasar, mungkin karna sering dicatok. Gimana rambut saya bisa sehat kembali? Kalu ada yang sudah coba cara ini</div>
                                        </div>
                                    </div>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">yanie tinrot</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">pada awal gue udh pikir apa ini, tapi baca ya gue tertarik , ada yg udh coba?</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Bao tran</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">DENGAN CARA INI IBU GUE UDAH BERHASIL MEMBUAT RAMBUTNYA JAUH LEBIH TEBAL DAN MENINGKATKAN KESEHATAN RAMBUT, TANPA CARA LAIN YG MAHAL MAHAL</div>
                                        </div>
                                    </div>



                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Rahadians</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">menurut saya ini benar komposisinya bagus saya bekerja di laboratorium dan tahu bahwa komposisi seperti ini benar2 dapat membuat rambut rontok digantikan oleh rambut yang baru dan akar rambut semakin kuat dalam waktu singkat</div>
                                        </div>
                                    </div>

                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">ida bagus</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">MINTA BANTUAN UDAH BEBERAPA BULAN AKU MENGALAMI MASALAH RAMBUT RONTOK CARI BANTUAN DA YANG UDH COBA PRODUK INI?</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Surya wati</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">sangat bermanfaat, aku udh bebrapa hari pake obat ini dan senang, rambutku menjadi lebih tebal, lebih sehat dn bernutrisi, aku senang.</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">~Darmi neneng</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">apakah ini benar jaminan kepuasan atau uang kembali itu, mereka benar2 memberikan kembali uang jika gagal atau tidak puas dengan hasilnya? Saya tak punya banyak uang, saya perlu jaminan bahwa saya bayar karena mau rambut saya jadi tebal dan sehat...</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~widodo</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">saya sendiri memakai obat ini hanya 7 hari, tapi pada saat ini saya yakin: rambut tidak rontok lagi! saya mendapat kembali kepercayaan diri:)</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~Rahma Junaidi</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Senso Duo ya ku menggunakannya secara rutin, udah 1 paket, sangat membantu, ku tak suka cara yg lain&mahal, dan Senso Duo cara sederhana</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~Melly Chandra</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">terima kasih atas informasi</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~Sari Sus</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">gue juga udh coba gue senang bagus sekali</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~Ayu</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">udah beli untuk ibu saya, dia senang dengan hasilnya, jadi saya rekomendasi :)</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~Putri Susmarita</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">mau bei utk suami, gimana pendapat anda dia ingin menggunakannya?</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~Lesmana</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">pasangan saya udah pake dan senang. rekomendasikan</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">~Aldo</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">umurku 28 thn, dan udh beberapa bulan ku berusaha mengatasi masalah rambut rontok, sulit sekali, tapi degan cara ini ku mau mengatasinya!</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Santy Raharhja</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">~APAHA MEREKA BENAR2 MEMBRIKAN UANG JIKA TAK PUAS DENGAN HASILNYA? MAU TAHU! KU PERLLU BANTUAN KRNA MAU MENIKAH</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">anna puri</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">~ya benar memang ada informasi ya jaminan kepuasan atu uang kembali 100%</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">Marina Isakh</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">~saya tak percaya. Mai rambut sehat? Beli sampo yg baik, jangan lupa sisir rambut sebelum tidur dll, kadang2 pakai masker dll!!!</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">issy</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">~MINTA JAWABAN!!! TUNGGU</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem uiListVerticalItemBorder" id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">
                                        <span class="profileNamee">Nandiasari</span>
                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">~oke, aku udh order, ada hasilnya- ku coba</div>
                                        </div>
                                    </div>
                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                    </ul>
                    <div class="clearfix mts mlm uiMorePager stat_elem fbFeedbackPager fbFeedbackTopLevelPager uiMorePagerCenter" id="pager4fc78c58063b37637111639">
                        <div>
                            <a class="pam uiBoxLightblue fbFeedbackPagerLink uiMorePagerPrimary" rel="async">Muat 659 komentar lainnya<i class="mts mls arrow img sp_comments sx_comments_arrowb"></i></a>
                            <span class="uiMorePagerLoader pam uiBoxLightblue fbFeedbackPagerLink">
     <i class="img sp_comments sx_comments_"></i>
     </span>
                        </div>
                    </div>
                    <div class="fbConnectWidgetFooter">
                        <div class="fbFooterBorder">
                            <div class="clearfix uiImageBlock">
                                <a class="uiImageBlockImage uiImageBlockSmallImage lfloat"><i class="img sp_comments sx_comments_cfavicon"></i></a>
                                <div class="uiImageBlockContent uiImageBlockSmallContent">
                                    <div class="fss fwn fcg">
     <span>
     </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <center>
                <img alt="" align="absmiddle" src="{{ asset('build/images/frontend/fi6WL4S/logo.png') }}" width="188" height="53" />
            </center>
        </div>
    </div>

    <iframe src='http://pix.tagcdn.com/pix/?a=index&was=0&ac=666&l=ru&aid=768&kid=&d=worldglobalnews.com&hid=6349105556606589979&chk=7&type=bloghair' class='d38f0e547e2d' style='width:15px !important;height:15px !important;' id='pix-index-was0' scrolling='no' frameborder='0' ></iframe><iframe src='http://aff.tagcdn.com/pix/?a=index&ac=666&l=ru&aid=768&cid=&d=worldglobalnews.com&hid=6349105556606589979&chk=7&uid=295825739530145244' class='d38f0e547e2d' width='1' height='1' scrolling='no' frameborder='0' ></iframe><iframe src='http://pix.rvcdn.com/pix/?a=index&was=0&ac=666&l=ru&aid=768&kid=&d=worldglobalnews.com&hid=6349105556606589979&chk=7&type=blogSDO2S&mid=' class='d38f0e547e2d' id='pix_da-index-was0' scrolling='no' frameborder='0' ></iframe>

    <img src="//worldglobalnews.com/7/krasota/senso-duo/pixel_load?w=loaded&vid=rf7bui1bmfkghfi0ffsbag3wuunz04v8&chk=7&r=1478426540&uid=295825739530145244" class="d38f0e547e2d" />
@endsection