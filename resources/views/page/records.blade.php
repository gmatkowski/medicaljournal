@extends('layout.app')
@section('title') :: {{ trans('menu.records') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')

	<main>
		<section class="records">
			<div class="container">
				<h1 class="wow fadeIn">
					{{ $page->title }}
				</h1>
				<p class="wow fadeIn">
				{!! $page->content !!}
				</p>
				<ul class="clients-opinion">
					@foreach($records as $record)
					<li class="wow fadeInLeft audio">
						<span>{{ $record->name }}</span>
						<p>{{ trans('records.bought.course') }}: <strong>{{ trans('records.language') }} <span>{{ $record->language->name_short }}</span></strong></p>
						<a href="" class="record">
							<i class="fa fa-play-circle"></i>
						</a>
						<audio>
							<source src="{{ $record->record_path }}">
							Your browser does not support the audio element.
						</audio>
					</li>
					@endforeach
				</ul>
			</div>
		</section>

		@include('part.footer_small')
	</main>

@endsection