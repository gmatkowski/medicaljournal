@extends('layout.terobosan',[
    'title' => 'Terobosan Dalam Memerangi Keriput'
])

@section('analytics')
    @include('ga.terobosan_test')
@endsection

@section('content')

    <div class="container">
        <h1>
            <b>3 ilmuwan memenangkan Nobel Kimia atas penemuannya tahun 2009, sebagai terobosan dalam memerangi keriput</b>
            <p class="date-tag">12 December, 2016</p>
        </h1>
    </div>
    <img src="{{ asset('build/images/frontend/awards.jpg') }}" style="width:100%" alt="">

    <div class="caption container">
        <div class="col-md-12">
            <strong style="display:inline-block;padding-left:20px;">V. Ramakrishnan</strong> mendapat Hadiah Nobel untuk penemuannya, sebagai terobosan dalam memerangi keriput
        </div>
    </div>

    {{--<div style="float:left;">--}}
        {{--<a href="http://sains-jurnal.com/leffery_acr_test">--}}
            {{--<img src="{{ asset('build/images/frontend/lefery-box2.jpg') }}" style="margin-left:20px; margin-right:20px; width:200px; display:block;" alt="">--}}
        {{--</a>--}}
        {{--<div style="text-align:center; font-size:20px;">--}}
            {{--<b>Klik dan Coba</b>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="container main-wrapper" style="padding-top:30px;padding-bottom:30px;">
        <div class="col-md-8">
            <p class="top">Hasil uji klinis, yang dilakukan oleh Prof. Romain Lefery, membuktikan bahwa ada formula, yang membantu untuk menangani keriput muka, kulit yang tidak kenyal, kendur dan kering. <b>Dengan formula ini, <span class="yellowbg">wajah dapat kelihatan lebih sehat dan muda.</span></b></p>
            <img src="{{ asset('build/images/frontend/pobrane.jpg') }}" class="pull-left img-thumbnail" style="margin-right:20px;" alt="">
            <p>Sebelum kita masuk lebih detail, di sini ada beberapa fakta mengenai terobosan medis tersebut. Kulit wanita di usia 65-an, setelah aplikasi formula tersebut, dari sudut pandang kedokteran, mulai terlihat seperti kulit wanita di usia 50-an. Lalu, dari sudut pandang kedokteran, kulit wanita di usia 55-an, setelah apikasi formula baru ini, <b>terlihat seperti kulit wanita muda.</b> Akibatnya, usia biologis kulit perempuan 30-35 tahun kembali ke waktu masa lalu, saat-saat mereka paling cantik.</p>
            <p>Memang, terlihat sedikit aneh pada pandangan pertama, terlalu optimis… Tetapi sering kita dengar sebuah pengetahuan baru secara cepat menjadi pengetahuan usang. Jika ada penemuan terbaru dalam ilmu kimia, yang dihargai dengan Nobel, tentang <span class="yellowbg">cara memerangi keriput dan membalikkan proses penuaan, ini jelas harus diperhitungkan.</span></p>
            <p><b>(Laporan langsung dari New York – Anette Yorin)</b> Berlangsung konferensi pers yang mengungkapkan bagaimana profesor Amerika berhasil mengembalikan vitalitas kulit dan melawan usia yang semakin tua. Rahasia yang tersembunyi adalah pendekatan yang baru terhadap masalah penuaan kulit. Berkat penemuan tiga ilmuwan, pemenang Nobel Kimia pada tahun 2009, yaitu V. Ramakrishnan, T. Steitz, A. Yonath, Prof. Romain Lefery menemukan <a href="http://sains-jurnal.com/leffery_acr_test">solusi untuk memanjakan dan meremajakan kulit secara revolusioner</a>.</p>
            <p><b>Formula ini sudah diuji secara laboratorium di universitas asal, pemeriksaan medis dan tes kosmetik.</b> Uji klinis tersebut telah membuktikan bahwa kita mampu untuk memutar kembali waktu dan mengembalikan peremajaan kulit yang hilang, dalam waktu singkat. Dan peristiwa itu terjadi di depan mata para wartawan yang berkumpul saat konferensi pers - <span class="yellowbg">seorang wanita menjadi lebih muda dan cantik dalam waktu singkat.</span></p>
            <p>Setelah beberapa kata pendahuluan, yang membahas mengenai uji klinis pada perempuan berusia 55 tahun, Prof. Romain Lefery mengidentifikasi beberapa masalah yang dihadapi perempuan berusia 30 hingga 65 tahun. Yang paling penting adalah: <b>keriput di dahi, keriput di sekitar mata dan di sekitar bibir, kerutan di wajah, kulit lembek dan kering, kerutan yang tersisa di sekitar dagu.</b> Lalu dia menunjukkan gambar-gambar klinis mendokumentasikan penemuan luar biasa ini.</p>
            <h3>Efek akan menjadi luar biasa</h3>
            <p>Acara dimulai dengan presentasi campuran dalam stoples, halus dan putih. Profesor mengoleskan campuran ini pada wajah Ibu Jane Smith secara merata, dengan menepuk perlahan diseluruh wajahnya. Formula ini mulai perlahan-lahan mengering dan kemudian menyerap dengan baik di kulit. Pada saat itu Profesor menunjukkan gambar-gambar yang diambil selama proses uji klinis. Kami juga menyaksikan bagaimana dalam waktu singkat, kerutan-kerutan dahi Jane Smith hilang, dan <b>wajahnya menjadi lebih cerah dan berseri seperti wajah bayi.</b> Kemudian kami melihat, bagaimana lipatan nasolabial, juga disebut sebagai kurung wajah atau alur bibir-pipi, segera menghilang. Keriput sekitar mata juga menghilang sampai tipis, dan semua itu terjadi di depan mata kami.</p>
            <p>Kulit Jane Smith menjadi lebih lembut untuk disentuh, dari sebelumnya. Ketika ia menatapnya, kami bisa melihat bagian kulit dia - kendur dan keriput sebelumnya - halus dan lembut setelah terapi. Ini bukan akhir cerita. Kami semakin takjub melihat bagaimana setelah menerapkan campuran, <span class="yellowbg">kulitnya menjadi lebih kencang</span>, dia mulai terlihat menarik, seperti wanita berusia 35 tahun. Pada akhir acara, kulit wajah Ibu Jane Smith terlihat kencang, sehat, muda dan berkilau, hingga orang lain tidak bisa percaya bahwa wajah dia rusak, lelah dan kering sebelumnya. Jadi penemuan luar biasa ini merubah Jane Smith, dari wanita berusia 55 tahun, yang kurang menarik, ke wanita berusia 35 tahun, yang sangat menarik. Ini dia bukti, sekarang kami mengetahui bahwa penemuan luar biasa itu benar.</p>
            <h3>Profesor menjelaskan</h3>
            <p>Bagi kebanyakan orang, proses transformasi yang luar biasa ini, tampak seperti keajaiban. Tapi ternyata tidak bagi para peneliti. Para peneliti menyebut fenomena itu bisa dijelaskan dalam penelitian, dan penelitian ini didasarkan pada kenyataan, oleh para dokter dan ilmuwan, yang memenangkan Nobel kimia, yang menemukan hal tersebut dalam struktur kulit DNA. Prof. Lefery melanjutkan: <b>usia otot-otot akan menjadi lebih tipis dan memanjang</b>, dan kelenjar-kelenjar pada kulit mulai menyusut ukurannya, sehingga tampak perubahan kondisi dan kontur wajah. Semakin menua kulit akan menipis dan kehilangan elastisitasnya. Ada beberapa alasan yang menyebabkan kulit kehilangan elastisitasnya, termasuk karena proses penuaan. Ada beberapa faktor yang mempengaruhi proses penuaan, dan tanda-tanda pertama penuaan ini muncul di sekitar mata.</p>
            <p><b>Apa yang membuat sel-sel kulit menjadi rusak?</b> Kurang hidrasi? Menurut perusahaan-perusahaan kosmetik besar, air adalah faktor terpenting dalam menjaga kondisi kelembaban kulit. Tapi jika benar, cukup sabun dan air untuk membuat kita selamanya muda. Pada kenyataannya, kita kehilangan sesuatu yang sama pentingnya, yaitu <span class="yellowbg">bahan-bahan nutrisi utama struktur DNA</span>, yang bertindak pada tingkat sel. Setelah usia 30 tahun kulit berhenti memproduksi bahan-bahan nutrisi tersebut. Dengan usia kulit kita mengalami kehilangan nutrisi yang sangat penting. Dengan kata lain - kulit kita mengalami kekurangan nutrisi sampai mati.</p>
            <h3>Apakah manusia dapat mengubah alam?</h3>
            <p>Prof. Romain Lefery menjelaskan lebih lanjut: bagaimana kalau obat membuktikan bahwa cara yang baru ditemukan dan ditulis, yaitu <a href="http://sains-jurnal.com/leffery_acr_test">nutrisi struktur DNA</a>, dapat memperbaiki kerusakan kulit, yang disebabkan oleh berlalunya waktu? Bagaimana kalau kulit kita memang bisa <b>mendapatkan kembali penampilan mudanya?</b> Dunia ilmu telah membuktikan bahwa bahan-bahan alami struktur nutrisi DNA dapat mengatasi kulit kering, melembabkan dan menghaluskan kulit wajah dan mendapatkan kembali keindahan alam Anda. Penemuan baru itu akan menutrisi wajah Anda dan menghilangkan keriput dalam waktu yang sangat singkat.</p>
            <p>Ya, telah ditemukan! Penelitian ilmiah berhasil mengungkapkan hal ini, dan hasil ini telah dikonfirmasi dalam uji klinis oleh para ilmuwan. <span class="yellowbg">Cukup aplikasikan struktur nutrisi DNA</span> secara langsung pada kulit wajah yang mengalami penuaan, untuk mendapatkan kembali penampilan mudanya dalam waktu yang sangat singkat. <b>Kerutan atau keriput di wajah, dahi, dan bawah mata menghilang SECARA PERMANEN!</b></p>
            <div class="penulis">
                <div class="col-md-12" style="margin:20px 0;background:#cccccc;padding:15px">
                    <img src="{{ asset('build/images/frontend/lefery.jpg') }}" style="margin-right:20px; width:180px;" class="pull-left" alt="">
                    <h4>Tentang Prof. Romain Lefery</h4>
                    <p>Dia lulus dari Medicine School of Houston. Prof. Romain Lefery adalah mantan anggota United States Hospital in Marsylia dan International Dermatological Clinic in New York, sekarang dia anggota United States Dermal Institute. Dia adalah penulis dan turut menulis banyak publikasi ilmiah tentang segala macam gangguan pada kulit. Dia tinggal dan bekerja di Dallas.</p>
                </div>
                <div class="clearfix"></div>
            </div>
            <center><a href="http://sains-jurnal.com/leffery_acr_test"><img src="{{ asset('build/images/frontend/lefery-box.jpg') }}" style="width:200px;" alt=""></a></center>
            <p style="font-size:23px;text-align:center;">Promo khusus: dengan mengklik pada link di bawah untuk berpartisipasi dalam program promo dan mendapatkan Lefery ACR dengan diskon 50% lebih murah! Penawaran khusus ini hanya berlaku sampai akhir hari ini.</p>
            <center><a href="http://sains-jurnal.com/leffery_acr_test" class="btn btn-lg btn-warning mobilebutton">Klik di sini untuk mendapatkan formula anti-age Prof. Lefery <br> untuk nutrisi struktur kulit DNA</a></center>
        </div>
        <div class="col-md-4" id="right-side">
            <h3>Berita yang paling dibaca:</h3>
            <ul>
                <li><a href="http://sains-jurnal.com/leffery_acr_test">4 Manfaat hebat di balik warna-warni paprika</a></li>
                <li><a href="http://sains-jurnal.com/leffery_acr_test">Bareskrim bongkar kasus vaksin palsu, 5 orang ditangkap</a></li>
                <li><a href="http://sains-jurnal.com/leffery_acr_test">Lupa sarapan bikin bau mulut?</a></li>
                <li><a href="http://sains-jurnal.com/leffery_acr_test">5 Cara agar berpuasa dapat dijalani dengan lebih mudah!</a></li>
                <li><a href="http://sains-jurnal.com/leffery_acr_test">Joe Allen akan segera pulang ke Swansea</a></li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection