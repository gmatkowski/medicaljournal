@extends('layout.admin_singin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

    <!-- page content -->
    <div class="right_col" role="main">

        @include('admin.errors')

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="container-fluid">
                    <div class="col-md-6">
                        <div class="x_title">
                            <h2>{{ trans('admin.menu.externals') }}</h2>
                            <div class="clearfix"></div>
                        </div>
                    </div>

                </div>

                <div class="x_content">

                    <table class="table table-striped responsive-utilities jambo_table">
                        <thead>
                            <tr class="headings">
                                <th class="column-title">Call Center Id</th>
                                <th class="column-title">{{trans('admin.common.name')}}</th>
                                @if(!Entrust::hasRole('headcc'))
                                    <th class="column-title">{{trans('admin.common.phone')}}</th>
                                @endif
                                <th class="column-title">{{trans('admin.common.extClName')}}</th>
                                <th class="column-title">{{trans('admin.common.sendedcc')}}</th>
                                {{--<th class="column-title">{{trans('admin.common.status')}}</th>--}}
                                {{--<th class="column-title">{{trans('admin.common.price')}}</th>--}}
                                <th class="column-title">{{trans('admin.common.date')}}</th>
                                <th class="column-title">Comment :</th>
                                <th class="column-title">Put Comment :</th>
                                <th class="column-title">Save Comment :</th>
                            </tr>
                        </thead>

                        <tbody>
                        @foreach($contacts as $contact)
                            {!! Form::open(['url' => 'externalicc/'.$contact->id]) !!}
                                <tr class="even pointer">
                                    <td class=" ">{{ $contact->cc_id ? $contact->cc_id : '-' }}</td>
                                    <td class=" ">{{ $contact->name }}</td>
                                    @if(!Entrust::hasRole('headcc'))
                                        <td class=" ">{{ $contact->phone }}</td>
                                    @endif
                                    <td class=" ">{{ $contact->externalClient->name }}</td>
                                    <td class=" ">{{ $contact->call_center==1 ? trans('admin.common.yes') : trans('admin.common.no')}}</td>
                                    {{--<td class=" ">{{ $contact->cc_sell==1 ? 'SOLD' : '--' }}</td>--}}
                                    {{--@if($contact->cc_sell==1)--}}
                                        {{--<td class=" ">{{ $contact->cc_price ? StrHelper::dotsInPrice($contact->cc_price) : 'price not filled in cc' }}</td>--}}
                                    {{--@else--}}
                                        {{--<td class=" ">--</td>--}}
                                    {{--@endif--}}
                                    <td class=" ">{{ $contact->created_at }}</td>
                                    <td class="break-words">{{ $contact->icc_comment ? $contact->icc_comment : 'no comment' }}</td>
                                    <td class=" ">
                                        {!! Form::textarea('comment', null, ['cols' => 10, 'rows' => 5]) !!}
                                    </td>
                                    <td class="  last">
                                        {!! Form::submit('Save Comment' ,['class' => 'btn btn-success']) !!}
                                    </td>
                                </tr>
                            {!! Form::close() !!}
                        @endforeach
                        </tbody>
                    </table>

                    <div class="text-right">
                        @if ($contacts->count() > 0)
                            {!! $contacts->render() !!}
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>

@endsection
