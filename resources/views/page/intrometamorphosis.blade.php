@extends('layout.garcinia')

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/garcinia/all.css') }}">
@endsection

@section('content')
    <hr class="metamorphosis">
    <div class="metamorphosis-header">
        <div class="page-container">
            <div class="ng with-desc one-line">
                <h1>TESTIMONI</h1>
                <p>Anda juga dapat merubah hidup Anda! Mereka sudah berhasil turun - sekarang mereka memiliki sosok ramping dan indah. Bagaimana dengan Anda?</p>
            </div>
        </div>
    </div>

    <section id="block-3" class="page-container standard metamorphosis-new">
        <div class="metamorphosis-container">
            <div id="metamorphosis-1">
                <h1>TIARA MAHARANY MENGALAMI PERUBAHAN BESAR</h1>
                <div class="row">
                    <div class="left-side">
                        <div class="image"><img src="{{ asset('build/images/frontend/index-1.jpg') }}" alt="Tiara Maharany"></div>
                        <h2>Terapi Tiara Maharany:</h2>
                        <table class="f1">
                            <tbody><tr>
                                <th> </th>
                                <th>SEBELUM:</th>
                                <th>SETELAH:</th>
                            </tr>
                            <tr>
                                <td>Berat badan:</td>
                                <td>90 kg</td>
                                <td>68 kg</td>
                            </tr>
                            <tr>
                                <td>% lemak:</td>
                                <td>30,40%</td>
                                <td>19,20%</td>
                            </tr>
                            <tr>
                                <td>Pendapat:</td>
                                <td>OBESITAS</td>
                                <td>BERAT BADAN NORMAL</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="f2">
                            <tbody><tr>
                                <th colspan="2">TERAPI DAN HASILNYA:</th>
                            </tr>
                            <tr>
                                <td>Dia berhasil turun: </td>
                                <td>-22 kg</td>
                            </tr>
                            <tr>
                                <td>Rata-rata per minggu: </td>
                                <td>1,7 kg / MINGGU.</td>
                            </tr>
                            <tr>
                                <td>Lemak dibakar: </td>
                                <td>11,20%</td>
                            </tr>
                            <tr>
                                <td>Waktu terapi dengan Garcinia Cambogia Forte: </td>
                                <td>3 BULAN</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-side">
                        <p class="in">Tiara Maharany, 35 thn. Tentang terapi dengan Garcinia Cambogia Forte:</p>
                        <h2>Perempuan berusia lebih dari 35 masih bisa cantik!</h2>
                        <div class="quote">
                            <p>Saat hamil berat saya naik sampai 14 kg... Sebelum menggunakan Garcinia Cambogia, saya berkonsultasi dengan dokter. Dia menegaskan bahwa Garcinia Cambogia alami dan efeknya aman. Pada bulan pertama saya mengecilkan perut, nanti saya menghilangkan selulit dan stretch mark, dan tubuh saya menjadi lebih kuat. Hari ini menandai 4 bulan setelah melahirkan, berat saya 54 kg dan saya merasa seksi kembali! Bagus.</p>
                            <div>
                                {{--<img src="{{ asset('build/images/frontend/sign-1.jpg') }}" alt="Tiara Maharany"><br>--}}
                                - Tiara Maharany
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr class="metamorphosis-sep with-purple-sep">
            <div id="metamorphosis-2">
                <h1>MENDAPATKAN KEMBALI DAYA TARIK</h1>
                <div class="row">
                    <div class="left-side">
                        <div class="image"><img src="{{ asset('build/images/frontend/index-2.jpg') }}" alt="Asvinely Aufa"></div>
                        <h2>Terapi Asvinely Aufa:</h2>
                        <table class="f1">
                            <tbody><tr>
                                <th> </th>
                                <th>SEBELUM:</th>
                                <th>SETELAH:</th>
                            </tr>
                            <tr>
                                <td>Berat Badan:</td>
                                <td>72 kg</td>
                                <td>63 kg</td>
                            </tr>
                            <tr>
                                <td>% lemak:</td>
                                <td>29,60%</td>
                                <td>21,40%</td>
                            </tr>
                            <tr>
                                <td>Pendapat:</td>
                                <td>KEGEMUKAN</td>
                                <td>BERAT BADAN NORMAL</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="f2">
                            <tbody><tr>
                                <th colspan="2">Terapi dan hasilnya</th>
                            </tr>
                            <tr>
                                <td>Dia berhasil turun: </td>
                                <td>-9 kg</td>
                            </tr>
                            <tr>
                                <td>Rata-rata per minggu: </td>
                                <td>1,3 kg / MINGGU.</td>
                            </tr>
                            <tr>
                                <td>Lemak dibakar: </td>
                                <td>8,20%</td>
                            </tr>
                            <tr>
                                <td>Waktu terapi dengan Garcinia Cambogia Forte: </td>
                                <td>1,5 BULAN</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-side">
                        <p class="in">Asvinely Aufa, 17 thn. Tentang terapi dengan Garcinia Cambogia Forte:</p>
                        {{--<h2>Brak czasu nie przeszkodził mi w zdobyciu wymarzonej figury!</h2>--}}
                        <div class="quote">
                            <p>Saya sibuk, tidak punya waktu untuk ini atau itu, tidak punya waktu untuk berolahraga. Teman saya bekerja sebagai pelatih fitness, dia memberi saya hadiah. Garcinia Cambogia membantu saya menurunkan berat badan dengan cepat. Saya turun 9 kg dalam 6 minggu, saya menghilangkan lemak di perut dan paha, dan tubuh saya menjadi lebih kuat dan elastis. Sekarang saya memiliki sosok ramping dan indah. Saya menjadi fit kembali!</p>
                            <div>
                                {{--<img src="{{ asset('build/images/frontend/sign-2.jpg') }}" alt="Asvinely Aufa"><br>--}}
                                - Asvinely Aufa
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-1 page-container" onclick="document.location.href='{{ route('page.preorder') }}'">
                    <p>JIKA ANDA BENAR-BENAR INGIN MENURUNKAN BERAT BADAN</p>
                    <h2><span>PESAN GARCINIA CAMBOGIA FORTE</span>SEKARANG JUGA</h2>
                    <a class="orange" href="{{ route('page.preorder') }}">PESAN SEKARANG</a>
                </div>
                <hr class="metamorphosis-sep with-purple-sep">
            </div>
            <div id="metamorphosis-3">
                <h1>KISAH NYATA DAN LUAR BIASA NURFITRIA</h1>
                <div class="row">
                    <div class="left-side">
                        <div class="image"><img src="{{ asset('build/images/frontend/index-3.jpg') }}" alt="Nurfitria Dwisyanto"></div>
                        <h2>Nurfitria Dwisyanto Terapi:</h2>
                        <table class="f1">
                            <tbody><tr>
                                <th> </th>
                                <th>SEBELUM:</th>
                                <th>SETELAH:</th>
                            </tr>
                            <tr>
                                <td>Berat badan:</td>
                                <td>76 kg</td>
                                <td>61 kg</td>
                            </tr>
                            <tr>
                                <td>% lemak dibakar:</td>
                                <td>27,80%</td>
                                <td>18,90%</td>
                            </tr>
                            <tr>
                                <td>Pendapat:</td>
                                <td>KEGEMUKAN</td>
                                <td>BERAT BADAN NORMAL</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="f2">
                            <tbody><tr>
                                <th colspan="2">TERAPI DAN HASILNYA</th>
                            </tr>
                            <tr>
                                <td>Dia berhasil turun: </td>
                                <td>-15 kg</td>
                            </tr>
                            <tr>
                                <td>Rata-rata per minggu: </td>
                                <td>1,5 kg / TYDZ.</td>
                            </tr>
                            <tr>
                                <td>Lemak dibakar: </td>
                                <td>8,9%</td>
                            </tr>
                            <tr>
                                <td>Waktu terapi dengan Garcinia Cambogia Forte: </td>
                                <td>2,5 BULAN</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-side">
                        <p class="in">Nurfitria Dwisyanto, 28 thn. Tentang terapi dengan Garcinia Cambogia Forte:</p>
                        {{--<h2>Uwielbiam reakcje dawno niewidzianych znajomych: WOW, ale z Ciebie laska!</h2>--}}
                        <div class="quote">
                            <p>Kita semua tahu bahwa sosok yang indah dan ramping adalah faktor yang mempengaruhi daya tarik. Tidak heran bahwa obesitas menyebabkan rasa malu... Dengan metode ini saya mendapatkan bentuk tubuh ideal dan kepercayaan diri kembali, metode ini membuat saya merasa seperti seorang wanita. Saya sudah kehilangan 24 kg, sekarang saya mencintai hidup saya dan sangat sering aku mendengar pujian. Saya senang ketika saya melihat refleksi di cermin. Sekarang saya tahu bahwa mendapatkan bentuk tubuh ideal itu lebih mudah daripada yang Anda bayangkan.</p>
                            <div>
                                {{--<img src="{{ asset('build/images/frontend/sign-3.jpg') }}" alt="Nurfitria Dwisyanto"><br>--}}
                                - Nurfitria Dwisyanto
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="metamorphosis-sep with-purple-sep">
            </div>
            <div id="metamorphosis-4">
                <h1>MUHAMMAD MENGALAMI PERUBAHAN KISAH NYATA</h1>
                <div class="row">
                    <div class="left-side">
                        <div class="image"><img src="{{ asset('build/images/frontend/index-4.jpg') }}" alt="Muhammad Sasmi"></div>
                        <h2>Terapi:</h2>
                        <table class="f1">
                            <tbody><tr>
                                <th> </th>
                                <th>SEBELUM:</th>
                                <th>SETELAH:</th>
                            </tr>
                            <tr>
                                <td>Berat badan:</td>
                                <td>98 kg</td>
                                <td>82 kg</td>
                            </tr>
                            <tr>
                                <td>% lemak dibakar:</td>
                                <td>26,40%</td>
                                <td>16,80%</td>
                            </tr>
                            <tr>
                                <td>Pendapat:</td>
                                <td>KEGEMUKAN</td>
                                <td>BERAT BADAN NORMAL</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="f2">
                            <tbody><tr>
                                <th colspan="2">TERAPI DAN HASILNYA</th>
                            </tr>
                            <tr>
                                <td>Dia berhasil turun kg: </td>
                                <td>-16 kg</td>
                            </tr>
                            <tr>
                                <td>Rata-rata per minggu: </td>
                                <td>1,6 kg / MINGGU.</td>
                            </tr>
                            <tr>
                                <td>Lemak dibakar: </td>
                                <td>9,60 %</td>
                            </tr>
                            <tr>
                                <td>Waktu terapi dengan Garcinia Cambogia Forte: </td>
                                <td>2,5 BULAN</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-side">
                        <p class="in">Muhammad Sasmi, 29 thn. Tentang terapi dengan Garcinia Cambogia Forte:</p>
                        {{--<h2>Odzyskałem zdrowie, pozbyłem się piwnego brzuszka</h2>--}}
                        <div class="quote">
                            <p>Selama 2 tahun saya selalu berjuang melawan berat badan saya, dan sudah mencoba olahraga yang ditawarkan, tetapi semua ini terlalu melelahkan. Pelatih saya merekomendasikan penggunaan Garcinia Cambogia, dan dalam waktu hanya beberapa hari, tubuh saya menjadi lebih kuat, dan saya melakukan latihan fisik yang baik dan benar, 60 menit tanpa berhenti. Saya turun 7 kg dalm 35 hari dan tubuh saya menjadi lebih berotot. Garcinia Cambogia diet ini kalo memang Anda mau mendapatkan badan yang bagus.</p>
                            <div>
                                {{--<img src="{{ asset('build/images/frontend/sign-4.jpg') }}" alt="Muhammad Sasmi"><br>--}}
                                - Muhammad Sasmi
                            </div>
                        </div>
                    </div>
                </div>
                <div class="banner-5 page-container" onclick="document.location.href='{{ route('page.preorder') }}'">
                    <p>DAPATKAN GARCINIA CAMBOGIA FORTE</p>
                    <p>OBAT DIREKOMENDASIKAN OLEG PARA AHLI GIZI<br>HANYA HARI INI DISKON<span> KHUSUS -100%</span></p>
                    <a class="orange" href="{{ route('page.preorder') }}">PESAN SEKARAG</a>
                </div>
                <hr class="metamorphosis-sep with-purple-sep">
            </div>
            <div id="metamorphosis-5">
                <h1>SOSOK RAMPING DAN INDAH KISAH DEWI</h1>
                <div class="row">
                    <div class="left-side">
                        <div class="image"><img src="{{ asset('build/images/frontend/index-5.jpg') }}" alt="Dewi Ismawati"></div>
                        <h2>Dewi Ismawati Terapi:</h2>
                        <table class="f1">
                            <tbody><tr>
                                <th> </th>
                                <th>SEBELUM:</th>
                                <th>SETELAH:</th>
                            </tr>
                            <tr>
                                <td>Berat badan:</td>
                                <td>75 kg</td>
                                <td>64 kg</td>
                            </tr>
                            <tr>
                                <td>% lemak dibakar:</td>
                                <td>32,50%</td>
                                <td>24,30%</td>
                            </tr>
                            <tr>
                                <td>Pendapat:</td>
                                <td>KEGEMUKAN</td>
                                <td>BERAT BADAN NORMAL</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="f2">
                            <tbody><tr>
                                <th colspan="2">TERAPI DAN HASILNYA</th>
                            </tr>
                            <tr>
                                <td>Dia berhasil turun: </td>
                                <td>-11 kg</td>
                            </tr>
                            <tr>
                                <td>Rata-rata per minggu: </td>
                                <td>2,1 kg / MINGGU.</td>
                            </tr>
                            <tr>
                                <td>Lemak dibakarj: </td>
                                <td>8,20%</td>
                            </tr>
                            <tr>
                                <td>Waktu terapi dengan Garcinia Cambogia Forte: </td>
                                <td>1,5 BULAN</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-side">
                        <p class="in">Dewi Ismawati, 19 thn. Tentang terapi dengan Garcinia Cambogia Forte:</p>
                        <h2>Kto wymyślił piperynę? Dajcie mu Nobla!</h2>
                        <div class="quote">
                            <p>Dulu saya malu ketika mencoba pakaian di depan cermin. Sekarang berat saya 61 kg dan saya senang. Hidup saya berubah jika saya berubah! Sosok ramping membuat saya percaya diri, saya merasa cantik. Saya mendengar banyak pujian tentang bagaimana saya berpakaian dan betapa sangat indah dilihat. Saya berhasil menurunkan berat badan!</p>
                            <div>
                                {{--<img src="{{ asset('build/images/frontend/sign-5.jpg') }}" alt="Dewi Ismawati"><br>--}}
                                - Dewi Ismawati
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="metamorphosis-sep with-purple-sep">
            </div>
            <div id="metamorphosis-6">
                <h1>PERUBAHAN BESAR TEGUH</h1>
                <div class="row">
                    <div class="left-side">
                        <div class="image"><img src="{{ asset('build/images/frontend/index-6.jpg') }}" alt="Teguh Rachmat"></div>
                        <h2>Teguh Rachmat Terapi:</h2>
                        <table class="f1">
                            <tbody><tr>
                                <th> </th>
                                <th>SEBELUM:</th>
                                <th>SETELAH</th>
                            </tr>
                            <tr>
                                <td>Berat badan:</td>
                                <td>105 kg</td>
                                <td>88 kg</td>
                            </tr>
                            <tr>
                                <td>% lemak dibakar:</td>
                                <td>28,30%</td>
                                <td>15,90%</td>
                            </tr>
                            <tr>
                                <td>Pendapat:</td>
                                <td>KEGEMUKAN</td>
                                <td>BERAT BADAN NORMAL</td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="f2">
                            <tbody><tr>
                                <th colspan="2">TERAPI DAN HASILNYA</th>
                            </tr>
                            <tr>
                                <td>Dia berhasil turun: </td>
                                <td>-17 kg</td>
                            </tr>
                            <tr>
                                <td>Rata-rata per minggu: </td>
                                <td>1,7 kg / MINGGU.</td>
                            </tr>
                            <tr>
                                <td>Lemak dibakar: </td>
                                <td>12,40%</td>
                            </tr>
                            <tr>
                                <td>Waktu terapi dengan Garcinia Cambogia Forte: </td>
                                <td>2,5 BULAN</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="right-side">
                        <p class="in">Teguh Rachmat, 27 thn. Tentang terapi dengan Garcinia Cambogia Forte:</p>
                        {{--<h2>Wyrzeźbione ciało dodało mi męskości</h2>--}}
                        <div class="quote">
                            <p>Saya tidak percaya kepada suplemen, tetapi Garcinia cambogia adalah cara yang sebenarnya bekerja. Setelah dua bulan penggunaan Garcinia cambogia, saya yakin. Saya menghilangkan lemak di perut, sekarang saya sudah turun 11 kg. Saya senang, karena Garcinia Cambogia membantu mengurangi kadar kolesterol juga. Sekarang saya merasa lebih sehat dan bugar.</p>
                            <div>
                                {{--<img src="{{ asset('build/images/frontend/sign-6.jpg') }}" alt="Robert Grajewski"><br>--}}
                                - Teguh Rachmat
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection