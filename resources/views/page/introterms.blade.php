<p><b>SYARAT & KETENTUAN</b></p>

<p>Syarat dan ketentuan di bawah ini merupakan perjanjian antara Pembeli dan Penjual, mengatur hak dan kewajiban, dan Penggunaan Situs web ini.</p>

<p><b>I. Definisi</b></p>

<p>Syarat dan Ketentuan – berarti dokumen ini yang mengatur syarat dan ketentuan;
Situs web – situs Garcinia Cambogia Forte tersedia secara online;
Situs – sebuah situs internet di mana Pembeli dapat melakukan pemesanan;
Pembeli – seorang yang mengunjungi Situs web;
Produk – berarti setiap barang yang diiklankan di Situs;
Perjanjian jual beli – Identitas kedua pihak, yaitu pihak Penjual dan pihak Pembeli, pengalihan kepemilikan Produk, harganya dan pembayaran;
Pemesanan – adalah pemesanan yang diajukan oleh Pembeli ke Situs untuk membeli Produk dari Penjual dengan mengadakan Perjanjian jual beli melalui Situs dan secara online, yang terdiri dari berbagai komponen yaitu jenis dan jumlah produk-produk yang dipesan, dilakukan melalui formulir pemesanan online yang tersedia di Situs atau cara lain yang dijelaskan dibawah pada bagian II.7 Syarat dan Ketentuan ini;
Penjual – pemilik Situs;
Pengembalian dan penukaran – penggantian biaya, berlaku untuk produk dibeli;
WIB – Waktu Indonesia Barat.</p>

<p><b>II. Kondisi Umum Penggunaan</b></p>

<p>1. Situs, informasi mengenai produk-produk dan deskripsinya harus dianggap sebagai informasi tentang produk dan bukan sebagai tawaran komersial.</p>

<p>2. Untuk membuat pesanan, Pembeli harus berusia di atas 18 tahun dan memiliki kapasitas hukum penuh untuk membuat Pesanan ditempat tinggal Pembeli.</p>

<p>3. Semua pemesanan dilakukan melalui Situs diterima 24 jam sehari, sepanjang tahun.</p>

<p>4. Pemesanan dilakukan melalui Situs pada hari kerja setelah jam 12.00 siang WIB, pada hari Sabtu, hari Minggu dan hari libur, akan dikirimkan pada hari berikutnya – hari kerja (dalam beberapa kasus, periode ini dapat diperpanjang).</p>

<p>5. Sebuah prasyarat yang diperlukan untuk realisasi Pemesanan adalah mengisi formilir pemesanan dengan benar dan lengkap atau melalui telepon.</p>

<p>6. Penjual berhak melakukan verifikasi terhadap informasi yang Pembeli berikan. Setelah pesanan diterima, Penjual dapat menghubungi Pembeli mengenai pemesanan pada nomor telpon yang telah diberikan. Jika tidak ada jawaban atau balasan, Penjual dapat membatalkan pesanan.</p>

<p>7. Pembeli dapat langsung menghubungi Penjual sebagai berikut:</p>

<p>1) melalui telepon dengan nomor layanan Situs – pada hari dan jam kerja, dari hari Senin pada hari Jumat, kecuali pada hari libur – Pembeli dapat melakukan pemesanan juga melalui telepon.</p>

<p>2) melalui email dengan Alamat email layanan Situs – Pembeli dapat melakukan pemesanan juga melalui email.</p>

<p>8. Faktur jual beli, yaitu faktur yang dikeluarkan oleh Penjual, berisi data tentang nama dan rincian dari Pembeli – informasi yang diisi dalam formulir pemesanan di Situs layanan atau informasi yang diberikan melalui cara lain, berdasarkan Pasal 7.1 dan 7.2, yaitu melalui telepon dengan nomor layanan Situs atau melalui email dengan Alamat email layanan Situs.</p>

<p>9. Penjual telah mengambil semua tindakan yang wajar untuk memastikan bahwa informasi yang terkandung dalam Situs ini adalah akurat, terkini, cocok dan lengkap, namun tidak dapat menjamin keakuratan informasi. Pembeli dapat menghubungi Penjual secara langsung dan Penjual akan melakukan segala upaya untuk memastikan keakuratan informasi dan memperbarui informasi yang terdapat.</p>

<p>10. Jika Pembeli melakukan Pemesanan melalui telepon, cara ini tidak bebas dari biaya koneksi sesuai dengan operator telekomunikasi yang digunakan oleh Pembeli.</p>

<p>11. Suplemen yang tersedia melalui Situs tidak dapat dikembalikan kepada Penjual jika kemasan telah dibuka.</p>

<b>III. Pembelian produk yang ditawarkan di Situs</b>

<p>1. Setelah melakukan Pemesanan, Penjual mengkirim email konfirmasi kepada Pembeli.</p>

<p>2. Setelah konfirmasi dan verifikasi Pemesanan, Penjual mengkirim email konfirmasi pemesanan dengan detail konfirmasi mengadakan perjanjian jual beli kepada Pembeli.</p>

<p>3. Perjanjian jual beli dijalankan setelah Penjual mengkonfirmasi Pemesanan.</p>

<p>4. Setelah mengadakan perjanjian jual beli dengan Penjual melalui Situs atau melalui telepon/email, Pembeli yang melakukan Pemesanan dapat membayar:</p> 

<p>1) denang uang cash, pembayaran dilakukan saat barang diterima (pembayaran ditempat),</p>

<p>2) transfer uang (tidak berlaku untuk pemesanan melalui telepon atau email).</p>

<p>5. Harga Produk di Situs adalah wajar sejak konfirmasi pemesanan diberikan, dengan mengirim email kepada Pembeli, ke alamat email yang diisi dalam formulir pemesanan, sesuai dengan Pasal 2 diatas. Harga tersebut tidak akan berubah, terlepas dari perubahan harga di Situs. Hanya terjadi jika ada harga yang salah di Situs, Pembeli akan diberitahu mengenai hal ini, sebelum mengirim email konfirmasi.</p>

<p>6. Produk dipesan di Situs Penjual akan dikirimkan melalui perusahaan pengiriman, kurir atau pos. Waktu pengiriman Produk 7 hari kerja, yaitu tidak melebihi 7 hari sejak melakukan pembayaran dimuka oleh Pembeli (yang dijelaskan diatas pada bagian 4.2) atau lebih dari 7 hari sejak mengirim kepada Pembeli email konfirmasi (yang dijelaskan diatas pada bagian 4.1), sesuai dengan Pasal 2 diatas. Jika pada hari melakukan Pemesanan Penjual tidak memiliki produk yang dipesan, jarak waktu 3-14 hari kerja untuk menyampaikan produk harus dihitung dari hari diterima Produk untuk Penjual (jika dipilih pembayaran ditempat, sesuai dengan Pasal 4.2 di atas); Pembeli akan diberitahu mengenai hal itu melalui email tersebut, sesuai dengan Pasal 2 diatas.</p>

<p>7. Untuk Produk yang tersedia, Penjual memberi diskon 100% untuk ongkos kirim dan membayar biaya tersebut sendiri. Untuk mendapat diskon tersebut harus melakukan pembayaran sebelum mengikirm Produk kepada Pembeli. Diskon hanya berlaku untuk upaya pertama untuk proses pengirimannya kepada Pembeli. Informasi mengenai ongkos kirim tanpa diskon tersedia di Situs.</p>

<p>8. Pesanan dapat dilakukan dari setiap negara atau wilayah, kecuali yang dijelaskan dibawah pada bagian 8a, namun di beberapa negara bisa hanya melakukan pembayaran dimuka melalui sistem pembayaran di Situs (sesuai dengan Pasal 4.2 diatas).</p>

<p>8a. Penjual tidak melakukan pengiriman pesanan ke Norwegia. Pesanan dari wilayah Norwegia, dengan alamat di wilayah negara ini, tidak akan dilakukan./<p> 

<p>9. Semua produk yang dijual di Situs adalah baru, bersertifikat dan bergaransi, dan ada informasi mengenai produk, sesuai dengan hukum yang berlaku di tempat tinggal Penjual.</p>

<p>10. Warna Produk yang tersedia melalui Situs dapat bervariasi dari foto karena pencahayaan, kualitas layar pixel dan pengaturan warna.</p>

<p>11. Pembayaran ditempat – jika dipilih opsi "Bayar di tempat". Pembayaran untuk Pesanan dilakukan oleh Pembeli saat barang diterima dilokasi, pada alamat yang ditunjukkan oleh Pembeli. Pembayaran dilakukan kepada kurir, agen perusahaan pengiriman atau agen pos.</p>

<p>12. Produk yang dijual adalah milik Penjual sampai saat pembayaran, sesuai dengan Perjanjian jual beli.</p>

<p>13. Jika Pembeli menolak menerima pesanan, tanpa kerusakan apapun, tanpa alasan apapun, atau jika tidak menerima pesanan tersebut pada waktunya (jika dipilih pembayaran ditempat, sesuai dengan Pasal 4.2 di atas), kasus ini harus dianggap sebagai menolak menerima pesanan, bukan sebagai selesai Perjanjian jual beli.
Dalam kasus ini, diskon untuk ongkos kirim tidak berlaku. Selain itu, biaya pengiriman kembali Produk akan dikenakan kepada Pembeli (informasi mengenai ongkos kirim tanpa diskon tersedia di Situs). Untuk melakukan pengiriman lagi harus melakukan oleh Pembeli pembayaran dimuka untuk biaya pengiriman lagi dan biaya pengiriman kembali Produk kepada Penjual (informasi mengenai biaya tersebut tersedia di Situs) ke rekening bank Penjual.</p>

<p>14. Jika Pembeli menolak menerima pesanan, tanpa kerusakan apapun, tanpa alasan apapun, atau jika tidak menerima pesanan tersebut pada waktunya (jika dipilih pembayaran ditempat, sesuai dengan Pasal 4.2 di atas), kasus ini harus dianggap sebagai menolak menerima pesanan, bukan sebagai selesai Perjanjian jual beli.
Pembeli harus melakukan pembayaran untuk biaya pengiriman. Selain itu, Pembeli harus membayar untuk pengiriman kembali Produk kepada Penjual ke rekening bank Penjual (informasi mengenai biaya tersebut tersedia di Situs) dalam waktu 7 hari kerja terhitung dari hari menolak menerima pesanan/tidak menerima pesanan tersebut pada waktunya. Untuk melakukan pengiriman lagi harus melakukan oleh Pembeli pembayaran dimuka untuk biaya pengiriman lagi dan biaya pengiriman kembali Produk kepada Penjual (informasi mengenai biaya tersebut tersedia di Situs) ke rekening bank Penjual.</p>

<p>15. Pasal 13 dan Pasal 14 yang dijelaskan diatas tidak berlaku untuk membatalkan Perjanjian jual beli oleh Pembeli.</p>

<p><b>IV. Kondisi promosi penjualan</b></p>

<p>Untuk Produk tertentu Penjual menawarkan:</p>

<p>1) penawaran khusus, sesuai dengan yang diatur dalam peraturan lain,</p>

<p>2) promosi, sesuai dengan yang diatur dalam peraturan lain,</p>

<p>3) diskon, sesuai dengan yang diatur dalam peraturan lain.</p>

<p><b>V. Pembatalan pesanan</b></p>

<p>Pembeli dapat membatalkan Pemesanan jika Pemesanan tidak diproses atau dikirimkan kepada Pembeli. Untuk tujuan ini, silahkan hubungi Penjual melalui formulir kontak di Situs untuk pembatalan pesanan yang belum dibayar dengan melaporkan nomor konfirmasi Pesanan, dengan alamat email dan nama lengkap. Data harus cocok dengan data Pembeli. Dalam kasus ketidakpatuhan dengan kondisi ini, khususnya jika informasi mengenai pembatalan pesanan tidak dikirim melalui formulir kontak, pesanan tidak akan dibatalkan dan akan dikirimkan, yaitu Pembeli akan menanggung semua biaya sesuai dengan perjanjian jual beli. Kesempatan untuk membatalkan Pesanan yang dijelaskan pada bagian Syarat dan Ketentuan ini, berlaku hanya untuk pesanan yang tidak dibayar. Pesanan dibayar dimuka diproses secara otomatis dan tidak ada kesempatan untuk membatalkannya.</p>

<p><b>VI. Pengembalian dan penukaran produk</b></p>

<p>1. Adanya retur penjualan atau kemungkinan produk dikembalikan oleh Pembeli karena adanya ketidaksesuaian atau kelebihan dengan barang yang dipesan. Jika terjadi kerusakan Produk ataupun kesalahan pengiriman yang diakibatkan oleh kesalahan Penjual, pengembalian Produk dapat dilakukan dengan menghubungi Penjual terlebih dahulu. Pembeli berhak menolak untuk menerima barang tersebut dan Penjual harus mengirim barang lagi.</p>

<p>2. Jika terjadi kerusakan mekanis Produk dilakukan oleh Pembeli, pengembalian Produk tidak dapat dilakukan.</p>

<p>3. Produk dikembalikan oleh Pembeli harus dikemas sehingga tidak rusak selama transportasi.</p>

<p>4. Dalam rangka peningkatan pelayanan, Produk dikembaikan harus ditandai dengan nomor pengembalian, yaitu Order ID. Nomor ini dapat mendapat melalui Situs atau formulir kontak. Nomor Order ID tersebut harus ditandai dengan jelas. Jika nomor Order ID tidak ditandai dengan jelas, Penjual tidak memberikan jaminan menerimanya.</p>

<p><b>VII. INFORMASI MENGENAI HAK UNTUK MEMBATALKAN PERJANJIAN JUAL BELI (hak untuk membatalkan Perjanjian jual beli tidak berlaku untuk Produk suplemen makanan dan pangan, jika kemasannya telah dibuka)</b></p>

<p>1. Hak untuk membatalkan Perjanjian jual beli</p>

<p>Pembeli berhak membatalkan Perjanjian jual beli dalam waktu 14 hari tanpa memberikan alasan apapun.</p>

<p>Membatalkan Perjanjian jual beli harus dilakukan dalam waktu tidak melebihi 14 hari terhitung dari hari barang diterima oleh Pembeli atau pihak ketiga yang dipilih oleh Pembeli (bukan seorang kurir).</p>

<p>Untuk mempunyai hak untuk membatalkan Perjanjian jual beli harus menghubungi layanan Situs melalui Alamat kontak Situs atau melalalui Alamat email Situs dan memberitahu mengenai keputusan untuk membatalkan Perjanjian jual beli melalui pernyataan tegas (yaitu surat dikirim lewat email atau melalui pos).</p>

<p>Pembeli dapat menggunakan contoh surat pembatalan perjanjian untuk membatalkan Perjanjian jual beli, namun ini tidak diwajibkan. Pembeli dapat mengisi dan mengirim formulir untuk membatalkan Perjanjian jual beli atau surat lain yang memberitahu mengenai keputusan untuk membatalkan Perjanjian jual beli melalui pernyataan tegas, melalui email di Situs kami. Jika Pembeli menggunakan cara ini, kami akan mengirimkan sebuah email konfirmasi mengenai menerima informasi mengenai membatalkan Perjanjian jual beli dengan perangkat penyimpanan (misalnya melalui email).</p>

<p>Untuk mempunyai hak untuk membatalkan Perjanjian jual beli dalam waktu 14 hari silahkan mengirimkan informasi mengenai membatalkan Perjanjian jual beli sebelum jarak waktu tersebut.</p>

<p>2. Membatalkan Perjanjian jual beli dan hasilnya</p>

<p>Biaya yang telah dibayarkan dikembalikan pada saat pembatalan Perjanjian jual beli, termasuk biaya pengiriman (kecuali biaya lain yang dihasilkan dari berbagai macam pilihan pengiriman, jika dipilih oleh Pembeli; itu karena kami menawarkan jasa pengiriman barang paling populer dan murah), secara cepat, dalam waktu tidak melebihi 14 hari terhitung dari hari diterima keputusan Pembeli untuk membatalkan Perjanjian jual beli. Biaya akan dikembalikan melalui transfer atau dengan uang cash, tergantung cara pembayaran yang dipilih sebelumnya; atau cara lain, sesuai dengan keinginan Pembeli; dalam hal apapun Pembeli tidak akan membayar apapun pada saat pembatalan Perjanjian.</p>

<p>Pihak Penjual dapat menunggu kembalinya uang sampai menerima Produk atau sampai menerima bukti pengirimannya, tergantung dari hasil peristiwa pertama.</p>

<p>Silahkan mengirim kembali Produk ke Alamat kontak Situs secara cepat yaitu dalam waktu tidak melebihi 14 hari terhitung dari hari diterima oleh kami keputusan Pembeli untuk membatalkan Perjanjian jual beli. Harus mengirim kebali Produk tersebut dalam waktu tidak melebihi 14 hari.</p>

<p>Pembeli harus membayar biaya mengembalikan barang. Biaya tersebut diperkirakan sekitar IDR 90,000. Informasi lengkap mengenai biaya tersebut dan ongkos pengiriman bisa dibaca di Situs.</p>

<p>Pembeli bertanggung jawab hanya untuk mengurangi nilai Produk yang dihasilkan dari menggunakannya dengan cara yang berbeda dari yang diperlukan untuk verifikasi macam dan kualitas Produk dan cara kerjannya.</p>

<p><b>VIII. Keluhan</b></p>

<p>1. Keluhan mengenai pelanggaran hak-hak Pembeli sebagai pengguna Situs dan mengenai mengadakan Perjanjian jual beli melalui Situs harus dikirimkan ke Alamat email Situs atau melalui pos ke Alamat kontak Situs.</p>

<p>2. Pembeli harus menulis nama depan dan nama belakang, alamat kontak, alamat email, informasi mengenai subjek keluhan dan sikap Pembeli terhadap soal ini.</p>

<p>3. Dalam prosedur umum untuk menangani keluhan, Penjual berhak melakukan verifikasi terhadap informasi yang diberikan yaitu Penjual dapat menghubungi Pembeli mengenai keluhan dan mengenai informasi lain, dalam kasus:</p>

<p>1) subjek keluhan atau sikap Pembeli kurang jelas, atau</p>

<p>2) harus mengidentifikasi masalah yang terjadi dan menangani keluhan dengan benar.</p>

<p>Jika tidak ada jawaban atau Pembeli tidak mengambil langkah-langkah untuk menjelaskan, Penjual dapat membatalkan keluhan.</p>

<p>4. Dalam waktu 14 hari (yaitu dalam waktu tidak melebihi 14 hari untuk mengirim pesanan atau email, sebagai menjawab terhadap keluhan) terhitung dari hari keluhan diterima, Penjual menjawab Pembeli mengenai keluhan, jawaban tersebut dikirimkan ke email Pembeli atau alamat pos yang diberikan dengan surat keluhan pelanggan, dan periode ini dapat diperpanjang dalam kasus memberikan informasi baru oleh Pembeli, setelah melakukan keluhan, tetapi tidak lebih dari satu bulan.</p>

<p>5. Pembeli selalu dapat meminta memecahkan masalah keluhan dengan bantuan pihak ketiga yaitu:</p>

<p>1) meminta bantuan mediasi yaitu meminta mediator yang terdaftar pada Pusat Mediasi Nasional,</p>

<p>2) membuat aplikasi ke pengadilan arbitrase yaitu arbitrase dalam penyelesaian permasalahan bisnis,</p>

<p>3) jika pihak Pembeli adalah seorang konsumen – meminta bantuan kepada advokat konsumen untuk memecahkan masalah keluhan.</p>

<p>6. Untuk menjelaskan semua hal itu secara terperinci: yang dijelaskan diatas pada bagian 5 tidak membuat klausula arbitrase atau kesepakatan untuk berpartisipasi dalam mediasi oleh pihak Penjual.</p>

<b>IX. Umum</b>

<p>1. Produk yang tersedia di Situs dan perjanjian ini diatur berdasarkan hukum tempat penjual. Seorang pembeli yang tertarik dengan tawaran penjual tersebut, harus mengetahui sendiri mengenai hukum lokal berkaitan dengan Produk tersedia di Situs yaitu pembelian, penyimpanan dan penggunaanya, dan mematuhi hukumnya.</p>

<p>2. Harga dalam mata uang lokal. Harga yang tersedia mencakup pajak tambahan dan biaya lainnya lagi, sesuai dengan hukum lokal, sudah termasuk pajak dan termasuk biaya pengiriman.</p>

<p>3. Jika ditempat tinggal Pembeli ada biaya lokal lainnya atau pajak lainnya, misalnya pajak impor (BM, PPN, PPH), maka Pembeli sendiri harus melakukan pembayaran tersebut.</p>

<p>4. Semua nama Produk yang disebutkan digunakan untuk tujuan identifikasi, dan mungkin merupakan merek dagang atau merek dagang terdaftar dari perusahaan masing-masing.</p>

<p>5. Seluruh konten Situs ini dilindungi oleh undang-undang hak cipta, dan seluruh konten Situs ini yaitu teks, data, grafik, citra, merek dagang, logo, ikon, konten Syarat dan Ketentuan, konten dalam Situs ini dilarang untuk dipublikasikan, dimodifikasi, disalin, direproduksi, digandakan atau diubah dengan cara apa pun, secara keseluruhan atau sebagian.</p>

<p>6. Jika seluruh atau sebagian dari Sayarat dan Ketentuan ini tidak sah, tidak dapat dilaksanakan atau ilegal menurut yurisdiksi/hak hukum maka ketentuan ini terputus untuk yurisdiksi tersebut. Sisa dari Syarat dan Ketentuan ini memiliki kekuatan penuh dan keabsahan atau berlakunya ketentuan tersebut tidak terpengaruh oleh yurisdiksi lainnya.</p>

<p>7. Syarat dan Ketentuan ini merupakan bagian integral dari Perjanjian jual beli.</p>

<p>8. Setelah mengadakan Perjanjian jual beli antara Penjual dan Pembeli, Syarat dan Ketentuan dapat diubah hanya dengan kesepakatan para pihak Penjual dan Pembeli.</p>

<p>9. Data pribadi Pembeli dimiliki dan diproses oleh Penjual dalam proses mengadakan Perjanjian jual beli. Pembeli memiliki hak untuk mengakses dan membetulkan data pribadi Pembeli. Memberikan data pribadi bersifat sukarela, tetapi diperlukan untuk mengadakan Perjanjian jual beli.</p>

<p>10. Undang-undang ini mulai berlaku pada tanggal 02.15.2016 tahun.</p>

<p>Contoh surat pembatalan perjanjian</p>

<p>(formulir ini harus mengisi dan mengirim hanya dalam kasus keinginan untuk membatalkan Perjanjian)</p>

Nama:<br>	
Email:<br>	
Dengan ini saya/kami* informasikan bahwa saya/kami* membatalkan Perjanjian kontrak kita, berlaku untuk produk berikut:<br>
Order ID:<br>	
Tanggal diterima barang:<br>	
Nama depan dan nama belakang Pembeli:<br>	
Alamat Pembeli:<br>
Tanda tangan:<br>	
Tanggal:<br>
