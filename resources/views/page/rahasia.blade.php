@extends('layout.rahasia',[
    'title' => 'Rahasia Menurunkan Berat Badan'
])

@section('analytics')
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-83521033-6', 'auto');
        ga('send', 'pageview');

    </script>
@endsection

@section('content')
    <div id="header">
        <div class="container">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="http://health.medical-jurnal.com">Home <span class="sr-only">(current)</span></a></li>
                            <li><a href="http://health.medical-jurnal.com">Fitness & Olahraga</a></li>
                            <li><a href="http://health.medical-jurnal.com">Tips Kesehatan</a></li>
                            <li><a href="http://health.medical-jurnal.com">Makanan & Diet Sehat</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </div>
    <div id="breadcrumb">
        <div class="container">
            <ol class="breadcrumb">
                <li><a href="http://health.medical-jurnal.com">Home</a></li>
                <li><a href="http://health.medical-jurnal.com">Berita</a></li>
                <li class="active">Rahasia Menurunkan Berat Badan!</li>
            </ol>
        </div>
    </div>
    <div class="container">
        <div class="col-md-8">
            <h1 style="text-align:center;">Seorang aktris dituntut ke pengadilan, karena membuka rahasia menurunkan berat badan!</h1>
            <center>
                <img id="actress" src="{{ asset('build/images/frontend/actress.jpg') }}" style="margin-bottom:20px;" alt="">
            </center>
            <p>Seorang aktris dari India, yang bernama Rucha Hasanis, dituntut ke pengadilan oleh Persatuan Ahli Gizi India 1 bulan yang lalu. Itu disebabkan karena ia membuka <a href="{{ route('page.preorder')}}">Rahasia Menurunkan Berat Badan</a> Dengan Cepat Setelah Melahirkan di sebuah program TV. Setelah program itu ditayangkan, para ahli gizi, Super Gym dan para pelatih pribadi... mereka kehilangan pelanggan! Semakin hari jumlah pelanggan semakin berkurang. &quot;Sekarang orang India tidak tertarik dengan bantuan yang kami tawarkan,&quot; ucap Muhammad Pobianto, CEO Super Gym, seperti dikutip Jakarta Post, Senin (21/09/2016). Akhirnya dia menang gugatan ke Persatuan Ahli Gizi India itu dan mendapatkan Rp 13,7 juta. Berikut ini wawancara dengan Rucha Hasanis.</p>
            <h4>Ibu Hasanis, tolong beritahu kami, apa yang terjadi?</h4>
            <p><img src="{{ asset('build/images/frontend/actress.png') }}" style="float:left;margin-right:10px;" alt="">Ibu Hasanis: Semua peristiwa ini dimulai beberapa bulan yang lalu, di sebuah program TV, ketika saya bercerita tentang cara ampuh untuk <a href="{{ route('page.preorder')}}">menurunkan berat badan</a> dengan mudah dan cepat, setelah melahirkan dan menyusui. Di program TV tersebut saya juga bercerita, bahwa saya pernah pergi ke Super Gym, dan saya tidak merekomendasikan solusi yang ditawarkan oleh para ahli gizi. Kenapa? Karena untuk <a href="{{ route('page.preorder')}}">menurunkan berat badan</a> ada 1 cara jauh lebih mudah dari kedua cara sebelumnya, <a href="{{ route('page.preorder')}}">cara murah dan alami</a>. Satu minggu setelah peristiwa itu terjadi, saya mendapat surat dari Persatuan Ahli Gizi India tertanggal 23 Juli 2016. Mereka meminta saya… untuk membantah apa yang saya katakan di media, karena kalau saya mengatakan sesuatu di media, pihak mereka tidak bisa membantah apa yang saya katakan!</p>
            <p>Tentu saya tidak melakukan itu, karena yang saya katakan memang benar yang sudah terbukti. Sekarang kebanyakan orang tahu bagaiamana cara <a href="{{ route('page.preorder')}}">menurunkan berat badan</a> tanpa diet, tanpa usaha apapun – yaitu tanpa para ahli gizi, tanpa Super Gym dan tanpa pelatih pribadi apapun... mereka kehilangan uang dan pelanggan, hal itu jelas. Saya bisa mengerti hal itu. Saya mengerti kemarahan mereka, tapi jangan lupa bahwa ada <a href="{{ route('page.preorder')}}">cara paling ampuh untuk menurunkan berat badan</a>, <a href="{{ route('page.preorder')}}">cara melangsingkan badan dengan cepat</a>, dengan cara alami!</p>
            <h4>Apa yang terjadi kemudian?</h4>
            <p>Hal inilah yang memunculkan hasutan-hasutan dan fitnah-fitnah terhadap saya di social media, mereka menelpon ke tempat kerja saya dan lain-lain… Mereka entah bagaimana menemukan nomor HP pribadi saya, dan menelpon dan mengancam. Akibatnya, saya mengganti nomor ponselku dan bahkan sewa pengawal pribadi, karena takut mereka. Tentu saya tak akan pernah lupa peristiwa itu!</p>
            <center><img id="fb" src="{{ asset('build/images/frontend/fb.png') }}" alt="" style="margin:20px 0;"></center>
            <p>Lalu saya mendapat surat dari pengadilan negeri untuk mengikuti sidang.</p>
            <center><img id="letter" src="{{ asset('build/images/frontend/letter.jpg') }}" alt=""></center>
            <h4>Apakah Anda terkejut dengan surat ini?</h4>
            <p>Ibu Hasanis: Tidak, saya marah! Mereka siap melakaukan apa saja demi uang, mereka menipu orang lain. Tentu saya memenangkan perkara ini, karena memiliki alat bukti kuat, dan saya merasa sebagai pihak yang benar. Akibatnya saya menang gugatan ke Persatuan Ahli Gizi India itu dan mendapatkan Rp 13,7 juta. Lalu saya mentransfer uang ini ke nomor rekening bagi orang miskin.</p>
            <h4>Silahkan beritahu kami, soal apa ini?</h4>
            <p><a href="{{ $voluum }}"><img src="{{ asset('build/images/frontend/garciniaCambogia.jpg') }}" style="float:right;width:250px;" alt=""></a>Penyebab peristiwa itu? Karena saya membuka <a href="{{ route('page.preorder')}}">rahasia <a href="{{ route('page.preorder')}}">menurunkan berat badan</a> di media, dengan cara baru, namanya <a href="{{ route('page.preorder')}}">Garcinia Cambogia Forte</a>.</p>
            <p>Ibu Hasanis: Cara ampuh yang direkomendasikan oleh teman saya. Dia bilang bahwa dengan cara ini dia berhasil turun 9 kg dengan cepat dan membersihkan tubuh dari racun-racun. Pada awal saya tidak percaya, tapi ini keajaiban, ini benar! Setelah 1 bulan, saya terkejut dengan hasilnya, <a href="{{ route('page.preorder')}}">saya berhasil turun 12 kg!</a> Selain itu, dengan cara ini, saya meningkatkan tenaga badan saya sehingga dapat beraktifitas dan bekerja lebih baik. Sulit dipercaya tapi memang ada: saya juga sudah <a href="{{ route('page.preorder')}}">mengatasi kulit bermasalah</a> dengan cara ini. Setelah terapi, setelah pemakaian <a href="{{ route('page.preorder')}}">Garcinia Cambogia Forte</a> ini saya terlihat lebih muda dari usia saya yang sebenarnya. Sekarang saya merasa lebih sehat dan segar, dan banyak orang selalu memuji saya terlihat cantik! <a href="{{ route('page.preorder')}}">Garcinia Cambogia Forte</a> itu luar biasa aja, saya terkejut dengan hasilnya, saya tidak bisa diamkan itu!</p>
            <h4>Apakah cara ini <a href="{{ $voluum }}">tersedia untuk khalayak umum?</a> Pasti mahal?</h4>
            <p>Ibu Hasanis: Tidak! Murah, dan dibandingkan dengan cara lain, seperti: bantuan dari para ahli gizi itu mahal, Super Gym itu mahal, pelatih pribadi juga mahal – ini sangat murah! Dan jangan lupa: menghemat waktu sama pentingnya menghemat uang. Dan <a href="{{ route('page.preorder')}}">Garcinia Cambogia Forte</a> itu <a href="{{ $voluum }}">tersedia untuk orang rata-rata</a>. Memang, pada saat ini tersedia hanya di toko online, cara memesannya – melalui halaman resmi.</p>
            <h4>Terima kasih atas wawancara ini, sangat menarik. Apakah Anda ingin menunjukkan sesuatu kepada para pembaca kami?</h4>
            <p>Ibu Hasanis: Sama-sama! Saya mengharapkan kepada semua orang yang membaca pengalaman saya ini... Jangan tertipu dengan para ahli gizi, jangan tertipu dengan para pelatih pribadi, karena mereka biasanya hanya ingin uang Anda saja! Jangan kehilangan uang Anda! Diet dan olahraga tidak bermanfaat bagi semua! Diet ketat dan olahraga kadang-kadang berbahaya bagi kesehatan. Selain itu, jangan lupa, bahwa menghemat waktu sama pentingnya menghemat uang. Sekarang ada cara baru untuk <a href="{{ route('page.preorder')}}">menurunkan berat badan</a> tanpa usaha apapun, dengan mudah - tanpa resiko apapun, cara aman dan alami.</p>
            <p><strong>Semoga sukses!</strong></p>
            <div class="center-button">
                <a href="{{ $voluum }}" class="btn btn-danger btn-lg">Mengenai Garcinia Cambogia Forte</a>
            </div>
        </div>
        <div class="col-md-4" style="text-align:center;">
            <h3 style="text-transform:uppercase;">Apa yang perlu kamu ketahui</h3>
            <a href="{{ $voluum }}">
                <img id="garciImg" src="{{ asset('build/images/frontend/garciniacambogia.jpg') }}" alt="">
            </a>
        </div>
    </div>
    <div id="comment-section">
        <div class="container">
            <div class="col-md-8">
                <h3>KOMENTAR</h3>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_1.jpg') }}" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Emma Ramayanti | 22.09.2016 - 15:55</h4>
                        Siapa sdh coba? Mau pesan ya, buat ibu saya, tapi saya sendiri juga mau deh!
                    </div>
                </div>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_7.jpg') }}" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Erni Puri | 23.09.2016 - 05:09</h4>
                        Gue udah pesan, cara pembayaran COD, pembayaran di tempat, gue tertarik, dan lagi nunggu. Teman gue bilang bahwa dia berhasil turun 8 kg dlm 2 minggu ya ia merasa lebih sehat dan cantik, lumayan!
                    </div>
                </div>
                <div class="media">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_2.jpg') }}" alt="">
                        </a>
                    </div>
                    <div class="media-body">
                        <h4 class="media-heading">Widia Resdiana | 23.09.2016 - 20:54</h4>
                        Sudah pesan 1 minggu lalu, sudah berhasil turun 4 kg, sayang aku tak tahu cara ini sebelumnya ya … Recommended banget!
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_3.jpg') }}" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Melly Parimita | 24.09.2016 - 15:18</h4>
                            Memang, murah, untuk sosok ramping, untuk kehilangan beberapa kg ya… Tapi ada pertanyaan: metde pembayaran transfer atau cod, di tempat??
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_5.jpg') }}" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Cindy Nurhayati | 25.09.2016 - 12:27</h4>
                            Melly pembayaran di tempat, jadi jangan khwatir ya… pesan sekarang juga dan tunggu barang saja ya.
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_6.jpg') }}" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Aminatun Isdawimah | 26.09.2016 - 12:28</h4>
                            <a href="{{ route('page.preorder')}}">Garcinia Cambogia Forte</a> – ya! Udah pakai selama 1 bln, dan turun 18 kg!!! tanpa efek samping. Tanpa usaha, semuanya beres, hilangkan lemak di perut dan paha dng cepat. Aku senang dengan obat ini, sdh rekomendasikkan temanku dan juga berhasil turun, obat ini sangat membantu.
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_4.jpg') }}" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Nurfitria Onida | 27.09.2016 - 15:26</h4>
                            Ya, aku setuju dengan semua. Sdh gunakan enam bulan dan berhasil turun 14 kg per 1 bulan. Tanpa efek yoyo! Bagi Anda yang ingin menurunkan berat dng cepat.
                        </div>
                    </div>
                    <div class="media">
                        <div class="media-left">
                            <a href="#">
                                <img class="media-object img-thumbnail" src="{{ asset('build/images/frontend/comm_10.gif') }}" alt="">
                            </a>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Ida Tita | 28.09.2016 - 21:28</h4>
                            ya itu benar di sebuah program TV yg lain, ku sdh mendengarkan tentang cara baru ini. mereka bilang bahwa hasilnya sdh terbukti secara ilmiah, cara ampuh untuk turunkan berat, cara alami.
                        </div>
                    </div>
                    <div class="center-button">
                        <a href="{{ $voluum }}" class="btn btn-danger btn-lg">Pesan Garcinia Cambogia Forte</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <div id="footer">

        </div>
@endsection
