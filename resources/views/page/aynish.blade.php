@extends('layout.app',[
    'title' => 'Bagaimana berat badan saya turun dari 85kg menjadi 54kg hanya dalam 2 bulan'
])

@section('styles')
    <link rel="stylesheet" href="{{ elixir('css/aynish/all.css') }}">
@endsection

@section('content')
    <header class="header container">
        <div class="row">
            <div class="span12">
                <div id="logo">
                    <span title="Melani Kurniawan's Blog"></span>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="post-194 post type-post status-publish format-standard has-post-thumbnail hentry category-blog tag-basecamp-kledung tag-gunung-sindoro tag-hiking tag-mendaki tag-pendakian tag-sindoro tag-sunrise pin-article span12 full">
                <div class="pin-container">
                    <img alt="" class="attachment-blog wp-post-image" src="{{asset('build/images/frontend/gr-coffee.jpg')}}">
                </div>
                <article class="article">
                    <h1 class="title">
    <span>Bagaimana berat badan saya turun dari 85kg menjadi 54kg hanya dalam 2 bulan
    </span>
                    </h1>
                    <div class="line">
                        <div class="entry-info viewcomments">
    <span class="entry-date">
    <i class="icon-time"></i>
    <span class="rdate">09.11.2016 - 17:39</span>
    </span>
                        </div>
                    </div>
                    <p>
                        Halo! Semua orang mulai sadar bahwa saya baru saja menurunkan berat badan saya secara drastis dan mulai memborbardir
                        saya dengan pertanyaan. Saya berusaha sebaik-baiknya untuk menjawabpertanyaan untuk membantu kalian semua, tapi gagal
                        – karena banyaknya pesan yang harus saya balas saya harus terus-menerus online. Sehingga, saya memutuskan untuk
                        menulis post ini untuk menjawab pertanyaan : „Bagaimana cara anda bisa menuurunkan 31kg?” (Ini bukan berarti anda
                        tidak bisa menulis kepada saya atau menanyakan sesuatu, hal ini hanya untuk membantu saya).
                    </p>
                    <h3 align="center">Transformasi saya hanya membutuhkan waktu DUA BULAN! HASIL YANG MENAKJUBKAN, BUKAN?</h3>
                    <div class="wp-caption aligncenter" id="attachment_179">
                        <a href="{{ $voluum }}">
                            <img alt="logistic sindoro" class="size-medium wp-image-179" src="{{asset('build/images/frontend/51711c_5f5e14230a18484db3e0adb19a2d409f.jpg')}}">
                        </a>
                    </div>
                    <h3 style="color: rgb(93, 134, 15);">Latar Belakang</h3>
                    <p></p>
                    <h3 align="center"></h3>
                    <p><b></b></p>
                    <p>
                        Saya memang tidak pernah kurus, namun saya melihat bahwa berat badan saya terus bertambah dan bertambah. Tiga
                        lipatan yang mengerikan muncul pada perut saya, saya terlihat seperti seekor bulldog menggunakan bikini, dan
                        pinggul, kaki, dan bokong saya terus membesar dan membesar! Saya memutuskan untuk memiliki tubuh yang fit dan
                        sehat bagaimanapun caranya, sehingga saya berhenti makan makanan berminyak dan gorengan dan berhenti ngemil pada
                        malam hari. Saya mulai lari di pagi hari dan olahraga di sore hari di gym tapi bukannya berat badan turun, berat
                        badan saya malahan terus bertambah lebih banyak lagi! Setelah sebulan, berat badan saya mencapai nilai yang
                        mengerikan – NYARIS 100KG! Saya mulai membuat diet saya menjadi lebih ketat, pantang memakan daging, roti, kentang
                        goreng dan makanan manis. Pada akhirnya saya hanya memakan buah-buahan dan sayuran dan hanya meminum air putih saja.
                        Saya mencoba beberapa teh dan pil penurun berat badan yang sangat mahal tapi sepertinya tidak ada yang cocok untuk
                        saya. Beberapa waktu kemudian, berat badan yang saya turunkan akan kembali kepadaku.
                    </p>
                    <p><b>Diet dan pil tidak membantu, saya merasa tertipu!</b></p>
                    <p><b>
                            Olahraga sangat berat dan butuh waktu yang sangat lama untuk melihat hasilnya. Jadi apa yang harus saya lakukan?
                        </b></p>
                    <p>
                        Setelah sejumlah diet, pil dan berjam-jam di gym dan jutaan uang saya telah habis untuk instruktur personal
                        akhirnya saya menyerah seluruhnya. Suatu hari saya kebetulan melihat sebuah artikel tentang <b>Garcinia Cambogia</b>
                        dan memutuskan untuk mencobanya. Walaupun saya pernah mendengar Demi Moore, Katy Perry, J-Lo dan banyak artis
                        lainnya menurunkan berat badan dengan Garcinia, awalnya saya skeptis. Namun setelah mencoba segalanya dan sekarang
                        saya putus asa, saya tidak punya pilihan lagi! Di samping itu, saya melihat ulasan tentang produknya dan ulasannya
                        sangat bagus!
                    </p>
                    <p align="center"><b>
                            Jutaan wanita di Eropa dan Amerika Serikat menurunkan berat badan mereka berkat <a href="{{ $voluum }}"> Garcinia Cambogia.</a>
                            Menurut penelitian, 96,7% dari mereka menurunkan 12-17kg dalam 3 minggu!
                        </b></p>
                    <p>
                        Jadi telah diputuskan! Saya mengunjungi website mereka, mengecek ulang segalanya dan memesan ekstrak Garcinia.
                        Paketnya dating dalam beberapa minggu. Saya membaca instruksinya dan mulai mengonsumsi 1-3 pil tiap harinya,
                        setelah makan pagi dan makan siang.
                    </p>
                    <div class="wp-caption aligncenter" id="attachment_144">
                        <a href="{{ $voluum }}">
                            <img alt="" class="size-medium wp-image-144" src="{{asset('build/images/frontend/garciniacambogia.jpg')}}" width="300" height="313">
                        </a>
                    </div>
                    <h3 style="color: rgb(93, 134, 15);">Hasil</h3>
                    <p>
                        Hanya dalam 2 minggu, hasilnya sungguh mengagumkan – berat badan saya turun 6kg! Pipi tembem saya hilang dan kulit
                        saya menjadi jauh lebih baik! Pinggul dan perut saya terlihat lebih kecil – dan mood saya mulai membaik! Saya mulai
                        percaya saya dapat terlihat cantik kembali tanpa diet ataupun olahraga! Sehingga, saya meneruskan memakan makanan
                        yang saya inginkan! Saya sudah cukup lelah berolahraga dan kelaparan…
                    </p>
                    <p>
                        Pada akhir dari minggu ketiga, saya menurunkan 12kg lagi! Saya menjadi lebih fit dan hal ini memotivasi saya
                        untuk menaiki tangga, daripada menggunakan lift. Yang dulunya merupakan suatu kegiatan yang berat sekarang menjadi
                        dengan mudah saya lakukan! Saya tidak percaya hal ini terjadi hanya karena saya mengikuti instruksi sederhana pada
                        kemasan Garcinia! Jika saya dapat melakukannya – semua orang bisa! Hanya 2 bulan dan 31kg berat badan saya hilang!
                    </p>
                    <p>
                        Saya mencapai berat badan ideal saya dalam 60 hari. Perut saya telah hilang, saya menghilagkan 31kg dan
                        mendapatkan tubuh yang dulunya tidak berani saya impikan!
                    </p>
                    <h3 align="center">Bahkan sampai sekarang, saya masih tidak percaya pada apa yang saya lihat di cermin.</h3>
                    <div class="wp-caption aligncenter" id="attachment_144">
                        <a href="{{ $voluum }}"><img alt="" class="size-medium wp-image-144" src="{{asset('build/images/frontend/51711c_131639a5c4874da8ba7f9a891701dff2.jpg')}}"></a>
                    </div>
                    <p><b>
                            Dan juga, <a href="{{ $voluum }}"> Garcinia Cambogia</a> telah diuji oleh National Academy of Sciences pada tahun 2015. Hasilnya seharusnya
                            membooming namun tidak pernah dipublikasi.
                        </b>
                    </p>
                    <p><b>
                            Saya kira hal ini karena mereka akan membuat perusahaan obat, klub fitness, ahli nutrisi dan klinik-klinik mahal bangkrut!
                        </b>
                    </p>
                    <p><b>
                            Tidak diragukan lagi :<a href="{{ $voluum }}"> Garcinia Cambogia</a> sangat murah!
                        </b>
                    </p>
                    <p>
                        Sekarang saya terus merekomendasikan <a href="{{ $voluum }}"> Garcinia Cambogia</a> kepada teman dan kolega saya. Penting : pesanlah produk
                        resmi dari website kami agar anda tidak mendapat produk palsu. Saya sangat senang dengan hasil yang saya dapatkan
                        dan saya percaya anda akan senang juga! Lupakan stereotip: anda tidak membutuhkan diet dan olahraga untuk
                        mendapatkan figur tubuh yang indah.
                    </p>
                    <p style="font-size: 25px;" align="center"><b> Berhenti memimpikan tubuh sempurna! Raihlah sendiri!</b></p>
                    <p><i>
                            N.B. Semua yang menurunkan berat badan hanya dengan mengonsumsi <a href="{{ $voluum }}"> Garcinia Cambogia</a>, tolong bagikan hasil anda
                            disini! Anda akan membantu meyakinkan orang lain bahwa ini memang sangat membantu!
                        </i></p>
                    <p><b>
                            Halo! Ada kabar baik untuk anda! Perwakilan dari perusahaan <a href="{{ $voluum }}"> Garcinia Cambogia</a> menghubungi saya dan
                            menawarkan diskon 50% bagi 100 pengunjung pertama dari blog saya yang memesan disini!
                        </b></p>
                    <div align="center">
                        <a class="ord_button" href="{{ $voluum }}">BAWA SAYA KE PENAWARAN</a>
                    </div>
                    <div class="clear"></div>
                    <div class="comments-area" id="comments">
                        <div class="comments-area-title">
                            <h4 class="hN">
                                <em>Komentar</em>
                            </h4>
                        </div>
                        <ol class="commentlist">
                            <li class="comment even thread-even depth-1">
                                <article class="comment-article media" id="comment-620711">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/comm_24.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Aishwarya Rai</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">10.11.2016 - 06:53</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Dear Aynish, terima kasih telah menginspirasi saya untuk akhirnya berhasil menghilangkan
                                                kg yang mengerikan itu! 3 minggu telah berjalan dan saya menurunkan 28kg. Berat badan saya sekarang
                                                57kg, dan seperti yang anda janjikan, tidak lagi naik turun. Salam hangat, Michelle.
                                            </p>
                                            <img alt="" src="{{asset('build/images/frontend/review_3.jpg')}}">
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment even thread-odd thread-alt depth-1">
                                <article class="comment-article media" id="comment-620801">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/51711c_27fdcb39c2a64559b08fba3b52f9961f.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Nirjara
                                            </cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">10.11.2016 - 22:38</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Saya sudah memimpikan tubuh yang langsing sejak lama, saya membaca tentang Garcinia
                                                Cambogia pada halaman ini 3 bulan lalu. Saya ragu-ragu untuk memesan namun ibu saya membujuk
                                                saya. Harganya sangat terjangkau untuk mewujudkan mimpi saya! Sekarang kami menurunkan berat
                                                badan bersama. Namun dia kelihatannya lebih sukses dibanding saya =(
                                            </p>
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment even thread-even depth-1">
                                <article class="comment-article media" id="comment-620858">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/76103_original.png')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Yi Ling</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">11.11.2016 - 17:02</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>Sangat senang saya menemukan blog ini. Saya pasti akan mencoba pil ini! Terima kasih</p>
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment even thread-even depth-1">
                                <article class="comment-article media" id="comment-620945">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/51711c_f508fb625d684710afde29394404e566.png')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Suman</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">12.11.2016 - 14:11</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Hai Aynish! Saya mengikuti nasihatmu dan mencoba <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b> dan yah… Anda menyelamatkan
                                                keluarga dan pernikahan saya =) Aynish, anda luar biasa! Sikap suami saya sangat berubah kepada
                                                saya akhir-akhir ini, dan saya mendapatkan kepercayaan diri lebih… Sekarang saya merasa SANGAT BAIK! =)
                                            </p>
                                            <img alt="" src="{{asset('build/images/frontend/51711c_4d48fbe6ae16493f9dc658dce4f1299a.png')}}" width="500">
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment even thread-odd thread-alt depth-1">
                                <article class="comment-article media" id="comment-620955">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/com.png')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name"> Shreya</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">13.11.2016 - 14:11</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Hai para wanita, saya akan segera bergabung dengan tim langsing!!! Saya baru saja mendapatkan
                                                <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b> saya! Terima kasih telah membagikan hasilmu dan hasilnya memotivasi saya untuk
                                                memulai ini. Semoga pada akhirnya saya akan menghilangkan semua lemak-lemak saya. Saya akan menulis
                                                perkembangan saya disini juga xoxo
                                            </p>
                                        </section>
                                    </div>
                                </article>
                                <ul class="children">
                                    <li class="comment odd alt depth-2">
                                        <article class="comment-article media" id="comment-627442">
                                            <aside class="comment__avatar media__img">
                                                <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/51711c_c028f68f8bd94fdab407603100e88ee4.png')}}" style="background-size: 100% 100%;" width="60" height="60">
                                            </aside>
                                            <div class="media__body">
                                                <header class="comment__meta comment-author">
                                                    <cite class="comment__author-name">Aynish</cite>
                                                    <time class="comment__time">
                                                        <span class="comment__timestamp rdate">14.11.2016 - 17:10</span>
                                                    </time>
                                                    <div class="comment__links">
                                                    </div>
                                                </header>
                                                <section class="comment__content comment">
                                                    <p>
                                                        Halo, Shreya! Saya sangat senang untukmu! Hal yang paling penting adalah keharmonisan
                                                        dari dalam namun hal itu tidak mungkin dicapai tanpa keharmonisan dengan tubuhmu.
                                                        Saya sangat tidak sabar melihat perkembanganmu. Dan ingat : langkah kecil lebih
                                                        baik daripada tidak melangkah sama sekali! Semoga beruntung!
                                                    </p>
                                                </section>
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                            </li>
                            <li class="comment odd alt thread-even depth-1">
                                <article class="comment-article media" id="comment-620978">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/comm_6.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Aarthi</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">15.11.2016 - 23:12</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Dear Aynish! Saya sangat senang telah menemukan website kamu dan sedang
                                                mencari bantuan. Saya telah mencoba berbagai cara untuk menurunkan berat badan namun, hasil
                                                saya sangat tidak sesuai dengan harapan saya. Tinggi saya 52 inci dan berat badan saya 97kg – memalukan!!!
                                                Saya perlu menurunkan setidaknya 36kg dan <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b> sepertinya adalah harapan terakhir saya..
                                            </p>
                                        </section>
                                    </div>
                                </article>
                                <ul class="children">
                                    <li class="comment odd alt depth-2">
                                        <article class="comment-article media" id="comment-627442">
                                            <aside class="comment__avatar media__img">
                                                <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/51711c_c028f68f8bd94fdab407603100e88ee4.png')}}" style="background-size: 100% 100%;" width="60" height="60">
                                            </aside>
                                            <div class="media__body">
                                                <header class="comment__meta comment-author">
                                                    <cite class="comment__author-name">Aynish</cite>
                                                    <time class="comment__time">
                                                        <span class="comment__timestamp rdate">17.11.2016 - 08:26</span>
                                                    </time>
                                                    <div class="comment__links">
                                                    </div>
                                                </header>
                                                <section class="comment__content comment">
                                                    <p>Halo, Aarthi! Saya bukan satu-satunya yang menjadi langsing dengan meminum kopi ini!
                                                        Teman-teman saya juga mencobanya dan benar-benar BEKERJA, jadi saya menjamin 100% bahwa kamu
                                                        tidak akan kecewa lagi. Kami semua senang dengan hasil kami dan berharap kamu juga! Para
                                                        wanita, saya ingin meminta kepada anda satu kali lagi, tolong gunakan beberapa menit untuk
                                                        membagikan hasilmu karena berbagi itu baik! Mungkin pesanmu akan meyakinkan orang lain
                                                        untuk menjadi cantik langsing dan sangat bahagia!
                                                    </p>
                                                </section>
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                            </li>
                            <li class="comment even thread-odd thread-alt depth-1">
                                <article class="comment-article media" id="comment-621831">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/comm_3.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Kavita</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">18.11.2016 - 20:57</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Saya mendapatkannya kemarin dan saya akan memulai hidup baru hari ini! (Tidak sabar untuk melihat perkembangannya)
                                            </p>
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment even thread-even depth-1">
                                <article class="comment-article media" id="comment-621876">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/51711c_839646b831e74cd9bf877bb71b19499emv2_001.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Baahubali Anushkahi</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">20.11.2016 - 12:53</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Dear Aynish! Terima kasih banyak! Berat badan saya turun dengan sangat cepat namun lebih penting lagi,
                                                berat badan saya tidak kembali lagi seperti biasanya! Ajaib! Saya merekomendasikan <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b>
                                                kepada teman-teman saya karena saya tidak tega melihat mereka kelaparan setengah mati terus menerus.
                                            </p>
                                            <img alt="" src="{{asset('build/images/frontend/51711c_839646b831e74cd9bf877bb71b19499emv2.jpg')}}">
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment odd alt thread-odd thread-alt depth-1">
                                <article class="comment-article media" id="comment-621975">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/comment_9.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Lindsey</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">22.11.2016 - 08:19</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Saya ada kencan makan malam di restoran mewah besok. Beberapa minggu lalu, cukup berpikir untuk pergi
                                                dan makan disana tanpa mengetahui berpa kalori dari makanannya adalah sebuah mimpi buruk.
                                                Sekarang saya tidak peduli! Saya tahu saya bisa makan apa saja yang saya inginkan dan berat
                                                badan saya tidak akan naik. Saya menurunkan 27,5kg berat badan saya dengan bantuak <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b>
                                                dalam waktu 1 bulan saja! Terima kasih, Aynish!
                                            </p>
                                            <img alt="" src="{{asset('build/images/frontend/foto12a.jpg')}}">
                                        </section>
                                    </div>
                                </article>
                                <ul class="children">
                                    <li class="comment even depth-2">
                                        <article class="comment-article media" id="comment-622126">
                                            <aside class="comment__avatar media__img">
                                                <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/51711c_d5eb1b5f43db44cf86ab7cc5d0c2ae3b.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                            </aside>
                                            <div class="media__body">
                                                <header class="comment__meta comment-author">
                                                    <cite class="comment__author-name">Allison Jenkins</cite>
                                                    <time class="comment__time">
                                                        <span class="comment__timestamp rdate">24.11.2016 - 07:24</span>
                                                    </time>
                                                    <div class="comment__links">
                                                    </div>
                                                </header>
                                                <section class="comment__content comment">
                                                    <p>
                                                        Wow Lindsey, apakah kamu benar-benar mencapai itu dalam 1 bulan saja? Mungkin saya harus
                                                        membeli <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b> juga… Saya baru saja naik 3kg lagi, sangat tidak bagus..
                                                    </p>
                                                </section>
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                            </li>
                            <li class="comment even thread-even depth-1">
                                <article class="comment-article media" id="comment-622513">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/consumer4.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Rani</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">26.11.2016 - 10:12</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Saya turun 18kg dalam 6 minggu! Sulit dipercaya! Terima kasih Aynish!
                                            </p>
                                            <img alt="" src="{{asset('build/images/frontend/review_11.jpg')}}">
                                        </section>
                                    </div>
                                </article>
                                <ul class="children">
                                    <li class="comment odd alt depth-2">
                                        <article class="comment-article media" id="comment-622603">
                                            <aside class="comment__avatar media__img">
                                                <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/51711c_d5eb1b5f43db44cf86ab7cc5d0c2ae3b.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                            </aside>
                                            <div class="media__body">
                                                <header class="comment__meta comment-author">
                                                    <cite class="comment__author-name">Allison Jenkins</cite>
                                                    <time class="comment__time">
                                                        <span class="comment__timestamp rdate">28.11.2016 - 16:52</span>
                                                    </time>
                                                    <div class="comment__links">
                                                    </div>
                                                </header>
                                                <section class="comment__content comment">
                                                    <p>
                                                        Selamat, Rani! Hasilmu luar biasa! Sekarang saya tidak ragu lagi kepada <b><a href="{{ $voluum }}">Garcinia Cambogia!!!</a></b>
                                                    </p>
                                                </section>
                                            </div>
                                        </article>
                                    </li>
                                </ul>
                            </li>
                            <li class="comment even thread-even depth-1">
                                <article class="comment-article media" id="comment-622622">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/comm_13.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Beth</cite>
                                            <span class="comment__timestamp"><span class="rdate">03.12.2016 - 18:12</span></span>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Lupakan semua keraguanmu, obat ini benar-benar bekerja! Teman saya juga menurunkan berat
                                                badan menggunakan <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b> – sekitar 18kg dalam 2 bulan jika tidak salah. Mengesankan, bukan?
                                            </p>
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment even thread-odd thread-alt depth-1">
                                <article class="comment-article media" id="comment-622751">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" i="" src="{{asset('build/images/frontend/51711c_4ea9ba04759b4a21b697f8d2c0c7583e.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name"> Laxmi</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp rdate">04.12.2016 - 02:21</span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Terima kasih atas ulasannya, ladies! Kalian memotivasi saya untuk memesan <b><a href="{{ $voluum }}">Garcinia Cambogia</a></b> dan
                                                berusaha memperbaik diri saya! Saya sudah turun 5kg, hanya 6 kg lagi menuju berat badan tujuan saya.
                                            </p>
                                        </section>
                                    </div>
                                </article>
                            </li>
                            <li class="comment even thread-even depth-1">
                                <article class="comment-article media" id="comment-622776">
                                    <aside class="comment__avatar media__img">
                                        <img class="comment__avatar-image grav-hashed grav-hijack" src="{{asset('build/images/frontend/comm_15.jpg')}}" style="background-size: 100% 100%;" width="60" height="60">
                                    </aside>
                                    <div class="media__body">
                                        <header class="comment__meta comment-author">
                                            <cite class="comment__author-name">Sindhu</cite>
                                            <time class="comment__time">
                                                <span class="comment__timestamp"><span class="rdate">04.12.2016 - 10:32</span></span>
                                            </time>
                                            <div class="comment__links">
                                            </div>
                                        </header>
                                        <section class="comment__content comment">
                                            <p>
                                                Tiga sepupu saya berubah seluruhnya setelah meminum kopi ini beberapa bulan. Mereka berubah dari
                                                perempuan plus-size biasa menjadi wanita cantik yang mengagumkan!
                                            </p>
                                        </section>
                                    </div>
                                </article>
                            </li>
                        </ol>
                        <div align="center">
                            <a class="ord_button for_bottom" href="{{ $voluum }}">BAWA SAYA KE PENAWARAN</a>
                        </div>
                    </div>
                    <div class="clear"></div>
                </article>
                <div style="clear: both;"></div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@endsection