@extends('layout.app')
@section('title') - bahasa inggris @endsection
@section('head')
    <link rel="canonical" href="{{ route('page.index') }}" />
@endsection
@section('top')
    <header class="with-slider">
        @include('asp.1.top')
    </header>
@endsection
@section('content')

    <main>
        <section class="banner">
            <div class="container">

                <div class="col-xs-12 col-xs-offset-0 col-md-4 col-md-offset-8 underbar-contact">
                    <a href="{{ route('page.contact') }}">{{ trans('welcome.slider.got.question') }}</a><br>
                    <a href="tel:{!! str_replace(' ', '',trans('common.phone.number')) !!}">
                        {{ trans('common.phone.prefix') }} {{ trans('common.phone.number') }}
                    </a>
                </div>

                <h1 class="wow bounceIn">
                    {{--@if (GeoIP::getCountry() == 'Indonesia')--}}
                        @if ($header)
                            {{ $header->text }} <strong>{{ $header->text2 }}
                                <span>{{ $header->text3 }}</span></strong>
                        @elseif(Cookie::has('10days'))
                            {{ trans('welcome.slider.top.part_1') }} <strong>{{ trans('welcome.slider.top.part_2') }}
                                <span>{{ trans('welcome.slider.top.part_3_10') }}</span></strong>
                        @else
                            {{ trans('welcome.slider.top.part_1') }} <strong>{{ trans('welcome.slider.top.part_2') }}
                                <span>{{ trans('welcome.slider.top.part_3') }}</span></strong>
                        @endif
                    {{--@else
                        METODE <br><strong>EMIL
                            <span>KREBS</span></strong>
                    @endif--}}
                </h1>

                <div id="slider" class="owl-carousel wow fadeIn">
                    @foreach($languages as $language)
                        <div class="item" icon="{{ $language->icon_path }}">
                            <table>
                                <tbody>
                                <tr>
                                    <td class="photo">
                                        @if(Cookie::has('10days'))
                                        <img src="{{ $language->image_10_path }}" alt="{{ $language->name }}"></td>
                                        @else
                                        <img src="{{ $language->image_path }}" alt="{{ $language->name }}"></td>
                                        @endif
                                    <td class="list">
                                        <h2>{{ $language->name }}</h2>
                                        {!! Cookie::has('10days')?str_replace('30 hari', '10 hari',$language->description_short):$language->description_short !!}
                                    </td>
                                    <td class="price">

                                        <span class="price">
						                @if($isPromotion)
                                                <strong>
                                                    <span>{{ $language->currency }}</span> {{ number_format($language->price,0,',','.') }}
                                                </strong>
                                                <p class="old">
                                                    <span>{{ $language->currency }}</span>
                                                    {{ number_format($language->price_old,0,',','.') }}
                                                </p>
                                            @else
                                                <strong>
                                                    <span>{{ $language->currency }}</span> {{ number_format($language->price_old,0,',','.') }}
                                                </strong>
                                            @endif
					                    </span>


                                       <a href="{{ route('asp.clip') }}"
                                          class="btn btn-green btn-big"
                                          type="button">{{ trans('welcome.how.it.works') }}</a>

                                    </td>

                                </tr>
                                </tbody>
                            </table>
                            {{--<ul class="actions">
                                @if($language->is_active)
                                    <li>
                                        @if(Session::has('try'))
                                            <a href="{{ route('page.try') }}"
                                               class="btn btn-blue">{{ trans('welcome.slider.try.lesson') }}</a>
                                        @else
                                            <a href="#" class="trigger btn btn-blue" type="button" data-toggle="modal"
                                               data-target="#try">{{ trans('welcome.slider.try.lesson') }}</a>
                                        @endif
                                    </li>

                                    <li>
                                        <a href="{{ route('page.languages',['language' => $language->symbol]) }}"
                                           class="btn btn-green">{{ trans('welcome.slider.order') }}</a>
                                    </li>
                                @else
                                    <li>
                                        <a href="#" class="trigger btn btn-blue" type="button"
                                           data-id="{{ $language->id }}" data-toggle="modal"
                                           data-target="#inactive">{{ trans('welcome.slider.try.lesson') }}</a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn btn-green" type="button" data-id="{{ $language->id }}"
                                           data-target="#inactive"
                                           data-toggle="modal">{{ trans('welcome.slider.order') }}</a>
                                    </li>
                                @endif
                            </ul>--}}
                        </div>
                    @endforeach

                </div>
                <ul class="slider-thumbs"></ul>
            </div>
        </section>
        {{--<section class="wow fadeIn how-it-works">
            <div class="container">
                <h2>{{ trans('welcome.how.it.works') }}</h2>

                <div class="video">
                    <div class="video-bg">
                        --}}{{--
                        <img src="{{ asset("build/images/user.png") }}" alt="" class="poster">

                        <video id="how-it-works" preload="none">
                            <source src="{{ asset('movie/krebs_id.mp4') }}" type="video/mp4">
                            Your browser does not support the video tag.
                        </video>
                        --}}{{--
                        <div id="player" data-id="MebhOoawIM8"></div>

                    </div>
                    --}}{{--
                    <button class="play-pause-btn">
                        <i class="fa fa-play-circle-o"></i>
                    </button>
                    --}}{{--
                </div>
            </div>
        </section>--}}
        @include('part.fb_comments')
        <section class="wow fadeIn author">
            <div class="container">
                <div class="img">
                    <img src="{{ asset("build/images/author.png") }}" alt="krebs"/>
                </div>
                <div class="about">
                    <h2>{{ trans_choice('common.emil.krebs', 1) }}</h2>

                    @if($pages->find(1))
                        <h3>{{ $pages->find(1)->title }}</h3>
                        <p>
                            {!! $pages->find(1)->content !!}
                        </p>
                    @endif
                </div>
                <div class="content">
                    @if($pages->find(2))
                        <h3>{{ $pages->find(2)->title }}</h3>
                        <p>{!! $pages->find(2)->content !!}</p>
                    @endif
                    <a href="https://id.wikipedia.org/wiki/Emil_Krebs" target="_blank" rel="nofollow">{{ trans('welcome.see.more.wiki') }}
                        <img src="{{ asset("build/images/wiki.png") }}" alt="wiki"/> </a>
                    @if($pages->find(3))
                        <div class="profits">
                            <h3>{{ $pages->find(3)->title }}</h3>

                            <div style="padding:10px">{!! Cookie::has('10days')?str_replace('30', '10', $pages->find(3)->content):$pages->find(3)->content !!}</div>
                        </div>
                    @endif
                </div>
            </div>
        </section>
        <section class="method-details">
            <div class="container">
                <div class="row">
                    <ul>
                        @if($pages->find(4))
                            @include('part.detail',['page' => $pages->find(4), 'icon' => asset("build/images/brain.png")])
                        @endif
                        @if($pages->find(5))
                            @include('part.detail',['page' => $pages->find(5), 'icon' => asset("build/images/badge.png")])
                        @endif
                        @if($pages->find(6))
                            @include('part.detail',['page' => $pages->find(6), 'icon' => asset("build/images/magic.png")])
                        @endif
                        @if($pages->find(7))
                            @include('part.detail',['page' => $pages->find(7), 'icon' => asset("build/images/planet.png")])
                        @endif
                        @if($pages->find(8))
                            @include('part.detail',['page' => $pages->find(8), 'icon' => asset("build/images/mp3.png")])
                        @endif
                        @if($pages->find(9))
                            @include('part.detail',['page' => $pages->find(9), 'icon' => asset("build/images/mob.png")])
                        @endif
                    </ul>
                    <div class="question">
                        <h3>{{ trans('welcome.did.we.interested.you.part_1') }}
                            <strong>{{ trans('welcome.did.we.interested.you.part_2') }}</strong></h3>

                        <p>{{ trans('welcome.did.we.interested.you.part_3') }}</p>

                        {!! form_start($form) !!}
                        <ul class="list-inline form-list">
                            <li>
                                {!! form_widget($form->first_name) !!}
                            </li>
                            <li>
                                {!! form_widget($form->last_name) !!}
                            </li>
                            <li>
                                {!! form_widget($form->email) !!}
                            </li>
                            <li>
                                {!! form_widget($form->phone) !!}
                            </li>
                            <li>
                                {!! form_widget($form->submit) !!}
                                {!! Form::hidden('languages[]',$languages->first()->id) !!}
                            </li>
                        </ul>
                        {!! form_end($form) !!}
                    </div>
                </div>
            </div>
        </section>
        <section class="more-about-method">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-10 details">
                        @if($pages->find(10))
                            <div class="col-md-5 desc">
                                <h2>{{ Cookie::has('10days')?str_replace('30 hari', '10 hari', $pages->find(10)->title):$pages->find(10)->title }}</h2>

                                <p>{!!  Cookie::has('10days')?str_replace('30 hari', '10 hari', $pages->find(10)->content):$pages->find(10)->content !!}</p>
                            </div>
                        @endif
                        <div class="col-md-7 jobs">
                            @if($pages->find(21))
                                <p style="color:#6b58cd;font-size:18px;">{!! $pages->find(21)->content !!}</p>
                            @endif
                            <section class="section join">
                                @foreach($jobs as $key => $job)
                                    <ul class="clean-list join-list">
                                        <li>{{ $job->name }}</li>
                                        <li class="break"><span class="break"> - </span></li>
                                        <li>
                                            <ul>
                                                <div class="difference">
                                                    <p>{{ $job->percent }}%
                                                        {{--<span>{{ trans('welcome.job.difference') }}</span></p>--}}
                                                </div>
                                                <div class="items">
                                                    <li class="with-skill" set-width="{{ $job->getBasePercent($jobs, $key) }}%">
                                                        <p>{{ $job->currency }} {{ number_format($job->price_new,2,',','.') }}</p>
                                                    </li>
                                                    <li class="without-skill" set-width="{{ $job->getOldPercent($jobs, $key) }}%">
                                                        <p>{{ $job->currency }} {{ number_format($job->price_old,2,',','.') }}</p>
                                                    </li>
                                                </div>
                                            </ul>
                                        </li>
                                    </ul>
                                @endforeach

                                <ul class="legend">
                                    <li><p>{{ trans('welcome.job.very.good.knowladge') }}</p></li>
                                    <li class="without"><p>{{ trans('welcome.job.very.no.knowladge') }}</p></li>
                                </ul>


                            </section>
                        </div>
                    </div>
                    <div class="stats">
                        <div class="title">
                            <h3>
                                <strong>{{ trans('common.method') }}</strong> {{ trans_choice('common.emil.krebs', 2) }}
                            </h3>
                        </div>
                        <ul>
                            @foreach($stats as $stat)
                                <li class="col-sm-6 col-md-3">
				                    <span class="img">
				                      <img src="{{ $stat->icon_path }}" alt="{{ $stat->name }}"/>
				                    </span>
                                    <h4>{{ $stat->value }}</h4>

                                    <p>{{ $stat->name }}</p>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section class="globe">
            <div class="container">
                <div class="row">
                    <ul class="opinions">
                        @foreach($mainOpinions as $opinion)
                            <li>
                                <span class="author"><img src="{{ $opinion->icon_path }}" alt="{{ $opinion->name }}"></span>

                                <p>{!! strip_tags($opinion->description) !!}</p>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </section>
        <section class="partners">
            <div class="container">
                <ul class="list-inline">
                    <li><img src="{{ asset("build/images/partners.png") }}" alt="partners" class="img-repsonsive"></li>
                </ul>
            </div>
        </section>

        <section class="expert">
            <div class="container">
                <div class="expert-area">
                    @if($pages->find(25))
                        <div class="hidden-xs img">
                            <img src="{{ asset("build/images/expert.png") }}" alt="expert">

                            <h3>{{ $pages->find(25)->title }}</h3>

                            <p>{!! $pages->find(25)->content !!}</p>
                        </div>
                    @endif
                    @if($pages->find(11))
                        <div class="content">
                            <h2>{{ trans('welcome.experts.opinion' )}}</h2>

                            <p>{!! $pages->find(11)->content !!}</p>
                            <span>{{ $pages->find(11)->title }}</span>
                            <span class="quote"><img src="{{ asset("build/images/quote.png") }}" alt="quote"></span>
                        </div>
                    @endif
                </div>
            </div>
        </section>

        @include('part.bottom_with_links')
        @include('part.footer_small')

    </main>
@endsection

@if ($header)
@section('scripts')
    <script type="text/javascript">
        $(function () {
            App.gaEvent('Welcome header', 'show', '{{ strip_tags($header->text.' '.$header->text2.' '.$header->text3) }}');
        });
    </script>
@endsection
@endif