@extends('layout.app')
@section('title') -  @endsection
@section('top')
    <header>
        @include('asp.1.top')
    </header>
@endsection
@section('content')
    <main>
        <section class="contact">
            <div class="container">
                <h1 class="wow fadeIn">{{ trans('welcome.how.it.works') }}</h1>


                <div class="video">
                    <div class="video-bg">
                        <div id="player" data-id="MebhOoawIM8"></div>
                    </div>
                </div>
                <br/><br/>

                <div class="row">
                    <div class="wow col-md-12 text-center">
                        @if(Session::has('try'))
                            <a href="{{ route('asp.trial') }}" class=" text-uppercase btn btn-green btn-big trigger"
                               type="button"
                            >{{ trans('welcome.slider.try.lesson') }}</a>
                        @else
                            <a href="#" class="btn btn-green btn-big trigger" type="button"
                               data-toggle="modal"
                               data-target="#try">{{ trans('welcome.slider.try.lesson') }}</a>
                        @endif
                    </div>
                </div>
            </div>
        </section>

    </main>

@endsection