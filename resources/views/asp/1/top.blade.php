<nav class="navbar navbar-inverse">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
        <span class="icon-bar"></span> <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ route('page.index') }}"><img src="{{ asset("build/images/logo.png") }}" alt="metode Emil Krebs"/></a>
    </div>

    <div id="navbar" class="collapse navbar-collapse">
      <ul class="nav navbar-nav">
        
        <li><a href="{{ route('page.languages') }}" style="color: #E64445;">{{ trans('menu.buy-now') }}</a>
        </li>
      </ul>
    </div>

    <div class="fb-like hidden-xs" data-href="https://www.facebook.com/KrebsMethodIndonesia/?fref=ts" data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

  </div>
</nav>