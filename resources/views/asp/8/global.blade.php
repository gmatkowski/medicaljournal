<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">


    <title>Baru! Metode Emil Krebs pembelajaran bahasa asing secara otomatis dalam 4 minggu.</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp6global/fbstyle.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp6global/normalize.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp6global/foundation.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp6global/styles.css') }}" media="all">

    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
    <!--[if lte IE 8]>
    <link rel="stylesheet" type="text/css" href="css/ie.css" media="all">
    <![endif]-->

    <script src="{{ asset('build/ext/asp6global/jquery.js') }}"></script>

    {{--<script language="Javascript" type="text/javascript">

        function dtimeodmieniony(d) {

            var dayNames = new Array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
            var now = new Date();

            var day = (now.getDate() < 10) ? '0' + now.getDate() : now.getDate();
            var month = (now.getMonth() + 1 < 10) ? '0' + (now.getMonth() + 1) : (now.getMonth() + 1);

            now.setDate(now.getDate() + d + 1);
            document.write(dayNames[now.getDay()] + ", " + day + "-"
                + month + "-" + now.getFullYear());
        }

        function dtime(d, type) {
            // Array of day names

            var dayNames;

            if (type == 1)
                dayNames = new Array('niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota');
            else
                dayNames = new Array('niedzieli', 'poniedziałku', 'wtorku', 'środy', 'czwartku', 'piątku', 'soboty');

            var now = new Date();
            var month = now.getMonth() + 1;
            if ((now.getDate() + d + 1) < 1) {
                month--;
            }
            if (month < 10) {
                var month = '0' + month;
            }
            now.setDate(now.getDate() + d + 1);

            if (month == '00') month = '12';

            document.write(dayNames[now.getDay()] + ' ' + now.getDate() + "." + month + "." + now.getFullYear());
        }

        function addcomm() {

            alert("Dzi\u0119kujemy za dodanie komentarza!");

            return true;
        }

        function etime(d, e) {
            var now = new Date();
            var now = new Date();
            var month = now.getMonth() + 1;
            if ((now.getDate() + d + 1) < 1) {
                month--;
            }
            if (month < 10) {
                var month = '0' + month;
            }
            now.setDate(now.getDate() + d + 1);
            document.write(now.getDate() + "-" + month + "-" + now.getFullYear());
        }

        $(function () {
            var pull = $('#pull');
            menu = $('#navbar ul');
            menuHeight = menu.height();

            $(pull).on('click', function (e) {
                e.preventDefault();
                menu.slideToggle();
            });

            $(window).resize(function () {
                var w = $(window).width();
                if (w > 320 && menu.is(':hidden')) {
                    menu.removeAttr('style');
                }
            });
            $('#awtd1383134730488').click(function () {
                return false;
            });
        });

    </script>
    <style>
        .hpromo {
            color: #BBB;
            font-size: 18px;
            letter-spacing: 5px;
            text-transform: uppercase;
            display: none;
            text-align: center;
        }
    </style>
    <style>.e451f6a00d29 {
            width: 1px !important;
            height: 1px !important;
            border: 0 !important;
            background: none !important;
            border-style: none !important;
            position: absolute;
        }</style>
    <style>.d38f0e547e2d {
            width: 2px !important;
            height: 2px !important;
            border: 0 !important;
            background: none !important;
            border-style: none !important;
            position: absolute;
        }</style>

    <script src="{{ asset('build/ext/asp6global/raven.js') }}"></script>
    <script>
        var RavenConfigUrl = "https://7ab2d92ceacb438e9620eef33c62b061@app.getsentry.com/72813";
        if (RavenConfigUrl.length > 0) {
            Raven.config(RavenConfigUrl).install();
        }
        var __vl = {};
        __vl['pid'] = 75;

        window.__domReady888__ = function (e) {
            function a() {
                b || (b = !0, e())
            }

            var b = !1;
            if (document.addEventListener)document.addEventListener("DOMContentLoaded", a, !1);
            else if (document.attachEvent) {
                try {
                    var f = null != window.frameElement
                } catch (g) {
                }
                if (document.documentElement.doScroll && !f) {
                    var d = function () {
                        if (!b)try {
                            document.documentElement.doScroll("left"), a()
                        } catch (c) {
                            setTimeout(d, 10)
                        }
                    };
                    d()
                }
                document.attachEvent("onreadystatechange", function () {
                    "complete" === document.readyState && a()
                })
            }
            if (window.addEventListener)window.addEventListener("load", a, !1);
            else if (window.attachEvent)window.attachEvent("onload", a);
            else {
                var c = window.onload;
                window.onload = function () {
                    c && c();
                    a()
                }
            }
        };

        window.__domReadyList888__ = [];
        window.__onDomReady888__ = function (b) {
            function c() {
                for (var a = 0; a < window.__domReadyList888__.length; a++)window.__domReadyList888__[a]()
            }

            window.__domReadyList888__.length || window.__domReady888__(c);
            window.__domReadyList888__.push(b)
        };

        var ajax88 = {};
        ajax88.x = function () {
            if (typeof XMLHttpRequest !== 'undefined') {
                return new XMLHttpRequest();
            }
            var versions = [
                'MSXML2.XmlHttp.6.0',
                'MSXML2.XmlHttp.5.0',
                'MSXML2.XmlHttp.4.0',
                'MSXML2.XmlHttp.3.0',
                'MSXML2.XmlHttp.2.0',
                'Microsoft.XmlHttp'
            ];

            var xhr;
            for (var i = 0; i < versions.length; i++) {
                try {
                    xhr = new ActiveXObject(versions[i]);
                    break;
                } catch (e) {
                }
            }
            return xhr;
        };

        ajax88.send = function (url, callback, method, data, async, type) {
            if (async === undefined) {
                async = true;
            }
            var x = ajax88.x();
            x.open(method, url, async);
            x.onreadystatechange = function () {
                if (x.readyState == 4) {
                    callback(x.responseText)
                }
            };
            x.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            x.setRequestHeader('V-Request-Type', type);
            if (method == 'POST') {
                x.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            }
            x.send(data)
        };

        ajax88.get = function (url, data, callback, type, async) {
            var callback = callback || function () {
                };
            var query = [];
            for (var key in data) {
                query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
            }
            ajax88.send(url + (query.length ? '?' + query.join('&') : ''), callback, 'GET', null, async, type)
        };

        ajax88.post = function (url, data, callback, type, async) {
            var callback = callback || function () {
                };
            var query = [];
            for (var key in data) {
                query.push(encodeURIComponent(key) + '=' + encodeURIComponent(data[key]));
            }
            ajax88.send(url, callback, 'POST', query.join('&'), async, type)
        };

        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-65291293-1', 'auto', {'allowLinker': true});
        ga('require', 'linker');
        ga('linker:autoLink', ['top-promocje.pl']);
        ga('set', 'campaignName', 'wp-link-mob');
        ga('set', 'campaignSource', 'wp');
        ga('set', 'campaignMedium', 'banner');
        ga('set', 'campaignContent', '142x106_silver---59');
        ga('send', 'pageview');

        window.__onDomReady888__(function () {
            ga('send', 'event', 'Page Load', 'Load', 'Article');

        });
        window.__onDomReady888__(function () {

            (function () {
                setInterval(function () {

                    ajax88.get('/__vae', null, null, 'poll');

                }, 1000 * 60 * 3);
            })();

        });</script>--}}

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-71149017-1', 'auto');
        ga('send', 'pageview');

    </script>
</head>
<body>

<div id="main-wrapper">
    {{--<div class="hpromo onet_on">Promocja</div>--}}
    <div class="row">


        <div id="header" class="small-12 columns">
            <div id="logo">
                <a href="#" target="_blank"><img src="{{ asset('build/ext/asp6global/logo.jpg') }}" width="250"></a>
            </div>

            <div class="divider2"></div>


            <div id="navbar" class="onet_off">
                <ul class="clearfix">
                    <li class="first"><a href="#" target="_blank">penemuan</a></li>
                    <li><a href="#" target="_blank">pengetahuan</a></li>
                    <li><a href="#" target="_blank">alam semesta</a></li>
                    <li><a href="#" target="_blank">Planet</a></li>
                    <li><a href="#" target="_blank">bumi</a></li>
                    <li><a href="#" target="_blank">alam</a></li>
                    <li><a href="#" target="_blank">orang-orang</a></li>
                    <li><a href="#" target="_blank">teknologi</a></li>
                    <li class="last"><a href="#" target="_blank">Fakta dan Mitos</a></li>
                    <li class="right"><a href="#">
                            <script type="text/javascript">etime(-1, 1);</script>
                        </a></li>
                </ul>
                <a href="#" id="pull">Menu</a>
            </div><!-- end of navbar -->
            <h1 id="main">Baru! Metode Emil Krebs pembelajaran bahasa asing secara otomatis dalam 4 minggu</h1>

        </div>
        <div id="content" class="small-12 medium-8 columns">
            <div id="socialBar">
                <img src="{{ asset('build/ext/asp6global/sb.png') }}" class="social-buttons">
                <img src="{{ asset('build/ext/asp6global/print.png') }}" class="print">
            </div>

            <div class="maria">
 		<span data-picture="" data-alt="Dr. Emil Krebs">
 		<span data-src="img/profimg.png"></span>
 			<span data-src="img/profimg.png"><img
                    src="{{ asset('build/ext/asp6global/profimg.png') }}"
                    alt="Dr. Emil Krebs"></span>
 			<noscript>
 				<img src="img/profimg.png" alt="">
 			</noscript>
 		</span>
                <p>Dr. Emil Krebs</p>
            </div>
            <p class="first-p">
                Institut Emil Krebs mengusulkan untuk orang Indonesia mencoba metode terkenal untuk pembelajaran bahasa
                asing dalam 4 minggu -- berdasarkan sistem yang dikembangkan oleh Dr. Emil Krebs, seorang polyglot dari
                Jerman, yang menguasai 68 bahasa. Dengan metode ini Anda bisa meningkatkan kemampuan bahasa asing
                <strong>secara
                    cepat dalam waktu singkat, seakan-akan Anda belajar satu tahun.</strong> Metode seorang polyglot
                yang berasal
                dari Jerman adalah tantangan untuk pembelajaran bahasa asing metode tradisional.
            </p>

            <p class="clearfix">Metode Krebs telah dipercaya oleh lebih dari 30 000 orang Indonesia, karena metode
                pembelajaran bahasa asing ini jauh lebih efektif daripada metode tradisional. Anda juga bisa mengikuti
                mereka, terlepas dari umur, pendidikan dan kemampuan Anda! Dalam 4 minggu Anda bisa lancar berbahasa
                Inggris, Cina atau Jepang, sehingga keluarga Anda, kenalan-kenalan dan rekan-rekan kerja Anda akan kaget
                melihatnya. Mereka semua tidak bisa percaya bahwa Anda belajar bahasa asing hanya 30 hari.</p>

            <p>Selalu ada waktu untuk memulai berbicara dalam bahasa asing. Anda dapat melihat hal ini dalam 4 minggu.
                Anda akan melihat perubahan dalam kehidupan sehari-hari Anda. Hanya dalam 30 hari Anda akan
                berkomunikasi dalam bahasa asing dengan lancar, dan membaca situs berita luar negeri tanpa masalah.
                Keluarga, rekan kerja, bos bahkan orang lain akan mengagumi dan memuji anda</p>

            <p><strong>Bagaimana cara kerjanya – metode Krebs pembelajaran bahasa asing secara otomatis</strong></p>

            <div
                style="display: none;	width:300px; height: auto; margin: 20px 20px 10px 0px; border: solid 1px #000; float: left; overflow: hidden;">
                <img src="{{ asset('build/ext/asp6global/wykres.png') }}" alt=""
                     style="width:300px; float: left; margin-top: -20px;">
                <p style="text-align: center; font-size:12px;"><i>Odsetek pracodawców wymagających od pracowników
                        znajomości co najmniej jednego języka obcego.</i></p>
            </div>

            <p>Dr. Emil Krebs telah mengembangkan metode pembelajaran bahasa dalam 4 minggu, berdasarkan bentuk belajar
                dengan cara mendengar. Masa kini metode Institut Krebs merupakan satu-satunya metode di Indonesia
                merampingkan dan mempercepat proses belajar kata-kata baru, karena metode tersebut Mengoptimalkan
                kemampuan otak anda sebanyak 100%.</p>

            <p>Metode-metode pembelajaran tradisional tidak menggunakan kemampuan alami otak Anda, karena tidak
                menggunakan proses menghafalkan dan mengasosiasikan kata-kata baru dengan gambar-gambar, untuk belajar
                bahasa asing. Metode-metode tradisional tersebut tidak efektif, karena hanya menggunakan kurang dari
                sepuluh persen kapasitas otak Anda. Metode Emil Krebs bekerja dengan cara yang berbeda. Metode
                Emil Krebs menggunakan 100 persen kemampuan alami otak Anda, metode tersebut mengaktifkan sel-sel dan
                bagian-bagian otak Anda yang biasanya tidak aktif.</p>


            <p>Metode pembelajaran bahasa asing dalam 4 minggu dengan cara mendengar, adalah cara menggunakan otak kiri
                dan kanan secara bersamaan untuk menghafal kata-kata baru. Otak kiri memproses informasi berupa kata dan
                otak kanan memproses informasi berupa gambar. Penggunaan otak kiri dan kanan adalah 5 kali lebih efetif
                daripada biasanya. Ini adalah cara untuk mengembangkan kemampuan kognitif, menghafalkan kata-kata baru
                dan mengasosiasikan kata-kata bahasa asing dengan kata-kata bahasa Indonesia. Dengan metode alamiah ini,
                kata dan kalimat baru secara otomatis akan masuk kedalam memori anda secara permanen.</p>


            <p><strong>Metode pembelajaran secara intensif dalam 4 minggu</strong></p>

            <div class="co-mowia-polki">
                <p><strong>Bagaimana pendapat orang Indonesia tentang Metode Krebs:
                    </strong></p>
                <p class="second"><em>„Saya mendapat pekerjaan baru, karena sekarang saya bisa berbicara dalam bahasa
                        Inggris dengan baik. Umur saya 47 tahun, tetapi saya membuat impresi yang baik di mata bos saya!
                        Semuanya berjalan lancar, dengan Metode Krebs.”
                    </em>– Andhika Widiawati</p>
            </div>

            <p>Setelah 1 hari penggunaan Metode Krebs Anda akan menguasai 120 kata-kata dan ekspresi baru. Setelah 1
                minggu Anda akan melihat bahwa kosakata bahasa asing Anda menjadi jauh lebih bervariasi. Setelah 4
                minggu penggunaan metode tersebut, Anda tidak lagi takut berbicara dalam bahasa asing, dan Anda akan
                memulai berkomunikasi dalam bahasa asing dengan lancar. Anda akan berbicara dalam bahasa asing dengan
                mudah, tanpa perlu berpikir dan tanpa masalah. Setelah 1 bulan Anda akan berbicara dengan lancar dalam
                bahasa Inggris, Cina atau bahasa lainnya yang Anda memilih: seakan-akan Anda belajar bahasa ini beberapa
                tahun. Kalau tidak, Anda bisa (Anda harus!) menggunakan garansi yang istimewa, yang diberikan oleh
                Institut Krebs untuk sistem pembelajaran audio. Garansi uang kembali, secara cepat dan tanpa pertanyaan
                apapun.</p>

            <p>Global Science merekomendasikan metode belajar ini untuk para pembaca. Dan hebatnya lagi, kamu bisa
                mendapat diskon sebesar Rp. 50000 untuk produk Emil Krebs dengan meng-klik link dibawah ini. Penawaran
                cuma berlaku untuk hari ini.
                <script type="text/javascript">etime(-1, 1);</script>
                .
            </p>

        </div>
        <div id="sidebar" class="small-12 medium-4 columns">
            <hr style="margin-top:0px;">


            <a href="#" target="_blank"><img src="{{ asset('build/ext/asp6global/langlady.jpg') }}"
                                             style="margin:15px; width: 225px;"></a>
            <p style="text-align: left; font-style:italic; background: #eeeeee; margin:-12px 15px 20px 15px; padding:10px; font-size:12px;">
                Ibu Neni Agustini adalah salah satu orang yang pertama menggunakan metode Emil Krebs pembelajaran bahasa
                asing secara otomatis dalam 4 minggu. Dengan metode tersebut, Ibu Neni Agustini meningkatkan kemampuan
                bahasa Inggris secara cepat dalam waktu singkat, dia pindah ke Australia dan sekarag dia bekerja di
                Sydney. Efek luar biasa!</p>

            <div id="PogodaNetWidget"
                 style="width: 280px; margin-left: 15px; font: 12px/1.3 arial,sans-serif;  padding: 9px 9px 5px; overflow: hidden;">

                <!-Weather in Jakarta, Indonesia on your site - HTML code - weatherforecastmap.com -->
                <div align="center">
                    <script src="http://www.weatherforecastmap.com/weather2.php?zona=indonesia_jakarta"></script>
                </div>
                <!-end of code-->


            </div>
            <script type="text/javascript" charset="utf-8" src="{{ asset('build/ext/asp6global/js_v2.htm') }}"
                    async="async"></script>
            <script type="text/javascript">document.getElementById('PogodaNetWidget').onclick = function () {
                    return false;
                };</script>

            <div class="straight"></div>
            <a href="{{ route('asp7.main', 'utm_source=website&utm_medium=news&utm_campaign=global_ios') }}" id="rek"
               target="_blank"
               style="display:block; width: 240px; height: 388px; margin-top:8px; text-decoration:none; background-color: #447799; margin: 20px auto; color: #fff;">
                <center><p style="height: 110px; font-size: 30px; margin: 0; padding: 20px 0; line-height: 120%;">
                        Metode Emil Krebs</p></center>
                <center><p style="margin: 10px 0 5px 0; height: 19px; font-weight: bold; font-size: 16px;">***JAMINAN
                        EFEK TERBAIK***</p></center>
                <center><p style="margin: 0; height: 102px;">Hanya dalam 4 minggu Anda akan menguasai materi 1 tahun
                        kursus bahasa asing</p></center>
                <div
                    style="width: 100%; padding: 0px; float: left; display:block; background-color: #257; padding: 10px 0;">
                    <center><p
                            style="margin: 0; height: 46px; line-height: 46px; color: #333; background: linear-gradient(to top, #F81 0%, #FC4 60%); width: 200px; border-radius: 5px;">
                            Beli</p></center>
                </div>
                <div style="width: 100%; padding: 0px; float: left; display:block; background-color: #AAA;">
                    <center><p style="margin: 0; height: auto; color: #333; font-size: 13px; padding: 20px 0;">Promo ini
                            hanya berlaku sampai akhir hari ini</p></center>
                </div>
            </a>
        </div>
    </div>

</div>

<div class="row">
    <div id="offer" class="small-12 columns">
        <p style="font-size: 28px; line-height:28px;"><a
                href="{{ route('asp7.main', 'utm_source=website&utm_medium=news&utm_campaign=global_ios') }}"
                target="_blank"
                style="text-decoration: underline; font-weight: bold;">"Kesempatan bagus!!! Klik
                disini untuk pesan online Metode Emil Krebs pembelajaran bahasa asing secara otomatis dalam 4 minggu
                &gt;&gt;</a></p>

        <p style="color: #A00; font-size: 16px; font-weight: bold;">Promosi khusus untuk
            <script type="text/javascript">dtimeodmieniony(-1);</script>
        </p>
    </div>
</div>

<div class="row">
    <div id="comments" class="small-12 columns">
        <div id="FB_HiddenContainer" style="position:absolute; top:-10000px; width:0px; height:0px;"></div>
        <div id="feedback_1HsYymlsW4NLzXtW1" style="font-family:Tahoma;">
            <div class="fbFeedbackContent" id="uz1cxy_1">
                <div class="stat_elem">
                    <div class="uiHeader uiHeaderTopBorder uiHeaderNav composerHider">
                        <div class="clearfix uiHeaderTop">
                            <a class="uiHeaderActions rfloat">Tambahkan komentar</a>
                            <div>
                                <h4 tabindex="0" class="uiHeaderTitle">
                                    <div class="uiSelector inlineBlock orderSelector lfloat uiSelectorNormal">
                                        <div class="wrap">
                                            <a class="uiSelectorButton uiButton uiButtonSuppressed" role="button"
                                               aria-haspopup="1" aria-expanded="false" data-label="683 comments"
                                               data-length="30" rel="toggle"><span
                                                    class="uiButtonText">683 komentar</span></a>
                                            <div class="uiSelectorMenuWrapper uiToggleFlyout">

                                            </div>
                                        </div>

                                    </div>
                                    <span class="phm indicator"></span>
                                </h4>
                            </div>
                        </div>
                    </div>
                </div>
                <ul class="uiList fbFeedbackPosts">


                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/41554_50302938_1878686864_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Wendy Ramdani</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Sudah ada yang mencoba?</div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">153</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">12 menit yang
                                                    lalu</abbr>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">


                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/273930_20904468_1027986766_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">farah donikara</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Aku menggunakan Metode Emil Krebs selama 3 minggu dan
                                            sekarang berbahasa dalam Inggris seakan-akan aku belajar 1 tahun, ya! Semua
                                            teman-teman saya terheran-heran, bahkan kakak saya yang tinggal di
                                            Singapura! Oke, saya tidak bisa berbahasa dalam Inggris seperti native
                                            speaker, tapi efek luar biasa. Saya tidak lagi takut berbicara dalam bahasa
                                            asing, bagus sekali! Terima kasih atas artikel ini!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">78</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">13 menit yang
                                                    lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/157804_21416303_1043059674_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">devi Indriantari</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Dengan metode pembelajaran ini saya pertama kali mulai
                                            berkounikasi dn lancar dalam bahasa asing! saya sudah takut saya bodoh,
                                            tetapi sekarang saya tahu saya cerdas dan pintar. dengan metode ini saya
                                            bisa percaya diri
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">59</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">25 menit yang
                                                    lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="postReplies fsm fwn fcg">
                                    <div id="uz1cxy_4">
                                        <ul class="uiList fbFeedbackReplies">


                                            <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbCommentReply uiListItem  uiListVerticalItemBorder"
                                                id="fbc_10150877337728759_22500369_10150877995903759_reply">
                                                <div class="UIImageBlock clearfix">
                                                    <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                                       tabindex="-1" aria-hidden="true"><img class="img"
                                                                                             src="{{ asset('build/ext/asp6global/157689_1027278331_1478344009_q.jpg') }}"
                                                                                             alt=""></a>
                                                    <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                                        <div class="postContainer fsl fwb fcb">

                                                            <span class="profileNamee">Leily Deden</span>

                                                            <div class="postContent fsm fwn fcg">
                                                                <div class="postText">Apakah sudah ada yang mencoba?
                                                                    Saya tertarik.
                                                                </div>
                                                                <div class="stat_elem">
                                                                    <div class="action_links fsm fwn fcg">
                                                                        <a id="uz1cxy_8">Balasan</a> ·
                                                                        <a class="fbUpDownVoteOption hidden_elem"
                                                                           rel="async-post">Like</a>
                                                                        ·
                                                                        <abbr data-utime="1338463406" class="timestamp">46
                                                                            menit yang lalu</abbr>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="postReplies fsm fwn fcg"></div>
                                                        <div class="fsm fwn fcg"></div>
                                                    </div>

                                                </div>
                                            </li>
                                        </ul>

                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/572741_30110787_2084442239_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">riri anshori</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Sudah sejak lama saya ingin sekali belajar bahasa Inggris,
                                            tetapi tanpa berhasil. Saya kehilangan banyak uang untuk membayar buku-buku,
                                            kursus bahasa Inggris, guru-guru dan lain-lain. Akhirnya sekarang dengan
                                            metode ini, saya akan belajar bahasa Inggris secara otodidak, murah dan
                                            terbaik. Saya membuat perubahan penting dalam hidup saya, karena saya akan
                                            pindah ke pacar saya, yang tinggal di Australia. Oleh karena itu, saya udah
                                            beli kursus Krebs itu, sehingga nanti saya bisa berkomunikasi dalam bahasa
                                            Inggris dengan lancar di sana. Alhamdulillah!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">243</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">1 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/174008_50902984_682021130_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Puput Sarto</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Barusan saya baca artikel di laman ini, tadi pagi saya
                                            baca di Facebook teman saya, dia senang dengan metode itu. Saya tertarik,
                                            sudah beli 2 paket kursus itu, untuk bahasa Inggris dan bahasa Cina, karena
                                            ini kesempatan bagus dengan harga itu. Saya ngak tahu, ada metode Krebs di
                                            toko online, bagus!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">105</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">1 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/371948_501645553_1716896386_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Guntur Juwita</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Luar biasa saja! Saya ingin sekali berbicara dalam 3
                                            bahasa asing sekurang-kurangnya, aku tak dapat menunggu untuk mulai belajar
                                            dengan metode krebs itu!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">273</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/275712_1815883270_368899092_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">widho santoso</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">

                                            Bagus, jadi cara ini bekerja baik, terima kasih, saya akan beli kursus Krebs
                                            itu untuk putri saya, sebagai kado ulang tahun untuknya. Saya harap dia suka
                                            ini, hadiah dalam kegiatan pembelarajan...
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">123</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/profimg1.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">gugun triwibowo</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Aku sdh 1 minggu belajar dengan Krebs itu dan aku sdh bisa
                                            bicara dalam bhs Inggris, bagus! aku sdh menguasai 1000 kata baru, cepat dn
                                            murah! bagus, karena kalo aku mau belajar di sekolah bahasa, itu mahal
                                            sekali! dan sekarang aku menghemat uang saja, dan belajar bahasa otodidak,
                                            bagus sekali, terima kasih!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">64</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/profimg2.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Yunita Kurniawan</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Saya berkali-kali mencoba belajar bahasa Inggris, tapi
                                            saya tidak punya waktu, karena ada suami dan anak-anak saya... Dengan metode
                                            ini saya belajar bhs dengan cepat, dan efektif! Bos saya senang, sekarang di
                                            perusahaan kami saya berkomunikasi dengan seorang kontraktor asal Amerika...
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">87</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/371738_1363268399_1637317047_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">hera syahri</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">
                                            Ku sdh beli kursus itu utk Bapak! Dia senang, karena dia bekerja di Belanda
                                            setiap satu tahun sekali, dan sekarang dia berbahasa dengan mudah. Metode
                                            ini sangat bermanfaat dengan harga baik
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">37</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/48783_12401144_1332233149_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Nenden Wibisono</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">aneh juga... aku butuh waktu bertahun tahun untuk belajar
                                            bahasa Inggris
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">214</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/187364_20501998_2048679844_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Hariyanto Sridyati</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">takhayul itu, Anda benar butuh waktu bertahun tahun utk
                                            belajar bhs? jelas, dalam 2 hari tak boleh menjadi native speaker, tapi
                                            metode ini jauh lebih cepat dan murah daripada di sekolah bahasa! saya
                                            senang dengan metode ini
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">122</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/163792_102948459783037_6575144_s.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Ahmad Lestari</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">
                                            Tadi malam saya dan istri saya mencoba metode itu, bagus sekali. kami dari
                                            dusun, tidak ada sekolah, dan ini adalah pertama kali kami mulai belajar
                                            bahasa asing. dengan metode itu kami bisa belajar otodidak, bagus sekali
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">124</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/273549_7706291_1106946751_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Gunawan Wijaksena</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">barusan aku baca tentang metode Krebs ini di Jakarta Post,
                                            jadi sudah bisa beli kursus bahasa itu, bagus!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">159</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">2 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/370345_7008369_2025512953_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Mathilda Fathrizal</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Setelah pelajaran pertama saya terheran-heran koq mudah
                                            sekali belajar bahasa dengan metode itu, super! pelajaran dengan metode
                                            Krebs itu adalah kenikmatan, dan saya sudah berkomunikasi sedikit dalam
                                            bahasa. BENAR!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">127</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">3 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/370176_564964504_308463864_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Hafidz Punto</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">setiap dua hari saya belajar bhs Italia dengan metode
                                            Krebs, dan efeknya bagus sekali. Saya sudah menguasai lebi dari seribu kata
                                            baru, dan saya sudah bisa berkomunikasi dalam bhs Italia dan saya sudah
                                            mengerti bhs itu. hari ini saya beli dari laman itu level bahasa paket yang
                                            lain. Mungkin saya juga mulai belajar bahasa Cina dengan metode itu?
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">136</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">3 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/371925_1426200070_1825128294_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">siswantini sinaga</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Aku butuh cara yg benar, dong. Aku mau mencari pekerjaan
                                            baru tercepat, tapi bahasa inggris saya kurang baik, jadi saya takut
                                            wawancara kerja. Ada berapa level bahasa, apakah ada level yg cocok untuk
                                            saya yg belum pernah digunakan dalam percakapan bhs inggris? Sudah ada yg
                                            mencoba metode itu?
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">98</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">3 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/371788_39603151_990746142_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">arif indrastuti</span>


                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">
                                            Saya sdh beli kursus itu 3 minggu lalu. Efeknya? luar biasa! saya sdh bisa
                                            berbicara dn orang bule di Jakarta, yg tersesat dan minta petunjuk arah
                                            pergi. Kami ngobrol saja dalam bhs inggris! saya setuju, metode itu yg
                                            paling bagus
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">68</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">4 jam yang lalu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>


                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/370953_20903876_26789988_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Tita Handwiko</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">menarik... apakah itu benar? gimana efeknya?</div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">79</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">6 godzin temu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/224406_100629153374069_2784614_n.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Desy Trim</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Saya mendengar tentang teknik itu selama kuliah di
                                            Psikologi. itu benar- kalo guna otak kiri dan kanan secara bersamaan, kalo
                                            pake suara dan gambar, kita mampu belajar jauh lebih cepat dan efektif
                                            daripada dn metode tradisional.
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">86</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">8 godzin temu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/70524_1387164496_88414351_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">agan helanita</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">tahun yg lalu aku belajar bhs Inggris di Jakarta Pusat, di
                                            sekolah yg terkenal, tapi sekarang tak ada uang, itu terlalu mahal. Sekarang
                                            aku senang mendengar tentang metode Emil Krebs, karena aku akn kembali ke
                                            bhs Inggris, aku akan belajar otodidak, lebih murah dan baik. Aku akan
                                            beritahu disini!
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">89</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">8 godzin temu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/getImage7.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Kurniawan Adi Herdian</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">TOLONG TULIS SESUATU YA!</div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">109</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">8 godzin temu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/369872_722424386_1857330401_q.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">yogi wijayanti</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Klik disini dan membaca saja, ya. Ada banyak orang yang
                                            berbicara dalam bahasa asing dengan metode luar biasa itu, mereka tak perlu
                                            berbohong, untuk apa?
                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">172</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">8 godzin temu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                    <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                        id="fbc_10150877187848759_22497027_10150877337728759">
                        <div class="UIImageBlock clearfix">
                            <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                               aria-hidden="true"><img class="img"
                                                       src="{{ asset('build/ext/asp6global/getImage8.jpg') }}"
                                                       alt=""></a>
                            <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                <div class="postContainer fsl fwb fcb">

                                    <span class="profileNamee">Tony Prasetyo Ferenz</span>

                                    <div class="postContent fsm fwn fcg">
                                        <div class="postText">Oke saya sdh beli. Ku beritahu nanti gimana efeknya.

                                        </div>
                                        <div class="stat_elem">
                                            <div class="action_links fsm fwn fcg">
                                                <a id="uz1cxy_5">Balasan</a> ·
                                                <a class="uiBlingBox postBlingBox"
                                                   data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                        class="img sp_comments sx_comments_like"></i><span class="text">122</span></a>
                                                ·
                                                <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                ·
                                                <abbr data-utime="1338433588" class="timestamp">9 godzin temu</abbr>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="fsm fwn fcg"></div>
                            </div>
                        </div>
                    </li>

                </ul>
                <div
                    class="clearfix mts mlm uiMorePager stat_elem fbFeedbackPager fbFeedbackTopLevelPager uiMorePagerCenter"
                    id="pager4fc78c58063b37637111639">
                    <div>
                        <a class="pam uiBoxLightblue fbFeedbackPagerLink uiMorePagerPrimary" rel="async">Muat 659
                            komentar lainnya<i class="mts mls arrow img sp_comments sx_comments_arrowb"></i></a>
                <span class="uiMorePagerLoader pam uiBoxLightblue fbFeedbackPagerLink">
                    <i class="img sp_comments sx_comments_"></i>
                </span>
                    </div>
                </div>
                <div class="fbConnectWidgetFooter">
                    <div class="fbFooterBorder">
                        <div class="clearfix uiImageBlock">
                            <a class="uiImageBlockImage uiImageBlockSmallImage lfloat"><i
                                    class="img sp_comments sx_comments_cfavicon"></i></a>
                            <div class="uiImageBlockContent uiImageBlockSmallContent">
                                <div class="fss fwn fcg">
                            <span>
                               <a class="uiLinkSubtle">Facebook plugin</a>
                            </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <center>
        <img alt="" src="{{ asset('build/ext/asp6global/logo.jpg') }}" align="absmiddle" width="188">
    </center>
</div>


<script src="{{ asset('build/ext/asp6global/picturefill.js') }}"></script>


<link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp6global/custom.css') }}" media="screen">
<script type="text/javascript" src="{{ asset('build/ext/asp6global/a') }}"></script>
<script src="{{ asset('build/ext/asp6global/a_002.htm') }}"></script>
<iframe class="xgptndygbaojihywzklx" style="width: 0px; height: 0px; display: none;"
        src="//www.remintrex.com/ceng/pub/ceng-tr.html?p=c7c3c5ce9544dda63336f88fe9581065&amp;op=b4bc0563c4b7ae6cbbd2e442a7091a29"></iframe>
<script type="text/javascript">
    var src = (('https:' == document.location.protocol) ? 'https://' : 'http://');

    new Image().src = src + 'adsearch.adkontekst.pl/deimos/tracking/?tid=102625&reid=1390&expire=720&nc=' + new Date().getTime();

</script>


<img src="{{ asset('build/ext/asp6global/rs.gif') }}">


</body>
</html>