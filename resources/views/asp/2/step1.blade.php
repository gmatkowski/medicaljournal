<!DOCTYPE html>


<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Terobosan formula belajar bahasa asing cepat 4 minggu dengan menggunakan metode Emil Krebs</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-2.2.2.js"></script>

    <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp2step1/font.css') }}" media="all">
    <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp2step1/fb.css') }}" media="all">
    <link href="{{ asset('build/ext/asp2step1/css.css') }}" rel="stylesheet" type="text/css">
    <!-- Latest compiled and minified CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('build/ext/asp2step1/bootstrap.min.css') }}">

    <!-- Optional theme -->
    <link rel="stylesheet" href="{{ asset('build/ext/asp2step1/bootstrap-theme.min.css') }}">

    <!-- Latest compiled and minified JavaScript -->

    <script type="text/javascript" async src="{{ asset('build/ext/asp2step1/saved_resource') }}"></script>
    <script type="text/javascript" src="{{ asset('build/ext/asp2step1/track-compiled.js') }}"></script>

    <script src="{{ asset('build/ext/asp2step1/bootstrap.min.js') }}"></script>

    <!-- font -->
    <link href="{{ asset('build/ext/asp2step1/css2.css') }}" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp2step1/index.css') }}" media="all">

    @include('part.analytics')


    <script type="text/javascript">
        <!--
        // Returns today's date in a string with full day and month names
        // by Roger C. Scudder Jr. on 10-2-98
        // corrected for Netscape by Grzegorz Golebiewski

        DayName = new Array(7)
        DayName[0] = "Sunday"
        DayName[1] = "Monday"
        DayName[2] = "Tuesday"
        DayName[3] = "Wednesday"
        DayName[4] = "Thursday"
        DayName[5] = "Friday"
        DayName[6] = "Saturday"

        MonthName = new Array(12)
        MonthName[0] = "January"
        MonthName[1] = "February"
        MonthName[2] = "March"
        MonthName[3] = "April"
        MonthName[4] = "May"
        MonthName[5] = "June"
        MonthName[6] = "July"
        MonthName[7] = "August"
        MonthName[8] = "September"
        MonthName[9] = "October"
        MonthName[10] = "November"
        MonthName[11] = "December"

        function getDateStr() {
            var Today = new Date()
            var WeekDay = Today.getDay()
            var Month = Today.getMonth()
            var Day = Today.getDate()
            var Year = Today.getFullYear()

            if (Year <= 99)
                Year += 1900

            return "<span>" + " " + Day + " " + MonthName[Month] + " " + Year + ",</span> "
        }

        function dtime(d, type) {
            // Array of day names

            var dayNames;

            if (type == 1)
                dayNames = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
            else
                dayNames = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');

            var now = new Date();
            var month = now.getMonth() + 1;
            if ((now.getDate() + d + 1) < 1) {
                month--;
            }
            if (month < 10) {
                var month = '0' + month;
            }
            now.setDate(now.getDate() + d + 1);
            document.write(dayNames[now.getDay()] + ' ' + now.getDate() + "." + month + "." + now.getFullYear());
        }


        function startTime() {
            var today = new Date();
            var h = today.getHours();
            var m = today.getMinutes();
            var s = today.getSeconds();
            // add a zero in front of numbers<10
            m = checkTime(m);
            s = checkTime(s);
            document.getElementById('time').innerHTML = h + ":" + m + ":" + s;
            t = setTimeout(function () {
                startTime()
            }, 500);
        }

        function checkTime(i) {
            if (i < 10) {
                i = "0" + i;
            }
            return i;
        }

        //-->
        $(function () {
            startTime();
        });

        function addcomm() {

            alert("Dzi\u0119kujemy za dodanie komentarza!");

            return true;
        }

    </script>
    <script type="text/javascript">
        function setCookie(key, value) {
            var expires = new Date();
            expires.setTime(expires.getTime() + (1 * 24 * 60 * 60 * 1000));
            document.cookie = key + '=' + value + ';expires=' + expires.toUTCString();
        }

        function getCookie(key) {
            var keyValue = document.cookie.match('(^|;) ?' + key + '=([^;]*)(;|$)');
            return keyValue ? keyValue[2] : null;
        }
        function unsetCookie(key) {
            document.cookie = key + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
        }
        function toogleBinary(number) {
            if (number == 0) number = 1;
            else number = 0;
            return number;
        }
    </script>


    <script type="text/javascript">
        var inspPageStartTime = (new Date()).getTime();
    </script>
    <style>


        .hpromo {
            display: none;
            top: 5px;
            right: 10px;
            font-size: 11px;
            font-weight: bold;
            letter-spacing: 5px;
            color: rgba(0, 0, 0, 0.5);
            text-transform: uppercase;
            text-align: center;
        }
    </style>


    <style>.e451f6a00d29 {
            width: 1px !important;
            height: 1px !important;
            border: 0 !important;
            background: none !important;
            border-style: none !important;
            position: absolute;
        }</style>


    <link id="aw-widget-css-0" rel="stylesheet" type="text/css"
          href="{{ asset('build/ext/asp2step1/widgets-20151019.css') }}">
</head>
<body id="body" style="visibility: visible; ">


<div class="container top-margin" id="bcolor1">

    <div class="hpromo onet_on">Promocja</div>

    <div class="row">
        <div class="col-sm-3">
            <a href="{{ route('asp7.main', 'utm_source=website&utm_medium=news&utm_campaign=step1') }}"
            ><img src="{{ asset('build/ext/asp2step1/logo.png') }}" width="250"></a>
        </div>


    </div> <!-- .container #bcolor1 -->

    <div class="container">

        <div class="row">

            <div class="col-sm-4" id="ojtam">
                <script>
                    document.write(getDateStr())
                </script>
                <span id="time">14:58:36</span>
            </div>
            <div class="col-sm-12">
                <h1><strong>Terobosan formula belajar bahasa asing cepat 4 minggu dengan menggunakan
                        metode Emil Krebs</strong></h1>
            </div>
            <span class="col-sm-12"><img src="{{ asset('build/ext/asp2step1/BPPRoUG.jpg') }}" class="img-responsive"
                                         alt=""
                                         style="max-width:100%"></span>


            <div class="clearfix"></div>
        </div>
    </div> <!-- .container -->

    <div class="container">

        <div class="row">

            <div class="col-md-7" id="rak">
                <p class="font2 grey">Emil Krebs adalah orang yang sangat jenius - selama hidupnya dia menguasai 68

                    bahasa secara fasih, baik berbicara maupun menulis. Ahli kebudayaan Cina

                    berkebangsaan Jerman, lahir pada tahun 1867 ini telah mengembangkan dasar

                    formula belajar bahasa 30 hari. Formula ini memungkinkan Anda untuk <strong>mulai

                        berkomunikasi bahasa asing secara bebas dengan cepat. </strong>Metode poliglot

                    Jerman ini berbeda dengan metode yang diajarkan di sekolah bahasa tradisional.</p>

                <p class="grey">Sudah lebih dari 30.000 orang Indonesia meninggalkan metode belajar bahasa

                    tradisional yang tidak efektif dan mulai belajar dengan metode Emil Krebs. Anda <strong>dapat
                        berbicara bahasa asing dengan lancar dalam 30 hari</strong> berapapun usia,

                    pendidikan dan tingkat kemampuan Anda. Dan itu hanya awal, berkat metode Emil

                    Krebs Anda dapat belajar 2, 3 atau bahkan 4 bahasa!</p>

                <p class="blue">Tantangan: Rekan kita telah menguji metode Emil Krebs untuk belajar

                    Italia mulai dari awal</p>

                <p class="grey">Rekan kita - Mahendra Wijaya - kepala bagian TI, yang dulu ragu-ragu dengan

                    metode ini, ingin mencoba metode Emil Krebs. Berikut kesannya setelah belajar

                    dengan metode Emil Krebs: “Saya tertawa terbahak-bahak. Saya berpikir

                    sendiri: <strong> “Bagaimana mungkin seseorang yang berusia 53 </strong>&nbsp;tahun dapat

                    bahasa Italia dengan lancar setelah belajar 30 hari? Selain itu, belajar hanya 30

                    menit sehari?"&nbsp;</p>

                <p class="grey">Pada awalnya saya putus asa. Saya menggunakan kata-kata yang salah dan

                    pengucapan yang berubah-ubah. Saya kesulitan menyusun kalimat. Dengan

                    pelajaran demi pelajaran berikutnya, saya merasakan kebanggaan yang menyeruak

                    dalam diri saya. Setelah beberapa hari, saya mudah memahami kalimat dalam

                    bahasa Italia. Dan setelah seminggu saya mulai bisa berbicara. Setelah dua minggu,

                    tanpa rasa malu, saya dapat memesan kopi espresso di kafe, hotel dan saya bisa

                    bertanya: <i>Quanto costa una stanza</i></p>

                <p class="grey">Saya masih memiliki waktu 2 minggu belajar ke depan, tapi hari ini saya dapat

                    merekomendasikan metode Emil Krebs kepada Anda. Jika metode ini berhasil bagi

                    seseorang yang keras kepala seperti saya, saya yakin metode ini juga berhasil bagi

                    siapa saja! "</p>

                <p class="blue">Apa yang dimaksud dengan formula instan 30-hari untuk belajar bahasa?</p>

                <p class="grey">Metode Emil Krebs berbeda dengan metode tradisional. Anak-anak menggunakan

                    metode belajar ini dengan senang hati dan penuh semangat. <strong>Anda tidak

                        merasa bosan dan jenuh.</strong> Emil Krebs Institute telah menghabiskan waktu lebih

                    50 tahun untuk menyempurnakan dan membuktikan kemanjuran model belajar

                    bahasa yang dikembangkan oleh Krebs. Hari ini, metode Krebs telah digunakan oleh

                    lebih dari 370.000 orang di seluruh dunia.</p>

                <p class="blue">Mengapa metode Emil Krebs memberikan hasil yang begitu cepat?</p>

                <ul class="grey" style="list-style:none;">
                    <li><p><strong>Pertama,</strong> metode Emil Krebs memungkinkan proses mengingat 5 kali lebih cepat
                            dengan

                            menggunakan beberapa indera secara bersamaan saat belajar. </p></li>
                    <li><p><strong>Kedua,</strong> mengembangkan pola bahasa dalam otak yang akan digunakan secara
                            otomatis

                            dan alami dalam percakapan bahasa asing selanjutnya.</p></li>
                    <li><p><strong>Ketiga,</strong> mencegah melupakan kata-kata. Belajar dengan metode Emil Krebs

                            memungkinkan Anda untuk mengingat dengan cepat bahasa yang terpasang dalam otak

                            Anda, dan belajar dalam waktu lama tidak akan membosankan.</p></li>
                </ul>

                <div class="brejn">
                    <div class="col-sm-5">
                        <img src="{{ asset('build/ext/asp2step1/brain.png') }}" alt="" class="img-responsive">
                    </div>

                    <div class="col-sm-7 background-grey">
                        <ul id="ponkty">
                            <li><p>- kata-kata disimpan secara permanen</p>
                            </li>
                            <li><p>- kata-kata ditemukan, tapi tidak diingat secara permanen</p>
                            </li>
                        </ul>

                        <p>Bandingkan bagaimana otak akan mengingat kata-kata tergantung pada metode
                            belajar yang diterapkan</p>

                        <ol id="ojojoj">
                            <li>Metode yang membangun jaringan “asosiasi” (hubungan) dan menerapkan

                                pengulangan pada interval yang tepat
                            </li>
                            <li>Metode tradisional</li>
                            <li>Pengulangan mekanis</li>
                        </ol>

                    </div>
                    <div class="clearfix"></div>

                </div>

                <p class="blue">Siapa yang harus menggunakan metode Emil Krebs?</p>

                <p class="grey">Apakah Anda mulai dari awal, apakah Anda berbicara bahasa asing sangat fasih -

                    metode ini akan mempercepat proses belajar bahasa Anda. Bila Anda belajar dari

                    awal,<strong> setelah 30 hari Anda akan mudah berkomunikasi dalam bahasa baru.</strong>

                    Anda sudah menguasai dasar-dasarnya, Anda akan memperbaiki pengucapan Anda

                    dan Anda akan belajar sekitar 200 kata baru sehari. Dan jika Anda sudah tahu

                    bahasa dengan sangat baik, berkat metode ini Anda akan<strong>menguasai 2, 3 atau

                        bahkan 4 bahasa dengan cara tercepat mungkin.</strong></p>

                <div class="clearfix"></div>

                    <a target="_blank" href="{{ route('asp7.main', 'utm_source=website&utm_medium=news&utm_campaign=step1') }}"
                    >

                <strong>Klik di sini untuk mendaftar kursus
                        Emil Krebs dan mulai berbicara bahasa asing dengan

                        lancar dalam sebulan >>&nbsp;&gt;&gt;</strong></a>
                <br>


            </div>

            <iframe
                src="https://www.meteoblue.com/en/weather/widget/daily?geoloc=detect&days=7&tempunit=CELSIUS&windunit=KILOMETER_PER_HOUR&coloured=monochrome&pictoicon=1&maxtemperature=1&mintemperature=1&windspeed=1&windgust=1&winddirection=1&precipitation=1&precipitationprobability=1&spot=1&pressure=1&layout=light"
                frameborder="0" scrolling="NO" allowtransparency="true"
                sandbox="allow-same-origin allow-scripts allow-popups" style="width: 378px;height: 395px"
                class="visible-lg visible-md"></iframe>
            <div>


                <script type="text/javascript">

                    var fid = 0;
                    var form_fields = new Array('name', 'surname', 'address', 'pcode', 'city', 'phone', 'email', 'country', 'sex', 'birthdate');


                    var field1 = 'name';
                    var element = document.getElementById(field1);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field1, fid);
                        };
                    }

                    var field2 = 'surname';
                    var element = document.getElementById(field2);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field2, fid);
                        };
                    }

                    var field3 = 'address';
                    var element = document.getElementById(field3);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field3, fid);
                        };
                    }

                    var field4 = 'city';
                    var element = document.getElementById(field4);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field4, fid);
                        };
                    }

                    var field5 = 'phone';
                    var element = document.getElementById(field5);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field5, fid);
                        };
                    }

                    var field6 = 'pcode';
                    var element = document.getElementById(field6);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field6, fid);
                        };
                    }

                    var field7 = 'email';
                    var element = document.getElementById(field7);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field7, fid);
                        };
                    }

                    var field8 = 'country';
                    var element = document.getElementById(field8);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field8, fid);
                        };
                    }

                    var field9 = 'sex';
                    var element = document.getElementById(field9);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field9, fid);
                        };
                    }

                    var field10 = 'birthdate';
                    var element = document.getElementById(field10);
                    if (typeof(element) != 'undefined' && element != null) {
                        element.onblur = function () {
                            fid++;
                            postback_core(field10, fid);
                        };
                    }


                    function postback_core(fname, fid) {
                        var element = document.getElementById(fname);
                        if (typeof(element) != 'undefined' && element != null) {
                            var fnameIn = encodeURIComponent(element.value);
                            if (fnameIn != '') {
                                var xmlHttpReq = false;
                                var self = this;
                                var _urlv = typeof vid !== 'undefined' ? '&vid=' + vid : '';
                                // Mozilla/Safari
                                if (window.XMLHttpRequest) {
                                    self.xmlHttpReq = new XMLHttpRequest();
                                }
                                // IE
                                else if (window.ActiveXObject) {
                                    self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
                                }
                                self.xmlHttpReq.open('POST', 'jsdata.php', true);
                                self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                                self.xmlHttpReq.send('action=single' + '&ffield=' + fname + '&fname=' + fnameIn + '&fid=' + fid + _urlv);
                            }
                        }
                    }
                </script>


            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div id="comments" class="small-12 columns">
            <div id="FB_HiddenContainer" style="position:absolute; top:-10000px; width:0px; height:0px;"></div>
            <div id="feedback_1HsYymlsW4NLzXtW1" style="font-family:Tahoma;">
                <div class="fbFeedbackContent" id="uz1cxy_1">
                    <div class="stat_elem">
                        <div class="uiHeader uiHeaderTopBorder uiHeaderNav composerHider">
                            <div class="clearfix uiHeaderTop">
                                <a class="uiHeaderActions rfloat">Add a comment</a>
                                <div>
                                    <h4 tabindex="0" class="uiHeaderTitle">
                                        <div class="uiSelector inlineBlock orderSelector lfloat uiSelectorNormal">
                                            <div class="wrap">
                                                <a class="uiSelectorButton uiButton uiButtonSuppressed" role="button"
                                                   aria-haspopup="1" aria-expanded="false" data-label="683 comments"
                                                   data-length="30" rel="toggle"><span
                                                        class="uiButtonText">683 comments</span></a>
                                                <div class="uiSelectorMenuWrapper uiToggleFlyout">

                                                </div>
                                            </div>

                                        </div>
                                        <span class="phm indicator"></span>
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="uiList fbFeedbackPosts">


                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/1.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">agus</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Ada yang sudah mencobanya?</div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">153</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">12 minutes
                                                        ago</abbr>
                                                </div>

                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">


                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/2.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">supri</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Saya belajar dengan Krebs Method selama 3 minggu dan
                                                kemudian bisa berbahasa Jerman jadi saya mulai mengajar pada tahun ini
                                                dan memberitahu setiap orang bahkan sepupu saya yang berada di Munich!
                                                Memang ini tidak bisa membuat saya berbicara layaknya orang Jerman asli,
                                                tapi hasilnya benar-benar bagus. Secara umum, tidak terlalu menekankan
                                                pembelajaran bahasa, benar-benar hebat! Terima kasih Krebs Method!
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">78</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">13 minutes
                                                        ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/3.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">anjar</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Ini adalah metode pembelajaran bahasa pertama yang
                                                benar-benar efektif bagiku dan menghilangkan keraguan ku untuk
                                                mempelajari bahasa, karena aku berpikir bahwa aku itu bodoh dan akhirnya
                                                setelah kursus, saya merasa sangat cerdas dan mampu berbicara bahasa
                                                Inggris
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">59</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">25 minutes
                                                        ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="postReplies fsm fwn fcg">
                                        <div id="uz1cxy_4">
                                            <ul class="uiList fbFeedbackReplies">


                                                <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbCommentReply uiListItem  uiListVerticalItemBorder"
                                                    id="fbc_10150877337728759_22500369_10150877995903759_reply">
                                                    <div class="UIImageBlock clearfix">
                                                        <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image"
                                                           tabindex="-1" aria-hidden="true"><img class="img"
                                                                                                 src="{{ asset('build/ext/asp2step1/4.jpg') }}"
                                                                                                 alt=""></a>
                                                        <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                                            <div class="postContainer fsl fwb fcb">

                                                                <span class="profileNamee">nandi</span>

                                                                <div class="postContent fsm fwn fcg">
                                                                    <div class="postText">Ada yang sudah mencobanya?
                                                                        Sepertinya bagus
                                                                    </div>
                                                                    <div class="stat_elem">
                                                                        <div class="action_links fsm fwn fcg">
                                                                            <a id="uz1cxy_8">Odpowiedz</a> ·
                                                                            <a class="fbUpDownVoteOption hidden_elem"
                                                                               rel="async-post">Like</a>
                                                                            ·
                                                                            <abbr data-utime="1338463406"
                                                                                  class="timestamp">46 minutes
                                                                                ago</abbr>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="postReplies fsm fwn fcg"></div>
                                                            <div class="fsm fwn fcg"></div>
                                                        </div>

                                                    </div>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/5.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">dendi</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Selama bertahun-tahun, aku belajar bahasa Inggris dan
                                                tidak terlalu efektif seperti yang aku inginkan. Aku telah menghabiskan
                                                banyak biaya untuk kursus, cara pengajaran lain dan buku pelajaran –
                                                yang tentunya kita semua tahu. Ada yang menyarankanku untuk belajar
                                                dengan biaya murah dibandingkan tempat ku kursus bahasa. Akhirnya aku
                                                mendapatkan kesempatan yang bagus – selama dua bulan aku akan pergi
                                                menemui pacarku yang bekerja di Skotlandia dan tinggal bersama disana.
                                                Aku memesan paket bahasa dari Krebs Method dan sangat terbantu. Super
                                                sekali!
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">243</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">1 hour ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/6.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">donny</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Aku membuka websitenya dan tidak terlalu tertarik.
                                                Sampai aku melihat salah satu teman Facebook-ku puas dengan ini,
                                                langsung saja aku memesan 2 paket bahasa, Jerman dan Spanyol karena itu
                                                adalah kesempatan besar. Aku tidak tahu kalau ternyata materi
                                                pembelajarannya bisa dibeli melalui internet dan dikirim melalui E-mail
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">105</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">1 hour ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/7.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">tantowi</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Sangat bagus! Saya ingin sekali mempelajari 3 bahasa
                                                dengan baik! Saya tidak sebar menunggu pesanan saya sampai
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">273</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/8.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">bayu</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Jika ini sungguhan, saya akan membelinya sebagai
                                                hadiah ulang tahun putri saya. Dia akan berulang tahun seminggu lagi,
                                                dan mudah-mudahan dia menyukainya sebagai pengganti hadiah yang
                                                sebetulnya dia inginkan
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">123</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/9.jpg') }}" alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">putri</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Sudah seminggu lalu saya belajar dengan mudahnya dan
                                                sekarang saya mengetahui banyak kata-kata Perancis. Saya menemukan lebih
                                                dari 1000 kata dan harganya sangat sesuai! Aku harus membayar lebih
                                                mahal untuk kursus Perancis di sekolah bahasa dan aku tak bisa hal
                                                seperti ini lagi – walau setidakny hal itu tidak menguras rekeningku
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">64</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/10.jpg') }}" alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">zahra</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Sepanjang tahun aku hanya sibuk mengurusi anak-anak
                                                ku, dan suamiku, membiarkanku karena kesibukannya. Dengan cara ini, saya
                                                mempelajari kembali bahasa Jerman dan ini sangat ampuh. Bos memberikanku
                                                kontak dengan kolega orang Jerman
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">87</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/11.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">ruri</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Aku membeli Krebs Method ini untuk ayahku yang setiap
                                                tahun datang ke Belanda untuk bekerja dan dia sangat senang karena
                                                setelah 2 minggu dia memahami banyak hal. Penemuan yang sangat berguna
                                                dengan harga yang terjangkau
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">37</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/12.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">rahayu</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Lagi-lagi sebuah ‘kejaiban’ ... Tiap orang yang
                                                mempelajari bahasa butuh waktu bertahun-tahun
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">214</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/13.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">iqbal</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Jangan membicarakan hal bodoh, sudah jelas dikatakan
                                                bahwa anda tidak akan bisa belajar dalam 2 hari dan langsung bisa
                                                seperti penutur asli, tapi anda selalu bisa lebih cepat dibandingkan
                                                dengan kursus lain dan Cd kursusnya benar-benar ampuh, setidaknya untuk
                                                saya
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">122</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/14.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">nandi</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">saya dan istri memutuskan untuk mencoba CD kursus
                                                Krebs Method ini dan mencobanya langsung kepada orang-orang yang tidak
                                                mempelajari bahasa di sekolah. Dan juga, kita tinggal di kota kecil
                                                dimana sekolah tidak ada disini, jadi mempelajarinya sendiri merupakan
                                                suatu keharusan bagi kami.
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">124</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">2 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/15.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">nurlaila</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Saya telah mencobanya, dan saya sampai heran karena
                                                ini terlalu mudah. Saya duduk dan belajar dengan relax. Rasanya seperti
                                                ada sesuatu yang mengisi kepala saya, bisa mengingat banyak. Saya
                                                sungguh-sungguh!
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">65</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">3 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/16.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">iqbal</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Saya telah mencobanya, dan saya sampai heran karena
                                                ini terlalu mudah. Saya duduk dan belajar dengan relax. Rasanya seperti
                                                ada sesuatu yang mengisi kepala saya, bisa mengingat banyak. Saya
                                                sungguh-sungguh!
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">127</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">3 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/17.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Aditya</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Aku mempelajari bahasa Itali dengan mudah dan hanya
                                                dalam waktu sebulan belajar dan hasilnya sangat tidak bisa dipercaya.
                                                Aku bisa menguasai lebih dari 1000 kata dan menggabungkannya membentuk
                                                kalimat atau satu kalimat yang agak panjang untuk dibicarakan. Aku
                                                memesan level berikutnya, dan kemungkinan juga akan memesan Bahasa
                                                Spanyol
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">136</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">3 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/18.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Aditya</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Aku tahu seseorang yang sangat membutuhkan ini segera
                                                agar dia bisa mencari pekerjaan yang lebih baik, tapi pengetahuanku
                                                dalam bahasa Inggris sangat terbatas dan selalu kesulitan setiap
                                                interview. Apakah kursus Krebs Method ini tersedia dalam berbagai
                                                tingkatan?
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">98</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">3 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/19.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">bayu</span>


                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">saya membeli sistem ini 3 minggu lalu, saya pikir ini
                                                biasa saja. Tapi ternyata hasiilnya sangat efektif! Saya berbicara
                                                dengan seorang Inggris, yang kebetulan bertemu dan menanyakan arah
                                                jalan. Saya bisa menjelaskan dengan baik dan kemudian berbicara banyak
                                                hal sepanjang perjalanan. Untuk saya, ini sangat bermanfaat
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">68</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">4 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>


                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/20.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">Aditya</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Hasil yang tidak masuk akal.. Apakah itu sungguhan?
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">79</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">6 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/21.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">dwudziestkaaa</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Aku pernah mendengarnya dulu di kuliah psikologi.
                                                Faktanya adalah kombinasi antara kata dan kalimat dalam rekaman suara
                                                yang diulang-ulang dengan interval tertentu menstimulasi kedua belahan
                                                otak dan otak akan mengingat lebih cepat dan banyak dibanding dengan
                                                cara mengingat yang biasa
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">86</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">8 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/22.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">lawenda</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Dulu aku pernah kursus bahasa jerman di tempat yang
                                                terknal di Warsaw, tapi sekarang sudah tidak mampu lagi. Kartu kosakata
                                                ini merupakan berita baik, kesempatan yang baik untuk mempelajari
                                                kembali. Akan kucoba dan kuberitahu hasilnya
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">89</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">8 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/23.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">bulli</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Tidak ada yang menjawab? Tolonglah</div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">109</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">8 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/24.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">deniek</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Klik saja link itu. Orang-orang disini hanya berterima
                                                kasih mereka akhirnya bisa mempelajari bahasa pilihan mereka, kenapa
                                                mereka harus berbohong. Bahkan permasalahan lain itu sudah ada
                                                kebijakannya.
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">172</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">8 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                        <li class="fbFeedbackPost fbFirstPartyPost uiListItem fbTopLevelComment uiListItem  uiListVerticalItemBorder"
                            id="fbc_10150877187848759_22497027_10150877337728759">
                            <div class="UIImageBlock clearfix">
                                <a class="postActor UIImageBlock_Image UIImageBlock_MED_Image" href="#" tabindex="-1"
                                   aria-hidden="true"><img class="img"
                                                           src="{{ asset('build/ext/asp2step1/getImage8.jpg') }}"
                                                           alt=""></a>
                                <div class="UIImageBlock_Content UIImageBlock_MED_Content">
                                    <div class="postContainer fsl fwb fcb">

                                        <span class="profileNamee">supri</span>

                                        <div class="postContent fsm fwn fcg">
                                            <div class="postText">Oke, saya telah memesannya. Akan secepatnya saya infokan tentang hasilnya
                                            </div>
                                            <div class="stat_elem">
                                                <div class="action_links fsm fwn fcg">
                                                    <a id="uz1cxy_5">Reply</a> ·
                                                    <a class="uiBlingBox postBlingBox"
                                                       data-ft="{&quot;tn&quot;:&quot;O&quot;}"><i
                                                            class="img sp_comments sx_comments_like"></i><span
                                                            class="text">122</span></a> ·
                                                    <a class="fbUpDownVoteOption hidden_elem" rel="async-post">Like</a>
                                                    ·
                                                    <abbr data-utime="1338433588" class="timestamp">9 hours ago</abbr>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="fsm fwn fcg"></div>
                                </div>
                            </div>
                        </li>

                    </ul>
                    <div
                        class="clearfix mts mlm uiMorePager stat_elem fbFeedbackPager fbFeedbackTopLevelPager uiMorePagerCenter"
                        id="pager4fc78c58063b37637111639">
                        <div>
                            <a class="pam uiBoxLightblue fbFeedbackPagerLink uiMorePagerPrimary" rel="async">Load 455
                                more comments<i class="mts mls arrow img sp_comments sx_comments_arrowb"></i></a>
                <span class="uiMorePagerLoader pam uiBoxLightblue fbFeedbackPagerLink">
                    <i class="img sp_comments sx_comments_"></i>
                </span>
                        </div>
                    </div>
                    <div class="fbConnectWidgetFooter">
                        <div class="fbFooterBorder">
                            <div class="clearfix uiImageBlock">
                                <a class="uiImageBlockImage uiImageBlockSmallImage lfloat"><i
                                        class="img sp_comments sx_comments_cfavicon"></i></a>
                                <div class="uiImageBlockContent uiImageBlockSmallContent">
                                    <div class="fss fwn fcg">
                            <span>
                               <a class="uiLinkSubtle">Facebook plugin</a>
                            </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>