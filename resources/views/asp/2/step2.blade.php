<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">


<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

  <title>Easy Phrases</title>
  <script src="https://code.jquery.com/jquery-2.2.2.js"></script>
  <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp2step2/css/normalize.css')}}" media="all">
  <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp2step2/css/foundation.min.css')}}" media="all">
  <link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp2step2/css/styles.css')}}" media="all">
  <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
  <!--[if lte IE 8]>
  <link rel="stylesheet" type="text/css" href="css/ie.css" media="all">
  <![endif]-->

  <script type="text/javascript" src="{{ asset('build/ext/asp2step2/js/jquery.pnotify.min.js')}}"></script>

  <link href="{{ asset('build/ext/asp2step2/css/jquery.pnotify.default.css')}}" media="all" rel="stylesheet" type="text/css" />
  <style>.e451f6a00d29 { width:1px !important; height:1px !important; border:0 !important; background:none !important; border-style:none !important; position:absolute; }</style>
  <style>.d38f0e547e2d { width:2px !important; height:2px !important; border:0 !important; background:none !important; border-style:none !important; position:absolute; }</style>

  @include('part.analytics')

</head>
<body onload="ga('send', 'event', 'Page Load', 'Load', 'Sale');">
<div class="row main-wrapper">
  <div id="content" class="small-12 medium-9 large-9 columns">
    <div class="content-inner-wrapper">
      <div class="stars">
        <img src="{{ asset('build/ext/asp2step2/img/stars.jpg')}}"/>
      </div>
      <h1 class="headline">
        Anda akan mulai berbicara bahasa asing
        selama dua minggu atau 100% kami
        kembalikan dana - DIJAMIN</h1>
      <div class="content-wrapper">
        <div class="box_1">
          <div class="info">
            <h3 id="t1">„ 100% perubahan”</h3>

            <p class="t1">

              „Ini adalah metode pembelajaran bahasa pertama, yang ampuh bagi Saya dan membantu

              menguasai tingkat dasar bahasa Arab hanya dalam sekitar dua minggu saja. Caranya pun

              lebih mudah dan menyenangkan daripada kursus bahasa tradisional. ”</p>

          </div>
          <div class="box_1-img">
            <img src="{{ asset('build/ext/asp2step2/img/LVzZ5OT.png') }}" alt="andy">
            <p class="author">
              <strong>Andy Rusmani</strong><br>
              <i>Telah menguasai level dasar Bahasa arab dalan 14 hari</i>
            </p>
          </div>
        </div>
        <div class="photo99"><img src="{{ asset('build/ext/asp2step2/img/MtMHAvQ.png') }}"></div>
        <p>Hallo,</p>
        <p>
          Nama saya Ifan Azhari .Sangat bahagia rasanya dapat memperkenalkan kursus bahasa dalam 2
          minggu dengan metode Krebs, karena metode ini adalah sebuah hal penting dalam linguistik.</p>
        <p>Selama 15 tahun Saya telah menjalankan sekolah bahasa di Jakarta, yang membantu lebih dari 4.700 orang untuk belajar bahasa asing, misalnya Inggris, Arab, Jerman, Jepang, Mandarin.</p>
        <p>Memiliki pengalaman yang banyak dalam bidang ini  telah mengajarkan saya satu -<strong> yaitu untuk dapat belajar bahasa dengan cepat Anda perlu sesuatu yang jauh lebih efisien daripada metode tradisional.</strong></p>
        <p>Yang benar adalah: agar dapat mulai bebas berkomunikasi dalam bahasa asing dalam waktu singkat yang kita butuhkan bukan hanya  belajar secara mekanis menghafal kata-kata. Tidak cukup menggunakan buku khusus atau menonton film dalam bahasa asing. Dibutuhkan sesuatu yang kuat - sebuah pusat memori aktivator benar, yang akan memberikan pembelajaran bahasa asing langsung dan komprehensif.</p>
        <p>Saya telah memeriksa berbagai metode,<strong> tetapi hanya satu yang memperbolehkan menguasai 200 kata-kata baru dan frase dalam cara yang cepat setiap harinya secara permanen .</strong></p>
        <p>Bahkan, orang orang yang telah berhasil mempelajari 500 kata dan frase selama 48 jam pertama bercerita kepada Saya mengenai efektifitas metode ini. Banyak dari mereka yang dapat memulai menggunakan bahasa baru tersebut dengan bebas setelah 1 minggu.</p>
        <p>Bagi Orang-orang yang selama bertahun-tahun berjuang dengan blokade mental sebelum berbicara dalam bahasa asing, metode Krebs adalah satu satunya jalan keluar agar meminimalkanketakutan dan kemalasan.</p>

        <p class="list-title">Berkat rumus otomatis dalam bahasa dapat dengan mudah:</p>
        <ul class="list_belt">
          <li><strong>menguasai lingkup dasar dari bahasa asing tersebut dalam 14 hari</strong></li>
          <li>menggunakan bahasa di tingkat menengah setelah 2 minggu</li>
          <li>meningkatkan kualifikasi profesional - <strong>akhirnya Anda mendapatkan kenaikan jabat atau pekerjaan dengan gaji yang lebih baik</strong>;</li>
          <li><strong>berkomunikasi dengan orang asing pada saat bertamasya</strong> - dengan mudahnya Anda akan mama reservasi hotel, bercakap dengan sopir taksi atau meminta bantuan medis</li>
          <li>mengurus pengembangan pribadi Anda sendiri,<strong>untuk meningkatkan kompetensi dan kecerdasan linguistik</strong> keluarga dan teman-teman akan terkesan dengan kemampuan Anda.</li>
        </ul>

        <p>Rumus, yang saya tulis, bekerja pada 3 bidang utama:</p>

        <h3 class="h_benefit">MENGHILANGKAN KEMALASAN UNTUK BELAJAR</h3>
        <p class="p_benefit">Anda TIDAK AKAN PERNAH mampu belajar bahasa asing jika Anda tidak memiliki motivasi yang tepat. Keengganan untuk belajar akan ditemui siapa saja cepat atau lambat. Metode ini akan menghilangkan kurangnya motivasi.<strong>Anda akan melihat kemajuan setiap hari </strong> dan dengandemikian akan memiliki keinginan yang jauh lebih lagi untuk set pembelajaran berikutnya. Kegembiraan belajar bahasa baru secara alami akan meningkat dan hal tersebut akan memblokir perasaan keengganan atau kemalasan.</p>

        <h3 class="h_benefit">INI MERANGSANG OTAK DAN MEMORY UNTUK KERJA 5 KALI LIPAT LEBIH PRODUKTIF</h3>
        <p class="p_benefit">Setelah beberapa hari tingkat aktivasi proses otak dan memori Anda tiba-tiba mulai tumbuh. Ini akan terjadi setiap hari, lebih atau kurangnya hinggai hari keempat. Level yang tinge ini akan menstimulasi pikiran dan memori kinerja menetap dan akan 5 kali lebih tinggi dari sebelumnya.</p>

        <h3 class="h_benefit">OTOMATIS AKAN MEMICU KECEPATAN BELAJAR BAHASA</h3>
        <p class="p_benefit">Dari saat Anda mulai belajar dengan metode  Emil Krebs, asimilasi bahasa akan otomatis. Anda

          tidak perlu berpikir tentang hal itu sepanjang waktu. Tugas latihan merangsang kerja kedua

          belahan otak, yang membuat waktu belajar metode baru dibentuk hingga 15 kali lebih koneksi

          saraf daripada metode pembelajaran tradisional. Akibatnya, <strong>kata-kata berikutnya diingat lebih

            baik dan lebih tahan lama. </strong>Seiring waktu, Anda mulai menggunakannya lancar dalam berbicara

          dan menulis, jadi mungkin lupa bahwa mereka ingat sama sekali ilmu yang dibutuhkan.</p>


        <p>Apa yang menentukan efektivitas metode otomatis.<strong>belajar bahasa adalah perubahan skema

            dengan pola yang lebih baik di otak, </strong> yang pada percakapan dalam bahasa lain kita

          menggunakannya secara otomatis dan alami. Ilmu pengetahuan didasarkan pada perendaman

          total - perendaman dalam bahasa sekitarnya. Mendengarkan dan pengulangan kalimat tertentu

          dalam interval tertentu membawa hasil yang luar biasa. Krebs menggunakan trik dan memori

          jangka pendek melalui interval dapat dengan cepat menghafal semua ekspresi atau kata-kata.</p>
        <p>Mekanisme alaminya mengikuti cara anak kecil yang ingin memulai berbicara- otomatis dan tidak

          sadar telah diterapkan bahwa pembelajaran orang dewasa seperti itu dapat menghasilkan hasil

          yang bagus. Sebagai anak-anak kita tidak tahu struktur gramatikal namun kompleks.Bahkan kita

          tidak mnegerti apa yang dibicarakan. Anak kecil pada awalnya mendengarkan, kemudian

          mengulangi - pemahaman datang dengan waktu. </p>
        <p>Setiap pelajaran telah disusun secara ilmiah sehingga bahan secara permanen masuk ke kepala

          Anda - setelah satu mendengarkan. Hanya duduk dan mulai mendengarkan. <strong>Setelah secara permanen hanya dalam satu hari menggunakan Emil Krebs, hingga 200 kata

            dan frase akan terhubungkan ke dalam memori jangka panjang Anda.</strong></p>
        <p> Sementara keteraturan

          mendengar akan memungkinkan Anda untuk belajar bahasa asing pada tingkat yang

          Sistem ini yang saya bicarakan telah terbukti, 5 kali kinerja yang lebih tinggi daripada metode

          tradisional pengajaran bahasa, kursus menggunakan <strong> audio Emil Krebs.</strong></p>

        <p>Belajar dengan metode Krebs:</p>

        <h3 class="h_benefit short">SEDERHANA</h3>
        <p class="p_benefit">Tidak perlu mekanik menghafal kosakata atau penggunaan buku teks khusus. Anda tidak perlu

          bantuan ekstra memori dan konsentrasi yang lebih. Satu-satunya hal yang Anda butuhkan adalah

          untuk mengingat sidang sehari-hari pelajaran berikutnya. Hanya duduk dan mulai mendengarkan.

          Otak Anda akan mulai menyerap air seperti spons!.</p>

        <h3 class="h_benefit short">SECARA  NATURAL MENINGKATKAN KERJA PIKIRAN</h3>
        <p class="p_benefit">Anda tahu pepatah berkata "menghafal, lulus dan lupakan"? Metode ini mengaktifkan

          bagian-bagian otak Anda yang memungkinkan informasi masuk ke memori jangka

          panjang. Penelitian telah menunjukkan bahwa kata-kata/frasa baru yang diulang-ulang

          pada interval yang tepat, akan tersimpan dalam memori Anda dalam waktu yang lama.

          Belajar kata-kata, kalimat atau frasa, terutama dengan menaikkan interval waktu, akan

          beralih dari memori sementara ke memori permanen.</p>

        <h3 class="h_benefit">MEMBERI EFEK SEGERA</h3>
        <p class="p_benefit">Cukup satu hari saja untuk menguasai sebanyak 200 kata-kata baru dan frase, merasakan

          kemajuan pertama dan kebebasan meningkat saat berbicara bahasa asing. Biasa mendengarkan

          kursus Emil Krebs membuat cepat menguasai bahasa Inggris, Arab, Cina atau bahasa lain dan

          Anda akan dapat bebas berkomunikasi dengan orang asing.

          Ini mungkin metode yang paling efektif untuk belajar bahasa asing yang pernah dibuat. Selain itu,

          telah diuji oleh tim neurolingwistik, itu juga diuji pada saya dan banyak orang lainnya, seperti Ibu

          Faria dari Jakarta.</p>



        <div class="box_1">
          <div class="info">
            <h3>„Efek Express”</h3>
            <p class="t2">Selama lebih dari lima tahun Saya mencoba untuk belajar bahasa Inggris, agar memiliki

              kesempatan yang lebih baik untuk menemukan pekerjaan dengan gaji yang baik. Saya sudah

              mencoba segalanya, kursus di sekolah populer bahasa, tutorial, pelajaran pribadi dengan guru -

              tidak berpengaruh! -Satu satunya adalah metode Emil Krebs telah terbukti menjadi hit! Saya

              melihat perbaikan besar setelah seminggu penggunaan. Saya mampu memimpin percakapan

              singkat dan menulis e-mail. Saya memiliki kesan bahwa otak saya menyerap kata-kata baru

              seperti spons!</p>

          </div>
          <div class="box_1-img">
            <img src="{{ asset('build/ext/asp2step2/img/P6cuJXj.png') }}" alt="">
            <p class="author">
              <strong>Aprilia Prastita</strong><br>
              <i>menguasai bahasa Inggris dalam 2 minggu</i>
            </p>
          </div>
        </div>

        <p>Ingat, bahwa cara ini sama seperti metode yang lainya atau bahkan jauh lebih mahal ,tidak akan

          bekerja until semuanya . Sebagian kecil orang tidak peduli tentang kemajuan secara teratur

          mengkonsolidasikan sehingga  cara tersebut bukan untuk mereka.</p>
        <p>Presiden Institute Emil Krebs begitu senang dengan efektivitas metode belajar bahasanya dan

          memberikan jaminan seperti:</p>

        <div id="gwarancja">
          <h1>Garansi Efisiensi  100% uang kembali</h1>

          <p class="gwar_text">Metode Krebs menjamin bahwa setelah menyelesaikan Kursus Emil Krebs, pelajar dapat menguasai bahasa yang dipilih di level berkomunikasi. Bilamana hasil kurang, klien dapat melaporkan permintaan uang kembali.</p>

          <p class="gwar_p">&nbsp;</p>
        </div>
        <h3>Bagaimana cara irit memesan kursus Bahasa asing metode Emil

          Krebs?</h3>
        <p>Efisiensi tinggi metode pembelajaran bahasa dengan Emil Krebs adalah hasil dari penelitian yang

          cutup lama dilakukan mengenai proses memori. Metode ini didasarkan pada jatuh tempo otak

          dalam pola jangka panjang, yang memungkinkan Anda untuk menghafal kata-kata baru 5 kali lebih

          baik dan lebih tahan lama. Oleh karena itu tidak bisa murah.</p>
        <p>Namun, Institut Emil Krebs memutuskan untuk memberikan diskon ketika Anda pertama kali

          mengunjungi halaman. Hanya Anda yang memutuskan apakah sekarang dan menerima diskon

          53%.</p>
        <p>UNTUK SEKALI diskon harga untuk kursus penuh Emil Krebs, yang terdiri dari 40 pelajaran audio

          masing-masing 30 menit, adalah <s>Rp 800 000</s> Rp 300 000</p>
        <p>Kemudian harga ini melompat.</p>
        <div class="box_achtung">
          <h3 class="red">CATATAN</h3>
          <p>Ikuti petunjuk di bawah untuk memesan kursus metode Emil Krebs.</p>
        </div>

        <p>Klik pada "Mulai belajar bahasa," dan mengisi formulir di halaman berikutnya. Versi digital akan

          masuk ke email Anda dalam beberapa menit atau jika memesan versi CD fisik - akan dikirimkan ke

          rumah Anda dalam beberapa hari. Setelah itu Anda sendiri yang akan mengevaluasi. Jika Anda

          tidak 100% puas, hanya Anda mendapatkan uang Anda kembali - tidak ada pertanyaan yang

          diajukan.</p>
        <p>Meskipun saya pribadi yakin bahwa Anda akan lebih dari puas. Sejujurnya saya berharap bahwa

          Anda akan senang. <strong>Saya pikir Anda dapat belajar bahasa lebih cepat dan lebih mudah

            daripada yang Anda pikirkan.</strong></p>

        <p style="text-align: right;">Good luck, <br>Ifan Azhari<br></p>
        <div id="bottle1" style="margin:20px auto; width:500px;">
          <p style="text-align: center;">Belajar bahasa asing oleh Emil Krebsa terdiri <br>dari 100% proses mental alami</p>
          <img style="margin-top:10px;" src="{{ asset('build/ext/asp2step2/img/G7u2wjn.png') }}">

        </div>
        <script language="JavaScript" type="text/JavaScript">
          function dtime(e,t)
          {
            var n;
            if(t==1)n=new Array('niedziela', 'poniedziałek', 'wtorek', 'środa', 'czwartek', 'piątek', 'sobota');
            else n=new Array('niedzieli', 'poniedziałku', 'wtorku', 'środy', 'czwartku', 'piątku', 'soboty');
            var r=new Date;
            var i=r.getMonth()+1;
            if(i<10){var i="0"+i}r.setDate(r.getDate()+e+1);
            document.write(n[r.getDay()]+" "+r.getDate()+"."+i+"."+r.getFullYear())
          }</script>
        <center style="margin-top:-50px;"><s>{{ $english->currency .' '. $english->price_old }}</s> {{ $english->currency .' '. $english->price }}</center>
        <br />
        <center><a href="{{ route('page.languages', 'en?ref=step2') }}" class="order-button" id="tocart">Mulai belajar bahasa</a></center><br>
        <p style="font-size: 16px;  text-align: center; line-height: 140%;">Memesan hari ini, menyimpan {{ $english->currency }} {!!  $english->price_old - $english->price !!}, dan mendapatkan jaminan kepuasan. Promosi Besok

          akan hangus.</p>

        <br />
        <h3>JAMINAN KESELAMATAN:</h3>
        <p>Janna kawathir Anda aman, tanya untuk di data oleh konsultant yang dibuhungi dan membantu

          Anda untuk mendapatkan hasil terbaik yang dapat dicapai dalam belajar bahasa.</p>
        <div class="satysfakcja">
          <img src="{{ asset('build/ext/asp2step2/img/JjXVtWL.png') }}">
        </div>


      </div>
    </div>
  </div>
  <div id="sidebar" class="small-12 medium-3 large-3 columns" >
    <div class="sidebar-inner-wrapper">
      <ul class="testimonials">
        <li class="opinion odd row-1">
          <img src="{{ asset('build/ext/asp2step2/img/71cHFU8.png') }}" alt="">
          <h3>Pembangunan berkelanjutan dan bekerja lebih baik</h3>
          <p>Tidak pernah mengenal bahasa tersebut = tidak ada prospek pekerjaan. Saya belajar bahasa
            Jepang dan semuanya berubah. Di tempat kerja Saya telah nik jabatan dan sekarang beverja
            erat dengan kantor cabang kami di Tokyo. Gaji saya telah meningkat hampir setengah, dan
            akhirnya mampu hidup dengan nyaman.</p>
          <p class="author"><strong>Herman Diwono</strong><br>
            Jakarta</p>
        </li>
        <li class="opinion even row-1">
          <img src="{{ asset('build/ext/asp2step2/img/HjwVZgS.png') }}" alt="">
          <h3>Mudah belajar adalah suatu kesenangan!</h3>
          <p>Metode Emil Krebs fantastis. Ini benar-benar bekerja! Anak saya sekarang di tahun pertama
            SMA dan cejas SMP memiliki masalah dengan Cina. Dengan file audio tersebut nilainya pun
            semakin baik. Dia berhenti merengek bahwa tidak ingin belajar, karena sekarang dapat
            melakukannya pada smartphone tercinta Anda.</p>
          <p class="author"><b>Gita Sugiyanti</b><br>
            Bandung</p>
        </li>
        <li class="opinion odd row-2 hide-for-mobile">
          <img src="{{ asset('build/ext/asp2step2/img/yYT5xsN.png') }}" alt="">
          <h3>Selalu di tangan - pada komputer Anda, smartphone dan mobil</h3>
          <p>Jika saya tidak menggunakan metode Krebs mungkin saya tidak akan pernah memutuskan
            mimpi Saya untuk perjalanan ke Paris. Sebelum pergi, saya menyalin pelajaran Krebs pada
            smartphone dan saya bisa belajar kata-kata Perancis di mana-mana - di rumah, di jalan untuk
            bekerja, dalam antrean di supermarket.Dalam perjalanan Saya yakin bahwa tidak akan ada
            masala dengan apapun, karna Saya mempunyai Krebs di tangan dan saya dapat
            menggunakanya kapanpun Saya Inginkan</p>
          <p class="author"><b>Dimas Aurano,</b><br>
            Medan</p>
        </li>
        <li class="opinion even row-2 hide-for-mobile">
          <img src="{{ asset('build/ext/asp2step2/img/YP2SICg.png') }}" alt="">
          <h3>Pilihan yang tepat!</h3>
          <p>Pada awalnya saya sangat skeptis ke metode Krebs. Saya sudah mencoba metode belamra
            Bahsa Inggris yang lain, dan tidak satupun dari mereka memberi efek . Saya tidak mampu
            berbicara ataupun mengenal kata kata yang gampang,tapi apa yang terjadi setelah 1
            pelajaran Krebs ada di luar harapan saya paling luar! Kata-kata, system pembelajaranya
            sancta menyenangkan dan segera melihat hasilnya. Aku jatuh cinta dengan cara baru saya
            belajar! Hari ini saya pada hari ke-11 dari belajar dan memahami sebagian besar kata-kata
            dari program dalam bahasa Inggris! Krebs sensasional!</p>
          <p class="author"><b>Nurul Mezili,</b><br>
            Semarang</p>
        </li>
        <li class="opinion odd row-3 hide-for-mobile">
          <img src="{{ asset('build/ext/asp2step2/img/RcUH3FQ.png') }}" alt="">
          <h3>Sertifikat adalah hanya selembar kertas</h3>
          <p>Siapa yang butuh kursus di sekolah, ketika memakai Emil Krebs? Tidak hanya itu kata-kata
            sendiri jatuh ke kepala, itu masih Anda belajar di mana Anda inginkan - rumah, mobil, toko!
            Sertifikat tidak menjamin kebebasan berkomunikasi dalam bahasa asing, hanya konfirmasi
            bahwa berjalan kursus. Saya alami sendiri ini dan Krebs benar-benar merekomendasikan
            kepada siapa pun.</p>
          <p class="author"><b>Robby Suherlan</b><br>
            Solok</p>
        </li>

        <li class="opinion even row-3">
          <img src="{{ asset('build/ext/asp2step2/img/JRKGb1S.png') }}" alt="">
          <h3>Semua orang belajar</h3>
          <p>Saya memiliki kursus ini dan saya sangat merekomendasikan. Bagian yang terbaik
            adalah hal dimana kita bisa memilih sendiri tingkat bahasa dan mempelajarinya. Satu
            bulan lagi Saya akan ke Australia untuk mencari pekerjaan dan tidak lagi takut untuk
            berkomunikasi dengan siapapun.Di Perth Tentu saja saya akan terus menggunakan
            kursus.</p>
          <p class="author"><b>Lia Amin</b><br>Sabang</p>
        </li>
      </ul>
    </div>
  </div>
</div>





<style>
  #ExitDiv {
    display: none;
    position: absolute;
    top: 10px;
    left: 15px;
    margin: auto;
    width: 700px;
    height: 154px;
    z-index: 1000;
    cursor: default;
    pointer-events: none;
  }

  #exitdiv_text1 {
    position: relative;
    top: 5px; left: 5px;
    width: 400px;
    height: 93px;
    font-family: 'Arial Narrow', Arial, sans-serif;
    font-size: 36px;
    color: #6E0606;
    font-weight: bold;
    font-stretch: condensed;
  }

  #exitdiv_text2 {
    position: absolute;
    top: 0;
    left: 400px;
    width: 300px;
    height:93px;
    font-family: arial;
    font-size: 56px;
    color: #6E0606;
    font-weight: bold;
  }

  #exitdiv_text3 {
    position: absolute;
    left: 225px;
    top: 100px;
    width: 230px;
    height:100px;
    font-family: arial; font-weight: bold;
    text-align: center;
    font-size: 24px;
    color: #04447C;
  }

  #left-wrapper {
    position: relative;
    width: 475px;
    height: 100px;
    float: left;
  }
  #left-wrapper #popup-1-top {
    position: relative;
    padding: 25px 0 0 35px;
    width: 217px;
    height: 61px;
  }
  #left-wrapper #popup-1-top #popup1-header-1 {
    margin: 0;
    padding: 0;
    font-family: arial;
    font-size:8px;
    color: #6F77C6;
  }
  #left-wrapper #popup-1-top #popup1-header-2 {
    margin-top: 5px;
    font-family: arial;
    font-size:7px;
    color: #353234;
  }
  #left-wrapper #exit_div_alert1 {
    width: 147px;
    margin-top: -25px;
    padding: 0 35px 5px 35px;
    background: url('{{ asset('build/ext/asp2step2/img/popup_1_middle.gif')}}') repeat-y  0% 0%;

    font-family: arial;
    font-size: 8px;
    color: #353234;
  }
  #left-wrapper #popup-1-bottom {
    width: 217px;
    height: 55px;
    background: url('{{ asset('build/ext/asp2step2/img/popup_1_bottom.gif')}}') no-repeat;
  }

  #left-wrapper #popup-1-bottom .texts {
    margin-left: 50px;
    font-family: arial;
    font-size: 9px;
    color: #151C55;
  }
  #left-wrapper #popup-1-bottom .text-1 {
    padding-top: 4px;
  }
  #left-wrapper #popup-1-bottom .text-2 {
    position: relative;
    padding-top: 8px;
  }
  #left-wrapper #popup-1-bottom .text-2 .arrow {
    position: absolute;
    bottom: -12px;
    right: -49px;
    width: 247px;
    height: 64px;
    background: url('{{ asset('build/ext/asp2step2/img/arrow_1.png')}}') no-repeat 0% 0%;

  }
  #left-wrapper #popup-3 {
    position: relative;
    width: 418px;
    height: 96px;

    float: right;

    background: url('{{ asset('build/ext/asp2step2/img/popup_3.gif')}}') no-repeat;

    font-family: arial;
  }
  #left-wrapper #popup-3 .texts {
    font-size: 9px;
    color: #000;
  }
  #left-wrapper #popup-3 .text-1 {
    padding: 33px 15px 0px 42px;
  }
  #left-wrapper #popup-3 .text-2, #left-wrapper #popup-3 .text-3 {
    position: absolute;
    bottom: 19px; left: 124px;
    width: 76px;
    height: 15px;

    font-size: 9px;
    text-align: center;
    color: #151C55;
  }

  #left-wrapper #popup-3 .text-3 {
    left: 208px;
  }

  #left-wrapper #popup-3 .text-3 .arrow {
    position: absolute;
    bottom: -7px;
    right: -14px;
    width: 101px;
    height: 137px;
    background: url('{{ asset('build/ext/asp2step2/img/arrow_2.png')}}') no-repeat 0% 0%;

  }
  #right-wrapper {
    position: relative;
    width: 225px;
    margin-top: 36px;
    float: right;
  }
  #right-wrapper #popup-2-top {
    width: 227px;
    height: 27px;
    background: url('{{ asset('build/ext/asp2step2/img/popup_2_top.gif') }}') no-repeat;
  }
  #right-wrapper #exit_div_alert3 {
    width: calc(227px - 36px);
    padding: 10px 18px;
    background: url('{{ asset('build/ext/asp2step2/img/popup_2_middle.gif') }}') repeat-y;
    font-family: arial;
    font-size:10px;
    color: #353234;
  }
  #right-wrapper #popup-2-bottom {
    width: 227px;
    height: 42px;
    background: url('{{ asset('build/ext/asp2step2/img/popup_2_bottom.gif') }}') no-repeat;
  }
  #right-wrapper #popup-2-bottom .texts {
    position: absolute;
    font-family: arial;
    font-size: 9px;
    text-align: center;
    color: #151C55;
  }
  #right-wrapper #popup-2-bottom .text-1, #right-wrapper #popup-2-bottom .text-2 {
    bottom: 21px;
    left: 27px;
    width: 74px;
    height: 15px;
  }
  #right-wrapper #popup-2-bottom .text-2 {
    bottom: 21px;
    left: 105px;
    width: 100px;
  }
  #right-wrapper #popup-2-bottom .text-2 .popup {
    position: absolute;
    bottom: -10px;
    right: -7px;
    width: 280px;
    height: 133px;
    background: transparent url('{{ asset('build/ext/asp2step2/img/arrow_3.png') }}') no-repeat 0% 0%;
  }
</style>






<script type='text/javascript'>

  var fid = 0;
  var form_fields=new Array('name', 'surname', 'address', 'pcode', 'city', 'phone', 'email', 'country', 'sex', 'birthdate');


  var field1 = 'name';
  var element =  document.getElementById(field1);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field1 ,fid);
    };
  }

  var field2 = 'surname';
  var element =  document.getElementById(field2);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field2 ,fid);
    };
  }

  var field3 = 'address';
  var element =  document.getElementById(field3);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field3 ,fid);
    };
  }

  var field4 = 'city';
  var element =  document.getElementById(field4);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field4 ,fid);
    };
  }

  var field5 = 'phone';
  var element =  document.getElementById(field5);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field5 ,fid);
    };
  }

  var field6 = 'pcode';
  var element =  document.getElementById(field6);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field6 ,fid);
    };
  }

  var field7 = 'email';
  var element =  document.getElementById(field7);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field7 ,fid);
    };
  }

  var field8 = 'country';
  var element =  document.getElementById(field8);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field8 ,fid);
    };
  }

  var field9 = 'sex';
  var element =  document.getElementById(field9);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field9 ,fid);
    };
  }

  var field10 = 'birthdate';
  var element =  document.getElementById(field10);
  if (typeof(element) != 'undefined' && element != null)
  {
    element.onblur=function(){
      fid++;
      postback_core(field10 ,fid);
    };
  }



  function postback_core(fname, fid) {
    var element =  document.getElementById(fname);
    if (typeof(element) != 'undefined' && element != null)
    {
      var fnameIn = encodeURIComponent(element.value);
      if(fnameIn != '')
      {
        var xmlHttpReq = false;
        var self = this;
        var _urlv = typeof vid !== 'undefined' ? '&vid='+vid : '';
        // Mozilla/Safari
        if (window.XMLHttpRequest) {
          self.xmlHttpReq = new XMLHttpRequest();
        }
        // IE
        else if (window.ActiveXObject) {
          self.xmlHttpReq = new ActiveXObject("Microsoft.XMLHTTP");
        }
        self.xmlHttpReq.open('POST', 'jsdata.php', true);
        self.xmlHttpReq.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        self.xmlHttpReq.send('action=single'+'&ffield='+fname+'&fname='+fnameIn+'&fid='+fid+_urlv);
      }
    }
  }


</script><script type="text/javascript">

  var fid=0;var form_fields=new Array('name','surname','address','pcode','city','phone','email','country', 'sex', 'birthdate');var field1='name';var element=document.getElementById(field1);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field1,fid);};}
  var field2='surname';var element=document.getElementById(field2);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field2,fid);};}
  var field3='address';var element=document.getElementById(field3);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field3,fid);};}
  var field4='city';var element=document.getElementById(field4);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field4,fid);};}
  var field5='phone';var element=document.getElementById(field5);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field5,fid);};}
  var field6='pcode';var element=document.getElementById(field6);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field6,fid);};}
  var field7='email';var element=document.getElementById(field7);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field7,fid);};}
  var field8='country';var element=document.getElementById(field8);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field8,fid);};}
  var field9='sex';var element=document.getElementById(field9);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field9,fid);};}
  var field10='birthdate';var element=document.getElementById(field10);if(typeof(element)!='undefined'&&element!=null)
  {element.onblur=function(){fid++;postback(field10,fid);};}
  function postback(fname,fid){var element=document.getElementById(fname);if(typeof(element)!='undefined'&&element!=null)
  {var fnameIn=encodeURIComponent(element.value);if(fnameIn!='')
  {var xmlHttpReq=false;var self=this;if(window.XMLHttpRequest){self.xmlHttpReq=new XMLHttpRequest();}
  else if(window.ActiveXObject){self.xmlHttpReq=new ActiveXObject("Microsoft.XMLHTTP");}
    self.xmlHttpReq.open('POST','jsdata.php',true);self.xmlHttpReq.setRequestHeader('Content-Type','application/x-www-form-urlencoded');self.xmlHttpReq.send('action=single'+'&ffield='+fname+'&fname='+fnameIn+'&fid='+fid);}}}
</script>
<link rel="stylesheet" type="text/css" href="{{ asset('build/ext/asp2step2/css/custom.css')}}" media="screen">
<script src="https://track.omgpl.com/action/application/?MID=821673&amp;PID=15643&amp;val=&amp;action=Landing"></script>
<script type='text/javascript'>
  var src = (('https:' == document.location.protocol) ? 'https://' : 'http://');

  /*new Image().src = src+'adsearch.adkontekst.pl/deimos/tracking/?tid=102625&reid=1390&expire=720&nc='+new Date().getTime();*/

</script>


<script type="text/javascript" src="{{ asset('build/ext/asp2step2/gdejs/xgde.js') }}"> </script>
</body>

</html>
