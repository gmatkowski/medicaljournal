@extends('layout.app')
@section('title') :: {{ trans('menu.terms') }} @endsection
@section('top')
  <header>
    @include('part.top')
  </header>
@endsection
@section('content')
  <main>
    <section>
      <div class="container">
        <h1>{{ $post->title }}</h1>
        <small>{{ trans('blog.by') }} {{ $post->author }}</small> <br/>
        <small>{{ trans('blog.added') }} {{ $post->created_at_formated }}</small> <br/>

        @if(!empty($post->icon))<img src="{{ $post->icon_path }}" alt="" />@endif

        <div class="row">
          <article>
            {!! $post->article !!}
          </article>
        </div>

      </div>
    </section>

    <aside>
      @foreach ($list as $element)
        <a href=" {{ url('blog').'/'.$element->id }} " style="display:block;">
          <img src="{{ $element->icon_thumb_path }}" />
          {{ $element->title }}
        </a>
      @endforeach
    </aside>
  </main>

@endsection