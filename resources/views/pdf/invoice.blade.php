<!doctype html>
<html lang="en">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title>Receipt / Tax Invoice</title>
	<style>
		body{
			font-family: 'Kinnari';
		}

		* { margin: 0; padding: 0; }
		#page-wrap { width: 800px; margin: 0 auto; }

		textarea { border: 0; overflow: hidden; resize: none; }
		table { border-collapse: collapse; }
		table td, table th { border: 1px solid black; padding: 5px; }

		#header { height: 15px; width: 100%; margin: 20px 0; background: #222; text-align: center; color: white; text-decoration: uppercase; letter-spacing: 20px; padding: 8px 0px; }

		#customer { line-height: 150%; }
		#address { width: 400px; height: 150px; float: left; line-height: 150%; }

		#logohelp input { margin-bottom: 5px; }

		#meta td { text-align: right;  }
		#meta td { text-align: left; background: #eee; }
		#meta td textarea { width: 100%; height: 20px; text-align: right; }

		#items { clear: both; width: 100%; margin: 30px 0 0 0; border: 1px solid black; }
		#items th { background: #eee; }
		#items textarea { width: 80px; height: 50px; }
		#items tr.item-row td { border: 0; vertical-align: top; text-align:center; }
		#items td { width: 300px; }
		#items td { width: 175px; }
		#items td.description textarea, #items td.item-name textarea { width: 100%; }
		#items td { border-right: 0; text-align: right; }
		#items td { border-left: 0; padding: 10px; }
		#items td.total-value textarea { height: 20px; background: none; }
		#items td { background: #eee; }
		#items td { border: 0; }

		#terms { text-align: center; margin: 20px 0 0 0; }
		#terms h5 { text-transform: uppercase; letter-spacing: 10px; border-bottom: 1px solid black; padding: 0 0 8px 0; margin: 0 0 8px 0; }
		#terms textarea { width: 100%; text-align: center;}

		textarea:hover, textarea:focus, #items td.total-value textarea:hover, #items td.total-value textarea:focus { background-color:#EEFF88; }

		#price-subtotal, #price-vat, #total-subtotal{width:80%; text-align:center;}
	</style>
</head>

<body>
	<div id="page-wrap">

		<textarea id="header">{{ $title }}</textarea>
		
		<div id="identity">
			<div id="address">
				Digital Commerce Indonesia<br>
				GRAHA LIMA LIMA, JL. TANAH ABANG II NO. 57<br>
				JAKARTA, Jakarta Pusat 10160<br>
				contact@medical-jurnal.com<br>
				+62 888 01000488<br>
				{{--Tax ID : {{ $tax_id }}--}}
			</div>
		</div>
		
		<div style="clear:both"></div>
		
		<div id="customer">
			<br>
			<div class="meta-head"><b>Order Date : {{ $order_date }}</b></div>
			<div class="meta-head"><b>Order Number : CM{{ $order_number }}</b></div>
			<div class="meta-head"><b>{{ $invoice_number_title }} Number : {{ $invoice_number }}</b></div>
			<div class="meta-head"><b>{{ $invoice_date_label }} Date: {{ $invoice_date }}</b></div>
		</div>

		<br>

		<table>
			<tr>
				<th>Billing Address:</th>
				<th>Shipping Address:</th>
			</tr>

			<tr class="item-row">
				<td>
					<span><b>Name:</b></span>
					{{ $name_to }}, {{ $surname_to }}<br>
					@if($address_to)
						<span><b>Address:</b></span>
						{{ $address_to }}<br>
					@endif
					<span><b>Tel:</b></span>
					{{ $phone_to }}<br>
					@if($email_to)
						<span><b>Email:</b></span>
						{{ $email_to }}
					@endif
				</td>
				<td>
					<span><b>Name:</b></span>
					{{ $name_to }}, {{ $surname_to }}<br>
					@if($address_to)
						<span><b>Address:</b></span>
						{{ $address_to }}<br>
					@endif
					<span><b>Tel:</b></span>
					{{ $phone_to }}<br>
					@if($email_to)
						<span><b>Email:</b></span>
						{{ $email_to }}
					@endif
				</td>
			</tr>
		</table>
		
		<table id="items">
		  <tr>
		      <th>PRODUCT</th>
		      <th>QTY</th>
			  <th>PRODUCT ID</th>
		      <th>PRICE</th>
		  </tr>

			@foreach ($orders as $order)
				<tr class="item-row">
					<td><span>{{ $order->language->name }}</span></td>
					<td><span>{{ $order->qty }}</span></td>
					<td><span>{{ $order->language->id }}</span></td>
					<td><span>{{ $order->price }}</span></td>
				</tr>
		 	@endforeach
		</table>

		<br>

		<table>
			<tr>
				<th id="price-subtotal">Price:</th>
				<th>{!! $price_before_vat_html !!}</th>
			</tr>

			<tr>
				<th id="price-vat">VAT 10%</th>
				<th>{!! $vat_html !!}</th>
			</tr>

			<tr>
				<th id="total-subtotal">
					<span><b>Total:</b></span>
				</th>
				<th>
					{!! $total_price_html !!}
				</th>
			</tr>
		</table>
		
		<div id="terms">
		  <div><b>FOR Digital Commerce Indonesia</b><br>
			  …………………………………………………………<br>
			  <b>AUTHORIZED SIGNATURE</b>
		  </div>
		</div>
	
	</div>
</body>
</html>
