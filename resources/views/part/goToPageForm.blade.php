{!! form_start($goToPageForm) !!}
{!! form_widget($goToPageForm->product, ['value' => $productSymbol]) !!}
{!! form_widget($goToPageForm->page) !!}
{!! form_row($goToPageForm->submit) !!}
{!! form_end($goToPageForm, false) !!}