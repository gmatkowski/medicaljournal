<div class="exitpopup">
    <a class="exitpopup-close"></a>
    <a href="{{ route('order.special') }}" class="exitpopup-buy"></a>
</div>

@section('scripts')
    <script type="text/javascript">

        window.onbeforeunload = (function () {
                var $popup = $('.exitpopup');
                if ($popup.is(':hidden')){
                    $popup.css('left', (window.innerWidth/2 - $popup.width()/2));
                    $popup.css('top', (window.innerHeight/2 - $popup.height()/2));
                    $popup.show();
                    return 'Stay and get extra discount';
                }
        });

        $('.exitpopup-close').click(function () {
            $('.exitpopup').hide();
        });

    </script>
@endsection