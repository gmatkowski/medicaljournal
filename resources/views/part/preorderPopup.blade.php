<div id="garciniaPopup" style="display:none;">
	<form id="garciniaContainer" action="{{ route('page.try.send') }}" method="POST">
		<div id="garciniaHeader">
			<h1>PERHATIAN!</h1>
			<p>Mengambil keuntungan dari Konsultasi Kesehatan Gratis</p>
		</div>
		<div id="garciniaBelt">
			Kami akan membantu Anda untuk menurunkan berat badan:
		</div>
		<input type="text" class="garciniaInputs" name="first_name" placeholder="Ketik Nama Anda" required>
		<input type="text" style="display:none;" name="last_name">
		<input type="text" style="display:none;" name="product" value="{{ $productSymbol }}">
		<input type="text" style="display:none;" name="popup" value="1">
		<input type="text" class="garciniaInputs" name="phone" placeholder="Ketik Nomor HP Anda" required>
		<input type="email" class="garciniaInputs" name="email" placeholder="Ketik Email Anda" required>
		<input type="submit" id="garciniaInputsSubmitButton" value="Ya, saya ingin Konsultasi Kesehatan Gratis">
	</form>
</div>
{{--
@if(!Cookie::get('welcomePopup'.ucfirst($productSymbol)) && (!Request::has('pop') || Request::get('pop') == 'true'))
	<script type="text/javascript">
		$(function () {
			$("#garciniaPopup").modal({
				fadeDuration: 100,
				escapeClose: false,
				clickClose: false,
			});
			$('.hidden-xs').hide();
			$(document).mouseup(function (e) {
				var container = $("garciniaPopup");
				if (!container.is(e.target) // if the target of the click isn't the container...
						&& container.has(e.target).length === 0) // ... nor a descendant of the container
				{
					$('.hidden-xs').show();
				}
			});
		});
	</script>
@endif
--}}