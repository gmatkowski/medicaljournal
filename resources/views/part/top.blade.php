<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span>
                <span class="icon-bar"></span> <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ route('page.index') }}"><img src="{{ asset("build/images/logo.png") }}"
                                                                          alt="metode Emil Krebs"/></a>
        </div>
        @if(!Request::has('nolinks'))
            <div id="navbar" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li {!! !is_null(Request::route()) && Request::route()->getName() == 'page.index'?'class="active"':'' !!}>
                        <a href="{{ route('page.index') }}">{{ trans('menu.homepage') }}</a>
                    </li>

                    {{-- <li {!! !is_null(Request::route()) && Request::route()->getName() == 'page.records'?'class="active"':'' !!}><a href="{{ route('page.records') }}">{{ trans('menu.records') }}</a></li> --}}

                    <li {!! !is_null(Request::route()) && Request::route()->getName() == 'page.lessons'?'class="active"':'' !!}>
                        <a href="{{ route('page.lessons') }}">{{ trans('menu.lessons') }}</a>
                    </li>

                    <li {!! !is_null(Request::route()) && Request::route()->getName() == 'page.languages'?'class="active"':'' !!}>
                        <a href="{{ route('page.languages') }}">{{ trans('menu.languages') }}</a>
                    </li>

                    <li {!! !is_null(Request::route()) && Request::route()->getName() == 'page.opinions'?'class="active"':'' !!}>
                        <a href="{{ route('page.opinions') }}">{{ trans('menu.opinions') }}</a>
                    </li>

                    <li {!! !is_null(Request::route()) && Request::route()->getName() == 'page.contact'?'class="active"':'' !!}>
                        <a href="{{ route('page.contact') }}">{{ trans('menu.contact') }}</a>
                    </li>

                    <li>
                        <a href="http://krebsmethod.co.id/blog">{{ trans('menu.blog') }}</a>
                    </li>

                    <li>
                        <a href="{{ route('page.try') }}" type="button" class="trigger">{{ trans('menu.try') }}</a>
                        {{--
                        <a href="#" class="trigger" type="button" data-toggle="modal" data-target="#{{ !Session::has('try')?'try':'test-lesson' }}">{{ trans('menu.try') }}</a>
                        --}}
                    </li>

                    <li><a href="{{ route('page.terms') }}">{{ trans('menu.terms') }}</a></li>
                    <li><a href="{{ route('page.languages') }}" style="color: #E64445;">{{ trans('menu.buy-now') }}</a>
                    </li>
                </ul>
            </div>
        @endif

        <div class="fb-like hidden-xs" data-href="https://www.facebook.com/KrebsMethodIndonesia/?fref=ts"
             data-layout="button_count" data-action="like" data-show-faces="false" data-share="true"></div>

    </div>
</nav>