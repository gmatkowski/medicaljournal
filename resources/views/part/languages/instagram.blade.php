<section id="more-opinions">

	@foreach($images as $image)
		<div>
			<span class="img">
				<a href="{{ $image->link }}" target="_blank">
					<img src="{{ $image->images->low_resolution->url }}" alt=""/>
				</a>
			</span>
			<span>{{ LocalizedCarbon::instance(\Carbon\Carbon::createFromTimestamp($image->created_time))->diffForHumans() }}</span>

				<p>@if(isset($image->caption->text)){{ $image->caption->text }}@endif</p>

		</div>
		@endforeach

</section>