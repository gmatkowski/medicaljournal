<section class="possibilities">
	<div class="container">
		<h2>
			{{ trans('welcome.use.possibilities') }} <strong>{{ trans('welcome.krebs.method') }}</strong>
		</h2>

		<div class="links">
			{{--
			<a href="{{ route('page.try') }}" type="button" class="btn btn-blue">{{ trans('welcome.slider.try.lesson') }}</a>
			--}}
			<a href="#" class="btn btn-blue trigger"
			   type="button"
			   data-toggle="modal"
			   data-target="#{{ !Session::has('try')?'try':'test-lesson' }}"
			>
				{{ trans('welcome.slider.try.lesson') }}
			</a>

			<a href="{{ route('page.languages') }}#buy"
			   class="btn btn-green">{{ trans('welcome.buy.now') }}</a>
		</div>
	</div>
</section>