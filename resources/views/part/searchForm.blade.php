{!! form_start($searchForm) !!}
<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
{!! form_widget($goToPageForm->product, ['value' => $productSymbol]) !!}
{!! form_widget($searchForm->query, ['value' => $query ?? '']) !!}
    <span class="fa fa-search form-control-feedback left" aria-hidden="true"></span>
</div>
{!! form_row($searchForm->submit) !!}
{!! form_end($searchForm, false) !!}