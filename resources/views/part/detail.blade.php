<li class="col-sm-6 col-md-4 box wow">
	<span class="img">
		<img src="{{ $icon }}" alt="ico"/>
	</span>
	<div class="content">
		<span class="title">
			<h3>{{ $page->title }}</h3>
		</span>
		<p>{!! $page->content !!}</p>
	</div>
</li>