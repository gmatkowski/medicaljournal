<!-- Wypróbuj -->
@if(isset($languages))
	<div class="modal fade" id="try" tabindex="-1" role="dialog" aria-labelledby="tryLabel">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="tryLabel">{{ trans('try.write.to.get') }}
						<strong>{{ trans('try.free.lesson') }}</strong>
					</h4>
				</div>
				<div class="modal-body">
					{!! form_start($tryForm,['class' => 'modal-form']) !!}

					<div class="lang">
						<p>{{ trans('languages.choose.language') }}</p>

						@foreach($languagesWithSample as $key => $language)
							<label for="lang-{{ $language->id }}" @if($key == 0) class="active" @endif>
								<img src="{{ $language->icon_path }}" alt="">
								<input type="radio" @if($key == 0) checked @endif name="language" id="lang-{{ $language->id }}" value="{{ $language->id }}"/>
							</label>
						@endforeach
					</div>

					{!! form_widget($tryForm->name,['attr' => ['class' => '']]) !!}
					{!! form_widget($tryForm->email,['attr' => ['class' => '']]) !!}
					{!! form_widget($tryForm->phone,['attr' => ['class' => '']]) !!}
					{!! form_widget($tryForm->age,['attr' => ['class' => '']]) !!}



					<div class="other-lang">
						<p>{{ trans('languages.choose.other') }}</p><br>
						<label for="oth-lang-english">
							<img src="{{ asset('build/images/langs/english.png') }}" alt="">
							<br><span>Inggris</span>
							<input type="checkbox" name="other_lang[]" id="oth-lang-english" value="English" />
						</label>

						<label for="oth-lang-german">
							<img src="{{ asset('build/images/langs/german.png') }}" alt="">
							<br><span>Jerman</span>
							<input type="checkbox" name="other_lang[]" id="oth-lang-german" value="German" />
						</label>

						<label for="oth-lang-chinese">
							<img src="{{ asset('build/images/langs/chinese.png') }}" alt="">
							<br><span>Cina</span>
							<input type="checkbox" name="other_lang[]" id="oth-lang-chinese" value="Chinese" />
						</label>

						<label for="oth-lang-japanese">
							<img src="{{ asset('build/images/langs/japanese.png') }}" alt="">
							<br><span>Jepang</span>
							<input type="checkbox" name="other_lang[]" id="oth-lang-japanese" value="Japanese" />
						</label>

						<label for="oth-lang-arab">
							<img src="{{ asset('build/images/langs/arab.png') }}" alt="">
							<br><span>Arab</span>
							<input type="checkbox" name="other_lang[]" id="oth-lang-arab" value="Arab" />
						</label>
					</div>

					{!! form_widget($tryForm->submit,['attr' => ['class' => '']]) !!}
					{!! form_end($tryForm) !!}
				</div>
			</div>
		</div>
	</div>

	<!-- Koniec Wypróbuj -->

	@if(Session::has('try'))
			<!-- Lekcja Próbna -->
	<div class="modal fade" id="test-lesson" tabindex="-1" role="dialog" aria-labelledby="testLesson">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span></button>
				</div>
				<div class="modal-body">
					<div>
						<!-- Tab panes -->
						<div class="tab-content">
							@foreach($languagesWithSample as $key => $language)
								<div role="tabpanel" class="tab-pane {{ $language->id == Session::get('try')?'active':'' }}" id="try_{{ $language->symbol }}">
									<h4 class="modal-title">{{ trans('common.choosen.language') }}
										<strong class="red">{{ $language->name_short }}</strong>
									</h4>

									<p>{{ trans('common.try.language.now') }}</p>
									<audio class="sample" src="{{ $language->sample_path }}" preload="none"></audio>
								</div>
							@endforeach
						</div>

						{{--
						<ul class="nav nav-tabs" role="tablist">
							<span>Wybierz inny język</span>
							@foreach($languages as $key => $language)
								@if(!empty($language->sample))
									<li role="presentation" class="{{ $language->id == Session::get('try')?'active':'' }}">
										<a href="#try_{{ $language->symbol }}" aria-controls="{{ $language->symbol }}" role="tab" data-toggle="tab">{{ $language->name_short }}</a>
									</li>
								@endif
							@endforeach
						</ul>
						--}}
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Koniec Lekcja Próbna -->
@endif


<div class="modal fade" id="inactive" tabindex="-1" role="dialog" aria-labelledby="inactiveLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="tryLabel">{{ trans('languages.inactive.leave.your.data.part_1') }}
					<strong>{{ trans('languages.inactive.leave.your.data.part_2') }}</strong>
				</h4>
			</div>
			<div class="modal-body">
				{!! form_start($inactiveForm,['class' => 'modal-form']) !!}
				{!! form_widget($inactiveForm->name,['attr' => ['class' => '']]) !!}
				{!! form_widget($inactiveForm->email,['attr' => ['class' => '']]) !!}
				{!! form_widget($inactiveForm->phone,['attr' => ['class' => '']]) !!}

				{!! form_widget($inactiveForm->submit,['attr' => ['class' => '']]) !!}
				{!! form_end($inactiveForm) !!}
			</div>
		</div>
	</div>
</div>
@endif

<div class="last-buyer" style="display:none;">
	    <span class="img">
	        <img src="{{ asset('build/images/basket.png') }}" alt="basket">
	    </span>

	<p>
		{{ trans('checkout.last.bought') }}
		<span><strong data-container="buyer-name"></strong> {{ trans('checkout.from') }}
			<span data-container="buyer-city"></span></span> <b data-container="buyer-product"></b>
	</p>
</div>

@section('scripts')
	@if(Session::has('popup'))
		<script type="text/javascript">
			$(function () {
				$('#{{ Session::get('popup') }}').modal();
			});
		</script>
	@endif
@append