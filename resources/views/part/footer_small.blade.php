<section class="from-clients">
	<div class="container">
		<div class="row">
			<div class="col-sm-6">
				<h2>{{ trans('common.contact.data') }}</h2>

				<div id="opinions">
					<div class="opinion">
						@if($footer_pages->find(20))
							<p>{!! $footer_pages->find(20)->content !!}</p>
						@endif
					</div>
				</div>
			</div>
			@if($footer_pages->find(12))
				<div class="col-sm-3">
					<h2>{{ $footer_pages->find(12)->title }}</h2>

					<p>{!! $footer_pages->find(12)->content !!}</p>
				</div>
			@endif
			@if($footer_pages->find(13))
				<div class="col-sm-3">
					<h2>{{ $footer_pages->find(13)->title }}</h2>

					<p>{!! $footer_pages->find(13)->content !!}</p>
				</div>
			@endif
		</div>
	</div>
</section>
@section('fb::footer')
	@include('part.fb_comments')
@endsection