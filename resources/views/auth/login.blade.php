@extends('layout.admin_singin')
@section('title') :: Sing in @endsection
@section('content')

	<div class="">
		<a class="hiddenanchor" id="toregister"></a> <a class="hiddenanchor" id="tologin"></a>

		<div id="wrapper">
			<div id="login" class="animate form">
				<section class="login_content">
					<div class="text-center">
						<img src="{{ asset('build/images/admin/order-logo.png') }}" alt="" />
					</div>
					{!! form_start($form) !!}
						<h1>Sign in</h1>

						<div>
							{!! form_widget($form->email) !!}
							{!! form_errors($form->email) !!}
						</div>
						<div>
							{!! form_widget($form->password) !!}
							{!! form_errors($form->password) !!}
						</div>
						<div>
							{!! form_widget($form->submit) !!}
						</div>
						<div class="clearfix"></div>

					{!! form_end($form) !!}
					<!-- form -->
				</section>
				<!-- content -->
			</div>

		</div>
	</div>

@endsection
