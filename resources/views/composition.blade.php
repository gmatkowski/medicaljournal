@extends('layout.app')

@section('content')

    <section class="composition row">
        <div class="container col-md-8 col-md-offset-2">
            <h1>KOMPOSISI <span>PENIRUM</span></h1>
            <div class="text col-md-12">
                <div class="left col-md-6 text-center">
                    <img alt="Penirum" src="{{ asset('build/ext/penis/3/block-7-img4-pl.png') }}"
                    class="img-responsive" style="margin:auto;">
                    <div class="order-button-big" style="position: relative;">
                        <a href="http://www.penirum.com/order">BELI&nbsp;SEKARANG</a>
                    </div>
                    <br style="clear: both">
                </div>
                <div class="right col-md-6">
                    <p>Penirium dikembangkan bekerja sama dengan para dokter ahli urologi terbaik. Komposisinya aman dan
                        jelas, terdiri dari zat-zat tumbuhan alami dan herbal yang populer dalam pengobatan selama
                        bertahun-tahun. Rahasia efektivitas Penirium adalah konsentrasi bahan-bahan yang baik dan
                        komposisi unik.
                    </p>
                    <hr>
                    <ul>
                        <li>Komposisinya 100% alami – asam amino, ekstrak tumbuhan dan herbal yang digunakan dalam
                            urologi selama bertahun-tahun
                        </li>
                        <li>Tanpa kimia dan steroid, bebas hormon apapun
                        </li>
                        <li>Semua bahan berasal dari pemasok bersertifikat dan jaminan kualitas tertinggi
                        </li>
                        <li>Penirium memenuhi semua persyaratan keamanan yang berlaku di seluruh dunia
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="composition-more row">
        <div class="container col-md-8 col-md-offset-2">
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">L-CARNITINE</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/ilkartynina.png') }}" alt="L-Karnityna">
                        L-Carnitine digunakan untuk pencegahan dan pengobatan penyakit kardiovaskular – membantu mengangkut
                        asam lemak dalam tubuh untuk menghasilkan energi dan meningkatkan ketahanan otot. Membantu untuk
                        mengoksidasi jaringan, meningkatkan motilitas sperma dan kadar testosteron dalam tubuh.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">L-ARGININ</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/larginina.png') }}" alt="L-Arginina">
                        L-Arginin membantu melebarkan pembuluh darah dan memfasilitasi aliran darah yang lebih baik di
                        seluruh tubuh. Meningkatkan aliran darah ke alat kelamin dan membantu untuk meningkatkan sirkulasi
                        darah di corpus cavernosum penis. L-Arginin juga sangat bermanfaat dalam proses regenerasi otot
                        setelah kegiatan olahraga.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">LYSINE</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/lizyna.png') }}" alt="Lizyna">
                        Lysine adalah asam amino endogen yang memainkan peran kunci dalam produksi kolagen. Meningkatkan
                        elastisitas jaringan kulit, elastisitas dinding pembuluh darah dan elastisitas tendon dan otot.
                        Lysine membantu mengencangkan sel-sel dan merangsang produksi hormon yang bertanggung jawab untuk
                        gairah seksual .
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">EKSTRAK LABU</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/dynia.png') }}" alt="Ekstrakt z nasion dyni">
                        Ekstrak labu terdiri dari fitosterol yang menyebabkan pengurangan prostaglandin dalam jaringan
                        prostat. Ekstrak labu adalah sumber seng yang bermanfaat untuk prostat dan menambah gairah seksual.
                        Selain itu, omega-3 asam lemak meningkatkan aliran darah dan fungsi corpus cavernosum.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">SENG</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/cynk.png') }}" alt="Cynk">
                        Seng bertanggung jawab untuk produksi testosteron – hormon utama yang menentukan libido dan fungsi
                        seksual pria, merangsang gairah seksual, meningkatkan fungsi corpus cavernosum dan memperpanjang
                        durasi ereksi. Seng menstimulasi organ reproduksi untuk menghasilkan sperma lebih banyak dan
                        meningkatkan motilitas sperma.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">GINSENG</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/zenszen.png') }}" alt="Żeń-szeń">
                        Manfaat dari ginseng sendiri adalah untuk membantu meningtkatkan gairah seksual dan menambah energi
                        dan vitalitas. Merangsang sekresi adrenalin dan menghasilkan respon terhadap rangsangan, maka pria
                        memiliki kemampuan mempertahankan ereksi lebih lama, ereksi yang luar biasa.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">SARSAPARILLA</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/sarsaparilla.png') }}" alt="Sarsaparilla">
                        Sarsaparilla adalah tanaman yang mengandung banyak androgen – hormon laki-laki yang bertanggung
                        jawab untuk produksi sperma. Bertindak sebagai afrodisiak alami, digunakan juga untuk mengobati
                        impotensi. Meningkatkan libido dan fungsi seksual pria.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">EKSTRAK CRANBERRY</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/zurawina.png') }}" alt="Ekstrakt z żurawiny">
                        Ekstrak cranberry memiliki efek menguntungkan pada fungsi jantung dan pembuluh darah, jadi
                        meningkatkan aliran darah di pembuluh darah yang ada di dalam penis. Merangsang gairah seksual,
                        mencegah penyakit prostat dan menghilangkan bakteri yang menyebabkan infeksi pada saluran kemih.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">EKSTRAK AKAR MANIS</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/lukrecja.png') }}" alt="Korzeń lukrecji">
                        Ekstrak akar manis merupakan salah satu bahan herbal yang meningkatkan libido pria secara alami.
                        Meningkatkan ukuran penis saat ereksi, meningkatkan kadar testosteron dalam tubuh dan meningkatkan
                        kualitas ereksi.
                    </p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="component">
                    <div class="ng">EKSTRAK MACA</div>
                    <p>
                        <img src="{{ asset('build/ext/penis/3/maca.png') }}" alt="Ekstrakt z korzenia maca">
                        Ekstrak maca meningkatkan kualitas air mani, meningkatkan libido pria dan menambah energi dalam
                        tubuh. Memiliki efek memperkuat dan meningkatkan tingkat energi, mengatasi kelelahan dan kelemahan
                        tubuh. Oleh karen itu, ekstrak maca dianjurkan terutama selama latihan intensif.
                    </p>
                </div>
            </div>

            <div class="warning col-md-12">
                <h2>! AWAS OBAT PALSU</h2>
                Obat dengan komposisi yang rahasia bisa berbahaya untuk kesehatan Anda.<br>
                Penirium yang asli Anda dapat membeli hanya di web resmi kami.
            </div>


        </div>
    </section>


@endsection