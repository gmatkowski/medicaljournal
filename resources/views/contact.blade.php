@extends('layout.app')

@section('content')

    <section class="contact row">
        <div class="container col-md-8 col-md-offset-2">
            <h1>KONTAK</h1>
            <p>Anda masih bingung, Anda ingin mempelajari lebih lanjut? Hubungi kami saja, dan kami akan menjawab setiap pertanyaan Anda!</p>
            <div class="info">
                Hubungi kami melalui nomor kontak +62 88801000488
                atau melalui formulir di bawah:
            </div>
            <form action="http://www.penirium.com/contact/" method="post" accept-charset="utf-8">
                <div style="display:none">
                    <input name="csrf_token_name" value="23d6f79205bc05460302e8d2bee59c5a" type="hidden">
                </div>
                <div class="text">
                    <div class="left">
                        <div class="form-element">
                            <label for="name">Nama:</label> <input name="name" id="name"
                                                                             placeholder="Nama"
                                                                             type="text"></div>
                        <div class="form-element">
                            <label for="e-mail">Email:</label> <input name="e-mail" id="e-mail"
                                                                       placeholder="Email" type="text"></div>
                        <div class="form-element">
                            <label for="tel">Telepon:</label> <input name="tel" id="tel"
                                                                     placeholder="Telepon" type="text"></div>

                        <div class="form-element">
                            <label for="message">Pesan:</label> <textarea name="message" cols="40" rows="10"
                                                                              id="message"
                                                                              placeholder="Pesan"></textarea>
                        </div>
                        <div class="submit">
                            <input name="submit" value="Kirim pesan" type="submit">
                        </div>
                    </div>
                    <div class="right">
                        <div class="numbers">
                            <img src="{{ asset('build/ext/penis/3/contact-icon.png') }}" alt="Contact - Icon" align="left">
                            Tim konsultan kami siap membantu Anda<br>
                            Senin-Jumat 8-21&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Sabtu 9-17
                        </div>
                        <img src="{{ asset('build/ext/penis/3/contact-img.png') }}" alt="Kontakt">
                    </div>
                </div>
            </form>
        </div>
    </section>


@endsection