<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Style-Type" content="text/css">
	<style type="text/css">
		p.p1 {margin: 0.0px 0.0px 10.6px 0.0px; font: 12.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p2 {margin: 0.0px 0.0px 10.6px 0.0px; font: 16.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p3 {margin: 0.0px 0.0px 10.6px 0.0px; font: 12.0px 'Times New Roman'; color: #4472c4; -webkit-text-stroke: #4472c4}
		p.p4 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p5 {margin: 0.0px 0.0px 10.6px 0.0px; font: 13.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p6 {margin: 0.0px 0.0px 0.0px 0.0px; font: 16.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p7 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000; min-height: 15.0px}
		p.p8 {margin: 0.0px 0.0px 10.6px 0.0px; font: 12.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000; min-height: 15.0px}
		p.p9 {margin: 0.0px 0.0px 0.0px 0.0px; font: 12.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p10 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; color: #4472c4; -webkit-text-stroke: #4472c4}
		span.s1 {font-kerning: none}
		span.s2 {font: 12.0px 'Times New Roman'; font-kerning: none}
		span.s3 {font: 12.0px 'Times New Roman'; font-kerning: none; color: #4472c4; -webkit-text-stroke: 0px #4472c4}
		span.s4 {font-kerning: none; color: #000000; -webkit-text-stroke: 0px #000000}
		span.s5 {text-decoration: underline ; font-kerning: none}
		span.s6 {font-kerning: none; background-color: #ffffff}
	</style>
</head>
<body>
<p class="p3"><span class="s4">Hi </span><span class="s1"><i>{{ $contact->name }}</i></span><span class="s4"><i>,</i></span></p>
<p class="p4"><span class="s1">Saya Fahmi dari Krebs Method.</span></p>
<p class="p5"><span class="s1"><br>
Berikut saya akan tunjukkan 3 pertanyaan yang sering diajukan.</span></p>
<p class="p6"><span class="s1"><b>Bagaimana mungkin belajar bahasa hanya 30 hari?</b></span></p>
<p class="p7"><span class="s1"></span><br></p>
<p class="p4"><span class="s1">Emil Krebs memformulasi beberapa ketentuan yang membuat metode ini diapresiasi lebih dari 357000 konsumen seluruh dunia. Metode ini tidak mengajarkan tata bahasa dan membaca, sebaliknya fokus pada mendengar – sebuah proses belajar natural anak-anak ketika belajar bahasa Indonesia. Dengan mengulang frasa dan kata dalam interval, maka memori jangka panjang akan menangkapnya sehingga sulit untuk dilupakan. Masing-masing pelajaran secara ilmiah diformulasi untuk memastikan Anda tidak takut berbicara.</span></p>
<p class="p2"><span class="s2"><br>
</span><span class="s1"><b>Untuk siapa kursus ini dirancang?</b></span></p>
<p class="p4"><span class="s1">Kursus Emil Krebs dipersiapkan untuk siapa saja yang ingin belajar atau meningkatkan kemampuan berbahasa. Mekanisme yang digunakan dalam kursus sangatlah natural untuk siapa saja, jadi baik dewasa maupun anak-anak dapat merasakan keuntungannya.</span></p>
<p class="p6"><span class="s1"><br>
<b>Bagaimana cara membeli dan apa yang akan didapat?</b></span></p>
<p class="p8"><span class="s1"></span><br></p>
<p class="p5"><span class="s1">Ada dua opsi. Anda dapat membeli versi digital dan membayar via ATM. Segera setelah membayar, Anda akan menerima tautan di email untuk mengunduh. Untuk beberapa lokasi, Anda juga dapat melakukan pembayaran di tempat – Jadi dalam 3-14 hari Anda akan menerima kursus dalam bentuk 2 CD dan membayar tunai lewat kurir kami. Anda dapat mendengarkan audio kapanpun, dimanapun, dan dengan segala perangkat – laptop, telepon, pemutar musik di mobil, etc</span></p>
<p class="p9"><span class="s1"><br>
</span></p>
<p class="p10"><span class="s4">Ingat </span><span class="s1"><i>{{ $contact->name }}</i></span><span class="s4">, Anda dapat menghubungi kami atau </span><a href="{{ route('order.recreate',['id' => $contact->id, 'token' => $contact->token, 'type' => 'contact', 'page.languages', 'email2']) }}"><span class="s5">tinggal membeli kursus kami sekarang kurang dari 1 menit – klik disini</span></a></p>
<br />
<p class="p4"><span class="s1">Salam hormat,<br>
</span><span class="s6">Fahmi</span></p>
</body>
</html>
