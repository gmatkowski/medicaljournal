@extends('layout.app')

@section('styles')
    <style type="text/css">
        body {
            background:#CECECE;
        }

        #white-box {
            width:591px;
            height:611px;
            margin:0 auto;
            background:#fff;
        }

        #header-box {
            padding:20px 0;
            background:#08b93e;
        }

        #content-box {
            font-family: Arial;
            font-size: 17px;
            color: #4A4A4A;
            padding:10px 40px;
        }

        #header-box-left {
            float:left;
            width:164px;
            padding-left:40px;
        }

        #header-box-right {
            width:357px;
            float:right;
            font-family: Roboto-Bold;
            font-size: 30px;
            color: #FFFFFF;
            padding-top:50px;
        }

        #footer-left {
            float: left;
            font-family: Arial;
            font-size: 17px;
            color: #4A4A4A;
            padding:20px 40px;
        }

        #footer-left a {
            color:#08b93e;
            text-decoration: none;
        }

        #footer-right {
            float: right;
            margin-right:40px;
            text-align: right;
        }

        #footer-right ul {
            list-style: none;
        }

        #footer-right li {
            float: left;
            padding:0 3px;
        }

        .clear {
            clear: both;
        }
    </style>
@endsection
@section('content')
    <div id="white-box">
        <div id="header-box">
            <div id="header-box-left"><img src="{{ $message->embed(storage_path('app/mailing2/customer-support.png')) }}" width="164" height="164" alt=""></div>
            <div id="header-box-right">Terima Kasih Atas Pemesanan Anda!</div>
            <div class="clear"></div>
        </div>
        <div id="content-box">
            <h2>Dear Customer</h2>
            <p>Tim kami akan segera menghubungi Anda melalui telpon untuk mengkonfirmasi pemesanan Anda dan alamat pengiriman.</p>
            <p>Silahkan menunggu untuk mengkonfirmasi pemesanan yang Anda lakukan.</p>
            <p>Setelah melakukan konfirmasi melalui telpon, tim kami akan langsung mengirimkan barang Anda.</p>
        </div>
        <div id="footer">
            <div id="footer-left">
                <strong>Bamboo Charcoal Black Mask Team</strong><br>
                <a href="#">http://promos-seru.com/masker-hitam</a>
            </div>
            <div id="footer-right">
                <ul>
                    <li><a href="#"><img src="{{ $message->embed(storage_path('app/mailing2/icon-fb.png')) }}" alt=""></a></li>
                    <li><a href="#"><img src="{{ $message->embed(storage_path('app/mailing2/icon-twitter.png')) }}" alt=""></a></li>
                    <li><a href="#"><img src="{{ $message->embed(storage_path('app/mailing2/icon-instagram.png')) }}" alt=""></a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@endsection