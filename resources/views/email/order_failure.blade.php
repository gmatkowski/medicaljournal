Halo {{ $order->name }}, <br />
Saya lihat saudara ingin membeli kursus Emil Krebs dan anda telah berhasil memperoleh diskon termurah. Namun, Anda tidak sanggup melakukan pembayaran dalam 24 jam.<br />
Tidak perlu khawatir, Saya akan memperpanjang promo untuk saudara. Silahkan klik tautan ini :<br />
<a href="{{ route('order.recreate',['id' => $order->id, 'token' => $order->token]) }}">Metode Emil Krebs</a><br />
Ingat, saudara bisa melakukan pembayaran tunai dengan layanan pesan antar – tidak perlu ke ATM.
<br /><br />
Hormat kami,<br />
Fahmi Bayu
