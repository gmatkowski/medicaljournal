@extends('layout.app')

@section('styles')
    <style>
        body{
            font-family: arial;
        }

        p{
            font-size:16px;
            margin-bottom: 5px;
        }

        .table{
            width: 730px;
            margin:auto;
        }

        .header{
            background:url("{{ $message->embed(storage_path('app/mailing2/header.png')) }}");
            background-repeat-y: no-repeat;
        }

        .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th{
            border-top:0;
        }
        .color-purple{
            color:#794293;
        }
        .purple-section{
            background:url("{{ $message->embed(storage_path('app/mailing2/purple.png')) }}");
            background-repeat-y: no-repeat;
            background-size: 100%;
            color:#fff;
            padding-left: 30px!important;
        }
        .purple-section p{
            margin-top: 9px
        }
        .color-green{
            color:#62B134;
        }
        .discount{
            width: 150px;
            margin:auto;
            display: block;
        }
        .med{
            width: 110px
        }
        .text-right{
            text-align: right;
        }
        .no{
            font-size:20px!important;
            margin-bottom: 70px
        }
        .star{
            height: 14px
        }
        .margin-top-0{
            margin-top: 0;
        }
        .green-btn{
            color:#fff;
            background-color: #62B134;
            font-size:15px;
            padding: 6px 20px;
            text-decoration:none;
        }
        .border-block{
            border-top:1px dashed #333!important;
            border-bottom: 1px dashed #333!important;
            padding-bottom: 20px
        }
        .img-point{
            width: 100px;
            height: 110px;
            margin-left: 7px
        }
        .arrow{
            width: 15px;
            margin:30px;
            margin-bottom: 30px
        }
        .text-center{
            text-align: center;
        }

        .para-block{
            width: 176px;
            float: left;
            font-size: 13px;
        }

        .para-block-1{
            width: 126px;
        }

        .para-block-2{
            width: 172px;
            margin-left: 4%;
        }

        .para-block-3{
            width: 172px;
            margin-left: 4%;
        }

        .para-block-4{
            margin-left: 2%;
        }

        @media(max-width: 730px){
            .table{
                width: 600px;
                margin:auto;
            }
        }
    </style>
@endsection
@section('content')
    <table class="table">
        <thead></thead>
        <tbody>
        <tr  colspan="3">
            <td colspan="3" class="header" >
                <p>Hi <span>{{ $order->name }}</span></p>
                <p>Terima Kasih telah berbelanja dengan kami!</p>
                <p>Kami dengan senang hati menginformasikan bahwa Anda telah memesan
                    <span class="color-purple">Garcinia Cambogia Forte</span><br>
                    Kami harap Anda puas dengan pemesanan Garcinia Anda.</p>
            </td>
        </tr>
        <tr colspan="3">
            <td colspan="3">
                <p><b>Rincian pemesanan:</b></p>
            </td>
        </tr>
        <tr colspan="3">
            <td colspan="3" class="purple-section">
                <p><b>Garcinia Cambogia</b> <span class="color-green">(Nerofet)</span></p>
            </td>
        </tr>
        <tr colspan="3">
            <td colspan="1">
                <img class="img-responsive med" src="{{ $message->embed(storage_path('app/mailing2/med.png')) }}" alt="images/med.png">
            </td>
            <td colspan="1">

                <p>Coba satu cara yang inovatif untuk menurunkan berat</p>
                <p>badan Anda:</p>
                <p>-turun hingga 9-12 kg dalam 1 bulan</p>
                <p>-membantu tubuh membakar lemak hingga 82 persen</p>
                <p>-meningkatkan metabolisme tubuh</p>
                <p>-menghilangkan selulit dan stretch mark</p>
                <p>4.6<img class="star" src="{{ $message->embed(storage_path('app/mailing2/star.png')) }}" alt="images/star.png"></p>
                <p class="margin-top-0">86 Ulasan dari pembeli</p>
            </td>
            <td colspan="1">
                <p class="text-right no">{!! StrHelper::spaceInPrice($product1->price_old) !!} {{ $product1->currency }}</p>
                <img class="img-responsive discount" src="{{ $message->embed(storage_path('app/mailing2/discount.png')) }}" alt="images/discount.png">
            </td>
        </tr>
        <tr >
            <td class="border-block"></td>
            <td class="border-block"></td>
            <td class="border-block" text-align="right">
                <p class="text-right">Subtotal: {!! StrHelper::spaceInPrice($product1->price) !!} {{ $product1->currency }}</p>
                <p class="text-right"><a href="#" class="green-btn">Gratis Pengiriman!</a></p>
                <p class="text-right">Total: {!! StrHelper::spaceInPrice($product1->price) !!} {{ $product1->currency }}</p>
                <p class="text-right margin-top-0">(VAT included)</p>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <p><b>30 hari garansi uang kembali:</b></p>
                <p>Kami yakin Anda akan senang dengan hasil yang diberikan Garcinia Cambogia, tapi apabila Anda <br>
                    tidak puas, Anda bisa mendapat uang kembali.</p>
                <p><b>Alur kerja dari layanan standar kami adalah sebagai berikut:</b></p>
            </td>
        </tr>
        <tr colspan="7">
            <td colspan="7">
                <img class="img-point" src="{{ $message->embed(storage_path('app/mailing2/img-1.png')) }}" alt="images/img-1.png">
                <img class="arrow"     src="{{ $message->embed(storage_path('app/mailing2/arrow.png')) }}" alt="images/arrow.png">
                <img class="img-point" src="{{ $message->embed(storage_path('app/mailing2/img-2.png')) }}" alt="images/img-2.png">
                <img class="arrow"     src="{{ $message->embed(storage_path('app/mailing2/arrow.png')) }}" alt="images/arrow.png">
                <img class="img-point" src="{{ $message->embed(storage_path('app/mailing2/img-3.png')) }}" alt="images/img-3.png">
                <img class="arrow"     src="{{ $message->embed(storage_path('app/mailing2/arrow.png')) }}" alt="images/arrow.png">
                <img class="img-point" src="{{ $message->embed(storage_path('app/mailing2/img-4.png')) }}" alt="images/4.img">
            </td>
        </tr>
        <tr>

            <td colspan="3">
                <p class="text-center para-block para-block-1">Konsultan kami akan <br>
                    menghubungi Anda <br>
                    untuk member <br>
                    solusi terbaik
                </p>
                <p class="text-center para-block para-block-2">
                    Anda memasukkan pesanan <br>
                    dan paket Anda akan <br>
                    segera siap diantar
                </p>
                <p class="text-center para-block para-block-3">
                    Paket sampai di tempat Anda<br>
                    kemudian Anda melakukan <br>
                    pembayaran pada saat <br>
                    pengiriman
                </p>
                <p class="text-center para-block para-block-4">
                    Anda bisa melakukan <br>
                    pemesanan dengan <br>
                    penawara spesial kami <br>
                    kapan saja
                </p>
            </td>
        </tr>
        <tr>
            <td class="border-block" colspan="3">
                <p><b>Butuh bantuan?</b></p>
                <p>Kunjungi website kami untuk  update terbaru produk kami atau hubungi call center di 0888 01000 488 </p>
            </td>
        </tr>
        </tbody>
    </table>
@endsection