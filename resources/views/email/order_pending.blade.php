Halo {{ $order->name }} <br />
Selamat. saudara berhasil mendapatkan diskon termurah di kursus Emil Krebs!<br />
Ingat bahwa Saudara punya waktu  24 jam untuk melakukan pembayaran lewat ATM. Diskon tidak akan berlaku bila melampui batas waktu.<br />
Jika mengalami kesulitan pembayaran,<br />
Silahkan menghubungi layanan bantuan kami<br />
{{ trans('common.phone.prefix') }} {{ trans('common.phone.number') }}
<br /><br />

ATAU<br /><br />

Saudara dapat memilih metode pesan antar dengan pembayaran tunai.<br />
<a href="{{ route('order.recreate',['id' => $order->id, 'token' => $order->token]) }}">Metode Emil Krebs</a>
<br /><br />
Hormat kami,<br />
Fahmi Bayu