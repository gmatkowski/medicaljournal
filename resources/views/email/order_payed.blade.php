<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<center>
    <img src="{{ $message->embed(storage_path('app/mailing2/logo.png')) }}" width="242" height="57" alt="">
    <!-- Save for Web Slices (mailing pobiery.psd) -->
    <table style="width:620px; height:465px; box-sizing:border-box; border:1px solid #0c5797; border-radius:5px; padding:30px 50px;"
           id="Table_01" width="620" height="478" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="19">&nbsp;</td>
        </tr>
        <tr>
            <td rowspan="10">&nbsp;</td>
            <td colspan="17">
                <center>
                    <h1 style="font-family:‘Open Sans’, Arial, sans-serif; font-size:28px; font-weight:600; color:#0c5797">
                        Terima kasih atas kepercayaan Anda!
                    </h1>

                    <h1 style="font-family:‘Open Sans’, Arial, sans-serif;font-size:28px; font-weight:300;margin-top:-15px;color:#0c5797">
                        Kuasai bahasa sekarang!
                    </h1>
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="17">
                <center>
                    <img src="{{ $message->embed(storage_path('app/mailing2/krebs.png')) }}" width="112" height="112"
                         alt="">
                </center>
            </td>
        </tr>
        <tr>
            <td colspan="6" rowspan="2">
                <img src="{{ $message->embed(storage_path('app/mailing2/mailing-pobiery_06.png')) }}" width="184"
                     height="56" alt="">
            </td>
            <td colspan="4">
                <center>
                    <h1 style="font-family:‘Open Sans’, Arial, sans-serif;font-size:34px; font-weight:600;margin-top:40px; margin-bottom:5px;color:#0c5797;">
                        Unduh:
                    </h1>
                </center>
            </td>
            <td colspan="7" rowspan="2">
                <img src="{{ $message->embed(storage_path('app/mailing2/mailing-pobiery_08.png')) }}" width="178"
                     height="56" alt="">
            </td>
        </tr>
        <tr>
            <td colspan="4">
                <img src="{{ $message->embed(storage_path('app/mailing2/mailing-pobiery_09.png')) }}" width="137"
                     height="5" alt="">
            </td>
        </tr>
        <tr>
            <td colspan="17">

                <div style="margin-bottom:15px; display:inline-block; margin-left:auto; margin-right:auto;text-align:center;">

                    @foreach($order->files as $key => $file)
                        <a style="display:inline-block;background: #0c5797;-webkit-border-radius: 5px;-moz-border-radius: 5px;border-radius: 5px;border-bottom:3px solid #08355b;width: 33px;height: 29px;padding-top: 8px;text-align:center;font-family:‘Open Sans’, Arial, sans-serif;font-size:20px;font-weight:600;color:#ffffff;text-decoration:none;margin-left: 10px;margin-top: 5px;"
                           href="{{ route('order.download',[$order->token,$file->hash]) }}">{{ $file->file->part }}</a>
                    @endforeach

                    <div style="clear:both;"/>
                        <br>
                        <span style="font-family:‘Open Sans’, Arial, sans-serif;font-size:16px; font-weight:300;margin-top:-15px;color:#0c5797">
						    Tunjukanlah kursus ini ke teman Anda supaya mereka mengetahui bahwa Anda akan menjadi ahli Bahasa Inggris dalam 30 hari.
	                    </span>
                </div>

            </td>

        </tr>
        <tr>
            <td colspan="17">
                <center>
                    <a href="https://web.facebook.com/sharer/sharer.php?app_id=113869198637480&sdk=joey&u=http%3A%2F%2Fkrebsmethod.co.id%2F&display=popup&ref=plugin&src=share_button">
                        <img src="{{ $message->embed(storage_path('app/mailing2/fb-share.png')) }}" width="200"
                             height="38" alt="">
                    </a>
                </center>
                <br/>
            </td>
        </tr>
        <tr>
            <td rowspan="4">&nbsp;</td>
            <td colspan="17">
                <img src="{{ $message->embed(storage_path('app/mailing2/mailing-pobiery_24.png')) }}" width="451"
                     height="6" alt="">
            </td>
            <td rowspan="4">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="15">
                <img src="{{ $message->embed(storage_path('app/mailing2/mailing-pobiery_26.png')) }}" width="451"
                     height="11" alt="">
            </td>
        </tr>
        <tr>
            <td rowspan="2">&nbsp;</td>
            <td colspan="12">
                <center>
                    <a style="color:#787878;font-family:‘Open Sans’, Arial, sans-serif;font-size:14px"> Jika Anda
                        memiliki pertanyaan silakan kunjungi situs web kami</a>
                    <a style="color:#787878;font-family:‘Open Sans’, Arial, sans-serif;font-size:14px"
                       href="http://krebsmethod.co.id/"><strong>http://krebsmethod.co.id </strong></a>
                </center>
            </td>
            <td colspan="2" rowspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td colspan="12">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="57" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="26" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="22" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="37" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="42" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="43" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="14" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="27" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="42" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="42" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="26" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="15" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="42" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="42" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="30" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="12" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="15" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="22" height="1" alt="">
            </td>
            <td>
                <img src="{{ $message->embed(storage_path('app/mailing2/spacer.gif')) }}" width="64" height="1" alt="">
            </td>
        </tr>
    </table>
    <!-- End Save for Web Slices -->
</center>
</body>
</html>