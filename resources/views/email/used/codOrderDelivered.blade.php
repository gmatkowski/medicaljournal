<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Your Message Subject or Title</title>
    <style type="text/css">
        body {
            width: 100% !important;
            -webkit-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
            margin: 0;
            padding: 0;
        }

        #backgroundTable {
            margin: 0;
            padding: 0;
            width: 100% !important;
            line-height: 100% !important;
        }

        img {
            outline: none;
            text-decoration: none;
            -ms-interpolation-mode: bicubic;
            vertical-align: middle;
        }

        a img {
            border: none;
        }

        p {
            margin: 1em 0;
            line-height: 18px;
        }

        table td {
            border-collapse: collapse;
            font-family: 'Arial', sans-serif;
            font-size: 13px;
            table-layout: fixed;
        }

        table {
            border-collapse: collapse;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        a {
            color: #6dcc4f;
            line-height: 25px;
            text-decoration: none;
        }

        .text-bold {
            font-weight: bold;
        }

        .box-heading {
            background-color: #6dcc4f;
            display: inline-block;
            width: 215px;
            height: 29px;
            padding-left: 5px;
            line-height: 29px;
        }

        .catatan-heading {
            background-color: #cccccc;
            display: inline-block;
            width: 215px;
            height: 29px;
            padding-left: 5px;
            line-height: 29px;
        }

        .catatan-table p {
            padding: 10px 20px;
        }

        p {
            padding-left: 20px;
            margin-bottom: 5px;
            margin-top: 5px;
            min-height: 18px;
        }

        .product-name {
            font-weight: bold;
        }

        .product-row {
            border-bottom: 1px solid #6dcc4f;
        }

        .span_subtext {
            font-size: 8px;
            color: red;
        }

        .sub_text_align {
            display: flex;
            justify-content: flex-end;
        }
    </style>

</head>

<body>
<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%">
    <tr>
        <td align="center">
            <table width="600">
                <tr>
                    <td align="center">
                        <table width="100%">
                            <tr>
                                <td colspan="100%" align="center">
                                    <h2>
                                        Pesanan telah Dikirim
                                    </h2>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="100%" align="center">
                                    <img src="{{ $message->embed(storage_path('app/mailing/pic.jpg')) }}" alt="" style="margin:20px 0;">
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="border: 1px solid #6dcc4f; border-left-width: 9px; margin-top:16px;margin-bottom:16px;" cellpadding="0"
                               cellspacing="0">
                            <tr>
                                <td>
                                    <p>Dear</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="text-bold">
                                        {{ $customer_first_name }}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p class="text-bold">
                                        {{ $customer_last_name }}
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <p>Terima kasih telah berbelanja di
                                        <a href="#">{{ $our_website }}</a> !</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br>
                                    <p>
                                        Pesanan Anda dengan ID Order
                                        <span style="color:#6dcc4f;">({{ $order_id }})</span> telah sukses diterima. Semoga Anda puas produk serta pelayanan
                                        kami.
                                    </p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Kami tunggu pembelanaan Anda selanjutnya.</p>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Terima kasih!</p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>



                <tr>
                    <td>
                        <table width="100%" style="margin-top:16px;margin-bottom:16px;border: 1px solid #6dcc4f;" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="100%">
                                        <span class="box-heading">
                                            <img src="{{ $message->embed(storage_path('app/mailing/Langkah-Selanjutnya.png')) }}" alt=""> Rincian Pesanan
                                        </span>
                                </td>
                            </tr>
                            @foreach ($orders as $order)
                                <tr class="product-row">
                                    <td colspan="50%">
                                        <table width="100%">
                                            <tr>
                                                <td align="center">
                                                    <img src="{{ $message->embed(storage_path('app/mailing/products/'.$order->language->symbol.'.png')) }}" alt="" style="margin:25px 0;">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td colspan="50%">
                                        <table width="100%">
                                            <tr>
                                                <td>
                                                    <p class="product-name">
                                                        {{ $order->language->name }}
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>
                                                        {{ \App\Helpers\StrHelper::dotsInPrice(round($order->price, 3)) }} IDR
                                                    </p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>
                                                        ({{ $order->qty }})
                                                    </p>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            @endforeach
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="margin-top:16px;margin-bottom:16px;">
                            <tr>
                                <td>
                                        <span class="box-heading">
                                            <img src="{{ $message->embed(storage_path('app/mailing/B0p8ZPe.png')) }}" alt=""> Metode Pengiriman via:
                                        </span>
                                </td>
                                <td></td>
                                <td>
                                        <span class="box-heading">
                                            Subtotal:
                                        </span>
                                </td>
                                <td>
                                        <span class="sub_text_align">
                                            R.p. {{ \App\Helpers\StrHelper::dotsInPrice($subtotal) }}
                                        </span>


                                </td>
                            </tr>
                            <tr>
                                <td colspan="100%">
                                    <p>
                                        Cash on Delivery
                                    </p>
                                </td>
                            </tr>

                            <tr>
                                <td>

                                </td>
                                <td></td>
                                <td>
                                        <span class="box-heading">
                                            Ongkos Kirim::
                                        </span>
                                </td>
                                <td>
                                        <span class="sub_text_align">
                                            Rp. 0.00
                                        </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p></p>
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>
                                        <span class="box-heading">
                                            Total:
                                        </span>
                                </td>
                                <td>
                                        <span class="sub_text_align">
                                            Rp. {{ \App\Helpers\StrHelper::dotsInPrice($subtotal) }}
                                        </span>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%" style="margin-top:16px;margin-bottom:16px;border: 1px solid #cccccc;" cellpadding="0" cellspacing="0"
                               class="catatan-table">
                            <tr>
                                <td>
                                        <span class="catatan-heading">
                                            Catatan:
                                        </span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <p>Hubungi <span style="color:#6dcc4f;">({{ $inbound_number }})</span> untuk informasi lebih lanjut, atau kirimkan email ke <span style="color:#6dcc4f;white-space:nowrap;">({{ $our_email }})</span>.
                                    </p>
                                    <p> Atau hubungi kami via Whatsapp <span style="color:#6dcc4f;">({{ $whatsapp_number }}) </span></p>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!-- 600px -->
        </td>
    </tr>
</table>
</body>

</html>