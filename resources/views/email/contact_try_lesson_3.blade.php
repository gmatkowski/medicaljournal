<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Style-Type" content="text/css">
	<style type="text/css">
		p.p1 {margin: 0.0px 0.0px 10.6px 0.0px; font: 12.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p2 {margin: 0.0px 0.0px 0.0px 0.0px; font: 16.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p3 {margin: 0.0px 0.0px 10.6px 0.0px; font: 13.0px 'Times New Roman'; color: #4472c4; -webkit-text-stroke: #4472c4}
		p.p4 {margin: 0.0px 0.0px 10.7px 0.0px; font: 13.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p5 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; color: #0070c0; -webkit-text-stroke: #0070c0}
		p.p6 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		p.p7 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000; min-height: 16.0px}
		p.p9 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; color: #4472c4; -webkit-text-stroke: #4472c4}
		li.li6 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; color: #000000; -webkit-text-stroke: #000000}
		li.li8 {margin: 0.0px 0.0px 0.0px 0.0px; font: 13.0px 'Times New Roman'; -webkit-text-stroke: #000000}
		span.s1 {font-kerning: none}
		span.s2 {font-kerning: none; color: #000000; -webkit-text-stroke: 0px #000000}
		span.s3 {text-decoration: underline ; font-kerning: none}
		span.s4 {-webkit-text-stroke: 0px #000000}
		span.s5 {color: #000000; -webkit-text-stroke: 0px #000000}
		span.s6 {font-kerning: none; color: #4472c4; -webkit-text-stroke: 0px #4472c4}
		span.s7 {font-kerning: none; color: #0070c0; -webkit-text-stroke: 0px #0070c0}
		span.s8 {text-decoration: underline line-through ; font-kerning: none}
		span.s9 {font-kerning: none; background-color: #ffffff}
		ul.ul1 {list-style-type: disc}
	</style>
</head>
<body>

<p class="p3"><span class="s2">Hi </span><span class="s1">{{ $contact->name }}</span><span class="s2">,</span></p>
<p class="p4"><span class="s1">Saya Fahmi. Pemberitahuan ini disampaikan karena permintaan terbanyak hari ini kami memberlakukan promosi spesial kami – Rp {{ number_format($language->price,0,',','.') }} untuk kursus bahasa Krebs Method.</span></p>
<p class="p5"><span class="s2">Kami menerima banyak permintaan untuk memberlakukan kembali harga diskon untuk konsumen. </span><span class="s3">Hari ini adalah kesempatan terakhir Anda untuk membeli kursus bahasa Krebs Method seharga Rp {{ number_format($language->price,0,',','.') }}, dari harga normal semula Rp {{ number_format($language->price_old,0,',','.') }} – Klik disini.</span></p>
<p class="p5"><span class="s1"><br>
</span></p>
<p class="p6"><span class="s1"><b>3 fakta yang menjadikan kursus Krebs Method spesial :</b></span></p>
<p class="p7"><span class="s1"></span><br></p>
<ul class="ul1">
	<li class="li6"><span class="s4"></span><span class="s1">Metode diciptakan oleh seorang ahli bahasa, Emil Krebs yang fasih 68 bahasa.</span></li>
	<li class="li8"><span class="s5"></span><span class="s1">Anda belajar bahasa baru dalam 30 hari</span></li>
	<li class="li8"><span class="s5"></span><span class="s1">Penelitiannya ditelaah dan digunakan untuk memformulasi kursus bahasa audio modern oleh Institusi Emil Krebs – Para profesor dari universitas di Munich, Jerman yang berkolaborasi dengan rekan dari Cambridge, Inggris.</span></li>
</ul>
<p class="p9"><span class="s1"><br>
</span></p>
<p class="p6"><span class="s6"><i>{{ $contact->name }}</i></span><span class="s1"><i>,</i>, ini adalah kesempatan terakhir untuk mendapatkan </span><span class="s7">{{ round($language->price_old / $language->price * 100 ,0) }}% </span><span class="s1">diskon. Ini penawaran luar biasa dari promosi yang sepertinya tidak akan kami lakukan lagi.</span></p>
<p class="p6"><span class="s1"><br>
</span></p>
<p class="p5"><a href="{{ route('order.recreate',['id' => $contact->id, 'token' => $contact->token, 'type' => 'contact', 'page.languages', 'email3']) }}"><span class="s3">Klik disini untuk membeli kursus bahasa Krebs Method dengan harga Rp {{ number_format($language->price,0,',','.') }} (</span><span class="s8">{{ number_format($language->price_old,0,',','.') }}</span><span class="s3">).</span></a></p>
<p class="p1"><span class="s1"><br>
</span></p>
<p class="p6"><span class="s1">Salam hormat,</span></p>
<p class="p6"><span class="s9">Fahmi Bayu</span></p>
</body>
</html>
