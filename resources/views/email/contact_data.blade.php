Halo {{ $contact->name }}, <br />
Saya lihat saudara mengunjungi website kami dan meminta untuk kami hubungi.<br />
Saya ingin menyampaikan dua fakta tentang kursus kami :<br />
-	Kursus kami sangat efisien dan setelah 40 pelajaran saudara akan mampu berkomunikasi dengan bebas dalam bahasa Inggris.<br />
-	Kedutaan dan agen di Amerika menggunakan kursus seperti ini.
<br /><br />
Kursus kami baru saja tersedia di Indonesia, oleh karena itu kami ingin memberikan tawaran sebaik-baiknya untuk saudara.<br />
Saudara hanya perlu mengklik tautan untuk memperoleh diskon terbaik kursus kami :<br />
<a href="{{ route('order.recreate',['id' => $contact->id, 'token' => $contact->token, 'type' => 'contact']) }}">Metode Emil Krebs</a><br />
Ingat, saudara bisa melakukan pembayaran tunai dengan layanan pesan antar ke lokasi tempat tinggal saudara.
<br /><br />
Salam saya,<br />
Fahmi Bayu