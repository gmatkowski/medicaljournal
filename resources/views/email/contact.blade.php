Customer, <br/>
<strong>{{ $data['name'] }}</strong>
<br/><br/>

@if(isset($data['message']))
	<strong>Message:</strong> <br/>
	{{ $data['message'] }}
	<br/><br/>
@endif

@if(isset($data['email']))
	Email: <br /><strong>{{ $data['email'] }}</strong>
@endif
@if(isset($data['phone']))
	<br/><br/>
	Phone: <br /> <strong>{{ $data['phone'] }}</strong>
@endif
<br/><br/>

@if(isset($data['language']))
	Language: <br/>
	<strong>{{ $data['language']->name }}</strong>
@endif
<br/><br/>