<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Style-Type" content="text/css">
	<style type="text/css">
		p.p1 {
			margin: 0.0px 0.0px 10.6px 0.0px;
			font: 12.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000
		}

		p.p2 {
			margin: 0.0px 0.0px 10.6px 0.0px;
			font: 16.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000
		}

		p.p3 {
			margin: 0.0px 0.0px 10.6px 0.0px;
			font: 12.0px 'Times New Roman';
			color: #4472c4;
			-webkit-text-stroke: #4472c4
		}

		p.p4 {
			margin: 0.0px 0.0px 0.0px 0.0px;
			font: 13.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000
		}

		p.p5 {
			margin: 0.0px 0.0px 10.6px 0.0px;
			font: 13.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000
		}

		p.p6 {
			margin: 0.0px 0.0px 0.0px 0.0px;
			font: 16.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000
		}

		p.p7 {
			margin: 0.0px 0.0px 0.0px 0.0px;
			font: 12.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000;
			min-height: 15.0px
		}

		p.p8 {
			margin: 0.0px 0.0px 10.6px 0.0px;
			font: 12.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000;
			min-height: 15.0px
		}

		p.p9 {
			margin: 0.0px 0.0px 0.0px 0.0px;
			font: 12.0px 'Times New Roman';
			color: #000000;
			-webkit-text-stroke: #000000
		}

		p.p10 {
			margin: 0.0px 0.0px 0.0px 0.0px;
			font: 13.0px 'Times New Roman';
			color: #4472c4;
			-webkit-text-stroke: #4472c4
		}

		span.s1 {
			font-kerning: none
		}

		span.s2 {
			font: 12.0px 'Times New Roman';
			font-kerning: none
		}

		span.s3 {
			font: 12.0px 'Times New Roman';
			font-kerning: none;
			color: #4472c4;
			-webkit-text-stroke: 0px #4472c4
		}

		span.s4 {
			font-kerning: none;
			color: #000000;
			-webkit-text-stroke: 0px #000000
		}

		span.s5 {
			text-decoration: underline;
			font-kerning: none
		}

		span.s6 {
			font-kerning: none;
			background-color: #ffffff
		}
	</style>
</head>
<body>

<p class="p3"><span class="s4">Hi </span><span class="s1">{{ $contact->name }},</span></p>

<p class="p4"><span class="s5">Terima kasih karena tertarik untuk belajar bahasa dengan metode Emil Krebs.</span></p>

<p class="p1"><span class="s1"><br>
</span></p>

<p class="p5">
	<span class="s1">Emil Krebs (1867 – 1930) adalah poligot paling menonjol di masa ini karena mampu berbicara dalam 68 bahasa. Menggunakan metode belajar yang dia ciptakan sendiri, si jenius ini mampu menguasai bahasa baru dalam beberapa minggu. Penemuannya telah dipelajari dan diperbaiki oleh Institusi Emil Krebs yang memformulasi kursus audio modern sehingga memungkinkan pembelajaran bahasa dalam 30 hari – dengan hanya 30 menit sehari!</span>
</p>

<p class="p1"><span class="s1"><br>
</span></p>

<p class="p4">
	<span class="s1"><b>Setelah 40 pelajaran, Anda akan belajar bahasa baru yang sangat membantu dalam :</b></span></p>
<ul class="ul1">
	<li class="li4">
		<span class="s6"></span><span class="s1">Karir professional dan gaji yang meningkat</span><span class="s7"><br>
</span></li>
	<li class="li4">
		<span class="s8"></span><span class="s5">Dengan mudah memutuskan untuk belajar atau berlibur di luar negeri</span><span class="s7"><br>
</span></li>
	<li class="li6"><span class="s6"></span><span class="s1">Mengajarkan bahasa pada anak Anda</span></li>
</ul>
<p class="p7"><span class="s1"></span><br></p>

<p class="p4">
	<span class="s9">{{ $contact->name }}</span><span class="s5">, hanya hari ini, </span><span class="s10">{{ \Carbon\Carbon::now()->formatLocalized('%A %d %B') }}, </span><span class="s5">kami punya promosi special – Anda dapat membeli kursus hanya dengan RP {{ number_format($language->price,0,',','.') }}, dari harga normal semula Rp {{ number_format($language->price_old,0,',','.') }}. Promosi hanya berlaku hari ini dan tergantung ketersediaan- dikarenakan jumlah promo kursus segera habis.</span>
</p>

<p class="p8"><span class="s1"><br>
</span></p>

<p class="p9">
	<a href="{{ route('order.recreate',['id' => $contact->id, 'token' => $contact->token, 'type' => 'contact', 'page.languages', 'email1']) }}"><span class="s11">Klik disini untuk memesan kursus dengan harga promo khusus : Rp {{ number_format($language->price,0,',','.') }}</span></a>
</p>

<p class="p1"><span class="s1"><br>
<br>
</span></p>

<p class="p1"><span class="s5">Best regards,</span><span class="s1"><br>
</span><span class="s5">Fahmi Bayu</span></p>
</body>
</html>
