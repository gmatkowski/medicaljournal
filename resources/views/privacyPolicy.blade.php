<p><b>Kebijakan Privasi www.agenda-kesehatan.com</b></p>

<p>Pemberitahuan Privasi ini menjelaskan jenis informasi pribadi yang kami kumpulkan, bagaimana kami menggunakan informasi tersebut, dengan siapa kami membagi informasi tersebut dan pilihan yang dapat Anda buat mengenai pengumpulan serta penggunaan dan pengungkapan informasi tersebut oleh kami. Kami juga menjelaskan langkah-langkah yang kami ambil guna melindungi keamanan informasi dan bagaimana Anda dapat bertanya tentang praktik privasi kami.</p>

<p><b>Informasi pribadi apa yang dikumpulkan?</b></p>

<p>Ketika melakukan pemesanan atau mendaftar di situs kami, sewajarnya Anda mungkin akan diminta untuk menuliskan nama Anda, alamat email, nomor telepon, usia atau detail lainnya untuk membantu Anda dengan pengalaman Anda.</p>

<p><b>Kapan informasi dikumpulkan?</b></p>

<p>Kami mengumpulkan informasi dari Anda ketika Anda menjawab survey atau mengisi formulir.</p>

<p><b>Bagaimana informasi Anda digunakan?</b></p>

<p>Kami dapat menggunakan informasi tentang Anda yang kami peroleh untuk mendaftarkan keanggotaan Anda di situs web kami, melakukan pembelian, mengikuti newsletter kami menggunakan situs kami atau layanan lain, yaitu:</p>

<p>• Informasi Anda membantu kami untuk lebih merespon kebutuhan pribadi Anda</p>
<p>• Untuk meningkatkan penawaran website kami</p>
<p>• Mengirim email berkala</p>
<p>• Meningkatkan layanan pelanggan (live chat, email atau HP)</p>

<p><b>Bagaimana kami melindungi data pribadi Anda?</b></p>

<p>Kami bekerja keras untuk memastikan bahwa situs web kami adalah aman. Situs kami menggunakan scanning kerentanan dan aplikasi pengaman.</p>

<p>Situs kami secara berkala dilakukan Scanning Malware.</p>

<p>Informasi pribadi Anda terkandung di balik jaringan aman dan hanya dapat diakses oleh sejumlah orang yang memiliki hak akses khusus untuk sistem tersebut, dan diperlukan untuk menjaga informasi rahasia. Kami menggunakan enkripsi Secure Sockets Layer (SSL) untuk melindungi informasi belanja Anda yang tertentu saat transit.</p>

<p>Kami menerapkan berbagai langkah-langkah keamanan ketika pengguna menempatkan pesanan masuk, menyerahkan, atau mengakses informasi mereka untuk menjaga keamanan informasi pribadi Anda.</p>

<p>Semua transaksi diproses melalui penyedia gerbang jaringan dan tidak disimpan atau diproses di server kami.</p>

<p><b>Apakah kami menggunakan teknologi 'cookie' pada situs kami?</b></p>

<p>Ya. Cookie adalah kode berukuran kecil, umumnya disimpan pada hard drive komputer pengguna, yang memampukan situs web untuk “memersonalisasikan” dirinya bagi setiap pengguna dengan mengingat informasi mengenai kunjungan pengguna ke situs web. Kami menggunakan teknologi “cookie” pada situs kami untuk melayani Anda dengan lebih cepat dan dengan kualitas lebih baik. Kami juga menggunakan teknologi “cookie” untuk menganalisis frekuensi dan waktu kunjung Anda ke website kami dan memahami preferensi dari para pengguna website kami demi meningkatkan dan menyesuaikan isi website kami.</p>

<p><b>Kami menggunakan cookies untuk tujuan-tujuan berikut:</b></p>
<p>Memahami dan menyimpan preferensi pengguna untuk masa depan kunjungan.</p>
<p>Memahami preferensi dari para pengguna website kami demi meningkatkan dan menyesuaikan isi website kami.</p>
<p>Menganalisis frekuensi dan waktu kunjung Anda ke website kami.</p>
<p>Kompilasi data agregat tentang lalu lintas situs dan interaksi situs dalam rangka untuk menawarkan penggunaan situs dan aplikasi yang lebih baik di masa depan. Situs juga dapat menggunakan layanan pihak ketiga terpercaya yang melacak informasi ini.</p>

<p>Anda dapat memilih untuk memblokir cookies dengan mengubah preferensi privasi Anda berkaitan dengan penggunaan cookies di browser Anda. Karena setiap browser berbeda, lihat Menu Bantuan dari browser Anda untuk mempelajari cara yang benar untuk memodifikasi cookies Anda.</p>

<p>Namun, Anda dapat mengalami kesulitan dalam menggunakan beberapa layanan jika Anda memblokir cookies.</p>

<p>Akan tetapi Anda mungkin mengunjungi situs kami secara anonim, ini tidak akan mempengaruhi layanan penggunaan.</p>

<p><b>Pengungkapan pihak ketiga</b></p>

<p>Kami tidak menjual atau menyewakan data pribadi Anda kepada pihak ketiga kecuali kami menyampaikan pemberitahuan terlebih dahulu kepada Anda. Ini tidak termasuk mitra pengelola situs dan pihak lain yang membantu kami dalam mengoperasikan situs kami, melakukan bisnis kami, atau melayani Anda, asalkan pihak-pihak setuju untuk merahasiakan informasi ini. Kami juga dapat melepaskan informasi Anda ketika kita percaya pelepasan tersebut sesuai untuk kepatuhan hukum, menegakkan kebijakan situs kami, atau melindungi hak, kepemilikan, atau keamanan kami dan pihak lain.</p>

<p>Namun, informasi pengunjung yang sifatnya non-personal dapat diberikan kepada pihak lain untuk pemasaran, iklan, atau penggunaan lainnya.</p>

<p><b>Tautan pihak ketiga</b></p>

<p>Dalam selang waktu yang berbeda-beda, atas pertimbangan kami, kami dapat menyertakan atau menawarkan produk atau layanan pihak ketiga dalam situs kami. Situs pihak ketiga ini memiliki kebijakan kerahasiaan yang terpisah dan independen. Oleh karena itu kami tidak bertanggung jawab atau kewajiban atas isi dan aktivitas situs terkait. Meskipun demikian, kami berusaha untuk melindungi integritas situs kami dan menyambut setiap umpan balik mengenai situs tersebut.</p>

<p><b>Google</b></p>

<p>Persyaratan iklan Google dapat disimpulkan oleh Prinsip Periklanan Google. Hal ini diletakkan di tempat untuk memberikan pengalaman positif bagi pengguna.</p>
<p>https://support.google.com/adwordspolicy/answer/1316548?hl=en</p>

<p>Sekarang kami tidak menggunakan Iklan Google AdSense di situs kami, kami dapat menggunakannya di masa depan.</p>

<p><b>COPPA (Perlindungan Privasi Anak Online)</b></p>

<p>Dalam hal pengumpulan informasi pribadi dari anak-anak di bawah usia 13 tahun, Perlindungan Privasi Anak Online (Children’s Online Privacy Protection Act: COPPA) menempatkan orang tua sebagai penanggung jawab utama dalam melindungi keamanan anak.</p>

<p>Kami tidak secara khusus memasarkan kepada anak-anak di bawah usia 13 tahun.</p>

<p><b>Praktik Informasi yang Adil</b></p>

<p>Prinsip Praktik Informasi yang Adil membentuk tulang punggung hukum kerahasiaan di Amerika Serikat dan konsep-konsep mereka termasuk telah memainkan peran penting dalam pengembangan hukum perlindungan data di seluruh dunia. Memahami Prinsip Praktek Informasi Yang Adil (Fair Information Practice Principles), dan bagaimana mereka harus dilaksanakan adalah penting untuk mematuhi berbagai undang-undang kerahasiaan yang melindungi informasi pribadi.</p>

<p><b>Agar sejalan dengan Praktik Informasi yang Adilkami akan mengambil tindakan responsif berikut, dalam hal terjadinya pelanggaran data terjadi:</b></p>
<p>Kami akan memberitahukan pengguna melalui pengumuman dalam situs</p>
<p>Dalam waktu 7 hari kerja</p>

<p>Kami juga setuju dengan prinsip ganti rugi individu, yang mengharuskan bahwa individu memiliki hak untuk mengejar hak-hak yang berkekuatan hukum terhadap pengumpul data dan prosesor yang gagal untuk mematuhi hukum. Prinsip ini tidak hanya mensyaratkan bahwa individu memiliki hak dilaksanakan terhadap pengguna data, tetapi juga bahwa individu memiliki jalan lain untuk pengadilan atau badan pemerintah untuk menyelidiki dan / atau menuntut ketidakpatuhan oleh pengolah data.</p>

<p><b>CAN SPAM Act</b></p>

<p>CAN–SPAM Act adalah hukum yang menetapkan aturan untuk email komersial, menetapkan persyaratan untuk pesan komersial, memberikan penerima hak untuk penghentian pengiriman email kepada mereka, dan merinci hukuman berat bagi pelanggaran.</p>

<p><b>Kami mengumpulkan alamat email Anda untuk:</b></p>
<p>Mengirimkan informasi, menanggapi pertanyaan, dll.</p>
<p>Melakukan pemesanan.</p>
<p>Mengirimkan informasi tambahan, terkait dengan produk atau layanan kami.</p>
<p>Mengirim email berkala tentang produk kami dan layanan lainnya.</p>

<p><b>Untuk sesuai dengan CAN-SPAM kami setuju untuk hal-hal sebagai berikut:</b></p>

<p><b>Jika suatu saat Anda ingin berhenti berlangganan dari menerima email di kemudian hari, Anda dapat mengirimkan email kepada kami di contact@medical-jurnal.com</b></p>
dan kami akan segera menghapus Anda dari <b>SEMUA</b> korespondensi.

<p><b>Menghubungi Kami</b></p>

<p>Jika ada pertanyaan tentang kebijakan privasi ini, Anda dapat menghubungi kami dengan menggunakan informasi di bawah ini.</p>

<p>Agenda-kesehatan.com<br>
GRAHA LIMA LIMA, JL. TANAH ABANG II NO. 57<br>
JAKARTA, Jakarta Pusat 10160<br>
Indonesia<br>
contact@medical-jurnal.com<br>
+62 888 01000488<br>
</p>