@extends('layout.mask',[
    'title' => 'มาส์กถ่านต้นไผ่',
    'meta_description' => 'มาส์กถ่านต้นไผ่',
    'meta_keywords' => 'มาส์กถ่านต้นไผ่'
])

@section('ico')
    <link rel="shortcut icon" href="{{ asset('build/images/frontend/blackMask.ico') }}" type="image/x-icon">
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bmask/all.css') }}" media="all">
@endsection

@section('content')
    <div class="header-main">
        <a class="logo" href="{{ $uri }}">StarHit</a>
        <div class="header-news">
            <div class="item">
                <a href="http://health.medical-jurnal.com/2016/09/27/makanan-penyebab-alergi">
                    <img alt="" src="{{ asset('build/images/frontend/granat.jpg') }}" class="photo">
                    <span class="title">อาหารก่อให้เกิดการแพ้</span>
                </a>
                <div class="info">อาหารที่ก่อให้เกิดอาการแพ้บ่อยครั้ง</div>
            </div>
            <div class="item">
                <a href="http://health.medical-jurnal.com/2016/09/11/spirulina-dalam-dunia-kedokteran">
                    <img alt="" src="{{ asset('build/images/frontend/spirulina-in-tablet-and-powder-form-1.jpg') }}" class="photo">
                    <span class="title">สาหร่ายสไปรูลิน่าในโลกการแพทย์</span>
                </a>
                <div class="info">สาหร่ายสไปรูลิน่าคือซูเปอร์ฟูดแรกของโลก!</div>
            </div>
        </div>
    </div>
    <div class="header-banner">
        <div title="" id="adriver_banner_316775540"></div>
    </div>
    <div class="wrap">
        <div class="content">

            <div class="primary">
                <div class="main" style="width:70%;">
                    <div class="crumbs">
                        <a href="{{ $uri }}" class="crumbs-main">หน้าหลัก</a>  /
                        <a href="{{ $uri }}" class="crumbs-section">เคล็ดลับสุขภาพ</a>  /
                        <span class="crumbs-section">หน้าเธอสิวหายแล้ว! เป็นไปได้อย่างไร?</span>
                    </div>
                    <h1 class="article-title">หน้าเธอสิวหายแล้ว! เป็นไปได้อย่างไร?</h1>
                    <h2 class="article-announce"> </h2>
                    <h2 class="article-announce"> </h2>
                    <div class="article-social-line">
                        <span class="article-date">2016-11-30 09:00</span>
                    </div>
                    <div class="article-container">
                        <div class="article-body">
                            <div class="figure">

                            </div><br>
                            <p><b>เธอมีอายุสามสิบปีต้นๆ แต่ไม่มีอะไรเปลี่ยนแปลง หน้าของเธอไร้สิวและผิวกระชับมากๆ</b></p>

                            <p></p><center><img src="{{ asset('build/images/frontend/c7.jpg') }}" width="640" height="480" alt=""></center><p></p>

                            <p><b>สามารถกำขัดสิวและสิวเสี้ยนให้หมดไปได้อย่างไร?</b></p>

                            <p></p><center><img src="{{ asset('build/images/frontend/8.jpg') }}" alt=""></center><p></p>

                            <p><b>รีดักชั่น:</b> สวัสดีคุณชินตา! คุณดูสวยมาก ถึงแม้ว่าตอนนี้คุณตั้งครรภ์และอายุสามสิบกว่าแล้ว!</p>

                            <p><b>ชินตา ปราตามา: </b> สวัสดีค่ะ และขอบคุณมากค่ะที่ให้เกียรติมาสัมภาษณ์ฉัน ฉันมีความยินดีอย่างยิ่ง บางครั้งที่ฉันดูกระจก ฉันจะถามกับตัวเองว่า เธออายุ 30 ปีจริงๆใช่ไหมชินตา ฉันรู้สึกว่าฉันอ่อนกว่าวัยขึ้นและรู้สึกเหมือนอายุเพียง 18 ปีเท่านั้น แต่ตอนนี้ฉันทราบแล้ว: <u><a href="{{ $uri }}">มีเคล็ดลับง่ายที่เราสามารถดูแลให้ผิวไร้สิวได้!</a></u></p>

                            <p><img src="{{ asset('build/images/frontend/b6.jpg') }}" width="280" height="250" alt="" align="left" style="margin:10px;"><b>รีดัคชั่น:</b> เข้าใจแล้วค่ะ บอกเราหน่อยค่ะว่าบางครั้งคุณไม่ไปออกรายการหรือไม่ค่ะ?</p>

                            <p><b>ชินตา ปราตามา: </b> ชใช่ค่ะ บางครั้ง แต่ตอนนี้ฉันไม่รู้สึกเบื่อตัวเองแล้ว ฉันกลับไปทำกิจกรรมต่างๆอีกครั้ง มีกิจกรรมสร้างสรรค์มากมายที่ฉันสามารถทำได้ ตอนนี้ฉันได้ร่วมงานกับ เตกูห์ สรัสวันโต และ ตีก้า รายันโต ในรายการทีวีรายการหนึ่ง ในรายการนี้ฉันต้องเดินบนเวทีสูง 1 เมตร</p>

                            <p><b>รีดัคชั่น:</b> มันไม่น่ากลัวเหรอคะ คุณกลัวไหม?</p>

                            <p><b>ชินตา ปราตามา: </b> กลัวค่ะ มันน่ากลัวมาก! ตอนนั้นฉันรู้สึกกลัว แต่ฉันสามารถเอาชนะความกลัวของฉันได้</p>

                            <p><b>รีดัคชั่น:</b> คุณกล้ามากเลยค่ะ จากการที่ได้ชมรายการของคุณ พูดเลยว่า <u><a href="{{ $uri }}">คุณมีสภาพร่างกายที่ดีมากๆ</a></u> คุณมีรูปร่างที่สลิม ไม่มีสิวบนใบหน้าและยังดูกระชับ คุณทำได้อย่างไรคะ <u><a href="{{ $uri }}">ยังมีผู้หญิงอีกมากที่มีปัญหาสิว...</a></u></p>

                            <p><b>ชินตา ปราตามา: </b> ใช่ค่ะ สำหรับรูปร่างที่สลิมของฉันนั้น ตามจริงฉันโชคดีมากที่มีระบบเผาผลาญที่ดี ตามจริงฉันชอบทานอาหารมากๆเลยนะคะ บางครั้ง ฉํนพยายามควบคุมตัวเอง อาทิ อาหารโปรดของฉันคือข้าวผัดและเมื่อวานฉันทานไปเยอะมากๆ...ดังนั้น วันนี้ฉันจึงเลือกทานอย่างอื่น แต่ฉันไม่เคยควบคุมอาหารอย่างเข้มงวด</p>

                            <p><b>รีดัคชั่น:</b> สำหรับผู้อ่านของเรา: <b>คุณมีใบหน้าที่ไร้สิวได้อย่างไรคะ</b>?</p>

                            <p></p><center><img src="{{ asset('build/images/frontend/99.jpg') }}" width="600" height="500" alt=""></center><p></p>

                            <p><b>ชินตา ปราตามา: </b> จริงๆแล้วไม่มีความลับนะคะ ฉันคิดบวกอยู่เสมือ ฉันชอบฟังเพลงและทำในสิ่งที่ชอบ ใช่แล้วค่ะ <u><a href="{{ $uri }}">มาส์กถ่านต้นไผ่</a></u> มีประโยชน์สำหรับฉันมากๆ</p>

                            <p><b>รีดัคชั่น:</b> มีสิ่งใหม่และสิ่งแตกต่าง..นี่คือวิธีที่คุณรักษาความงามบนใบหน้า <a href="{{ $uri }}">ไร้สิวและสิวเสี้ยน</a>ใช่ไหมคะ?</p>

                            <p><img src="{{ asset('build/images/frontend/b1.jpg') }}" width="150" height="150" alt="" align="right" style="margin:10px;"><b>ชินตา ปราตามา: </b> ใช่แล้วค่ะ ด้วย <u><a href="{{ $uri }}">มาส์ก</a></u> ตัวนี้ ไม่เพียงแต่ทำความสะอาดและคืนความอ่อนเยาว์ให้แก่ผิวเท่านั้น แต่ยังช่วยรักษาอาการต่างๆและเพิ่มอายุให้แก่ผิว มีส่วนผสมจากธรรมชาติ ไม่ว่าจะเป็น โปรวิตามินบี 5 ถ่านไม้ไผ่ คอลลาเจน ส้มบาหลี น้ำมันหอมระเหย ธัญพืช จากข้อมูลหมอผิวหนังของฉัน (เธอแนะนำผลิตภัณฑ์ตัวนี้) องค์ประกอบนี้ช่วย <u><a href="{{ $uri }}">กำจัดปัญหาผิว ลดเลือนริ้วรอย</a></u> และทำความผิวให้สะอาดและความสดชื่น</p>

                            <p><b>รีดัคชั่น:</b> แล้วผลลัพธ์เป็นอย่างไรคะ เราขอดูตอนที่ใช้ครั้งแรกได้ไหมคะ?</p>

                            <p><b>ชินตา ปราตามา: </b> ใช่ค่ะ ใช้เวลาเพียงหนึ่งสัปดาห์เท่านั้น ตอนแรก ฉันสามารถกำจัดสิวและสิวเสี้ยนได้ รวมถึงรูขุมขนกระชับขึ้น นอกจากนี้ <u><a href="{{ $uri }}">มาส์กหน้า</a></u>ตัวนี้ยังมีประโยชน์อื่นๆ น้องสาวของฉัน นูร์ฟรีเทรีย ใช้ <u><a href="{{ $uri }}">มาส์กถ่านต้นไผ่</a></u> เช่นกัน ลองดูนะคะ</p>

                            <p><b>รีดัคชั่น:</b> เข้าใจแล้วค่ะ รูปเซลฟี่ดูดีมากๆ น่าสนใจค่ะ แล้วราคาแพงมากไหมค่ะ?</p>

                            <p></p><center><img src="{{ asset('build/images/frontend/b5.jpg') }}" alt=""></center><p></p>

                            <p><b>ชินตา ปราตามา: </b> จริงแล้วหากคุณเปรียบเทียลกับผลิตภัณฑ์ความงามตัวอื่นในตลาด <u><a href="{{ $uri }}">มาส์กถ่านต้นไผ่</a></u> มีราคาถูกกว่ามาก ฉันซื้อมาส์กตัวนี้จากเว็บไซต์ทางการ แต่เพื่อของฉันบอกว่าตอนนี้มีข้อเสนอพิเศษและราคาถูกว่า</p>

                            <p><b>รีดัคชั่น:</b> ขอบคุณมากที่ให้ความสัมภาษณ์กับเรา คุณต้องการบอกอะไรเพิ่มเติมกับผู้อ่านไหมคะ?</p>

                            <p><b>ชินตา ปราตามา: </b> ด้วยความยินดีค่ะ! <u><a href="{{ $uri }}">ลองมาสก์ถ่านต้นไผ่</a></u> ในการทำความสะอาดผิวของคุณจากสิ่งสกปรกและสิ่งตกค้าง ที่อุดตันรูขุมขนและก่อให้เกิดสิวเสี้ยนและช่วยรักษาความงามของคุณ! ในการทำให้ใบหน้าของคุณอ่อนกว่าวัยและงดงาม ไร้สิวและสิวเสี้ยน คุณควรใช้เครื่องสำอางที่ดีๆและราคาไม่แพง รวมถึง <u><a href="{{ $uri }}">มาส์กหน้า</a></u> ที่ได้รับการพิสูจน์ผลลัพธ์แล้ว!</p>

                            <p>เราหวังว่าบทความนี้จะมีประโยชน์ต่อผู้อ่านของเราทุกคน โดยเฉพาะผู้ที่สนใจในวิธีการใหม่ในการกำจัดสิวและสิวเสี้ยน และรักษาความงามไว้ได้: <u><a href="{{ $uri }}">เว็บไซต์ทางการของผู้ผลิต</a></u></p>

                            <p>คุณชินต้าตกลงที่จะแสดงลิงค์เข้าถึงผู้ผลิตทางการของ <u><a href="{{ $uri }}">มาส์กถ่านต้นไผ่</a></u></p>

                            <p class="clear"></p>

                            <a href="{{ $uri }}" class="button" style="display:block!important; text-align:center;">ซื้อมาส์กถ่านต้นไผ่</a>

                            <div class="comments-block">
                                <div class="title">ความเห็น</div>
                                <div class="container">
                                    <ul class="list">
                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">ทีก้า วีเดียวาติ</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>คุณชินต้าดูดีสุด...ฉันอยากดูดีเหมือนเธอตอนอายุ 30!</p></div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/d7.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">เฮตตี้ ฮาร์ตาตี โนวีก้า</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>คุณชินต้าดูดีสุด...ฉันอยากดูดีเหมือนเธอตอนอายุ 30!</p>
                                                    <p>มาส์กหน้าถ่านต้นไผ่! ดีที่สุดแล้วที่ฉันรู้จัก! ฉันต้องเสียเงินไปมากสำหรับค่าครีม เซรัม แต่ในที่สุดได้พบกับมาส์กหน้าถ่านต้นไผ่! มาส์กสีดำ! วิธีนี้ช่วยรักษาความงามและกำจัดสิวจากใบหน้า! ตั้งแต่ครั้งแรกที่ใช้!</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/b2.jpg') }}" widht="250" height="250" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/14.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">อิสซี่ ยูเลียซารี</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>สูตรนี้ราคาถูกกว่าร้านเสริมสวย...</p>

                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/10.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">อีบู ซูเรียติ</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>วิธีการนี้มีประสิทธิภาพในการกำจัดสิวและสิวเสี้ยนอย่างเป็นธรรมชาติ โดยไม่ใช้สารเคมี เยี่ยมไปเลย</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/b3.jpg') }}" widht="250" height="250" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/15.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">รารา เดวี</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ฉันได้ลองแล้ว ดีมากๆ แต่ฉันลืมใช้ตลอด....แต่มันดีมากๆเลยนะ</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/7.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">โรสยีดาตูล โคอิโรห์</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ฉันยังไม่ได้ใช้เลยแต่สนใจมากๆ หน้าฉันมีสิวและอยากได้มาลองใช้...มีผลิตภัณฑ์ตัวอื่นๆที่ทำให้ฉันผิดหวัง ฉันอยากลองมาส์กตัวนี้ มีใครลองใช้บ้างแล้วคะ ใช้นานเท่าไหร่คะ?</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/17.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">ดาวีต้า ซีเรการ์</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ฉันลองใช้ผลิตภัณฑ์ตัวนี้มาสักพักแล้ว ดีมากเลย เนื่องจากผิวของฉันไวต่อความรู้สึกมากๆ (แพ้ง่าย) ฉันใช้มาได้ 1 เดือนแล้ว ใช้ทุกวันเลยและฉันมีความสุขมากๆเพราะว่าสิวของฉันหายไปหมด</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/11.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">ซารี  ไฟซาห์</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>เพื่อนของฉันซื้อมาจากสเปน และฉันซื้อมาส์กของฉันเองเมื่อสองสามอาทิตย์ที่แล้ว ผลลัพธ์ที่ได้นั้นยอดเยี่ยมมากๆ ผิวของฉันดีขึ้นมากและไม่มีสิวเลย ผิวดูสวยและอ่อนวัยขึ้น แนะนำเลยค่ะ!</p>
                                                    <p></p><center><img src="{{ asset('build/images/frontend/b4.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/b8.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">ลิสน่า วาตี</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ดีมากๆ! ผลลัพธ์ที่ได้เห็นได้ชัดในเวลาเพียง 1 สัปดาห์เท่านั้น!</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/18.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">อีเก้ อัสมิน</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ฉันเพิ่งเข้าไปดูในเว็บไซต์มาก และมีข้อเสนอพิเศษ ฉันควรซื้อไหม?</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/c5.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/9.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">ฟานี่ กีกี้</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ก่อนหน้านี้ฉันมีสิวและสิวเสี้ยน แต่ตอนนี้หายไปหมดแล้ว! ฉันสามารถกำจัดสิวและสิวเสี้ยนไปได้ทั้งหมด มาส์กตัวนี้ดีมาก ฉันสั่งซื้อจากเว็บไซต์ทางการ</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/c6.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/19.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">รีนี่  ฉันเดานิงสีห์</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>น่าสนใจมากๆ ฉันจะซื้อมาใช้สักตัวเพราะไม่มีอันตราย – ดูดีมาก ช่วยให้ผิวของฉันกระชับและได้รับสารอาหาร</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/20.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">ดีดี้ วิเดียวาตี</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>ฉันซื้อมาหนึ่งชิ้น! มาส์กตัวนี้มีประสิทธิภาพมากๆในการกำจัดสิวบนหน้าอย่างง่ายดาย</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/12.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">นาดี วิดยันโต</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>หน้าฉันมีทั้งสิวและสิวเสี้ยน ต้องกำจัดแล้ว! ไม่อยากจะเชื่อเลยว่ามีผลิตภัณฑ์แบบนี้ด้วย! สิวของฉันหายไปทั้งหมดภายในเวลาเพียงหนึ่งเดือน!</p>
                                                    <p></p><center><img src="{{ asset('build/images/frontend/c1.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $uri }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/21.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">แซม วิจิก</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>เป็นเวลานานแล้วที่ฉันมองหาผลิตภัณฑ์แบบนี้ ขอบคุณมากค่ะ</p>
                                                </div>
                                            </div>
                                        </li>
                                        {{--NEW COMMENTS--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">Nurul</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>Iya lhoo bgs bgttt, cm pke dikit aj, bekas2 jrwtku ga kliatan, tp stl 3 botol HEBAT! kliatan mulusss:) bebas dari jrwt!! mau order lg:) akhir2 ini wajahku jadi lebih bersih dan cerah stlh 1,5 bulan penggunaanya ! makasih Black Mask!!</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">dewi34</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>kisah nyata!!! stlh 3 botol!!! cara menghilangkan jerawat batu <3 HEBAT</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">Nurlina Darmawan</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>masker ini adalah perawatan yang bagus untuk melawan jerawat! kulit bermasalah adalah sesuatu yang terus menghantui saya meskipun saya sudah mencoba banyak hal! Suatu saat, saya membaca tentan Bamboo Charcoal Black Mask dan memutuskan untuk memesannya karena penasaran. TETAPI setelah 2 minggu pemakaian, saya tidak percaya! kulit saya menjadi lebih segar, dan setelah 1 bulan jauh lebih cantik!!! 3 botol masker ini sangat membantu sehingga saya akan memesannya lagi.</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">pepi</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>Aduh! ini kelihatan seperti sihir! Tak malu lagi, 4-5 botol cukup!!!:)))))))))))</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">Icha Wiadiawati</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>Saya memutuskan untuk mencoba masker ini, memesannya (pengiriman sangat cepat), dan memakainya selama sebulan. Hasilnya membuat saya sangat senang! Setelah 1 botol kulit bertambah bersih dan segar, setelah 4 botol hampir seperti bersinar, bebas dari jerawat, komedo dan flek hitam! Black Mask menghilangkan jerawat dan bintik hitam di wajah saya hanya dalam 1-1,5 bulan. Singkatnya, saya menyukai produk ini dan akan memesan lebih banyak!</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">Berta</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>Wahh, bersih bgt ya wajahnya sejak pakai 3 botol bambu charco black mask</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">youla</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>makasih yah berkat 1 botol black mask wajah sya suda mulai becahaya dan stlh 3 botol... bekas jerwt suda mulai memudar:))) ajaib</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">mei</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>Wahh, jerawatnya sdh hilang ya muka jg bersih bgt berkat 4 botol Bambooo Charcooal Black Mask :))</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">Rauna</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>akhirnya!!! terbebas dari jerawat & komedo <3 terima kasih bambu charcoa black mask!!! 3-4 botol cukup!!</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--<li class="item">--}}
                                            {{--<a href="{{ $uri }}" class="del-button pop"></a>--}}
                                            {{--<div class="user-pic">--}}
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            {{--</div>--}}
                                            {{--<div class="comment">--}}
                                                {{--<div class="comment-info">--}}
                                                    {{--<span class="user-name">Fitri Ramidah</span>--}}
                                                {{--</div>--}}
                                                {{--<div class="comment-text">--}}
                                                    {{--<p>Saya memiliki masalah kulit sejak kecil. Kulit saya cepat berminyak dan jerawat serta bintik-bintik muncul (bahkan bekas luka). Teman saya merekomendasikan masker ini. Saya mencobanya dan setelah 6 kali pemakaian, kulit saya menjadi lebih kering dan bersih. Setelah 15 kali (2 botol)… jerawat mulai menghilang!!! Setelah pemakaian yang intensif selama sebulan, wajah saya jauh lebih baik. Saya senang sekali!</p></div>--}}
                                            {{--</div>--}}
                                        {{--</li>--}}
                                        {{--NEW COMMENTS END--}}
                                    </ul>
                                </div>
                            </div>
                            <center><a href="{{ $uri }}" class="button">ซื้อมาส์กถ่านต้นไผ่</a></center>
                        </div>
                    </div>
                </div>
                <div style="float:right; width:29%; border-radius: 10px;">
                    <div class="overlap">ซื้อมาส์กถ่านต้นไผ่</div>
                    <a href="{{ $uri }}">
                        <img src="{{ asset('build/images/frontend/bambooSide.jpeg') }}" style="width:100%; height:25%;" />
                    </a>
                    <br><br>
                    <div class="overlap">สามารถกำจัดสิวและสิวเสี้ยนได้อย่างไร</div>
                    <video width="320" height="240" controls>
                        <source src="{{ asset('videos/13719138_1551469038494774_318641093_n.mp4') }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
                <a href="{{ $uri }}"></a>
                <div class="right">
                    <div class="right-cell dontMiss">

                        {{--<table cols="1" style="margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr></tr>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: 1px solid rgb(109, 172, 216); padding: 0px;" align="left" valign="top">--}}
                        {{--<div class="abouttext">--}}
                        {{--<center></center>--}}
                        {{--</div>--}}
                        {{--<center>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 2px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v1.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder) }}">Саня</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Хех. Неплохо выглядит Салтыкова)))--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--только что--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v2.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder) }}">Валера</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Не думал, что ей уже 50 )--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--32 минуты назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v3.jpg') }}" width="50px">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder) }}">Ниночка Ростина</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--У меня с лицом все впорядке, но знакомым покажу, хватает у меня друзей с прыщиками.--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--54 минуты назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v4.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder) }}">Виктор Аников</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Брат в Америке живет, так вот говорит что там действительно ОЧЕНЬ популярен эта черная маска...--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v5.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder) }}">Катёна Шантурова</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Впечатляет! Я бы тоже такую маску прикупила. Хочу лицо в порядок привести--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}
                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                        {{--<tbody>--}}
                        {{--<tr>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                        {{--<img src="{{ asset('build/images/frontend/v6.jpg') }}">--}}
                        {{--</td>--}}
                        {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                        {{--<div class="vk_name">--}}
                        {{--<a href="{{ route('page.maskPreOrder) }}">Ирина Лонгина</a>--}}
                        {{--</div>--}}
                        {{--<div class="vk_text">--}}
                        {{--Заказа себе небольшой курс, буду пробовать)--}}
                        {{--</div>--}}
                        {{--<div class="vk_info">--}}
                        {{--час назад--}}
                        {{--</div>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}

                        {{--</center>--}}
                        {{--</td>--}}
                        {{--</tr>--}}
                        {{--</tbody>--}}
                        {{--</table>--}}

                    </div>
                    <div class="block banner-side-mid">
                    </div>
                </div><div class="clear"></div>
            </div>
        </div>
        <div class="header">
            <div class="header-menu">
                <ul>
                    <li class="item novosti">
                        <a href="{{ $uri }}"><span>หน้าหลัก</span></a>
                    </li>
                    <li class="item eksklusiv">
                        <a href="{{ $uri }}"><span>ฟิตเนสและกีฬา</span></a>
                    </li>
                    <li class="item interview">
                        <a href="{{ $uri }}"><span>คล็ดลับสุขภาพ</span></a>
                    </li>
                    <li class="item photoistorii">
                        <a href="{{ $uri }}"><span>อาหารสุขภาพ</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
