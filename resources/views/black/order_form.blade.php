{!! form_start($form, ['id' => 'order']) !!}

        {!! form_widget($form->first_name) !!}
        {!! form_widget($form->email) !!}
        {!! form_widget($form->phone) !!}

        <div class="time">
                <div class="name-t">
                        <button class="button white" type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" form="order">{{ $submitContent }}</button>
                        <br>
                        <b> NOMOR: 0888 01000 488</b>
                        <br>
                        <b>EMAIL: CONTACT@SAINS-JURNAL.COM</b>
                </div>
        </div>
        <div id="wa-wrapper-grey">
                <div id="webwhatsapp">
                        <img src="{{ asset('build/images/frontend/wa-icon.png') }}" alt="">
                        <p>Order by WhatsApp</p>
                        <p class="green">{{ Config::get('whatsapp.number') }}</p>
                </div>
        </div>

        {!! form_widget($form->productId, ['value' => $product2->id]) !!}

        {!! form_widget($form->params, ['value' => $uriParams]) !!}

{!! form_end($form, false) !!}