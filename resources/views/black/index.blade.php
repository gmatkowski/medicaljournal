@extends('layout.mask',[
    'title' => 'Bamboo Charcoal Black Mask',
    'meta_description' => 'Bamboo Charcoal Black Mask',
    'meta_keywords' => 'Bamboo Charcoal Black Mask'
])

@section('ico')
    <link rel="shortcut icon" href="{{ asset('build/images/frontend/blackMask.ico') }}" type="image/x-icon">
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bmask/all.css') }}" media="all">
@endsection

@section('chat')
    <!-- Start of krebs Zendesk Widget script -->
    <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","krebs.zendesk.com");
        /*]]>*/</script>
    <!-- End of krebs Zendesk Widget script -->
@endsection

@section('content')
    <div class="header-main">
        <a class="logo" href="{{ $volum }}">StarHit</a>
        <div class="header-news">
            <div class="item">
                <a href="http://health.medical-jurnal.com/2016/09/27/makanan-penyebab-alergi">
                    <img alt="" src="{{ asset('build/images/frontend/granat.jpg') }}" class="photo">
                    <span class="title">Makanan Penyebab Alergi</span>
                </a>
                <div class="info">Makanan yang paling banyak menyebabkan reaksi alergi</div>
            </div>
            <div class="item">
                <a href="http://health.medical-jurnal.com/2016/09/11/spirulina-dalam-dunia-kedokteran">
                    <img alt="" src="{{ asset('build/images/frontend/spirulina-in-tablet-and-powder-form-1.jpg') }}" class="photo">
                    <span class="title">Spirulina Dalam Dunia Kedokteran</span>
                </a>
                <div class="info">Spirulina adalah superfood pertama di dunia!</div>
            </div>
        </div>
    </div>
    <div class="header-banner">
        <div title="" id="adriver_banner_316775540"></div>
    </div>
    <div class="wrap">
        <div class="content">

            <div class="primary">
                <div class="main" style="width:70%;">
                    <div class="crumbs">
                        <a href="{{ $volum }}" class="crumbs-main">Homepage</a>  /
                        <a href="{{ $volum }}" class="crumbs-section">Tips Kesehatan</a>  /
                        <span class="crumbs-section">Dia berhasil terbebas dari jerawat! Bagaimana caranya?</span>
                    </div>
                    <h1 class="article-title">Dia berhasil terbebas dari jerawat! Bagaimana caranya?</h1>
                    <h2 class="article-announce"> </h2>
                    <h2 class="article-announce"> </h2>
                    <div class="article-social-line">
                        <span class="article-date">2016-11-30 09:00</span>
                    </div>
                    <div class="article-container">
                        <div class="article-body">
                            <div class="figure">

                            </div><br>
                            <p><b>Usianya menginjak 30 tahun, tapi ia terlihat tidak berubah sama sekali. Tidak ada jerawat di wajahnya, masih terlihat sangat kencang.</b></p>

                            <p></p><center><img src="{{ asset('build/images/frontend/c7.jpg') }}" width="640" height="480" alt=""></center><p></p>

                            <p><b>Bagaimana caranya, untuk terbebas dari jerawat, komedo dan flek hitam?</b></p>

                            <p></p><center><img src="{{ asset('build/images/frontend/8.jpg') }}" alt=""></center><p></p>

                            <p><b>Redaksi:</b> Assalamualaikum Ibu Shinta! Anda terlihat cantik saja, meskipun faktanya bahwa Anda baru mengandung di usia 30 tahun!</p>

                            <p><b>Shinta Pratama:</b> Waalaikumsalam. Terima kasih, saya sangat senang. Sebenarnya, kadang-kadang, ketika saya melihat cermin saya berpikir: "Shinta, benarkah usia Anda sekarang ini 30 tahun?!". Memang saya merasa jauh lebih muda, seakan-akan umur saya 18 tahun. Tapi sekarang saya tahu: <u><a href="{{ $volum }}">ada sejumlah tips sederhana yang bisa kita coba untuk terbebas dari jerawat!</a></u></p>

                            <p><img src="{{ asset('build/images/frontend/b6.jpg') }}" width="280" height="250" alt="" align="left" style="margin:10px;"><b>Redaksi:</b> Ya, begitu. Tolong beritahu kami, apakah Anda rindu dunia showbiz atau tidak?</p>

                            <p><b>Shinta Pratama:</b> Ya, kadang-kadang. Tapi sebenarnya, pada saat ini, saya tidak merasa bosan dengan diri sendiri. Saya kembali ke kegiatan saya, ada banyak kegiatan kreatif yang dapat dilakukan saya. Baru saja saya bersama Teguh Suraswanto dan Tika Rayanto berpartisipasi dalam TV show. Di show ini saya berjalan diatas balok titian yang berukuran tinggi 1 meter!</p>

                            <p><b>Redaksi:</b>  Hal itu tidak menakutkan? Anda tak takut?</p>

                            <p><b>Shinta Pratama:</b> Ya, saya takut! Jujur saya takut pada saat itu, tapi saya berhasil mengatasi ketakutan saya.</p>

                            <p><b>Redaksi:</b> Anda sangat berani. Melihat Anda di acara ini dapat dicatat bahwa <u><a href="{{ $volum }}">Anda dalam kondisi sangat baik</a></u>. Anda tetap ramping, tidak ada jerawat di wajah Anda, masih terlihat sangat kencang. Bagaimana caranya? Ada <u><a href="{{ $volum }}">banyak wanita yang terus berjuang untuk terbebas dari jerawat...</a></u></p>

                            <p><b>Shinta Pratama:</b> Ya, tentang sosok ramping saya: sebenarnya, saya hanya beruntung, memiliki predisposisi genetik saja. Salah satu aktivitas favorit dalam hidup saya adalah aktivitas makan. Kadang-kadang saya coba mengendalikan diri. Misalnya, makanan favorit saya adalah makan nasi goreng, dan kemarin saya makannya banyak ya... Jadi hari ini saya makan sesuatu yang lain saja. Namun, saya pernah melakukan diet ketat, saya tidak perlu diet apapun.</p>

                            <p><b>Redaksi:</b> Untuk para pembaca kami: <b>bagaimana cara Anda untuk terbebas dari jerawat</b>?</p>

                            <p></p><center><img src="{{ asset('build/images/frontend/99.jpg') }}" width="600" height="500" alt=""></center><p></p>

                            <p><b>Shinta Pratama:</b> Sebenarnya, tidak ada rahasia apapun. Saya selalu mencoba berpikir positif saja, saya mendengarkan musik favorit saya, melakukan apa yang saya suka. Ya, dan <u><a href="{{ $volum }}">Bamboo Charcoal Black Mask</a></u> sangat bermanfaat untuk saya.</p>

                            <p><b>Redaksi:</b> Sesuatu yang baru dan berbeda ya... Apakah ini cara Anda <b>untuk menjaga kecantikan</b>, dengan wajah kencang, <a href="{{ $volum }}">bebas dari jerawat, komedo dan flek hitam</a>?</p>

                            <p><img src="{{ asset('build/images/frontend/b1.jpg') }}" width="150" height="150" alt="" align="right" style="margin:10px;"><b>Shinta Pratama:</b> Ya, karena <u><a href="{{ $volum }}">masker tersebut</a></u> tidak hanya <b>membersihkan dan meremajakan kulit wajah</b>, namun juga menyembuhkannya, memberikan kesegaran dan meningkatkan vitalitas kulit yang menurun. Komposisinya alami: Pro-Vitamin B5, Arang Bambu, Kolagen, Jeruk Bali Essential Oil, Gandum. Menurut dokter kecantikan saya (dia menyarankan produk ini), komposisi ini justru dapat membantu <u><a href="{{ $volum }}">mengatasi kulit bermasalah, meminimalisir ketidaksempurnaan kulit wajah</a></u> dan menciptakan wajah yang bersih dan segar.</p>

                            <p><b>Redaksi:</b> Bagaimana dengan hasilnya, bisa dilihat sejak pemakaian pertama, dalam waktu singkat?</p>

                            <p><b>Shinta Pratama:</b> Ya, dalam waktu kira-kira seminggu. Pada awalnya <b>saya berhasil menghilangkan jerawat dan komedo, dan mengencilkan pori-pori</b>. Ada jauh lebih banyak manfaat yang ditawarkan oleh <u><a href="{{ $volum }}">masker ini</a></u>, saya pribadi memakainya setiap hari. Adik saya, Nurfitria, juga memakai <u><a href="{{ $volum }}">Bamboo Charcoal Black Mask</a></u> itu. Silahkan lihat gambarnya.</p>

                            <p><b>Redaksi:</b> Begitu. Selfienya bagus. Sangat menarik, pasti mahal harganya?</p>

                            <p></p><center><img src="{{ asset('build/images/frontend/b5.jpg') }}" alt=""></center><p></p>

                            <p><b>Shinta Pratama:</b> Sebenarnya kalau dibandingkan dengan krim-krim kecantikan dan cara menjaga kecantikan wajah lain, yang beredar dipasaran, <u><a href="{{ $volum }}">Bamboo Charcoal Black Mask</a></u> murah sekali. Saya berhasil beli <u><a href="{{ $volum }}">masker tersebut</a></u> di website resmi. Tapi teman-teman saya nanti bilang bahwa sekarang ada promo, sehingga mereka mendapatkannya lebih murah lagi.</p>

                            <p><b>Redaksi:</b> Terima kasih atas wawancara ini, sangat menarik. Apakah Anda ingin menunjukkan sesuatu kepada para pembaca kami?</p>

                            <p><b>Shinta Pratama:</b> Sama-sama! <u><a href="{{ $volum }}">Coba Bamboo Charcoal Black Mask</a></u> untuk <b>membersihkan kulit wajah dari kotoran dan debu</b> yang dapat menyumbat pori-pori penyebab timbulnya komedo, untuk menjaga kecantikan! Agar wajah terlihat awet muda dan cantik, dan <b>terbebas dari jerawat, komedo dan flek hitam</b>, cukup menggunakan kosmetik yang baik dengan harga terjangkau. Dan masker wajah <u><a href="{{ $volum }}">tersebut</a></u> adalah bukti!</p>

                            <p>Semoga artikel ini bermanfaat untuk para pembaca, khususnya untuk Anda yang tertarik dengan cara baru ini <b>untuk terbebas dari jerawat, komedo dan flek hitam, dan menjaga kecantikan:</b> website produsen resmi <u><a href="{{ $volum }}">masker tersebut</a></u>.</p>

                            <p>Ibu Shinta setuju untuk menunjukkan link website produsen resmi <u><a href="{{ $volum }}">Masker Wajah «Bamboo Charcoal Black Mask»</a></u>.</p>

                            <p class="clear"></p>

                            <a href="{{ $volum }}" class="button" style="display:block!important; text-align:center;">BELI Bamboo Charcoal Black Mask</a>

                            <div class="comments-block">
                                <div class="title">Komentar-komentar</div>
                                <div class="container">
                                    <ul class="list">
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Tika Widiawati</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Ibu Shinta cantik ya… usia 30 thn mau cantik seperti dia!</p></div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/d7.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Hetty Hartati Novika</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>>>>Ibu Shinta cantik ya… usia 30 thn mau cantik seperti dia!</p>
                                                    <p>Masker Wajah Bamboo Charcoal Black Mask! Paling hebat yg aku tahu! Aku kehilangan banyak uang untuk krim, serum, dll... Akhirnya Bamboo Charcoal Black Mask! Cara untuk menjaga kecantikan, terbebas dari jerawat yg benar! Setelah penggunaan pertama!</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/b2.jpg') }}" widht="250" height="250" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/14.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Issy Yuliasari</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Formula itu lebih murah daripada salon kecantikan dll…</p>

                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/10.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Ibu Suriait</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Cara sangat efektif utk menghilangkan jerawat dan komedo secara alami. Tanpa kimia, bagus.</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/b3.jpg') }}" widht="250" height="250" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/15.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rara Devi</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>oh sdh coba,bagus, tapi selalu lupa gunakannya ya.. tapi bagus sekali.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/7.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rosyidatul Khoiroh</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Aku belum menggunakan, sangat menarik, ada jerawat di wajah, mau beli… produk lain itu kecewa saja. Mau coba masker ini. sudah ada yg coba, berapa lama gunakanya?</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/17.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Dwita Siregar</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Saya sudah lama menggunakan terapi ini, bagus sekali, karena kulit saya sangat sensitif (alergi), gunakannya sdh 1 bulan, setipa hari, dan senang sekali, karena jerawat dan komedo sudah habis.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/11.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Sari Faizah</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>temanku belinya di Spanyol `sdh beli masku itu beberapa minggu lalu. Efeknya luar biasa, kulitku bagus, tak ada masalah, jerawat di wajahku sdh habus, wajahku segar dan cantik, terlihat lebih segar & muda. Rekomendasikan!</p>
                                                    <p></p><center><img src="{{ asset('build/images/frontend/b4.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/b8.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Lisna Wati</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>bagus sekali! Hasilnya dapat terlihat setelah 1 minggu!</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/18.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Ige Azmin</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>saya baru megunjungi website itu, ada promo ya, tapi saya masih binung , beli atau tidak?</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/c5.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/9.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Fany Kiki</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>sebelumnya ada jerawat dan komedo, sekarang habis! Saya berhasil menghilangkan jerawat komedo dll sekarang terlihat jauh lebih baik. Masker ini bagus sekali, saya pesan melalui website resmi.</p>

                                                    <p></p><center><img src="{{ asset('build/images/frontend/c6.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/19.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rini Handayaningsih</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>sangat menarik, saya akan beli, karena tak ada bahaya apapun- komposisinya bagus. Lifting dan nutrisi kulit wajah ya.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/20.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Dede Widiawati</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>saya sdh beli! Menurut saya masker itu sangat efektif untuk menghilangkan jerawat di wajah dengan mudah.</p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/12.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Nadi Widyanto</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>jerawat dan komedo di wajah. Harus mengatasinya! Sulit dipercaya tapi memang ada, dalam 1 bulan berhasil menghilangkannya- benar-benar!</p>
                                                    <p></p><center><img src="{{ asset('build/images/frontend/c1.jpg') }}" alt=""></center><p></p>
                                                </div>
                                            </div>
                                        </li>

                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                <img alt="" src="{{ asset('build/images/frontend/21.jpg') }}" width="40">
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Sam Wijik</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Sudah lama cari sesuatu seperti ini, makasih ya.</p>
                                                </div>
                                            </div>
                                        </li>
                                        {{--NEW COMMENTS--}}
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Nurul</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Iya lhoo bgs bgttt, cm pke dikit aj, bekas2 jrwtku ga kliatan, tp stl 3 botol HEBAT! kliatan mulusss:) bebas dari jrwt!! mau order lg:) akhir2 ini wajahku jadi lebih bersih dan cerah stlh 1,5 bulan penggunaanya ! makasih Black Mask!!</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">dewi34</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>kisah nyata!!! stlh 3 botol!!! cara menghilangkan jerawat batu <3 HEBAT</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Nurlina Darmawan</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>masker ini adalah perawatan yang bagus untuk melawan jerawat! kulit bermasalah adalah sesuatu yang terus menghantui saya meskipun saya sudah mencoba banyak hal! Suatu saat, saya membaca tentan Bamboo Charcoal Black Mask dan memutuskan untuk memesannya karena penasaran. TETAPI setelah 2 minggu pemakaian, saya tidak percaya! kulit saya menjadi lebih segar, dan setelah 1 bulan jauh lebih cantik!!! 3 botol masker ini sangat membantu sehingga saya akan memesannya lagi.</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">pepi</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Aduh! ini kelihatan seperti sihir! Tak malu lagi, 4-5 botol cukup!!!:)))))))))))</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Icha Wiadiawati</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Saya memutuskan untuk mencoba masker ini, memesannya (pengiriman sangat cepat), dan memakainya selama sebulan. Hasilnya membuat saya sangat senang! Setelah 1 botol kulit bertambah bersih dan segar, setelah 4 botol hampir seperti bersinar, bebas dari jerawat, komedo dan flek hitam! Black Mask menghilangkan jerawat dan bintik hitam di wajah saya hanya dalam 1-1,5 bulan. Singkatnya, saya menyukai produk ini dan akan memesan lebih banyak!</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Berta</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Wahh, bersih bgt ya wajahnya sejak pakai 3 botol bambu charco black mask</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">youla</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>makasih yah berkat 1 botol black mask wajah sya suda mulai becahaya dan stlh 3 botol... bekas jerwt suda mulai memudar:))) ajaib</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">mei</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Wahh, jerawatnya sdh hilang ya muka jg bersih bgt berkat 4 botol Bambooo Charcooal Black Mask :))</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Rauna</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>akhirnya!!! terbebas dari jerawat & komedo <3 terima kasih bambu charcoa black mask!!! 3-4 botol cukup!!</p></div>
                                            </div>
                                        </li>
                                        <li class="item">
                                            <a href="{{ $volum }}" class="del-button pop"></a>
                                            <div class="user-pic">
                                                {{--<img alt="" src="{{ asset('build/images/frontend/13.jpg') }}" width="40">--}}
                                            </div>
                                            <div class="comment">
                                                <div class="comment-info">
                                                    <span class="user-name">Fitri Ramidah</span>
                                                </div>
                                                <div class="comment-text">
                                                    <p>Saya memiliki masalah kulit sejak kecil. Kulit saya cepat berminyak dan jerawat serta bintik-bintik muncul (bahkan bekas luka). Teman saya merekomendasikan masker ini. Saya mencobanya dan setelah 6 kali pemakaian, kulit saya menjadi lebih kering dan bersih. Setelah 15 kali (2 botol)… jerawat mulai menghilang!!! Setelah pemakaian yang intensif selama sebulan, wajah saya jauh lebih baik. Saya senang sekali!</p></div>
                                            </div>
                                        </li>
                                        {{--NEW COMMENTS END--}}
                                    </ul>
                                </div>
                            </div>
                            <center><a href="{{ $volum }}" class="button">BELI Bamboo Charcoal Black Mask</a></center>
                        </div>
                    </div>
                </div>
                <div style="float:right; width:29%; border-radius: 10px;">
                    <div class="overlap">BELI BAMBOO CHARCOAL BLACK MASK</div>
                    <a href="{{ $volum }}">
                        <img src="{{ asset('build/images/frontend/bambooSide.jpeg') }}" style="width:100%; height:25%;" />
                    </a>
                    <br><br>
                    <div class="overlap">CARA MENGHILANGKAN JERAWAT KOMEDO DAN FLEK HITAM</div>
                    <video width="320" height="240" controls>
                        <source src="{{ asset('videos/13719138_1551469038494774_318641093_n.mp4') }}" type="video/mp4">
                        Your browser does not support the video tag.
                    </video>
                </div>
                <a href="{{ $volum }}"></a>
                <div class="right">
                    <div class="right-cell dontMiss">

                        {{--<table cols="1" style="margin-top: 5px; margin-bottom: 6px;" cellpadding="0" cellspacing="0">--}}
                            {{--<tbody>--}}
                            {{--<tr></tr>--}}
                            {{--<tr>--}}
                                {{--<td style="background-color: rgb(255, 255, 255); border: 1px solid rgb(109, 172, 216); padding: 0px;" align="left" valign="top">--}}
                                    {{--<div class="abouttext">--}}
                                        {{--<center></center>--}}
                                    {{--</div>--}}
                                    {{--<center>--}}
                                        {{--<table cols="2" style="margin-top: 0px; border-top: 2px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                                                    {{--<img src="{{ asset('build/images/frontend/v1.jpg') }}">--}}
                                                {{--</td>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                                                    {{--<div class="vk_name">--}}
                                                        {{--<a href="{{ $volum }}">Саня</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_text">--}}
                                                        {{--Хех. Неплохо выглядит Салтыкова)))--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_info">--}}
                                                        {{--только что--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                                                    {{--<img src="{{ asset('build/images/frontend/v2.jpg') }}">--}}
                                                {{--</td>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                                                    {{--<div class="vk_name">--}}
                                                        {{--<a href="{{ $volum }}">Валера</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_text">--}}
                                                        {{--Не думал, что ей уже 50 )--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_info">--}}
                                                        {{--32 минуты назад--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                                                    {{--<img src="{{ asset('build/images/frontend/v3.jpg') }}" width="50px">--}}
                                                {{--</td>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                                                    {{--<div class="vk_name">--}}
                                                        {{--<a href="{{ $volum }}">Ниночка Ростина</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_text">--}}
                                                        {{--У меня с лицом все впорядке, но знакомым покажу, хватает у меня друзей с прыщиками.--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_info">--}}
                                                        {{--54 минуты назад--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                                                    {{--<img src="{{ asset('build/images/frontend/v4.jpg') }}">--}}
                                                {{--</td>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                                                    {{--<div class="vk_name">--}}
                                                        {{--<a href="{{ $volum }}">Виктор Аников</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_text">--}}
                                                        {{--Брат в Америке живет, так вот говорит что там действительно ОЧЕНЬ популярен эта черная маска...--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_info">--}}
                                                        {{--час назад--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                                                    {{--<img src="{{ asset('build/images/frontend/v5.jpg') }}">--}}
                                                {{--</td>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                                                    {{--<div class="vk_name">--}}
                                                        {{--<a href="{{ $volum }}">Катёна Шантурова</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_text">--}}
                                                        {{--Впечатляет! Я бы тоже такую маску прикупила. Хочу лицо в порядок привести--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_info">--}}
                                                        {{--час назад--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}
                                        {{--<table cols="2" style="margin-top: 0px; border-top: 1px solid rgb(218, 225, 232);" cellpadding="0" cellspacing="0">--}}
                                            {{--<tbody>--}}
                                            {{--<tr>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 60px; padding: 8px 4px;" align="center" valign="top">--}}
                                                    {{--<img src="{{ asset('build/images/frontend/v6.jpg') }}">--}}
                                                {{--</td>--}}
                                                {{--<td style="background-color: rgb(255, 255, 255); border: medium none; width: 150px; padding: 6px 6px 6px 4px; vertical-align: top;" align="left" valign="top">--}}
                                                    {{--<div class="vk_name">--}}
                                                        {{--<a href="{{ $volum }}">Ирина Лонгина</a>--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_text">--}}
                                                        {{--Заказа себе небольшой курс, буду пробовать)--}}
                                                    {{--</div>--}}
                                                    {{--<div class="vk_info">--}}
                                                        {{--час назад--}}
                                                    {{--</div>--}}
                                                {{--</td>--}}
                                            {{--</tr>--}}
                                            {{--</tbody>--}}
                                        {{--</table>--}}

                                    {{--</center>--}}
                                {{--</td>--}}
                            {{--</tr>--}}
                            {{--</tbody>--}}
                        {{--</table>--}}

                    </div>
                    <div class="block banner-side-mid">
                    </div>
                </div><div class="clear"></div>
            </div>
        </div>
        <div class="header">
            <div class="header-menu">
                <ul>
                    <li class="item novosti">
                        <a href="{{ $volum }}"><span>HOMEPAGE</span></a>
                    </li>
                    <li class="item eksklusiv">
                        <a href="{{ $volum }}"><span>FITNESS & OLAHRAGA</span></a>
                    </li>
                    <li class="item interview">
                        <a href="{{ $volum }}"><span>TIPS KESEHATAN</span></a>
                    </li>
                    <li class="item photoistorii">
                        <a href="{{ $volum }}"><span>MAKANAN & DIET SEHAT</span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
@endsection
