<!DOCTYPE html>
<html>
    <head>

    <title>Black Mask - Membantu membersihkan kulitmu dari noda dan jerawat!</title>
    <meta name="description" content="Black Mask - Membantu membersihkan kulitmu dari noda dan jerawat!">
    <meta name="keywords" content="Black Mask, Masker Black Mask, Harga Black Mask, Ulasan tentang Black Mask, Beli Online  Black Mask">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=480">

    <link href="img/favicon.ico" type="image/x-icon" rel="shortcut icon">
    <link href="img/favicon.ico" type="image/x-icon" rel="icon">

    <link rel="stylesheet" type="text/css" href="{{ elixir('css/maskerHitam2/all.css') }}">

    @include('pixel.maskerhitam')

    </head>
<body>

    <div class="main-wrap">
        <div class="section block-1" style="background: url(&quot;build/images/frontend/b11.jpg&quot;) center top no-repeat;">
            <div class="block-1-1">
                <h1 class="Roboto-Bold col-fff">BLACK MASK</h1>
                <h2 class="Roboto-Regular col-fff">BLACK MASK MENYINGKIRKAN JERAWAT <br>DAN KOMEDO</h2>
            </div>
            <div class="block-1-2">
                <div class="old Roboto-Bold col-fff"><span class="price_land_s4">{{ StrHelper::spaceInPrice($product2->price_old) }}</span> <span class="price_land_curr">{{ $product2->currency }}</span></div>
                <div class="sale pfdintextcomppro-medium col-fff">-50%</div>
                <div class="new Roboto-Bold col-e53c8b"><span class="price_land_s1">{{ StrHelper::spaceInPrice($product2->price) }}</span> <span class="price_land_curr">{{ $product2->currency }}</span></div>
            </div>
            <div class="block-1-3">
                <p class="Roboto-Bold col-fff">Sisa waktu untuk membeli:</p>
                <div class="countbox">
                    <div class="countbox-num">
                        <div class="countbox-hours1 timer-wrapper-time-hour-one"></div>
                        <div class="countbox-hours2 timer-wrapper-time-hour-two"></div>
                        <div class="countbox-hours-text"></div>
                    </div>
                    <div class="countbox-space"></div>
                    <div class="countbox-num">
                        <div class="countbox-mins1 timer-wrapper-time-minute-one"></div>
                        <div class="countbox-mins2 timer-wrapper-time-minute-two"></div>
                        <div class="countbox-mins-text"></div>
                    </div>
                    <div class="countbox-space"></div>
                    <div class="countbox-num">
                        <div class="countbox-secs1 timer-wrapper-time-second-one"></div>
                        <div class="countbox-secs2 timer-wrapper-time-second-two"></div>
                        <div class="countbox-secs-text"></div>
                    </div>
                </div>
            </div>

            {!! form_start($form, ['id' => 'order']) !!}

            {!! form_widget($form->first_name) !!}
            {!! form_widget($form->email) !!}
            {!! form_widget($form->phone) !!}

            {!! form_widget($form->productId, ['value' => 2]) !!}

            <button type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();"></button>

            {!! form_end($form, false) !!}

            <p class="b1p col-fff">Black Mask memberi<br>
                hasil nyata dalam<br>sekali pemakaian!</p>
            <p class="b2p col-fff">Formula yang efektif untuk<br>
                melawan masalah pada kulit, sudah terkenal<br>
                di seluruh dunia.</p>
        </div>
        <div class="section block-2" style="background: url(&quot;build/images/frontend/b22.jpg&quot;) center top no-repeat;">
            <h2>Black Mask mengandung:</h2>
            <h3 class="Roboto-Bold col-ff57a5">provitamin B5</h3>
            <p>memberi efek menenangkan, melembabkan (menembus jauh ke dalam lapisan kulit dan mengikat air), melembutkan dan menghaluskan kulit. Mengembalikan elastisitas pada kulit, menenangkannya, mengurangi iritasi dan pembengkakan pada kulit.</p>
            <h3 class="Roboto-Bold col-ff57a5">BAMBOO CHARCOAL</h3>
            <p>bahan yang ampuh untuk membersihkan kulit Anda secara mendalam. Zat aktifnya menembus ke dalam pori-pori kulit, membersihkannya dari kotoran dan zat berbahaya. Efektif melawan bintik hitam dan mengontrol kelenjar minyak.</p>
            <h3 class="Roboto-Bold col-ff57a5">wheat germ</h3>
            <p>Meratakan warna kulit, mengencangkan, meregenerasi sel kulit, memudarkan kerut, melembutkan, melembabkan, dan menenangkan kulit. Bahan aktifnya menjadikan kulit lebih lembut dan halus</p>
            <h3 class="Roboto-Bold col-ff57a5">Minyak jeruk bali</h3>
            <p>Menormalkan produksi minyak di wajah, mengecilkan pori dan mencerahkan kulit. Meningkatkan kinerja pelembab alami kulit sehingga dapat mencegah timbulnya bintik hitam.</p>
        </div>
        <div class="section block-3 col-fff">
            <h2 class="Roboto-Regular">CARA MENGGUNAKAN<br><span class="Roboto-Bold">BLACK MASK?</span></h2>
            <div class="step">
                <h3>Tahap 1</h3>
                <p>Aplikasikan masker dengan lapisan tebal pada area wajah yang bermasalah, hindari area mata, sekitar rambut dan area yang sedang terluka.</p>
            </div>
            <div class="step">
                <h3>Tahap 2</h3>
                <p>Diamkan dan tunggu selama 25 menit, hingga masker menjadi kering dan elastis. Saat ini bahan-bahan aktif dalam masker akan bekerja dan masuk ke dalam lapisan kulit. </p>
            </div>
            <div class="step">
                <h3>Tahap 3</h3>
                <p>Angkat masker dengan gerakan lembut dan bersihkan sisa masker yang tertinggal di wajah dengan air hangat. Gunakan 3 kali dalam seminggu.</p>
            </div>
        </div>
        <div class="section block-4">
            <h2 class="col-fff">Pendapat ahli:</h2>
            <h3 class="col-fff">Karen Adamson<br><span>Dokter ahli biologi</span></h3>
            <ul>
                <li>Hasil terlihat pada 91 dari 100 subjek  setelah pemakaian pertama</li>
                <li>84% kulit terlihat lebih sehat setelah 14 hari pemakaian masker secara teratur</li>
                <li>93% pengguna Black Mask menyingkirkan jerawat dan komedo secara permanen setelah pemakaian teratur (1 bulan)</li>
            </ul>
            <p>Produk kami hanya menggunakan bahan premium alami. Perusahaan kami menggunakan metode inovatif dan modern dalam menciptakan rangkaian produk perawatan kulit yang sukses.</p>
            <p>Sebelum dijual, kosmetik kami telah melewati kontrol dermatologis dan uji klinis. Black Mask sangat efektif dalam memerangi jerawat dan masalah kulit.</p>
            <a class="button" href="#order"></a>
        </div>
        <div class="section block-5 col-fff">
            <h2 class="Roboto-Regular">komentar <span class="Roboto-Bold">konsumen</span></h2>
            <div class="rev">
                <img alt="купить Black Mask" title="купить Black Mask" src="{{ asset('build/images/frontend/rev1.png') }}">
                <h3>Dewi</h3>

                <p>Aku tadinya tidak percaya ada masker yang bisa membantuku! Tapi pacarku suka sekali menngunakan masker ini dan meyakinkanku untuk memesan. Nah, akhirnya kurir datang membawa pesananku, aku bayar saat pesanan datang, dan mulai mencoba. Awalnya aku tidak memperhatikan apapun, tapi setelah seminggu jerawat sudah berkurang dan kulitku jadi lebih bersih. Setelah sebulan, wajahku benar-benar berubah, kulitku jadi semakin cantik dari sebelumnya!</p>
            </div>
            <div class="rev">
                <img alt="купить Black Mask" title="купить Black Mask" src="{{ asset('build/images/frontend/rev2.png') }}">
                <h3>Tika</h3>

                <p>Cara yang sangat bagus untuk menyingkirkan jerawat dan komedo! Kulit yang bermasalah sudah menghantui saya selama bertahun-tahun dan selama ini saya biarkan saja. Kemudian saya mengetahui tentang Black Mask ini dan mencoba memesannya hanya untung iseng saja. Tapi setelah menggunakannya selama 2 minggu, saya sungguh terkejut, kulitku jadi lebih cantik dan sehat. Masker ini sangat membantu, dan saya berniat untuk memesannya lagi.</p>
            </div>
            <div class="rev">
                <img alt="купить Black Mask" title="купить Black Mask" src="{{ asset('build/images/frontend/rev3.png') }}">
                <h3>Hetty</h3>

                <p>Saya membaca banyak ulasan tentang Black Mask ini di website-website luar negeri, dan sekarang saya memutuskan untuk mencobanya sendiri. Saya memesan 2 botol (untuk pemakaian satu bulan) dan setelah satu bulan tidak hanya jerawat dan komedo saja yang lenyap tapi kulit jadi semakin bersih dan sehat. Setelah menggunakan ini, kulit terasa halus dan kenyal. Jika ada yang bilang produk ini jelek, mungkin mereka telah membeli barang yang palsu.</p>
            </div>
        </div>
        <div class="section block-6" style="background: url(&quot;build/images/frontend/b66.jpg&quot;) center top no-repeat;">
            <h2 class="Roboto-Regular col-ff57a5">PENGIRIMAN DAN PEMBAYARAN</h2>
            <ul>
                <li>Gratis Ongkos Kirim</li>
                <li>Pembayaran dapat dilakukan saat pengiriman</li>
                <li>Anda dapat membatalkan pembelian apabila sales kami tidak konfirmasi pembelian dalam 14 hari</li>
            </ul>
            <div class="block-1-2">
                <div class="old Roboto-Bold col-fff"><span class="price_land_s4">{{ StrHelper::spaceInPrice($product2->price_old) }}</span> <span class="price_land_curr">{{ $product2->currency }}</span></div>
                <div class="sale pfdintextcomppro-medium col-fff">-50%</div>
                <div class="new Roboto-Bold col-e53c8b"><span class="price_land_s1">{{ StrHelper::spaceInPrice($product2->price) }}</span> <span class="price_land_curr">{{ $product2->currency }}</span></div>
            </div>
            <div class="block-1-3" id="order">
                <p class="Roboto-Bold col-fff">Sisa waktu untuk membeli:</p>
                <div class="countbox">
                    <div class="countbox-num">
                        <div class="countbox-hours1 timer-wrapper-time-hour-one"></div>
                        <div class="countbox-hours2 timer-wrapper-time-hour-two"></div>
                        <div class="countbox-hours-text"></div>
                    </div>
                    <div class="countbox-space"></div>
                    <div class="countbox-num">
                        <div class="countbox-mins1 timer-wrapper-time-minute-one"></div>
                        <div class="countbox-mins2 timer-wrapper-time-minute-two"></div>
                        <div class="countbox-mins-text"></div>
                    </div>
                    <div class="countbox-space"></div>
                    <div class="countbox-num">
                        <div class="countbox-secs1 timer-wrapper-time-second-one"></div>
                        <div class="countbox-secs2 timer-wrapper-time-second-two"></div>
                        <div class="countbox-secs-text"></div>
                    </div>
                </div>
            </div>

            {!! form_start($form, ['id' => 'order']) !!}

            {!! form_widget($form->first_name) !!}
            {!! form_widget($form->email) !!}
            {!! form_widget($form->phone) !!}

            {!! form_widget($form->productId, ['value' => 2]) !!}

            <button type="submit"></button>

            {!! form_end($form, false) !!}

            <center>
                <a href="" onclick="window.open('/privacy-policy', 'windowname1', 'width=1024, height=768, scrollbars=1'); return false;">Kebijakan Privasi</a>
            </center>
        </div>
    </div>

    <script src="{{ asset('build/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('build/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>

    <script>
        String.prototype.toHHMMSS = function () {
            var sec_num = parseInt(this, 10),
                    hours = Math.floor(sec_num / 3600),
                    minutes = Math.floor((sec_num - (hours * 3600)) / 60),
                    seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            return hours + ' ' + minutes + ' ' + seconds;
        };

        var start = moment({hour: 0, minute: 0, seconds: 0, milliseconds: 0}).unix();
        var now = moment().unix();
        var count = String((86400 - (now - start)));

        var counter = setInterval(timer, 1000);

        function timer() {
            if (parseInt(count) <= 0) {
                clearInterval(counter);
                $('#promotion-holder').remove(0);
                return;
            }
            var temp = count.toHHMMSS();
            var tempArr = temp.split(' ');
            var hour1 = tempArr[0][0];
            var hour2 = tempArr[0][1];
            var min1 = tempArr[1][0];
            var min2 = tempArr[1][1];
            var sec1 = tempArr[2][0];
            var sec2 = tempArr[2][1];

            count = (parseInt(count) - 1).toString();
            $('.timer-wrapper-time-hour-one').html(hour1);
            $('.timer-wrapper-time-hour-two').html(hour2);
            $('.timer-wrapper-time-minute-one').html(min1);
            $('.timer-wrapper-time-minute-two').html(min2);
            $('.timer-wrapper-time-second-one').html(sec1);
            $('.timer-wrapper-time-second-two').html(sec2);
        }
    </script>

</body>
</html>
