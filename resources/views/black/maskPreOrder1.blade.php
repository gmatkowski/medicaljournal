@extends('layout.mask',[
    'title' => 'Bamboo Charcoal Black Mask',
    'meta_description' => 'Bamboo Charcoal Black Mask',
    'meta_keywords' => 'Bamboo Charcoal Black Mask'
])

@section('ico')
    <link rel="shortcut icon" href="{{ asset('build/images/frontend/blackMask2.ico') }}" type="image/x-icon">
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bmask2/all.css') }}">
@endsection

@section('analytics')
    @include('ga.blackmask')
@endsection

@section('trackConversions')
    @include('pixel.maskerhitam')
@endsection

@section('chat')
    <!-- Start of krebs Zendesk Widget script -->
    <script>/*<![CDATA[*/window.zEmbed||function(e,t){var n,o,d,i,s,a=[],r=document.createElement("iframe");window.zEmbed=function(){a.push(arguments)},window.zE=window.zE||window.zEmbed,r.src="javascript:false",r.title="",r.role="presentation",(r.frameElement||r).style.cssText="display: none",d=document.getElementsByTagName("script"),d=d[d.length-1],d.parentNode.insertBefore(r,d),i=r.contentWindow,s=i.document;try{o=s}catch(e){n=document.domain,r.src='javascript:var d=document.open();d.domain="'+n+'";void(0);',o=s}o.open()._l=function(){var o=this.createElement("script");n&&(this.domain=n),o.id="js-iframe-async",o.src=e,this.t=+new Date,this.zendeskHost=t,this.zEQueue=a,this.body.appendChild(o)},o.write('<body onload="document._l();">'),o.close()}("https://assets.zendesk.com/embeddable_framework/main.js","krebs.zendesk.com");
        /*]]>*/</script>
    <!-- End of krebs Zendesk Widget script -->
@endsection

@section('content')
    <div id="top-head">
        <div class="top_menu_wp">
            <div class="nav_wp">
                <nav class="fixed" id="top_menu">
                    <ul id="smoothMenu" class="clear">
                        <li>
                            <a class="hasevent" data-scroll="" href="#description" id="floating-event">Cara Kerja</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#how-use" id="floating-event">Cara Penggunaan</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#doctor" id="floating-event">Pendapat Seorang Ahli</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#pluses" id="floating-event">Manfaat</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#reviews" id="floating-event">Komentar</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#faq" id="floating-event">Pertanyaan Umum</a>
                        </li>
                        <li class="last">
                            <a class="hasevent active" data-scroll="" href="#footer" id="floating-event">Beli Sekarang</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div id="header">
        <div class="width">
            <div class="carrot">
                <div>Bamboo Charcoal Black Mask</div>
                Masker Penghilang Jerawat dan Komedo</div>
            <div class="content">
                <h1>Kulit <br>bermasalah?</h1>
                <h2>Cara Membersihkan Wajah yang Benar Menurut Para Ahli</h2>
                <div class="problems">
                    <span>JERAWAT</span>
                    <span>KOMEDO</span>
                    <span>FLEK HITAM</span>
                </div>
                <ul>
                    <li>Arang Bambu: berguna untuk membersihkan wajah dengan benar dan menyeluruh</li>
                    <li>Provitamin B5: sangat ampuh untuk mengatasi kulit bermasalah</li>
                    <li>Komposisinya alami, masker ini terbuat dari bahan-bahan yang mengandung vitamin-vitamin yang sangat baik untuk kulit wajah Anda (gandum, jeruk bali essential oil, kolagen)</li>
                </ul>
                <div class="sale">Diskon 50%</div>
                <div class="price">
                    <del>{{ $product2->price_old }} {{ $product2->currency }}</del>
                    <div>{{ $product2->price }} {{ $product2->currency }}</div>
                </div>
            </div>
        </div>
    </div>

    <div id="description">
        <div class="width">
            <div class="image">
                <div class="title">Cara Kerja
                    <br>
                    <div class="title-mask">Bamboo
                        <br>Charcoal Black
                    </div>
                </div>
                <p>Cara efektif untuk mengatasi kulit wajah yang bermasalah, sangat terkenal di seluruh dunia.<br>
                    <span>HASILNYA BISA DILIHAT SETELAH BEBERAPA KALI PEMAKAIAN.</span></p>
            </div>
            <div class="left-block">
                <div class="item">
                    <div>ARANG BAMBU</div>
                    Bahan aktif dan alami, dapat meningkatkan vitalitas kulit yang menurun, digunakan untuk membersihkan kulit wajah dari kotoran dan debu yang dapat menyumbat pori-pori penyebab timbulnya komedo. Arang Bambu merupakan bahan yang sangat efektif untuk pengobatan jerawat. Menembus pori-pori untuk membantu menjaga kulit tetap bersih dari noda jerawat baru.
                </div>

                <div class="item last">
                    <div>PROVITAMIN B5</div>
                    Memiliki sifat humektan, emolien dan moisturiser, punya khasiat menghilangkan kerutan wajah, melembutkan kulit, dan mencegah penguapan air keluar dari kulit wajah. Provitamin B5 membantu menyerap kotoran, membersihkan pori-pori, dan menenangkan kulit yang teriritasi.
                </div>
            </div>
            <div class="right-block">
                <div class="item">
                    <div>GANDUM</div>
                    Memiliki kandungan nutrisi tinggi yang dibutuhkan tubuh, gandum dapat mepercantik serta melembutkan kulit, gandu juga mengandung serat yang baik. Selain itu, kandungan saponin yang tedapat pada gandum memiliki efek emolien pada kulit, sangat bermanfaat untuk menjaga kelenturan julit wajah.
                </div>
                <div class="item last small">
                    <div>JERUK BALI ESSENTIAL OIL</div>
                    Sangat bermanfaat untuk menjaga produksi sebum, mengencangkan pori-pori wajah, meningkatkan kejelasan dan sirkulasi darah pada wajah, mencegah dan menghilangkan komedo dan flek hitam.
                </div>

            </div>
            <span></span>
            <div class="bottle">
                <img src="{{ asset('build/images/frontend/bottle.jpg') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div id="form-block">
        <div class="width">
            <div class="left-block">
                <div class="title">
                    <span>Beli sekarang juga</span><br>
                    <span>Bamboo Charcoal Black Mask</span><br>
                    <span>harga segera naik!</span>
                </div>
                <div class="sale">Diskon 50%</div>
                <div class="price">
                    <del>{{ $product2->price_old }} {{ $product2->currency }}</del>
                    <div>{{ $product2->price }} {{ $product2->currency }}</div>
                </div>
            </div>
            <div class="right-block">
                <div class="time">
                    <div class="name-t">Promo berlaku sampai:</div>
                    <div id="timer">
                        <div class="hours">
                            <p class="timer-wrapper-time-hour"></p>
                            <span>{{ trans('welcome.slider.promition.header.hours') }}</span>
                        </div>
                        <b class="dots">:</b>
                        <div class="mins">
                            <p class="timer-wrapper-time-minute"></p>
                            <span>{{ trans('welcome.slider.promition.header.minutes') }}</span>
                        </div>
                        <b class="dots">:</b>
                        <div class="secs">
                            <p class="timer-wrapper-time-second"></p>
                            <span>{{ trans('welcome.slider.promition.header.seconds') }}</span>
                        </div>
                    </div>
                </div>
                <a href="#footer" class="button green toform">Beli Sekarang</a>
            </div>
        </div>
    </div>

    <div id="how-use">
        <div class="width">
            <div class="title">Cara Penggunaan
                <br><div class="title-mask">Bamboo Charcoal Black Mask</div>
                <br><div>Masker Penghilang Jerawat dan Komedo</div>
            </div>
            <div class="text">HASILNYA AKAN TERLIHAT SETELAH PEMAKAIAN PERTAMA</div>
            <div class="clear"></div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/ico1.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <p>Langkah 1</p>
                <div>
                    Oleskan masker secara merata pada bagian wajah, pada daerah yang terdapat dermatitis, hindari daerah sekitar mata dan bibir karena rawan iritasi.
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/ico2.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <p>Langkah 2</p>
                <div>
                    Diamkan selama 25 menit, hingga masker menjadi kering dan elastis: zat-zat aktif akan meresap ke dalam kulit dan memperbaiki sel-sel kulit wajah Anda.
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/ico3.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <p>Langkah 3</p>
                <div>Hapus masker dengan gerakan tangan saja. Anda bisa bersihkan masker tadi dengan air hangat. Gunakan 3 kali seminggu.</div>
            </div>
            <div class="clear"></div>
            <a href="#footer" class="button green toform">Beli Sekarang</a>
        </div>
    </div>

    <div id="info-block">
        <div class="width">
            <div class="title white">Merawat Kulit Wajah
                <br>Dengan Benar</div>
            <span>Artikel dari Wikipedia.org:</span>
            <p>
                Kulit bermasalah adalah jenis-jenis kulit yang sulit diurus, yang berjerawat, banyak noda hitamnya, pori-pori besar, peka terhadap kosmetik, dll. Kulit bermasalah merupakan penyakit kulit yang cukup besar jumlah penderitanya. <br><br> Yang membuat masalah semakin rumit, bakteri biasanya ada di kulit, yang disebut p.acne, yang cenderung berkembang biak di dalam kelenjar sebaceous yang tersumbat, yang menghasilkan zat-zat yang menimbulkan iritasi daerah sekitarnya. Kelenjar tersebut terus membengkak, dan mungkin akan pecah, kemudian menyebarkan radang ke kulit daerah sekitarnya.
            </p>
        </div>
    </div>

    <div id="doctor">
        <div class="width">
            <div class="left-block">
                <div class="title">Pendapat Seorang Ahli<br><div class="title-mask">Bamboo Charcoal Black Mask</div></div>
                <div class="name">Teguh Suraswanto</div>

                <div class="name-b">(Seorang pakar kesehatan kulit, kosmetik desainer)</div>
                <br>
                <div class="status">Bamboo Charcoal Black Mask adalah jenis kosmetik yang merupakan produk berbahan alami, berkualitas premium dan sehat. Perusahaan kami menciptakan jenis kosmetik baru yang dibuat secara inovatif, Perawatan Kulit Wajah.</div>
                <div class="message">
                    Sebelum memasarkan produk-produk kosmetik kami diuji oleh para ahli dermatologi, dan terbukti secara klinis.
                    <br><br>
                    Sudah terbukti secara ilmiah: Bamboo Charcoal Black Mask sangat efektif untuk mengatasi jerawat dan kulit bermasalah.
                </div>
            </div>
            <div class="right-block">
                <div class="item item1">
                    <div>Masker Penghilang Komedo dan Jerawat Sebelum</div>
                    <img src="{{ asset('build/images/frontend/before.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </div>
                <div class="item item2">
                    <div>Masker Penghilang Komedo dan Jerawat Setelah</div>
                    <img src="{{ asset('build/images/frontend/after.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </div>
                <div class="clear"></div>
                <ul>
                    <span>Bamboo Charcoal Black Mask ini telah diuji pada 100 orang dan terbukti efektif:</span><br><br>
                    <li><span>91 orang dapat dilihat hasil positif segera setelah penggunaan pertama </span></li>
                    <li><span>84 orang memiliki kulit jauh lebih cerah dan bersih setelah 14 hari penggunaanya</span></li>
                    <li><span>93 pengguna Bamboo Charcoal Black Mask berhasil menghilangkan jerawat dan komedo dalam waktu pengobatan yang direkomendasikan (yaitu 1 bulan)</span></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div id="image"></div>

    <div id="pluses">
        <div class="width">
            <div class="title">Manfaat Utama
                <br><div class="title-mask">Bamboo Charcoal Black Mask</div></div>
            <div class="text">FORMULA AMPUH BAMBOO CHARCOAL BLACK MASK UNTUK MENGATASI JERAWAT DAN KOMEDO</div>
            <div class="clear"></div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop1.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    Komposisinya 100% Alami
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop2.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    Vitamin dan mineral 8 kali lebih kuat
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop3.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    Jaminan Kepuasan
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop4.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    Formulanya diperkaya dengan Pro-Vitamin B5
                    (solusi ampuh untuk mengatasi kulit bermasalah)
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop5.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    Efek maksimal dengan harga bagus
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop6.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    Tanpa efek samping, cara pemakaiannya sangat mudah!
                </div>
            </div>
            <div class="title-people">Ribuan pengguna di seluruh dunia berpuas hati dengan hasilnya!</div>
            <div class="people-img"></div>
        </div>
    </div>

    <div id="reviews">
        <div class="width">
            <div class="title white">Testimoni Pengguna<br>Bamboo Charcoal<br>Black Mask</div>

            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Carousel indicators -->
                {{--<ol class="carousel-indicators">--}}
                    {{--<li data-target="#myCarousel" data-slide-to="0" class="active"></li>--}}
                    {{--<li data-target="#myCarousel" data-slide-to="1"></li>--}}
                    {{--<li data-target="#myCarousel" data-slide-to="2"></li>--}}
                {{--</ol>--}}
                <!-- Wrapper for carousel items -->
                <div class="carousel-inner">
                    <div class="item active">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face01.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Shinta Wijayanto, 34 tahun, Medan</span>
                            <div>
                                Dalam 1 minggu jerawat hilang semua! Kulit saya jadi cerah, lebih bersih dan lebih muda, Black Mask membersihkan kulit wajah secara optimal dan cepat!
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face02.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Tika Widiawati, 21 tahun, Denpasar</span>
                            <div>
                                Beberapa temen pake Black Mask itu bagus sekali! Jerawat menghilang seperti sihir. Akhirnya aku bisa bernafas lega!
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face03.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Mai Hong, 29 tahun, Jakarta</span>
                            <div>
                                Aku senang dengan solusi ini! Dulu aku merasa malu, karena ada jerawat dan komedo di wajah saya, aku tak tahu melakukan apa...
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face04.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Hetty Hartati Novika, 31 tahun, Solo</span>
                            <div>
                                Jerawat, komedo dan flek hitam menghilang, cukup beberapa hari saja! Ku menjadi lebih sehat dan cantik, seperti orang muda.
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face05.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Erny Purwantono, 30 tahun, Bandung</span>
                            <div>
                                Pria dengan jerawat biasanya merasa terganggu kepercayaan dirinya. Sebelumnya ada jerawat dan komedo di dahi, di sekitar mata dan bibir. Black Mask itu solusi mengatasinya!
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face06.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Herlina Sari, 41 tahun, Balikpapan</span>
                            <div>
                                Tak malu lagi! Jauh lebih efektif daripada produk lain!!!
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face07.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Janti Soediro, 49 tahun</span>
                            <div>
                                Setelah 1 bulan akhirnya saya terlihat sebagai wanita cantik dan muda. Black Mask itu bagus sekali!
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face08.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>Dewi Suraswanto, 33 tahun</span>
                            <div>
                                Aku rekomendasikan terapi ini untuk semua wanita, karena akhirnya aku terlihat lebih muda, tanpa jerawat, tanpa komedo bahkan tanpa flek hitam.
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Carousel controls -->
                {{--<div class="flexslider carousel">--}}
                    {{--<ul class="flex-direction-nav">--}}
                        {{--<li>--}}
                            {{--<a class="flex-next" href="#myCarousel" data-slide="next">Next</a>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</div>--}}
                <!-- Carousel controls -->
                {{--<a class="carousel-control left" href="#myCarousel" data-slide="prev">--}}
                    {{--<span class="glyphicon glyphicon-chevron-left"></span>--}}
                {{--</a>--}}
                {{--<a class="carousel-control right" href="#myCarousel" data-slide="next">--}}
                    {{--<span class="glyphicon glyphicon-chevron-right"></span>--}}
                {{--</a>--}}
            </div>

        {{--<div class="flexslider carousel">--}}
                {{--<div style="overflow: hidden; position: relative;" class="flex-viewport"></div>--}}
                {{--<ol class="flex-control-nav flex-control-paging">--}}
                    {{--<li><a class="">1</a></li>--}}
                    {{--<li><a class="flex-active">2</a></li>--}}
                    {{--<li><a class="">3</a></li>--}}
                    {{--<li><a class="">4</a></li>--}}
                    {{--<li><a class="">5</a></li>--}}
                    {{--<li><a class="">6</a></li>--}}
                {{--</ol>--}}
                {{--<ul class="flex-direction-nav">--}}
                    {{--<li><a class="flex-prev" href="#">Previous</a></li>--}}
                    {{--<li><a class="flex-next" href="#">Next</a></li>--}}
                {{--</ul>--}}

                {{--<div class="flex-viewport" style="overflow: hidden; position: relative;">--}}
                    {{--<ul style="width: 2000%; transition-duration: 0.6s; transform: translate3d(-930px, 0px, 0px);" class="slides">--}}

                        {{--<li style="width: 465px; float: left; display: block;" class="clone">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava1.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Юлия Дорофеева 25 лет, Москва</span>--}}
                                {{--<div>--}}
                                    {{--Решила попробовать эту маску, заказала (очень быстро доставили),--}}
                                    {{--провела полный курс 1 месяц. Результат очень радует! Кожа стала намного--}}
                                    {{--чище и свежее, прямо сияет! Bamboo Charcoal Black Mask отлично помогает избавиться от--}}
                                    {{--угрей и черных точек, в общем мне понравилось, буду заказывать ещё!--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li><li style="width: 465px; float: left; display: block;" class="clone">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava6.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Наталья Осипова 20 лет, Москва</span>--}}
                                {{--<div>--}}
                                    {{--Никогда бы не поверила, что какая-то маска способна мне помочь!--}}
                                    {{--Но моя подруга, прибывавшая в восторге от этой маски убедила и меня--}}
                                    {{--заказать. Ну, что же курьер принес заказ (не ожидала, что так быстро), я--}}
                                    {{--оплатила и стала пробовать. Сначала ничего не заметила, но после недели--}}
                                    {{--прыщей стало меньше и кожа разгладилась. После месяца, моё лицо--}}
                                    {{--полностью изменилось, такой прекрасной кожи у меня никогда не было!--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}

                        {{--<li style="width: 465px; float: left; display: block;" class="flex-active-slide">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava1.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Tika Widiawati, 23 tahun, Denpasar</span>--}}
                                {{--<div>--}}
                                    {{--Ku sudah mencoba beberapa krim wajah merek terkenal, tapi itu gagal. Dan akhirnya... aku menemukan bantuan dan berhasil melewatinya, namanya formula Bamboo Charcoal Black Mask. Ketika Anda mulai menyeka formula itu di wajah, Anda bisa merasa, bahwa itu adalah sesuatu yang lain. Setelah 3 minggu menggunakannya keriput dan tanda-tanda penuaan menghilang. Saya berhasil, masih muda:) Ya... Aku bisa bernafas lega.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}

                        {{--<li class="" style="width: 465px; float: left; display: block;">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava2.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Павел Фролов 19 лет, Архангельск</span>--}}
                                {{--<div>--}}
                                    {{--У меня с детства проблемная кожа, очень быстро жирнится и--}}
                                    {{--вылезают угри и прыщи (даже рубцы появились). Эту маску мне посоветовала--}}
                                    {{--моя подруга. Попробовал, после первых 6-ти процедур кожа и правда--}}
                                    {{--изменилась, стала суше и чище, прыщи стали исчезать. После месячного--}}
                                    {{--интенсивного использования моё лицо изменилось до неузнаваемости, я--}}
                                    {{--очень доволен!--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="" style="width: 465px; float: left; display: block;">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava3.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Елена Лобзева 34 года, Воронеж</span>--}}
                                {{--<div>--}}
                                    {{--Очень хорошее средство для борьбы с акне и угрями! Проблемная--}}
                                    {{--кожа это - недуг, который преследует меня постоянно, что только не--}}
                                    {{--пробовала! Узнала о Черной маске и решила заказать просто ради интереса.--}}
                                    {{--Но после использования в течение 2-х недель я не поверила своим глазам,--}}
                                    {{--кожа стала намного лучше и красивее. Мне помогла эта маска, буду--}}
                                    {{--заказывать ещё.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="" style="width: 465px; float: left; display: block;">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava4.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Оксана Белова 27 лет, Сочи</span>--}}
                                {{--<div>--}}
                                    {{--Читала много отзывов о Bamboo Charcoal Black Mask на зарубежных сайтах, и вот--}}
                                    {{--решила попробовать эту маску. Заказала себе 2 флакона (для месячного--}}
                                    {{--курса) и почти через месяц не только избавилась от тех прыщей что были,--}}
                                    {{--но и новые не появились. А после нанесения кожа чистая, приятная и--}}
                                    {{--гладкая на ощупь. Если кто-то пишет, что маска плохая, то они--}}
                                    {{--пользовались дешевыми китайскими подделками.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="" style="width: 465px; float: left; display: block;">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava5.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Сергей Корнилов 24 года, Москва</span>--}}
                                {{--<div>--}}
                                    {{--Спасибо производителям этой чудесной маски, мне она очень--}}
                                    {{--помогла! Эффективное и мощное средство, которое действительно работает--}}
                                    {{--(проверено лично). Провел курс, очень рад результату, кожа обрела--}}
                                    {{--здоровый и привлекательный вид. Кстати, мне прислали заказ бесплатно.--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li class="" style="width: 465px; float: left; display: block;">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava6.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Наталья Осипова 20 лет, Москва</span>--}}
                                {{--<div>--}}
                                    {{--Никогда бы не поверила, что какая-то маска способна мне помочь!--}}
                                    {{--Но моя подруга, прибывавшая в восторге от этой маски убедила и меня--}}
                                    {{--заказать. Ну, что же курьер принес заказ (не ожидала, что так быстро), я--}}
                                    {{--оплатила и стала пробовать. Сначала ничего не заметила, но после недели--}}
                                    {{--прыщей стало меньше и кожа разгладилась. После месяца, моё лицо--}}
                                    {{--полностью изменилось, такой прекрасной кожи у меня никогда не было!--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}
                        {{--<li style="width: 465px; float: left; display: block;" class="clone">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava1.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Юлия Дорофеева 25 лет, Москва</span>--}}
                                {{--<div>--}}
                                    {{--Решила попробовать эту маску, заказала (очень быстро доставили),--}}
                                    {{--провела полный курс 1 месяц. Результат очень радует! Кожа стала намного--}}
                                    {{--чище и свежее, прямо сияет! Bamboo Charcoal Black Mask отлично помогает избавиться от--}}
                                    {{--угрей и черных точек, в общем мне понравилось, буду заказывать ещё!--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li><li style="width: 465px; float: left; display: block;" class="clone">--}}
                            {{--<div class="review">--}}
                                {{--<img src="{{ asset('build/images/frontend/ava6.png') }}" alt="Маска Bamboo Charcoal Black Mask отзывы" title="Маска Bamboo Charcoal Black Mask отзывы">--}}
                                {{--<span>Наталья Осипова 20 лет, Москва</span>--}}
                                {{--<div>--}}
                                    {{--Никогда бы не поверила, что какая-то маска способна мне помочь!--}}
                                    {{--Но моя подруга, прибывавшая в восторге от этой маски убедила и меня--}}
                                    {{--заказать. Ну, что же курьер принес заказ (не ожидала, что так быстро), я--}}
                                    {{--оплатила и стала пробовать. Сначала ничего не заметила, но после недели--}}
                                    {{--прыщей стало меньше и кожа разгладилась. После месяца, моё лицо--}}
                                    {{--полностью изменилось, такой прекрасной кожи у меня никогда не было!--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</li>--}}

                    {{--</ul>--}}
                {{--</div>--}}

                {{--<ol class="flex-control-nav flex-control-paging">--}}
                    {{--<li><a class="">1</a></li>--}}
                    {{--<li><a class="">2</a></li>--}}
                    {{--<li><a>3</a></li>--}}
                    {{--<li><a>4</a></li>--}}
                    {{--<li><a>5</a></li>--}}
                    {{--<li><a>6</a></li>--}}
                    {{--<li><a>7</a></li>--}}
                    {{--<li><a>8</a></li>--}}
                {{--</ol>--}}
                {{--<ul class="flex-direction-nav">--}}
                    {{--<li><a class="flex-prev" href="#">Previous</a></li>--}}
                    {{--<li><a class="flex-next" href="#">Next</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}
        </div>
    </div>

    <div id="footer">
        <div class="width">
            <div class="title">Promo hanya hari ini
                <br>pesan sekarang dengan
                <br>diskon 50%,
            </div>
            <div class="price">
                <del>{{ $product2->price_old }} {{ $product2->currency }}</del>
                <div>{{ $product2->price }} {{ $product2->currency }}</div>
            </div>
            <div class="time">
                <div class="name-t">Promo berlaku sampai:</div>
                <div id="timer2">
                    <div class="hours">
                        <p class="timer-wrapper-time-hour"></p>
                        <span>{{ trans('welcome.slider.promition.header.hours') }}</span>
                    </div>
                    <b class="dots">:</b>
                    <div class="mins">
                        <p class="timer-wrapper-time-minute"></p>
                        <span>{{ trans('welcome.slider.promition.header.minutes') }}</span>
                    </div>
                    <b class="dots">:</b>
                    <div class="secs">
                        <p class="timer-wrapper-time-second"></p>
                        <span>{{ trans('welcome.slider.promition.header.seconds') }}</span>
                    </div>
                </div>
            </div>

            @include('black.order_form', ['form' => $form, 'submitContent' => 'BELI SEKARANG'])
        </div>
    </div>

    <!-- WhatsApp Garcinia Block Start -->
    <div id="wa-wrapper-grey-mobile">
        <div id="mobilewhatsapp">
            <a href="whatsapp://send?text=whatsapp">
                <img src="{{ asset('build/images/frontend/wa-icon.png') }}" alt="">
                <p>Order by WhatsApp</p>
                <p class="green">{{ Config::get('whatsapp.number') }}</p>
            </a>
            <a href="intent://send/{{ str_replace(")", "", str_replace("(", "", str_replace(" ", "", Config::get('whatsapp.number')))) }}#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end">Open WhatsApp chat window</a>
        </div>
    </div>
    <!-- WhatsApp Garcinia Block End -->

    <div id="work">
        <div class="width">
            <div class="title">Bagaimana Cara Pesannya:</div>
            {{--<div class="text"><b>Экспресс-доставка</b> по СНГ</div>--}}
            <div class="clear"></div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-01.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>Klik pada tombol „Beli Sekarang”</div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-02.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>Isi formulir online di halaman berikutnya</div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-03.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>Kami akan menghubungi dengan Anda melalui telepon untuk mengkonfirmasikan</div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-04.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>Anda mendapatkan pengiriman Anda dan bayar di tempat (COD)</div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div id="faq">
        <div class="width">
            <div class="title">Pertanyaan Umum</div>
            <div class="clear"></div>

            <div class="faq-top">

                <div class="spoiler_links redi">Sebagai tanggapan atas permintaan para konsumen. Komposisi Bamboo Charcoal Black Mask</div>
                <div style="display: none;" class="spoiler_body">Sekali lagi, sebagai tanggapan atas permintaan para pembaca, kami mengeluarkan gambar produk asli beserta deskripsinya.<br><br>

                    <div class="in-dev">Komposisi</div><br>
                    Bahan-bahan alami: Aqua (1), Polivinil Alkohol (2), Gliserin (3), Propilen Glikol (4), Minyak Gandum (5), Squalane (6), Panthenol (7), Kolagen (8) Arang Bambu (9), Minyak Jeruk Bali (10)<br><br>
                    <ul>
                        <li>Aqua - air</li>
                        <li>
                            Polivinil alkohol - digunakan sebagai dasar, untuk membersihkan kulit wajah dari kotoran, debu dan bakteri. Fungsi dan manfaat (angka dari 1 sampai 10): fungsi melindungi 9, fungsi membersihkan. Cocok untuk segala jenis kulit. Tanpa efek samping apa pun.
                        </li>
                        <li>
                            Gliserin - adalah produk alami yang berasal dari tumbuh-tumbuhan. Membantu menyerap bahan-bahan bergizi ke dalam kulit. Gliserin merupakan emolien dan stabilisator alami yang memberikan efek dingin pada kulit dan sangat bermanfaat bila digunakan dengan bahan lainnya. Tanpa efek samping apapun. Tidak ada reaksi alergi apapun, tidak ada efek samping. Cocok untuk segala jenis kulit, termasuk kulit yang berminyak dan sensitif.
                        </li>
                        <li>
                            Propilen glikol - fungsi dan manfaat (angka dari 1 sampai 10): memiliki sifat moisturiser 10. Cocok untuk segala jenis kulit. Tingkat bahaya: 3 (sangat rendah). Sering digunakan dalam produk kosmetik. Tidak menumpuk didalam tubuh, sehingga sering juga digunakan dalam produk makanan. Tanpa efek samping apa pun.
                        </li>
                        <li>
                            Minyak gandum - bahan alami. Mampu menutrisi kulit secara alami serta menyamarkan noda dengan cepat, gandum dapat mempercantik serta melembutkan kulit. Gandum juga mengandung serat yang baik, efek emolien pada kulit, sangat bermanfaat untuk menjaga kelenturan kulit wajah, melancarkan sirkulasi darah, membuat wajah lebih terlihat mulus, lebih cerah dan muda. Bahan-bahan alami: Omega-3, Vitamin E, tokokoferoly, karotenoid, beta-karoten, lesitin, metionin, squalene, pitosterol, vitamin, vitamin A, D, PP, asam pantotenat, asam folat, seng, besi, kalium, sulfur, fosfor, protein, allantoin.
                        </li>
                        <li>
                            Squalane - bahan alami dari minyak zaitun. Merupakan sumber asam lemak omega 3 yang sangat baik, membantu kulit menjaga kelembabannya secara optimal, mengatasi masalah penuaan dini, membantu zat-zat aktit untuk diresap ke dalam kulit dan regenerasi sel-sel kulit wajah. Cocok untuk segala jenis kulit, termasuk kulit yang berminyak dan sensitif.
                        </li>

                        <li>
                            Panthenol - merupakan Pro-vitamin B5, memiliki sifat humektan, emolien dan moisturiser. Banyak digunakan dalam produk kosmetik, untuk merawat kulit dan rambut,  direkomendasikan untuk anak-anak, melembutkan kulit dan mencegah penguapan air keluar dari kulit wajah. Tanpa efek samping apapun.
                        </li>
                        <li>
                            Kolagen - digunakan sebagai dasar, membantu untuk menyerap bahan-bahan bergizi ke dalam kulit. Cocok untuk segala jenis kulit, termasuk kulit yang berminyak dan sensitif. Bahan aman dan alami, tidak menimbulkan efek samping apapun.
                        </li>
                        <li>
                            Arang Bambu - bahan alami, digunakan untuk membersihkan kulit wajah dari kotoran dan debu yang dapat menyumbat pori-pori penyebab timbulnya komedo. Arang Bambu merupakan bahan yang sangat efektif untuk pengobatan jerawat. Bekerja dengan menembus pori-pori untuk membantu menjaga kulit tetap bersih dari noda jerawat baru. Cocok untuk segala jenis kulit, termasuk kulit yang berminyak dan sensitif. Bahan aman dan alami tanpa efek samping apapun.
                        </li>
                        <li>
                            Minyak Jeruk Bali - berkhasiat membuat kulit tampak lebih cerah, membantu regenerasi kulit, agar anda mendapatkan kulit wajah yang bersih dan bercahaya, sangat bermanfaat untuk menjaga produksi sebum, mengencangkan pori-pori wajah, meningkatkan kecerahan dan sirkulasi darah pada wajah, mencegah dan menghilangkan komedo dan flek hitam.
                        </li>
                        <li>
                            Total berat paket: 60 g
                        </li>
                    </ul>
                    <br>
                    <div class="in-dev">Penyimpanan dan Penggunaan</div><br>
                    Simpan di tempat yang gelap, pada suhu ruangan (15° C  hingga 25°C). Setelah pemakaian, tutup rapat dan simpan di tempat yang tidak terlihat dan tidak terjangkau oleh anak-anak. Tanpa efek samping, cara pemakaiannya sangat mudah.

                </div>

                <div class="spoiler_links">Bolehkah saya menggunakan kosmetik atau krim wajah yang lain, selama penggunaan Bamboo Charcoal Black Mask?</div>
                <div style="display: none;" class="spoiler_body">
                    Ya. Anda dapat menggunakan kosmetik, krim wajah dll. selama penggunaan Bamboo Charcoal Black Mask. Sebelum memakai masker jangan lupa membersihkan kulit muka terlebih dahulu.
                </div>

                <div class="spoiler_links">Hasilnya bisa dilihat setelah berapa kali pemakaian?</div>
                <div style="display: none;" class="spoiler_body">
                    Hasilnya akan terlihat setelah pemakaian pertama, Bamboo Charcoal Black Mask akan membuat kulit wajah Anda menjadi lebih sehat, kencang, cerah dan awet muda. Kami menganjurkan Anda menggunakan masker secara teratur, untuk pencegahan.
                </div>

                <div class="spoiler_links">Apakah ada reaksi alergi? Apakah ada efek samping?</div>
                <div style="display: none;" class="spoiler_body">
                    Kami menjamin 99% bahwa produk alami ini tidak menimbulkan efek samping apapun dan tidak ada reaksi alergi apapun. Namun, sebelum pemakaian, letakkan sampel produk pada pergelangan tangan bagian dalam dan tunggulah 20 menit untuk melihat apakah reaksi terjadi.
                </div>

                <div class="spoiler_links">Apakah masker ini cocok untuk segala jenis kulit?</div>
                <div style="display: block;" class="spoiler_body">
                    Ya, masker ini cocok untuk segala jenis kulit, termasuk kulit yang berminyak dan sensitif.
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('build/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('build/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script src="{{ elixir('js/bmask2/all.js') }}"></script>

    <script>
        $(function() {
            $('.carousel').carousel();

            $('.spoiler_links').click(function() {
                if ($(this).next(
                                '.spoiler_body').css(
                                "display") == "none") {
                    $('.spoiler_body').hide(
                            'normal');
                    $(this).next('.spoiler_body').toggle(
                            'fast');
                } else $('.spoiler_body').hide(
                        'fast');
                return false;
            });
        });
    </script>

    <script>
        $(function() {
            $(window).scroll(function() {
                if ($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });
            $('#toTop').click(function() {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
            });
        });
    </script>

    {{--<script type="text/javascript">--}}
    {{--$(function(){--}}
    {{--M1.initComebacker(3000);--}}
    {{--var M1Text = {--}}
    {{--'validation_name': 'Укажите корректные ФИО.',--}}
    {{--'validation_phone1': 'Номер телефона может содержать только цифры, символы "+", "-", "(", ")" и пробелы.',--}}
    {{--'validation_phone2': 'В вашем телефоне слишком мало цифр.',--}}
    {{--'comebacker_text': 'ВНИМАНИЕ'--}}
    {{--};--}}
    {{--M1.validateAndSendForm(false, M1Text);--}}
    {{--});--}}
    {{--</script>--}}

    {{--<script>--}}
        {{--var vPlayer = null;--}}
        {{--$(function() {--}}
            {{--var product = 1327,--}}
                    {{--url = location.href,--}}
                    {{--length = 0,--}}
                    {{--keyVal = '',--}}
                    {{--arFio = [],--}}
                    {{--arPhone = [];--}}

            {{--$('input[name=\'phone\']').bind('keyup change', function(){--}}
                {{--var form = $(this).parents('form'),--}}
                        {{--name = form.find('input[name=\'name\']').val(),--}}
                        {{--phone;--}}

                {{--phone = $(this).val().replace(/\D+/g,'');--}}
                {{--if(phone.length >= 8){--}}
                    {{--getFormData();--}}
                    {{--$.ajax({--}}
                        {{--type: 'POST',--}}
                        {{--url: 'http://m1-shop.ru/send_stat_order/',--}}
                        {{--data: {--}}
                            {{--name: name,--}}
                            {{--phone: phone,--}}
                            {{--name_json: JSON.stringify(arFio),--}}
                            {{--phone_json: JSON.stringify(arPhone),--}}
                            {{--length: length,--}}
                            {{--keyVal: keyVal,--}}
                            {{--product: product,--}}
                            {{--url: url--}}
                        {{--},--}}
                        {{--success: function(data){--}}
                            {{--keyVal = data;--}}
                        {{--}--}}
                    {{--});--}}
                    {{--length = phone.length;--}}
                {{--}--}}
            {{--});--}}

            {{--$('form').submit(function(){--}}
                {{--if(keyVal.length > 0){--}}
                    {{--$.ajax({--}}
                        {{--type: 'POST',--}}
                        {{--url: 'http://m1-shop.ru/send_stat_order/',--}}
                        {{--data: {--}}
                            {{--del: 1,--}}
                            {{--keyVal: keyVal--}}
                        {{--}--}}
                    {{--});--}}
                {{--}--}}
            {{--});--}}

            {{--function getFormData() {--}}
                {{--arFio = [];--}}
                {{--arPhone = [];--}}
                {{--$('form').each(function(){--}}
                    {{--var phone = $(this).find('input[name=\'phone\']').val();--}}
                    {{--var fio = $(this).find('input[name=\'name\']').val();--}}
                    {{--phone = phone.replace(/\D+/g,'');--}}
                    {{--if (phone.length >= 8){--}}
                        {{--arPhone.push(phone.toString());--}}
                        {{--if(typeof fio != 'undefined')--}}
                            {{--arFio.push(fio.toString());--}}
                    {{--}--}}
                {{--});--}}
            {{--}--}}
        {{--});--}}
    {{--</script>--}}

    <script>
        String.prototype.toHHMMSS = function () {
            var sec_num = parseInt(this, 10),
                    hours = Math.floor(sec_num / 3600),
                    minutes = Math.floor((sec_num - (hours * 3600)) / 60),
                    seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            return hours + ' ' + minutes + ' ' + seconds;
        };

        var start = moment({hour: 0, minute: 0, seconds: 0, milliseconds: 0}).unix();
        var now = moment().unix();
        var count = String((86400 - (now - start)));

        var counter = setInterval(timer, 1000);

        function timer() {
            if (parseInt(count) <= 0) {
                clearInterval(counter);
                $('#promotion-holder').remove(0);
                return;
            }
            var temp = count.toHHMMSS();
            var tempArr = temp.split(' ');
            var hour = tempArr[0];
            var min = tempArr[1];
            var sec = tempArr[2];

            count = (parseInt(count) - 1).toString();
            $('.timer-wrapper-time-hour').html(hour);
            $('.timer-wrapper-time-minute').html(min);
            $('.timer-wrapper-time-second').html(sec);
        }
    </script>
@endsection
