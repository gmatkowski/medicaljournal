@extends('layout.mask',[
    'title' => 'มาส์กถ่านต้นไผ่',
    'meta_description' => 'มาส์กถ่านต้นไผ่',
    'meta_keywords' => 'มาส์กถ่านต้นไผ่'
])

@section('ico')
    <link rel="shortcut icon" href="{{ asset('build/images/frontend/blackMask2.ico') }}" type="image/x-icon">
@endsection

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ elixir('css/bmask2/all.css') }}">
@endsection

@section('content')
    <div id="top-head">
        <div class="top_menu_wp">
            <div class="nav_wp">
                <nav class="fixed" id="top_menu">
                    <ul id="smoothMenu" class="clear">
                        <li>
                            <a class="hasevent" data-scroll="" href="#description" id="floating-event">วิธีการทำงาน</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#how-use" id="floating-event">วิธีใช้</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#doctor" id="floating-event">ความเห็นผู้เชี่ยวชาญ</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#pluses" id="floating-event">ประโยชน์</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#reviews" id="floating-event">ความเห็น</a>
                        </li>
                        <li>
                            <a class="hasevent" data-scroll="" href="#faq" id="floating-event">ความเห็นที่พบบ่อย</a>
                        </li>
                        <li class="last">
                            <a class="hasevent active" data-scroll="" href="#footer" id="floating-event">ซื้อตอนนี้</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div id="header">
        <div class="width">
            <div class="carrot">
                <div>มาส์กถ่านต้นไผ่</div>
                มาส์กสิวและสิวเสี้ยน</div>
            <div class="content">
                <h1>คุณมีผิวที่มีปัญหาใช่ไหม?</h1>
                <h2>วิธีการทำความสะอาดใบหน้าตามคำแนะนำของผู้เชี่ยวชาญ</h2>
                <div class="problems">
                    <span>สิวและสิวเสี้ยน</span>
                </div>
                <ul>
                    <li>ถ่านต้นไผ่: ช่วยทำความสะอาดผิวอย่างล้ำลึก</li>
                    <li>โปรวิตามินบี 5: ประสิทธิภาพสูงในการแก้ไขปัญหาผิว</li>
                    <li>อุดมไปด้วยส่วนผสมจากธรรมชาติ มาส์กตัวนี้ผลิตจากส่วนผสมวิตามินต่างๆที่มีประโยชน์ต่อผิวหน้า (ธัญพืช ส้มบาหลี น้ำมันหอมระเหย คอลลาเจน)</li>
                </ul>
                <div class="sale">ส่วนลด 50%</div>
                <div class="price">
                    <del>{{ $product2->price_old }} {{ $product2->currency }}</del>
                    <div>{{ $product2->price }} {{ $product2->currency }}</div>
                </div>
            </div>
        </div>
    </div>

    <div id="description">
        <div class="width">
            <div class="image">
                <div class="title">วิธีการทำงาน
                    <br>
                    <div class="title-mask">ถ่านต้นไผ่</div>
                </div>
                <p>วิธีการที่มีประสิทธิภาพในการแก้ไขปัญหาผิวของคุณ ได้รับความนิยมทั่วโลก<br>
                    <span>เห็นผลลัพธ์ได้ทันทีหลังการใช้ถ่านต้นไผ่สองสามครั้ง</span></p>
            </div>
            <div class="left-block">
                <div class="item">
                    ส่วนประกอบจากธรรมชาติที่สำคัญช่วยทำความสะอาดผิวจากสิ่งสกปรกและสิ่งตกค้างที่อุดตันรูขุมขนที่ก่อให้เกิดสิวเสี้ยน ถ่านต้นไผ่มีประสิทธิภาพในการรักษาสิว ลดสิวและทำให้ผิวสะอาด
                </div>

                <div class="item last">
                    <div>โปรวิตามินบี 5</div>
                    มีส่วนช่วยในการเพิ่มความชุ่มชื่นและความยืนหยุ่นให้แก่ผิว อีกทั้งยังลดริ้วรอยบนใบหน้า ทำให้ผิวนุ่มขึ้นและป้องกันน้ำระเหยออกจากผิว โปรวิตามินบี 5 ยังช่วยดูดซึมสิ่งสกปรก ทำความสะอาดรูขุมขนและลดผิวที่อักเสบและระคายเคือง
                </div>
            </div>
            <div class="right-block">
                <div class="item">
                    <div>ธัญพืช</div>
                    อุดมไปด้วยสารอาหารที่ร่างกายต้องการ ธัญพืชสามารถช่วยให้ผิวนุ่มและสวยขึ้นและยังมีเส้นใย นอจกานี้ซานินในธัญพืชช่วยรักษาความยืดหยุ่นของใบหน้า
                </div>
                <div class="item last small">
                    <div>น้ำมันหอมระเหยส้มบาหลี</div>
                    มีประโยชน์ในการรักษาการผลิตน้ำมัน กระชับรูขุมขนใบหน้า เพิ่มการไหลเวียนของเลือด ป้องกันและรักษาสิวและสิวเสี้ยน
                </div>

            </div>
            <span></span>
            <div class="bottle">
                <img src="{{ asset('build/images/frontend/bottle.png') }}" width="300px" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div id="form-block">
        <div class="width">
            <div class="left-block">
                <div class="title">
                    <span>ซื้อตอนนี้</span><br>
                    <span>ราคามาส์กต้นไผ่จะขึ้นในเร็วๆนี้!</span>
                </div>
                <div class="sale">ลด 50%</div>
                <div class="price">
                    <del>{{ $product2->price_old }} {{ $product2->currency }}</del>
                    <div>{{ $product2->price }} {{ $product2->currency }}</div>
                </div>
            </div>
            <div class="right-block">
                <div class="time">
                    <div class="name-t">ข้อเสนอสิ้นสุดใน:</div>
                    <div id="timer">
                        <div class="hours">
                            <p class="timer-wrapper-time-hour"></p>
                            <span>ชั่วโมง</span>
                        </div>
                        <b class="dots">:</b>
                        <div class="mins">
                            <p class="timer-wrapper-time-minute"></p>
                            <span>นาที</span>
                        </div>
                        <b class="dots">:</b>
                        <div class="secs">
                            <p class="timer-wrapper-time-second"></p>
                            <span>วินาที</span>
                        </div>
                    </div>
                </div>
                <a href="#footer" class="button green toform">ซื้อตอนี้</a>
            </div>
        </div>
    </div>

    <div id="how-use">
        <div class="width">
            <div class="title">วิธีการใช้
                <br><div class="title-mask">มาส์กถ่านต้นไผ่</div>
                <br><div>มาส์กรักษาสิวและสิวเสี้ยน</div>
            </div>
            <div class="text">เห็นผลลัพธ์ตั้งแต่ครั้งแรกที่ใช้</div>
            <div class="clear"></div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/ico1.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <p>ขั้นตอนที่ 1</p>
                <div>
                    ทามาส์กให้ทั่วใบหน้า โดยเฉพาะบริเวณที่มีปัญหาผิว หลีกเลี่ยงบริเวณรอบด้วงตาและริมฝีปากเพื่อป้องกันการระคายเคือง
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/ico2.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <p>ขั้นตอนที่ 2</p>
                <div>
                    มาส์กทิ้งไว้ 25 นาที จนกว่ามาส์กจะแห่งและยืดหยุ่น สารต่างๆจะซึมเข้าสู่ผิวและซ่อมแซมเซลล์ผิวหน้าของคุณ
                </div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/ico3.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <p>ขั้นตอนที่ 3</p>
                <div>ลอกมาส์กออก คุณสามารถใช้น้ำอุ่นทำความสะอาดมาส์กได้เช่นกัน ใช้ 3 ครั้งต่อสัปดาห์</div>
            </div>
            <div class="clear"></div>
            <a href="#footer" class="button green toform">ซื้อตอนี้</a>
        </div>
    </div>

    <div id="info-block">
        <div class="width">
            <div class="title white">รักษาผิวหน้าของคุณอย่างถูกต้อง</div>
            <span>บทความจาก Wikipedia.org:</span>
            <p>
                ผิวที่มีปัญหาเป็นผิวที่รักษาได้ยาก มีทั้งสิว สิวเสี้ยน รูขุมขนกว้างและแพ้เครื่องสำอางง่าย และผู้คนส่วนใหญ่ทั่วโลกมีปัญหาผิว
                <br><br>
                สิ่งที่ทำให้ปัญหาแย่ลงคือแบคทีเรียที่อยู่บนผิวมีชื่อว่า P.Acne ซึ่งจะเข้าไปอุดตันต่อมน้ำมัน ก่อให้เกิดการระคายเคืองต่อเนื้อเยื่อ เมื่อต่อมน้ำมันบวมขั้น ทำให้เกิดสิวอักเสบ
            </p>
        </div>
    </div>

    <div id="doctor">
        <div class="width">
            <div class="left-block">
                <div class="title">ความเห็นผู้เชี่ยวชาญ<br><div class="title-mask">มาส์กถ่านต้นไผ่</div></div>
                <div class="name">เตกูห์ สุรัสวันโต</div>

                <div class="name-b">(ผู้เชี่ยวชาญด้านสุขภาพผิว, เครื่องสำอาง)</div>
                <br>
                <div class="status">มาส์กถ่านต้นไผ่ เป็นเครื่องสำอางจากส่วนผสมธรรมชาติ คุณภาพสูงและดีต่อสุขภาพ บริษัทของเราผลิตเครื่องสำอางนวัตกรรมในการดูแลผิวหน้า</div>
                <div class="message">
                    ก่อนการเปิดตัวผลิตภัณฑ์ของเรา ผลิตภัณฑ์ผ่านการทดสอบโดยแพทย์ผิวหนังและพิสูจน์ทางคลินิก
                    <br><br>
                    ได้รับการพิสูจน์ทางวิทยาศาสตร์: มาส์กถ่านต้นไผ่มีประสิทธิภาพในการรักษาสิวและปัญหาผิว
                </div>
            </div>
            <div class="right-block">
                <div class="item item1">
                    <div>ก่อนที่มาส์กรักษาสิวและสิวเสี้ยน</div>
                    <img src="{{ asset('build/images/frontend/before.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </div>
                <div class="item item2">
                    <div>หลังจากที่มาส์กรักษาสิวและสิวเสี้ยน</div>
                    <img src="{{ asset('build/images/frontend/after.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </div>
                <div class="clear"></div>
                <ul>
                    <span>มาส์กถ่านต้นไผ่ได้รับการทดสอบแล้วกว่า100 รายและได้รับการพิสูจน์ผลลัพธ์ที่มีประสิทธิภาพ:</span><br><br>
                    <li><span>ใน 91 ราย เราเห็นผลลัพธ์เชิงบวกในครั้งแรกที่ใช้</span></li>
                    <li><span>ใน 84 รายมีผิวที่ดีและสะอาดขึ้นหลังใช้ 14 วัน</span></li>
                    <li><span>ผู้ใช้มาส์กถ่านต้นไผ่ 93 ราย สามารถกำจัดสิวและสิวเสี้ยนภายในเวลาที่แนะนำ (1 เดือน)</span></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div id="image"></div>

    <div id="pluses">
        <div class="width">
            <div class="title">ประโยชน์หลัก
                <br><div class="title-mask">มาส์กถ่านต้นไผ่</div></div>
            <div class="text">สูตรมหัศจรรย์จากมาส์กถ่านต้นไผ่ในการกำจัดสิวและสิวเสี้ยน</div>
            <div class="clear"></div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop1.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    ส่วนผสมจากธรรมชาติ 100%
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop2.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    วิตามินและแร่ธาตุเข้มข้นกว่าถึง 8 เท่า
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop3.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    รับประกันความพึงพอใจ
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop4.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    สูตรนี้อุดมไปด้วยโปรวิตามินบี 5 (ช่วยแก้ไขปัญหาผิวอย่างตรงจุด)
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop5.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    ผลลัพธ์สูงสุดในราคาน่ารัก
                </div>
            </div>
            <div class="item">
                <p><span></span><img src="{{ asset('build/images/frontend/icop6.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                </p>
                <div>
                    ไม่มีผลข้างเคียง ใช้ได้ง่าย
                </div>
            </div>
            <div class="title-people">มีผู้ใช้หลายหมื่นคนทั่วโลกที่พึงพอใจกับผลลัพธ์!</div>
            <div class="people-img"></div>
        </div>
    </div>

    <div id="reviews">
        <div class="width">
            <div class="title white">คำยืนยันจากผู้ใช้มาส์กถ่านต้นไผ่</div>

            <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    {{--<div class="item active">--}}
                        {{--<div class="review">--}}
                            {{--<img src="{{ asset('build/images/frontend/face01.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">--}}
                            {{--<span>Shinta Wijayanto, 34 tahun, Medan</span>--}}
                            {{--<div>--}}
                                {{--Dalam 1 minggu jerawat hilang semua! Kulit saya jadi cerah, lebih bersih dan lebih muda, Black Mask membersihkan kulit wajah secara optimal dan cepat!--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="item active">
                        <div class="review">
                            <img src="{{ asset('build/images/frontend/face02.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                            <span>ตีก้า วิเดียวาตี , อายุ 21 ปี, เดนปาซาร์</span>
                            <div>
                                เพื่อนของฉันบางคนใช้มาส์กตัวนี้และได้ผลดีมาก! สิวหายไปอย่างเหลือเชื่อ ในที่สุดฉันสบายใจอีกครั้ง!
                            </div>
                        </div>
                    </div>
                    {{--<div class="item">--}}
                        {{--<div class="review">--}}
                            {{--<img src="{{ asset('build/images/frontend/face03.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">--}}
                            {{--<span>Mai Hong, 29 tahun, Jakarta</span>--}}
                            {{--<div>--}}
                                {{--Aku senang dengan solusi ini! Dulu aku merasa malu, karena ada jerawat dan komedo di wajah saya, aku tak tahu melakukan apa...--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                        {{--<div class="review">--}}
                            {{--<img src="{{ asset('build/images/frontend/face04.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">--}}
                            {{--<span>Hetty Hartati Novika, 31 tahun, Solo</span>--}}
                            {{--<div>--}}
                                {{--Jerawat, komedo dan flek hitam menghilang, cukup beberapa hari saja! Ku menjadi lebih sehat dan cantik, seperti orang muda.--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                        {{--<div class="review">--}}
                            {{--<img src="{{ asset('build/images/frontend/face05.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">--}}
                            {{--<span>Erny Purwantono, 30 tahun, Bandung</span>--}}
                            {{--<div>--}}
                                {{--Pria dengan jerawat biasanya merasa terganggu kepercayaan dirinya. Sebelumnya ada jerawat dan komedo di dahi, di sekitar mata dan bibir. Black Mask itu solusi mengatasinya!--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                        {{--<div class="review">--}}
                            {{--<img src="{{ asset('build/images/frontend/face06.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">--}}
                            {{--<span>Herlina Sari, 41 tahun, Balikpapan</span>--}}
                            {{--<div>--}}
                                {{--Tak malu lagi! Jauh lebih efektif daripada produk lain!!!--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                        {{--<div class="review">--}}
                            {{--<img src="{{ asset('build/images/frontend/face07.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">--}}
                            {{--<span>Janti Soediro, 49 tahun</span>--}}
                            {{--<div>--}}
                                {{--Setelah 1 bulan akhirnya saya terlihat sebagai wanita cantik dan muda. Black Mask itu bagus sekali!--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="item">--}}
                        {{--<div class="review">--}}
                            {{--<img src="{{ asset('build/images/frontend/face08.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">--}}
                            {{--<span>Dewi Suraswanto, 33 tahun</span>--}}
                            {{--<div>--}}
                                {{--Aku rekomendasikan terapi ini untuk semua wanita, karena akhirnya aku terlihat lebih muda, tanpa jerawat, tanpa komedo bahkan tanpa flek hitam.--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>
            </div>
        </div>
    </div>

    <div id="footer">
        <div class="width">
            <div class="title">วันนี้เท่านั้น!
                <br>ข้อเสนอส่วนลด
            </div>
            <div class="price">
                <del>{{ $product2->price_old }} {{ $product2->currency }}</del>
                <div>{{ $product2->price }} {{ $product2->currency }}</div>
            </div>
            <div class="time">
                <div class="name-t">ข้อเสนอสิ้นสุดใน:</div>
                <div id="timer2">
                    <div class="hours">
                        <p class="timer-wrapper-time-hour"></p>
                        <span>ชั่วโมง</span>
                    </div>
                    <b class="dots">:</b>
                    <div class="mins">
                        <p class="timer-wrapper-time-minute"></p>
                        <span>นาที</span>
                    </div>
                    <b class="dots">:</b>
                    <div class="secs">
                        <p class="timer-wrapper-time-second"></p>
                        <span>วินาที</span>
                    </div>
                </div>
            </div>

            @include('black.order_form', ['form' => $form, 'submitContent' => 'ซื้อตอนี้'])
        </div>
    </div>

    <!-- WhatsApp Garcinia Block Start -->
    <div id="wa-wrapper-grey-mobile">
        <div id="mobilewhatsapp">
            <a href="whatsapp://send?text=whatsapp">
                <img src="{{ asset('build/images/frontend/wa-icon.png') }}" alt="">
                <p>สั่งซื้อทาง WhatsApp</p>
                <p class="green">{{ Config::get('whatsapp.number') }}</p>
            </a>
            <a href="intent://send/{{ str_replace(")", "", str_replace("(", "", str_replace(" ", "", Config::get('whatsapp.number')))) }}#Intent;scheme=smsto;package=com.whatsapp;action=android.intent.action.SENDTO;end">Open WhatsApp chat window</a>
        </div>
    </div>
    <!-- WhatsApp Garcinia Block End -->

    <div id="work">
        <div class="width">
            <div class="title">วิธีการสั่งซื้อ:</div>
            {{--<div class="text"><b>Экспресс-доставка</b> по СНГ</div>--}}
            <div class="clear"></div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-01.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>คลิกปุ่ม “ซื้อตอนนี้”</div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-02.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>กรอกแบบฟอร์มออนไลน์ในหน้าถัดไป</div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-03.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>เราจะติดต่อคุณทางโทรศัพท์เพื่อยืนยัน</div>
            </div>
            <div class="item">
                <img src="{{ asset('build/images/frontend/zakaz-04.png') }}" alt="Masker Penghilang Jerawat dan Komedo" title="Masker Penghilang Jerawat dan Komedo">
                <div>คุณจะได้รับการจัดส่งสินค้าและชำระเงินปลายทาง</div>
            </div>
            <div class="clear"></div>
        </div>
    </div>

    <div id="faq">
        <div class="width">
            <div class="title">คำถามที่พบบ่อย</div>
            <div class="clear"></div>

            <div class="faq-top">

                <div class="spoiler_links redi">เพื่อตอบสนองคำขอของลูกค้า ส่วนประกอบของมาส์กถ่านต้นไผ่</div>
                <div style="display: none;" class="spoiler_body">อีกครั้งแล้วที่ตอบสนองคำขอของผู้อ่าน เรากำลังตีเผยแพร่รูปภาพผลิตภัณฑ์จริงพร้อมกับคำอธิบาย<br><br>

                    <div class="in-dev">ส่วนประกอบ</div><br>
                    ส่วนผสมจากธรรมชาติ: อควา (1), โพลีไวนิลแอลกอฮอล์ (2), กลีเซอรีน (3), โพรพิลีนไกลคอล (4), น้ำมันข้าวสาลี (5), สควาเลน (6), แพนทีนอล (7), คอลลาเจน (8) ถ่านไม้ไผ่ (9), น้ำมันส้มโอ (10)<br><br>
                    <ul>
                        <li>อควา – น้ำ</li>
                        <li>
                            โพลีไวนิลแอลกอฮอล์ – ถูกใช้เป็นพื้นฐานเพื่อทำความสะอาดผิวหน้าจากสิ่งสกปรก ผงฝุ่นและแบคทีเรีย การทำงานและประโยชน์คือ (จาก 1 ถึง 10): การป้องกัน 9 คลีนเซอร์ เหมาะสำหรับทุกสภาพผิว ไม่มีผลข้างเคียง
                        </li>
                        <li>
                            กลีเซอรีน – ผลิตภัณฑ์จากธรรมชาติที่ทำจากพืช ช่วยในการดูดซับคุณค่าสารบำรุงเข้าสู่ผิว กลีเซอรีนทำให้ผิวนุ่มนวลอย่างเป็นธรรมชาติและเป็นผู้ปรับเสถียรภาพที่ให้ใบหน้ารู้สึกเย็นสบายและเห็นผลมากเมื่อใช้ร่วมกับสารอื่น ๆ เหมาะสำหรับทุกสภาพผิว รวมทั้งผิวมันและผิวแพ้ง่าย
                        </li>
                        <li>
                            โพรพิลีนไกลคอล - การทำงานและประโยชน์คือ (จาก 1 ถึง 10): ทำหน้าที่เป็นครีมบำรุงผิว 10 เหมาะสำหรับทุกสภาพผิว ส่วนประกอบที่อันตราย: 3 (ต่ำมาก) นิยมใช้ในผลิตภัณฑ์เครื่องสำอาง ไม่เกิดการสะสมในร่างกาย ดังนั้นจึงใช้ในผลิตภัณฑ์อาหารได้เช่นกัน ไม่มีผลข้างเคียง
                        </li>
                        <li>
                            น้ำมันข้าวสาลี – ส่วนผสมจากธรรมชาติ สามารถบำรุงผิวได้อย่างเป็นธรรมชาติและคราบจางหายอย่างรวดเร็ว ข้าวสาลีทำให้ผิวสวยและทำให้ผิวนุ่มนวล ข้าวสาลียังมีเส้นใยที่ดีซึ่งเป็นประสิทธิภาพที่ดีที่ทำให้ผิวดูนวล ดีมากต่อการรักษาความยืดหยุ่นบนผิวหน้า เพิ่มการไหลเวียนของเลือด ผิวเรียบเนียน และทำให้อ่อนวัย ส่วนผสมจากธรรมชาติ: โอเมก้า 3 วิตามินอี โทโคฟีรอล แคโรทีนอยด์ บีตาแคโรทีน เลซิติน เมไธโอนีน สควาเลน ไฟโตสเตอรอล วิตามิน A D  และ PP กรดแพนโทเทนิก กรดโฟลิก สังกะสี เหล็ก โพแทสเซียม กำมะถัน ฟอสฟอรัส โปรตีน อัลลันโทอิน
                        </li>
                        <li>
                            สควาเลน – ส่วนผสมจากธรรมชาติที่ทำจากน้ำมันมะกอก แหล่งที่มาของโอเมก้า 3 ซึ่งเป็นกรดไขมันดี ช่วยบำรุงรักษาผิว ช่วยเพิ่มความชุ่มชื้นได้อย่างดีที่สุด กำจัดปัญหาริ้วรอยในช่วงต้น ช่วยให้สารที่ใช้งานถูกดูดซึมเข้าสู่ผิวและซ่อมแซมเซลล์ผิวบนใบหน้า เหมาะสำหรับทุกสภาพผิว รวมทั้งผิวมันและผิวแพ้ง่าย
                        </li>

                        <li>
                            แพนทีนอล - โปรวิตามินบี 5 มีสารฮิวเมกเตนท์ ทำให้ผิวนวลและส่งผลให้ผิวชุ่มชื้น ใช้กันทั่วไปในผลิตภัณฑ์เครื่องสำอางเพื่อดูแลผิวและผม แนะนำสำหรับเด็ก ทำให้ผิวนุ่มและช่วยป้องกันผิวหน้าให้คงสภาพชุ่มชื่น ไม่มีผลข้างเคียง
                        </li>
                        <li>
                            คอลลาเจน – ถูกใช้เป็นพื้นฐานเพื่อช่วยดูดซับคุณค่าสารบำรุงเข้าสู่ผิว เหมาะสำหรับทุกสภาพผิว รวมทั้งผิวมันและผิวแพ้ง่าย ส่วนผสมที่ปลอดภัยและเป็นธรรมชาติ ไม่มีผลข้างเคียง
                        </li>
                        <li>
                            ถ่านไม้ไผ่ – ส่วนผสมจากธรรมชาติ ใช้ทำความสะอาดผิวหน้าจากสิ่งสกปรกและฝุ่นละอองที่ทำให้เกิดการอุดตันของรูขุมขนซึ่งเป็นสาเหตุของบ่อเกิดสิวการอุดตัน ถ่านไม้ไผ่เป็นส่วนผสมที่มีประสิทธิภาพมากในการรักษาสิว ทำความสะอาดรูขุมขนเพื่อช่วยป้องกันไม่ให้สิวใหม่ เหมาะสำหรับทุกสภาพผิว รวมทั้งผิวมันและผิวแพ้ง่าย ส่วนผสมที่ปลอดภัยและเป็นธรรมชาติ ไม่มีผลข้างเคียง
                        </li>
                        <li>
                            น้ำมันส้มโอ – ทำให้ผิวขาวขึ้น ช่วยฟื้นฟูผิวเพื่อที่ได้รับใบหน้าที่สะอาดหมดจดและผ่องสดใส และช่วยควบคุมความมัน กระชับรูขุมขนบนใบหน้า เพิ่มความกระจ่างใส และการไหลเวียนโลหิตของใบหน้า ช่วยป้องกันและกำจัดการอุดตันของรูขุมขนและจุดด่างดำ
                        </li>
                        <li>
                            น้ำหนักรวมของแพ็กเกจ: 60 กรัม
                        </li>
                    </ul>
                    <br>
                    <div class="in-dev">การจัดเก็บและการใช้งาน</div><br>
                    กรุณาเก็บในที่มืด ในอุณหภูมิห้อง (15 ° C ถึง 25 ° C)  หลังจากการใช้งาน กรุณาปิดผนึกให้แน่นและเก็บให้ไกลจากเด็กและสายตาของเด็ก ไม่มีผลข้างเคียง การใช้งานง่ายดาย

                </div>

                <div class="spoiler_links">ฉันสามารถใช้เครื่องสำอางตัวอื่นหรือครีมทาหน้าขณะที่ใช้มาส์กถ่านต้นไผ่ได้ไหม?</div>
                <div style="display: none;" class="spoiler_body">
                    ได้ค่ะ คุณสามารถใช้เครื่องสำอางอื่น ครีมทาหน้าหรืออื่น ๆ ขณะที่ใช้มาส์กถ่านต้นไผ่ได้ค่ะ ก่อนที่ใช้มาส์ก อย่าลืมล้างหน้าของคุณก่อน
                </div>

                <div class="spoiler_links">เมื่อไหร่ฉันถึงจะเห็นผล?</div>
                <div style="display: none;" class="spoiler_body">
                    สามารถเห็นผลได้หลังจากการใช้งานครั้งแรก มาส์กถ่านต้นไผ่จะช่วยให้ผิวหน้าของคุณจะมีสุขภาพดีขึ้น กระชับมากขึ้นและคงความเยาว์วัย เราขอแนะนำให้คุณใช้มาส์กเป็นประจำเพื่อการป้องกันการอุดตันรูขุมขน
                </div>

                <div class="spoiler_links">มีอาการแพ้บ้างไหม?</div>
                <div style="display: none;" class="spoiler_body">
                    รับประกัน 99% ว่าเป็นผลิตภัณฑ์จากธรรมชาติ ซึ่งไม่ก่อให้เกิดผลข้างเคียงใด ๆ และไม่มีอาการแพ้ใด ๆ แต่ก่อนการใช้งาน กรุณาใช้ผลิตภัณฑ์ตัวอย่างทาบนข้อมือด้านในของคุณ และรอ 20 นาทีเพื่อดูว่ามีอาการใด ๆ เกิดขึ้น
                </div>

                <div class="spoiler_links">มาส์กนี้เหมาะสำหรับทุกประเภทของผิวหรือไหม?</div>
                <div style="display: block;" class="spoiler_body">
                    ใช่ค่ะ มาส์กนี้เหมาะสำหรับทุกสภาพผิว รวมทั้งผิวมันและผิวแพ้ง่าย
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('build/js/vendor/jquery.min.js') }}"></script>
    <script src="{{ asset('build/js/vendor/bootstrap.min.js') }}"></script>
    <script src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script src="{{ elixir('js/bmask2/all.js') }}"></script>

    <script>
        $(function() {
            $('.carousel').carousel();

            $('.spoiler_links').click(function() {
                if ($(this).next(
                                '.spoiler_body').css(
                                "display") == "none") {
                    $('.spoiler_body').hide(
                            'normal');
                    $(this).next('.spoiler_body').toggle(
                            'fast');
                } else $('.spoiler_body').hide(
                        'fast');
                return false;
            });
        });
    </script>

    <script>
        $(function() {
            $(window).scroll(function() {
                if ($(this).scrollTop() != 0) {
                    $('#toTop').fadeIn();
                } else {
                    $('#toTop').fadeOut();
                }
            });
            $('#toTop').click(function() {
                $('body,html').animate({
                    scrollTop: 0
                }, 800);
            });
        });
    </script>

    <script>
        String.prototype.toHHMMSS = function () {
            var sec_num = parseInt(this, 10),
                    hours = Math.floor(sec_num / 3600),
                    minutes = Math.floor((sec_num - (hours * 3600)) / 60),
                    seconds = sec_num - (hours * 3600) - (minutes * 60);

            if (hours < 10) {
                hours = "0" + hours;
            }
            if (minutes < 10) {
                minutes = "0" + minutes;
            }
            if (seconds < 10) {
                seconds = "0" + seconds;
            }
            return hours + ' ' + minutes + ' ' + seconds;
        };

        var start = moment({hour: 0, minute: 0, seconds: 0, milliseconds: 0}).unix();
        var now = moment().unix();
        var count = String((86400 - (now - start)));

        var counter = setInterval(timer, 1000);

        function timer() {
            if (parseInt(count) <= 0) {
                clearInterval(counter);
                $('#promotion-holder').remove(0);
                return;
            }
            var temp = count.toHHMMSS();
            var tempArr = temp.split(' ');
            var hour = tempArr[0];
            var min = tempArr[1];
            var sec = tempArr[2];

            count = (parseInt(count) - 1).toString();
            $('.timer-wrapper-time-hour').html(hour);
            $('.timer-wrapper-time-minute').html(min);
            $('.timer-wrapper-time-second').html(sec);
        }
    </script>
@endsection
