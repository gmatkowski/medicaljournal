<html lang="pl"><head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <title></title>

    <link rel="shortcut icon" href="{{asset('build/images/frontend/medical.ico')}}">

    <link rel="stylesheet" href="{{ elixir('css/hallupro/all.css') }}">
    <link rel="stylesheet" href="{{ elixir('css/pre/all.css') }}">

    @include('ga.halux')
    @include('pixel.hallupro')
</head>
<body class="pl">

<div class="widget visible">
    <div class="widgetWrap"><p class="sale-text"> diskon <span>50%</span></p>

        <div class="countdown"><span id="countdown-text">periode diskon terbatas:</span> <span id="hours">00</span>:<span id="min">46</span>:<span id="sec">46</span></div>
        <a href="#buyForm" class="btn red">untuk memesan</a></div>
</div>
@desktop
<br><br><br>
@enddesktop
    <div class="wrapper ">

        <div class="row">

            <div class="content">

                <h1><strong>Dapatkan kembali kaki indah dan sehat. </strong>Anda dapat mengatasi bunion dalam 7 hari!</h1>

                <ul class="hal-under-h1" style="float:left;">

                    <li>Menghilangkan pembengkakan dan kapalan</li>

                    <li>Terbebas dari nyeri dan rasa kurang nyaman</li>

                    <li>Kaki sehat tanpa jari bengkak dan bengkok</li>

                </ul>

                <img src="{{ asset('build/images/frontend/upper-halux.jpg') }}" style="float:right;" alt="" />

                <div class="clearfix"></div>

                <div class="quote margin-top-30">

                    <h2>„Kaki indah dan sehat tanpa usaha apapun – sulit dipercaya tapi memang ada, cara sederhana”</h2>

                    <div class="row">

                        <div class="left-side">

                            <p>„Kita semua tahu bahwa kaki indah dan sehat merupakan simbol keindahan wanita. Hal itu jelas – sebelum terapi saya malu sekali, ada bunion dan jari bengkok cukup lama… Saya tidak tahu bahwa cukup beberapa hari untuk mengatasi masalah kaki, misalnya bunion. Dalam beberapa hari saya mendapatkan kembali kaki indah dan sehat. Akhirnya tampak saya seperti wanita sejati! Saya merekomendasikan cara ini dengan sepunuh hati.”</p>

                            <p><strong>Anggia Sikumbang</strong><br>Dia berhasil mengatasi bunion dalam 11 hari</p>

                        </div>

                        <img src="{{ asset('build/images/frontend/img11.jpg') }}" class="right" alt="Hal quote1 img">

                    </div>

                </div>

                <section class="block-1 margin-top-20">

                    <div class="row">

                        <p>”Halo,</p>

                        <figure class="pull-right">

                            <img src="{{ asset('build/images/frontend/spec.jpg') }}" class="block-1-img" alt="Hal spec">

                        </figure>

                        <p class="margin-top-15">Saya senang untuk memberitahu Anda bahwa metode baru untuk mengatasi bunion akhirnya resmi tersedia di Indonesia. Berikut adalah cara revolusioner dalam ilmu pengobatan dan pemeliharaan tapak kaki. Selama 16 tahun saya bekerja sebagai dokter. Kenyataannya, saya sudah membantu ratusan ribu orang untuk menyembuhkan penyakit bunion tanpa operasi. Mereka mendapatkan kembali kaki indah dan sehat, mengobati bunion, meluruskan jari bengkok, mengatasi peradangan dan pembengkakan, menghilangkan kapalan. Selama banyak tahun pengalaman, saya telah belajar hal yang sangat penting. Untuk mengobati bunion secara permanen, dibutuhkan motivasi kuat, kemauan dan keteraturan. Mengapa?</p>

                        <p class="margin-top-15">
                            Kebenaran mungkin mengejutkan Anda. Memang benar bahwa menurunkan berat badan atau perubahan jenis sepatu – semuanya nihil untuk mengatasi masalah kaki secara permanen. Akhirnya, operasi bedah merupakan jalan terakhir untuk mengatasi bunion. Padahal, Anda perlu sesuatu yang kuat dan efektif untuk mengatasi masalah kaki tanpa meninggalkan rumah. Anda perlu suatu cara pengobatan bunion yang aman dan permamen.
                            <br>
                            Saya telah mencoba banyak cara. Hanya ada satu solusi untuk mengobati bunion dan mengatasi masalah kaki. Inilah satu-satunya cara untuk menghilangkan nyeri dan meluruskan jari bengkok dengan mudah dan cepat.
                        </p>

                        <p class="margin-top-15">Malahan, saya sering membantu wanita yang berhasil mengatasi bunion hanya dalam 24 jam. Kebanyakan wanita menikmati kaki indah dan sehat setelah 2 minggu pemakaian cara baru tersebut.</p>

                        <p class="margin-top-15">Ada wanita yang berhasil mengatasi masalah kaki, sendi dan tulang dalam 20 hari.</p>

                        <p class="margin-top-15">Sudah berapa lama Anda berjuang dengan masalah kaki, bunion dan nyeri, dan belum berhasil? Sekarang Anda dapat mendapatkan kembali kaki indah dan sehat, menjadi sama seperti wanita sejati. Dengan cara tersebut Anda dapat:</p>

                        <ul class="hal-under-h1">

                            <li>Mendapatkan kembali kaki indah dan sehat dalam 15-25 hari</li>

                            <li>Memperkuat sendi dan otot kaki hingga 80%</li>

                            <li>Meningkatkan kekuatan struktur yang menstabilkan kaki</li>

                            <li>Mengatasi pembengkakan dan peradangan dalam 7-10 hari</li>

                            <li>Menghilangkan deformasi dan kapalan secara efektif</li>

                        </ul>

                        <p class="margin-top-20">Hasil ini bisa dicapai karena metode tersebut bekerja dalam 3 cara, secara bersamaan:</p>

                    </div>

                </section>

                <section>

                    <h2 class="one-line">MEMBANTU MENGATASI NYERI</h2>

                    <p class="blue-left">Ketika sendi kaki Anda terasa tak nyaman, sulit sekali mengatasi bunion secara efektif. Nyeri dan rasa tak nyaman membuat Anda lelah, akibatnya Anda tidak percaya lagi kepada pengobatan. Oleh karena itu, cara baru tersebut membantu Anda mengatasi masalah kaki dengan mudah dan cepat, secara permanen dan selamanya. Dengan mengatasi nyeri, Anda dapat mengatasi masalah kaki secara nyaman dan efektif.</p>

                </section>

                <section class="block-3">

                    <h2 class="one-line">MEMPERKUAT SENDI DAN OTOT</h2>

                    <p class="blue-left">Setelah mengurangi ketegangan dan nyeri otot, sendi kaki Anda akan menjadi hingga 4 kali lipat lebih kuat. Cukup beberapa hari untuk meningkatkan kadar dan memperkuat struktur ligamen dan tendon (penghubung antar tulang) dan meningkatkan aliran darah ke kaki Anda. Dengan memperkuat sendi dan otot Anda secara permanen, Anda akan terbebas dari nyeri dan menikmati kaki indah dan sehat.</p>

                    <h2 class="one-line" style="margin-top:28px">MEMBANTU MENGHILANGKAN BUNION</h2>

                    <p class="blue-left">Setelah pemakaian pertama, Anda mengatasi masalah kaki secara otomatis. Lupakan mengenai terapi, lupakan mengenai obat atau alat lain. Dengan bantalan bunion tersebut, Anda dapat mengobati bunion secara nyaman dan efektif, tanpa rasa sakit. Anda mendapatkan kembali kaki indah dan sehat hanya dalam beberapa hari.</p>

                    <p class="margin-top-30">Kenapa alat tersebut begitu istimewa? <b>Konstruksinya unik dan biomekanik</b>...</p>

                    <p class="margin-top-15">Bantalan bunion adalah alat yang berbeda dari produk lain, karena diformulasikan dengan menggunakan teknologi terbaru dan bahan-bahan berkualitas tinggi yang nyaman digunakan. Sebelumnya, alat tersebut hanya digunakan oleh para ahli - untuk rehabilitasi atlet dari cedera olahraga. Sekarang digunakan oleh orang biasa – teknologinya biomekanik, sehingga memenuhi semua jenis sepatu. Anda dapat memakainnya dengan nyaman setiap hari, tanpa rasa sakit atau nyeri, karena konstruksinya aman. Artinya, Anda dapat mengobati masalah kaki dengan cepat. Konstruksinya unik dan membantu Anda mengatasi masalah kaki, sendi dan tulang dalam waktu singkat, secara efektif, aman dan nyaman. Inilah terobosan dalam kedokteran podiatrik, karena inilah cara pengobatan tanpa rasa sakit atau nyeri - cara nyaman dan mudah. Penemuan saya sudah dipatenkan dan sekarang tersedia di Indonesia. Inilah alat aman dan nyaman, cocok untuk semua jenis kaki. Selain itu, bantalan bunion saya tidak menimbulkan efek samping atau reaksi alergi apapun.</p>

                    <p class="margin-top-15">Hasilnya sangat manjur, sudah terbukti ribuan wanita yang berhasil mengobati bunion. Rahasia kesuksesan alat tersebut adalah konstruksinya – sangat unik dan biomekanik. Anda dapat memakai bantalan bunion saya setiap hari dengan nyaaman, dan mengatasi masalah kaki dalam waktu singkat.</p>

                    <p class="margin-top-15">Memang, kita harus menyadari bahwa alat begitu istimewa perlu menggunakan teknologi terbaru dan bahan-bahan ortopedi berkualitas tinggi - yang nyaman digunakan. Saya menghubungi produsen yang memasok alat asli tersebut. Sekarang bantalan bunion resmi tersedia di Indonesia - dengan nama dagang {{ $product3->name}}.</p>

                    <p class="margin-top-15">{{ $product3->name}} membantu Anda mengatasi masalah kaki:</p>

                    <h2 class="one-line margin-top-20">DENGAN MUDAH</h2>

                    <p class="blue-left">Tanpa konsultasi dengan dokter, tanpa bedah, tanpa latihan, tanpa meninggalkan rumah. Biarkan alat ini yang berkerja untuk Anda. Yang harus Anda lakukan adalah memakai bantalan bunion ini dan... lupakan mengenai masalah kaki. Setelah beberapa hari kaki Anda menjadi indah dan sehat.</p>

                    <h2 class="one-line margin-top-20">DENGAN CEPAT</h2>

                    <p class="blue-left">Bantalan bunion membantu Anda menghilangkan bunion 24 jam per hari, karena pemakaiannya 100% aman dan nyaman. Jadi, dibandigkan degan produk lain, Anda dapat mengatasi masalah kaki dan mengobati bunion hingga 4 kali lipat lebih cepat. </p>

                    <h2 class="one-line margin-top-20">DENGAN AMAN DAN NYAMAN</h2>

                    <p class="blue-left">Dengan alat ortopedi ini tidak hanya Anda dapat mengobati bunion. Konstruksinya biomekanik, jadi  bantalan bunion ini juga membantu Anda: meningkatkan aliran darah, menghilangkan pembengkakan, peradangan dan kapalan pada kaki, mengobati dan memperkuat sendi, tulang dan otot kaki Anda secara intensif.</p>

                    <p class="margin-top-15">Setiap hari ada semakin banyak wanita Indonesia yang berhasil mengobati bunion hanya dalam beberapa hari dengan alat ini. Dulu kebanyakan mereka tidak menyadari bahwa ada cara begitu sederhana untuk mengatasi masalah kaki. Sekarang mereka dapat menikmati kaki indah dan sehat, tanpa masalah apapun.</p>

                    <p class="margin-top-15">Selama banyak tahun pengalaman, saya telah mengetahui bahwa alat ini adalah cara ampuh untuk mengobati bunion dan mengatasi masalah kaki. Efektivitasnya telah terbukti oleh banyak wanita. Salah satu dari mereka adalah ibu Nurfitria Mubarokah dari Bandung.</p>

                </section>

                <div class="quote background">

                    <div class="row">

                        {{--<h2>„Cara alami untuk kaki indah. Benar!”</h2>--}}

                        <h2>„Cara alami untuk kaki indah.<br>Benar!”</h2>

                        <p>„Saya sudah mencoba banyak cara untuk mengobati bunion, tapi gagal. Akhirnya… saya mencoba {{ $product3->name}} ini, cara aman dan efektif. 3 minggu cukup untuk meluruskan jari bengkok dan mengobati bunion secara permanen. Setelah terapi saya mendapatkan kembali kaki indah, sekarang saya merasa seperti wanita sejati, cantik dan sehat.”</p>

                        <p id="mobilebg">
                            <img src="{{ asset('build/images/frontend/21bg.png') }}" alt="">
                        </p>
                        <p class="sign margin-top-15">
                            {{--<strong>Nurfitria Mubarokah</strong><br>--}}
                            Dia berhasil mengobati bunion, peradangan dan pembengkakan dalam 21 hari.
                        </p>
                    </div>

                </div>

                <div class="clearfix"></div>

                <p class="margin-top-20">{{ $product3->name}} menjadi semakin laris dan semakin banyak wanita yang dapat menikmati kaki indah. Memang, kita harus menyadari bahwa cara ini – sama seperti cara lain – tidak sama untuk semua orang. Sebagian kecil orang yang mempunyai kerentanan genetik. Artinya, terapi mereka dapat berlangsung hingga 2-3 minggu lebih lama.</p>

                <p class="margin-top-15">Namun, kami yakin {{ $product3->name}} adalah cara ampuh. Oleh karena itu kami memberikan jaminan.</p>

                <section class="end">

                    <h2 class="margin-top-30">BAGAIMANA CARA MEMESAN {{ $product3->name}} DAH MENGHEMAT<br>{!! StrHelper::spaceInPrice($product3->price_old-$product3->price) !!} {{ $product3->currency }}?</h2>

                    <p class="margin-top-15">Bahan-bahan ortopedi berkualitas tinggi sulit didapatkan – jadi bantalan bunion biomekanik yang efektif dan nyaman digunakan tidak bisa murah. Namun, produsen memberikan diskon khusus untuk Anda yang pertama kali mengunjungi website kami. Anda cukup memesan hari ini, maka Anda akan mendapatkan diskon spesial {{ round(100-($product3->price*100/$product3->price_old)) }}%.</p>

                    <p class="margin-top-15">Promo hanya hari ini: bantalan bunion berkualitas tinggi {{ $product3->name}} {!! StrHelper::spaceInPrice($product3->price) !!} {{ $product3->currency }}</p>

                    <p class="margin-top-15">Harga segera naik!</p>

                    <div class="info">

                        Untuk memesan bantalan bunion biomekanik<br>{{ $product3->name}}, bacalah petunjuk berikut. <span>Jangan melakukan pembayaran di muka apapun!</span>

                    </div>

                    <p><strong>1.</strong> Klik pada tombol „Pesan Sekarang”.
                       <strong><br>2.</strong> Isi formulir online di halaman berikutnya.
                       <strong><br>3.</strong> Siap! Anda akan mendapatkan {{ $product3->name}} dalam 2-3 hari.
                    </p>

                    <p class="margin-top-15">Secara pribadi, saya yakin bahwa Anda akan sepenuhnya puas. Pasti Anda akan mengobati masalah bunion jauh lebih cepat dari yang Anda bayangkan.</p>

                    <div class="art-sign">

                        Semoga sukses!”

                    </div>

                    <div class="product-order">

                        <p class="black">100% asli bantalan bunion biomekanik {{ $product3->name}}</p>

                        <div class="product-image">

                            <img src="{{ asset('build/images/frontend/hallupro.jpg') }}" alt="Hallupro">

                            <div class="price">

                                <span>{!! StrHelper::spaceInPrice($product3->price_old) !!} {{ $product3->currency }}</span>

                                {!! StrHelper::spaceInPrice($product3->price) !!} {{ $product3->currency }}
                            </div>

                        </div>

                        <p class="hal_center">Pesan sekarang untuk menghemat {!! StrHelper::spaceInPrice($product3->price_old-$product3->price) !!} {{ $product3->currency }} dan mendapatkan pengiriman gratis. Tanpa resiko apapun. PROMO hanya hari ini!</p>
                        <br>
                        {{--<div style="text-align:center">--}}
                            {{--<b>nomor: 0888 01000 488</b><br>--}}
                            {{--<b>email: contact@medical-jurnal.com</b>--}}
                        {{--</div>--}}
                        @include('page.garcinia_order_form', [
                            'form' => $form,
                            'header1' => 'YA! Saya ingin mendapatkan '.$product3->name.' dengan promo, dengan harga <b>'. StrHelper::spaceInPrice($product3->price).' '.$product3->currency.'</b>',
                            'header2' => 'Pengiriman '.$product3->name.' dilakukan melalui kurir atau pos, secara GRATIS, dalam beberapa hari kedepan. Metode pembayaran: COD (Pembayaran di Tempat).',
                            'productId' => 3,
                            'productName' => $product3->name
                        ])

                    </div>

                </section>

                <footer>

                    <a href="#" onclick="window.open('privacy-policy', 'windowname1', 'width=670, height=765, scrollbars=1'); return false;">Kebijakan Privasi</a>

                </footer>

            </div>

            <aside>

                <div class="effect">

                    <img src="{{ asset('build/images/frontend/aside1.jpg') }}" alt="hal aside 1">

                    <div class="txt">

                        <h4>„Aku tak malu lagi. Kaki indah dan sehat!”</h4>

                        <p>Sebelum terapi aku malu memakai sandal… Ada bunion dan saya tidak merasa seperti wanita sejati. Dengan {{ $product3->name}} saya meluruskan jari bengkok dan mendapatkan kembali kaki indah. Aku tak malu lagi! Saya sangat terkejut dengan hasilnya. Aku gembira dan bersyukur. Suamiku juga :)</p>

                        <p class="end"><i>Retno Nikmadana</i><br>Solo</p>

                    </div>

                </div>

                <div class="effect">

                    <img src="{{ asset('build/images/frontend/aside2.jpg') }}" alt="hal aside 2">

                    <div class="txt">

                        <h4>„Saya berhasil mengobati bunion”</h4>

                        <p>Saya suka memakai high heels, jadi ketika makin dewasa jari kaki saya semakin bengkok, saya cari solusi supaya meluruskannya. {{ $product3->name}} sangat ajaib! Hanya dalam 2 minggu saya berhasil meluruskan jari bengkok, menghilangkan peradangan dan pembengkakan. Akhirnya saya bebas dari nyeri dan rasa kurang nyaman – saya cantik dan sehat. Terima kasih atas {{ $product3->name}}.</p>

                        <p class="end"><i>Perwi Darmayanti</i><br>Medan</p>

                    </div>

                </div>

                <div class="effect">

                    <img src="{{ asset('build/images/frontend/aside3.jpg') }}" alt="hal aside 3">

                    <div class="txt">

                        <h4>„Kaki indah setelah beberapa pemakaian”</h4>

                        <p>Dulu gue malu, gue tak percaya diri – malu pergi ke pantai, ke kolam renang, ke salon kecantikan. Gue malu sekali jari bengkok gue… Sekarang kaki gue indah dan sehat dan gue suka pergi kolam renang, fitness atau SPA. Bantalan bunion ini menyelamatkan hidup gue!</p>

                        <p class="end"><i>Surya Wati</i><br>Surabaya</p>

                    </div>

                </div>

                <div class="effect">

                    <img src="{{ asset('build/images/frontend/aside4.jpg') }}" alt="hal aside 4">

                    <div class="txt">

                        <h4>„Obat untuk masalah sendi”</h4>

                        <p>{{ $product3->name}} tak hanya membuat kaki indah dan sehat, tapi juga membantu mengobati masalah sendi. Saya sudah lupa mengenai peradangan, pembengkakan dan kapalan, saya tak mengalami nyeri, tak mengalami rasa kurang nyaman. Sekarang saya dapat menikmati berjalan lagi! Saya merasa jauh lebih sehat dan cantik. Siapa tak suka!</p>

                        <p class="end"><i>Dwita Siregar</i><br>Semarang</p>

                    </div>

                </div>

                <div class="effect">

                    <img src="{{ asset('build/images/frontend/aside5.jpg') }}" alt="hal aside 5">

                    <div class="txt">

                        <h4>„Cara ampuh untuk mengobati bunion!”</h4>

                        <p>Cukup lama saya mengalami masalah ini - ada bunion ya. Saya mengalami nyeri dan saya malu saja… Saya sudah coba banyak cara pengobatan, akhirnya {{ $product3->name}} membantu saya mengatasi masalah tersebut. Cukup memakai {{ $product3->name}} setiap hari daam waktu singkat. Lupakan mengenai bunion, 25 hari terapi dengan {{ $product3->name}} cukup. Sekarang suami saya suka menyentuh kaki saya, karena indah :) Saya merekomendasikan.</p>

                        <p class="end"><i>Nandiasari Susianto</i><br>Samarinda</p>

                    </div>

                </div>

                <div class="effect">

                    <img src="{{ asset('build/images/frontend/aside6.jpg') }}" alt="hal aside 6">

                    <div class="txt">

                        <h4>„Senang dengan kaki telanjang”</h4>

                        <p>Dengan {{ $product3->name}} aku berhasil menghilangkan bunion pada kaki aku. Cukup memakai bantalan bunion tersebut untuk mengobati bunion secara permanen dan selamanya. Sulit dipercaya tapi memang ada! Inilah cara sederhana, akhirnya saya senang dengan kaki saya. Coba!</p>

                        <p class="end"><i>Wati Istanti</i><br>Padang</p>

                    </div>

                </div>

            </aside>

        </div>

    </div>

    <div class="fade" style="display: none;">

        <div class="superpromo">


            <span class="circle-percent">73%!</span>

            <h1><span>POCZEKAJ!</span>Odbierz jednorazowy rabat

            </h1>

            <p style="margin-top: 10px; margin-bottom: 10px;">Oferta skierowana jest <strong>TYLKO do Ciebie</strong> i za 15 minut wygaśnie.</p>

            <p style="font-size: 16px;">By odebrać promocyjne opakowanie, wróć na stronę.</p>

            <div class="actions">

                <a class="greenBtn" style="margin-top: 5px; padding: 15px 40px; background: #b7276d;" href="/promo/12">Chcę odebrać zniżkę</a><br>

                <span onclick="javascript:$('div.fade').hide();" id="close-superpromo">Rezygnuję z promocji</span>

            </div>

        </div>

    </div>

    @include('part.countdown')
</body>
</html>