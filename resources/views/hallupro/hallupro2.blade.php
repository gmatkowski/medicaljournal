<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="{{ elixir('css/hallupro2/all.css') }}">
    @include('part.chat')
</head>
<body>
<div class="container">
    <h1>Revolusi dalam pengobatan bunion. Sebuah temuan revolusioner yang menghapus bunion dalam beberapa hari.</h1>
    <p class="date-tag">23 December, 2016</p>
</div>
<img src="{{ asset('build/images/frontend/banner-2.jpg') }}" style="width:100%" alt="">

<div class="caption container">
    <div class="col-md-12">
        <strong style="display:inline-block;padding-left:20px;">Hasil obat</strong>
    </div>
</div>

<div class="container main-wrapper" style="padding-top:30px;padding-bottom:30px;">
    <div class="col-md-8">
        <p>Hasilnya dapat dilihat pada gambar di bawah ini, hasil yang dicapai oleh Ibu Soeria Soemantri hanya setelah 15 hari terapi</p>

        <img class="img-responsive" src="{{ asset('build/images/frontend/kaki-3.jpg') }}" alt="">
        <p>Jari bengkok, masalah bunion, rasa nyeri yang berulang-ulang dll. – hampir 17 ribu wanita Indonesia berhasil mengatasi masalah tersebut dengan mudah. Tanpa operasi, tanpa bedah, tanpa dokter, tanpa meninggalkan rumah. Para dokter spesialis ortopedi setuju: cara baru yang ditemukan oleh Prof. Gardner adalah temuan revolusioner dalam ilmu pengobatan dan pemeliharaan tapak kaki. Apakah tidak akan ada lagi semua masalah kaki?</p>
        <h2>Mengatasi masalah kaki – AS mempelopori revolusi dalam pengobatan kaki</h2>
        <p>Penelitian terbaru menunjukkan bahwa lebih dari 212 ribu wanita Indonesia berhasil mengatasi masalah bunion dan mendapatkan kembali kaki indah dengan cara baru dan inovatif. 86% wanita tersebut berhasil mengatasinya hanya dalam 2 minggu. Ini bukan sihir, ini hasil nyata: obat alami yang ditemukan oleh Dr. Prof. Gardner, seorang dokter spesialis dalam kedokteran podiatrik dari Amerika, menjadi semakin laris manis di seluruh dunia. Apa fenomena cara baru tersebut?</p>

        <h2>Bagaimana masalah kesehatan Anda dapat diatasi dalam 48 jam?</h2>
        <p>Satu hal yang pasti: untuk mengatasi masalah kaki dan mengobati bunion Anda harus mengatasi sumber masalah dengan tuntas. Selain mendapatkan kembali kaki indah dan sehat, dengan meningkatkan kinerja tulang dan sendi kaki secara aktif dan intensif, Anda dapat mengatasi masalah bunion secara permanen. Setelah diteliti secara mendalam, baik melalui penelitian lapangan maupun penelitian eksperimen, terbukti bahwa inilah terapi baru dan inovatif untuk mengatasi masalah bunion, nyeri, peradangan dan pembengkakan hanya dalam 48 jam. Obat tersebut ialah cara ampuh untuk meningkatkan aliran darah dan oksigen dalam kaki, dan memperkuat otot kaki – yaitu membantu Anda mendapatkan kembali kaki indah dan sehat tanpa bunion dengan mudah.</p>
        <h2>Senjata rahasia untuk mengobati bunion</h2>
        <img src="{{ asset('build/images/frontend/kaki.png') }}" class="pull-left" style="margin-bottom:20px;margin-right:20px;" alt="">
        <p>Inilah cara ampuh yang ditawarkan oleh dunia ilmu pengetahuan dan teknologi modern untuk mengatasi bunion tanpa operasi, tanpa bedah, tanpa meninggalkan rumah – cara efektif dan aman, mudah dan murah. Sekarang Anda bisa mengatasi bunion secara permanen dan selamanya.</p>
        <p>Para dokter spesialis dalam kedokteran podiatrik menekankan bahwa rahasia kesuksesan terapi baru tersebut hanya ada satu bahkan sangat unik. Apa ini, terapi maju dengan bantalan bunion, dibuat oleh seorang ahli dalam kedokteran podiatrik dan rehabilitasi atlet dari cedera olahraga – Peter Gardner?</p>
        <p>Diformulasikan dengan menggunakan teknologi terbaru, konstruksi alat unik dan biomekanik, dan bahan-bahan berkualitas tinggi – yang nyaman digunakan – bantalan bunion tersebut adalah alat yang luar biasa.</p>
        <p>"Alat tersebut memperbaiki aliran darah dan mempercepat regenerasi jaringan, sehingga mengurangi nyeri dan masalah kaki dengan cepat, memperkuat sendi dan otot kaki, dan meningkatkan kekuatan struktur yang menstabilkan kaki. Inilah satu-satunya bantalan bunion dengan konstruksi alat begitu unik yang tersedia di pasaran, yang membantu mengatasi masalah kaki secara aman dan permanen, tanpa meninggalkan rumah.”<br>
            <strong>Dr. Teguh Widiawati, ahli bedah ortopedik</strong>
        </p>
        <h2>Mencegah lebih baik daripada mengobati</h2>
        <p>Apa yang menarik tentang alat inovatif tersebut adalah bahwa teknologinya biomekanik, sehingga memenuhi semua jenis sepatu. Anda dapat memakai bantalan bunion tersebut dengan nyaman setiap hari, karena konstruksinya aman dan biomekanik.</p>
        <p>Apa yang penting dari alat tersebut adalah bahwa cara kerjanya bersifat multitasking, yaitu membantu mengobati masalah sendi dan tulang yang serius, mencegah peradangan, menghilangkan kapalan dan lain-lain. Artinya, Anda dapat mengatasi hampir semua masalah kaki, yaitu bunion, pembengkakan dan jari bengkok – tanpa dokter.</p>
        <p>Terlepas dari cara kerjanya – yang sangat cepat – jangan lupa, bahwa tidak semua orang dapat mengatasi masalah kaki tersebut dalam 7 atau 10 hari. Ada orang yang menunggu hingga 3 atau 4 minggu untuk mengatasi dan mengobati masalah kaki secara permanen.</p>
        <img class="img-responsive" src="{{ asset('build/images/frontend/kaki2.jpg') }}" style="width:100%;margin-bottom:10px;"alt="">
        <p>{{ $product3->name }} tidak hanya menghilangkan pembengkakan dan kapalan secara efektif, tapi juga mengobati masalah sendi dan tulang yang serius (masalah bunion). Alat tersebut mengatasi nyeri, mengurangi peradangan, dan meningkatkan kekuatan struktur yang menstabilkan kaki. Namun, hasilnya tergantung pada beberapa faktor. Untuk Anda yang mengalami obesitas (kegemukan) atau mempunyai kerentanan genetik, terapi dapat berlangsung selama 30 hari. Jadi, pengobatan dan waktunya dapat bervariasi tergantung pada tubuh manusia – tapi hasilnya telah terbukti secara klinis dan semua orang akhirnya dapat mencapai hasilnya yang diharapkan.</p>
        <p>Dr. Prof. Gardner, seorang dokter spesialis dalam kedokteran podiatrik dan rehabilitasi, dikenal sebagai penemu bantalan bunion – alat yang sangat inovatif</p>
        <h2>Masalah kaki bikin malu?</h2>
        <p>Atasi dengan cara sederhana ini. Di Indonesia masih banyak wanita yang tidak menyadari bahwa mereka dapat memerangi masalah kaki dan mendapatkan kembali kaki indah dan sehat tanpa meninggalkan rumah, dengan mudah dan cepat. Namun, metode Gardner yang semakin laris membawa harapan besar bagi wanita Indonesia – akhirnya wanita di Tanah Air dapat sembuh dari masalah kaki secara revolusioner.</p>
        <center><a href="{{ $volum }}"><img src="{{ asset('build/images/frontend/hallupro.jpg') }}" alt=""></a></center>
        <p style="font-size:18px;text-align:center;">Awas alat palsu – para ahli memperingatkan bahwa di pasaran muncul banyak alat palsu yang murah dan meniru produk asli, yaitu bantalan bunion Prof. Gardner. Para ahli ortopedi merekomendasikan bantalan bunion prof. Gardner, karena konstruksinya unik dan biomekanik, diformulasikan dengan menggunakan bahan-bahan ortopedi berkualitas tinggi – yang sulit didapatkan. Tentu saja, Anda bisa mencoba mencari produk lain, tapi jangan lupa bahwa mungkin Anda tidak dapat memuaskan dengan hasil produk palsu tersebut.
        </p>
        <center><a href="{{ $volum }}" class="btn btn-lg btn-warning">Klik di sini untuk mendapatkan bantalan bunion <br>{{ $product3->name }} dan terbebas dari masalah kaki </a></center>
    </div>
    <div class="col-md-4" id="right-side">

        <h3 style="margin-top:0px;">Komentar</h3>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo01.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Rosa Dese</h4>
                4 tahun yg lalu ada bunion dan operasi. Sekarang ada satu kali lagi:/ ku tak mau bayar untu operasi lagi tak mau rasa sakit lagi:( ku harap bantalan bunion ini dapat membantu
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo02.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Suci Sudirman</h4>
                siapa yang sudah coba? Berhasil?
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo03.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Erlinda Cayao</h4>
                ibu gue senang dengan hasilnya:) dia berhasil mengobati masalah kaki hanya dalam 3 minggu kalo tak salah
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo04.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Nada Marsudi</h4>
                @Suci: coba hasilnya bervariasi tergantung pada tubuh manusia coba sendiri aja...
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo05.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Izmatul Farihah</h4>
                harap bantalan bunion dapat membantu aku! aku udh 2 tahun mengalami masalah ini
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo06.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Ning Farid</h4>
                dokter ku rekomendasikan bantalan bunion ini ya bagus dalam 2 minggu pemakaian aku berhasil mengobati bunion BAGUS SEKALI. aku tak mengalami nyeri lagi
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo07.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Kindy Retna</h4>
                saya suka memakai high heels tapi takut bunion! Ibu dan nenek saya ada masalah bunion mungkin saya beli dan kadang2 coba hanya sebagai profilaksis
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo08.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Ida Tirta</h4>
                ide bagusya mencegah lebih baik daripada mengobati
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo09.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Lisna Wati</h4>
                bagus sekali:) dalam 3 minggu gue berhasil mengobati bunion
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo10.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Rina Ratdyani</h4>
                aku udh beberapa hari mau beli tapi masih bingun. gimana kalo gagal?
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo18.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Lia Maulia</h4>
                Harus coba!
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo11.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Ratna Sari</h4>
                Aku beli utk ibuku dan dia senang sekali, dn terkejut dengan hasilnya HEBAT tapi tanpa konsultasi dengan dokter? Harus konsultasi dengan dokter
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo17.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Anggia</h4>
                untuk apa? Ini bukan obat ini alat aja sama seperti kaus kaki :P jangan khwatir dong
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo12.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Eva Hadriyantini</h4>
                udh pakai 4 hari kapalan hilang, bagus :)
            </div>
        </div>
        <div class="media">
            <div class="media-left">
                <a href="{{ $volum }}">
                    <img class="media-object img-circle" style="width:50px" src="{{ asset('build/images/frontend/photo14.jpg') }}" alt="...">
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">Dinna Handini</h4>
                jauh lebih baik daripada operasi! Tanpa bedah!!
            </div>
        </div>

    </div>
    <div class="clearfix"></div>
</div>
</body>
</html>