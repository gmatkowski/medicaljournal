@extends('layout.app', ['title' => $product3->name.' Checkout'])

@section('styles')
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ elixir('css/hallupro-checkout/all.css') }}">
@endsection

@section('trackConversions')
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-KRCHZ7X');</script>
    <!-- End Google Tag Manager -->
@endsection

@section('pixel')
    @include('pixel.hallupro_lead')
@endsection

@section('content')
    <header>
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">Dr.Hallux</a>
                </div>
                <div class="links"><p class="lock">JAMINAN PRODUK ASLI</p><p><span>&copy;</span>TRANSAKSI PEMBELIAN AMAN</p></div>
            </div>
        </nav>
    </header>
    <main>
        <div class="container-fluid">
            <div class="row">
                {!! form_start($form) !!}
                    <div class="col-md-9 col-xs-12">
                        <div class="usps">
                            <p class="check">Turun 12 kg dalam 5 minggu</p>
                            <p class="check reverted">Dengan {{ $product3->name }} berat badan Anda akan turun
                                hingga 12 kg hanya dalam 5 minggu. Selain itu, tubuh anda
                                akan bersih dari racun dan meningkatkan energi tubuh Anda.</p>
                        </div>
                        <div class="info">
                            <p>Pengiriman gratis! Pesan {{ $product3->name }} sekarang juga dan dapatkan
                                pengiriman gratis dengan harga 0 {{ $product3->currency }}</p>
                        </div>
                        <div class="checkout-steps">
                            <div id="step1" class="step">
                                <ul class="steps-list">
                                    <li class="current">PILIH PAKET</li>
                                    <li>FORMULIR PEMESANAN</li>
                                    <li>PEMBAYARAN</li>
                                    <li>KONFIRMASI PESANAN</li>
                                </ul>
                                <ul class="options">
                                    <li>
                                        <input type="radio" name="iCheck" value="1.960.000/980.000/TURUN SECARA MAKSIMAL" id="3">
                                        <label for="">
                                            <div class="photo">
                                                <img src="{{ asset('build/images/hallupro-checkout/5-promo.png') }}" alt="">
                                                <strong>3 Gratis</strong>
                                            </div>
                                            <div class="desc">
                                                <h3>PAKET TURUN SECARA MAKSIMAL</h3>
                                                <p>3 bulan terapi</p>
                                                <p>3 botol {{ $product3->name }} + 3 gratis</p>
                                                <p><strong>1.960.000 {{ $product3->currency }}</strong> untuk 90 hari terapi (total 180 pil)</p>
                                                <p class="notice">
                                                    Harganya lebih murah <strong>326.667 {{ $product3->currency }}</strong> per 1 botol
                                                    Anda menghemat uang = <strong>980.000 {{ $product3->currency }}!</strong>
                                                </p>
                                                <p>+ 0 {{ $product3->currency }} untuk <strong>pengiriman gratis!</strong></p>
                                            </div>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="radio" name="iCheck" value="1.250.000/220.000/OPTIMAL" id="2" checked>
                                        <label for="">
                                            <div class="photo">
                                                <img src="{{ asset('build/images/hallupro-checkout/3-promo.png') }}" alt="">
                                                <strong>1 Gratis</strong>
                                            </div>
                                            <div class="desc">
                                                <h3>PAKET OPTIMAL</h3>
                                                <p>45 hari terapi</p>
                                                <p>2 botol {{ $product3->name }} + 1 gratis</p>
                                                <p><strong>1.250.000 {{ $product3->currency }} </strong> untuk 45 hari terapi (total 90 pil)</p>
                                                <p class="notice">
                                                    Harganya lebih murah <strong>416.667 {{ $product3->currency }}</strong> per 1 botol
                                                    Anda menghemat uang = <strong>220.000 {{ $product3->currency }}!</strong>
                                                </p>
                                                <p>+ 0 {{ $product3->currency }} untuk <strong>pengiriman gratis!</strong></p>
                                            </div>
                                        </label>
                                    </li>
                                    <li>
                                        <input type="radio" value="{!! StrHelper::dotsInPrice($product3->price) !!}/0/START" name="iCheck" id="1">
                                        <label for="">
                                            <div class="photo">
                                                <img src="{{ asset('build/images/hallupro-checkout/product.png') }}" alt="">
                                                <strong>3 Gratis</strong>
                                            </div>
                                            <div class="desc">
                                                <h3>PAKET START</h3>
                                                <p>15 hari terapi</p>
                                                <p>1 botol {{ $product3->name }}</p>
                                                <p><strong>490.000 {{ $product3->currency }}</strong> untuk 15 hari terapi (total 30 pil)</p>
                                                <p>+ 0 {{ $product3->currency }} untuk <strong>pengiriman gratis!</strong></p>
                                            </div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div id="step2" class="step">
                                <ul class="steps-list">
                                    <li>PILIH PAKET</li>
                                    <li class="current">FORMULIR PEMESANAN</li>
                                    <li>PEMBAYARAN</li>
                                    <li>KONFIRMASI PESANAN</li>
                                </ul>
                                {!! form_errors($form->email) !!}
                                {!! form_errors($form->first_name) !!}
                                {!! form_errors($form->surname) !!}
                                {!! form_errors($form->address) !!}
                                {!! form_errors($form->district) !!}
                                {!! form_errors($form->subdistrict) !!}
                                {!! form_errors($form->postcode) !!}
                                <ul>
                                    <li>{!! form_widget($form->email) !!}</li>
                                    <li class="half">{!! form_widget($form->first_name) !!}</li>
                                    <li class="half">{!! form_widget($form->surname) !!}</li>
                                    <li>{!! form_widget($form->address) !!}</li>
                                    <li>{!! form_widget($form->district) !!}</li>
                                    <li>{!! form_widget($form->subdistrict) !!}</li>
                                    <li class="half">{!! form_widget($form->postcode) !!}</li>
                                    <li class="half">{!! form_widget($form->city) !!}</li>
                                    <li class="half">{!! form_widget($form->country) !!}</li>
                                </ul>
                            </div>
                            <div id="step3" class="step">
                                <ul class="steps-list">
                                    <li>PILIH PAKET</li>
                                    <li>FORMULIR PEMESANAN</li>
                                    <li class="current">PEMBAYARAN</li>
                                    <li>KONFIRMASI PESANAN</li>
                                </ul>
                                <div id="shipping">
                                    <h2>TRANSFER LANGSUNG</h2>
                                    <div class="wrapper">
                                        <div class="col">
                                            <h3>shipping method</h3>
                                            <ul>
                                                <li><label for="">Payment online</label>{!! Form::radio('pembayaran', 'Veritrans') !!}</li>
                                                <li><label for="">Cash on Delivery</label>{!! Form::radio('pembayaran', 'COD', true) !!}</li>
                                            </ul>
                                        </div>
                                        <div class="col">
                                            <h3>check your order</h3>
                                            <div class="product-block">
                                                <div class="photo"><img src="{{ asset('build/images/hallupro-checkout/product.png') }}" alt=""></div>
                                                <p class="desc">
                                                    <small>{{ $product3->name }}</small>
                                                    <input type="text" class="qty" value="3" disabled>
                                                    x
                                                    <span>490.000</span> {{ $product3->currency }}
                                                </p>
                                            </div>
                                            <p class="promo">
                                                promo
                                                <span class="pricePromo">220.000</span> {{ $product3->currency }}
                                            </p>
                                            <p class="promo">
                                                total
                                                <span class="priceTotal">1.250.000</span> {{ $product3->currency }}
                                            </p>

                                            <a href="#confirmation" class="btn">Payment</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="step4" class="step">
                                <ul class="steps-list">
                                    <li>PILIH PAKET</li>
                                    <li>FORMULIR PEMESANAN</li>
                                    <li>PEMBAYARAN</li>
                                    <li class="current">KONFIRMASI PESANAN</li>
                                </ul>
                                <div id="confirmation">
                                    <div class="col">
                                        <span class="check"></span>
                                    </div>
                                    <div class="col">
                                        <p><span>Pesanan : </span>PAKET OPTIMAL</p>
                                        <p><span>Ongkos Kirim :</span>FREE</p>
                                        <p><span>Yang Harus Dibayar : </span>1.250.000 {{ $product3->currency }}</p>
                                        <img src="{{ asset('build/images/hallupro-checkout/payment.png') }}" alt="">
                                        <p class="lock">We care about the privacy of your data</p>
                                        {!! form_widget($form->price) !!}
                                        {!! form_widget($form->qty) !!}
                                        <button type="submit" onclick="this.disabled=true;this.value='Sending, please wait...';this.form.submit();" class="btn">PESAN SEKARANG</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {!! form_end($form, false) !!}
                <aside class="col-md-3 col-xs-12">
                    <div class="box man">
                        <h3>Aman</h3>
                        <p>Garfir {{ $product3->name }} 100% lerbuat dari bahan-bahan alami. Pemakaian {{ $product3->name }} aman dan alami,lanpa efek samping apapun.</p>
                    </div>
                    <div class="box lock">
                        <h3>KERAHASIAAN</h3>
                        <p>Kami menghormati privasi Anda - kami lidak menjual, menyewakan atau mengungkapkan data pribadi Anda kepada pihak keliga.</p>
                    </div>
                    <div class="box hammer">
                        <h3>KEBIJAKAN</h3>
                        <p>Yakinlah bahwa produk Anda akan dikirimkan dalam paket lanpa tanda identifikasi alau pelabelan, hanya melalui kurir, langsung kepada Anda.</p>
                    </div>
                    <div class="box smile">
                        <h3>JAMINAN KEPUASAN</h3>
                        <p>{{ $product3->name }} berhasil membantu 94,5% orang. Efeldivitasnya telah terbukti secara klinis.</p>
                    </div>
                </aside>
            </div>
        </div>
    </main>
@endsection

@section('endBodyScripts')
    <script type="text/javascript">
        $("[name='iCheck']").change(function () {
            var qty = $(this).attr('id');
            var value = $(this).val();
            var paket = value.split('/');
            $('.qty').each(function() {
                switch (qty) {
                    case '2':
                        $(this).val(parseInt(qty)+1);
                        break;
                    case '3':
                        $(this).val(parseInt(qty)+3);
                        break;
                    default:
                        $(this).val(qty);
                }
            });
            $('.pricePromo').html(paket[1]);
            $('.priceTotal').each(function() {
                $(this).html(paket[0]);
                $(this).val(paket[0]);
            });
            $('#paketForCli').html(paket[2]);
            $('#priceForCli').html(paket[0]);
        });
    </script>
@endsection