<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="google-site-verification" content="fcU_d2EKLQOPuxoc0nl8-PAmAzeOt2rE88hhYR5ljUY">
    <meta name="robots" content="noindex, nofollow">

    <link rel="shortcut icon" href="/layout/1/images/medicreporter/favicon.png">

    <title></title>

    <link rel="stylesheet" href="{{ elixir('css/prehallupro/all.css') }}">

    <style>
        article.main {max-width: 746px;}
        .page-container {max-width: 1072px;}
        div.link-box p:nth-of-type(1) {font-family: 'open_sansbold';}
        article div.footer {margin-bottom: 22px;}
        body.hallu-pro-v2 div.page-view div.left-side article.main div.body p b
        {
            font-size: 13px;
        }
    </style>
    <style type="text/css">
        .fb_hidden{position:absolute;top:-10000px;z-index:10001}.fb_reposition{overflow:hidden;position:relative}.fb_invisible{display:none}.fb_reset{background:none;border:0;border-spacing:0;color:#000;cursor:auto;direction:ltr;font-family:"lucida grande", tahoma, verdana, arial, sans-serif;font-size:11px;font-style:normal;font-variant:normal;font-weight:normal;letter-spacing:normal;line-height:1;margin:0;overflow:visible;padding:0;text-align:left;text-decoration:none;text-indent:0;text-shadow:none;text-transform:none;visibility:visible;white-space:normal;word-spacing:normal}.fb_reset>div{overflow:hidden}.fb_link img{border:none}@keyframes fb_transform{from{opacity:0;transform:scale(.95)}to{opacity:1;transform:scale(1)}}.fb_animate{animation:fb_transform .3s forwards}
        .fb_dialog{background:rgba(82, 82, 82, .7);position:absolute;top:-10000px;z-index:10001}.fb_reset .fb_dialog_legacy{overflow:visible}.fb_dialog_advanced{padding:10px;-moz-border-radius:8px;-webkit-border-radius:8px;border-radius:8px}.fb_dialog_content{background:#fff;color:#333}.fb_dialog_close_icon{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 0 transparent;_background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yL/r/s816eWC-2sl.gif);cursor:pointer;display:block;height:15px;position:absolute;right:18px;top:17px;width:15px}.fb_dialog_mobile .fb_dialog_close_icon{top:5px;left:5px;right:auto}.fb_dialog_padding{background-color:transparent;position:absolute;width:1px;z-index:-1}.fb_dialog_close_icon:hover{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -15px transparent;_background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yL/r/s816eWC-2sl.gif)}.fb_dialog_close_icon:active{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yq/r/IE9JII6Z1Ys.png) no-repeat scroll 0 -30px transparent;_background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yL/r/s816eWC-2sl.gif)}.fb_dialog_loader{background-color:#f6f7f9;border:1px solid #606060;font-size:24px;padding:20px}.fb_dialog_top_left,.fb_dialog_top_right,.fb_dialog_bottom_left,.fb_dialog_bottom_right{height:10px;width:10px;overflow:hidden;position:absolute}.fb_dialog_top_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 0;left:-10px;top:-10px}.fb_dialog_top_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -10px;right:-10px;top:-10px}.fb_dialog_bottom_left{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -20px;bottom:-10px;left:-10px}.fb_dialog_bottom_right{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ye/r/8YeTNIlTZjm.png) no-repeat 0 -30px;right:-10px;bottom:-10px}.fb_dialog_vert_left,.fb_dialog_vert_right,.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{position:absolute;background:#525252;filter:alpha(opacity=70);opacity:.7}.fb_dialog_vert_left,.fb_dialog_vert_right{width:10px;height:100%}.fb_dialog_vert_left{margin-left:-10px}.fb_dialog_vert_right{right:0;margin-right:-10px}.fb_dialog_horiz_top,.fb_dialog_horiz_bottom{width:100%;height:10px}.fb_dialog_horiz_top{margin-top:-10px}.fb_dialog_horiz_bottom{bottom:0;margin-bottom:-10px}.fb_dialog_iframe{line-height:0}.fb_dialog_content .dialog_title{background:#6d84b4;border:1px solid #365899;color:#fff;font-size:14px;font-weight:bold;margin:0}.fb_dialog_content .dialog_title>span{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/yd/r/Cou7n-nqK52.gif) no-repeat 5px 50%;float:left;padding:5px 0 7px 26px}body.fb_hidden{-webkit-transform:none;height:100%;margin:0;overflow:visible;position:absolute;top:-10000px;left:0;width:100%}.fb_dialog.fb_dialog_mobile.loading{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/ya/r/3rhSv5V8j3o.gif) white no-repeat 50% 50%;min-height:100%;min-width:100%;overflow:hidden;position:absolute;top:0;z-index:10001}.fb_dialog.fb_dialog_mobile.loading.centered{width:auto;height:auto;min-height:initial;min-width:initial;background:none}.fb_dialog.fb_dialog_mobile.loading.centered #fb_dialog_loader_spinner{width:100%}.fb_dialog.fb_dialog_mobile.loading.centered .fb_dialog_content{background:none}.loading.centered #fb_dialog_loader_close{color:#fff;display:block;padding-top:20px;clear:both;font-size:18px}#fb-root #fb_dialog_ipad_overlay{background:rgba(0, 0, 0, .45);position:absolute;bottom:0;left:0;right:0;top:0;width:100%;min-height:100%;z-index:10000}#fb-root #fb_dialog_ipad_overlay.hidden{display:none}.fb_dialog.fb_dialog_mobile.loading iframe{visibility:hidden}.fb_dialog_content .dialog_header{-webkit-box-shadow:white 0 1px 1px -1px inset;background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#738ABA), to(#2C4987));border-bottom:1px solid;border-color:#1d4088;color:#fff;font:14px Helvetica, sans-serif;font-weight:bold;text-overflow:ellipsis;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0;vertical-align:middle;white-space:nowrap}.fb_dialog_content .dialog_header table{-webkit-font-smoothing:subpixel-antialiased;height:43px;width:100%}.fb_dialog_content .dialog_header td.header_left{font-size:12px;padding-left:5px;vertical-align:middle;width:60px}.fb_dialog_content .dialog_header td.header_right{font-size:12px;padding-right:5px;vertical-align:middle;width:60px}.fb_dialog_content .touchable_button{background:-webkit-gradient(linear, 0% 0%, 0% 100%, from(#4966A6), color-stop(.5, #355492), to(#2A4887));border:1px solid #29487d;-webkit-background-clip:padding-box;-webkit-border-radius:3px;-webkit-box-shadow:rgba(0, 0, 0, .117188) 0 1px 1px inset, rgba(255, 255, 255, .167969) 0 1px 0;display:inline-block;margin-top:3px;max-width:85px;line-height:18px;padding:4px 12px;position:relative}.fb_dialog_content .dialog_header .touchable_button input{border:none;background:none;color:#fff;font:12px Helvetica, sans-serif;font-weight:bold;margin:2px -12px;padding:2px 6px 3px 6px;text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog_content .dialog_header .header_center{color:#fff;font-size:16px;font-weight:bold;line-height:18px;text-align:center;vertical-align:middle}.fb_dialog_content .dialog_content{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat 50% 50%;border:1px solid #555;border-bottom:0;border-top:0;height:150px}.fb_dialog_content .dialog_footer{background:#f6f7f9;border:1px solid #555;border-top-color:#ccc;height:40px}#fb_dialog_loader_close{float:left}.fb_dialog.fb_dialog_mobile .fb_dialog_close_button{text-shadow:rgba(0, 30, 84, .296875) 0 -1px 0}.fb_dialog.fb_dialog_mobile .fb_dialog_close_icon{visibility:hidden}#fb_dialog_loader_spinner{animation:rotateSpinner 1.2s linear infinite;background-color:transparent;background-image:url(https://static.xx.fbcdn.net/rsrc.php/v3/yD/r/t-wz8gw1xG1.png);background-repeat:no-repeat;background-position:50% 50%;height:24px;width:24px}@keyframes rotateSpinner{0%{transform:rotate(0deg)}100%{transform:rotate(360deg)}}
        .fb_iframe_widget{display:inline-block;position:relative}.fb_iframe_widget span{display:inline-block;position:relative;text-align:justify}.fb_iframe_widget iframe{position:absolute}.fb_iframe_widget_fluid_desktop,.fb_iframe_widget_fluid_desktop span,.fb_iframe_widget_fluid_desktop iframe{max-width:100%}.fb_iframe_widget_fluid_desktop iframe{min-width:220px;position:relative}.fb_iframe_widget_lift{z-index:1}.fb_hide_iframes iframe{position:relative;left:-10000px}.fb_iframe_widget_loader{position:relative;display:inline-block}.fb_iframe_widget_fluid{display:inline}.fb_iframe_widget_fluid span{width:100%}.fb_iframe_widget_loader iframe{min-height:32px;z-index:2;zoom:1}.fb_iframe_widget_loader .FB_Loader{background:url(https://static.xx.fbcdn.net/rsrc.php/v3/y9/r/jKEcVPZFk-2.gif) no-repeat;height:32px;width:32px;margin-left:-16px;position:absolute;left:50%;z-index:4}
    </style>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-85228073-6', 'auto');
        ga('send', 'pageview');

    </script>
    @include('part.chat')
</head>
<body class="hallu hallu-pro-v2 subpage">
<div id="fb-root" class=" fb_reset">
    <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
        <div>
            <iframe name="fb_xdm_frame_http" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" id="fb_xdm_frame_http" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="http://staticxx.facebook.com/connect/xd_arbiter/r/iPrOY23SGAp.js?version=42#channel=f3aff330222f36c&amp;origin=http%3A%2F%2Fwww.medicreporters.com" style="border: none;"></iframe>
            <iframe name="fb_xdm_frame_https" frameborder="0" allowtransparency="true" allowfullscreen="true" scrolling="no" id="fb_xdm_frame_https" aria-hidden="true" title="Facebook Cross Domain Communication Frame" tabindex="-1" src="https://staticxx.facebook.com/connect/xd_arbiter/r/iPrOY23SGAp.js?version=42#channel=f3aff330222f36c&amp;origin=http%3A%2F%2Fwww.medicreporters.com" style="border: none;"></iframe>
        </div>
    </div>
    <div style="position: absolute; top: -10000px; height: 0px; width: 0px;">
        <div></div>
    </div>
</div>

<div class="page-container">

    <header id="main-header">
        <h1 class="logo">
            <a href="#url"><img src="{{asset('build/images/frontend/fi6WL4S/logo2.png')}}" alt="Medic Reporters"></a>
        </h1>
    </header>

    <ol class="breadcrumb">
    </ol>
    <div class="page-view">
        <h1>Revolusi dalam pengobatan bunion.</h1>
        <p class="data-publish">Hasil obat: 17.12.2016</p>
        <div class="left-side">
            <div class="banner">
                <header>
                    <figure>
                        <img src="{{ asset('build/images/frontend/hallu-banner.jpg') }}" alt="Efekt preparatu">
                    </figure>
                </header>
            </div>
            <article class="main">
                <div class="body">
                    <p><b style="font-size: 18px; line-height: 25px;">Jari bengkok, masalah bunion, rasa nyeri yang berulang-ulang dll. – hampir 17 ribu wanita Indonesia berhasil mengatasi masalah tersebut dengan mudah. Tanpa operasi, tanpa bedah, tanpa dokter, tanpa meninggalkan rumah. Para dokter spesialis ortopedi setuju: cara baru yang ditemukan oleh Prof. Gardner adalah temuan revolusioner dalam ilmu pengobatan dan pemeliharaan tapak kaki. Apakah tidak akan ada lagi semua masalah kaki?</b></p>
                    <p>Penelitian terbaru menunjukkan bahwa lebih dari 212 ribu wanita Indonesia berhasil mengatasi masalah bunion dan mendapatkan kembali kaki indah dengan cara baru dan inovatif. 86% wanita tersebut berhasil mengatasinya hanya dalam 2 minggu. Ini bukan sihir, ini hasil nyata: obat alami yang ditemukan oleh Dr. Prof. Gardner, seorang dokter spesialis dalam kedokteran podiatrik dari Amerika, menjadi semakin laris manis di seluruh dunia. Apa fenomena cara baru tersebut?</p>
                    <p>Satu hal yang pasti: untuk mengatasi masalah kaki dan mengobati bunion Anda harus mengatasi sumber masalah dengan tuntas. Selain mendapatkan kembali kaki indah dan sehat, dengan meningkatkan kinerja tulang dan sendi kaki secara aktif dan intensif, Anda dapat mengatasi masalah bunion secara permanen. Setelah diteliti secara mendalam, baik melalui penelitian lapangan maupun penelitian eksperimen, terbukti bahwa inilah terapi baru dan inovatif untuk mengatasi masalah bunion, nyeri, peradangan dan pembengkakan hanya dalam 48 jam. Obat tersebut ialah cara ampuh untuk meningkatkan aliran darah dan oksigen dalam kaki, dan memperkuat otot kaki – yaitu membantu Anda mendapatkan kembali kaki indah dan sehat tanpa bunion dengan mudah.</p>
                    <p>Inilah cara ampuh yang ditawarkan oleh dunia ilmu pengetahuan dan teknologi modern untuk mengatasi bunion tanpa operasi, tanpa bedah, tanpa meninggalkan rumah – cara efektif dan aman, mudah dan murah. Sekarang Anda bisa mengatasi bunion secara permanen dan selamanya.</p>
                    <p>Para dokter spesialis dalam kedokteran podiatrik menekankan bahwa rahasia kesuksesan terapi baru tersebut hanya ada satu bahkan sangat unik. Apa ini, terapi maju dengan bantalan bunion, dibuat oleh seorang ahli dalam kedokteran podiatrik dan rehabilitasi atlet dari cedera olahraga – Peter Gardner?</p>
                    <p>Apa yang menarik tentang alat inovatif tersebut adalah bahwa teknologinya biomekanik, sehingga memenuhi semua jenis sepatu. Anda dapat memakai bantalan bunion tersebut dengan nyaman setiap hari, karena konstruksinya aman dan biomekanik.</p>
                    <p>Apa yang penting dari alat tersebut adalah bahwa cara kerjanya bersifat multitasking, yaitu membantu mengobati masalah sendi dan tulang yang serius, mencegah peradangan, menghilangkan kapalan dan lain-lain. Artinya, <b>Anda dapat mengatasi hampir semua masalah kaki, yaitu bunion, pembengkakan dan jari bengkok – tanpa dokter.</b></p>
                    <p>Atasi dengan cara sederhana ini. Di Indonesia masih banyak wanita yang tidak menyadari bahwa mereka dapat memerangi masalah kaki dan <b>mendapatkan kembali kaki indah dan sehat tanpa meninggalkan rumah, dengan mudah dan cepat</b>. Namun, metode Gardner yang semakin laris membawa harapan besar bagi wanita Indonesia – akhirnya wanita di Tanah Air dapat sembuh dari masalah kaki secara revolusioner.</p>
                    <p>Awas alat palsu – para ahli memperingatkan bahwa di pasaran muncul banyak alat palsu yang murah dan meniru produk asli, yaitu bantalan bunion Prof. Gardner. Para ahli ortopedi merekomendasikan bantalan bunion prof. Gardner, karena konstruksinya unik dan biomekanik, diformulasikan dengan menggunakan bahan-bahan ortopedi berkualitas tinggi – yang sulit didapatkan. Tentu saja, Anda bisa mencoba mencari produk lain, tapi jangan lupa bahwa mungkin Anda tidak dapat memuaskan dengan hasil produk palsu tersebut.</p>
                    <p style="float: right;">Semoga sukses!</p>
                </div>
            </article>
            <aside>
                <aside class="hidden-xs">
                    <a href="http://www.accuweather.com/id/id/jakarta/208971/weather-forecast/208971" class="aw-widget-legal">
                        <!--
                        By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
                        -->
                    </a>
                    <div id="awcc1472489642912" class="aw-widget-current" data-locationkey="208971" data-unit="c" data-language="id" data-useip="false" data-uid="awcc1472489642912"></div>
                    <script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
                </aside>
            </aside>
        </div>
    </div>

</div>

<footer id="main-footer">
    <div class="page-container page-container-footer">
        <a href="#url"><img class="footer-logo" src="{{asset('build/images/frontend/fi6WL4S/logo3.png')}}" alt="Medic Reporters"></a>
    </div>
</footer>
<div id="popup" style="display: none;">
    <div class="window">
        <h1>Dziękujemy!</h1>
        <p>Twój komentarz został przesłany do moderacji.</p>
        <button class="close" onclick="$('#popup').hide();">ZAMKNIJ</button>
    </div>
</div>

</body>
</html>