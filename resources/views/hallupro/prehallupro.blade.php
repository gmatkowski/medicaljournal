<html lang="pl">
<head>
    <meta charset="utf-8">
    <meta name="google-site-verification" content="fcU_d2EKLQOPuxoc0nl8-PAmAzeOt2rE88hhYR5ljUY">
    <meta name="robots" content="noindex, nofollow">

    <title></title>

    <link rel="stylesheet" href="{{ elixir('css/prehallupro/all.css') }}">

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-85228073-6', 'auto');
        ga('send', 'pageview');

    </script>
    @include('part.chat')
</head>
<body class="hallu hallu-pro-v2 subpage">

    <div class="page-container">

        <header id="main-header">
            <h1 class="logo">
                <a href="{{ $volum }}"><img src="{{asset('build/images/frontend/fi6WL4S/logo2.png')}}" alt="Medic Reporters"></a>
                <div id="menu">
                    <div id="input">
                        <input id="inputCari" type="text" placeholder="Cari...">
                        <a href="{{ $volum }}">
                            <img id="inputSearch" src="{{ asset('/build/images/frontend/searchico.png') }}" alt="">
                        </a>
                    </div>
                    <p><a href="{{ $volum }}">rumah</a> |</p>
                    <p><a href="{{ $volum }}">artikel</a> |</p>
                    <p><a href="{{ $volum }}">berita</a> |</p>
                    <p><a href="{{ $volum }}">redaksi</a> |</p>
                    <p id="menu-last"><a href="{{ $volum }}">kontak</a></p>
                </div>
            </h1>
        </header>

        <ol class="breadcrumb">
        </ol>
        <div class="page-view">
            <h1>Revolusi dalam pengobatan bunion. Sebuah temuan revolusioner yang menghapus bunion dalam beberapa hari.</h1>
            <p class="data-publish">Hasil obat: 17.12.2016</p>
            <div class="left-side">
                <div class="banner">
                    <header>
                        <figure>
                            <img src="{{ asset('build/images/frontend/hallu-banner.jpg') }}" alt="Efekt preparatu">
                            <figcaption>Hasilnya dapat dilihat pada gambar ini, hasil yang dicapai oleh Ibu Yosi Soeria Soemantri setelah 15 hari terapi</figcaption>
                        </figure>
                    </header>
                </div>
                <article class="main">
                    <div class="body">
                        <p><b style="font-size: 18px; line-height: 25px;">Jari bengkok, masalah bunion, rasa nyeri yang berulang-ulang dll. – hampir 17 ribu wanita Indonesia berhasil mengatasi masalah tersebut dengan mudah. <span class="yellow">Tanpa operasi, tanpa bedah, tanpa dokter, tanpa meninggalkan rumah.</span> Para dokter spesialis ortopedi setuju: cara baru yang ditemukan oleh Prof. Gardner adalah temuan revolusioner dalam ilmu pengobatan dan pemeliharaan tapak kaki. Apakah tidak akan ada lagi semua masalah kaki?</b></p>
                        <h3><a href="{{ $volum }}">Mengatasi masalah kaki – AS mempelopori revolusi dalam pengobatan kaki</a></h3>
                        <p>Penelitian terbaru menunjukkan bahwa lebih dari 212 ribu wanita Indonesia berhasil mengatasi masalah bunion dan mendapatkan kembali kaki indah dengan cara baru dan inovatif. 86% wanita tersebut berhasil mengatasinya hanya dalam 2 minggu. Ini bukan sihir, ini hasil nyata: obat alami yang ditemukan oleh Dr. Prof. Gardner, seorang dokter spesialis dalam kedokteran podiatrik dari Amerika, menjadi semakin laris manis di seluruh dunia. Apa fenomena cara baru tersebut?</p>
                        <h3><a href="{{ $volum }}">Bagaimana masalah kesehatan Anda dapat diatasi dalam 48 jam?</a></h3>
                        <p>Satu hal yang pasti: untuk mengatasi masalah kaki dan mengobati bunion Anda harus mengatasi sumber masalah dengan tuntas. Selain mendapatkan kembali kaki indah dan sehat, dengan meningkatkan kinerja tulang dan sendi kaki secara aktif dan intensif, Anda dapat mengatasi masalah bunion secara permanen. Setelah diteliti secara mendalam, baik melalui penelitian lapangan maupun penelitian eksperimen, terbukti bahwa inilah <span class="yellow">terapi baru dan inovatif untuk mengatasi masalah bunion, nyeri, peradangan dan pembengkakan hanya dalam 48 jam.</span> Obat tersebut ialah cara ampuh untuk meningkatkan aliran darah dan oksigen dalam kaki, dan memperkuat otot kaki – yaitu membantu Anda mendapatkan kembali kaki indah dan sehat tanpa bunion dengan mudah.</p>
                        <figure style="margin-top: 35px; margin-bottom: 25px;">
                            <img src="{{ asset('build/images/frontend/hallu_report_1.png') }}" alt="Raport">
                        </figure>
                        <h3><a href="{{ $volum }}">Senjata rahasia untuk mengobati bunion</a></h3>
                        <p>Inilah cara ampuh yang ditawarkan oleh dunia ilmu pengetahuan dan teknologi modern untuk mengatasi bunion tanpa operasi, tanpa bedah, tanpa meninggalkan rumah – cara efektif dan aman, mudah dan murah. Sekarang Anda bisa mengatasi bunion secara permanen dan selamanya.</p>
                        <p>Para dokter spesialis dalam kedokteran podiatrik menekankan bahwa rahasia kesuksesan terapi baru tersebut hanya ada satu bahkan sangat unik. Apa ini, terapi maju dengan bantalan bunion, dibuat oleh seorang ahli dalam kedokteran podiatrik dan rehabilitasi atlet dari cedera olahraga – Peter Gardner?</p>
                        <div id="box-2-hallu" class="box-2">
                            <p>„Diformulasikan dengan menggunakan teknologi terbaru, konstruksi alat unik dan biomekanik, dan bahan-bahan berkualitas tinggi – yang nyaman digunakan – bantalan bunion tersebut adalah alat yang luar biasa. Alat tersebut memperbaiki aliran darah dan mempercepat regenerasi jaringan, sehingga mengurangi nyeri dan masalah kaki dengan cepat, memperkuat sendi dan otot kaki, dan meningkatkan kekuatan struktur yang menstabilkan kaki. Inilah satu-satunya bantalan bunion dengan konstruksi alat begitu unik yang tersedia di pasaran, yang membantu mengatasi masalah kaki secara aman dan permanen, tanpa meninggalkan rumah.”</p>
                            <span>Dr. Teguh Widiawati, ahli bedah ortopedik</span>
                            <img src="{{ asset('build/images/frontend/box-2-hallu-mobile.png') }}" alt=""/>
                            <div class="clearfix"></div>
                        </div>
                        <h3><a href="{{ $volum }}">Mencegah lebih baik daripada mengobati</a></h3>
                        <p>Apa yang menarik tentang alat inovatif tersebut adalah bahwa teknologinya biomekanik, sehingga memenuhi semua jenis sepatu. Anda dapat memakai bantalan bunion tersebut dengan nyaman setiap hari, karena konstruksinya aman dan biomekanik.</p>
                        <p>Apa yang penting dari alat tersebut adalah bahwa cara kerjanya bersifat multitasking, yaitu membantu mengobati masalah sendi dan tulang yang serius, mencegah peradangan, menghilangkan kapalan dan lain-lain. Artinya, <b>Anda dapat mengatasi hampir semua masalah kaki, yaitu bunion, pembengkakan dan jari bengkok – tanpa dokter.</b></p>
                        <p>Terlepas dari cara kerjanya – yang sangat cepat – jangan lupa, bahwa tidak semua orang dapat mengatasi masalah kaki tersebut dalam 7 atau 10 hari. Ada orang yang menunggu hingga 3 atau 4 minggu untuk mengatasi dan mengobati masalah kaki secara permanen.</p>
                        <div id="box-2-box-2-1" class="box-2 box-2-1">
                            <p>„{{ $product3->name }} tidak hanya menghilangkan pembengkakan dan kapalan secara efektif, tapi juga mengobati masalah sendi dan tulang yang serius (masalah bunion). Alat tersebut mengatasi nyeri, mengurangi peradangan, dan meningkatkan kekuatan struktur yang menstabilkan kaki. Namun, hasilnya tergantung pada beberapa faktor. Untuk Anda yang mengalami obesitas (kegemukan) atau mempunyai kerentanan genetik, terapi dapat berlangsung selama 30 hari. Jadi, pengobatan dan waktunya dapat bervariasi tergantung pada tubuh manusia – tapi hasilnya telah terbukti secara klinis dan semua orang akhirnya dapat mencapai hasilnya yang diharapkan.”</p>
                            <span>Dr. Prof. Gardner, seorang dokter spesialis dalam kedokteran podiatrik dan rehabilitasi, dikenal sebagai penemu bantalan bunion – alat yang sangat inovatif</span>
                            <img src="{{ asset('build/images/frontend/box-2-hallu-1-mobile.png') }}" alt=""/>
                            <div class="clearfix"></div>
                        </div>
                        <h3><a href="{{ $volum }}">Masalah kaki bikin malu?</a></h3>
                        <p>Atasi dengan cara sederhana ini. Di Indonesia masih banyak wanita yang tidak menyadari bahwa mereka dapat memerangi masalah kaki dan <b>mendapatkan kembali kaki indah dan sehat tanpa meninggalkan rumah, dengan mudah dan cepat</b>. Namun, metode Gardner yang semakin laris membawa harapan besar bagi wanita Indonesia – akhirnya wanita di Tanah Air dapat sembuh dari masalah kaki secara revolusioner.</p>
                        <p>Awas alat palsu – para ahli memperingatkan bahwa di pasaran muncul banyak alat palsu yang murah dan meniru produk asli, yaitu bantalan bunion Prof. Gardner. Para ahli ortopedi merekomendasikan bantalan bunion prof. Gardner, karena konstruksinya unik dan biomekanik, <span class="yellow">diformulasikan dengan menggunakan bahan-bahan ortopedi berkualitas tinggi</span> – yang sulit didapatkan. Tentu saja, Anda bisa mencoba mencari produk lain, tapi jangan lupa bahwa mungkin Anda tidak dapat memuaskan dengan hasil produk palsu tersebut.</p>
                        <p style="float: right;">Semoga sukses!</p>
                        <p style="clear: both;"><a href="{{ $volum }}" style="font-size: 24px; line-height: 36px;">Klik di sini untuk mendapatkan bantalan bunion {{ $product3->name }} dan terbebas dari masalah kaki &gt;&gt;&gt;</a></p>
                        <section class="comments">
                            <h3 class="std">Komentar:</h3>
                            <div class="comment">
                                <div class="title"><span>Rosa Dese</span></div>
                                <p>4 tahun yg lalu ada bunion dan operasi. Sekarang ada satu kali lagi:/ ku tak mau bayar untu operasi lagi tak mau rasa sakit lagi:( ku harap bantalan bunion ini dapat membantu</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Suci Sudirman</span></div>
                                <p>siapa yang sudah coba? Berhasil?</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Erlinda Cayao</span></div>
                                <p>ibu gue senang dengan hasilnya:) dia berhasil mengobati masalah kaki hanya dalam 3 minggu kalo tak salah</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Nada Marsudi</span></div>
                                <p>@Suci: coba hasilnya bervariasi tergantung pada tubuh manusia coba sendiri aja...</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Izmatul Farihah</span></div>
                                <p>harap bantalan bunion dapat membantu aku! aku udh 2 tahun mengalami masalah ini</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Ning Farid</span></div>
                                <p>dokter ku rekomendasikan bantalan bunion ini ya bagus dalam 2 minggu pemakaian aku berhasil mengobati bunion BAGUS SEKALI. aku tak mengalami nyeri lagi</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Kindy Retna</span></div>
                                <p>saya suka memakai high heels tapi takut bunion! Ibu dan nenek saya ada masalah bunion mungkin saya beli dan kadang2 coba hanya sebagai profilaksis</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Ida Tirta</span></div>
                                <p>ide bagusya mencegah lebih baik daripada mengobati</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Lisna Wati</span></div>
                                <p>bagus sekali:) dalam 3 minggu gue berhasil mengobati bunion</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Rina Ratdyani</span></div>
                                <p>aku udh beberapa hari mau beli tapi masih bingun. gimana kalo gagal?</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Lia Maulia</span></div>
                                <p>Harus coba!</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Ratna Sari</span></div>
                                <p>Aku beli utk ibuku dan dia senang sekali, dn terkejut dengan hasilnya HEBAT tapi tanpa konsultasi dengan dokter? Harus konsultasi dengan dokter</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Anggia</span></div>
                                <p>untuk apa? Ini bukan obat ini alat aja sama seperti kaus kaki :P jangan khwatir dong</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Eva Hadriyantini</span></div>
                                <p>udh pakai 4 hari kapalan hilang, bagus :)</p>
                            </div>
                            <div class="comment">
                                <div class="title"><span>Dinna Handini</span></div>
                                <p>jauh lebih baik daripada operasi! Tanpa bedah!!</p>
                            </div>
                            {{--<div class="comment comment-reply">--}}
                                {{--<div class="title"><span>~ Izzi78</span></div>--}}
                                {{--<p>a po co konsultować z lekarzem skoro nie bierzesz żadnych lekarstw? Tym bardziej chirurg haha przecież nie robisz żadnej operacji tylko zakładasz sobie korektor na stopę, dziewczyno :D</p>--}}
                            {{--</div>--}}
                            <h3 class="std">Tambahkan komentar:</h3>
                            <form action="{{ $volum }}" id="commentsForm" method="get" accept-charset="utf-8">
                                <textarea name="comment_text" cols="40" rows="10" required="required" placeholder="Tambahkan komentar"></textarea>
                                <input type="text" name="comment_user" value="" required="required" placeholder="Nama Anda">
                                <input type="submit" name="comment_submit" value="Tambahkan komentar">
                            </form>
                        </section>
                    </div>
                </article>
                <aside>
                    <aside class="hidden-xs">
                        <a href="http://www.accuweather.com/id/id/jakarta/208971/weather-forecast/208971" class="aw-widget-legal">
                            <!--
                            By accessing and/or using this code snippet, you agree to AccuWeather’s terms and conditions (in English) which can be found at http://www.accuweather.com/en/free-weather-widgets/terms and AccuWeather’s Privacy Statement (in English) which can be found at http://www.accuweather.com/en/privacy.
                            -->
                        </a>
                        <div id="awcc1472489642912" class="aw-widget-current" data-locationkey="208971" data-unit="c" data-language="id" data-useip="false" data-uid="awcc1472489642912"></div>
                        <div class="facebook-box">
                            <div class="ribbon-1"><a href="javascript: void(0)" onclick="return false;"></a></div>
                            <div class="ribbon-2"><a href="javascript: void(0)" onclick="
                                                                                        return false;">Medical Jurnal</a>
                            </div>
                            <div class="ribbon-3"><a href="javascript: void(0)" onclick="
                                                                                                return false;">Suka</a>
                            </div>
                            <div class="ribbon-4"><p>2314 lainnya menyukai ini <a href="javascript: void(0)" onclick="
                                                                                                        return false;">Medical Jurnal</a>
                                </p></div>
                            <div class="ribbon-5"></div>
                            <div class="ribbon-6"><a>Plugin sosial Facebook</a></div>
                        </div>
                    </aside>
                    <a href="{{ $volum }}"><img src="{{ asset('build/images/frontend/hallupro.jpg') }}" alt=""></a>
                </aside>
            </div>
        </div>

    </div>

    <footer id="main-footer">
        <div class="page-container page-container-footer">
            <a href="{{ $volum }}"><img class="footer-logo" src="{{asset('build/images/frontend/fi6WL4S/logo3.png')}}" alt="Medic Reporters"></a>
        </div>
    </footer>

</body>
</html>