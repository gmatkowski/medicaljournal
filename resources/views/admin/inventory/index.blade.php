@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

        <!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>
                            Product Stock Availability
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    @role(['admin', 'ccadmin'])
                        <form class="form-horizontal" action="{{ route('admin.inventory') }}" method="get">
                            <div class="input-prepend input-group pull-right" style="width:230px;">
                                            <span class="add-on input-group-addon"><i
                                                        class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="hidden" name="product" value="{{ $productSymbol }}"/>
                                <input type="text" style="width: 200px" name="range" id="reservation"
                                       class="form-control" value="{{ $range }}"/>
                            </div>
                            <input type="submit" class="btn btn-success pull-right" value="Show"/>
                        </form>
                        <br>
                    @endrole
                </div>
            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">DATE</th>
                        <th class="column-title">OPEN</th>
                        <th class="column-title">IN</th>
                        <th class="column-title">OUT</th>
                        <th class="column-title">EDIT</th>
                        <th class="column-title">CLOSE</th>
                    </tr>
                    </thead>

                    <tbody>
                        @foreach($stocks as $key => $stock)
                            <tr class="even pointer">
                                <td class=" ">{{ $stock->created_at }}</td>
                                <td class=" ">{{ $stock->open }}</td>
                                @if(Entrust::hasRole('admin'))
                                    {!! form_start($form) !!}
                                        <td class=" ">{!! form_widget($form->in, ['value' => $stock->in]) !!}</td>
                                        <td class=" ">{!! form_widget($form->out, ['value' => $stock->out]) !!}</td>
                                        <td class=" "> {!! form_row($form->submit,['attr' => ['class' => 'btn btn-success pull-left']]) !!}</td>
                                        {!! form_widget($form->productSymbol, ['value' => $productSymbol]) !!}
                                        {!! form_widget($form->id, ['value' => $stock->id]) !!}
                                    {!! form_end($form) !!}
                                @else
                                    <td class=" ">{{ $stock->in }}</td>
                                    <td class=" ">{{ $stock->out }}}</td>
                                @endif
                                <td class=" ">{{ $stock->close }}</td>
                            </tr>
                        @endforeach
                    </tbody>

                </table>

                <div class="text-right">
                    {!! $stocks->render() !!}
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection

@section('scripts')

    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#reservation').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <!-- /datepicker -->

@endsection
