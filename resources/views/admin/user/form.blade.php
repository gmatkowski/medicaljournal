@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.users') }} :: {{ $mode }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="x_panel">
		<div class="x_title">
			<h2>{{ trans('admin.menu.users') }}
				<small>{{ $mode }}</small>
			</h2>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			{!! form_start($form,['class' => 'form-horizontal form-label-left']) !!}

			<div class="form-group">
				{!! form_label($form->role,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->role,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->role) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->name,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->name,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->name) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->email,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->email,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->email) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->signature,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->signature,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->signature) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->password,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->password,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->password) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->password_confirmation,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->password_confirmation,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->password_confirmation) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->show_stock,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-sm-1">
					{!! form_widget($form->show_stock,['attr' => ['class' =>'form-control checkbox-inline']]) !!}
				</div>
			</div>

			<div class="ln_solid"></div>

			{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
			{!! form_end($form) !!}

		</div>
	</div>

</div>

@endsection
