@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.users') }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-6">
					<div class="x_title">
						<h2>{{ trans('admin.menu.users') }}
							<small>{{trans('admin.common.list')}}</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-sm-6 text-right">
					<a href="{{ route('admin.users.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> {{ trans('admin.users.add') }}</a>
				</div>
			</div>

			<div class="x_content">

				<table class="table table-striped responsive-utilities jambo_table">
					<thead>
					<tr class="headings">
						<th class="column-title">{{ trans('admin.common.id') }}</th>
						<th class="column-title">{{ trans('admin.common.name') }}</th>
						<th class="column-title">{{ trans('admin.users.signature') }}</th>
						<th class="column-title">{{ trans('admin.users.email') }}</th>
						<th class="column-title">{{ trans('admin.users.role') }}</th>
						<th class="column-title">{{ trans('admin.common.date') }}</th>
						<th class="column-title no-link last"><span class="nobr">{{ trans('admin.common.actions') }}</span>
						</th>
					</tr>
					</thead>

					<tbody>
					@foreach($users as $user)
						@role(['admin', 'css', 'consultant'])
							<tr class="even pointer">
								<td class=" ">{{ $user->id }}</td>
								<td class=" ">{{ $user->name }}</td>
								<td class=" ">{{ $user->signature }}</td>
								<td class=" ">{{ $user->email }}</td>
								<td class=" ">{{ $user->roles->first()->display_name }}</td>
								<td class=" ">{{ $user->created_at }}</td>
								<td class=" last">
									<a href="{{ route('admin.users.edit',['id' => $user->id]) }}">{{ trans('admin.common.edit') }}</a>
									<a href="{{ route('admin.users.delete',['id' => $user->id]) }}" class="confirm">{{ trans('admin.common.delete') }}</a>
								</td>
							</tr>
						@endrole
						@role('ccadmin')
							@if($user->roles->first()->name!='admin' && $user->roles->first()->name!='ccadmin')
								<tr class="even pointer">
									<td class=" ">{{ $user->id }}</td>
									<td class=" ">{{ $user->name }}</td>
									<td class=" ">{{ $user->signature }}</td>
									<td class=" ">{{ $user->email }}</td>
									<td class=" ">{{ $user->roles->first()->display_name }}</td>
									<td class=" ">{{ $user->created_at }}</td>
									<td class=" last">
										<a href="{{ route('admin.users.edit',['id' => $user->id]) }}">{{ trans('admin.common.edit') }}</a>
										<a href="{{ route('admin.users.delete',['id' => $user->id]) }}" class="confirm">{{ trans('admin.common.delete') }}</a>
									</td>
								</tr>
							@endif
						@endrole
					@endforeach
					</tbody>

				</table>

				<div class="text-right">
					{!! $users->render() !!}
				</div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>

</div>

@endsection
