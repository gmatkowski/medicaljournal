@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.products') }} :: {{ $mode }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="x_panel">
		<div class="x_title">
			<h2>{{ trans('admin.menu.products') }}
				<small>{{ $mode }}</small>
			</h2>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			{!! form_start($form,['class' => 'form-horizontal form-label-left']) !!}

			<div class="" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
					@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
						<li role="presentation" @if($localeCode == LaravelLocalization::getCurrentLocale()) class="active" @endif >
							<a href="#lang_{{ $localeCode }}" id="lang-{{ $localeCode }}" role="tab" data-toggle="tab" aria-expanded="true">{{ $properties['name'] }}</a>
						</li>
					@endforeach
				</ul>
				<div id="myTabContent" class="tab-content">
					@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
						<div role="tabpanel" class="tab-pane fade @if($localeCode == LaravelLocalization::getCurrentLocale()) active in @endif " id="lang_{{ $localeCode }}" aria-labelledby="lang-{{ $localeCode }}">

							<div class="form-group">
								<label for="title" class="control-label">{{ trans('admin.common.name') }}</label>
								{!! Form::input('text', 'translation[' . $localeCode . '][name]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->name : ''), ['class' => 'form-control']) !!}
							</div>

							<div class="form-group">
								<label for="title" class="control-label">{{ trans('admin.products.short') }}</label>
								{!! Form::input('text', 'translation[' . $localeCode . '][name_short]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->name_short : ''), ['class' => 'form-control']) !!}
							</div>

							<div class="form-group">
								<label for="content" class="control-label">{{ trans('admin.products.shortdesc') }}</label>
								{!! Form::textarea('translation[' . $localeCode . '][description_short]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->description_short : ''), ['class' => 'form-control col-sm-3']) !!}
							</div>

							<div class="form-group">
								<label for="content" class="control-label">{{ trans('admin.products.trialdesc') }}</label>
								{!! Form::textarea('translation[' . $localeCode . '][description_try]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->description_try : ''), ['class' => 'form-control col-sm-3']) !!}
							</div>

							<div class="form-group">
								<label for="content" class="control-label">{{ trans('admin.common.price') }}</label>
								{!! Form::input('text','translation[' . $localeCode . '][price]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->price : ''), ['class' => 'form-control col-sm-3']) !!}
							</div>

							<div class="form-group">
								<label for="content" class="control-label">{{ trans('admin.products.old') }}</label>
								{!! Form::input('text','translation[' . $localeCode . '][price_old]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->price_old : ''), ['class' => 'form-control col-sm-3']) !!}
							</div>

							<div class="form-group">

								<label for="content" class="control-label">Special price (popup on exit)</label>
								{!! Form::input('text','translation[' . $localeCode . '][price_spec]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->price_spec : ''), ['class' => 'form-control col-sm-3']) !!}
							</div>

							<div class="form-group">
								<label for="content" class="control-label">Currency</label>

								{!! Form::input('text','translation[' . $localeCode . '][currency]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->currency : ''), ['class' => 'form-control col-sm-4']) !!}
							</div>
						</div>
					@endforeach
				</div>
			</div>

			{!! form_row($form->amount) !!}
			{!! form_row($form->min_amount) !!}
			{!! form_row($form->is_active) !!}
			{!! form_row($form->symbol) !!}
			{!! form_row($form->sample) !!}
			{!! form_row($form->image) !!}
			{!! form_row($form->image_10days) !!}
			{!! form_row($form->icon) !!}
			{!! form_row($form->shipment_item_id) !!}

			{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
			{!! form_end($form) !!}
		</div>
	</div>

</div>

<script type="text/javascript">
	$(function () {
		$('textarea').summernote({
			height: 300,
		});
	});
</script>

@endsection
