@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.products') }} :: {{ trans('admin.products.links') }} @endsection
@section('content')
		
		<!-- page content -->
<div class="right_col" role="main">
	
	@include('admin.errors')
	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-6">
					<div class="x_title">
						<h2>{{ trans('admin.products.links') }}
							<small>{{ trans('admin.common.list') }}</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-sm-6 text-right">
					<a href="{{ route('admin.languages.renew.links.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> {{ trans('admin.products.generate') }}</a>
				</div>
			</div>
			
			<div class="x_content">
				
				<table class="table table-striped responsive-utilities jambo_table">
					<thead>
					<tr class="headings">
						<th class="column-title">{{ trans('admin.common.id') }}</th>
						<th class="column-title">{{ trans('admin.common.token') }}</th>
						<th class="column-title">{{ trans('admin.common.link') }}</th>
						<th class="column-title">{{ trans('admin.common.date') }}</th>
					</tr>
					</thead>
					
					<tbody>
					@foreach($links as $object)
						<tr class="even pointer">
							<td class=" ">{{ $object->id }}</td>
							<td class=" ">{{ $object->token }}</td>
							<td class=" ">{{ $object->link }}</td>
							<td class=" ">{{ $object->created_at }}</td>
						</tr>
					@endforeach
					</tbody>
				
				</table>
				
				<div class="text-right">
					{!! $links->render() !!}
				</div>
			
			</div>
		</div>
	</div>
	
	<div class="clearfix"></div>

</div>

@endsection
