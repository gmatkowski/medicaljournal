@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.products') }} :: {{ trans('admin.menu.packages') }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-6">
					<div class="x_title">
						<h2>{{ trans('admin.menu.packages') }}
							<small>{{ trans('admin.common.list') }}</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

			<div class="x_content">

				{!! Form::open(['url' => route('admin.languages.store.package')]) !!}

				<table class="table table-striped responsive-utilities jambo_table">
					<thead>
					<tr class="headings">
						<th class="column-title">{{ trans('admin.products.package') }}</th>
						<th class="column-title">{{ trans('admin.products.percent') }}</th>
					</tr>
					</thead>
					<tbody>
					@foreach($packages as $key => $object)
						@if($key > 0)
							<tr>
								<td>{{trasn('admin.products.packageof')}} {{ $key+1 }} {{trasn('admin.products.languages')}}</td>
								<td>
									<div class="col-sm-3">{!! Form::input('text','packages['.($key+1).'][percent]',$packages->get($key+1)?$packages->get($key+1)->percent:0,['class' => 'form-control col-sm-3']) !!}</div>
									<div class="col-sm-9"> %</div>
								</td>
							</tr>
						@endif
					@endforeach
					</tbody>
				</table>

				{!! Form::submit( trans('admin.common.save') ,['class' => 'btn btn-success col-sm-12']) !!}

				{!! Form::close() !!}

			</div>
		</div>
	</div>

</div>

@endsection
