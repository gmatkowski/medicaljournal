@extends('layout.admin')
@section('title') :: {{ trans('admin.products.files') }} {{ $language->name }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-6">
					<div class="x_title">
						<h2>{{ trans('admin.products.files') }} - {{ $language->name }}
							<small>{{ trans('admin.products.list') }}</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-sm-6 text-right offset-0">
					<a href="{{ route('admin.language.files.create',['id' => $language->id]) }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> {{ trans('admin.products.addfile') }}</a>
				</div>
			</div>

			<div class="x_content">

				<table class="table table-striped responsive-utilities jambo_table">
					<thead>
					<tr class="headings">
						<th class="column-title">{{ trans('admin.common.id') }}</th>
						<th class="column-title">{{ trans('admin.products.file') }}</th>
						<th class="column-title">{{ trans('admin.common.date') }}</th>
						<th class="column-title no-link last"><span class="nobr">{{ trans('admin.common.actions') }}</span>
						</th>
					</tr>
					</thead>

					<tbody>
					@foreach($objects as $object)
						<tr class="even pointer">
							<td class=" ">{{ $object->id }}</td>
							<td class=" ">{{ $object->file }}</td>
							<td class=" ">{{ $object->created_at }}</td>
							<td class=" last">
								<a href="{{ route('admin.language.files.edit',['id' => $object->id]) }}">{{ trans('admin.common.edit') }}</a>
								<a href="{{ route('admin.language.files.delete',['id' => $object->id]) }}" class="confirm">{{ trans('admin.common.delete') }}</a>
							</td>
						</tr>
					@endforeach
					</tbody>

				</table>

				<div class="text-right">
					{!! $objects->render() !!}
				</div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>

</div>

@endsection
