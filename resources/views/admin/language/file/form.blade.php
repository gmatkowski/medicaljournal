@extends('layout.admin')
@section('title') :: {{ trans('admin.common.files') }} :: {{ $mode }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="x_panel">
		<div class="x_title">
			<h2>{{ trans('admin.common.files') }}
				<small>{{ $mode }}</small>
			</h2>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			{!! form_start($form,['class' => 'form-horizontal form-label-left']) !!}

			{!! form_row($form->file) !!}

			{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
			{!! form_end($form) !!}

		</div>
	</div>

</div>

@endsection
