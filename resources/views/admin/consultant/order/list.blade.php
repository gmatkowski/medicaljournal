@extends('layout.admin')
@section('title') :: Orders @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-6">
					<div class="x_title">
						<h2>Orders
							<small>List of orders</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>

				<div class="col-sm-6 text-right">
					<a href="{{ route('consultant.orders.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> Create new order</a>
				</div>
			</div>

		</div>

		<div class="x_content">

			<table class="table table-striped responsive-utilities jambo_table">
				<thead>
				<tr class="headings">
					<th class="column-title">Id</th>
					<th class="column-title">Client name</th>
					<th class="column-title">Phone</th>
					<th class="column-title">Product (s)</th>
					<th class="column-title">Status</th>
					<th class="column-title">Cost</th>
					<th class="column-title">Date</th>
					<th class="column-title no-link last"><span class="nobr">Actions</span>
					</th>
				</tr>
				</thead>

				<tbody>
				@foreach($orders as $order)
					<tr class="even pointer">
						<td class=" ">{{ $order->id }}</td>
						<td class=" ">{{ str_limit($order->name,50) }}</td>

						<td class=" ">{{ $order->phone }}</td>
						<td class=" ">{{ $order->languages_names }}</td>
						<td class=" ">
							{{ $order->status_name }}
						</td>
						<td class=" ">{{trans('common.currency.symbol', [], null, $order->lang)}} {{ $order->price }}</td>
						<td class=" ">{{ $order->created_at }}</td>
						<td class=" last">
							<a href="{{ route('consultant.orders.details',['id' => $order->id]) }}" class="btn btn-primary">Details</a>
							<a href="{{ route('consultant.orders.delete',['id' => $order->id]) }}" class="btn btn-danger confirm">Delete</a>
						</td>
					</tr>
				@endforeach
				</tbody>

			</table>

			<div class="text-right">
				{!! $orders->render() !!}
			</div>

		</div>
	</div>
</div>

<div class="clearfix"></div>

</div>

@endsection
