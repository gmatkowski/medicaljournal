@extends('layout.admin')
@section('title') :: Orders :: {{ $mode }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	<div class="page-title">
		<div class="title_left">
			<h3>{{ ucfirst($mode) }} order</h3>
		</div>
	</div>
	<div class="clearfix"></div>

	@include('admin.errors')
	{!! form_start($form,['class' => 'form-horizontal form-label-left']) !!}
	<div class="col-sm-6">
		<div class="x_panel">
			<div class="x_title">
				<h2>Client data</h2>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">


				<div class="col-sm-6 offset-0">
					{!! form_row($form->first_name) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-6 offset-0">
					{!! form_row($form->last_name) !!}
				</div>

				<div class="clearfix"></div>

				<div class="col-sm-3 offset-0">
					{!! form_row($form->email) !!}
				</div>

				<div class="col-sm-3 offset-1">
					{!! form_row($form->phone) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-3 offset-0">
					{!! form_row($form->age) !!}
				</div>

				<div class="col-sm-3 offset-1">
					{!! form_row($form->sex) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-6 offset-0">
					{!! form_row($form->other_lang) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-12 offset-0">
					{!! form_row($form->notes) !!}
				</div>
				<div class="clearfix"></div>

			</div>
		</div>



	</div>
	<div class="col-sm-6">

		<div class="x_panel">
			<div class="x_title">
				<h2>Products</h2>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<div class="col-sm-12 offset-0">

					@foreach($languages as $key => $language)
						@if($language->is_active == 1)
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-2">
										{!! Form::checkbox('languages['.$language->id.'][id]',$language->id, $key == 0,['class'=>'flat']) !!}
									</div>
									<div class="col-sm-4">{{ $language->name }}</div>
									<div class="col-sm-1">{!! Form::label('languages['.$language->id.'][price]','Price') !!}</div>
									<div class="col-sm-5">
										{!! Form::text('languages['.$language->id.'][price]',$language->price,['class' => 'form-control']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						@endif
					@endforeach
				</div>
				<div class="clearfix"></div>

			</div>
		</div>


		<div class="x_panel">
			<div class="x_title">
				<h2>Address data</h2>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<div id="address">
					<div class="col-sm-6 offset-0">
						{!! form_row($form->country_id,['attr' => ['class' => 'form-control selectable','data-sequence' => 1, 'data-name' => 'country']]) !!}
					</div>
					<div class="col-sm-6">
						{!! form_row($form->province_id,['attr' => ['class' => 'form-control selectable','data-sequence' => 2, 'data-name' => 'province']]) !!}
					</div>
					<div class="col-sm-6 offset-0">
						{!! form_row($form->city_id,['attr' => ['class' => 'form-control selectable','data-sequence' => 3, 'data-name' => 'city']]) !!}
					</div>
					<div class="col-sm-6">
						{!! form_row($form->district_id,['attr' => ['class' => 'form-control selectable last','data-sequence' => 4, 'data-name' => 'district']]) !!}
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-sm-6 offset-0">
					{!! form_row($form->subdistrict) !!}
				</div>

				<div class="col-sm-6">
					{!! form_row($form->address) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-3 offset-0">
					{!! form_row($form->postal_code) !!}
				</div>
				<div class="clearfix"></div>

			</div>
		</div>
	</div>

	<div class="col-sm-6">

		<div class="x_panel">
			<div class="x_content">
				{!! Form::checkbox('to_confirm','1', false,['class'=>'flat']) !!}
				{!! Form::label('to_confirm', 'Confirmation required?') !!}
			</div>
		</div>
	</div>

	<div class="col-sm-12">
		{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
	</div>

	<div class="clearfix"></div>

	{!! form_end($form) !!}

</div>

<script type="text/javascript">
	$(function () {
		$(".multiple").select2({
			maximumSelectionLength: 4,
			placeholder: "With Max Selection limit 4",
			allowClear: true
		});
	});
</script>

@endsection
