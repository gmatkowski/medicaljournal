@extends('layout.admin')
@section('title') :: Orders :: {{ $order->id }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="x_panel">
		<div class="x_title">
			<h2>Orders
				<small>{{ $order->id }}</small>
			</h2>

			<div class="clearfix"></div>
		</div>

		<div class="x_content">

			<section class="content invoice">
				<!-- title row -->
				<div class="row">
					<div class="col-xs-12 invoice-header">
						<h1>
							<i class="fa fa-globe"></i> Order.
							<small class="pull-right">Date: {{ $order->created_at_formated->format('d/m/Y') }}</small>
						</h1>
					</div>
					<!-- /.col -->
				</div>
				<!-- info row -->
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
						From
						<address>
							<strong>{{ Config::get('order.from') }}</strong> <br>{{ Config::get('order.address') }}
							<br>{{ Config::get('order.city') }}
							<br>Phone: {{ Config::get('order.phone') }} <br>Email: {{ Config::get('order.email') }}
						</address>
					</div>
					<!-- /.col -->
					<div class="col-sm-4 invoice-col">
						To
						<address>
							<strong>{{ $order->name }}</strong>
							@if($order->method == 2)
								<br>{{ $order->address->address_full_state }}
								<br>{{ $order->address->city }}
							@endif
							<br>Phone: {{ $order->phone }} <br>Email: {{ $order->email }}
						</address>
					</div>
					<!-- /.col -->
					<div class="col-sm-4 invoice-col">
						<b>Order ID:</b> {{ $order->id }} <br>
						<b>Payment date:</b> {{ $order->payed_at_formated->format('d/m/Y H:i:s') }} <br>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->

				<!-- Table row -->
				<div class="row">
					<div class="col-xs-12 table">
						<table class="table table-striped">
							<thead>
							<tr>
								<th>Quantity</th>
								<th>Product</th>
								<th>Product ID</th>
								<th>Price</th>
							</tr>
							</thead>
							<tbody>
							@foreach($order->languages as $language)
								<tr>
									<td>1</td>
									<td>{{ $language->language->name }}</td>
									<td>{{ $language->language->id }}</td>
									<td>{{ trans('common.currency.symbol', [], null, $order->lang) }} {{ number_format($language->price,0,',','.') }}</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->

				<div class="row">
					<!-- accepted payments column -->
					<div class="col-xs-6">

					</div>
					<!-- /.col -->
					<div class="col-xs-6">
						<p class="lead">Payment date {{ $order->payed_at_formated->format('d/m/Y') }}</p>

						<div class="table-responsive">
							<table class="table">
								<tbody>
								<tr>
									<th style="width:50%">Price:</th>
									<td>{{ trans('common.currency.symbol', [], null, $order->lang) }} {{ $order->price }}</td>
								</tr>
								<tr>
									<th>summary:</th>
									<td>{{ trans('common.currency.symbol', [], null, $order->lang) }} {{ $order->price }}</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->

				{{--
				<div class="row no-print">
					<div class="col-xs-12">
						<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print
						</button>
						<button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
						</button>
						<button class="btn btn-primary pull-right" style="margin-right: 5px;">
							<i class="fa fa-download"></i> Generate PDF
						</button>
					</div>
				</div>
				--}}
			</section>
		</div>

	</div>

</div>

@endsection
