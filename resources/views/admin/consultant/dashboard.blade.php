@extends('layout.admin')
@section('title') ::Dashboard @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">
	<h1 class="text-center">Hello, {{ Auth::user()->name }}</h1>

	<div class="text-center">
		<a href="{{ route('consultant.orders.create') }}" class="btn btn-success btn-lg">Create new order</a>
	</div>

</div>

@endsection
