<!-- top navigation -->
<div class="top_nav">

	<div class="nav_menu">
		<nav class="" role="navigation">
			<div class="nav toggle">
				<a id="menu_toggle"><i class="fa fa-bars"></i></a>
			</div>

			<ul class="nav navbar-nav navbar-right">
				<li class="">
					<a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						{{ Auth::user()->name }}
						@if(Auth::user()->getStampAttribute()!='[]'){{ Auth::user()->stamp}}@endif
						({{ Auth::user()->role[0]->display_name }})
						<span class=" fa fa-angle-down"></span>
					</a>
					<ul class="dropdown-menu dropdown-usermenu animated fadeInDown pull-right">
						@if(Entrust::hasRole('admin'))
							<li><a href="{{ route('admin.users.edit',['id' => Auth::id()]) }}"> Profile</a></li>
						@endif
						<li><a href="{{ route('admin.logout') }}"><i class="fa fa-sign-out pull-right"></i> Logout</a>
						</li>
					</ul>
				</li>

			</ul>
		</nav>
	</div>

</div>
<!-- /top navigation -->