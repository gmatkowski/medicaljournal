@extends('layout.admin')
@section('title') ::Dashboard @endsection
@section('content')

<!-- page content -->
<div class="right_col" role="main">
	<h1 class="text-center">Hello, {{ Auth::user()->name }}</h1>

	{{--@if(Entrust::ability('admin, ccadmin, headcc,css', 'consultants-orders-counter'))--}}
		{{--<div class="text-center">--}}
			{{--<b>Consultants have created today {{ $consultantsOrders }} orders.</b>--}}
		{{--</div>--}}
		{{--<br>--}}
		{{--<div id="chart-div"></div>--}}
		{{--{!! $lava->render('ColumnChart', 'Garcinia', 'chart-div') !!}--}}
		{{--<br>--}}
	{{--@endif--}}

	{{--@if(Entrust::ability('admin', 'create-order'))--}}
		{{--<div class="text-center">--}}
			{{--<a href="{{ route('orders.create') }}" class="btn btn-success btn-lg">Create new order</a>--}}
		{{--</div>--}}
	{{--@endif--}}
	<br><br><br><br><br><br>
	@if(Entrust::ability('admin', 'export-contacts'))
		{{--<div class="text-center">--}}
			{{--<a href="{{ route('get.allMails') }}" class="btn btn-default btn-lg">Export Mailing</a>--}}
		{{--</div>--}}
		{{--<div class="text-center">--}}
			{{--<a href="{{ route('get.unsoldMails') }}" class="btn btn-default btn-lg">Export Mailing (Unsold)</a>--}}
		{{--</div>--}}
		{{--<br>--}}
		{{--<div class="text-center">--}}
			{{--<a href="{{ route('get.soldMails') }}" class="btn btn-default btn-lg">Export Mailing (Sold)</a>--}}
		{{--</div>--}}
	@endif

</div>

@endsection

