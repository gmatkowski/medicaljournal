<div class="col-md-3 left_col">
	<div class="left_col scroll-view">

		<div class="navbar nav_title" style="border: 0;background: #EDEDED;">
			<a href="{{ route('admin.home') }}">
				<img src="{{ asset('build/images/admin/order-logo.png') }}" class="img-responsive" alt=""/>
			</a>
		</div>
		<div class="clearfix"></div>

		<!-- menu prile quick info -->
		<div class="profile">
			<div class="profile_info">
				<span>Hello,</span>
				<h2>{{ Auth::user()->name }}</h2>
			</div>
			<div class="clearfix"></div>
		</div>
		<!-- /menu prile quick info -->

		<br/>

		<!-- sidebar menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			@if(Entrust::hasRole(['editor'])===false)
				<div class="menu_section">
					<h3>General</h3>
					<ul class="nav side-menu">

						<li {!! Route::currentRouteName() == 'admin.home'?'class="active"':'' !!}>
							<a href="{{ route('admin.home') }}"><i class="fa fa-home"></i> {{ trans('admin.menu.dashboard') }} </a>
						</li>

						@if(Entrust::ability('admin', 'delete-posts,edit-posts,create-posts'))
						{{--<li {!! Route::currentRouteName() == 'admin.blog'?'class="active"':'' !!}>
							<a href="{{ route('admin.blog') }}"><i class="fa fa-thumb-tack"></i> {{ trans('admin.menu.blog') }} </a>
						</li>--}}
						@endif

						@role('admin')
							{{--<li {!! Route::currentRouteName() == 'admin.headers'?'class="active"':'' !!}>
								<a href="{{ route('admin.headers') }}"><i class="fa fa-header"></i> {{ trans('admin.menu.headers') }} </a>
							</li>--}}
						@endrole
						@if(Entrust::hasRole(['admin', 'ccadmin']))
							{{--<li {!! $controller == 'PageController'?'class="active"':'' !!}>
								<a href="{{ route('admin.pages') }}"><i class="fa fa-newspaper-o"></i> {{ trans('admin.menu.pages') }} </a>
							</li>

							<li {!! $controller == 'RecordController'?'class="active"':'' !!}>
								<a href="{{ route('admin.records') }}"><i class="fa fa-music"></i> {{ trans('admin.menu.recordings') }} </a>
							</li>--}}

							<li {!! $controller == 'LanguageController' || $controller == 'LanguageFileController'?'class="active"':'' !!}>
								<a href="{{ route('admin.languages') }}"><i class="fa fa-language"></i> {{ trans('admin.menu.products') }}
									</a>

							</li>

						{{--

							<li {!! $controller == 'JobController'?'class="active"':'' !!}>
								<a href="{{ route('admin.jobs') }}"><i class="fa fa-bar-chart-o"></i> {{ trans('admin.menu.professions') }} </a>
							</li>

							<li {!! $controller == 'LessonController'?'class="active"':'' !!}>
								<a href="{{ route('admin.lessons') }}"><i class="fa fa-book"></i> {{ trans('admin.menu.lessons') }} </a>
							</li>

							<li {!! $controller == 'FaqController'?'class="active"':'' !!}>
								<a href="{{ route('admin.faqs') }}"><i class="fa fa-question-circle"></i> {{ trans('admin.menu.faq') }} </a>
							</li>

							<li {!! $controller == 'StatController'?'class="active"':'' !!}>
								<a href="{{ route('admin.stats') }}"><i class="fa fa-signal"></i> {{ trans('admin.menu.stats') }} </a>
							</li>

							<li {!! $controller == 'OpinionController'?'class="active"':'' !!}>
								<a href="{{ route('admin.opinions') }}"><i class="fa fa-comment"></i> {{ trans('admin.menu.opinions') }} </a>
							</li>--}}
						@endif

						@if(Entrust::ability('admin', 'list-contacts'))
							<li {!! $controller == 'ContactController'?'class="active"':'' !!}>
								<a><i class="fa fa-at"></i> {{ trans('admin.menu.contacts') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.contacts', ['product' => 'cambogia']) }}">Cambogia</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'blackmask']) }}">BlackMask</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'hallupro']) }}">DrHallux</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'penirum']) }}">Penirum</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'waist']) }}">Perfect Shape</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'detoclean']) }}">Detoclean</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'coffee']) }}">Green Coffee Fat Burner</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'joint']) }}">Joint Cure</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'posture']) }}">PostureFixerPRO</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'bra']) }}">FlyBra</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'powerbank']) }}">PowerBank(Adsbalance)</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'ultraslim']) }}">Ultra Slim</a></li>

									<li><a href="{{ route('admin.contacts', ['product' => 'bustiere']) }}">Bustiere (Brest Creame)</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'vervalen']) }}">Vervalen (fungus remedy)</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'claireinstantwhitening']) }}">Claire Instant Whitening</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'manuskin']) }}">Manuskin Active (Psoriasis treatment)</a></li>

									<li><a href="{{ route('admin.contacts', ['product' => 'flexaplus']) }}">Flexa Plus</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'detoxic']) }}">Detoxic</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'dietbooster']) }}">Diet Booster</a></li>

									<li><a href="{{ route('admin.contacts', ['product' => 'imove']) }}">iMove</a></li>

									<li><a href="{{ route('admin.contacts', ['product' => 'dominator']) }}">Dominator</a></li>

									<li><a href="{{ route('admin.contacts', ['product' => 'nutrilashpromo']) }}">NutrilashPromo</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'leferyacrpromo']) }}">LeferyACRPromo</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'forsopromo']) }}">ForsoPromo</a></li>
									<li><a href="{{ route('admin.contacts', ['product' => 'ultrahairpromo']) }}">UltrahairPromo</a></li>

									<li><a href="{{ route('admin.contacts', ['product' => 'musclegain']) }}">Muscle Gain</a></li>
								</ul>
							</li>
							<li {!! $controller == 'ExternalClientController'?'class="active"':'' !!}>
								<a><i class="fa fa-at"></i> {{ trans('admin.menu.external') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">
{{--									<li><a href="{{ route('admin.external.clients', ['product' => 'cambogia']) }}">Cambogia</a></li>--}}
									<li><a href="{{ route('admin.external.clients', ['product' => 'blackmask']) }}">BlackMask</a></li>
									<li><a href="{{ route('admin.external.clients', ['product' => 'hallupro']) }}">DrHallux</a></li>
									<li><a href="{{ route('admin.external.clients', ['product' => 'waist']) }}">Perfect Shape</a></li>
									<li><a href="{{ route('admin.external.clients', ['product' => 'detoclean']) }}">Detoclean</a></li>
									<li><a href="{{ route('admin.external.clients', ['product' => 'coffee']) }}">Green Coffee Fat Burner</a></li>
									<li><a href="{{ route('admin.external.clients', ['product' => 'joint']) }}">Joint Cure</a></li>
								</ul>
							</li>
							<li {!! $controller == 'ExternalStatsController'?'class="active"':'' !!}>
								<a><i class="fa fa-at"></i> {{ trans('admin.menu.external.stats') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">
									<li><a href="{{ route('admin.externalstats.clients', ['product' => 'cambogia']) }}">Cambogia</a></li>
									<li><a href="{{ route('admin.externalstats.clients', ['product' => 'blackmask']) }}">BlackMask</a></li>
									<li><a href="{{ route('admin.externalstats.clients', ['product' => 'hallupro']) }}">DrHallux</a></li>
									<li><a href="{{ route('admin.externalstats.clients', ['product' => 'waist']) }}">Perfect Shape</a></li>
									<li><a href="{{ route('admin.externalstats.clients', ['product' => 'detoclean']) }}">Detoclean</a></li>
									<li><a href="{{ route('admin.externalstats.clients', ['product' => 'coffee']) }}">Green Coffee Fat Burner</a></li>
									<li><a href="{{ route('admin.externalstats.clients', ['product' => 'joint']) }}">Joint Cure</a></li>
								</ul>
							</li>
							<li {!! $controller == 'ExportController'?'class="active"':'' !!}>
								<a href="{{ route('admin.personalstats.index') }}"><i class="fa fa-at"></i> {{ trans('admin.menu.personal.stats') }}
								</a>
							</li>
						@endif
						@if(Entrust::hasRole(['admin', 'ccadmin']))
							<li {!! $controller == 'InventoryController'?'class="active"':'' !!}>
								<a><i class="fa fa-asterisk"></i> {{ trans('admin.menu.inventory') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">
									<li><a href="{{ route('admin.inventory', ['product' => 'garcinia-cambogia']) }}">Cambogia</a></li>
									<li><a href="{{ route('admin.inventory', ['product' => 'black-mask']) }}">BlackMask</a></li>
									<li><a href="{{ route('admin.inventory', ['product' => 'hallu-pro']) }}">DrHallux</a></li>
									<li><a href="{{ route('admin.inventory', ['product' => 'waist']) }}">Perfect Shape</a></li>
									<li><a href="{{ route('admin.inventory', ['product' => 'detoclean']) }}">Detoclean</a></li>
									<li><a href="{{ route('admin.inventory', ['product' => 'coffee']) }}">Green Coffee Fat Burner</a></li>
									<li><a href="{{ route('admin.inventory', ['product' => 'joint']) }}">Joint Cure</a></li>
									<li><a href="{{ route('admin.inventory', ['product' => 'posture']) }}">PostureFixerPRO</a></li>
								</ul>
							</li>
						@endif
						@if(Entrust::ability('admin', 'list-all-orders,list-cod-orders,list-cod-by-css-orders,list-own-orders'))
							<li>
								<a><i class="fa fa-truck"></i> Garcinia{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'garcinia-cambogia']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=garcinia-cambogia']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
											{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=garcinia-cambogia']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=garcinia-cambogia']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=garcinia-cambogia']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=garcinia-cambogia']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=garcinia-cambogia']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=garcinia-cambogia']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> {{ $product2->name }}{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'black-mask']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=black-mask']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
											{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=black-mask']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=black-mask']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=black-mask']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=black-mask']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=black-mask']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=black-mask']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> {{ $product3->name }}{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'hallu-pro']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=hallu-pro']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
											{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=hallu-pro']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=hallu-pro']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=hallu-pro']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=hallu-pro']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=hallu-pro']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=hallu-pro']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> {{ $product4->name }}{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'waist']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=waist']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
											{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=waist']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=waist']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=waist']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=waist']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=waist']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=waist']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> {{ $product5->name }}{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'detoclean']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=detoclean']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
											{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=detoclean']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=detoclean']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=detoclean']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=detoclean']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=detoclean']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=detoclean']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> GreenCoffee{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'coffee']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=coffee']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
											{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=coffee']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=coffee']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=coffee']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=coffee']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=coffee']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=coffee']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> JointCure{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'joint']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=joint']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
											{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=joint']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=joint']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=joint']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=joint']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=joint']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=joint']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> PostureFixer{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'posture']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=posture']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=posture']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=posture']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=posture']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=posture']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=posture']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=posture']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> FlyBra{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'bra']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=bra']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=bra']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=bra']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=bra']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=bra']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=bra']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=bra']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> PowerBank{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'powerbank']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=powerbank']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=powerbank']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=powerbank']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=powerbank']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=powerbank']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=powerbank']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=powerbank']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
							<li>
								<a><i class="fa fa-truck"></i> UltraSlim{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'ultraslim']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=ultraslim']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=ultraslim']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=ultraslim']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=ultraslim']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=ultraslim']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=ultraslim']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=ultraslim']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> Bustiere{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'bustiere']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=bustiere']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=bustiere']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=bustiere']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=bustiere']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=bustiere']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=bustiere']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=bustiere']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> Vervalen{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'vervalen']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=vervalen']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=vervalen']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=vervalen']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=vervalen']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=vervalen']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=vervalen']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=vervalen']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> InstantWhitening{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'claireinstantwhitening']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=claireinstantwhitening']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=claireinstantwhitening']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=claireinstantwhitening']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=claireinstantwhitening']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=claireinstantwhitening']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=claireinstantwhitening']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=claireinstantwhitening']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> Manuskin{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'manuskin']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=manuskin']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=manuskin']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=manuskin']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=manuskin']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=manuskin']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=manuskin']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=manuskin']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> GarciniaPromo{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'garcinia-cambogiapromo']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=garcinia-cambogiapromo']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=garcinia-cambogiapromo']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=garcinia-cambogiapromo']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=garcinia-cambogiapromo']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=garcinia-cambogiapromo']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=garcinia-cambogiapromo']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=garcinia-cambogiapromo']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> DetocleanPromo{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'detocleanpromo']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=detocleanpromo']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=detocleanpromo']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=detocleanpromo']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=detocleanpromo']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=detocleanpromo']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=detocleanpromo']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=detocleanpromo']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> JointCurePromo{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'jointpromo']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=jointpromo']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=jointpromo']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=jointpromo']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=jointpromo']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=jointpromo']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=jointpromo']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=jointpromo']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> Flexa Plus{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'flexaplus']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=flexaplus']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=flexaplus']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=flexaplus']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=flexaplus']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=flexaplus']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=flexaplus']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=flexaplus']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> Detoxic{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'detoxic']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=detoxic']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=detoxic']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=detoxic']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=detoxic']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=detoxic']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=detoxic']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=detoxic']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> Diet Booster{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'dietbooster']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=dietbooster']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=dietbooster']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=dietbooster']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=dietbooster']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=dietbooster']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=dietbooster']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=dietbooster']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> iMove{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'imove']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=imove']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=imove']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=imove']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=imove']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=imove']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=imove']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=imove']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> Dominator{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'dominator']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=dominator']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=dominator']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=dominator']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=dominator']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=dominator']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=dominator']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=dominator']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>





							<li>
								<a><i class="fa fa-truck"></i> NutrilashPromo{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'nutrilashpromo']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=nutrilashpromo']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=nutrilashpromo']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=nutrilashpromo']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=nutrilashpromo']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=nutrilashpromo']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=nutrilashpromo']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=nutrilashpromo']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> LeferyACRPromo{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'leferyacrpromo']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=leferyacrpromo']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=leferyacrpromo']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=leferyacrpromo']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=leferyacrpromo']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=leferyacrpromo']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=leferyacrpromo']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=leferyacrpromo']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> ForsoPromo{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'forsopromo']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=forsopromo']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=forsopromo']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=forsopromo']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=forsopromo']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=forsopromo']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=forsopromo']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=forsopromo']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> UltrahairPromo{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'ultrahairpromo']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=ultrahairpromo']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=ultrahairpromo']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=ultrahairpromo']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=ultrahairpromo']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=ultrahairpromo']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=ultrahairpromo']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=ultrahairpromo']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>

							<li>
								<a><i class="fa fa-truck"></i> MuscleGain{{ trans('admin.menu.orders') }}<span class="fa fa-chevron-down"></span></a>
								<ul class="nav child_menu" style="display: none">

									<li><a href="{{ route('admin.orders', ['product' => 'musclegain']) }}">{{trans('admin.menu.list')}}</a></li>

									@if(Entrust::ability('admin', 'list-cod-orders,list-cod-by-css-orders'))
										<li>
											<a href="{{ route('admin.orders.to.cod', ['param' => '?product=musclegain']) }}">{{trans('admin.menu.codtosend')}}</a>
										</li>
										{{--<li>--}}
										{{--<a href="{{ route('admin.orders.attempts', ['param' => '?product=musclegain']) }}">{{trans('admin.menu.cod.duplicated')}}</a>--}}
										{{--</li>--}}
										<li>
											<a href="{{ route('admin.orders.to.confirm', ['param' => '?product=musclegain']) }}">{{trans('admin.menu.codtoconfirm')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.archived', ['param' => '?product=musclegain']) }}">{{trans('admin.menu.cod-archived')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.after.cod', ['param' => '?product=musclegain']) }}">{{trans('admin.menu.codsent')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer', ['param' => '?product=musclegain']) }}">{{trans('admin.menu.banktransfer')}}</a>
										</li>
										<li>
											<a href="{{ route('admin.orders.bank.transfer.after.cod', ['param' => '?product=musclegain']) }}">{{trans('admin.menu.banktransfer.sent')}}</a>
										</li>
									@endif
								</ul>
							</li>
						@endif

						@role('admin')
							{{--<li>--}}
								{{--<a><i class="fa fa-book"></i> {{ trans('admin.menu.invoices') }}<span class="fa fa-chevron-down"></span></a>--}}
								{{--<ul class="nav child_menu" style="display: none">--}}
									{{--<li>--}}
										{{--<a href="{{ route('admin.orders.invoices', ['param' => '?product=garcinia-cambogia']) }}">Garcinia</a>--}}
									{{--</li>--}}
									{{--<li>--}}
										{{--<a href="{{ route('admin.orders.invoices', ['param' => '?product=black-mask']) }}">BlackMask</a>--}}
									{{--</li>--}}
									{{--<li>--}}
										{{--<a href="{{ route('admin.orders.invoices', ['param' => '?product=hallu-pro']) }}">HalluPro</a>--}}
									{{--</li>--}}
								{{--</ul>--}}
							{{--</li>--}}

							{{--<li>--}}
								{{--<a href="{{ route('admin.orders.cdata') }}"><i class="fa fa-database"></i> {{ trans('admin.menu.cdata') }}</a>--}}
							{{--</li>--}}

							{{--<li>--}}
								{{--<a href="{{ route('admin.settings') }}"><i class="fa fa-asterisk"></i> {{ trans('admin.menu.settings') }}</a>--}}
							{{--</li>--}}
						@endrole

						@if(Entrust::ability('admin', 'manage-users'))
							<li {!! $controller == 'UserController'?'class="active"':'' !!}>
								<a href="{{ route('admin.users') }}"><i class="fa fa-users"></i> {{ trans('admin.menu.users') }} </a>
							</li>
						@endif
					</ul>
				</div>
			@endif
		</div>
		<!-- /sidebar menu -->
	</div>
</div>