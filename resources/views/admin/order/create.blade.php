@extends('layout.admin')
@section('title') :: Orders :: {{ $mode }} @endsection
@section('content')
	
	<!-- page content -->
	<div class="right_col" role="main">
		
		<div class="page-title">
			<div class="title_left">
				<h3>{{ ucfirst($mode) }} order</h3>
			</div>
		</div>
		<div class="clearfix"></div>
		
		@include('admin.errors')
		{!! form_start($form,['class' => 'form-horizontal form-label-left']) !!}
		<div class="col-sm-6">
			<div class="x_panel">
				<div class="x_title">
					<h2>Client data</h2>
					
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					
					<div class="col-sm-6 offset-0">
						{!! form_row($form->first_name) !!}
					</div>
					<div class="clearfix"></div>
					
					<div class="col-sm-6 offset-0">
						{!! form_row($form->last_name) !!}
					</div>
					
					<div class="clearfix"></div>
					
					<div class="col-sm-3 offset-0">
						{!! form_row($form->email) !!}
					</div>
					
					<div class="col-sm-3 offset-1">
						{!! form_row($form->phone) !!}
					</div>
					<div class="col-sm-3 offset-1">
						{!! form_row($form->size) !!}
					</div>
					<div class="clearfix"></div>
					
					<div class="col-sm-3 offset-0">
						{!! form_row($form->age) !!}
					</div>
					
					<div class="col-sm-3 offset-1">
						{!! form_row($form->sex) !!}
					</div>
					
					<div class="clearfix"></div>
					
					<div class="col-sm-12 offset-0">
						{!! form_row($form->notes) !!}
					</div>
					<div class="clearfix"></div>
				
				</div>
			</div>
		
		
		</div>
		<div class="col-sm-6">
			
			<div class="x_panel">
				<div class="x_title">
					<h2>Products</h2>
					
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<div class="col-sm-12 offset-0">
						
						@foreach($languages as $key => $language)
							@if($language->is_active == 1)
								<div class="col-sm-12">
									<div class="form-group">
										<div class="col-sm-1">
											{!! Form::checkbox('languages['.$language->id.'][id]',$language->id, $key == 0,['class'=>'flat']) !!}
										</div>
										<div class="col-sm-2">{{ $language->name }}</div>
										{!! Form::hidden('languages['.$language->id.'][symbol]',$language->symbol) !!}
										<div class="col-sm-1">{!! Form::label('languages['.$language->id.'][qty]','Qty') !!}</div>
										<div class="col-sm-2">
                                            @if($key==9)
                                                {!! Form::select('languages['.$language->id.'][qty]',
                                                [
                                                '1' => '1',
                                                ], 1, ['id' => 'product'.++$key.'qty', 'class' => 'form-control selectable product'.$key]) !!}
                                            @elseif($key==15 || $key==16 || $key==17)
												{!! Form::select('languages['.$language->id.'][qty]',
												[
												'3' => '3',
												'6' => '6',
												'9' => '9',
												], 3, ['id' => 'product'.++$key.'qty', 'class' => 'form-control selectable product'.$key]) !!}
											@else
                                                {!! Form::select('languages['.$language->id.'][qty]',
                                                [
                                                '1' => '1',
                                                '2' => '2',
                                                '3' => '3',
                                                '4' => '4',
                                                '5' => '5',
                                                '6' => '6'
                                                ], 1, ['id' => 'product'.++$key.'qty', 'class' => 'form-control selectable product'.$key]) !!}
											@endif
										</div>
										<div class="col-sm-1">{!! Form::label('languages['.$language->id.'][price]','Price') !!}</div>
										<div class="col-sm-2">
											{!! Form::text('languages['.$language->id.'][price]', $language->price,['id' => 'product'.+$key.'price', 'class' => 'form-control', 'onkeydown' => Entrust::hasRole(['admin', 'ccadmin', 'css']) ? '' : 'return false;']) !!}
										</div>
										<div class="col-sm-1">{!! Form::label('languages['.$language->id.'][total_price]','Total Price') !!}</div>
										<div class="col-sm-2">
											{!! Form::text('languages['.$language->id.'][total_price]', $language->price, ['id' => 'product'.$key.'totalPrice', 'class' => 'form-control', 'disabled' => 'disabled', 'style' => 'width:85px']) !!}
										</div>
										<div class="clearfix"></div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
					<div class="clearfix"></div>
				
				</div>
			</div>
			
			<div class="x_panel">
				<div class="x_title">
					<h2>Address data</h2>
					
					<div class="clearfix"></div>
				</div>
				<div class="x_content">
					
					<div id="address">
						<div class="col-sm-6 offset-0">
							{!! form_row($form->postal_code,['attr' => ['class' => 'form-control selectable','data-sequence' => 1, 'data-name' => 'postalcode']]) !!}
						</div>
						<div class="col-sm-6">
							{!! form_row($form->subdistrict,['attr' => ['disabled' => true, 'class' => 'form-control selectable last','data-sequence' => 2, 'data-name' => 'subdistrict']]) !!}
						</div>
						<div class="col-sm-6 offset-0">
							{!! form_row($form->hiddenDistrict) !!}
							{!! form_row($form->district_id,['attr' => ['disabled' => true, 'class' => 'form-control selectable','data-sequence' => 3, 'data-name' => 'district']]) !!}
						</div>
						<div class="col-sm-6">
							{!! form_row($form->hiddenCity) !!}
							{!! form_row($form->city_id,['attr' => ['disabled' => true, 'class' => 'form-control selectable','data-sequence' => 4, 'data-name' => 'city']]) !!}
						</div>
						<div class="col-sm-6 offset-0">
							{!! form_row($form->hiddenProvince) !!}
							{!! form_row($form->province_id,['attr' => ['disabled' => true, 'class' => 'form-control selectable','data-sequence' => 5, 'data-name' => 'province']]) !!}
						</div>
						<div class="col-sm-6">
							{!! form_row($form->country_id) !!}
						</div>
						<div class="clearfix"></div>
					</div>

					<div class="col-sm-12 offset-0">
						{!! form_row($form->address) !!}
					</div>
					<div class="clearfix"></div>
				
				</div>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="x_panel">
				<div class="x_content">
					{!! Form::checkbox('to_confirm','1', false,['class'=>'flat']) !!}
					{!! Form::label('to_confirm', 'Confirmation required?') !!}
					{!! Form::checkbox('bank_transfer','1', false,['id' => 'bank_transfer', 'class'=>'flat', 'data-areas' => $bank_transfer_areas]) !!}
					{!! Form::label('bank_transfer', 'Bank Transfer') !!}
				</div>
			</div>
		</div>
		
		<div class="col-sm-12">
			{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
		</div>
		
		<div class="clearfix"></div>
		
		{!! form_end($form, false) !!}
	
	</div>

	<script type="text/javascript">
		$(function () {
            $('#postal_code').change(function () {
                var bank_transfer = $('#bank_transfer');
				var areas = bank_transfer.data().areas;
				var picked = this.value;
                bank_transfer.iCheck('uncheck');
                areas.forEach(function(value) {
                    if(value==picked)
					{
                        bank_transfer.iCheck('check');
					}
                });
            });

            var prices = {
                product1:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product2:
                    {
                        1 : 450000,
                        2 : 900000,
//                    '2in1' : 450000,
						3 : 990000,
						4 : 1800000,
						5 : 2250000,
						6 : 1800000
                },
                product3:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product4:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product5:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product6:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product7:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product8:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product9:
                    {
                        1 : 590000,
                        2 : 1180000,
                        3 : 1500000,
                        4 : 2360000,
                        5 : 2950000,
                        6 : 2360000
                    },
                product11:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product12:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product13:
                    {
                        1 : 590000,
                        2 : 1180000,
                        3 : 1500000,
                        4 : 2360000,
                        5 : 2950000,
                        6 : 2360000
                    },
                product14:
                    {
                        1 : 590000,
                        2 : 1180000,
                        3 : 1500000,
                        4 : 2360000,
                        5 : 2950000,
                        6 : 2360000
                    },
                product15:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product16:
                    {
                        3 : 1250000,
                        6 : 2500000,
                        9 : 3750000,
                    },
                product17:
                    {
                        3 : 1250000,
                        6 : 2500000,
                        9 : 3750000,
                    },
                product18:
                    {
                        3 : 1250000,
                        6 : 2500000,
                        9 : 3750000,
                    },
                product19:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product20:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
                product21:
                    {
                        1 : 490000,
                        2 : 980000,
                        3 : 1250000,
                        4 : 1960000,
                        5 : 2450000,
                        6 : 1960000
                    },
            };

            $.each([ 'product1', 'product2', 'product3', 'product4', 'product5', 'product6', 'product7', 'product8', 'product9', 'product11', 'product12', 'product13', 'product14', 'product15', 'product16', 'product17', 'product18', 'product19', 'product20', 'product21' ], function( index, value ) {

                $('#product16totalPrice').val('1250000');
                $('#product17totalPrice').val('1250000');
                $('#product18totalPrice').val('1250000');

                var select = $('select.'+value);

                select.change(function () {
                    var promoPrice = prices[value][this.value];

                    $('#'+value+'totalPrice').val(promoPrice);
                    $('#'+value+'price').val(promoPrice/this.value);
                });

                $('#'+value+'price').on("input",function(){
                    $('#'+value+'totalPrice').val($('#'+value+'qty').val()*$(this).val());
                });
            });

		});
	</script>

@endsection
