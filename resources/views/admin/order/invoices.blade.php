@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

        <!-- page content -->
<div class="right_col" role="main">

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    @role(['admin', 'ccadmin'])
                        <form id="exportInvoicesForm" class="form-horizontal" action="{{ route('admin.orders.exportInvoices') }}" method="POST">
                            <div class="x_title">
                                <h2>
                                    <i class="fa fa-download"></i> Select Type :
                                    <select id="type" name="type">
                                        <option value="get" selected>Invoices</option>
                                        <option value="cancel">Credit Note</option>
                                    </select>
                                </h2>
                                <div class="clearfix"></div>
                            </div>

                                <div class="input-prepend input-group pull-right" style="width:230px;">
                                                <span class="add-on input-group-addon"><i
                                                            class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="hidden" id="product" name="product" value="{{ $productSymbol }}"/>
                                    <input type="text" style="width: 200px" name="range" id="reservation"
                                           class="form-control" value="{{ $range }}"/>
                                </div>
                                <input type="submit" id="getAllBetween" class="btn btn-success pull-right" value="Get all between">
                         </form>
                        <form id="invoices"></form>
                    @endrole
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection

@section('scripts')

    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#reservation').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });

            $('#getAllBetween').click(function(event) {
                event.preventDefault();
                var uri = $('#exportInvoicesForm').attr('action');
                $.post(uri, {type: $('#type').val(), range: $('#reservation').val(), product: $('#product').val()}, function(zip) {
                    $('#invoices').attr('action', zip);
                    $('#invoices').submit();
                });
            });
        });
    </script>
    <!-- /datepicker -->

@endsection
