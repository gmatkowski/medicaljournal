@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.orders') }} ({{ trans('admin.menu.codtosend') }}) @endsection
@section('content')
	
	<!-- page content -->
	<div class="right_col" role="main">
		
		@include('admin.errors')
		
		<div class="col-md-12 col-sm-12 col-xs-12">
			<div class="x_panel">
				<div class="container-fluid">
					<div class="col-sm-4">
						<div class="x_title">
							<h2>{{ trans('admin.menu.orders') }}
								<small>{{ $title }}</small>
							</h2>
							<div class="clearfix"></div>
						</div>
						@include('part.searchForm')
					</div>

					@if(isset($orders[0]) && $orders[0]->shipped==1)
						<div class="col-md-3 col-md-offset-3">
							<form id="export-form" class="form-horizontal" action="{{ route('admin.orders.report') }}">

								<div class="input-prepend input-group pull-left" style="width:230px;">
											<span class="add-on input-group-addon"><i
														class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
									<input type="text" style="width: 200px" name="range" id="reservation"
										   class="form-control" value="{{ $time['past'] }} - {{ $time['now'] }}"/>
								</div>
								<div class="btn-group pull-left">
									<button class="btn btn-primary pull-right" name="report" value="Credit Note" style="margin-right: 5px;"><i class="fa fa-download"></i> Get Credit Note Report</button>
									<button class="btn btn-primary pull-right" name="report" value="Orders" style="margin-right: 5px;"><i class="fa fa-download"></i> Get Sales Report</button>
								</div>
							</form>
						</div>
					@endif

				</div>
				
				<div class="x_content">
					@if(Entrust::ability('admin', 'filter-orders'))
						Show created by: <a href="{{ route(Route::currentRouteName(), '?f=customer' )}}">customer</a> |
						<a href="{{ route(Route::currentRouteName(), '?f=cc' )}}">cc</a> |
						<a href="{{ route(Route::currentRouteName()) }}">all</a>
					@endif
					
					{!! Form::open(['route' => 'admin.orders.shipment.send.multiple']) !!}
					<table class="table table-striped responsive-utilities jambo_table bulk_action">
						<thead>
						<tr class="headings">
							@if(isset($orders[0]) && $orders[0]->shipped==1)
								<th class="column-title"><span class="fa fa-refresh"></span></th>
							@endif
							<th>
								<input type="checkbox" id="check-all" class="flat">
							</th>

							<th class="column-title">{{ trans('admin.common.ref') }}</th>
							<th class="column-title">{{ trans('admin.common.name') }}</th>
							<th class="column-title">{{ trans('admin.common.phone') }}</th>
							<th class="column-title">{{ trans('admin.common.course') }}</th>
							@if($productSymbol==='waist')
								<th class="column-title">{{ trans('admin.common.size') }}</th>
							@endif
							<th class="column-title">{{ trans('admin.common.status') }}</th>
							<th class="column-title">{{ trans('admin.common.courier') }}</th>
							<th class="column-title">{{ trans('admin.common.sent') }}</th>
							<th class="column-title">{{ trans('admin.common.amount') }}</th>
							<th class="column-title">{{ trans('admin.common.date') }}</th>
							<th class="column-title">{{ trans('admin.common.by') }}</th>
							<th class="column-title no-link last">
								<span class="nobr">{{ trans('admin.common.actions') }}</span></th>
							
							<th class="bulk-actions" colspan="12">
								<a class="antoo" style="color:#fff; font-weight:500;">{{ trans('admin.common.action') }} (
									<span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
							</th>
						</tr>
						</thead>
						
						<tbody>
						@if ($orders->count() > 0)
							@foreach($orders as $order)
								<tr class="even pointer" style="background: {{ !is_null($order->user_id)?'#E9E8E9':'' }};">
									@if($orders[0]->shipped==1)
										<td class=" ">
											@if($order->status===2 && isset($order->address) && $order->address->subDistrict->postcode->shipping_provider_name!='RPX' && $order->address->subDistrict->postcode->shipping_provider_name!='Area not supported anymore')
												<a href="{{ route('api.deliveryId', ['id' => $order->id]) }}"><span class="fa fa-refresh"></span></a>
											@endif
										</td>
									@endif
									<td class="a-center ">
										<input type="checkbox" data-name="table_records" class="flat" name="orders[]" value="{{ $order->id }}">
									</td>
									<td class=" ">{{ $order->ref_or_id }}</td>
									<td class=" ">{{ str_limit($order->name,50) }}</td>
									
									<td class=" ">{{ $order->phone }}</td>
									<td class=" ">{{ $order->languages_names }}</td>
									@if($productSymbol==='waist')
										<td class=" ">{{ $order->size }}</td>
									@endif
									<td class=" ">
										<span data-action="editable" data-value="{{ $order->status }}" data-source="{{ route('admin.orders.status.list') }}" data-type="select" data-pk="{{ $order->id }}" data-url="{{ route('admin.orders.editable') }}" id="status" data-title="Status">{{ $order->status_name }}</span>
									</td>
									<td class=" ">
										{{ $order->getCourier() }}
									</td>
									<td class=" ">{{ $order->shipped ? 'Sent' : 'Not sent' }}</td>
									<td class=" ">{{trans('common.currency.symbol', [], null, $order->lang)}} {{ $order->price }}</td>
									<td class=" ">{{ $order->created_at }}</td>
									<td class=" ">
										@if (!is_null($order->user_id))
											{{ $order->user->name }} {{$order->user->stamp}}
										@else
											{{ trans('admin.common.customer') }}
										@endif
										@if ($order->duplicated ==1 && $order->shipped ==1)
											<i class="fa fa-copy"/>
										@endif
										@if (!is_null($order->comment))
											<div class="order-comment-wrapper">
												<i class="fa fa-comment"></i><br>
												<div class="order-comment">{{ $order->comment }}</div>
											</div>
										@endif
									
									</td>
									<td class=" last">
										@if(($title=='COD (not sent)' || $title=='COD (sent)') && Entrust::hasRole('consultant')===false && $order->invoice && $order->invoice->invoice_no)
											<a href="{{ route('admin.orders.invoice', ['id' => $order->id, 'invoiceType' => 'get']) }}" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Invoice</a>
										@endif
										{{--@if ($order->shipped==0 && $order->address && $order->address->district && $order->getCovered())--}}
											{{--@if(Entrust::ability('admin', 'mark-order-as-sent'))--}}
												{{--<a href="{{ route('admin.orders.shipment.mark',['id' => $order->id]) }}" class="btn btn-xs btn-success confirm">{{ trans('admin.button.markassent') }}</a>--}}
											{{--@endif--}}
											{{--@if(Entrust::ability('admin', 'send-order'))--}}
												{{--<a href="{{ route('admin.orders.shipment.send',['id' => $order->id]) }}" class="btn btn-xs btn-success confirm">{{ trans('admin.button.send') }}</a>--}}
											{{--@endif--}}
										{{--@endif--}}
										{{--@if ($order->to_confirm==0&&$order->shipped==0)--}}
											{{--<a href="{{ route('admin.orders.shipment.confirm',['id' => $order->id]) }}" class="btn btn-xs btn-success">{{ trans('admin.button.to-confirm') }}</a>--}}
										{{--@endif--}}

										{{--@if ($order->method!=7)--}}
											{{--<a href="{{ route('admin.orders.bank.transfer.confirm',['id' => $order->id]) }}" class="btn btn-xs btn-success">{{ trans('admin.button.to-bank-transfer') }}</a>--}}
										{{--@endif--}}
										
										{{--@if ($order->shipped==1 && $order->address && $order->address->district && $order->getCovered())--}}
											{{--@if(Entrust::ability('admin', 'mark-order-as-sent'))--}}
												{{--<a href="{{ route('admin.orders.shipment.unsend',['id' => $order->id]) }}" class="btn btn-xs btn-success confirm">{{ trans('admin.button.unsend') }}</a>--}}
											{{--@endif--}}
										{{--@endif--}}
										
										<a href="{{ route('admin.orders.details',['id' => $order->id]) }}" class="btn btn-xs btn-primary">{{ trans('admin.button.details') }}</a>
										{{--@if(Entrust::ability('admin', 'edit-order'))--}}
											{{--<a href="{{ route('admin.orders.edit',['id' => $order->id]) }}?product={{ $productSymbol }}" class="btn btn-xs btn-warning">{{ trans('admin.button.edit') }}</a>--}}
										{{--@endif--}}
											{{--@if(Entrust::ability('admin', 'delete-order')--}}
                                                {{--&& !$order->invoice()->first()--}}
                                                {{--&& !$order->shipped--}}
                                            {{--)--}}
											{{--<a href="{{ route('admin.orders.archive',['id' => $order->id]) }}" class="btn btn-xs btn-dark">{{ trans('admin.button.archive') }}</a>--}}
{{--											<a href="{{ route('admin.orders.delete',['id' => $order->id]) }}" class="btn btn-xs btn-danger confirm">{{ trans('admin.button.delete') }}</a>--}}
										{{--@endif--}}

									</td>
								</tr>
							@endforeach
							
							<tr>
								<td colspan="11">

									{{--@if(Entrust::ability('admin', 'send-order'))--}}
										{{--{!! Form::submit(trans('admin.button.sendmarked'),['class' => 'btn btn-success', 'name' => 'send-button']) !!}--}}
									{{--@endif--}}
									{{--@if ($order->to_confirm==0&&$order->shipped==0)--}}
										{{--{!! Form::submit(trans('admin.button.to-confirm-marked'),['class' => 'btn btn-success', 'name' => 'to-confirm-button']) !!}--}}
									{{--@endif--}}
									{{--@if(Entrust::ability('admin', 'delete-order'))--}}
										{{--<div class="btn btn-danger" id="delete-marked-button">{{ trans('admin.button.delete-marked') }}</div>--}}
										{{--<span class="delete-marked">{{ trans('admin.common.are-you-sure') }}</span>--}}
										{{--{!! Form::submit(trans('admin.common.yes'),[--}}
											{{--'class' => 'btn btn-danger delete-marked',--}}
											{{--'name' => 'delete-button'--}}
										{{--]) !!}--}}
									{{--@endif--}}
								</td>
							</tr>
						@endif
						</tbody>
					
					</table>
					{!! Form::close() !!}
					
					<div class="text-right">
						{{--@if(($title=='COD (not sent)' || $title=='COD (sent)') && Entrust::hasRole('consultant')===false)--}}
							{{--<form action="{{ route('admin.orders.invoices', ['title' => $title, 'page' => $currentPage, 'invoiceType' => 'get', 'productSymbol' => $productSymbol]) }}" method="POST">--}}
								{{--<button type="submit" class="btn btn-xs btn-primary"><i class="fa fa-download"></i> Get Invoices for this Page</button>--}}
							{{--</form>--}}
							{{--<br>--}}
						{{--@endif--}}
							@if($mode == 'list')
								@include('part.goToPageForm')
								{!! $orders->appends(['f' => Request::input('f')])->render() !!}
							@endif
					</div>
				
				</div>
			</div>
		</div>
		
		<div class="clearfix"></div>
	
	</div>

@endsection

@section('scripts')

	<!-- datepicker -->
	<script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
	<script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

	<script type="text/javascript">
        $('#reservation').daterangepicker(null, function (start, end, label) {
            console.log(start.toISOString(), end.toISOString(), label);
        });
		$('#summary').daterangepicker({
			singleDatePicker: true,
			startDate: $('#summary').val()
		}, function (start, label) {
			console.log(start.toISOString(), label);
		});
	</script>
	<!-- /datepicker -->


	<script type="text/javascript">
		$('#delete-marked-button').click(function () {
			$(this).hide();
			$('.delete-marked').show();
		});
	</script>

@endsection