@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.orders') }} @endsection
@section('content')

    <!-- page content -->
    <div class="right_col" role="main">

        @include('admin.errors')

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="container-fluid">
                    <div class="col-sm-6">
                        <div class="x_title">
                            <h2>{{ trans('admin.menu.orders') }} : {{ $counters['orders'] }}
                                <small>{{ trans('admin.menu.list') }}</small>
                            </h2>
                            <div class="clearfix"></div>
                        </div>
                        @role(['admin', 'ccadmin'])
                            <b>ADDED :</b> TODAY: <b>{{ $counters['today'] }}</b>, YESTERDAY: <b>{{ $counters['yesterday'] }}</b>,
                            RESULT OF SPECIFIC DAY FROM CALENDAR DATE: <b>{{ $counters['specificDay'] }}</b>
                            <form action="{{ route('admin.orders') }}" class="form-horizontal" method="get">
                                <input type="submit" class="btn btn-success pull-right" value="Pick Date To Summary"/>
                                <div class="input-prepend input-group pull-right" style="width:130px;">
                                    <span class="add-on input-group-addon">
                                        <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                    </span>
                                    <input type="hidden" name="product" value="{{ $productSymbol }}"/>
                                    <input type="text" style="width: 130px" name="summaryRange" id="summary" class="form-control" value="{{ $summaryRange }}"/>
                                </div>
                            </form>
                            <br><br>
                        @endrole
                        @include('part.searchForm')
                    </div>

                    @if(Entrust::ability('admin', 'export-orders'))
                        <div class="col-md-3 col-md-offset-3">
                            <form id="export-form" class="form-horizontal" action="" method="post">

                                <div class="input-prepend input-group pull-left" style="width:230px;">
                                            <span class="add-on input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                    <input type="text" style="width: 200px" name="range" id="reservation"
                                           class="form-control" value="{{ $time['past'] }} - {{ $time['now'] }}"/>
                                    <input type="text" name="productSymbol" value="{{ $productSymbol }}" style="display:none" />
                                </div>

                                <div class="btn-group pull-left">

                                    <ul class="nav nav-pills">
                                        <li class="dropdown">
                                            <a href="#" data-toggle="dropdown"
                                               class="dropdown-toggle btn btn-success"> {{ trans('admin.button.exportcsv') }}
                                                <span class="caret"></span> </a>
                                            <ul class="dropdown-menu" id="menu1">
                                                @foreach($methods as $key => $method)
                                                    <li class="dropdown-submenu">
                                                        <a href="#">{{ $method }}</a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <input type="submit" class="text-submit"
                                                                       formaction="{{ route('admin.orders.export',['filter' => $key,'status' => $key==2?'not-shipped':0]) }}"
                                                                       value="{{ $key==2?trans('admin.orders.not-sent'): trans('admin.orders.pending') }}"/>
                                                            </li>
                                                            {{--<li>
                                                                <input type="submit" class="text-submit" formaction="{{ route('admin.orders.export',['filter' => $key,'status' => 1]) }}" value="{{ trans('admin.orders.paid') }}" />
                                                            </li>--}}
                                                            @if ($key == 2)
                                                                <li>
                                                                    <input type="submit" class="text-submit"
                                                                           formaction="{{ route('admin.orders.export',['filter' => $key,'status' => 'to-confirm']) }}"
                                                                           value="{{ trans('admin.button.to-confirm') }}"/>
                                                                </li>
                                                            @endif
                                                            <li>
                                                                <input type="submit" class="text-submit"
                                                                       formaction="{{ route('admin.orders.export',['filter' => $key,'status' => $key==2?'shipped':2]) }}"
                                                                       value="{{ trans('admin.orders.sent') }}"/>
                                                            </li>

                                                            {{--<li>
                                                                <input type="submit" class="text-submit" formaction="{{ route('admin.orders.export',['filter' => $key,'status' => 4]) }}" value="{{ trans('admin.orders.rejected') }}" />
                                                            </li>--}}
                                                        </ul>
                                                    </li>
                                                @endforeach
                                                <li><input type="submit" class="text-submit"
                                                           formaction="{{ route('admin.orders.export',['filter' => 'all']) }}"
                                                           value="{{ trans('admin.orders.all') }}                              "/>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>


                                </div>

                            </form>
                        </div>
                    @endif
                </div>

                <div class="x_content">

                    <table class="table table-striped responsive-utilities jambo_table">
                        <thead>
                        <tr class="headings">
                            <th class="column-title">{{ trans('admin.common.ref') }}</th>
                            <th class="column-title">{{ trans('admin.common.name') }}</th>
                            <th class="column-title">{{ trans('admin.common.phone') }}</th>
                            <th class="column-title">{{ trans('admin.common.email') }}</th>
                            <th class="column-title">Courier</th>
                            {{--<th class="column-title">{{ trans('admin.common.status') }}</th>--}}
                            {{--<th class="column-title">{{ trans('admin.common.payment') }}</th>--}}
                            {{--<th class="column-title">{{ trans('admin.common.shipment') }}</th>--}}
                            {{--<th class="column-title">{{ trans('admin.common.amount') }}</th>--}}
                            <th class="column-title">{{ trans('admin.common.date') }}</th>
                            <th class="column-title" title="{{ trans('admin.common.by.d') }}">
                                {{ trans('admin.common.by') }}
                            </th>
                            <th class="column-title no-link last"><span
                                    class="nobr">{{ trans('admin.common.actions') }}</span>
                            </th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($orders as $order)
                            <tr class="even pointer" style="background: {{ !is_null($order->user_id)?'#E9E8E9':'' }};">
                                <td class=" ">{{ $order->ref_or_id }}</td>
                                <td class=" ">{{ str_limit($order->name,50) }}</td>

                                <td class=" ">{{ $order->phone }}</td>
                                <td class=" ">
                                    <div class="email-box" >{{ $order->email }}</div>
                                    <i class="fa fa-edit edit-email"></i>
                                    <i class="fa fa-save save-email" id="{{$order->id}}"></i>

                                </td>
                                <td class=" ">
                                    {{ $order->getCourier() }}
                                </td>
                                {{--<td class=" ">
                                    <span data-action="editable" data-value="{{ $order->status }}"
                                          data-source="{{ route('admin.orders.status.list') }}" data-type="select"
                                          data-pk="{{ $order->id }}" data-url="{{ route('admin.orders.editable') }}"
                                          id="status" data-title="Status">{{ $order->status_name }}</span>
                                </td>--}}

                                {{--<td class=" ">{{ $order->method_name }} @if($order->method == 2)--}}
                                        {{--({{ $order->nocod == 1 ? trans('admin.orders.notlocated') : trans('admin.orders.located') }}--}}
                                        {{--) @endif </td>--}}
                                {{--<td class=" ">{{ $order->version_name }}</td>--}}
                                {{--<td class=" ">{{trans('common.currency.symbol', [], null, $order->lang)}} {{ $order->price }}</td>--}}
                                <td class=" ">{{ $order->created_at }}</td>
                                <td class=" ">
                                    @if (!is_null($order->user_id))
                                        {{ $order->user->name }} {{$order->user->stamp}}
                                    @else
                                        {{ trans('admin.common.customer') }}
                                    @endif
                                </td>
                                <td class=" last">
                                    <a href="{{ route('admin.orders.details',['id' => $order->id]) }}"
                                       class="btn btn-xs btn-primary">{{ trans('admin.button.details') }}</a>

                                    {{--@if(Entrust::hasRole('admin')--}}
                                        {{--&& !$order->invoice()->first()--}}
                                        {{--&& !$order->shipped--}}
                                    {{--)--}}
                                        {{--<a href="{{ route('admin.orders.delete',['id' => $order->id]) }}" class="btn btn-danger btn-xs confirm">{{ trans('admin.button.delete') }}</a>--}}
                                    {{--@endif--}}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>

                    </table>

                    <div class="text-right">
                        @if ($mode != 'search')

                            @if(!$orders->isEmpty())
                                @include('part.goToPageForm')
                            @endif

                            {!! $orders->render() !!}
                        @endif
                    </div>

                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>

@endsection

@section('scripts')
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#reservation').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#summary').daterangepicker({
                singleDatePicker: true,
                startDate: $('#summary').val()
            }, function (start, label) {
                console.log(start.toISOString(), label);
            });
        });
    </script>
    <!-- /datepicker -->

    <script type="text/javascript">
        $('.edit-email').click(function () {
            var parent = $(this).parent();
            var box = parent.find('.email-box');
            var content = box.html();
            box.html('');
            $(this).hide();
            parent.find('.save-email').show();
            box.append('<input type="text" value="' + content + '"/>');
        });

        $('.save-email').click(function () {
            var parent = $(this).parent();
            var box = parent.find('.email-box');
            var input = parent.find('input');

            var url = encodeURI("{{ route('admin.orders.email.save') }}/" + $(this).attr('id') + "/" + input.val());

            $.get(url);

            box.html(input.val());
            $(this).hide();
            parent.find('.edit-email').show();
        });

    </script>

@endsection