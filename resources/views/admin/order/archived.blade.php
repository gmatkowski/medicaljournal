@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.orders') }} ({{ trans('admin.menu.codtosend') }}) @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-12">
					<div class="x_title">
						<h2>{{ trans('admin.menu.orders') }}
							<small>{{ $title }}</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

			<div class="x_content">

				{!! Form::open(['route' => 'admin.orders.shipment.send.multiple']) !!}
				<table class="table table-striped responsive-utilities jambo_table bulk_action">
					<thead>
					<tr class="headings">
						<th>
							<input type="checkbox" id="check-all" class="flat">
						</th>


						<th class="column-title">{{ trans('admin.common.ref') }}</th>
						<th class="column-title">{{ trans('admin.common.name') }}</th>
						<th class="column-title">{{ trans('admin.common.phone') }}</th>
						<th class="column-title">{{ trans('admin.common.course') }}</th>
						<th class="column-title">{{ trans('admin.common.status') }}</th>
						<th class="column-title">{{ trans('admin.common.sent') }}</th>
						<th class="column-title">{{ trans('admin.common.amount') }}</th>
						<th class="column-title">{{ trans('admin.common.date') }}</th>
						<th class="column-title">{{ trans('admin.common.by') }}</th>
						<th class="column-title no-link last"><span class="nobr">{{ trans('admin.common.actions') }}</span></th>

						<th class="bulk-actions" colspan="10">
							<a class="antoo" style="color:#fff; font-weight:500;">{{ trans('admin.common.action') }} (
								<span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
						</th>
					</tr>
					</thead>

					<tbody>
					@foreach($orders as $order)
						<tr class="even pointer" style="background: {{ !is_null($order->user_id)?'#E9E8E9':'' }};">
							<td class="a-center ">
								<input type="checkbox" data-name="table_records" class="flat" name="orders[]" value="{{ $order->id }}">
							</td>
							<td class=" ">{{ $order->ref_or_id }}</td>
							<td class=" ">{{ str_limit($order->name,50) }}</td>

							<td class=" ">{{ $order->phone }}</td>
							<td class=" ">{{ $order->languages_names }}</td>
							<td class=" ">
								<span data-action="editable" data-value="{{ $order->status }}" data-source="{{ route('admin.orders.status.list') }}" data-type="select" data-pk="{{ $order->id }}" data-url="{{ route('admin.orders.editable') }}" id="status" data-title="Status">{{ $order->status_name }}</span>
							</td>
							<td class=" ">{{ $order->shipped ? 'Sent' : 'Not sent' }}</td>
							<td class=" ">{{trans('common.currency.symbol', [], null, $order->lang)}} {{ $order->price }}</td>
							<td class=" ">{{ $order->created_at }}</td>
							<td class=" ">
								@if (!is_null($order->user_id))
									{{ $order->user->name }} {{$order->user->stamp}}
								@else
									{{ trans('admin.common.customer') }}
								@endif
							</td>

							<td class=" last">
								{{--@if ($order->method!=7)--}}
									{{--<a href="{{ route('admin.orders.bank.transfer.confirm',['id' => $order->id]) }}" class="btn btn-xs btn-success">{{ trans('admin.button.to-bank-transfer') }}</a>--}}
								{{--@endif--}}

								{{--@if(Entrust::ability('admin', 'edit-order'))--}}
									{{--<a href="{{ route('admin.orders.restore',['id' => $order->id]) }}" class="btn btn-xs btn-info"><i class="fa fa-refresh"></i> {{ trans('admin.button.restore') }}</a>--}}
								{{--@endif--}}

								{{--@if(Entrust::ability('admin', 'delete-order') && !$order->invoice()->first())--}}
									{{--<a href="{{ route('admin.orders.delete',['id' => $order->id]) }}" class="btn btn-xs btn-danger confirm"><i class="fa fa-remove"></i> {{ trans('admin.button.delete') }}</a>--}}
								{{--@endif--}}
							</td>
						</tr>
					@endforeach
					<tr>
						<td colspan="11">
							{{--@if(Entrust::ability('admin', 'delete-order'))--}}
							{{--<div class="btn btn-danger" id="delete-marked-button">{{ trans('admin.button.delete-marked') }}</div>--}}
							{{--<span class="delete-marked">{{ trans('admin.common.are-you-sure') }}</span>--}}
							{{--{!! Form::submit(trans('admin.common.yes'),[--}}
								{{--'class' => 'btn btn-danger delete-marked',--}}
								{{--'name' => 'delete-button'--}}
							{{--]) !!}--}}
							{{--@endif--}}
						</td>
					</tr>
					</tbody>

				</table>
				{!! Form::close() !!}

				<div class="text-right">
					@if(!$orders->isEmpty())
						@include('part.goToPageForm')
					@endif

					{!! $orders->appends(['f' => Request::input('f')])->render() !!}
				</div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>

</div> 

@endsection

@section('scripts')

	<!-- datepicker -->
	<script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
	<script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

	<script type="text/javascript">
		$('#summary').daterangepicker({
			singleDatePicker: true,
			startDate: $('#summary').val()
		}, function (start, label) {
			console.log(start.toISOString(), label);
		});
	</script>
	<!-- /datepicker -->

	<script type="text/javascript">
		$('#delete-marked-button').click(function(){
			$(this).hide();
			$('.delete-marked').show();
		});

		$('.calls').change(function(){
			var url = encodeURI("{{ route('admin.orders.calls.save') }}/"+$(this).attr('name')+"/"+$(this).val());

			$.get( url );

		});
	</script>

@endsection