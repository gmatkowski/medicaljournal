@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.orders') }} @endsection
@section('content')

    <!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-sm-6">
                    <div class="x_title">
                        <h2>{{ trans('admin.menu.cdata') }}</h2>
                        <div class="clearfix"></div>
                    </div>

                </div>

            </div>

            <div class="x_content">
                Total: {{ $total }}
                <form action="" method="get" enctype="multipart/form-data">
                    <table class="table table-striped responsive-utilities jambo_table">
                        <thead>
                        <tr class="headings">
                            <th class="column-title">{{ trans('admin.common.name') }}<br>&nbsp;</th>
                            <th class="column-title">
                                Province<br>
                                <input type="text" name="province" class="filter" value="{{ Input::get('province') }}"/>
                            </th>
                            <th class="column-title">
                                City<br>
                                <input type="text" name="city" class="filter" value="{{ Input::get('city') }}"/>
                            </th>
                            <th class="column-title">
                                District<br>
                                <input type="text" name="district" class="filter" value="{{ Input::get('district') }}"/>
                            </th>
                            <th class="column-title">
                                Subdistrict<br>
                                <input type="text" name="subdistrict" class="filter"
                                       value="{{ Input::get('subdistrict') }}"/>
                            </th>
                            <th class="column-title">
                                Age<br>
                                <div style="width:60px; ">
                                    <input type="text" name="age_from" class="filter age"
                                           value="{{ Input::get('age_from') }}"/> -
                                    <input type="text" name="age_to" class="filter age" value="{{ Input::get('age_to') }}"/>
                                </div>
                            </th>
                            <th class="column-title">
                                Sex<br>
                                <input type="text" name="sex" class="filter" value="{{ Input::get('sex') }}"/>
                            </th>
                            <th class="column-title">
                                Other lang<br>
                                <div style="width:150px; ">
                                    <input type="text" name="other_lang" class="filter"
                                           value="{{ Input::get('other_lang') }}"/>
                                    <button class="filter submit"><i class="fa fa-filter"></i></button>
                                    <a class="filter" href="{{ route('admin.orders.cdata') }}">Clear</a>
                                </div>
                            </th>
                        </tr>
                        </thead>

                        <tbody>

                        @foreach($orders as $order)
                            <tr>
                                <td class="column-title">{{ $order->address->full_name }}</td>
                                <td class="column-title">{{ $order->address->district->city->province->name }}</td>
                                <td class="column-title">{{ $order->address->district->city->name }}</td>
                                <td class="column-title">{{ $order->address->district->name }}</td>
                                <td class="column-title">{{ $order->address->subdistrict }}</td>
                                <td class="column-title">{{ $order->age }}</td>
                                <td class="column-title">{{ $order->sex }}</td>
                                <td class="column-title">{{ $order->other_lang }}</td>
                            </tr>
                        @endforeach

                        </tbody>

                    </table>
                </form>

                <div class="text-right">

                    {!! $orders->appends([
                        'province' => Input::get('province'),
                        'city' => Input::get('city'),
                        'district' => Input::get('district'),
                        'subdistrict' => Input::get('subdistrict'),
                        'age_from' => Input::get('age_from'),
                        'age_to' => Input::get('age_to'),
                        'sex' => Input::get('sex'),
                        'other_lang' => Input::get('other_lang'),
                    ])->render() !!}

                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection
