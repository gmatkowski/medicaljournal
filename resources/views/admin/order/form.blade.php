@extends('layout.admin')
@section('title') :: Orders :: {{ $mode }} @endsection
@section('content')

	<!-- page content -->
<div class="right_col" role="main">

	<div class="page-title">
		<div class="title_left">
			<h3>{{ ucfirst($mode) }} order</h3>
		</div>
	</div>
	<div class="clearfix"></div>

	@include('admin.errors')
	{!! form_start($form,['class' => 'form-horizontal form-label-left', 'url' => route('admin.orders.update', ['?product='.$productSymbol]) ]) !!}
	<div class="col-sm-6">
		<div class="x_panel">
			<div class="x_title">
				<h2>Client data</h2>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">


				<div class="col-sm-6 offset-0">
					{!! form_row($form->first_name) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-6 offset-0">
					{!! form_row($form->last_name) !!}
				</div>

				<div class="clearfix"></div>

				<div class="col-sm-3 offset-0">
					{!! form_row($form->email) !!}
				</div>

				<div class="col-sm-3 offset-1">
					{!! form_row($form->phone) !!}
				</div>
				<div class="col-sm-3 offset-1">
					@if($order->method===7)
						{!! form_row($form->unique_number) !!}
					@else
						{!! form_row($form->unique_number, ['attr' => ['style' => 'display:none'], 'label_attr' => ['style' => 'display:none']]) !!}
					@endif
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-3 offset-0">
					{!! form_row($form->age) !!}
				</div>

				<div class="col-sm-3 offset-1">
					{!! form_row($form->sex) !!}
				</div>
				<div class="col-sm-3 offset-1">
					{!! form_row($form->size) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-6 offset-0">
					{!! form_row($form->other_lang) !!}
				</div>
				<div class="clearfix"></div>

				<div class="col-sm-12 offset-0">
					{!! form_row($form->notes) !!}
				</div>
				<div class="clearfix"></div>

			</div>
		</div>



	</div>
	<div class="col-sm-6">

		<div class="x_panel">
			<div class="x_title">
				<h2>Products</h2>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<div class="col-sm-12 offset-0">

					@foreach($languages as $key => $language)
						@if($language->is_active == 1)
							<div class="col-sm-12">
								<div class="form-group">
									<div class="col-sm-1">
										{!! Form::checkbox('languages['.$language->id.'][id]',$language->id, $order->hasProduct($language->id), ['class'=>'flat']) !!}
									</div>
									<div class="col-sm-2">{{ $language->name }}</div>

									<div class="col-sm-1">{!! Form::label('languages['.$language->id.'][qty]','Qty') !!}</div>
									<div class="col-sm-2">
										@if($key==9)
											{!! Form::select('languages['.$language->id.'][qty]',
											[
											'1' => '1',
											], $order->getQty($language->id), ['id' => 'product'.++$key.'qty', 'class' => 'form-control selectable product'.$key]) !!}
										@elseif($key==15 || $key==16 || $key==17)
											{!! Form::select('languages['.$language->id.'][qty]',
											[
											'3' => '3',
											'6' => '6',
											'9' => '9',
											], 3, ['id' => 'product'.++$key.'qty', 'class' => 'form-control selectable product'.$key]) !!}
										@else
											{!! Form::select('languages['.$language->id.'][qty]',
											[
											'1' => '1',
											'2' => '2',
											'3' => '3',
											'4' => '4',
											'5' => '5',
											'6' => '6'
											], $order->getQty($language->id), ['id' => 'product'.++$key.'qty', 'class' => 'form-control selectable product'.$key]) !!}
										@endif
									</div>

									<div class="col-sm-1">{!! Form::label('languages['.$language->id.'][price]','Price') !!}</div>
									<div class="col-sm-2">
										{!! Form::text('languages['.$language->id.'][price]', $order->getLanguagePrice($language->id) ? $order->getLanguagePrice($language->id) : $language->price, ['id' => 'product'.+$key.'price', 'class' => 'form-control', 'onkeydown' => Entrust::hasRole(['admin', 'ccadmin', 'css']) ? '' : 'return false;']) !!}
									</div>
									<div class="col-sm-1">{!! Form::label('languages['.$language->id.'][total_price]','Total Price') !!}</div>
									<div class="col-sm-2">
										{!! Form::text('languages['.$language->id.'][total_price]', 0, ['id' => 'product'.$key.'totalPrice', 'class' => 'form-control', 'disabled' => 'disabled', 'style' => 'width:85px']) !!}
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
						@endif
					@endforeach
				</div>
				<div class="clearfix"></div>

			</div>
		</div>

		<div class="x_panel">
			<div class="x_title">
				<h2>Address data</h2>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<div id="address">
					<div class="col-sm-6 offset-0">
						{!! form_row($form->postal_code, [
							'selected' => isset($order->address->subDistrict->postcode) ? $order->address->subDistrict->postcode->id : [],
							'attr' => [
								'class' => 'form-control selectable',
								'data-sequence' => 1,
								'data-name' => 'postalcode'
							]
						]) !!}
					</div>
					<div class="col-sm-6">
						{!! form_row($form->subdistrict, [
							'selected' => isset($order->address->subDistrict) ? $order->address->subDistrict->id : [],
							'attr' => [
								'class' => 'form-control selectable last',
								'data-sequence' => 2,
								'data-name' => 'subdistrict'
							]
						]) !!}
					</div>
					<div class="col-sm-6 offset-0">
						{!! form_row($form->district_id, [
							'selected' => isset($order->address->district) ? $order->address->district->id : [],
							'attr' => [
								'disabled' => true,
								'class' => 'form-control selectable',
								'data-sequence' => 3,
								'data-name' => 'district'
							]
						]) !!}
						{!! form_row($form->hiddenDistrict, ['value' => isset($order->address->district->id) ? $order->address->district->id : '']) !!}
					</div>
					<div class="col-sm-6">
						{!! form_row($form->city_id, [
							'selected' => isset($order->address->district) ? $order->address->district->city->id : [],
							'attr' => [
								'disabled' => true,
								'class' => 'form-control selectable',
								'data-sequence' => 4,
								'data-name' => 'city'
							]
						]) !!}
						{!! form_row($form->hiddenCity, ['value' => isset($order->address->district->city->id) ? $order->address->district->city->id  : '']) !!}
					</div>
					<div class="col-sm-6 offset-0">
						{!! form_row($form->province_id, [
                            'selected' => isset($order->address->district) ? $order->address->district->city->province->id : [],
                            'attr' => [
                            	'disabled' => true,
                                'class' => 'form-control selectable',
                                'data-sequence' => 5,
                                'data-name' => 'province'
                            ]
                        ]) !!}
						{!! form_row($form->hiddenProvince, ['value' => isset($order->address->district->city->province->id) ? $order->address->district->city->province->id : '']) !!}
					</div>
				</div>

				<div class="col-sm-6">
					{!! form_row($form->country_id) !!}
				</div>

				<div class="col-sm-12 offset-0">
					{!! form_row($form->address, ['value' => isset($order->address) ? $order->address->address : '']) !!}
				</div>
				<div class="clearfix"></div>

			</div>
		</div>
	</div>

	<div class="col-sm-12">
		{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
	</div>

	<div class="clearfix"></div>

	{!! form_end($form) !!}

</div>

<script type="text/javascript">
	$(function () {

        var prices = {
            product1:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product2:
                {
                    1 : 450000,
                    2 : 900000,
//                    '2in1' : 450000,
					3 : 990000,
					4 : 1800000,
					5 : 2250000,
					6 : 1800000
            },
            product3:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product4:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product5:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product6:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product7:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product8:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product9:
                {
                    1 : 590000,
                    2 : 1180000,
					3 : 1500000,
					4 : 2360000,
					5 : 2950000,
					6 : 2360000
            	},
            product10:
                {
                    1 : 990000,
                },
            product11:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product12:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product13:
                {
                    1 : 590000,
                    2 : 1180000,
                    3 : 1500000,
                    4 : 2360000,
                    5 : 2950000,
                    6 : 2360000
                },
            product14:
                {
                    1 : 590000,
                    2 : 1180000,
                    3 : 1500000,
                    4 : 2360000,
                    5 : 2950000,
                    6 : 2360000
                },
            product15:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product16:
                {
                    3 : 1250000,
                    6 : 2500000,
                    9 : 3750000,
                },
            product17:
                {
                    3 : 1250000,
                    6 : 2500000,
                    9 : 3750000,
                },
            product18:
                {
                    3 : 1250000,
                    6 : 2500000,
                    9 : 3750000,
                },
            product19:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product20:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
            product21:
                {
                    1 : 490000,
                    2 : 980000,
                    3 : 1250000,
                    4 : 1960000,
                    5 : 2450000,
                    6 : 1960000
                },
        };

		$.each([ 'product1', 'product2', 'product3', 'product4', 'product5', 'product6', 'product7', 'product8', 'product9', 'product10', 'product11', 'product12', 'product13', 'product14', 'product15', 'product16', 'product17', 'product18', 'product19', 'product20', 'product21' ], function( index, value ) {
			var select = $('select.'+value);

				$('#'+value+'totalPrice').val($('#'+value+'price').val()*select.val());

				select.change(function () {
					var promoPrice = prices[value][this.value];

					$('#'+value+'totalPrice').val(promoPrice);
					$('#'+value+'price').val(promoPrice/this.value);
				});

            $('#'+value+'price').on("input",function(){
                $('#'+value+'totalPrice').val($('#'+value+'qty').val()*$(this).val());
            });
		});

	});
</script>

@endsection
