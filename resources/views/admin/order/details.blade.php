@extends('layout.admin')
@section('title') :: {{ trans('admin.orders.order') }} :: {{ $order->id }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="x_panel">
		<div class="x_title">
			<h2>{{ trans('admin.orders.order') }}
				<small>{{ $order->id }}</small>
			</h2>

			<div class="clearfix"></div>
		</div>

		<div class="x_content">

			<section class="content invoice">
				<!-- title row -->
				<div class="row">
					<div class="col-xs-12 invoice-header">
						<h1>
							<i class="fa fa-globe"></i> {{ trans('admin.orders.order') }}.
							<small class="pull-right">{{ trans('admin.orders.date') }} {{ $order->created_at_formated->format('d/m/Y') }}</small>
						</h1>
					</div>
					<!-- /.col -->
				</div>
				<!-- info row -->
				<div class="row invoice-info">
					<div class="col-sm-4 invoice-col">
						From
						<address>
							<strong>{{ Config::get('order.from') }}</strong> <br>{{ Config::get('order.address') }}
							<br>{{ Config::get('order.city') }}
							<br>{{ trans('admin.orders.phone') }} {{ Config::get('order.phone') }} <br>Email: {{ Config::get('order.email') }}
						</address>
					</div>
					<!-- /.col -->
					<div class="col-sm-4 invoice-col">
						To
						<address>
							<strong>{{ $order->name }}</strong>
							@if($order->isCOD() && isset($order->address->district))
								<br>{{ $order->address->address_full_state }}
								@if(isset($order->address->district))
									<br>{{ $order->address->address }}
								@endif
							@elseif($order->old==1 && isset($order->old_province) && isset($order->old_city) && isset($order->old_district))
								<br>{{ $order->old_province }}, {{ $order->old_city }}, {{ $order->old_district }}
							@endif
							<br>{{ trans('admin.orders.phone') }} {{ $order->phone }} <br>Email: {{ $order->email }}
						</address>
					</div>
					<!-- /.col -->
					<div class="col-sm-4 invoice-col">
						<b>{{ trans('admin.orders.orderid') }}:</b> {{ $order->id }} <br>
						<b>{{ trans('admin.orders.paid') }}:</b> {{ $order->payed_at_formated->format('d/m/Y H:i:s') }} <br>
						<b>{{ trans('admin.orders.linksent') }}:</b> {{ $order->sended_at_formated->format('d/m/Y H:i:s') }} <br>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->

				<!-- Table row -->
				<div class="row">
					<div class="col-xs-12 table">
						<table class="table table-striped">
							<thead>
							<tr>
								<th>{{ trans('admin.orders.qty') }}</th>
								<th>{{ trans('admin.orders.product') }}</th>
								<th>{{ trans('admin.orders.productid') }}</th>
								<th>{{ trans('admin.orders.price') }}</th>
							</tr>
							</thead>
							<tbody>
								@foreach($order->languages as $language)
									<tr>
										<td>{{ ($language->price==245000 || $language->price==225000) ? '2in1' : $language->qty }}</td>
										<td>{{ $language->language->name }}</td>
										<td>{{ $language->language->id }}</td>
										<td>{{ trans('common.currency.symbol', [], null, $order->lang) }} {{ round($language->price, 3) }}</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->

				<div class="row">
					<!-- accepted payments column -->
					<div class="col-xs-6">

					</div>
					<!-- /.col -->
					<div class="col-xs-6">
						<p class="lead">{{ trans('admin.orders.paidtodate') }} {{ $order->payed_at_formated->format('d/m/Y') }}</p>

						<div class="table-responsive">
							<table class="table">
								<tbody>
								<tr>
									<th>{{ trans('admin.orders.total') }}{{ trans('admin.orders.price') }}:</th>
									<td>{{ trans('common.currency.symbol', [], null, $order->lang) }} {{ $order->price }}</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- /.col -->
				</div>
				<!-- /.row -->


				<div class="row no-print">
					<div class="col-xs-12">
						{{--<button class="btn btn-default" onclick="window.print();"><i class="fa fa-print"></i> Print--}}
						{{--</button>--}}
						{{--<button class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment--}}
						{{--</button>--}}
						{{--<button class="btn btn-primary pull-right" style="margin-right: 5px;">--}}
							{{--<i class="fa fa-download"></i> Generate PDF--}}
						{{--</button>--}}

						@if($order->invoice && $order->invoice->credit_no)
							<form action="{{ route('admin.orders.invoice', ['id' => $order->id, 'invoiceType' => 'cancel']) }}" method="POST">
								<button type="submit" class="btn btn-primary pull-right" style="margin-right: 5px;"><i class="fa fa-remove"></i> Credit Note</button>
							</form>
						@endif
					</div>
				</div>

			</section>
		</div>

	</div>

</div>

@endsection
