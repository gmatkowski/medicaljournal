@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.settings') }} @endsection
@section('content')
		
		<!-- page content -->
<div class="right_col" role="main">
	
	@include('admin.errors')
	

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-6">
					<div class="x_title">
						<h2>{{ trans('admin.menu.settings') }}
							<small>{{ trans('admin.common.list') }}</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>

			<div class="x_content">

				{!! form($form) !!}

			</div>
		</div>
	</div>

</div>

@endsection
