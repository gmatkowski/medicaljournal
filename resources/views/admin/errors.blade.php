@if(Session::has('message'))
	<div class="x_content">
		<div class="alert alert-{{ Session::get('message.type') }} alert-dismissible fade in" role="alert">
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
				<span aria-hidden="true">×</span>
			</button>
			{{ Session::get('message.message') }}
		</div>
	</div>
@endif