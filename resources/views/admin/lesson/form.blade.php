@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.lessons') }} :: {{ $mode }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="x_panel">
		<div class="x_title">
			<h2>{{ trans('admin.menu.lessons') }}
				<small>{{ $mode }}</small>
			</h2>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			{!! form_start($form,['class' => 'form-horizontal form-label-left']) !!}

			<div class="" role="tabpanel" data-example-id="togglable-tabs">
				<ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
					@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
						<li role="presentation" @if($localeCode == LaravelLocalization::getCurrentLocale()) class="active" @endif >
							<a href="#lang_{{ $localeCode }}" id="lang-{{ $localeCode }}" role="tab" data-toggle="tab" aria-expanded="true">{{ $properties['name'] }}</a>
						</li>
					@endforeach
				</ul>
				<div id="myTabContent" class="tab-content">
					@foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
						<div role="tabpanel" class="tab-pane fade @if($localeCode == LaravelLocalization::getCurrentLocale()) active in @endif " id="lang_{{ $localeCode }}" aria-labelledby="lang-{{ $localeCode }}">

							<div class="form-group">
								<label for="title" class="control-label">{{ trans('admin.common.name') }}</label>
								{!! Form::input('text', 'translation[' . $localeCode . '][name]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->name : ''), ['class' => 'form-control']) !!}
							</div>

							<div class="form-group">
								<label for="content" class="control-label">{{ trans('admin.common.desc') }}</label>
								{!! Form::textarea('translation[' . $localeCode . '][description]', (isset($object) && $object->hasTranslation($localeCode) ? $object->translate($localeCode)->description : ''), ['class' => 'form-control col-sm-3']) !!}
							</div>
						</div>
					@endforeach
				</div>
			</div>

			<div class="col-sm-3 offset-0">
			{!! form_row($form->level) !!}
			</div>
			<div class="clearfix"></div>

			{!! form_row($form->icon) !!}

			{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
			{!! form_end($form) !!}
		</div>
	</div>

</div>

<script type="text/javascript">
	$(function () {
		$('textarea').summernote({
			height: 300,
		});
	});
</script>

@endsection
