@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.lessons') }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="container-fluid">
				<div class="col-sm-6">
					<div class="x_title">
						<h2>{{ trans('admin.menu.lessons') }}
							<small>{{ trans('admin.common.list') }}</small>
						</h2>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="col-sm-6 text-right">
					<a href="{{ route('admin.lessons.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> {{ trans('admin.lessons.add') }}</a>
				</div>
			</div>

			<div class="x_content">

				<table class="table table-striped responsive-utilities jambo_table">
					<thead>
					<tr class="headings">
						<th class="column-title">{{ trans('admin.common.id') }}</th>
						<th class="column-title">{{ trans('admin.common.icon') }}</th>
						<th class="column-title">{{ trans('admin.common.name') }}</th>
						<th class="column-title">{{ trans('admin.lessons.level') }}</th>
						<th class="column-title">{{ trans('admin.common.date') }}</th>
						<th class="column-title no-link last"><span class="nobr">{{ trans('admin.common.actions') }}</span>
						</th>
					</tr>
					</thead>

					<tbody>
					@foreach($objects as $object)
						<tr class="even pointer">
							<td class=" ">{{ $object->id }}</td>
							<td class=" ">@if(!empty($object->icon))<img src="{{ $object->icon_path }}" alt=""/>@endif</td>
							<td class=" ">{{ $object->name }}</td>
							<td class=" ">{{ $object->level_name }}</td>
							<td class=" ">{{ $object->created_at }}</td>
							<td class=" last">
								<a href="{{ route('admin.lessons.edit',['id' => $object->id]) }}">{{ trans('admin.common.edit') }}</a>
								<a href="{{ route('admin.lessons.delete',['id' => $object->id]) }}" class="confirm">{{ trans('admin.common.delete') }}</a>
							</td>
						</tr>
					@endforeach
					</tbody>

				</table>

				<div class="text-right">
					{!! $objects->render() !!}
				</div>

			</div>
		</div>
	</div>

	<div class="clearfix"></div>

</div>

@endsection
