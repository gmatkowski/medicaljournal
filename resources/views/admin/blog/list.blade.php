@extends('layout.admin')
@section('title') :: BLOG @endsection
@section('content')

        <!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-sm-6">
                    <div class="x_title">
                        <h2>{{ trans('admin.blog.articles') }}
                            <small>{{ trans('admin.blog.list') }}</small>
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-sm-6 text-right">
                    <a href="{{ route('admin.blog.create') }}" class="btn btn-success"><i class="fa fa-plus-circle"></i> {{ trans('admin.blog.add') }}</a>
                </div>
            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">{{ trans('admin.blog.id') }}</th>
                        <th class="column-title">{{ trans('admin.blog.title') }}</th>
                        <th class="column-title">{{ trans('admin.blog.date') }}</th>
                        <th class="column-title no-link last"><span class="nobr">{{ trans('admin.blog.actions') }}</span>
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($objects as $object)
                        <tr class="even pointer">
                            <td class=" ">{{ $object->id }}</td>
                            <td class=" ">{{ $object->title }}</td>
                            <td class=" ">{{ $object->created_at }}</td>
                            <td class=" last">
                                <a href="{{ route('admin.blog.edit',['id' => $object->id]) }}">{{ trans('admin.blog.edit') }}</a>
                                <a href="{{ route('admin.blog.delete',['id' => $object->id]) }}" class="confirm">{{ trans('admin.blog.delete') }}</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>

                <div class="text-right">
                    {{--{!! $objects->render() !!}--}}
                </div>

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection
