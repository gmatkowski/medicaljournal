@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.personal.stats') }} @endsection
@section('content')

        <!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>
                            Personal Stats
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    @role(['admin', 'ccadmin'])
                        <form class="form-horizontal" action="{{ route('admin.personalstats.index') }}" method="get">
                            <div class="input-prepend input-group pull-right" style="width:230px;">
                                            <span class="add-on input-group-addon"><i
                                                        class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" style="width: 200px" name="range" id="reservation"
                                       class="form-control" value="{{ $range }}"/>
                            </div>
                            <input type="submit" class="btn btn-success pull-right" value="Export"/>
                        </form>
                        <br>
                    @endrole
                </div>

            </div>

        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection

@section('scripts')

    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#reservation').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <!-- /datepicker -->

@endsection
