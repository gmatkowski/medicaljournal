@extends('layout.admin')
@section('title') :: orders :: {{ $mode }} @endsection
@section('content')

		<!-- page content -->
<div class="right_col" role="main">

	@include('admin.errors')

	<div class="x_panel">
		<div class="x_title">
			<h2>Zamówienia
				<small>{{ $mode }}</small>
			</h2>

			<div class="clearfix"></div>
		</div>
		<div class="x_content">

			{!! form_start($form,['class' => 'form-horizontal form-label-left']) !!}

			<div class="form-group">
				{!! form_label($form->first_name,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->first_name,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->first_name) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->last_name,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->last_name,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->last_name) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->email,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->email,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->email) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->phone,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->phone,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->phone) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->price,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="row">
						<div class="col-sm-9">
							{!! form_widget($form->price,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
							{!! form_errors($form->price) !!}
						</div>
						<div class="col-sm-3">
							{{ $order->currency }}
						</div>
					</div>
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->product,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->product,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->product) !!}
				</div>
			</div>

			<div class="form-group">
				{!! form_label($form->status,['label_attr' => ['class' =>'control-label col-md-3 col-sm-3 col-xs-12']]) !!}
				<div class="col-md-6 col-sm-6 col-xs-12">
					{!! form_widget($form->status,['attr' => ['class' =>'form-control col-md-7 col-xs-12']]) !!}
					{!! form_errors($form->status) !!}
				</div>
			</div>

			<div class="ln_solid"></div>
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
				{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success']]) !!}
			</div>

			{!! form_row($form->submit,['attr' => ['class' => 'btn btn-success col-sm-12']]) !!}
			{!! form_end($form) !!}

		</div>
	</div>

</div>

@endsection
