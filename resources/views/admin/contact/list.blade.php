@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

        <!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>All {{ trans('admin.menu.contacts') }}
                            <small>{{ trans('admin.menu.list') }}</small>
                        </h2>

                        <div class="clearfix"></div>
                    </div>
                    @role(['admin', 'ccadmin'])
                        <b>ADDED :</b> TODAY: <b>{{ $counters['today'] }}</b>, YESTERDAY: <b>{{ $counters['yesterday'] }}</b>,
                           RESULT OF SPECIFIC DATE FROM CALENDAR: <b>{{ $counters['specificDay'] }}</b>
                        <br><br>
                        <form action="{{ route('admin.contacts') }}" class="form-horizontal" method="get">
                            <div class="input-prepend input-group pull-right" style="width:230px;">
                                            <span class="add-on input-group-addon">
                                                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                            </span>
                                <input type="text" style="width: 200px" name="summaryRange" id="summary"
                                       class="form-control" value="{{ $time['yesterday'] }} - {{ $time['now'] }}"/>
                                <input type="hidden" name="product" value="{{ $prod }}"/>
                            </div>
                            <input type="submit" class="btn btn-success pull-right" value="Pick Date To Summary"/>
                        </form>
                        <br>
                    @endrole
                </div>

                @if(Entrust::ability('admin', 'export-contacts'))
                    <div class="col-md-3 col-md-offset-3">

                        <form class="form-horizontal" action="{{ route('admin.contacts.export') }}" method="post">
                            <div class="input-prepend input-group pull-right" style="width:230px;">
                                        <span class="add-on input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" style="width: 200px" name="range" id="export"
                                       class="form-control" value="{{ $time['yesterday'] }} - {{ $time['now'] }}"/>
                                <input type="hidden" name="product" value="{{ $prod }}"/>
                            </div>
                            <input type="submit" class="btn btn-success pull-right" value="{{trans('admin.button.export')}}"/>
                        </form>

                    </div>
                @endif

            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">{{trans('admin.common.id')}}</th>
                        <th class="column-title">{{trans('admin.common.name')}}</th>
                        @if(!Entrust::hasRole('headcc'))
                            <th class="column-title">{{trans('admin.common.email')}}</th>
                            <th class="column-title">{{trans('admin.common.phone')}}</th>
                        @endif
                        {{--<th class="column-title">{{trans('admin.common.popup')}}</th>--}}
                        <th class="column-title">{{trans('admin.common.sendedcc')}}</th>
                        <th class="column-title">{{trans('admin.common.date')}}</th>
                        <th class="column-title no-link last"><span class="nobr">{{trans('admin.common.actions')}}</span>
                        </th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($objects as $object)
                        <tr class="even pointer">
                            <td class=" ">{{ $object->id }}</td>
                            <td class=" ">{{ $object->name }}</td>
                            @if(!Entrust::hasRole('headcc'))
                                <td class=" ">{{ $object->email }}</td>
                                <td class=" ">{{ $object->phone }}</td>
                            @endif
                            {{--<td class=" ">{{ $object->popup==1 ? trans('admin.common.yes') : trans('admin.common.no')}}</td>--}}
                            <td class=" ">{{ $object->call_center==1 ? trans('admin.common.yes') : trans('admin.common.no')}}</td>
                            <td class=" ">{{ $object->created_at }}</td>
                            <td class=" last">
                                @if(Entrust::ability('admin', 'delete-contacts') && !$object->call_center)
                                <a href="{{ route('admin.contacts.delete',['id' => $object->id]) }}"
                                   class="confirm">{{ trans('admin.button.delete')}}</a>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>

                <div class="text-right">
                    @if(!$objects->isEmpty())
                        @include('part.goToPageForm')
                    @endif
                </div>

                <div class="text-right">
                    @if ($mode != 'search' && $objects->count() > 0)
                        {!! $objects->render() !!}
                    @endif
                </div>



            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>



@endsection

@section('scripts')

    <!-- datepicker -->
        <script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
        <script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#summary').daterangepicker(null, function (start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });
                $('#export').daterangepicker(null, function (start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });
//                $('#summary').daterangepicker({
//                    singleDatePicker: true,
//                    startDate: $('#summary').val()
//                }, function (start, label) {
//                    console.log(start.toISOString(), label);
//                });
            });
        </script>
    <!-- /datepicker -->

@endsection