@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

        <!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>
                            All External Contacts : {{ $counters['all'] }}
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    @role(['admin', 'ccadmin'])
                    RESULT FROM CALENDAR DATE: <b>{{ $counters['specificDate'] }}</b>
                    <br>
                    SOLD FROM CALENDAR DATE: <b>{{ $counters['soldInSpecificDate'] }}</b>
                    <br><br>
                    <form action="{{ route('admin.external.clients') }}" class="form-horizontal" method="get">
                        <div class="input-prepend input-group pull-right" style="width:230px;">
                                        <span class="add-on input-group-addon">
                                            <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
                                        </span>
                            <input type="text" style="width: 200px" name="summaryRange" id="summary"
                                   class="form-control" value="{{ $time['yesterday'] }} - {{ $time['now'] }}"/>
                            <input type="hidden" name="product" value="{{ $productSymbol }}"/>
                        </div>
                        <input type="submit" class="btn btn-success pull-right" value="Pick Date To Summary"/>
                    </form>
                    <br>
                    @endrole
                </div>

                @if(Entrust::ability('admin', 'export-contacts'))
                    <div class="col-md-3 col-md-offset-3">

                        <form class="form-horizontal" action="{{ route('admin.external.export') }}" method="post">
                            <div class="input-prepend input-group pull-right" style="width:230px;">
                                        <span class="add-on input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="text" style="width: 200px" name="range" id="export"
                                       class="form-control" value="{{ $time['yesterday'] }} - {{ $time['now'] }}"/>
                                <input type="hidden" name="product" value="{{ $productSymbol }}"/>
                            </div>
                            <input type="submit" class="btn btn-success pull-right" value="{{trans('admin.button.export')}}"/>
                        </form>

                    </div>
                @endif

            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">Call Center Id</th>
                        <th class="column-title">Pub Id</th>
                        <th class="column-title">{{trans('admin.common.name')}}</th>
                        @if(!Entrust::hasRole('headcc'))
                            <th class="column-title">{{trans('admin.common.phone')}}</th>
                        @endif
                        <th class="column-title">{{trans('admin.common.extClName')}}</th>
                        <th class="column-title">click_id</th>
                        <th class="column-title">{{trans('admin.common.status')}}</th>
                        <th class="column-title">{{trans('admin.common.price')}} ({{ $currency }})</th>
                        <th class="column-title">{{trans('admin.common.date')}}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($objects as $object)
                        <tr class="even pointer">
                            <td class=" ">
                                @if($object->cc_id )
                                    {{ $object->cc_id }}
                                @else
                                    @if(StrHelper::validatePhone($object->phone))
                                        -
                                    @else
                                        Invalid Phone
                                    @endif
                                @endif
                            </td>
                            <td class=" ">{{ $object->pub_id ? $object->pub_id : '-' }}</td>
                            <td class=" ">{{ $object->name }}</td>
                            @if(!Entrust::hasRole('headcc'))
                                <td class=" ">{{ $object->phone }}</td>
                            @endif
                            <td class=" ">{{ isset($object->externalClient) ? $object->externalClient->name : '-' }}</td>
                            <td class=" " style="max-width: 11em; word-wrap: break-word;">{{ $object->click_id ? $object->click_id : '--' }}</td>
                            @if($object->isTooLate())
                                <td class=" " style="color:red;">
                            @else
                                <td class=" ">
                            @endif
                                {{ $object->getStatus() }}</td>
                            @if($object->cc_sell==1)
                                <td class=" ">{{ $object->cc_price ? StrHelper::dotsInPrice($object->cc_price) : 'price not filled in cc' }}</td>
                            @else
                                <td class=" ">--</td>
                            @endif
                            @if($object->isTooLate())
                                <td class="  last" style="color:red;">
                            @else
                                <td class="  last">
                            @endif
                                {{ $object->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>

                <div class="text-right">
                    @if(!$objects->isEmpty())
                        @include('part.goToPageForm')
                    @endif
                </div>

                <div class="text-right">
                    @if ($mode != 'search' && $objects->count() > 0)
                        {!! $objects->render() !!}
                    @endif
                </div>

                @if(Entrust::ability('admin', 'export-contacts'))
                    <div class="col-md-3 col-md-offset-3">

                        <form class="form-horizontal" action="{{ route('admin.external.export') }}" method="post">
                            <div class="input-prepend input-group pull-right" style="width:230px;">
                                <input type="hidden" name="product" value="{{ $productSymbol }}"/>
                                <input type="hidden" name="range" value="{{ $time['yesterday'] }} - {{ $time['now'] }}"/>
                                <input type="hidden" name="notSold" value="1"/>
                            </div>
                            <input type="submit" class="btn btn-success pull-right" value="{{trans('admin.button.export')}} Not Sold"/>
                        </form>

                    </div>
                @endif

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection

@section('scripts')

    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#summary').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
            $('#export').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
//            $('#summary').daterangepicker({
//                singleDatePicker: true,
//                startDate: $('#summary').val()
//            }, function (start, label) {
//                console.log(start.toISOString(), label);
//            });
        });
    </script>
    <!-- /datepicker -->

@endsection
