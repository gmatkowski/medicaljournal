@extends('layout.admin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

        <!-- page content -->
<div class="right_col" role="main">

    @include('admin.errors')

    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="container-fluid">
                <div class="col-md-6">
                    <div class="x_title">
                        <h2>
                            Contacts : {{ $counters['all'] }}
                        </h2>
                        <div class="clearfix"></div>
                    </div>
                    @role(['admin', 'ccadmin'])
                        <form class="form-horizontal" action="{{ route('admin.externalstats.clients') }}" method="get">
                            <div class="input-prepend input-group pull-right" style="width:230px;">
                                            <span class="add-on input-group-addon"><i
                                                        class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                <input type="hidden" name="product" value="{{ $productSymbol }}"/>
                                <input type="text" style="width: 200px" name="range" id="reservation"
                                       class="form-control" value="{{ $range }}"/>
                            </div>
                            <input type="radio" name="status" value="all" {{ $status=='all' ? 'checked' : '' }}> show ALL<br>
                            <input type="radio" name="status" value="sold" {{ $status=='sold' ? 'checked' : '' }}> show SOLD<br>
                            <input type="radio" name="status" value="hold" {{ $status=='hold' ? 'checked' : '' }}> show ON HOLD<br>
                            <input type="radio" name="status" value="reject" {{ $status=='reject' ? 'checked' : '' }}> show REJECT<br>
                            <br>
                            <div class="form-group">
                                <label for="sel1">Pick client:</label>
                                <select class="form-control" id="sel1" name="client">
                                    @foreach($externals as $external)
                                        <option value="{{ $external->id }}" {{ $client && $client==$external->id ? 'selected' : '' }}>{{ $external->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="submit" class="btn btn-success pull-right" value="Search"/>
                        </form>
                        <br>
                    @endrole
                </div>

            </div>

            <div class="x_content">

                <table class="table table-striped responsive-utilities jambo_table">
                    <thead>
                    <tr class="headings">
                        <th class="column-title">Call Center Id</th>
                        <th class="column-title">{{trans('admin.common.name')}}</th>
                        @if(!Entrust::hasRole('headcc'))
                            <th class="column-title">{{trans('admin.common.phone')}}</th>
                        @endif
                        <th class="column-title">{{trans('admin.common.extClName')}}</th>
                        <th class="column-title">click_id</th>
                        <th class="column-title">{{trans('admin.common.status')}}</th>
                        <th class="column-title">{{trans('admin.common.price')}} ({{ $currency }})</th>
                        <th class="column-title">Post Back</th>
                        <th class="column-title">{{trans('admin.common.date')}}</th>
                    </tr>
                    </thead>

                    <tbody>
                    @foreach($objects as $object)
                        <tr class="even pointer">
                            <td class=" ">
                                @if($object->cc_id )
                                    {{ $object->cc_id }}
                                @else
                                    @if(StrHelper::validatePhone($object->phone))
                                        -
                                    @else
                                        Invalid Phone
                                    @endif
                                @endif
                            </td>
                            <td class=" ">{{ $object->name }}</td>
                            @if(!Entrust::hasRole('headcc'))
                                <td class=" ">{{ $object->phone }}</td>
                            @endif
                            <td class=" ">{{ isset($object->externalClient) ? $object->externalClient->name : '-' }}</td>
                            <td class=" " style="max-width: 11em; word-wrap: break-word;">{{ $object->click_id ? $object->click_id : '--' }}</td>
                            @if($object->isTooLate())
                                <td class=" " style="color:red;">
                            @else
                                <td class=" ">
                            @endif
                                {{ $object->getStatus() }}</td>
                            @if($object->cc_sell==1)
                                <td class=" ">{{ $object->cc_price ? StrHelper::dotsInPrice($object->cc_price) : 'price not filled in cc' }}</td>
                            @else
                                <td class=" ">--</td>
                            @endif
                                <td class=" ">
                                    {{ $object->postback || $object->postback_hold ? 'Yes' : 'No' }}
                                </td>
                            @if($object->isTooLate())
                                <td class="  last" style="color:red;">
                            @else
                                <td class="  last">
                            @endif
                                {{ $object->created_at }}</td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>

                <div class="text-right">
                    @if(!$objects->isEmpty())
                        @include('part.goToPageForm')
                    @endif
                </div>

                <div class="text-right">
                    @if ($mode != 'search' && $objects->count() > 0)
                        {!! $objects->render() !!}
                    @endif
                </div>

                {{--TOTAL SOLD: <b>{{ $soldCounter }}</b>--}}
                {{--<br>--}}
                {{--TOTAL ON HOLD: <b>{{ $onHoldCounter }}</b>--}}
                {{--<br>--}}
                {{--TOTAL ON HOLD TOO OLD: <b>{{ $onHoldTooOldCounter }}</b>--}}
                {{--<br>--}}
                {{--TOTAL CLOSED: <b>{{ $closedCounter }}</b>--}}
                {{--<br>--}}
                {{--TOTAL DUPLICATED: <b>{{ $duplicatedCounter }}</b>--}}
                {{--<br>--}}

            </div>
        </div>
    </div>

    <div class="clearfix"></div>

</div>

@endsection

@section('scripts')

    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('build/js/vendor/moment.min2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('build/js/vendor/daterangepicker.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#reservation').daterangepicker(null, function (start, end, label) {
                console.log(start.toISOString(), end.toISOString(), label);
            });
        });
    </script>
    <!-- /datepicker -->

@endsection
