@extends('layout.admin_singin')
@section('title') :: {{ trans('admin.menu.contacts') }} @endsection
@section('content')

    <!-- page content -->
    <div class="right_col" role="main">

        @include('admin.errors')

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="container-fluid">
                    <div class="col-md-6">
                        <div class="x_title">
                            <h2>
                                FunCPA Post Backs
                                <small>{{ trans('admin.menu.list') }}</small>
                            </h2>

                            <div class="clearfix"></div>
                        </div>
                </div>
                <div class="x_content">

                    {!! Form::open(['url' => 'api/send/postBacks']) !!}
                        <table class="table table-striped responsive-utilities jambo_table">
                            <thead>
                                <tr class="headings">
                                    <th class="column-title"></th>
                                </tr>
                            </thead>

                                <tbody>
                                        <tr class="even pointer">
                                            <td class=" ">
                                                <b>Put click_id on each line :</b>
                                                <br><br>
                                                {!! Form::textarea('phones', null, ['cols' => 200, 'rows' => 30, 'style' => 'width:auto']) !!}
                                            </td>
                                        </tr>
                                </tbody>
                        </table>
                    {!! Form::submit('Send Post Backs' ,['class' => 'btn btn-success']) !!}
                    {!! Form::close() !!}

                </div>
            </div>
        </div>

        <div class="clearfix"></div>

    </div>

@endsection
