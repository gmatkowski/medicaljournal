@extends('layout.app')
@section('title') :: {{ trans('menu.contact') }} @endsection

@section('content')
	<main>
		<section class="contact">
			<div class="container">
				<h1 class="wow fadeIn">404</h1>

				<div class="row">

					<div class="col-md-12 text-center">
						<p>
							{{ trans('common.404') }}
						</p>
					</div>
				</div>
			</div>
		</section>
	</main>

@endsection