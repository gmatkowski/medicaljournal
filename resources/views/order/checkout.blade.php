@extends('layout.app')
@section('title') :: {{ trans('menu.terms') }} {{ ucfirst(trans('routes.checkout')) }} @endsection

@section('content')
<div class="row">
	
</div>
@endsection

@section('scripts')

	@if(Config::get('veritrans.production'))
		<script src="https://api.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
	@else
		<script src="https://api.sandbox.veritrans.co.id/v2/assets/js/veritrans.min.js"></script>
	@endif
	<script type="text/javascript">
		$(function () {
			App.buy.init('{!! $packages->keyBy('in_pack')->toJson() !!}', '{!! $languagesJson !!}');
			App.buy.available.init();
			{{--App.gaEvent('Checkout', 'Show', '{{ $user_data['first_name'] }} {{ $user_data['last_name'] }} {{ $user_data['email'] }}');--}}
		});
	</script>

	@if(old('payment'))
		<script type="text/javascript">
			$(function () {

				var input = $('.payment-options-nav').find('input[name="payment"][value="{{ old('payment') }}"]');
				var parent = input.parents('li');
				parent.find('> a').trigger('click');
			})
		</script>
	@endif

@endsection