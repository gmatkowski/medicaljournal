@extends('layout.app')
@section('title') :: {{ trans('menu.languages') }} @endsection
@section('top')
	<header>
		@include('part.top')
	</header>
@endsection
@section('content')
	<main>
		<section class="contact">
			<div class="container">
				<div class="row">
					{!! form($form,['name' => 'payment-form']) !!}
				</div>
			</div>
		</section>
	</main>

@endsection

@section('scripts')
	<script type="text/javascript">
		$(function () {
			$('form[name="payment-form"]').submit();
		})
	</script>
@endsection